#!/bin/bash
#nohup redis-server &
#python manage.py migrate --settings mudra.dev_settings
#celery -A mudra worker -l info
#python manage.py runserver 0.0.0.0:8000 --settings mudra.dev_settings

nohup redis-server /etc/redis.conf &
python manage.py migrate --settings mudra.production_settings
celery -A mudra worker -l info &
python manage.py runserver 0.0.0.0:8000 --settings mudra.production_settings
