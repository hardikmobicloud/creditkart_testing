from datetime import timedelta
from datetime import datetime
import razorpay
import hmac
import hashlib

from django.apps.registry import apps
import random
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.shortcuts import redirect
from django.contrib.auth.models import User
from rest_framework.response import Response
from MApi.razorpay import payment_status, order_status
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

Personal = apps.get_model('User', 'Personal')
NPCI = apps.get_model('npci', 'NPCI')
Payments = apps.get_model('Payments', 'Payment')

# Test cred
Key_id = 'rzp_test_FIEv6FzTK4cKxr'
Key_secret = 'AUAgWyzansR434zsZDLVzTOw'

# Prod cred
# Key_id = 'rzp_live_Y5lFYWm2huQpwi'
# Key_secret = 'guqrrTgcrdYvM5OapnCd1SR5'

# website_base_url = "http://35.154.28.133:3000/"
website_base_url = "https://thecreditkart.com/"


class NpciRechargeChecksumGenerateAPIWebsite(APIView):
    """
        Razorpay Checksum API
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None
        ctx = {}
        # print("==request data", request.data)
        try:
            client_id = str(request.data.get('client_id'))
            TXN_AMOUNT = request.data.get('TXN_AMOUNT')
            random_number = random.sample(range(99999), 1)
            # Considering shopping order_id, CUST_ID and TXN_AMOUNT is getting.
            ORDER_ID = "RC" + str(client_id)
            # shopping_order_id = "123456789"
            CUST_ID = ""
            user_id = request.user.id
            if user_id:
                user = User.objects.filter(id=user_id)
                if user:
                    CUST_ID = user[0].username

            url = "http://15.206.113.44:8015/npci/callback_recharge_status_check_api/"
            amount = float(TXN_AMOUNT) * 100
            data = {'amount': amount, 'currency': 'INR', 'receipt': ORDER_ID, 'payment_capture': '1'}
            client = razorpay.Client(auth=(Key_id, Key_secret))
            order = client.order.create(data)
            personal = Personal.objects.filter(user_id=request.user.id).last()
            name = None
            dec = None
            email_id = None
            mobile_no = None
            resp_data = {}
            real_amount = order['amount'] / 100
            if personal:
                name = personal.name
                dec = personal.name
                email_id = personal.email_id
                mobile_no = personal.user.username
            if order:
                ctx['order'] = order
                resp_data = {
                    'order_id': order['id'],
                    'amount': str(real_amount),
                    'status': order['status'],
                    'created_at': order['created_at'],
                    'currency': order['currency'],
                    'image_url': '',
                    'key_id': Key_id,
                    'name': name,
                    'dec': dec,
                    'callbackurl': url,
                    'order_id_ck': ORDER_ID,
                    'email_id': email_id,
                    'mobile_no': mobile_no,
                }
                # print("response data", resp_data)
                timestamp = order['created_at']
                try:
                    timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
                    # timestamp = datetime.datetime.strptime(timestamp, "%d/%m/%Y %H:%M:%S").strftime(
                    #     '%Y-%m-%d')
                except Exception as e:
                    print(e)
                    pass
                paym = Payments.objects.create(order_id=ORDER_ID, pg_transaction_id=resp_data['order_id'], \
                                               status="initiated", amount=real_amount, platform_type="Web", \
                                               category="Recharge", product_type="Npci", date=timestamp,
                                               return_status="rstatus", pay_source="Rpay")

                if paym and paym is not None:
                    # print('client_id', client_id)
                    npci_obj = NPCI.objects.filter(client_id=client_id)
                    if npci_obj:
                        # print('npci_obj', npci_obj)
                        npci_obj[0].payment.add(paym)

                message = "Successfully Done"
                status = 1
            else:
                message = "Something Went Wrong"
                status = 0

            return Response(
                {
                    'status': status,
                    'message': message,
                    'data': resp_data
                }
            )
        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


class NpciRepaymentChecksumGenerateAPIWebsite(APIView):
    """
        Razorpay Checksum API
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None
        ctx = {}
        # print("==request data", request.data)
        try:
            client_id = str(request.data.get('client_id'))
            TXN_AMOUNT = request.data.get('TXN_AMOUNT')
            payment_mode = request.data.get('payment_mode')
            random_number = random.sample(range(99999), 1)
            # Considering shopping order_id, CUST_ID and TXN_AMOUNT is getting.
            ORDER_ID = "RCRP" + str(client_id)
            # shopping_order_id = "123456789"
            CUST_ID = ""
            user_id = request.user.id
            if user_id:
                user = User.objects.filter(id=user_id)
                if user:
                    CUST_ID = user[0].username

            if payment_mode == "wallet":
                wallet_amount = 0
                from EApi.credit import get_user_wallet_amount
                wallet_dict = get_user_wallet_amount(user_id)
                if wallet_dict:
                    wallet_amount = wallet_dict["balance_amount"]

                if wallet_amount < TXN_AMOUNT:
                    # This means wallet balance is minimum..
                    return Response(
                        {
                            "status": 2,
                            "message": "Insufficient wallet balance.."
                        }
                    )

                else:
                    from EApi.wallet import user_wallet_sub
                    # Creating payment entry..
                    payment = Payments.objects.create(status="success", order_id=ORDER_ID, amount=TXN_AMOUNT,
                                                      category='Repayment', product_type="Npci", type="Ecom_Wallet",
                                                      date=datetime.now(), platform_type="Web")

                    # Creating Wallet entry..
                    wallet_status = "repayment"
                    wallet_obj = user_wallet_sub(user_id, TXN_AMOUNT, None, None, wallet_status)

                    # Creating Credit entry..
                    credit_status = "repayment"
                    from EApi.credit import credit_balance_update
                    credit_obj = credit_balance_update(user_id, TXN_AMOUNT, credit_status)

                    if None not in (wallet_obj, credit_obj):
                        npci_obj = NPCI.objects.filter(client_id=client_id).first()
                        if npci_obj:
                            npci_obj.credit_history.add(credit_obj)
                            if payment:
                                npci_obj.payment.add(payment)

                            # Updating recharge credit status..
                            from npci.views import update_recharge_status
                            update_recharge = update_recharge_status(npci_obj.id)

                            return Response(
                                {
                                    "status": 3,
                                    "message": "Your Repayment transaction done successfully.."
                                }
                            )

                    return Response(
                        {
                            "status": 0,
                            "message": "Something went wrong.."
                        }
                    )

            if payment_mode == "cash":
                url = "http://15.206.113.44:8015/npci/callback_recharge_status_check_api/"
                amount = float(TXN_AMOUNT) * 100
                data = {'amount': amount, 'currency': 'INR', 'receipt': ORDER_ID, 'payment_capture': '1'}
                client = razorpay.Client(auth=(Key_id, Key_secret))
                order = client.order.create(data)
                personal = Personal.objects.filter(user_id=request.user.id).last()
                name = None
                dec = None
                email_id = None
                mobile_no = None
                resp_data = {}
                real_amount = order['amount'] / 100
                if personal:
                    name = personal.name
                    dec = personal.name
                    email_id = personal.email_id
                    mobile_no = personal.user.username
                if order:
                    ctx['order'] = order
                    resp_data = {
                        'order_id': order['id'],
                        'amount': str(real_amount),
                        'status': order['status'],
                        'created_at': order['created_at'],
                        'currency': order['currency'],
                        'image_url': '',
                        'key_id': Key_id,
                        'name': name,
                        'dec': dec,
                        'callbackurl': url,
                        'order_id_ck': ORDER_ID,
                        'email_id': email_id,
                        'mobile_no': mobile_no,
                    }
                    # print("response data", resp_data)
                    timestamp = order['created_at']
                    try:
                        timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
                        # timestamp = datetime.datetime.strptime(timestamp, "%d/%m/%Y %H:%M:%S").strftime(
                        #     '%Y-%m-%d')
                    except Exception as e:
                        print(e)
                        pass
                    paym = Payments.objects.create(order_id=ORDER_ID, pg_transaction_id=resp_data['order_id'], \
                                                   status="initiated", amount=real_amount, platform_type="Web", \
                                                   category="Repayment", product_type="Npci", date=timestamp,
                                                   return_status="rstatus", pay_source="Rpay")

                    if paym and paym is not None:
                        # print('client_id', client_id)
                        npci_obj = NPCI.objects.filter(client_id=client_id)
                        if npci_obj:
                            # print('npci_obj', npci_obj)
                            npci_obj[0].payment.add(paym)

                    message = "Successfully Done"
                    status = 1
                else:
                    message = "Something Went Wrong"
                    status = 0

                return Response(
                    {
                        'status': status,
                        'message': message,
                        'data': resp_data
                    }
                )

        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


@method_decorator(csrf_exempt, name='dispatch')
class CallbackRechargeRazorpay(APIView):
    def post(self, request, *args, **kwargs):
        # def callback_order_razorpay(request):
        # redirect_url = 'http://15.207.21.5:3000/account/process_payment?order_id='
        redirect_url = website_base_url + "account/process_payment?order_id="
        # print("test data ---- ", request.data)
        data = request.data
        razorpay_payment_id = data["razorpay_payment_id"]
        razorpay_order_id = data["razorpay_order_id"]
        razorpay_signature = data["razorpay_signature"]
        payment = Payments.objects.filter(pg_transaction_id=razorpay_order_id)
        if payment:
            client_id = ""
            payment_for = ""
            order_id_s = str(payment[0].order_id)
            if "RC" in order_id_s:
                client_id = order_id_s[2:]
                payment_for = "recharge"
            if "RCRP" in order_id_s:
                client_id = order_id_s[4:]
                payment_for = "repayment"
            # print("client_id", client_id)
            data = {
                "client_id": client_id,
                "payment_for": payment_for,
                "razorpay_payment_id": razorpay_payment_id,
                "razorpay_order_id": razorpay_order_id,
                "razorpay_signature": razorpay_signature,
                "pay_order_id": payment[0].order_id
            }
            if client_id is not None:
                if payment_for == "recharge":
                    Recharge_transaction_Status_Check_auto(data)
                if payment_for == "repayment":
                    Repayment_transaction_Status_Check_auto(data)
                redirect_with_oid = redirect_url + str(client_id)
                return redirect(redirect_with_oid)

        return redirect(website_base_url + "account/payment")


def Recharge_transaction_Status_Check_auto(data):
    status = None
    message = None
    amount = 0.0
    date_strip = None
    pay_status = None
    # print("data 000000000000000000000000000  ----- ", data)
    client_id = data['client_id']
    razorpay_payment_id = data['razorpay_payment_id']
    razorpay_order_id = data['razorpay_order_id']
    razorpay_signature = data['razorpay_signature']
    # generated_signature = hmac_sha256(razorpay_order_id + "|" + razorpay_payment_id, secret);
    client = razorpay.Client(auth=(Key_id, Key_secret))
    msg = "{}|{}".format(str(data['razorpay_order_id']), str(data['razorpay_payment_id']))
    secret = str(Key_secret)
    key = bytes(secret, 'utf-8')
    body = bytes(msg, 'utf-8')
    dig = hmac.new(key=key,
                   msg=body,
                   digestmod=hashlib.sha256)
    generated_signature = dig.hexdigest()
    result = hmac.compare_digest(generated_signature, razorpay_signature)
    order_text = data["order_id"]
    if result == True:
        # print("##########################333         8888   ----- :", result)
        pass
    order_resp_data = order_status(razorpay_order_id)
    if order_resp_data and order_resp_data is not None:
        status = 1
        message = "Sign Verification Success"
    payment_data = payment_status(razorpay_payment_id)
    if payment_data and payment_data is not None:
        if payment_data['method'] == 'card':
            mode = 'DC'
        elif payment_data['method'] == 'upi':
            mode = 'UPI'
        elif payment_data['method'] == 'netbanking':
            mode = 'NB'
        else:
            mode = 'RWT'
        timestamp = payment_data['created_at']
        try:
            timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
            # timestamp = datetime.datetime.strptime(timestamp, "%d/%m/%Y %H:%M:%S").strftime(
            #     '%Y-%m-%d')
        except Exception as e:
            print(e)
            pass

        if payment_data['status'] == 'captured':
            pay_status = 'success'
            status = '1'
            message = 'Transaction successful.'

        elif payment_data['status'] == 'authorized':
            pay_status = 'pending'
            status = '3'
            message = 'Transaction pending'
        else:
            pay_status = 'failed'
            status = '2'
            message = 'Transaction failed'

        Payments.objects.filter(order_id=data["pay_order_id"],
                                category='Recharge').update(status=pay_status,
                                                            transaction_id=data["razorpay_payment_id"], mode=mode,
                                                            update_date=datetime.now())

        npci_obj = NPCI.objects.filter(client_id=client_id).first()
        if npci_obj:
            user_id = npci_obj.user.id
            if pay_status == "success":
                # TODO Call NPCI recharge API..
                from npci.npci_api import call_npci_recharge_api
                call_recharge = call_npci_recharge_api(npci_obj.id)

                # Giving cashback amount to user..
                if user_id:
                    from npci.views import random_amount
                    cashback_amount = random_amount()
                    if cashback_amount:
                        from EApi.wallet import user_wallet_update
                        update_wallet = user_wallet_update(user_id, cashback_amount, None, None, "recharge_cashback")
                        if update_wallet:
                            npci_obj.wallet_history.add(update_wallet)

            else:
                # Update Npci recharge entry..
                npci_obj = NPCI.objects.filter(id=npci_obj.id).update(status=pay_status, updated_date=datetime.now())

        return Response(
            {
                'status': status,
                'message': message
            }
        )


def Repayment_transaction_Status_Check_auto(data):
    status = None
    message = None
    amount = 0.0
    date_strip = None
    pay_status = None
    # print("data 000000000000000000000000000  ----- ", data)
    client_id = data['client_id']
    razorpay_payment_id = data['razorpay_payment_id']
    razorpay_order_id = data['razorpay_order_id']
    razorpay_signature = data['razorpay_signature']
    # generated_signature = hmac_sha256(razorpay_order_id + "|" + razorpay_payment_id, secret);
    client = razorpay.Client(auth=(Key_id, Key_secret))
    msg = "{}|{}".format(str(data['razorpay_order_id']), str(data['razorpay_payment_id']))
    secret = str(Key_secret)
    key = bytes(secret, 'utf-8')
    body = bytes(msg, 'utf-8')
    dig = hmac.new(key=key,
                   msg=body,
                   digestmod=hashlib.sha256)
    generated_signature = dig.hexdigest()
    result = hmac.compare_digest(generated_signature, razorpay_signature)
    order_text = data["order_id"]
    if result == True:
        # print("##########################333         8888   ----- :", result)
        pass
    order_resp_data = order_status(razorpay_order_id)
    if order_resp_data and order_resp_data is not None:
        status = 1
        message = "Sign Verification Success"
    payment_data = payment_status(razorpay_payment_id)
    if payment_data and payment_data is not None:
        if payment_data['method'] == 'card':
            mode = 'DC'
        elif payment_data['method'] == 'upi':
            mode = 'UPI'
        elif payment_data['method'] == 'netbanking':
            mode = 'NB'
        else:
            mode = 'RWT'
        timestamp = payment_data['created_at']
        try:
            timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
            # timestamp = datetime.datetime.strptime(timestamp, "%d/%m/%Y %H:%M:%S").strftime(
            #     '%Y-%m-%d')
        except Exception as e:
            print(e)
            pass

        if payment_data['status'] == 'captured':
            pay_status = 'success'
            status = '1'
            message = 'Transaction successful.'

        elif payment_data['status'] == 'authorized':
            pay_status = 'pending'
            status = '3'
            message = 'Transaction pending'
        else:
            pay_status = 'failed'
            status = '2'
            message = 'Transaction failed'

        Payments.objects.filter(order_id=data["pay_order_id"],
                                category='Repayment').update(status=pay_status,
                                                             transaction_id=data["razorpay_payment_id"], mode=mode,
                                                             update_date=datetime.now())

        user_id = None
        npci_obj = NPCI.objects.filter(client_id=client_id).first()
        if npci_obj:
            user_id = npci_obj.user.id
        if pay_status == "success":
            if user_id:
                # Create success entry in credit..
                from EApi.credit import credit_balance_update
                credit_status = "repayment"
                credit_add = credit_balance_update(user_id, amount, credit_status)
                if credit_add:
                    npci_obj.credit_history.add(credit_add)
                    from npci.views import update_recharge_status
                    update_recharge = update_recharge_status(npci_obj.id)

        return Response(
            {
                'status': status,
                'message': message
            }
        )
