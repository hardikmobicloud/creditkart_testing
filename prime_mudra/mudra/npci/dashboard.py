from django.shortcuts import render
from django.shortcuts import HttpResponse, redirect
# Create your views here..
from datetime import date, datetime, timedelta
from django.db.models import Sum
import json
from django.apps.registry import apps
import pandas as pd
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt

NPCI = apps.get_model('npci', 'NPCI')
SchemeAmount = apps.get_model('npci', 'SchemeAmount')


def category_description(request, type, category):
    ctx = {}
    if type and category:
        ctx["type"] = type
        ctx["category"] = category
    return render(request, "Ecom/dashboard/npci_admin_dashboard.html/", ctx)


@csrf_exempt
@login_required(login_url="/ecom/login/")
def category_description_data(request):
    ctx = {}
    mobile_recharge = None
    dth_recharge = None
    postpaid_mobile = None
    electric_bill = None
    landline_bill = None
    gas_bill = None
    water_bill = None
    broadband_bill = None
    insurance_bill = None
    if "type" in request.POST:
        type = request.POST["type"]
        category = request.POST["category"]
        if type and category:
            if category == "npci_receive":
                payment_mode = "cash"
            elif category == "credit":
                payment_mode = "credit"
            elif category == "wallet":
                payment_mode = "wallet"
            else:
                payment_mode = None
            if payment_mode and payment_mode is not None:
                if type == "count":
                    npci_list = NPCI.objects.filter(status="success", payment_mode=payment_mode).values_list(
                        "service_category")
                    df = pd.DataFrame(npci_list, columns=["service_category"])
                    data_df = df.groupby(df['service_category'])['service_category'].count().sort_index(axis=0)
                elif type == "amount":
                    npci_list = NPCI.objects.filter(status="success", payment_mode=payment_mode).values_list(
                        "service_category", "amount")
                    df = pd.DataFrame(npci_list, columns=["service_category", "amount"])
                    data_df = df.groupby(df['service_category'])['amount'].sum().sort_index(axis=0)
                else:
                    pass
            else:
                if type == "count":
                    npci_list = NPCI.objects.filter(status="success").values_list("service_category")
                    df = pd.DataFrame(npci_list, columns=["service_category"])
                    data_df = df.groupby(df['service_category'])['service_category'].count().sort_index(axis=0)
                elif type == "amount":
                    npci_list = NPCI.objects.filter(status="success").values_list("service_category", "amount")
                    df = pd.DataFrame(npci_list, columns=["service_category", "amount"])
                    data_df = df.groupby(df['service_category'])['amount'].sum().sort_index(axis=0)
                else:
                    pass

            # npci_list = NPCI.objects.filter(status="success").values_list("service_category", "amount")
            # df = pd.DataFrame(npci_list, columns=["service_category", "amount"])
            # data_df = df.groupby(df['service_category'])['amount'].sum().sort_index(axis=0)
            result = data_df.to_json(orient="split")
            parsed = json.loads(result)
            status = parsed["index"]
            counts = parsed["data"]
            count_list = []
            status_list = ['mobile_recharge', 'dth_recharge', 'postpaid_mobile', 'electric_bill', 'landline_bill',
                           'gas_bill',
                           'water_bill',
                           'broadband_bill', 'insurance_bill']
            for i in status_list:
                if i in status:
                    index = status.index(i)
                    count_list.append(counts[index])
                else:
                    count_list.append(0)

            mobile_recharge = count_list[0]
            dth_recharge = count_list[1]
            postpaid_mobile = count_list[2]
            electric_bill = count_list[3]
            landline_bill = count_list[4]
            gas_bill = count_list[5]
            water_bill = count_list[6]
            broadband_bill = count_list[7]
            insurance_bill = count_list[8]

            ctx["mobile_recharge"] = mobile_recharge
            ctx["dth_recharge"] = dth_recharge
            ctx["postpaid_mobile"] = postpaid_mobile
            ctx["electric_bill"] = electric_bill
            ctx["landline_bill"] = landline_bill
            ctx["gas_bill"] = gas_bill
            ctx["water_bill"] = water_bill
            ctx["broadband_bill"] = broadband_bill
            ctx["insurance_bill"] = insurance_bill
    return HttpResponse(json.dumps(ctx))


@login_required(login_url="/ecom/login/")
def dashboard_amount_count_data(request):
    ctx = {}
    cash_sum = None
    cash_count = None
    credit_sum = None
    credit_count = None
    wallet_sum = None
    wallet_count = None
    total_sum = None
    total_count = None

    npci_list = NPCI.objects.filter(status="success").values_list("amount", "payment_mode")
    if npci_list:
        df = pd.DataFrame(npci_list, columns=["amount", "payment_mode"])
        data_df = df.groupby(df['payment_mode'])['amount'].sum().sort_index(axis=0)
        count_df = df.groupby(df['payment_mode'])['payment_mode'].count().sort_index(axis=0)
        result = data_df.to_json(orient="split")
        count_result = count_df.to_json(orient="split")
        parsed = json.loads(result)
        count_parsed = json.loads(count_result)
        status = parsed["index"]
        counts = parsed["data"]

        count_counts = count_parsed["data"]

        sum_list = []
        count_list = []
        status_list = ['cash', 'credit', 'wallet']
        for i in status_list:
            if i in status:
                index = status.index(i)
                sum_list.append(counts[index])

                count_list.append(count_counts[index])
            else:
                sum_list.append(0)

                count_list.append(0)

        cash_sum = sum_list[0]
        cash_count = count_list[0]
        credit_sum = sum_list[1]
        credit_count = count_list[1]
        wallet_sum = sum_list[2]
        wallet_count = count_list[2]

        total_sum = cash_sum + credit_sum + wallet_sum
        total_count = cash_count + credit_count + wallet_count

    ctx.update(
        {
            "cash_sum": cash_sum,
            "cash_count": cash_count,
            "credit_sum": credit_sum,
            "credit_count": credit_count,
            "wallet_sum": wallet_sum,
            "wallet_count": wallet_count,
            "total_sum": total_sum,
            "total_count": total_count
        }
    )
    return HttpResponse(json.dumps(ctx))


@login_required(login_url="/ecom/login/")
def dashboard_data_1(request):
    ctx = {}
    repayment = 0
    npci_obj = NPCI.objects.filter(status="success", payment_mode="credit")
    if npci_obj:
        for npci in npci_obj:
            repayment_added = npci.credit_history.filter(status="repayment")
            if repayment_added:
                for repayment_1 in repayment_added:
                    repayment += repayment_1.repayment_add
    ctx["repayment_amount"] = repayment
    return HttpResponse(json.dumps(ctx))
