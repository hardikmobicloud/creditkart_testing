from django.urls import path
from .dashboard import *
from .npci_api import ReturnRechargeBillPaymentToken, RechargePlaceApi, RechargeBillPaymentModeSelectionApi, \
    RechargeCalculationsApi, RechargeHistoryAPI, RepaymentHistoryAPI, NPCIWebHook, ParticularRechargeRepaymentAPI, \
    RechargePlansApi, fetch_status_by_recharge_id, fetch_status_by_client_id

from .npci_razorpay import NpciRechargeChecksumGenerateAPIWebsite, CallbackRechargeRazorpay, \
    NpciRepaymentChecksumGenerateAPIWebsite

from .npci_paytm import ChecksumGenerateNpciRechargeWebsite, ChecksumGenerateNpciRepaymentWebsite, \
    callback_recharge_repayment_response, callback_recharge_response, RechargeTransactionStatusCheckApplication, \
    RechargeRepaymentTransactionStatusCheckApplication, ChecksumGenerateNpciRecharge, ChecksumGenerateNpciRepayment

from .views import datewise_dashboard_report, monthwise_dashboard_report, serach_recharge, search_recharge_data, \
    recharge_transaction, npci_defaulters, npci_graph, npci_graph_data, search_payment, search_payment_data, \
    monthwise_repayment, datewise_repayment

urlpatterns = [

    # path("get_token/". get_token)
    path("get_token/", ReturnRechargeBillPaymentToken.as_view()),

    # recharge API's
    # select mode API (cash / credit)
    path("recharge_bill_mode_selection/", RechargeBillPaymentModeSelectionApi.as_view()),
    path("recharge_calculation_api/", RechargeCalculationsApi.as_view()),
    path("recharge_place_api/", RechargePlaceApi.as_view()),
    path("npci_web_hook/", NPCIWebHook.as_view()),
    path("recharge_plans/", RechargePlansApi.as_view()),

    # status fetch

    path("fetch_status_by_recharge_id/<recharge_id>/", fetch_status_by_recharge_id),
    path("fetch_status_by_client_id/<client_id>/", fetch_status_by_client_id),

    # recharge razorpay API's
    path("recharge_checksum_generate_website/", NpciRechargeChecksumGenerateAPIWebsite.as_view()),
    path("recharge_repayment_checksum_generate_website/", NpciRepaymentChecksumGenerateAPIWebsite.as_view()),
    path("callback_recharge_status_check_api/", CallbackRechargeRazorpay.as_view()),

    path("recharge_history_api/", RechargeHistoryAPI.as_view()),
    path("repayment_history_api/", RepaymentHistoryAPI.as_view()),
    path("particular_recharge_repayment/", ParticularRechargeRepaymentAPI.as_view()),

    # Paytm urls..
    path('checksum_generate_npci_recharge', ChecksumGenerateNpciRecharge.as_view()),
    path('checksum_generate_npci_recharge_web/', ChecksumGenerateNpciRechargeWebsite.as_view()),
    path('checksum_generate_npci_repayment', ChecksumGenerateNpciRepayment.as_view()),
    path('checksum_generate_npci_repayment_web/', ChecksumGenerateNpciRepaymentWebsite.as_view()),

    path('recharge_trancation_status_check_application/', RechargeTransactionStatusCheckApplication.as_view()),
    path('recharge_repayment_trancation_status_check_application/',
         RechargeRepaymentTransactionStatusCheckApplication.as_view()),

    path('callback_recharge_url/<order_id>', callback_recharge_response),
    path('callback_recharge_repayment_url/<order_id>', callback_recharge_repayment_response),

    # NPCI reports
    path("datewise_dashboard_report/<service_category>/<month>/<category>/<type>/", datewise_dashboard_report),
    path("recharge_transaction/<date>/<service_category>/<category>/", recharge_transaction),
    path("monthwise_dashboard_report/<service_category>/<category>/<type>/", monthwise_dashboard_report),
    path("npci_defaulters/", npci_defaulters),

    # NPCI search
    path("serach_recharge/", serach_recharge),
    path("search_recharge_data/", search_recharge_data),
    path("search_payment/", search_payment),
    path("search_payment_data/", search_payment_data),

    # NPCI dashboard url
    path("category_description_data/", category_description_data),
    path("category_description/<type>/<category>/", category_description),
    path("dashboard_amount_count_data/", dashboard_amount_count_data),
    path("dashboard_amount_count_data/", dashboard_amount_count_data),
    path("dashboard_data_1/", dashboard_data_1),

    # NPCI graphs
    path("npci_graph/<type>/", npci_graph),
    path("npci_graph_data/", npci_graph_data),

    # repayment reports
    path("monthwise_repayment/", monthwise_repayment),
    path("datewise_repayment/<month>/", datewise_repayment),

]
