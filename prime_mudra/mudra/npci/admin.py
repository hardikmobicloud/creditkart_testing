from django.contrib import admin
from .models import *


# Register your models here.


class NPCIAdmin(admin.ModelAdmin):
    list_display = ('number', 'operatorcode', 'account', 'amount', 'date', 'bill_verify_id', 'client_id',
                    'recharge_id', 'payment_mode', 'service_category', 'credit', 'wallet', 'created_date',
                    'updated_date', 'status')
    list_filter = ('payment_mode', 'service_category', 'status')
    search_fields = ['number', 'recharge_id', 'client_id', 'user__username']


admin.site.register(NPCI, NPCIAdmin)


class SchemeAmountAdmin(admin.ModelAdmin):
    list_display = ('amount', 'amount_count', 'count', 'status', 're_status', 'date', 'created_date',
                    'updated_date')
    list_filter = ('status', 're_status')
    search_fields = ['count', 'amount']


admin.site.register(SchemeAmount, SchemeAmountAdmin)
