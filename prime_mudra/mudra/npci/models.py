from ecom.models import Credit, UserWallet
from django.contrib.auth.models import User
from mudra import constants, ecom_constants
from Payments.models import Payment
from django.db import models


class NPCI(models.Model):
    """
        number = Mobile Number/ DTH Number to Recharge
        operatorcode = Operator Code for Services (See List of Operator Code)
        amount = Amount for transaction to recharge
        account = (Used for Electric, Landline, GAS, Insurance,Water, Broadband)
        auth = (Used for Electric, Landline, GAS, Insurance,Water, Broadband)
        bill_verify_id = (Used for Electric, Landline, GAS, Insurance,Water, Broadband)
        recharge_id = unique id from npci
        client_id  = Your Unique Transaction ID (Only Number Accept)
        type = cash or credit
        service_category = service for which payment is to made
        service_type = bill or recharge
        status = payment current status
    """
    number = models.CharField(max_length=100, null=True, blank=True)
    operatorcode = models.CharField(max_length=100, null=True, blank=True)
    account = models.CharField(max_length=100, null=True, blank=True)
    auth = models.CharField(max_length=80, null=True, blank=True)
    amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    date = models.DateTimeField(null=True, blank=True, db_index=True)
    bill_verify_id = models.CharField(max_length=100, null=True, blank=True)
    client_id = models.CharField(max_length=100, null=True, blank=True)
    recharge_id = models.CharField(max_length=100, null=True, blank=True)
    payment_mode = models.CharField(max_length=100, null=True, blank=True, choices=constants.NPCI_TYPE)
    service_category = models.CharField(max_length=80, null=True, blank=True,
                                        choices=constants.NPCI_SERVICE_CATEGORY)
    service_type = models.CharField(max_length=50, null=True, blank=True, choices=constants.NPCI_SERVICE_TYPE)
    status = models.CharField(max_length=80, null=True, blank=True, choices=constants.STATUS_TYPE)

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user", null=True, blank=True)
    credit = models.ForeignKey(Credit, on_delete=models.CASCADE, related_name="npci_user_credit", null=True, blank=True)
    credit_status = models.CharField(max_length=25, null=True, blank=True, choices=ecom_constants.CREDIT_STATUS,
                                     db_index=True)
    wallet = models.ForeignKey(UserWallet, on_delete=models.CASCADE, related_name="npci_user_wallet", null=True,
                               blank=True)
    wallet_history = models.ManyToManyField(UserWallet, related_name="npci_wallet_history", null=True, blank=True)
    credit_history = models.ManyToManyField(Credit, related_name="npci_credit_history", null=True, blank=True)
    payment = models.ManyToManyField(Payment, related_name="user_payment", null=True,
                                     blank=True)
    response = models.TextField(null=True, blank=True)
    re_status = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE,
                                   related_name="npci_created_by")
    updated_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE,
                                   related_name="npci_updated_by")

    def __str__(self):
        return '%s %s %s' % (self.number, self.status, self.service_category)


class SchemeAmount(models.Model):
    """
    This model is to store random amounts for cashback..
    """
    amount = models.FloatField(null=True, blank=True)
    amount_count = models.FloatField(null=True, blank=True)
    count = models.FloatField(null=True, blank=True)
    status = models.BooleanField(default=True, blank=True, null=True)
    re_status = models.BooleanField(default=True, blank=True, null=True)
    date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
