import random
import os
import sys
from datetime import datetime, timedelta
import requests
import json
import time
from django.shortcuts import HttpResponse
from django.db.models import Sum
from django.contrib.auth.models import User
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics
from django.apps.registry import apps

NPCI = apps.get_model('npci', 'NPCI')
UserWallet = apps.get_model('ecom', 'UserWallet')
Payment = apps.get_model('Payments', 'Payment')
Credit = apps.get_model('ecom', 'Credit')

# Staging credentials..

# username = "APIMA3062920"
# password = "595676"
# secret_key = "2582-5fd74187ad77e-526267"
# base_url = "http://staging.apiscript.in/"

# Production credentials..
username = "APIMA9542250"
password = "624711"
secret_key = "9162-6002c8e6c4cfe-293640"
email_id = "swapnil.madiyar@mudrakwik.com"

# base_url = "http://staging.apiscript.in/"
base_url = "https://services.apiscript.in/"


def get_token():
    """
    This function returns Token..
    """
    token = None
    url = base_url + "jwt_encode"
    payload = {
        'secret_key': secret_key,
        'email_id': email_id,
        'TimeStamp': int(time.time())
    }
    token_data_dict = requests.post(url, data=payload)
    if token_data_dict:
        token_data = json.loads(token_data_dict.text)
        if "error_code" in token_data:
            # print("token_data", token_data)
            error_code = token_data["error_code"]
            if error_code == '0':
                token = token_data["encode_token"]
    # print("token", token)
    return token


class ReturnRechargeBillPaymentToken(APIView):
    """
    To generate payload
    url : /npci/get_token/
    Note:
        Token required
        Authorization : Token b4683a8182d85dfb5106182c381aceb36d836f2a

    :parameter:
        Method: GET
        Response
        {
            "status": 1,
            "username": "APIMA3062920",
            "pwd": "595676",
            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJUaW1lU3RhbXAiOjE2MDk1NzE0ODAsIkVtYWlsSUQiOiJzd2FwbmlsLm1hZGl5YXJAbXVkcmFrd2lrLmNvbSJ9.ViTXTFt1umqVMuns4Ys8JZ1VR5JSIjIFYedoxmCC6qA"
        }

    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def get(self, request):
        payload = {
            "status": 1,
            'username': username,
            'pwd': password,
            "token": get_token()
        }
        return Response(payload)


# get_token()

def get_status():
    url = base_url + "recharge/status"
    # url = "http://staging.apiscript.in/recharge/status"
    payload = {
        'username': username,
        'pwd': password,
        'recharge_id': 2,
        "token": get_token()
    }
    # print("====", payload)
    token_data_dict = requests.post(url, data=payload)
    # print("response", token_data_dict.text)


# get_status()


def check_bill(payload):
    response = None
    url = base_url + "recharge/check_bill"
    # url = "http://staging.apiscript.in/recharge/check_bill"
    # payload = {
    #     'username': username,
    #     'pwd': password,
    #     'operatorcode': 2,
    #     'number': 2,
    #     'account': 2,
    #     'amount': 2,
    #     "token": get_token()
    # }
    # print("====", payload)
    token_data_dict = requests.post(url, data=payload)
    if token_data_dict:
        response = token_data_dict.json()
    # print("response", token_data_dict.json())

    return response


def plan():
    url = "http://staging.apiscript.in/recharge/plan"
    payload = {
        'username': username,
        'pwd': password,
        'operator': "Airtel",
        "token": get_token()
    }
    # print("====", payload)
    token_data_dict = requests.post(url, data=payload)
    # print("response", token_data_dict.text)


# plan()


def operator():
    url = "http://staging.apiscript.in/recharge/operator"
    payload = {
        'username': username,
        'pwd': password,
        "token": get_token()
    }
    # print("====", payload)
    token_data_dict = requests.post(url, data=payload)
    # print("response", token_data_dict.text)


# operator()


def test_transaction_api():
    url = "http://staging.apiscript.in/recharge/status"
    payload = {
        'username': username,
        'pwd': password,
        "token": get_token(),
        "recharge_id": "2"
    }
    # print("====", payload)
    token_data_dict = requests.post(url, data=payload)
    # print("response", token_data_dict.json())


# transaction_api()

def get_recharge_bill_transaction_status(recharge_id):
    """
        get transaction status of recharge / bill

        :param
        recharge_id

        :returns
        json response of transaction status API

    """
    url = base_url + "recharge/status"
    # url = "http://staging.apiscript.in/recharge/status"
    payload = {
        'username': username,
        'pwd': password,
        "token": get_token(),
        "recharge_id": recharge_id
    }
    # print("===get_recharge_bill_transaction_status=", payload)
    token_data_dict = requests.post(url, data=payload)
    # print("get_recharge_bill_transaction_status  response", token_data_dict.json())
    return token_data_dict.json()


def npci_recharge_api(payload):
    """
    calls npci recharge api
    :param
        recharge / bill payload
    :return
        json response of response recharge / bill API
    """
    # url = "http://staging.apiscript.in/recharge/api"
    url = base_url + "recharge/api"
    # print("payload====", payload)
    recharge_response = requests.post(url, data=payload)
    recharge_response = recharge_response.json()
    return recharge_response


def validate_request_data(data):
    """
    validate request data from user for recharge API
    :returns
        valid data:
            dictionary with status 1
        invalid data:
            status : 3 / 4

    param:
        api_type: bill / recharge

    """
    data_dict = {}
    status = 4
    message = "Missing mandatory parameters"

    if "api_type" in data:
        # api_status should not be empty
        if data["api_type"] is not None and data["api_type"] != "":
            status = "success"
            api_type = data["api_type"]
            # if request is for recharge
            if api_type == "recharge":
                # for recharge
                request_parameters_list = ["operatorcode", "number", "amount"]
            else:
                # for bill
                request_parameters_list = ["operatorcode", "number", "amount", "account", "auth"]

            for i in request_parameters_list:
                if i in data:
                    if i == "amount":
                        recharge_amount = data[i]
                        if type(recharge_amount) == type(""):
                            if "'" in recharge_amount or '"' in recharge_amount:
                                recharge_amount = recharge_amount[1:-1]
                                recharge_amount = float(recharge_amount)
                            else:
                                recharge_amount = float(recharge_amount)
                        elif type(recharge_amount) == type(1.1):
                            recharge_amount = recharge_amount
                        else:
                            recharge_amount = float(recharge_amount)
                        data_dict.update(
                            {
                                i: recharge_amount
                            }
                        )
                    else:
                        data_dict.update(
                            {
                                i: data[i]
                            }
                        )

                else:
                    status = 3
                    message = "Missing mandatory parameter " + i
                    error_dict = {
                        "status": status,
                        "message": message
                    }
                    return error_dict

            status = 1
            data_dict['status'] = status
            return data_dict

    error_dict = {
        "status": status,
        "message": message
    }
    return error_dict


def generate_unique_id_microsecond():
    """
        generates unique id with current year, month, day, hour, minute, second, microsecond
        + random number
        len of the unique id: 21-23
    """
    date = datetime.now()
    random_number = random.sample(range(000, 99999), 1)
    unique_id = str(date.year)[2:] + "" + str(date.month) + "" + str(date.day) + "" + str(
        date.hour) + "" + str(date.minute) + "" + str(date.second) + "" + "" + str(random_number[0])

    return unique_id


def generate_unique_id_npci(npci_type):
    """
        generated alpha numeric unique id for payment's order id
    :param
    npci_type : recharge
    :returns
        RC21141518123154776912158
    """
    unique_id = generate_unique_id_microsecond()
    prefix = ''
    if npci_type == "recharge":
        prefix = "RC"
    unique_id = prefix + str(unique_id)
    return unique_id


def create_payment_entry(data, status):
    """
    This function creates an entry in Payment table for transaction

    parameters :
        data = dictionary of validate data
        status = "initiated
    return :
        payment object

    # Note: the numeric part of order_id will be used in recharge api as client_id

        example:
        Payment: order_id = RC21141518123154776912158
        npci: client_id = 21141518123154776912158
    """
    # generating unique order id for transaction
    # Note: the numeric part of order_id is will be used in recharge api as client_id. check doc above
    npci_type = "recharge"
    order_id = generate_unique_id_npci(npci_type)
    try:
        payment = Payment.objects.create(status=status, order_id=data['order_id'], amount=data["amount"],
                                         category='Recharge', product_type="Npci", type=data["payment_type"],
                                         date=datetime.now()
                                         )
        return payment

    except Exception as e:
        print('-----------in exception create_payment_entry----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        return False


def create_recharge_bill_payment_entry(data, user, credit, wallet, payment, credit_status):
    """
        This function creates an entry in NPCI table

        parameters :
            data = dictionary of validate data
            user = user id
            credit = credit object
            wallet = wallet object
            payment = payment object
            status = status

        return :
            NPCI object

    """
    npci_obj = None
    try:
        npci_obj = NPCI.objects.create(**data, user=user, credit=credit, wallet=wallet, credit_status=credit_status)
        if npci_obj and npci_obj is not None:
            if credit and credit is not None:
                npci_obj.credit_history.add(credit)

            if wallet and wallet is not None:
                npci_obj.wallet_history.add(wallet)

            if payment and payment is not None:
                npci_obj.payment.add(payment)
            return npci_obj

    except Exception as e:
        print('-----------in exception create_payment_entry----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        return npci_obj


def create_recharge_bill_payment_data(data, user):
    """
    this function will create status entry in NPCI

    parameters :
        data = dictionary of validate data
        user = user id
        status = status

    return :
        operation status (success / fail)

    """
    npci_obj = None
    credit_obj = None
    wallet_obj = None
    payment = None
    wallet_amount = 0
    last_balance_credit = 0
    amount = float(data["amount"])
    payment_mode = data["payment_mode"]

    npci_credit_status = "Failed"

    # generating unique order id for transaction
    # Note: the numeric part of order_id is will be used in recharge api as client_id. check doc above
    npci_type = "recharge"
    order_id = generate_unique_id_npci(npci_type)

    if payment_mode == "credit":
        # validating for sufficient balance
        from ecom.ecom_payments import check_user_defaulter_status
        default_status = check_user_defaulter_status(user.id)
        if default_status == "False":
            credit_obj = Credit.objects.filter(user=user.id).exclude(
                status__in=["initiated", "recharge_initiated"]).order_by('id').last()
            if credit_obj:
                # print('credit==========', credit_obj.credit)
                if credit_obj.balance_credit > 0:
                    last_balance_credit = credit_obj.balance_credit
                    if last_balance_credit >= amount:
                        from EApi.credit import credit_balance_update
                        credit_status = "recharge_success"
                        # npci_credit_status = "Initiated"
                        credit_obj = credit_balance_update(user.id, amount, credit_status)
                    else:
                        # insufficient credit amount
                        return npci_obj

    if payment_mode == "wallet":
        # validating for sufficient balance
        from EApi.credit import get_user_wallet_amount
        wallet_dict = get_user_wallet_amount(user.id)
        if wallet_dict:
            wallet_amount = wallet_dict["balance_amount"]

        if wallet_amount >= amount:
            from EApi.wallet import user_wallet_sub
            wallet_status = "recharge_success"
            wallet_obj = user_wallet_sub(user.id, amount, None, None, wallet_status)
            data["payment_type"] = "Ecom_Wallet"
            data["order_id"] = order_id
            payment_status = "success"
            payment = create_payment_entry(data, payment_status)
            data.pop("payment_type")
            data.pop("order_id")
        else:
            # insufficient wallet amount
            return npci_obj

    if payment_mode == "cash":
        data["payment_type"] = "Cash"
        data["order_id"] = order_id
        # payment_status = "initiated"
        # payment = create_payment_entry(data, payment_status)
        data.pop("order_id")
        data.pop("payment_type")

    data["client_id"] = order_id.replace("RC", "")
    npci_obj = create_recharge_bill_payment_entry(data, user, credit_obj, wallet_obj, payment, npci_credit_status)
    if npci_obj and npci_obj is not None:
        return npci_obj

    return npci_obj


def recharge_payment_slabs(amount):
    """
        This function takes order_details_id and gives slabs of installments according
        to amount used for this order..
    """
    slabs_dict = {}
    if amount:
        today_date = datetime.now()
        first_installment_date = today_date + timedelta(days=30)

        dates = first_installment_date
        if amount != 0:
            amount += int(amount) * 0.025
            # amount = round(amount)
            slabs_dict = {dates: amount}
            return slabs_dict
        else:
            return slabs_dict


class RechargeBillPaymentModeSelectionApi(APIView):
    """
    This APi will return checkout calculation data for credit and cash mode

    In Cash mode there is cashback given to user in user wallet
    In Credit mode user will have to pay amount + 2.5% of the amount after the tenure (30 days)

    Note:
        Token required
        Authorization : Token b4683a8182d85dfb5106182c381aceb36d836f2a

    parameters :
            number = Mobile Number/ DTH Number to Recharge
            operatorcode = Operator Code for Services (See List of Operator Code)
            amount = Amount for transaction to recharge
            account = (Used for Electric, Landline, GAS, Insurance,Water, Broadband)
            auth = (Used for Electric, Landline, GAS, Insurance,Water, Broadband)
            bill_verify_id = (Used for Electric, Landline, GAS, Insurance,Water, Broadband)
            recharge_id = unique id from npci
            client_id  = Your Unique Transaction ID (Only Number Accept)
            type = cash or credit
            service_category = service for which payment is to made
            service_type = bill or recharge

        method = post
        return :

            success :
                            {
                "status": 1,
                "credit_status": "False",
                "total_amount": 100,
                "pay_now": {
                    "cashback": "",
                    "description": "",
                    "payment_mode": "cash"
                },
                "credit": [
                    {
                        "date": "03/02/2021 02:36:33 PM",
                        "amount": 102.5,
                        "description": ""
                    }
                ]
            }


        status:
        1 : success
        2 : Something went wrong
        4 : User not found

    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        request_data = request.data
        data_dict = {}
        cash_dict = {}
        status = 2
        message = "Something went wrong"
        user_id = request.user.is_anonymous
        if not user_id:
            if "payment_mode_type" not in request.data or \
                    "service_category" not in request.data or \
                    "service_type" not in request.data:
                return Response(
                    {
                        "status": 5,
                        "message": "Missing mandatory parameters"
                    }

                )
            validated_data = validate_request_data(request_data)
            if validated_data:
                if "status" in validated_data:
                    credit_list = []
                    is_defaulter = "False"
                    validated_data["type"] = request_data["payment_mode_type"]
                    validated_data["service_category"] = request_data["service_category"]
                    validated_data["service_type"] = request_data["service_type"]
                    if validated_data['status'] == 1:

                        cash_dict.update(
                            {'cashback': "", 'description': "", "payment_mode": "cash"})

                        from ecom.ecom_payments import check_user_defaulter_status
                        is_defaulter = check_user_defaulter_status(request.user.id)
                        # get repayment date and payment amount (+ 2.5%)
                        credit = recharge_payment_slabs(validated_data["amount"])
                        if credit:
                            for date, amount in credit.items():
                                from MApi.loan import convert_date_into_readable_format
                                date = convert_date_into_readable_format(date)
                                credit_dict = {'date': date, 'amount': round_of(amount), 'description': ""}
                                credit_list.append(credit_dict)

                        return Response(
                            {
                                'status': 1,
                                'credit_status': is_defaulter,
                                'total_amount': validated_data["amount"],
                                'pay_now': cash_dict,
                                'credit': credit_list
                            }
                        )

                    status = validated_data['status']
                    message = validated_data['message']
                # return error response for invalid data
                return Response(
                    {
                        "status": status,
                        "message": message
                    }

                )

        return Response(
            {
                "status": 4,
                "message": "User not found"
            }
        )


def get_recharge_bill_payload(validated_data):
    """
        added credentials and token in data
    :param
        recharge / bill payload
    :returns
        recharge / bill payload with credentials and token
    """
    credentials_dict = {
        'username': username,
        'pwd': password,
        "token": get_token()
    }
    # Given two dictionaries, merge them into a new dict as a shallow copy.
    payload = validated_data.copy()
    payload.update(credentials_dict)
    # print("payload-===============", payload)
    return payload


def process_recharge_response(response):
    """
    :param
        npci json response

    recharge_status : npci response's status
    status :
        success
        unable to update NPCI
        Something went wrong

        if npci response returns error_code 1 then
            status = npci response's message

    :returns
        status, recharge_status

    Error Response:
        {
            "message":"Paramenter is missing.","error_code":"1"
        }

    Success Response:
        {
            "message":"Your recharge request is accepted.","recharge_id":2,"recharge_status":"Pending",
            "amount":"10","number":"7070645987","client_id":"100","operator_code":"100",
            "recharge_datetime":"2018-06-28 10:55:26 PM","error_code":"0"
        }

    PENDING CASE:
        {
            "message":"Your recharge request is accepted.","recharge_id":209171,
            "recharge_status":"Pending","amount":"10.0","number":"8806366904",
            "client_id":"21161215587732823500994","recharge_datetime":"2021-01-06 12:15:58 PM",
            "error_code":"0"
        }
    """
    # print("Inside process_recharge_response")
    status = None
    recharge_status = "Error"
    if response:
        if "error_code" in response:
            if str(response["error_code"]) == "1":
                # error case
                status = response["message"]
                response['recharge_status'] = "Failed"
                # print("process_recharge_response status============", status)

                npci = update_recharge_bill_payment(response)

            elif str(response["error_code"]) == "0":
                # success case
                # Updating Recharge entry..
                npci = update_recharge_bill_payment(response)

                if "recharge_status" in response:
                    recharge_status = response["recharge_status"]

            else:
                status = "Something went wrong"
                # print("Something went wrong", status)

    # print("Outside process_recharge_response", recharge_status)
    return recharge_status


def convert_into_datetime(date):
    """
        converts datetime format from
        2021-01-06 06:04:01 PM
        to
        2021-01-06 06:04:01
        : return converted datetime or False in case of exception
    """
    try:
        converted_date = datetime.strptime(date, '%Y-%m-%d %H:%M:%S %p').strftime(
            '%Y-%m-%d %H:%M:%S')
        return converted_date

    except Exception as e:
        print('-----------in exception  convert_into_datetime----------')
        print(e.args)
        import sys
        import os
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)

    return False


def update_recharge_bill_payment(response):
    """
    This function will update NPCI entry for the same transaction

    parameters :
        response = Recharge API success response

    return :

        NPCI object if updated
        or
        None if not updated

    """
    recharge_id = None
    npci_obj = None
    converted_date = None

    if "recharge_id" in response:
        recharge_id = response["recharge_id"]

    npci_status = "initiated"
    recharge_status = response["recharge_status"]
    if recharge_status == "Pending":
        npci_status = "pending"
    elif recharge_status == "Success":
        npci_status = "success"
    elif recharge_status == "Failed":
        npci_status = "failed"

    datetime_data = None
    if "recharge_datetime" in response:
        datetime_data = response["recharge_datetime"]

    if datetime_data:
        converted_date = convert_into_datetime(response["recharge_datetime"])
        if not converted_date:
            converted_date = None

    npci_obj = NPCI.objects.filter(client_id=response["client_id"]).first()
    if npci_obj:
        credit_status = "Failed"
        if recharge_status in ["Success", "success"]:
            wallet_id = None
            if npci_obj.wallet:
                wallet_id = npci_obj.wallet.id

            if wallet_id:
                wallet_obj = UserWallet.objects.filter(id=wallet_id, status="recharge_initiated")
                if wallet_obj:
                    UserWallet.objects.filter(id=wallet_id).update(status="recharge_success")

            credit_id = None
            if npci_obj.credit:
                credit_status = "Initiated"
                credit_id = npci_obj.credit.id

            if credit_id:
                credit_obj = Credit.objects.filter(id=credit_id, status="recharge_initiated")
                if credit_obj:
                    Credit.objects.filter(id=credit_id).update(status="recharge_success")

        update_npci = NPCI.objects.filter(id=npci_obj.id).update(status=npci_status,
                                                                 credit_status=credit_status,
                                                                 date=converted_date,
                                                                 recharge_id=recharge_id, response=response,
                                                                 updated_date=datetime.now())

        if update_npci:
            # print("updated npci object", update_npci)
            pass
    else:
        return npci_obj


def process_recharge_api(payload, type):
    """
        process recharge api:
            1. get recharge payload with credentials
            2. call's NPCI API
            3. process NPCI API's response
    """
    # print("Inside process_recharge_api")
    recharge_status = "Error"
    if payload and payload is not None:
        # get recharge payload with credentials
        payload = get_recharge_bill_payload(payload)
        if payload and payload is not None:
            # call npci recharge api
            if type == "bill":
                res = check_bill(payload)
                if res:
                    if "is_verify" in res:
                        if res["is_verify"] == "true":
                            payload["bill_verify_id"] = res["verify_ref_id"]

                        else:
                            if "client_id" not in res:
                                res["client_id"] = payload["client_id"]

                            res['recharge_status'] = "Failed"
                            res['error_code'] = '1'
                            process_recharge_response(res)
                            recharge_status = "failed_to_verify"
                            return recharge_status

                    else:
                        if "client_id" not in res:
                            res["client_id"] = payload["client_id"]

                        res['recharge_status'] = "Failed"
                        res['error_code'] = '1'
                        process_recharge_response(res)

            response = npci_recharge_api(payload)
            # print("response========================================", response)
            if response and response is not None:
                if "client_id" not in response:
                    response["client_id"] = payload["client_id"]

                # process recharge api response
                res = process_recharge_response(response)
                if res:
                    recharge_status = res
                else:
                    recharge_status = "Error"

    # print("Outside process_recharge_api", recharge_status)
    return recharge_status


class RechargePlaceApi(APIView):
    """
        this API will create entry in NPCI , Credit , Wallet , Payment table and call for recharge API

        parameters :
            number = Mobile Number/ DTH Number to Recharge
            operatorcode = Operator Code for Services (See List of Operator Code)
            amount = Amount for transaction to recharge
            account = (Used for Electric, Landline, GAS, Insurance,Water, Broadband)
            auth = (Used for Electric, Landline, GAS, Insurance,Water, Broadband)
            bill_verify_id = (Used for Electric, Landline, GAS, Insurance,Water, Broadband)
            recharge_id = unique id from npci
            client_id  = Your Unique Transaction ID (Only Number Accept)
            payment_mode_type = cash or credit
            service_category = service for which payment is to made
            service_type = bill or recharge

        return :
        {
                "status": 1,
                "message": message,
                "data": data_dict
        }

        sample input :

            {
                "api_type" : "recharge",
                "number" : "8806366904",
                "amount" : 100,
                "operatorcode": "1",
                "bill_verify_id" : "1",
                "client_id" : 1,
                "payment_mode_type" : "Cash",
                "service_category" :"mobile recharge",
                "service_type" : "recharge",
                "final_payment_mode":"cash"

            }

        sample output :
            {
                "status": 1,
                "message": "Your Recharge initiated successfully",
                "generate_checksum": true,
                "recharge_id": "RC2115102254640066041886",
                "amount": 100
            }

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        request_data = request.data
        data_dict = {}
        api_status = 3
        message = "Something went wrong"
        user_id = request.user.is_anonymous

        if not user_id:
            if "final_payment_mode" not in request.data or "service_category" not in request.data or "service_type" not in request_data:
                return Response(
                    {
                        "status": 9,
                        "message": "Missing mandatory parameters [final_payment_mode, service_category,service_type]"
                    }
                )
            validated_data = validate_request_data(request_data)
            if validated_data:
                if "status" in validated_data:
                    if validated_data['status'] == 1:
                        status = None
                        user = request.user
                        payment_mode = request_data["final_payment_mode"]
                        validated_data["payment_mode"] = request_data["final_payment_mode"]
                        validated_data["service_category"] = request_data["service_category"]
                        validated_data["service_type"] = request_data["service_type"]

                        # if the payment_mode is wallet / credit then payment will be success
                        # user can only select this 2 options if the recharge amount is >= to  wallet / credit
                        if payment_mode in ["credit", "wallet"]:
                            status = "success"
                            validated_data['status'] = "success"

                        # validated_data['status'] will be the status for NPCI model
                        if payment_mode == "cash":
                            validated_data['status'] = "initiated"
                            status = "initiated"

                        npci = create_recharge_bill_payment_data(validated_data, user)
                        if npci:
                            if payment_mode == "cash":
                                return Response(
                                    {
                                        "status": 1,
                                        "message": "Your Recharge initiated successfully",
                                        "generate_checksum": True,
                                        "client_id": npci.client_id,
                                        "amount": validated_data['amount']
                                    }
                                )

                            if payment_mode in ["credit", "wallet"]:
                                npci_obj = NPCI.objects.filter(client_id=npci.client_id, re_status=True).first()
                                if npci_obj:
                                    call_recharge = call_npci_recharge_api(npci_obj.id)
                                    # print("call_recharge", call_recharge)
                                    if call_recharge and call_recharge is not None:
                                        if call_recharge == "Pending":
                                            message = "Your Recharge is Pending"

                                        elif call_recharge in ["Failure", "Failed", "Error"]:
                                            if call_recharge in ["Failure", "Failed"]:
                                                message = "Your Recharge is Failed"

                                            if call_recharge == "Error":
                                                message = "Your recharge could not Proceed"

                                            # failed response from NPCI side
                                            # This means Payment is success and recharge is failed..
                                            # Return Transaction amount in users wallet..
                                            if npci_obj.user:
                                                payment_mode = npci_obj.payment_mode
                                                if payment_mode == "wallet":
                                                    # payment mode is wallet
                                                    wallet_obj = npci_obj.wallet_history.filter(
                                                        status="recharge_failed")
                                                    # check if amount is already return to user
                                                    if wallet_obj:
                                                        # amount already return to user do nothing
                                                        pass
                                                    else:
                                                        # if amount is not return to user for failed transaction return amount in thw wallet
                                                        from EApi.wallet import user_wallet_update
                                                        return_wallet = user_wallet_update(npci_obj.user.id,
                                                                                           npci_obj.amount, None,
                                                                                           None,
                                                                                           "recharge_failed")
                                                        if return_wallet:
                                                            npci_obj.wallet_history.add(return_wallet)

                                                if payment_mode == "credit":
                                                    credit_obj = npci_obj.credit_history.filter(
                                                        status="recharge_failed")
                                                    # check if amount is already return to user
                                                    if credit_obj:
                                                        # amount already return to user do nothing
                                                        pass
                                                    else:
                                                        # if amount is not return to user for failed transaction return amount in thw credit
                                                        from EApi.credit import credit_balance_update
                                                        return_credit = credit_balance_update(npci_obj.user.id,
                                                                                              npci_obj.amount,
                                                                                              "recharge_failed")

                                                        if return_credit:
                                                            npci_obj.credit_history.add(return_credit)

                                        elif call_recharge == "Success":
                                            # Giving cashback amount to user..
                                            if user_id:
                                                if npci_obj.payment_mode in ["cash", "wallet"]:
                                                    get_cashback_obj = npci_obj.wallet_history.filter(
                                                        status="recharge_cashback")
                                                    if not get_cashback_obj:
                                                        from npci.views import random_amount
                                                        cashback_amount = random_amount()
                                                        if cashback_amount and cashback_amount != 0:
                                                            from EApi.wallet import user_wallet_update
                                                            update_wallet = user_wallet_update(user_id, cashback_amount,
                                                                                               None,
                                                                                               None,
                                                                                               "recharge_cashback")
                                                            if update_wallet:
                                                                npci_obj.wallet_history.add(update_wallet)

                                            message = "Your Recharge Done successfully",
                                        elif call_recharge == "failed_to_verify":
                                            message = "Unable to verify bill"
                                        else:
                                            message = "Your recharge could not Proceed"

                                return Response(
                                    {
                                        "status": 2,
                                        "message": message,
                                        "generate_checksum": False,
                                        "client_id": npci.client_id,
                                        "amount": validated_data['amount']
                                    }
                                )

                        else:
                            # if create_recharge_bill_payment_data returned False
                            return Response(
                                {
                                    "status": 2,
                                    "message": "Your Recharge could not be processed due to insufficient balance"
                                }
                            )
                    else:
                        # return error response for invalid data
                        return Response(
                            validated_data
                        )

            return Response(
                {
                    "status": api_status,
                    "message": message,
                    "data": validated_data
                }
            )

        return Response(
            {
                "status": 4,
                "message": "User not found"
            }
        )


class RechargeCalculationsApi(APIView):
    """
        This API calculates amount with credit and wallet and returns final payment method..

    :param:
        {
            "recharge_amount": "",
            "payment_mode": ""
        }

    status:
        8  : Missing mandatory parameters [recharge_amount, payment_mode]
        9  : Mandatory parameters [recharge_amount, payment_mode] cannot be blank or None
        10 : Something went wrong
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        try:

            user_id = request.user.id
            if "recharge_amount" not in request.data or "payment_mode" not in request.data:
                return Response(
                    {
                        "status": 9,
                        "message": "Missing mandatory parameters [recharge_amount, payment_mode]"
                    }
                )

            recharge_amount = request.data.get('recharge_amount')
            payment_mode = request.data.get('payment_mode')
            if None in (recharge_amount, payment_mode) or "" in (recharge_amount, payment_mode):
                return Response(
                    {
                        "status": 8,
                        "message": "Mandatory parameters [recharge_amount, payment_mode] cannot be blank or None"
                    }
                )

            # converting amount to int
            if type(recharge_amount) == type(""):
                if "'" in recharge_amount or '"' in recharge_amount:
                    recharge_amount = recharge_amount[1:-1]
                    recharge_amount = float(recharge_amount)
                else:
                    recharge_amount = float(recharge_amount)
            elif type(recharge_amount) == type(1.1):
                recharge_amount = recharge_amount
            else:
                recharge_amount = float(recharge_amount)
            recharge_amount = float(recharge_amount)
            message = ""
            used_wallet = 0
            pay_amount = 0
            used_credit = 0
            final_payment_mode = None
            if payment_mode == "cash":
                wallet_amount = 0
                from EApi.credit import get_user_wallet_amount
                wallet_dict = get_user_wallet_amount(user_id)
                if wallet_dict:
                    wallet_amount = wallet_dict["balance_amount"]
                if wallet_amount >= recharge_amount:
                    used_wallet = recharge_amount
                    final_payment_mode = "wallet"
                    message = "you can use your wallet amount"
                else:
                    message = "Your wallet balance is not sufficient.. You have to pay full amount.."
                    pay_amount = recharge_amount
                    final_payment_mode = "cash"

            if payment_mode == "credit":
                from EApi.auth import check_personal_info
                check_address_availability = check_personal_info(user_id, payment_mode)
                if check_address_availability:
                    address_available = "True"
                else:
                    address_available = "False"
                    return Response(
                        {
                            "status": 1,
                            "message": "Address not available"
                        }
                    )

                # Here we get the Credit data..
                last_balance_credit = 0
                extra_amount = round_of((recharge_amount * 2.5) / 100)
                recharge_amount += extra_amount

                from EApi.credit import user_credit_add
                data = user_credit_add(user_id)
                if data:
                    last_balance_credit = data['resp_balance_credit']

                if last_balance_credit >= recharge_amount:
                    from ecom.ecom_payments import check_user_defaulter_status
                    defaulter_status = check_user_defaulter_status(user_id)
                    if defaulter_status == "True":
                        message = "You can not user your credit because of your remaining repayments.."
                        pay_amount = recharge_amount
                        final_payment_mode = "cash"

                    else:
                        used_credit = recharge_amount
                        final_payment_mode = "credit"
                        message = "you can use your credit"

                else:
                    message = "Your credit balance is not sufficient.. You have to pay full amount.."
                    pay_amount = recharge_amount
                    final_payment_mode = "cash"

            return Response(
                {
                    "status": 2,
                    "message": message,
                    "used_wallet": used_wallet,
                    "used_credit": used_credit,
                    "pay_amount": pay_amount,
                    "final_payment_mode": final_payment_mode
                }
            )

        except Exception as e:
            print('-----------in exception---RechargeCalculationsApi-------')
            print("Args: ", e.args)
            import os
            import sys
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print("RechargeCalculationsApi--", exc_type, fname, exc_tb.tb_lineno)

            status = 10
            message = "Something went wrong"
            return Response(
                {
                    "status": status,
                    "message": message
                }
            )


class RechargeHistoryAPI(generics.ListAPIView):
    """
    This API gives all recharge transactions

    api method : get

    url : http://127.0.0.1:8000/npci/recharge_history_api/

    request : None

    token : required

    response :

            success :
                            {
                "status": 1,
                "message": "success",
                "data": [
                    {
                        "id": 159,
                        "client_id": "2111414626536485797627",
                        "number": "8208809184",
                        "amount": 19.0,
                        "status": "failed"
                    },
                    {
                        "id": 158,
                        "client_id": "21114143132118413168537",
                        "number": "8208809184",
                        "amount": 19.0,
                        "status": "pending"
                    },
                    {
                        "id": 155,
                        "client_id": "211121146526021373365973",
                        "number": "8208809184",
                        "amount": 19.0,
                        "status": "success"
                    },
                    {
                        "id": 154,
                        "client_id": "21112114465164022629598",
                        "number": "8208809184",
                        "amount": 19.0,
                        "status": "success"
                    },

                        ]
                    }

                ]
            }

    status :

            1 : "message": "success"
            2 : "message": "No history found"
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):

        response_list = []
        user = User.objects.filter(id=request.user.id)
        if user:
            npci_objects = NPCI.objects.exclude(status="initiated").filter(user=user[0].id, re_status=True).order_by(
                "-id")
            if npci_objects is not None:
                for npci in npci_objects:
                    npci_dict = {}

                    npci_dict = {
                        "id": npci.id,
                        "client_id": npci.client_id,
                        "number": npci.number,
                        "amount": npci.amount,
                        "status": npci.status
                    }

                    response_list.append(npci_dict)

                return Response(
                    {
                        "status": 1,
                        "message": "success",
                        "data": response_list
                    }
                )

            return Response(
                {
                    "status": 2,
                    "message": "No history found",
                    "data": response_list
                }
            )


class RepaymentHistoryAPI(generics.ListAPIView):
    """
    This API gives success recharge transactions of user

    api method : get

    url : http://127.0.0.1:8000/npci/repayment_history_api/

    request : None

    token : required

    response :

            success :
                {
                    "status": 1,
                    "message": "success",
                    "data": [
                        {
                            "npci_id": 155,
                            "client_id": "211121146526021373365973",
                            "recharge_id": null,
                            "number": "8208809184",
                            "status": "success",
                            "recharge_amount": 19.0,
                            "is_payable": true,
                            "recharge_date": "2020-12-12"
                        },
                        {
                            "npci_id": 154,
                            "client_id": "21112114465164022629598",
                            "recharge_id": null,
                            "number": "8208809184",
                            "status": "success",
                            "recharge_amount": 19.0,
                            "is_payable": true,
                            "recharge_date": "2021-01-12"
                        },

                    ]
                }



    status :

            1 : "message": "success"
            2 : "message": "Record not found"

    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):

        response_list = []
        user = User.objects.filter(id=request.user.id)
        if user:
            npci_objects = NPCI.objects.filter(user=user[0].id, payment_mode="credit").exclude(
                credit_status="Failed").order_by("-id")
            if npci_objects is not None:
                for npci in npci_objects:
                    is_payable = False
                    pending_amount = 0
                    amount = 0
                    pay_amount = 0
                    if npci.amount:
                        amount = npci.amount
                        pay_amount = float(amount * 0.025) + amount
                        pay_amount = round_of(pay_amount)

                    repayment_added = \
                        npci.credit_history.filter(status="Repayment", re_status=True).aggregate(
                            Sum("repayment_add"))[
                            "repayment_add__sum"]

                    if repayment_added and repayment_added is not None:
                        if repayment_added < pay_amount:
                            is_payable = True
                    else:
                        repayment_added = 0
                        is_payable = True

                    npci_dict = {
                        "npci_id": npci.id,
                        "client_id": npci.client_id,
                        "recharge_id": npci.recharge_id,
                        "number": npci.number,
                        "status": npci.status,
                        "recharge_amount": amount,
                        "is_payable": is_payable,
                        "recharge_date": str(npci.created_date)[:10],

                    }

                    response_list.append(npci_dict)

                return Response(
                    {
                        "status": 1,
                        "message": "success",
                        "data": response_list
                    }
                )

            return Response(
                {
                    "status": 2,
                    "message": "Record not found",
                    "data": response_list
                }
            )


def get_recharge_message(status):
    recharge_message = "Your recharge could not Proceed"
    if status:
        if status == "Pending":
            recharge_message = "Your Recharge is Pending"
        elif status == "Failed":
            recharge_message = "Your Recharge is Failed"
        elif status == "Success":
            recharge_message = "Your Recharge Done successfully",
        elif status == "Error":
            recharge_message = "Your recharge could not Proceed"
        else:
            recharge_message = "Your recharge could not Proceed"

    return recharge_message


def call_npci_recharge_api(npci_id):
    """
        generates payload and calls process_recharge_api()
    """
    # print("Inside call_npci_recharge_api")
    client_id = None
    bill_verify_id = None
    account = None
    auth = None
    operatorcode = None
    number = None
    amount = None
    final_recharge_status = "Error"
    npci_obj = NPCI.objects.filter(id=npci_id).first()
    if npci_obj and npci_obj is not None:
        client_id = npci_obj.client_id
        operatorcode = npci_obj.operatorcode
        number = npci_obj.number
        amount = npci_obj.amount
        account = npci_obj.account
        bill_verify_id = npci_obj.bill_verify_id

    payload = {
        "client_id": client_id,
        "operatorcode": operatorcode,
        "number": number,
        "amount": amount
    }

    if npci_obj.service_type == "bill":
        payload['bill_verify_id'] = bill_verify_id
        payload['account'] = account

        if npci_obj.auth is not None:
            auth = npci_obj.auth
            payload['auth'] = auth

    response = process_recharge_api(payload, npci_obj.service_type)
    if response:
        final_recharge_status = response

    # print("outside call_npci_recharge_api", final_recharge_status)
    return final_recharge_status


def convert(string):
    list1 = []
    list1[:0] = string
    return list1


from math import ceil


def round_of(n):
    number = n
    result = (number / 1) - (number // 1)
    res = str(result)[2:]
    res = convert(res)
    l = []
    for i in reversed(res):
        l.append(int(i))
    for i in range(len(l)):
        if i != len(l) - 1:
            # l.append(int(i))
            if l[i] >= 5:
                l[i + 1] += 1
            else:
                pass
        else:
            if l[i] >= 5:
                return ceil(n)
            else:
                return round(n)


import urllib.parse


class NPCIWebHook(generics.ListAPIView):
    """
    NPCI webhook for status update
    it will update NPCI status after changes in status of recharge from NPCI side
    it will also return wallet or credit amount in case of "Failed" response
    """

    def post(self, request, *args, **kwargs):
        # print("Inside webhook post==================================")
        res = request.POST["data"]
        if res:
            npci_obj = None
            recharge_status = None
            response = urllib.parse.unquote_plus(res)
            # print("response", response)
            response = json.loads(response)
            # print("json.loads(response)==============", response)
            if response:
                if "recharge_id" in response and "amount" in response and "recharge_status" in response:
                    recharge_id = response["recharge_id"]
                    recharge_status = response["recharge_status"]
                    if None not in (recharge_id, recharge_status):
                        npci_obj = NPCI.objects.filter(recharge_id=recharge_id)

                if not npci_obj:
                    if "client_id" in response and "recharge_status" in response:
                        client_id = response["client_id"]
                        recharge_status = response["recharge_status"]
                        if None not in (client_id, recharge_status):
                            npci_obj = NPCI.objects.filter(client_id=client_id)

            # print("NPCI OBJECT=================", npci_obj)
            if npci_obj:
                user_id = npci_obj[0].user.id
                # NPCI entry found
                if recharge_status in ["Failure", "Failed"]:
                    # failed response from NPCI side
                    # This means Payment is success and recharge is failed..
                    # Return Transaction amount in users wallet..
                    if npci_obj[0].user:
                        payment_mode = npci_obj[0].payment_mode
                        if payment_mode in ["wallet", "cash"]:
                            recharge_amount = npci_obj[0].amount
                            # payment mode is wallet
                            wallet_obj = npci_obj[0].wallet_history.filter(status="recharge_failed")
                            # check if amount is already return to user
                            if wallet_obj:
                                # amount already return to user do nothing
                                pass
                            else:
                                # if amount is not return to user for failed transaction return amount in thw wallet
                                from EApi.wallet import user_wallet_update
                                return_wallet = user_wallet_update(npci_obj[0].user.id, recharge_amount, None,
                                                                   None,
                                                                   "recharge_failed")
                                if return_wallet:
                                    npci_obj[0].wallet_history.add(return_wallet)

                        if payment_mode == "credit":
                            recharge_amount = npci_obj[0].amount
                            credit_obj = npci_obj[0].credit_history.filter(status="recharge_failed")
                            # check if amount is already return to user
                            if credit_obj:
                                # amount already return to user do nothing
                                pass
                            else:
                                # if amount is not return to user for failed transaction return amount in thw credit
                                from EApi.credit import credit_balance_update
                                return_credit = credit_balance_update(npci_obj[0].user.id, recharge_amount,
                                                                      "recharge_failed")

                                if return_credit:
                                    npci_obj[0].credit_history.add(return_credit)

                        # update NPCI entry with "failed" status
                        NPCI.objects.filter(id=npci_obj[0].id).update(status="failed",
                                                                      updated_date=datetime.now())

                elif recharge_status == "Success":
                    # recharge successfull !!!
                    # update NPCI entry with "success" status
                    NPCI.objects.filter(id=npci_obj[0].id).update(status="success",
                                                                  updated_date=datetime.now())

                    # Giving cashback amount to user..
                    if user_id:
                        if npci_obj[0].payment_mode in ["cash", "wallet"]:
                            get_cashback_obj = npci_obj[0].wallet_history.filter(status="recharge_cashback")
                            if not get_cashback_obj:
                                from npci.views import random_amount
                                cashback_amount = random_amount()
                                if cashback_amount and cashback_amount != 0:
                                    from EApi.wallet import user_wallet_update
                                    update_wallet = user_wallet_update(user_id, cashback_amount, None, None,
                                                                       "recharge_cashback")
                                    if update_wallet:
                                        npci_obj[0].wallet_history.add(update_wallet)

                elif recharge_status == "Pending":
                    # recharge pending
                    # update NPCI entry with "Pending" status
                    NPCI.objects.filter(id=npci_obj[0].id).update(status="pending",
                                                                  updated_date=datetime.now())

                else:
                    pass

        return HttpResponse("Okay")


class ParticularRechargeRepaymentAPI(generics.ListAPIView):
    """
    This API return repayment related data of particular recharge

    api method : POST

    url : http://127.0.0.1:8000/npci/particular_recharge_repayment/

    request :

            {
               "npci_id" : "122"    (Primary key)
            }

    response :

            success :
                                {
                "status": 1,
                "message": "success",
                "wallet_amount": 1859.0,
                "data": [
                    {
                        "client_id": "21191046138561942919103",
                        "recharge_id": "209223",
                        "number": "212603258",
                        "status": "pending",
                        "recharge_amount": 20.0,
                        "pay_amount": 21,
                        "paid_amount": 0,
                        "pending_amount": 21,
                        "is_payable": true,
                        "recharge_date": "2021-01-09",
                        "due_date": "2021-02-08"
                    }
                ]
            }

    status :

            1 : "message" : "success"
            2 : "message": "No history found",
            3 : "message": "NPCI ID is empty"
            4 : "message": "Missing Mandatory Parameters 'NPCI ID '"

        """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        response_list = []
        user = User.objects.filter(id=request.user.id)
        if user:
            wallet_amount = 0
            from EApi.credit import get_user_wallet_amount
            wallet_dict = get_user_wallet_amount(request.user.id)
            if wallet_dict:
                wallet_amount = wallet_dict["balance_amount"]

            if "npci_id" in request.data:
                npci_id = request.data["npci_id"]
                if npci_id and npci_id is not None:
                    npci_objects = NPCI.objects.filter(user=user[0].id, id=npci_id)
                    if npci_objects and npci_objects is not None:
                        for npci in npci_objects:
                            is_payable = False
                            pending_amount = 0
                            amount = 0
                            pay_amount = 0
                            due_date = None
                            if npci.amount:
                                amount = npci.amount
                                pay_amount = amount + (amount * 0.025)
                                pay_amount = round_of(pay_amount)
                                due_date = npci.created_date + timedelta(days=30)

                            repayment_added = \
                                npci.credit_history.filter(status="Repayment", re_status=True).aggregate(
                                    Sum("repayment_add"))[
                                    "repayment_add__sum"]

                            if repayment_added and repayment_added is not None:
                                if repayment_added < pay_amount:
                                    is_payable = True
                            else:
                                repayment_added = 0
                                is_payable = True

                            pending_amount = pay_amount - repayment_added

                            npci_dict = {
                                "client_id": npci.client_id,
                                "recharge_id": npci.recharge_id,
                                "number": npci.number,
                                "status": npci.status,
                                "recharge_amount": amount,
                                "pay_amount": pay_amount,
                                "paid_amount": repayment_added,
                                "pending_amount": pending_amount,
                                "is_payable": is_payable,
                                "recharge_date": str(npci.created_date)[:10],
                                "due_date": str(due_date)[:10],

                            }

                            response_list.append(npci_dict)

                        return Response(
                            {
                                "status": 1,
                                "message": "success",
                                "wallet_amount": wallet_amount,
                                "data": response_list
                            }
                        )

                    return Response(
                        {
                            "status": 2,
                            "message": "No history found",
                            "data": response_list
                        }
                    )

                return Response(
                    {
                        "status": 3,
                        "message": "NPCI ID is empty",
                    }
                )

            return Response(
                {
                    "status": 4,
                    "message": "Missing Mandatory Parameters 'NPCI ID '",
                }
            )


class RechargePlansApi(generics.ListAPIView):
    def post(self, request, *args, **kwargs):
        payload = {}
        if "operator_name" in request.data:
            operator_name = request.data["operator_name"]
            if operator_name and operator_name is not None:
                url = base_url + "recharge/plan"
                payload = {
                    'operator': operator_name,
                    'username': username,
                    'pwd': password,
                    "token": get_token()
                }
                # print("payload", payload)
                plans_dict = requests.post(url, data=payload)
                if plans_dict:
                    plans_dict = plans_dict.json()

                error_code = None
                if "error_code" in plans_dict:
                    error_code = plans_dict["error_code"]

                if error_code == "0":
                    if "Plan is found for" in plans_dict["message"]:
                        return Response(
                            {
                                "status": 1,
                                "message": "success",
                                "plans": plans_dict
                            }
                        )

                    else:
                        return Response(
                            {
                                "status": 2,
                                "message": plans_dict["message"]
                            }
                        )

                else:
                    return Response(
                        {
                            "status": 3,
                            "message": plans_dict["message"]
                        }
                    )

            return Response(
                {
                    "status": 4,
                    "message": "please provide operator name"
                }
            )
        return Response(
            {
                "status": 5,
                "message": "missing mandatory parameters"
            }
        )


def fetch_status_by_client_id(request, client_id):
    message = None
    if client_id and client_id is not None:
        payload = {
            'client_id': client_id,
            'username': username,
            'pwd': password,
            "token": get_token()
        }
        url = base_url + "recharge/status"

        status_response = requests.post(url, data=payload)
        # print("response", status_response)
        status_response = status_response.json()
        if status_response and status_response is not None:
            if "error_code" in status_response:
                if status_response["error_code"] == "0":
                    if "client_id" in status_response and "recharge_status" in status_response:
                        client_id = status_response["client_id"]
                        recharge_status = status_response["recharge_status"]
                        if None not in (client_id, recharge_status):
                            npci_obj = NPCI.objects.filter(client_id=client_id)
                            if npci_obj:
                                user_id = npci_obj[0].user.id
                                # NPCI entry found
                                if recharge_status in ["Failure", "Failed"]:
                                    # failed response from NPCI side
                                    # This means Payment is success and recharge is failed..
                                    # Return Transaction amount in users wallet..
                                    if npci_obj[0].user:
                                        payment_mode = npci_obj[0].payment_mode
                                        if payment_mode in ["wallet", "cash"]:
                                            recharge_amount = npci_obj[0].amount
                                            # payment mode is wallet
                                            wallet_obj = npci_obj[0].wallet_history.filter(status="recharge_failed")
                                            # check if amount is already return to user
                                            if wallet_obj:
                                                # amount already return to user do nothing
                                                pass
                                            else:
                                                # if amount is not return to user for failed transaction return amount in thw wallet
                                                from EApi.wallet import user_wallet_update
                                                return_wallet = user_wallet_update(npci_obj[0].user.id,
                                                                                   recharge_amount, None,
                                                                                   None,
                                                                                   "recharge_failed")
                                                if return_wallet:
                                                    npci_obj[0].wallet_history.add(return_wallet)

                                        if payment_mode == "credit":
                                            recharge_amount = npci_obj[0].amount
                                            credit_obj = npci_obj[0].credit_history.filter(status="recharge_failed")
                                            # check if amount is already return to user
                                            if credit_obj:
                                                # amount already return to user do nothing
                                                pass
                                            else:
                                                # if amount is not return to user for failed transaction return amount in thw credit
                                                from EApi.credit import credit_balance_update
                                                return_credit = credit_balance_update(npci_obj[0].user.id,
                                                                                      recharge_amount,
                                                                                      "recharge_failed")

                                                if return_credit:
                                                    npci_obj[0].credit_history.add(return_credit)

                                        # update NPCI entry with "failed" status
                                        NPCI.objects.filter(id=npci_obj[0].id).update(status="failed",
                                                                                      updated_date=datetime.now())

                                    message = "recharge failed"

                                elif recharge_status in ["Success", "success"]:
                                    # recharge successfull !!!
                                    # update NPCI entry with "success" status
                                    NPCI.objects.filter(id=npci_obj[0].id).update(status="success",
                                                                                  updated_date=datetime.now())

                                    # Giving cashback amount to user..
                                    if user_id:
                                        if npci_obj[0].payment_mode in ["cash", "wallet"]:
                                            get_cashback_obj = npci_obj[0].wallet_history.filter(
                                                status="recharge_cashback")
                                            if not get_cashback_obj:
                                                from npci.views import random_amount
                                                cashback_amount = None
                                                try:
                                                    cashback_amount = random_amount()
                                                except Exception as e:
                                                    print("Inside exception of random_amount", e)

                                                if cashback_amount and cashback_amount != 0:
                                                    from EApi.wallet import user_wallet_update
                                                    update_wallet = user_wallet_update(user_id, cashback_amount, None,
                                                                                       None,
                                                                                       "recharge_cashback")
                                                    if update_wallet:
                                                        npci_obj[0].wallet_history.add(update_wallet)

                                    message = "recharge success"

                                elif recharge_status == "Pending":
                                    # recharge pending
                                    # update NPCI entry with "Pending" status
                                    NPCI.objects.filter(id=npci_obj[0].id).update(status="pending",
                                                                                  updated_date=datetime.now())

                                    message = "recharge pending"
                                else:
                                    message = recharge_status

                            else:
                                message = "record not found"
                        else:
                            message = "client id in response is none"

                    else:
                        message = "client id not present in  response"

                else:
                    message = "error " + status_response["message"]

    return HttpResponse(message)


def fetch_status_by_recharge_id(request, recharge_id):
    message = None
    if recharge_id and recharge_id is not None:
        payload = {
            'recharge_id': recharge_id,
            'username': username,
            'pwd': password,
            "token": get_token()
        }
        url = base_url + "recharge/status"

        status_response = requests.post(url, data=payload)
        # print("response", status_response)
        status_response = status_response.json()
        # print("status_response", status_response)
        if status_response and status_response is not None:
            if "error_code" in status_response:
                if status_response["error_code"] == "0":
                    if "recharge_id" in status_response and "recharge_status" in status_response:
                        recharge_id = status_response["recharge_id"]
                        recharge_status = status_response["recharge_status"]
                        # print("recharge_id", recharge_id)
                        # print("recharge_status", recharge_status)
                        if None not in (recharge_id, recharge_status):
                            npci_obj = NPCI.objects.filter(recharge_id=recharge_id)
                            if npci_obj:
                                user_id = npci_obj[0].user.id
                                # NPCI entry found
                                if recharge_status in ["Failure", "Failed"]:
                                    # failed response from NPCI side
                                    # This means Payment is success and recharge is failed..
                                    # Return Transaction amount in users wallet..
                                    if npci_obj[0].user:
                                        payment_mode = npci_obj[0].payment_mode
                                        if payment_mode in ["wallet", "cash"]:
                                            recharge_amount = npci_obj[0].amount
                                            # payment mode is wallet
                                            wallet_obj = npci_obj[0].wallet_history.filter(status="recharge_failed")
                                            # check if amount is already return to user
                                            if wallet_obj:
                                                # print("amount already return to user do nothing")
                                                pass
                                            else:
                                                # print("Inside creating wallet entry===========")
                                                # if amount is not return to user for failed transaction return amount in thw wallet
                                                from EApi.wallet import user_wallet_update
                                                return_wallet = user_wallet_update(npci_obj[0].user.id,
                                                                                   recharge_amount, None,
                                                                                   None,
                                                                                   "recharge_failed")
                                                if return_wallet:
                                                    npci_obj[0].wallet_history.add(return_wallet)

                                        if payment_mode == "credit":
                                            recharge_amount = npci_obj[0].amount
                                            credit_obj = npci_obj[0].credit_history.filter(status="recharge_failed")
                                            # check if amount is already return to user
                                            if credit_obj:
                                                # amount already return to user do nothing
                                                pass
                                            else:
                                                # print("Inside creating credit entry===========")
                                                # if amount is not return to user for failed transaction return amount in thw credit
                                                from EApi.credit import credit_balance_update
                                                return_credit = credit_balance_update(npci_obj[0].user.id,
                                                                                      recharge_amount,
                                                                                      "recharge_failed")

                                                if return_credit:
                                                    npci_obj[0].credit_history.add(return_credit)

                                        # update NPCI entry with "failed" status
                                        NPCI.objects.filter(id=npci_obj[0].id).update(status="failed",
                                                                                      updated_date=datetime.now())

                                    message = "recharge failed"

                                elif recharge_status in ["Success", "success"]:
                                    # recharge successfull !!!
                                    # update NPCI entry with "success" status
                                    NPCI.objects.filter(id=npci_obj[0].id).update(status="success",
                                                                                  updated_date=datetime.now())

                                    # Giving cashback amount to user..
                                    if user_id:
                                        if npci_obj[0].payment_mode in ["cash", "wallet"]:
                                            get_cashback_obj = npci_obj[0].wallet_history.filter(
                                                status="recharge_cashback")
                                            if not get_cashback_obj:
                                                from npci.views import random_amount
                                                cashback_amount = random_amount()
                                                # print("cashback_amount", cashback_amount)
                                                if cashback_amount and cashback_amount != 0:
                                                    from EApi.wallet import user_wallet_update
                                                    update_wallet = user_wallet_update(user_id, cashback_amount, None,
                                                                                       None,
                                                                                       "recharge_cashback")
                                                    if update_wallet:
                                                        npci_obj[0].wallet_history.add(update_wallet)

                                    message = "recharge success"

                                elif recharge_status == "Pending":
                                    # recharge pending
                                    # update NPCI entry with "Pending" status
                                    NPCI.objects.filter(id=npci_obj[0].id).update(status="pending",
                                                                                  updated_date=datetime.now())

                                    message = "recharge pending"
                                else:
                                    message = recharge_status
                            else:
                                message = "record not found"

                        else:
                            message = "client id in response is none"

                    else:
                        message = "client id not present in  response"

                else:
                    message = "error " + status_response["message"]

    return HttpResponse(message)
