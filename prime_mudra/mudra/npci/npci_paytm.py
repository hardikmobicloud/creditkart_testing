import datetime
import json
import random

import requests
from MApi.checksum_web import generate_checksum
from django.apps.registry import apps
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Sum
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics
from rest_framework.authentication import *
from rest_framework.permissions import *
from rest_framework.response import Response
from rest_framework.views import APIView
from math import ceil

# from .order import get_orders_delivery_charge, get_order_details_delivery_charge

# Paytm Testing Credentials
# ECOM_PROD_MERCHANT_MID = settings.ECOM_TEST_MERCHANT_MID
# ECOM_PROD_MERCHANT_KEY = settings.ECOM_TEST_MERCHANT_KEY
# ECOM_INDUSTRY_TYPE_ID = settings.ECOM_TEST_INDUSTRY_TYPE_ID
# ECOM_CHANNEL_ID = settings.ECOM_TEST_CHANNEL_ID
# ECOM_WEBSITE_CHANNEL_ID = settings.ECOM_WEBSITE_TEST_CHANNEL_ID
# ECOM_WEBSITE = settings.ECOM_TEST_WEBSITE

# Paytm Production Credentials
ECOM_PROD_MERCHANT_MID = settings.ECOM_PROD_MERCHANT_MID
ECOM_PROD_MERCHANT_KEY = settings.ECOM_PROD_MERCHANT_KEY
ECOM_INDUSTRY_TYPE_ID = settings.ECOM_INDUSTRY_TYPE_ID
ECOM_CHANNEL_ID = settings.ECOM_CHANNEL_ID
ECOM_WEBSITE_CHANNEL_ID = settings.ECOM_WEBSITE_CHANNEL_ID
ECOM_WEBSITE = settings.ECOM_WEBSITE
ECOM_WEBSITE_WEBSITE = settings.ECOM_WEBSITE_WEBSITE

# website_base_url = "http://35.154.28.133:3000/"
# website_base_url = "http://65.0.135.113:3000/"
website_base_url = "https://thecreditkart.com/"

# callback_base_url = "http://15.206.113.44:8015/"
callback_base_url = "https://ecom.thecreditkart.com/"

# PAYMENT_MODE_ONLY = "YES"
# PAYMENT_TYPE_ID = "UPI"
# AUTH_MODE = "USRPWD"

Payment = apps.get_model('Payments', 'Payment')
NPCI = apps.get_model('npci', 'NPCI')
Credit = apps.get_model('ecom', 'Credit')
UserWallet = apps.get_model('ecom', 'UserWallet')


class ChecksumGenerateNpciRechargeWebsite(APIView):
    """
        Paytm Order Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        # print("hereeeeeeeeeeeeeeeeeeeeeeeeeee")
        status = None
        message = None
        salt = None
        try:
            client_id = str(request.data.get('client_id'))
            TXN_AMOUNT = request.data.get('TXN_AMOUNT')
            random_number = random.sample(range(99999), 1)
            ORDER_ID = "RC" + str(client_id) + "e" + str(random_number[0])
            user = request.user

            merchant_key = ECOM_PROD_MERCHANT_KEY
            MID = ECOM_PROD_MERCHANT_MID
            INDUSTRY_TYPE_ID = ECOM_INDUSTRY_TYPE_ID
            CHANNEL_ID = ECOM_WEBSITE_CHANNEL_ID
            WEBSITE = ECOM_WEBSITE_WEBSITE

            # Callback_url = callback_base_url + "npci/callback_recharge_url/"
            Callback_url = callback_base_url + "npci/callback_recharge_url/"

            # shopping_order_id = "123456789"
            CUST_ID = ""
            user_id = request.user.id
            if user_id:
                user = User.objects.filter(id=user_id)
                if user:
                    CUST_ID = user[0].username

            TXN_AMOUNT = str(request.data.get('TXN_AMOUNT'))
            # TXN_AMOUNT = "1000"
            # Considering shopping order_id, CUST_ID and TXN_AMOUNT is getting.

            CALLBACK_URL = str(Callback_url) + '' + str(ORDER_ID)
            dict_param = {
                'MID': MID,
                'ORDER_ID': ORDER_ID,
                'CUST_ID': CUST_ID,
                'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                'CHANNEL_ID': CHANNEL_ID,
                'TXN_AMOUNT': TXN_AMOUNT,
                'WEBSITE': WEBSITE,
                'CALLBACK_URL': CALLBACK_URL,
                # 'PAYMENT_MODE_ONLY': PAYMENT_MODE_ONLY,
                # 'AUTH_MODE': AUTH_MODE,
                # 'PAYMENT_TYPE_ID': PAYMENT_TYPE_ID
            }
            # print('dict_parameter', dict_param)
            return_checksum = generate_checksum(dict_param, merchant_key, salt=None)

            if return_checksum:
                payment = Payment.objects.create(status='initiated', type="Cash",
                                                 order_id=str(ORDER_ID), amount=float(TXN_AMOUNT), category='Recharge',
                                                 product_type="Npci", platform_type="Web", date=datetime.datetime.now())
                # After the payment objects creation, it is needed to add this payment to respected order ID's
                # entry in orders table.
                # print('payment=========', payment)
                if payment and payment is not None:
                    # print('client_id', client_id)
                    npci_obj = NPCI.objects.filter(client_id=client_id)
                    if npci_obj:
                        # print('npci_obj', npci_obj)
                        npci_obj[0].payment.add(payment)
                        # print('here==========')

                message = "Successfully Done"
                status = 1
            else:
                message = "Something Went Wrong"
                status = 0

            # print("11111111111111111111111111111111111111111111111111111111111111111111111111")
            return Response(
                {
                    'status': status,
                    'message': message,
                    'checksum': return_checksum,
                    'mid': MID,
                    'orderId': ORDER_ID,
                    'industryType': INDUSTRY_TYPE_ID,
                    'website': WEBSITE,
                    'channel': CHANNEL_ID,
                    'callback': CALLBACK_URL,
                    'amount': TXN_AMOUNT,
                    'cust_id': CUST_ID,
                    # 'PAYMENT_MODE_ONLY': PAYMENT_MODE_ONLY,
                    # 'AUTH_MODE': AUTH_MODE,
                    # 'PAYMENT_TYPE_ID': PAYMENT_TYPE_ID
                }
            )

        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


class ChecksumGenerateNpciRecharge(APIView):
    """
        Paytm Order Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        # print("hereeeeeeeeeeeeeeeeeeeeeeeeeee")
        status = None
        message = None
        salt = None
        try:
            client_id = str(request.data.get('client_id'))
            TXN_AMOUNT = request.data.get('TXN_AMOUNT')
            random_number = random.sample(range(99999), 1)
            ORDER_ID = "RC" + str(client_id) + "e" + str(random_number[0])
            user = request.user

            merchant_key = ECOM_PROD_MERCHANT_KEY
            MID = ECOM_PROD_MERCHANT_MID
            INDUSTRY_TYPE_ID = ECOM_INDUSTRY_TYPE_ID
            CHANNEL_ID = ECOM_CHANNEL_ID
            WEBSITE = ECOM_WEBSITE
            # Callback_url = "https://ecom.thecreditkart.com/ecomapi/callback_order_url/"
            # Callback_url = "http://13.126.93.37:8007/ecomapi/callback_order_url/"
            Callback_url = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="
            # Callback_url = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="

            # shopping_order_id = "123456789"
            CUST_ID = ""
            user_id = request.user.id
            if user_id:
                user = User.objects.filter(id=user_id)
                if user:
                    CUST_ID = user[0].username

            TXN_AMOUNT = str(request.data.get('TXN_AMOUNT'))
            # TXN_AMOUNT = "1000"
            # Considering shopping order_id, CUST_ID and TXN_AMOUNT is getting.

            CALLBACK_URL = str(Callback_url) + '' + str(ORDER_ID)
            dict_param = {
                'MID': MID,
                'ORDER_ID': ORDER_ID,
                'CUST_ID': CUST_ID,
                'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                'CHANNEL_ID': CHANNEL_ID,
                'TXN_AMOUNT': TXN_AMOUNT,
                'WEBSITE': WEBSITE,
                'CALLBACK_URL': CALLBACK_URL,
                # 'PAYMENT_MODE_ONLY': PAYMENT_MODE_ONLY,
                # 'AUTH_MODE': AUTH_MODE,
                # 'PAYMENT_TYPE_ID': PAYMENT_TYPE_ID
            }
            # print('dict_parameter', dict_param)
            return_checksum = generate_checksum(dict_param, merchant_key, salt=None)

            if return_checksum:
                payment = Payment.objects.create(status='initiated', type="Cash",
                                                 order_id=str(ORDER_ID), amount=float(TXN_AMOUNT), category='Recharge',
                                                 product_type="Npci", platform_type="App", date=datetime.datetime.now())
                # After the payment objects creation, it is needed to add this payment to respected order ID's
                # entry in orders table.
                # print('payment=========', payment)
                if payment and payment is not None:
                    # print('client_id', client_id)
                    npci_obj = NPCI.objects.filter(client_id=client_id)
                    if npci_obj:
                        # print('npci_obj', npci_obj)
                        npci_obj[0].payment.add(payment)
                        # print('here==========')

                message = "Successfully Done"
                status = 1
            else:
                message = "Something Went Wrong"
                status = 0

            # print("11111111111111111111111111111111111111111111111111111111111111111111111111")
            return Response(
                {
                    'status': status,
                    'message': message,
                    'checksum': return_checksum,
                    'mid': MID,
                    'orderId': ORDER_ID,
                    'industryType': INDUSTRY_TYPE_ID,
                    'website': WEBSITE,
                    'channel': CHANNEL_ID,
                    'callback': CALLBACK_URL,
                    'amount': TXN_AMOUNT,
                    'cust_id': CUST_ID,
                    # 'PAYMENT_MODE_ONLY': PAYMENT_MODE_ONLY,
                    # 'AUTH_MODE': AUTH_MODE,
                    # 'PAYMENT_TYPE_ID': PAYMENT_TYPE_ID
                }
            )

        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


@csrf_exempt
def callback_recharge_response(request, order_id):
    # redirect_url = 'http://15.207.21.5:3000/account/process_payment?order_id='
    redirect_url = website_base_url + "account/process_payment?order_id="
    if order_id is not None:
        Recharge_transaction_Status_Check_auto(order_id)
        redirect_with_oid = redirect_url + str(order_id)
        return redirect(redirect_with_oid)
    return redirect(website_base_url + "account/payment")


@csrf_exempt
def callback_recharge_repayment_response(request, order_id):
    # redirect_url = 'http://15.207.21.5:3000/account/process_payment?order_id='
    redirect_url = website_base_url + "account/process_payment?order_id="
    if order_id is not None:
        Recharge_Repayment_transaction_Status_Check_auto(order_id)
        redirect_with_oid = redirect_url + str(order_id)
        return redirect(redirect_with_oid)
    return redirect(website_base_url + "account/payment")


class ChecksumGenerateNpciRepaymentWebsite(APIView):
    """
        Paytm Repayment Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None
        # print("==request data", request.data)

        try:
            merchant_key = ECOM_PROD_MERCHANT_KEY
            MID = ECOM_PROD_MERCHANT_MID
            INDUSTRY_TYPE_ID = ECOM_INDUSTRY_TYPE_ID
            CHANNEL_ID = ECOM_WEBSITE_CHANNEL_ID
            WEBSITE = ECOM_WEBSITE_WEBSITE

            Callback_url = callback_base_url + "npci/callback_recharge_repayment_url/"
            # Callback_url = callback_base_url + "npci/callback_recharge_repayment_url/"

            client_id = str(request.data.get('client_id'))
            random_number = random.sample(range(99999), 1)
            TXN_AMOUNT = request.data.get('TXN_AMOUNT')
            payment_mode = request.data.get('payment_mode')
            # Considering shopping order_id, CUST_ID and TXN_AMOUNT is getting.
            ORDER_ID = "RCRP" + str(client_id) + 'e' + str(random_number[0])

            # shopping_order_id = "123456789"
            CUST_ID = ""
            user_id = request.user.id
            if user_id:
                user = User.objects.filter(id=user_id)
                if user:
                    CUST_ID = user[0].username

            if payment_mode == "wallet":
                wallet_amount = 0
                from EApi.credit import get_user_wallet_amount
                wallet_dict = get_user_wallet_amount(user_id)
                if wallet_dict:
                    wallet_amount = wallet_dict["balance_amount"]

                if wallet_amount < TXN_AMOUNT:
                    # This means wallet balance is minimum..
                    return Response(
                        {
                            "status": 2,
                            "message": "Insufficient wallet balance.."
                        }
                    )

                else:
                    from EApi.wallet import user_wallet_sub
                    # Creating payment entry..
                    payment = Payment.objects.create(status="success", order_id=ORDER_ID, amount=TXN_AMOUNT,
                                                     category='Repayment', product_type="Npci", type="Ecom_Wallet",
                                                     date=datetime.datetime.now(), platform_type="Web")

                    # Creating Wallet entry..
                    wallet_status = "repayment"
                    wallet_obj = user_wallet_sub(user_id, TXN_AMOUNT, None, None, wallet_status)

                    # Creating Credit entry..
                    credit_status = "repayment"
                    from EApi.credit import credit_balance_update
                    credit_obj = credit_balance_update(user_id, TXN_AMOUNT, credit_status)

                    if None not in (wallet_obj, credit_obj):
                        npci_obj = NPCI.objects.filter(client_id=client_id).first()
                        if npci_obj:
                            npci_obj.credit_history.add(credit_obj)
                            if payment:
                                npci_obj.payment.add(payment)

                            # Updating recharge credit status..
                            from npci.views import update_recharge_status
                            update_recharge = update_recharge_status(npci_obj.id)

                            return Response(
                                {
                                    "status": 3,
                                    "message": "Your Repayment transaction done successfully.."
                                }
                            )

                    return Response(
                        {
                            "status": 0,
                            "message": "Something went wrong.."
                        }
                    )

            if payment_mode == "cash":
                CALLBACK_URL = str(Callback_url) + '' + str(ORDER_ID)
                dict_param = {
                    'MID': MID,
                    'ORDER_ID': ORDER_ID,
                    'CUST_ID': CUST_ID,
                    'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                    'CHANNEL_ID': CHANNEL_ID,
                    'TXN_AMOUNT': str(TXN_AMOUNT),
                    'WEBSITE': WEBSITE,
                    'CALLBACK_URL': CALLBACK_URL,
                    # 'PAYMENT_MODE_ONLY': PAYMENT_MODE_ONLY,
                    # 'AUTH_MODE': AUTH_MODE,
                    # 'PAYMENT_TYPE_ID': PAYMENT_TYPE_ID
                }
                # print('dict_parameter', dict_param)
                return_checksum = generate_checksum(dict_param, merchant_key, salt=None)

                if return_checksum:
                    payment = Payment.objects.create(status='initiated',
                                                     type="Cash",
                                                     order_id=str(ORDER_ID), amount=float(TXN_AMOUNT),
                                                     category='Repayment',
                                                     platform_type="Web",
                                                     product_type="Npci", date=datetime.datetime.now())

                    # After the payment objects creation, it is needed to add this payment to respected order ID's
                    # entry in orders table..
                    # print('payment=========', payment)
                    if payment and payment is not None:
                        # print('client_id', client_id)
                        npci_obj = NPCI.objects.filter(client_id=client_id)
                        if npci_obj:
                            # print('npci_obj', npci_obj)
                            npci_obj[0].payment.add(payment)

                    message = "Successfully Done"
                    status = 1
                else:
                    message = "Something Went Wrong"
                    status = 0

                return Response(
                    {
                        'status': status,
                        'message': message,
                        'checksum': return_checksum,
                        'mid': MID,
                        'orderId': ORDER_ID,
                        'industryType': INDUSTRY_TYPE_ID,
                        'website': WEBSITE,
                        'channel': CHANNEL_ID,
                        'callback': CALLBACK_URL,
                        'amount': str(TXN_AMOUNT),
                        'cust_id': CUST_ID,
                        # 'PAYMENT_MODE_ONLY': PAYMENT_MODE_ONLY,
                        # 'AUTH_MODE': AUTH_MODE,
                        # 'PAYMENT_TYPE_ID': PAYMENT_TYPE_ID
                    }
                )

        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


class ChecksumGenerateNpciRepayment(APIView):
    """
        Paytm Repayment Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None
        # print("==request data", request.data)

        try:
            merchant_key = ECOM_PROD_MERCHANT_KEY
            MID = ECOM_PROD_MERCHANT_MID
            INDUSTRY_TYPE_ID = ECOM_INDUSTRY_TYPE_ID
            CHANNEL_ID = ECOM_CHANNEL_ID
            WEBSITE = ECOM_WEBSITE
            # Callback_url = "https://ecom.thecreditkart.com/ecomapi/callback_repayment_url/"
            # Callback_url = "http://15.206.113.44:8015/ecomapi/callback_repayment_url/"
            Callback_url = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="
            # Callback_url = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="

            client_id = str(request.data.get('client_id'))
            random_number = random.sample(range(99999), 1)
            TXN_AMOUNT = request.data.get('TXN_AMOUNT')
            payment_mode = request.data.get('payment_mode')
            # Considering shopping order_id, CUST_ID and TXN_AMOUNT is getting.
            ORDER_ID = "RCRP" + str(client_id) + "e" + str(random_number[0])

            # shopping_order_id = "123456789"
            CUST_ID = ""
            user_id = request.user.id
            if user_id:
                user = User.objects.filter(id=user_id)
                if user:
                    CUST_ID = user[0].username

            if payment_mode == "wallet":
                wallet_amount = 0
                from EApi.credit import get_user_wallet_amount
                wallet_dict = get_user_wallet_amount(user_id)
                if wallet_dict:
                    wallet_amount = wallet_dict["balance_amount"]

                if wallet_amount < TXN_AMOUNT:
                    # This means wallet balance is minimum..
                    return Response(
                        {
                            "status": 2,
                            "message": "Insufficient wallet balance.."
                        }
                    )

                else:
                    from EApi.wallet import user_wallet_sub
                    # Creating payment entry..
                    payment = Payment.objects.create(status="success", order_id=ORDER_ID, amount=TXN_AMOUNT,
                                                     category='Repayment', product_type="Npci", type="Ecom_Wallet",
                                                     date=datetime.datetime.now(), platform_type="App")

                    # Creating Wallet entry..
                    wallet_status = "repayment"
                    wallet_obj = user_wallet_sub(user_id, TXN_AMOUNT, None, None, wallet_status)

                    # Creating Credit entry..
                    credit_status = "repayment"
                    from EApi.credit import credit_balance_update
                    credit_obj = credit_balance_update(user_id, TXN_AMOUNT, credit_status)

                    if None not in (wallet_obj, credit_obj):
                        npci_obj = NPCI.objects.filter(client_id=client_id).first()
                        if npci_obj:
                            npci_obj.credit_history.add(credit_obj)
                            if payment:
                                npci_obj.payment.add(payment)

                            # Updating recharge credit status..
                            from npci.views import update_recharge_status
                            update_recharge = update_recharge_status(npci_obj.id)

                            return Response(
                                {
                                    "status": 3,
                                    "message": "Your Repayment transaction done successfully.."
                                }
                            )

                    return Response(
                        {
                            "status": 0,
                            "message": "Something went wrong.."
                        }
                    )

            if payment_mode == "cash":
                CALLBACK_URL = str(Callback_url) + '' + str(ORDER_ID)
                dict_param = {
                    'MID': MID,
                    'ORDER_ID': ORDER_ID,
                    'CUST_ID': CUST_ID,
                    'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                    'CHANNEL_ID': CHANNEL_ID,
                    'TXN_AMOUNT': str(TXN_AMOUNT),
                    'WEBSITE': WEBSITE,
                    'CALLBACK_URL': CALLBACK_URL,
                    # 'PAYMENT_MODE_ONLY': PAYMENT_MODE_ONLY,
                    # 'AUTH_MODE': AUTH_MODE,
                    # 'PAYMENT_TYPE_ID': PAYMENT_TYPE_ID
                }
                # print('dict_parameter', dict_param)
                return_checksum = generate_checksum(dict_param, merchant_key, salt=None)

                if return_checksum:
                    payment = Payment.objects.create(status='initiated',
                                                     type="Cash",
                                                     order_id=str(ORDER_ID), amount=float(TXN_AMOUNT),
                                                     category='Repayment',
                                                     platform_type="App",
                                                     product_type="Npci", date=datetime.datetime.now())

                    # After the payment objects creation, it is needed to add this payment to respected order ID's
                    # entry in orders table..
                    # print('payment=========', payment)
                    if payment and payment is not None:
                        # print('client_id', client_id)
                        npci_obj = NPCI.objects.filter(client_id=client_id)
                        if npci_obj:
                            # print('npci_obj', npci_obj)
                            npci_obj[0].payment.add(payment)

                    message = "Successfully Done"
                    status = 1
                else:
                    message = "Something Went Wrong"
                    status = 0

                return Response(
                    {
                        'status': status,
                        'message': message,
                        'checksum': return_checksum,
                        'mid': MID,
                        'orderId': ORDER_ID,
                        'industryType': INDUSTRY_TYPE_ID,
                        'website': WEBSITE,
                        'channel': CHANNEL_ID,
                        'callback': CALLBACK_URL,
                        'amount': str(TXN_AMOUNT),
                        'cust_id': CUST_ID,
                        # 'PAYMENT_MODE_ONLY': PAYMENT_MODE_ONLY,
                        # 'AUTH_MODE': AUTH_MODE,
                        # 'PAYMENT_TYPE_ID': PAYMENT_TYPE_ID
                    }
                )

        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


def Recharge_Repayment_transaction_Status_Check_auto(order_id):
    import requests
    status = None
    message = None
    amount = 0.0
    date_strip = None
    try:
        if order_id:
            if 'RCRP' in order_id:
                client_id = ''
                if order_id:
                    client_id = order_id[4:]
                    client_id = client_id.split('e')
                    client_id = client_id[0]
                paytm_status = ""
                merchant_key = ECOM_PROD_MERCHANT_KEY
                m_id = ECOM_PROD_MERCHANT_MID
                dict_param = {
                    'MID': m_id,
                    'ORDERID': order_id,
                }

                checksum = str(generate_checksum(dict_param, merchant_key))

                dict_param["CHECKSUMHASH"] = checksum
                url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                # url = 'https://securegw-stage.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                r = requests.get(url=url)
                if r.status_code == 200:

                    paytm_responce = r.json()

                    if paytm_responce['STATUS'] == 'TXN_SUCCESS':
                        paytm_status = 'success'
                    elif paytm_responce['STATUS'] == 'TXN_FAILURE':
                        paytm_status = 'failed'
                    elif paytm_responce['STATUS'] == 'PENDING':
                        paytm_status = 'pending'

                    if 'TXNAMOUNT' in paytm_responce:
                        amount = paytm_responce['TXNAMOUNT']
                        if amount is None or amount == '':
                            amount = 0.0
                    if 'TXNDATE' in paytm_responce:
                        date_strip = paytm_responce['TXNDATE']
                        if date_strip is None or date_strip == '':
                            date_strip = None
                    mode = ''
                    if 'PAYMENTMODE' in paytm_responce:
                        if paytm_responce['PAYMENTMODE']:
                            mode = paytm_responce['PAYMENTMODE']

                    Payment.objects.filter(product_type="Npci", order_id=order_id,
                                           category='Repayment').update(status=paytm_status,
                                                                        transaction_id=paytm_responce[
                                                                            'TXNID'],
                                                                        amount=amount,
                                                                        mode=mode,
                                                                        type='App',
                                                                        response=paytm_responce,
                                                                        date=date_strip,
                                                                        update_date=datetime.datetime.now())

                    user_id = None
                    npci_obj = NPCI.objects.filter(client_id=client_id).first()
                    if npci_obj:
                        user_id = npci_obj.user.id
                    if paytm_status == "success":
                        if user_id:
                            # Create success entry in credit..
                            from EApi.credit import credit_balance_update
                            credit_status = "repayment"
                            credit_add = credit_balance_update(user_id, amount, credit_status)
                            if credit_add:
                                npci_obj.credit_history.add(credit_add)
                                from npci.views import update_recharge_status
                                update_recharge = update_recharge_status(npci_obj.id)

                            # try:
                            #     number = npci_obj.user.username
                            #     osms_kwrgs = {
                            #         'sms_type': "repayment",
                            #         'number': str(numbekr)
                            #     }
                            #     order_sms(**osms_kwrgs)
                            # except Exception as e:
                            #     print("error in repayment")

                            status = '1'
                            message = 'Transaction successful.'

                    elif paytm_status == 'failed':

                        status = '2'
                        message = 'Transaction failed'

                    elif paytm_status == 'pending':

                        status = '3'
                        message = 'Transaction pending'

                return Response(
                    {
                        'status': status,
                        'message': message
                    }
                )

            else:
                return Response(
                    {
                        'status': status,
                        'message': "order id is not valid"
                    }
                )

        else:
            return Response(
                {
                    'status': status,
                    'message': "order id could not be none"
                }
            )

    except Exception as e:

        import sys
        import os

        print('-----------in exception----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)


def pass_order_id_order(request, order_id):
    if order_id is not None:
        data = RechargeTransactionStatusCheckApplication(order_id)
    return HttpResponse("ok")


def pass_order_id_repayment(request, order_id):
    if order_id is not None:
        data = RechargeRepaymentTransactionStatusCheckApplication(order_id)
    return "OKay"


def Recharge_transaction_Status_Check_auto(order_id):
    import requests
    status = None
    payment_message = ""
    recharge_message = "Your recharge could not Proceed"
    amount = 0.0
    date_strip = None
    # print("order_id=================================", order_id)
    try:
        if order_id:
            paytm_status = ""
            merchant_key = ECOM_PROD_MERCHANT_KEY
            m_id = ECOM_PROD_MERCHANT_MID
            dict_param = {
                'MID': m_id,
                'ORDERID': order_id,
            }

            client_id = None
            if "RC" in order_id:
                client_id = order_id[2:]
                client_id = client_id.split('e')
                client_id = client_id[0]

            checksum = str(generate_checksum(dict_param, merchant_key))

            dict_param["CHECKSUMHASH"] = checksum
            url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
            # url = 'https://securegw-stage.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
            r = requests.get(url=url)
            if r.status_code == 200:

                paytm_responce = r.json()
                # print("paytm response", paytm_responce)
                if paytm_responce['STATUS'] == 'TXN_SUCCESS':
                    paytm_status = 'success'
                elif paytm_responce['STATUS'] == 'TXN_FAILURE':
                    paytm_status = 'failed'
                elif paytm_responce['STATUS'] == 'PENDING':
                    paytm_status = 'pending'

                if 'TXNAMOUNT' in paytm_responce:
                    amount = paytm_responce['TXNAMOUNT']
                    if amount is None or amount == '':
                        amount = 0.0
                if 'TXNDATE' in paytm_responce:
                    date_strip = paytm_responce['TXNDATE']
                    if date_strip is None or date_strip == '':
                        date_strip = None
                mode = ''
                if 'PAYMENTMODE' in paytm_responce:
                    if paytm_responce['PAYMENTMODE']:
                        mode = paytm_responce['PAYMENTMODE']

                Payment.objects.filter(order_id=order_id,
                                       category='Recharge').update(status=paytm_status,
                                                               transaction_id=paytm_responce[
                                                                   'TXNID'],
                                                               amount=amount,
                                                               mode=mode,
                                                               type='App',
                                                               response=paytm_responce,
                                                               date=date_strip,
                                                               update_date=datetime.datetime.now())

                npci_obj = NPCI.objects.filter(client_id=client_id).first()
                # print("npci_obj", npci_obj)
                if npci_obj:
                    user_id = npci_obj.user.id
                    if paytm_status == "success":
                        status = '1'
                        payment_message = 'Transaction successful.'

                        from npci.npci_api import call_npci_recharge_api
                        try:
                            recharge_status = call_npci_recharge_api(npci_obj.id)
                            if recharge_status:
                                if recharge_status in ["Failed", "Failure", "Error"]:
                                    # This means Payment is success and recharge is failed..
                                    # Return Transaction amount in users wallet..
                                    from EApi.wallet import user_wallet_update
                                    return_wallet = user_wallet_update(user_id, float(amount), None, None,
                                                                       "recharge_failed")
                                    # print("wallet balance return", return_wallet)

                                    if return_wallet:
                                        npci_obj.wallet_history.add(return_wallet)

                                if recharge_status in ["Success", "success"]:
                                    # Giving cashback amount to user..
                                    if user_id:
                                        get_cashback_obj = npci_obj.wallet_history.filter(
                                            status="recharge_cashback")
                                        if not get_cashback_obj:
                                            from npci.views import random_amount
                                            cashback_amount = random_amount()
                                            if cashback_amount and cashback_amount != 0:
                                                from EApi.wallet import user_wallet_update
                                                update_wallet = user_wallet_update(user_id, cashback_amount, None, None,
                                                                                   "recharge_cashback")
                                                if update_wallet:
                                                    npci_obj.wallet_history.add(update_wallet)

                                from npci.npci_api import get_recharge_message
                                recharge_message = get_recharge_message(recharge_status)

                        except Exception as e:
                            import sys
                            import os
                            print(
                                "Inside Exception of Recharge_transaction_Status_Check_auto == call_npci_recharge_api",
                                e)
                            print('-----------in exception last loan dpd----------')
                            print(e.args)
                            exc_type, exc_obj, exc_tb = sys.exc_info()
                            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                            print(exc_type, fname, exc_tb.tb_lineno)

                    else:
                        if paytm_status == 'failed':
                            status = '2'
                            payment_message = 'Transaction failed'

                        if paytm_status == 'pending':
                            status = '3'
                            payment_message = 'Transaction pending'

                        # Update Npci recharge entry..
                        npci_obj = NPCI.objects.filter(id=npci_obj.id).update(status=paytm_status,
                                                                              updated_date=datetime.datetime.now())

            return Response(
                {
                    'status': status,
                    'payment_message': payment_message,
                    'recharge_message': recharge_message
                }
            )

        else:
            return Response(
                {
                    'status': status,
                    'message': "order could not be None"
                }
            )

    except Exception as e:
        import sys
        import os
        print('-----------in exception----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)


class RechargeTransactionStatusCheckApplication(generics.ListAPIView):
    """
    This Api is to Check Order ransaction status check and update the info after order payment..
    Url: ecomapi/user_orders
    Authentication token required
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User toke is required..
    method = get
    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    def post(self, request, *args, **kwargs):
        order_id = request.data.get("order_id")
        import requests
        status = None
        payment_message = None
        recharge_message = None
        amount = 0.0
        date_strip = None
        try:
            if order_id:
                client_id = ''
                if 'RC' in order_id:
                    client_id = order_id[2:]
                    client_id = client_id.split('e')
                    client_id = client_id[0]
                paytm_status = ""
                merchant_key = ECOM_PROD_MERCHANT_KEY
                m_id = ECOM_PROD_MERCHANT_MID
                dict_param = {
                    'MID': m_id,
                    'ORDERID': order_id,
                }

                checksum = str(generate_checksum(dict_param, merchant_key))

                dict_param["CHECKSUMHASH"] = checksum
                # url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                url = 'https://securegw-stage.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                r = requests.get(url=url)
                if r.status_code == 200:

                    paytm_responce = r.json()

                    if paytm_responce['STATUS'] == 'TXN_SUCCESS':
                        paytm_status = 'success'
                    elif paytm_responce['STATUS'] == 'TXN_FAILURE':
                        paytm_status = 'failed'
                    elif paytm_responce['STATUS'] == 'PENDING':
                        paytm_status = 'pending'

                    if 'TXNAMOUNT' in paytm_responce:
                        amount = paytm_responce['TXNAMOUNT']
                        if amount is None or amount == '':
                            amount = 0.0
                    if 'TXNDATE' in paytm_responce:
                        date_strip = paytm_responce['TXNDATE']
                        if date_strip is None or date_strip == '':
                            date_strip = None
                    mode = ''
                    if 'PAYMENTMODE' in paytm_responce:
                        if paytm_responce['PAYMENTMODE']:
                            mode = paytm_responce['PAYMENTMODE']

                    Payment.objects.filter(order_id=order_id,
                                           category='Recharge').update(status=paytm_status,
                                                                       transaction_id=paytm_responce[
                                                                           'TXNID'],
                                                                       amount=amount,
                                                                       mode=mode,
                                                                       type='App',
                                                                       response=paytm_responce,
                                                                       date=date_strip,
                                                                       update_date=datetime.datetime.now())

                    npci_obj = NPCI.objects.filter(client_id=client_id).first()
                    if npci_obj:
                        user_id = npci_obj.user.id
                        if paytm_status == "success":
                            status = '1'
                            payment_message = 'Transaction successful.'

                            from npci.npci_api import call_npci_recharge_api
                            try:
                                recharge_response = call_npci_recharge_api(npci_obj.id)
                                if recharge_response:
                                    if recharge_response in ["Failed", "Failure", "Error"]:
                                        # This means Payment is success and recharge is failed..
                                        # Return Transaction amount in users wallet..
                                        from EApi.wallet import user_wallet_update
                                        return_wallet = user_wallet_update(user_id, amount, None, None,
                                                                           "recharge_failed")
                                        # print("wallet balance return", return_wallet)

                                    if recharge_response in ["Success", "success"]:
                                        # Giving cashback amount to user..
                                        if user_id:
                                            get_cashback_obj = npci_obj.wallet_history.filter(
                                                status="recharge_cashback")
                                            if not get_cashback_obj:
                                                from npci.views import random_amount
                                                cashback_amount = random_amount()
                                                if cashback_amount and cashback_amount != 0:
                                                    from EApi.wallet import user_wallet_update
                                                    update_wallet = user_wallet_update(user_id, cashback_amount, None,
                                                                                       None,
                                                                                       "recharge_cashback")
                                                    if update_wallet:
                                                        npci_obj.wallet_history.add(update_wallet)

                                    from npci.npci_api import get_recharge_message
                                    recharge_message = get_recharge_message(recharge_response)

                            except Exception as e:
                                import sys
                                import os
                                print(
                                    "Inside Exception of RechargeTransactionStatusCheckApplication == call_npci_recharge_api",
                                    e)
                                print('-----------in exception last loan dpd----------')
                                print(e.args)
                                exc_type, exc_obj, exc_tb = sys.exc_info()
                                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                                print(exc_type, fname, exc_tb.tb_lineno)

                        else:
                            if paytm_status == 'failed':
                                status = '2'
                                payment_message = 'Transaction failed'

                                # Return transaction amount in Users Wallet..

                            if paytm_status == 'pending':
                                status = '3'
                                payment_message = 'Transaction pending'

                            # Update Npci recharge entry..
                            npci_obj = NPCI.objects.filter(id=npci_obj.id).update(status=paytm_status,
                                                                                  updated_date=datetime.datetime.now())

                    return Response(
                        {
                            'status': status,
                            'payment_message': payment_message,
                            'recharge_message': recharge_message
                        }
                    )

            else:
                return Response(
                    {
                        'status': status,
                        'message': "order id could not be none"
                    }
                )

        except Exception as e:
            import sys
            import os
            print('-----------in exception----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)


class RechargeRepaymentTransactionStatusCheckApplication(generics.ListAPIView):
    """
    This API is to check repayments for Selected order..
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        import requests
        status = None
        message = None
        amount = 0.0
        date_strip = None
        order_id = request.data.get('order_id')
        try:
            if order_id:
                if 'RCRP' in order_id:
                    client_id = ''
                    if order_id:
                        client_id = order_id[4:]
                        client_id = client_id.split('e')
                        client_id = client_id[0]
                    paytm_status = ""
                    merchant_key = ECOM_PROD_MERCHANT_KEY
                    m_id = ECOM_PROD_MERCHANT_MID
                    dict_param = {
                        'MID': m_id,
                        'ORDERID': order_id,
                    }

                    checksum = str(generate_checksum(dict_param, merchant_key))

                    dict_param["CHECKSUMHASH"] = checksum
                    # url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                    url = 'https://securegw-stage.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                    r = requests.get(url=url)
                    if r.status_code == 200:

                        paytm_responce = r.json()

                        if paytm_responce['STATUS'] == 'TXN_SUCCESS':
                            paytm_status = 'success'
                        elif paytm_responce['STATUS'] == 'TXN_FAILURE':
                            paytm_status = 'failed'
                        elif paytm_responce['STATUS'] == 'PENDING':
                            paytm_status = 'pending'

                        if 'TXNAMOUNT' in paytm_responce:
                            amount = paytm_responce['TXNAMOUNT']
                            if amount is None or amount == '':
                                amount = 0.0
                        if 'TXNDATE' in paytm_responce:
                            date_strip = paytm_responce['TXNDATE']
                            if date_strip is None or date_strip == '':
                                date_strip = None
                        mode = ''
                        if 'PAYMENTMODE' in paytm_responce:
                            if paytm_responce['PAYMENTMODE']:
                                mode = paytm_responce['PAYMENTMODE']

                        Payment.objects.filter(product_type="Npci", order_id=order_id,
                                               category='Repayment').update(status=paytm_status,
                                                                            transaction_id=paytm_responce[
                                                                                'TXNID'],
                                                                            amount=amount,
                                                                            mode=mode,
                                                                            type='App',
                                                                            response=paytm_responce,
                                                                            date=date_strip,
                                                                            update_date=datetime.datetime.now())

                        user_id = None
                        npci_obj = NPCI.objects.filter(client_id=client_id).first()
                        if npci_obj:
                            user_id = npci_obj.user.id
                        if paytm_status == "success":
                            if user_id:
                                # Create success entry in credit..
                                from EApi.credit import credit_balance_update
                                credit_status = "repayment"
                                credit_add = credit_balance_update(user_id, amount, credit_status)
                                if credit_add:
                                    npci_obj.credit_history.add(credit_add)
                                    from npci.views import update_recharge_status
                                    update_recharge = update_recharge_status(npci_obj.id)

                                # try:
                                #     number = npci_obj.user.username
                                #     osms_kwrgs = {
                                #         'sms_type': "repayment",
                                #         'number': str(number)
                                #     }
                                #     order_sms(**osms_kwrgs)
                                # except Exception as e:
                                #     print("error in repayment")

                                status = '1'
                                message = 'Transaction successful.'

                        elif paytm_status == 'failed':

                            status = '2'
                            message = 'Transaction failed'

                        elif paytm_status == 'pending':

                            status = '3'
                            message = 'Transaction pending'

                    return Response(
                        {
                            'status': status,
                            'message': message
                        }
                    )

                else:
                    return Response(
                        {
                            'status': status,
                            'message': "order id is not valid"
                        }
                    )

            else:
                return Response(
                    {
                        'status': status,
                        'message': "order id could not be none"
                    }
                )

        except Exception as e:

            import sys
            import os

            print('-----------in exception----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
