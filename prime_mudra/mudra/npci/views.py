from django.shortcuts import render, HttpResponse

# Create your views here..
from datetime import date, datetime, timedelta
from django.db.models import Sum
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
import pandas as pd
import json
from django.contrib.auth.decorators import login_required
from django.apps.registry import apps

NPCI = apps.get_model('npci', 'NPCI')
Payments = apps.get_model('Payments', 'Payment')
SchemeAmount = apps.get_model('npci', 'SchemeAmount')
Credit = apps.get_model('ecom', 'Credit')
UserWallet = apps.get_model('ecom', 'UserWallet')


def user_npci_defaulter(user_id):
    """
    This function returns NPCI credit users defaulter status based on their repayments..
    """
    defaulter_status = "False"
    if user_id is not None:
        get_npci_objects = NPCI.objects.filter(user=user_id).exclude(credit_status="Failed")
        if get_npci_objects:
            for recharge in get_npci_objects:
                recharge_date = recharge.created_date
                today_date = datetime.now()
                repay_due_date = recharge_date + timedelta(days=30)
                if recharge.credit:
                    total_paid = 0
                    credit_amount = recharge.credit.actual_used
                    paid_amount = get_recharge_repayment_amount(recharge.id)
                    if paid_amount:
                        total_paid = paid_amount
                    if today_date > repay_due_date and total_paid < credit_amount:
                        defaulter_status = "True"
                        return defaulter_status
                    else:
                        defaulter_status = "False"
    return defaulter_status


def get_credit_recharge_repay_details(recharge_id):
    """
    This function returns
    """
    data_dict = {}
    credit_used = 0
    total_paid = 0
    pending_amount = 0
    repay_due_date = None
    if recharge_id is not None:
        recharge_obj = NPCI.objects.filter(id=recharge_id).first()
        if recharge_obj:
            repay_due_date = recharge_obj.created_date + timedelta(days=30)
            if recharge_obj.credit:
                credit_used = recharge_obj.actual_used
                paid_amount = get_recharge_repayment_amount(recharge_obj.id)
                if paid_amount:
                    total_paid = paid_amount
                pending_amount = credit_used - total_paid

    data_dict["credit_used"] = credit_used
    data_dict["total_paid"] = total_paid
    data_dict["pending_amount"] = pending_amount
    data_dict["repay_due_date"] = repay_due_date
    return data_dict


def get_recharge_repayment_amount(recharge_id):
    amount = 0
    if recharge_id is not None:
        get_recharge_obj = NPCI.objects.filter(id=recharge_id).first()
        if get_recharge_obj:
            paid_amount = get_recharge_obj.credit_history.filter(status="repayment",
                                                                 type="Npci").aggregate(Sum('repayment_add'))[
                'repayment_add__sum']
            if paid_amount:
                amount = paid_amount
    return amount


def update_recharge_status(recharge_id):
    """
    This function updates credit status of NPCI recharge entry by calculating credit used and repayments done..
    """
    if recharge_id is not None:
        npci_obj = NPCI.objects.filter(id=recharge_id)
        if npci_obj:
            total_paid = 0
            actual_used = 0
            if npci_obj[0].credit:
                actual_used = npci_obj[0].credit.actual_used
            repayment_add = npci_obj[0].credit_history.filter(status="repayment").aggregate(Sum('repayment_add'))[
                'repayment_add__sum']
            if repayment_add:
                total_paid = repayment_add
            credit_status = ""
            if total_paid >= actual_used:
                credit_status = "Paid"
            elif 0 < total_paid < actual_used:
                credit_status = "Pay"
            update_obj = NPCI.objects.filter(id=recharge_id).update(credit_status=credit_status)
            if update_obj:
                return True

    return False


def random_amount():
    import random
    amount_list = []
    amount = 0
    samount = SchemeAmount.objects.filter(status=True)
    if samount:
        samount_count = samount.aggregate(Sum("count"))["count__sum"]
        if samount_count >= 100:
            SchemeAmount.objects.update(count=0)
    for sa in samount:
        if sa.amount_count and (sa.amount_count != sa.count):
            sub_count = sa.amount_count - sa.count
            if sub_count > 0:
                for i in range(int(sub_count)):
                    amount_list.append(sa.amount)
    if amount_list:
        amount = random.choice(amount_list)
        smamount = SchemeAmount.objects.filter(amount=amount, status=True)
        a_count = 0
        if smamount:
            a_count = int(smamount[0].count)
            a_count = a_count + 1
            smamount.update(count=a_count)
    return amount


def datewise_dashboard_report(request, service_category, month, category, type):
    ctx = {}
    data_dict = {}
    total_amount = None
    date_list = NPCI.objects.filter(created_date__startswith=month).values_list("created_date", flat=True)
    if date_list:
        created_date = [str(i)[:10] for i in date_list]
        # taking unique date
        dates = list(dict.fromkeys(created_date))
        if dates and dates is not None:
            for date in dates:
                total = 0
                cash_amount = 0
                credit_amount = 0
                wallet_amount = 0

                if type and category:
                    if category == "npci_receive":
                        payment_mode = "cash"
                    elif category == "credit":
                        payment_mode = "credit"
                    elif category == "wallet":
                        payment_mode = "wallet"
                    else:
                        payment_mode = None
                    if payment_mode and payment_mode is not None:

                        if type == "count":
                            npci_object = NPCI.objects.filter(service_category=service_category, status="success",
                                                              payment_mode=payment_mode,
                                                              created_date__startswith=date).count()

                        elif type == "amount":

                            npci_object = NPCI.objects.filter(service_category=service_category, status="success",
                                                              payment_mode=payment_mode,
                                                              created_date__startswith=date).aggregate(Sum("amount"))[
                                "amount__sum"]

                        else:
                            pass
                    else:
                        if type == "count":
                            npci_object = NPCI.objects.filter(service_category=service_category, status="success",
                                                              created_date__startswith=date).count()
                        elif type == "amount":
                            npci_object = NPCI.objects.filter(service_category=service_category, status="success",
                                                              created_date__startswith=date).aggregate(Sum("amount"))[
                                "amount__sum"]
                        else:
                            pass
                    if npci_object and npci_object is not None:

                        data_dict.update(
                            {
                                date: npci_object
                            }
                        )

                    else:
                        data_dict.update(
                            {
                                date: 0
                            }
                        )

    ctx["type"] = type
    ctx["service_category"] = service_category
    ctx["category"] = category
    ctx["data"] = data_dict
    return render(request, "Ecom/NPCI/datewise_dashboard_report.html", ctx)


def recharge_transaction(request, date, service_category, category):
    ctx = {}
    if category is not None and service_category is not None:
        if category == "npci_receive":
            payment_mode = "cash"
        elif category == "credit":
            payment_mode = "credit"
        elif category == "wallet":
            payment_mode = "wallet"
        else:
            payment_mode = None

        if payment_mode and payment_mode is not None:
            npci_obj = NPCI.objects.filter(created_date__startswith=date, service_category=service_category,
                                           payment_mode=payment_mode)
        else:
            npci_obj = NPCI.objects.filter(created_date__startswith=date, service_category=service_category)

        if npci_obj and npci_obj is not None:
            ctx["data"] = npci_obj
    return render(request, "Ecom/NPCI/recharge_transaction.html", ctx)


@login_required(login_url="/ecom/login/")
def monthwise_dashboard_report(request, service_category, category, type):
    ctx = {}
    data_dict = {}

    if type and category:
        if category == "npci_receive":
            payment_mode = "cash"
        elif category == "credit":
            payment_mode = "credit"
        elif category == "wallet":
            payment_mode = "wallet"
        else:
            payment_mode = None
        if payment_mode and payment_mode is not None:
            if type == "count":
                npci_object = NPCI.objects.filter(service_category=service_category, payment_mode=payment_mode,
                                                  status="success").values_list(
                    "created_date",
                    "amount")

                if npci_object:
                    df = pd.DataFrame(npci_object, columns=["created_date", "amount"])
                    df['YearMonth'] = pd.to_datetime(df['created_date']).dt.strftime("%Y-%m")

                    data_df = df.groupby(df['YearMonth'])['YearMonth'].count().sort_index(axis=0)

            elif type == "amount":

                npci_object = NPCI.objects.filter(service_category=service_category, payment_mode=payment_mode,
                                                  status="success").values_list(
                    "created_date",
                    "amount")

                if npci_object:
                    df = pd.DataFrame(npci_object, columns=["created_date", "amount"])
                    df['YearMonth'] = pd.to_datetime(df['created_date']).dt.strftime("%Y-%m")

                    data_df = df.groupby(df['YearMonth'])['amount'].sum().sort_index(axis=0)
            else:
                pass
        else:
            if type == "count":
                npci_object = NPCI.objects.filter(service_category=service_category,
                                                  status="success").values_list(
                    "created_date",
                    "amount")

                if npci_object:
                    df = pd.DataFrame(npci_object, columns=["created_date", "amount"])
                    df['YearMonth'] = pd.to_datetime(df['created_date']).dt.strftime("%Y-%m")

                    data_df = df.groupby(df['YearMonth'])['YearMonth'].count().sort_index(axis=0)
            elif type == "amount":
                npci_object = NPCI.objects.filter(service_category=service_category,
                                                  status="success").values_list(
                    "created_date",
                    "amount")

                if npci_object:
                    df = pd.DataFrame(npci_object, columns=["created_date", "amount"])
                    df['YearMonth'] = pd.to_datetime(df['created_date']).dt.strftime("%Y-%m")

                    data_df = df.groupby(df['YearMonth'])['amount'].sum().sort_index(axis=0)
            else:
                pass

    if npci_object:

        result = data_df.to_json(orient="split")
        parsed = json.loads(result)
        month = parsed["index"]
        sum = parsed["data"]

        for i in range(len(month)):
            data_dict.update(
                {
                    month[i]: sum[i]
                }
            )

    ctx["category"] = category
    ctx["type"] = type
    ctx["data"] = data_dict
    ctx["service_category"] = service_category
    return render(request, "Ecom/NPCI/monthwise_dashboard_report.html", ctx)


def serach_recharge(request):
    return render(request, "Ecom/NPCI/serach_recharge.html")


def search_payment(request):
    return render(request, "Ecom/NPCI/search_payment.html")


def search_recharge_data(request):
    data_dict = {}
    data_list = []
    payment_list = []
    ctx = {}
    credit = None
    balance_credit = None
    used_credit = None
    wallet_balance_amount = None
    wallet_used = None
    wallet = None
    npci_obj = None
    repayment = None
    if request.method == "POST":
        id = request.POST.get("id")
        search_by = request.POST.get("search_by")
        if id and id is not None:
            if search_by == "recharge_id":
                npci_obj = NPCI.objects.select_related("credit", "user", "wallet").filter(recharge_id=id)
            elif search_by == "client_id":
                npci_obj = NPCI.objects.select_related("credit", "user", "wallet").filter(client_id=id)
            elif search_by == "mobile":
                npci_obj = NPCI.objects.select_related("credit", "user", "wallet").filter(number=id)
            elif search_by == "user":
                npci_obj = NPCI.objects.select_related("credit", "user", "wallet").filter(user__username=id)
            else:
                pass

            if npci_obj and npci_obj is not None:
                for npci in npci_obj:
                    id = npci.id

                    if npci.credit:
                        credit = npci.credit.credit

                    if npci.user:
                        user = npci.user.username

                        wallet_obj = UserWallet.objects.filter(user=npci.user).exclude(status="initiated").last()
                        if wallet_obj:
                            wallet_balance_amount = wallet_obj.balance_amount

                        credit_obj = Credit.objects.filter(user=npci.user).exclude(status="initiated").last()
                        if credit_obj:
                            balance_credit = credit_obj.balance_credit
                            used_credit = credit_obj.used_credit

                        repayment_added = npci.credit_history.filter(created_date__gte=npci.created_date,
                                                                     status="repayment")
                        if repayment_added:
                            for repayment_1 in repayment_added:
                                repayment += repayment_1.repayment_add

                    if npci.wallet:
                        wallet_used = npci.wallet.used_amount

                    if npci.user:
                        user = npci.user.username

                    if npci.payment:
                        payment = npci.payment.order_by("-id")
                        if payment:
                            for payment_obj in payment:
                                payment_list.append([payment_obj.order_id, payment_obj.status, payment_obj.pay_source,
                                                     payment_obj.platform_type, payment_obj.product_type,
                                                     payment_obj.amount, payment_obj.category, payment_obj.mode])

                    data_dict.update(
                        {
                            "number": npci.number,
                            "operatorcode": npci.operatorcode,
                            "account": npci.account,
                            "auth": npci.auth,
                            "amount": npci.amount,
                            "bill_verify_id": npci.bill_verify_id,
                            "client_id": npci.client_id,
                            "recharge_id": npci.recharge_id,
                            "payment_mode": npci.payment_mode,
                            "service_category": npci.service_category,
                            "service_type": npci.service_type,
                            "status": npci.status,
                            "user": user,
                            "date": str(npci.date),
                            "credit": credit,
                            "balance_credit": balance_credit,
                            "used_credit": used_credit,
                            "wallet_used": wallet_used,
                            "wallet_balance_amount": wallet_balance_amount,
                            "repayment": repayment,
                            "payment_list": payment_list
                        }
                    )
                    data_list.append(data_dict)

                ctx["data"] = data_list
                ctx["message"] = "success"
            else:
                ctx["message"] = "No such record found"

        else:
            ctx["message"] = "Missing Mandatory parameters"
    # return HttpResponse(json.dumps(ctx))
    return render(request, "Ecom/NPCI/serach_recharge.html", ctx)


@csrf_exempt
def search_payment_data(request):
    data_dict = {}
    data_list = []
    ctx = {}

    if request.method == "POST":
        id = request.POST.get("id")
        search_by = request.POST.get("search_by")
        id = id.strip()
        if id and id is not None:
            if search_by == "order_id":
                payment_obj = Payments.objects.filter(order_id=str(id))
            elif search_by == "transaction_id":
                payment_obj = Payments.objects.filter(transaction_id=id)
            else:
                pass

            if payment_obj and payment_obj is not None:
                for payment in payment_obj:
                    data_dict.update(
                        {
                            "order_id": payment.order_id,
                            "transaction_id": payment.transaction_id,
                            "pg_transaction_id": payment.pg_transaction_id,
                            "status": payment.status,
                            "amount": payment.amount,
                            "category": payment.category,
                            "mode": payment.mode,
                            "type": payment.type,
                            "description": payment.description,
                            "date": str(payment.date),
                            "create_date": str(payment.create_date),
                            "update_date": str(payment.update_date),
                            "pay_source": payment.pay_source,
                            "product_type": payment.product_type,
                            "platform_type": payment.platform_type
                        }
                    )

                ctx["data"] = data_dict
                ctx["message"] = "success"
            else:
                ctx["message"] = "No such record found"

        else:
            ctx["message"] = "Missing Mandatory parameters"

    return HttpResponse(json.dumps(ctx))


def npci_defaulters(request):
    ctx = {}
    defaulter_dict = {}
    upto_date = datetime.now() - timedelta(days=30)
    npci_obj = NPCI.objects.filter(created_date__lt=upto_date, payment_mode="credit", status="success")
    if npci_obj:
        for npci in npci_obj:
            is_defaulter = False
            total_repayment = 0
            repayment_added = npci.credit_history.filter(created_date__gte=npci.created_date,
                                                         status="repayment")

            if repayment_added:
                for repayment in repayment_added:
                    total_repayment += repayment.repayment_add
            else:
                total_repayment = 0

            if total_repayment < npci.amount:
                is_defaulter = True
                defaulter_amount = float(npci.amount) - float(total_repayment)
            else:
                pass

            if is_defaulter == True:
                defaulter_dict.update(
                    {
                        npci: defaulter_amount
                    }
                )
        ctx["data"] = defaulter_dict

    return render(request, "Ecom/NPCI/npci_defaulters.html", ctx)


def test():
    ctx = {}
    defaulter_dict = {}
    upto_date = datetime.now() - timedelta(days=30)
    npci_obj = NPCI.objects.filter(created_date__lt=upto_date, payment_mode="credit", status="success")
    if npci_obj:
        for npci in npci_obj:
            is_defaulter = False
            total_repayment = 0
            repayment_added = npci_obj.credit_history.filter(created_date__gte=npci_obj.created_date,
                                                             status="repayment")
            if repayment_added:
                for repayment in repayment_added:
                    total_repayment += repayment.repayment_add
                if total_repayment >= npci.amount:
                    is_defaulter = True
                    defaulter_amount = float(npci.amount) - float(total_repayment)
                    defaulter_dict.update(
                        {
                            npci: defaulter_amount
                        }
                    )
                else:
                    pass

        ctx["data"] = defaulter_dict


# test()

@login_required(login_url="/ecom/login/")
def npci_graph(request, type):
    ctx = {}
    ctx["type"] = type
    return render(request, "Ecom/NPCI/npci_graph.html", ctx)


@csrf_exempt
@login_required(login_url="/ecom/login/")
def npci_graph_data(request):
    from datetime import date, datetime, timedelta
    ctx = {}
    all_recharge = 0
    mobile_recharge = 0
    dth_recharge = 0
    postpaid_mobile = 0
    electric_bill = 0
    landline_bill = 0
    gas_bill = 0
    water_bill = 0
    broadband_bill = 0
    insurance_bill_list = 0

    if "type" in request.POST:
        type = request.POST["type"]
        ctx['type'] = type
    else:
        return HttpResponse(json.dumps(ctx))

    all_recharge_list = []
    mobile_recharge_list = []
    dth_recharge_list = []
    postpaid_mobile_list = []
    electric_bill_list = []
    landline_bill_list = []
    gas_bill_list = []
    water_bill_list = []
    broadband_bill_list = []
    insurance_bill_list = []

    m = datetime.now().month
    y = datetime.now().year
    z = datetime.now().day
    if m == 12:
        month = 1
        year = y + 1
    else:
        month = m
        year = y
    total_days = (date(year, month, 1) - date(y, m, 1)).days
    if z > total_days:
        ndays = z
    else:
        ndays = z + 1
    d1 = date(y, m, 1)
    d2 = date(y, m, ndays)
    delta = d2 - d1
    date_list = [(d1 + timedelta(days=i)).strftime('%Y-%m-%d') for i in range(delta.days + 1)]

    last_date = []
    for final_date in date_list:
        last_date.append(final_date)

        if type == "count":

            all_recharge = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success").count()
            if all_recharge != 0:
                all_recharge_list.append(all_recharge)

            mobile_recharge = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success",
                                                                                              service_category="mobile_recharge").count()
            if mobile_recharge != 0:
                mobile_recharge_list.append(mobile_recharge)

            dth_recharge = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success",
                                                                                           service_category="dth_recharge").count()
            if dth_recharge != 0:
                dth_recharge_list.append(dth_recharge)

            postpaid_mobile = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success",
                                                                                              service_category="postpaid_mobile").count()
            if postpaid_mobile != 0:
                postpaid_mobile_list.append(postpaid_mobile)

            electric_bill = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success",
                                                                                            service_category="electric_bill").count()
            if electric_bill != 0:
                electric_bill_list.append(electric_bill)

            landline_bill = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success",
                                                                                            service_category="landline_bill").count()
            if landline_bill != 0:
                landline_bill_list.append(landline_bill)

            gas_bill = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success",
                                                                                       service_category="gas_bill").count()
            if gas_bill != 0:
                gas_bill_list.append(gas_bill)

            water_bill = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success",
                                                                                         service_category="water_bill").count()
            if water_bill != 0:
                water_bill_list.append(water_bill)

            broadband_bill = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success",
                                                                                             service_category="broadband_bill").count()
            if broadband_bill != 0:
                broadband_bill_list.append(broadband_bill)

            insurance_bill = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success",
                                                                                             service_category="insurance_bill").count()
            if insurance_bill != 0:
                insurance_bill_list.append(insurance_bill)

        elif type == "amount":
            all_recharge = \
                NPCI.objects.filter(created_date__startswith=final_date).filter(status="success").aggregate(
                    Sum("amount"))[
                    "amount__sum"]
            if all_recharge != 0:
                all_recharge_list.append(all_recharge)

            mobile_recharge = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success",
                                                                                              service_category="mobile_recharge").aggregate(
                Sum("amount"))[
                "amount__sum"]
            if mobile_recharge != 0:
                mobile_recharge_list.append(mobile_recharge)

            dth_recharge = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success",
                                                                                           service_category="dth_recharge").aggregate(
                Sum("amount"))[
                "amount__sum"]
            if dth_recharge != 0:
                dth_recharge_list.append(dth_recharge)

            postpaid_mobile = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success",
                                                                                              service_category="postpaid_mobile").aggregate(
                Sum("amount"))[
                "amount__sum"]
            if postpaid_mobile != 0:
                postpaid_mobile_list.append(postpaid_mobile)

            electric_bill = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success",
                                                                                            service_category="electric_bill").aggregate(
                Sum("amount"))[
                "amount__sum"]
            if electric_bill != 0:
                electric_bill_list.append(electric_bill)

            landline_bill = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success",
                                                                                            service_category="landline_bill").aggregate(
                Sum("amount"))[
                "amount__sum"]
            if landline_bill != 0:
                landline_bill_list.append(landline_bill)

            gas_bill = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success",
                                                                                       service_category="gas_bill").aggregate(
                Sum("amount"))[
                "amount__sum"]
            if gas_bill != 0:
                gas_bill_list.append(gas_bill)

            water_bill = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success",
                                                                                         service_category="water_bill").aggregate(
                Sum("amount"))[
                "amount__sum"]
            if water_bill != 0:
                water_bill_list.append(water_bill)

            broadband_bill = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success",
                                                                                             service_category="broadband_bill").aggregate(
                Sum("amount"))[
                "amount__sum"]
            if broadband_bill != 0:
                broadband_bill_list.append(broadband_bill)

            insurance_bill = NPCI.objects.filter(created_date__startswith=final_date).filter(status="success",
                                                                                             service_category="insurance_bill").aggregate(
                Sum("amount"))[
                "amount__sum"]
            if insurance_bill != 0:
                insurance_bill_list.append(insurance_bill)
        else:
            pass

    ctx['date_list'] = last_date
    ctx['all_recharge_list'] = all_recharge_list
    ctx['mobile_recharge_list'] = mobile_recharge_list
    ctx['dth_recharge_list'] = dth_recharge_list
    ctx['postpaid_mobile_list'] = postpaid_mobile_list
    ctx['electric_bill_list'] = electric_bill_list
    ctx['landline_bill_list'] = landline_bill_list
    ctx['gas_bill_list'] = gas_bill_list
    ctx['water_bill_list'] = water_bill_list
    ctx['broadband_bill_list'] = broadband_bill_list
    ctx['insurance_bill_list'] = insurance_bill_list

    return HttpResponse(json.dumps(ctx))


def monthwise_repayment(request):
    ctx = {}
    data_dict = {}
    date_list = NPCI.objects.filter(status="success", payment_mode="credit").values_list("date", flat=True)
    # taking date from date list
    print("date_list", date_list)
    date_list = [str(i)[:7] for i in date_list]
    # taking unique dates
    date_list = list(dict.fromkeys(date_list))
    if date_list:
        for date in date_list:
            repayment = 0
            npci_obj = NPCI.objects.filter(date__startswith=date, status="success", payment_mode="credit")
            if npci_obj:
                for npci in npci_obj:
                    repayment_added = npci.credit_history.filter(status="repayment")
                    if repayment_added:
                        for repayment_1 in repayment_added:
                            repayment += repayment_1.repayment_add

            data_dict.update(
                {
                    date: repayment
                }
            )
    ctx["data"] = data_dict
    ctx["type"] = "month"
    return render(request, "Ecom/NPCI/monthwise_repayment.html", ctx)


def datewise_repayment(request, month):
    ctx = {}
    data_dict = {}
    date_list = NPCI.objects.filter(date__startswith=month, status="success", payment_mode="credit").values_list("date",
                                                                                                                 flat=True)
    # taking date from date list
    print("date_list", date_list)
    date_list = [str(i)[:10] for i in date_list]
    # taking unique dates
    date_list = list(dict.fromkeys(date_list))
    if date_list:
        for date in date_list:
            repayment = 0
            npci_obj = NPCI.objects.filter(date__startswith=date, status="success", payment_mode="credit")
            if npci_obj:
                for npci in npci_obj:
                    repayment_added = npci.credit_history.filter(status="repayment")
                    if repayment_added:
                        for repayment_1 in repayment_added:
                            repayment += repayment_1.repayment_add

            data_dict.update(
                {
                    date: repayment
                }
            )
    ctx["data"] = data_dict
    return render(request, "Ecom/NPCI/monthwise_repayment.html", ctx)



