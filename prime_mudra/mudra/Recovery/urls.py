# from django.conf.urls import url
# from django.urls import path

# from .views import *
# from django.urls import path

# urlpatterns = [
#     # auth
#     url('login/', user_login, name='Login'),
#     url('logout/', user_logout, name='Logout'),

#     # Reports
#     url('overdue_details_view/', Overdue_Details_view1.as_view(), name='Overdue_Details_view'),
#     url('overdue_details_view_status/', Overdue_Details_view_Status.as_view(), name='Overdue_Details_view_Status'),
#     url('month_wise_report/', All_Months_wise_Record.as_view(), name='month_wise_report'),

#     # Others
#     url('recovery_call_history/', recovery_call_history, name='recovery_call_history'),

#     # Dashboard
#     url('dashboard/', Dashboard_view.as_view(), name='Dashboard'),
#     url('recovery-user-all-details/', RecoveryLoanApplicationViewNew.as_view(), name='user_all_details'),
#     path(r'recovery-userloan-application-list/<slug:date>/', RecoveryLoanApplicationView.as_view(), name='recovery_userloan_application_list'),
#     path(r'user-all-details/<int:id>/', RecoveryUserAllDetails.as_view(), name='recovery_user_all_details'),
#     path(r'add-user/(?P<message>)/', AddUser.as_view(), name='Add_user'),
#     path(r'add-user/', AddUser.as_view(), name='Add_user'),
#     path(r'add-user-function/', AddUserFunction.as_view(), name='add_user_function'),


#     url('month_wise_call/', CallDetails.as_view(), name='month_wise_call'),
#     url('call_history_data_on_day/', Call_history_data_on_day.as_view(), name='Call_history_data_on_day'),
#     url('call_reminder_today/', CallReminder_Today.as_view(), name='call_reminder_today'),
#     url('months_wise_record/', Months_wise_Record.as_view(), name='Months_wise_Record'),
#     url('months_wise_record_comp/', Months_wise_Record_comp.as_view(), name='Months_wise_Record_comp'),
#     url('recovery_commission/', Recovery_Commission_view.as_view(), name='Recovery_Commission'),


#     url('incentive_details/', MonthWiseIncentive.as_view(), name='incentive_details'),

#     # charts
#     url('charts/', charts, name='charts'),
#     url('recovery_amount/', recovery_amount, name='recovery_amount'),
#     # url('getDefaulterslist_de/', getDefaulterslist_de, name='getDefaulterslist_de'),
#     url(r'^get_completed_recovery_amount/', get_completed_recovery_amount, name='get_completed_recovery_amount'),
#     url(r'^total_recovery_amount/', total_recovery_amount, name='total_recovery_amount'),
#     url(r'^total_defaulter/', total_defaulter_pie_chart, name='total_defaulter'),
#     url(r'^recovery_amount_v2/', recovery_amount2, name='recovery_amount_v2'),
#     url(r'^reset_password/', ResetPassword.as_view(), name='reset_password'),
#     url(r'^reset_password1/', ResetPassword1.as_view(), name='reset_password1'),
#     url(r'^user_active_status/', User_Active_Status.as_view(), name='user_active_status'),
#     url(r'^extra_defaulters/$', extra_defaulters, name='extra_defaulters'),

#     # Side Bar
#     url(r'^admin_self_recovery/', admin_defaulter.as_view(), name='admin_self_recovery'),

#     # Credit Verification
#     url('recovery-user-all-details/', RecoveryLoanApplicationViewNew.as_view(), name='user_all_details'),
#     path(r'recovery-userloan-application-list/<slug:date>/', RecoveryLoanApplicationView.as_view(),
#          name='recovery_userloan_application_list'),
#     path(r'user-all-details/<int:id>/', RecoveryUserAllDetails.as_view(), name='recovery_user_all_details'),
#     path(r'user_wise_loan_details/<username>/', UserWiseLoanDetailsView.as_view(), name='user_wise_loan_details'),

#     # Group
#     path(r'add-user/(?P<message>)/', AddUser.as_view(), name='Add_user'),
#     path(r'add-user/', AddUser.as_view(), name='Add_user'),
#     path(r'add-user-function/', AddUserFunction.as_view(), name='add_user_function'),

#     # Recovery Business dashboard
#     path('datewise_shared_data/', DatewiseSharedData.as_view()),
#     path('userwise_shared_data/<date>/', UserwiseSharedData.as_view()),
#     path('total_employee/', Total_employee.as_view()),
#     path('employee_details/<user>/', EmployeeDetails.as_view()),
#     path('employee_share_details/<user>/', EmployeeShareDetails.as_view()),
#     path('employee_sub_details/<user>/', EmployeeSubDetails.as_view()),
#     path('employee_notreg_details/<user>/', Employee_NotReg_Details.as_view()),
#     path('employee_app_details/<user>/', EmployeeAppDetails.as_view()),

#     # Accounts Dashboard report
#     path(r'date-report/', DateReport.as_view(), name='date_report'),
#     path(r'report-between-date/', ReportBetweenDates.as_view(), name='report-by-date'),
#     path(r'download-csv/', CsvDownload.as_view(), name='Download_CSV'),

#     path(r'user-contacts/<user>/', UserContactDetails.as_view(), name='user_contacts'),
#     path(r'multiple-defaulters/', MultilpleDefaultersAllocate.as_view(), name='multiple_defaulters'),

#     # urls for yelo leads from API
#     path(r'yelo-all-details-datewise/<month>/', YeloApplicationViewNew.as_view(), name='yelo_all_details_datewise'),
#     path(r'yelo-all-details-monthwise/', YeloMonthwise_Details.as_view(), name='yelo_all_details_datewise'),

#     path(r'yelo-paid-amount-datewise/', YeloPaidAmountView.as_view(), name='yelo_all_details_datewise'),
#     path(r'yelo-subscribed-user-datewise/', YeloSubscribedView.as_view(), name='yelo_all_details_datewise'),
#     path(r'yelo-installed-leads-datewise/', YeloInstalledLeadsView.as_view(), name='yelo_all_details_datewise'),
#     path(r'yelo-leads-datewise/<slug:date>/', YeloDatewiseLeadsView.as_view(), name='yelo_leads_datewise'),
#     path(r'yelo-subs-datewise/<slug:date>/', YeloDatewiseSubsLeadsView.as_view(), name='yelo_subs_datewis'),


#     # This urls are for DynamicDisbursed model Partner reports..
#     path('monthwise-dynamic-registered/', MonthwiseDynamicRegistered.as_view()),
#     path('daywise-dynamic-registered/<on_month>/', DaywiseDaynamicRegistered.as_view()),
#     path('dynamic-registered-ondate/<on_date>/', DaynamicRegisteredOndate.as_view()),
#     path('monthwise-dynamic-approved/', MonthwiseDynamicApproved.as_view()),
#     path('daywise-dynamic-approved/<on_month>/', DaywiseDaynamicApproved.as_view()),
#     path('dynamic-approved-ondate/<on_date>/', DaynamicApprovedOndate.as_view()),

#     # path('all_repayments', AllRepaymentData.as_view())
#     path('monthly_recovery_calls/', MonthlyRecoveryCalls.as_view()),
#     path('daywise_recovery_calls/<on_month>/', DaywiseRecoveryCalls.as_view()),
#     path('userwise_calls_ondate/<on_date>/', UserwiseRecoveryCalls.as_view()),

#     path("user_data/<username>/", user_data),


#     path('remark/', remark),
#     path('remark_view/', remark_view),
#     path('create_remark/', create_remark),
#     path('remark_update/', remark_update),

#     # defaulter installment report
#     # path('defaulter_installment_data/', defaulter_installment_data),
#     path('defaulter_installment_data/<date>/', defaulter_installment_data),
#     path('call_data/<recovery_id>/', call_data),
#     path('show_slab_data/', show_slab_data),
#     path('contact_master_data/<order_details_id>/', contact_master_data),

#     # recovery installment report
#     path('monthwise_recovery_installment_data/', monthwise_recovery_installment_data),
#     path('datewise_recovery_installment_data/<date>/', datewise_recovery_installment_data),
#     path('recovery_installment_data/<date>/', recovery_installment_data),

#     # recovery defaulter monthwise, datewise report
#     path('monthwise_defaulter_amount/', monthwise_defaulter_amount),
#     path('datewise_defaulter_amount/<date>/', datewise_defaulter_amount),

#     # recovery amount monthwise, datewise report
#     path('recovery_monthwise/', recovery_monthwise),
#     path('recovery_datewise/<date>/', recovery_datewise),
#     path('show_recovery_datewise/<date>/', show_recovery_datewise),

#     # insentive topper report
#     path("recovery_topper_all_data/", recovery_topper_all_data),

#     # insentive amount report
#     path('monthwise_incentive_data/', monthwise_incentive_data),
#     path('datewise_incentive_data/<date>/', datewise_incentive_data),
#     path("insentive_data/<date>/", insentive_data),

#     path("get_recovery_team_data/", get_recovery_team_data),
#     path("get_assign_list/", get_assign_list),
#     path("assign_already_defaulters/", assign_already_defaulters),
#     path("create_already_present_order_entry/", create_already_present_order_entry),

#     # call topper
#     path("call_topper_data/", call_topper_data),
#     path("call_all_data/", call_all_data),
#     path("year_wise_count/<user>/", year_wise_count),
#     path("month_wise_count/<year>/<user>/", month_wise_count),
#     path("date_wise_count/<month>/<user>/", date_wise_count),

#     # move defaulter
#     path('move_defaulters/', move_defaulters),
#     path('assign_move_defaulters/', assign_move_defaulters),

#     #User Active Deactive
#     path("user_status/", user_status),
#     path("user_status_update/", user_status_update),

#     #incentive user wise
#     path("user_wise_insentive_data/", user_wise_insentive_data),
#     path("year_insentive/<username>/", year_insentive),
#     path("month_insentive/<year>/<username>/", month_insentive),
#     path("date_insentive/<month>/<username>/", date_insentive),

#     path("year_wise_insentive_data/", year_wise_insentive_data),
#     path("month_wise_insentive_data/<year>/", month_wise_insentive_data),
#     path("date_wise_insentive_data/<month>/", date_wise_insentive_data),

#     # Change Password
#     path('reset_password/', ResetPassword.as_view()),

#     # change recovery status
#     url('change_recovery_status/', change_recovery_status),
#     url('recovery_call_change_status/', recovery_call_change_status, name='recovery_call_change_status'),

#     #commission
#     path('commission/', commission),
#     path('commission_view/', commission_view),
#     path('create_commission/', create_commission),
#     path('commission_update/', commission_update),

#     #self recovery
#     path('monthwise_self_recovery_data/', monthwise_self_recovery_data),
#     path('datewise_self_recovery_data/<month>/', datewise_self_recovery_data),
#     path('self_recovery_data/<date>/', self_recovery_data),

#     # charts
#     path('incentive_pie_chart/', incentive_pie_chart),
#     path('recovery_pie_chart/', recovery_pie_chart),
#     path('recovery_count_chart/', recovery_count_chart),

#     path("monthwise_graph/", monthwise_graph),

#     #defaulter report
#     path('user_wise_defaulter/', user_wise_defaulter),
#     path('month_wise_defaulter/<user>/', month_wise_defaulter),
#     path('date_wise_defaulter/<user>/<month>/', date_wise_defaulter),

#     path('month_wise_defaulter_data/', month_wise_defaulter_data),
#     path('date_wise_defaulter_data/<month>/', date_wise_defaulter_data),

#     url('recovery_count/', recovery_count, name='recovery_count'),
#     url('incentive_amount/', incentive_amount, name='incentive_amount'),

# ]

from django.conf.urls import url
from django.urls import path

from .views import *
from django.urls import path

# from LoanAppData.views import get_subscription_payment_data

urlpatterns = [
    # auth
    url('login/', user_login, name='Login'),
    url('logout/', user_logout, name='Logout'),

    # Reports
    url('overdue_details_view/', Overdue_Details_view1.as_view(), name='Overdue_Details_view'),
    url('overdue_details_view_status/', Overdue_Details_view_Status.as_view(), name='Overdue_Details_view_Status'),
    url('month_wise_report/', All_Months_wise_Record.as_view(), name='month_wise_report'),

    # Others
    url('recovery_call_history/', recovery_call_history, name='recovery_call_history'),

    # Dashboard
    url('dashboard/', Dashboard_view.as_view(), name='Dashboard'),
    # url('recovery-user-all-details/<status>/', RecoveryLoanApplicationViewNew.as_view(), name='user_all_details'),
    # path(r'recovery-userloan-application-list/<slug:date>/', RecoveryLoanApplicationView.as_view(),
    #      name='recovery_userloan_application_list'),
    path(r'user-all-details/<int:id>/', RecoveryUserAllDetails.as_view(), name='recovery_user_all_details'),
    path(r'add-user/(?P<message>)/', AddUser.as_view(), name='Add_user'),
    path(r'add-user/', AddUser.as_view(), name='Add_user'),
    path(r'add-user-function/', AddUserFunction.as_view(), name='add_user_function'),

    url('change_recovery_status/', ChangeStatusRecovery.as_view(), name='change_recovery_status'),
    url('recovery_call_change_status/', recovery_call_change_status, name='recovery_call_change_status'),
    url('month_wise_call/', CallDetails.as_view(), name='month_wise_call'),
    url('call_history_data_on_day/', Call_history_data_on_day.as_view(), name='Call_history_data_on_day'),
    url('call_reminder_today/', CallReminder_Today.as_view(), name='call_reminder_today'),
    url('months_wise_record/', Months_wise_Record.as_view(), name='Months_wise_Record'),
    url('months_wise_record_comp/', Months_wise_Record_comp.as_view(), name='Months_wise_Record_comp'),
    url('recovery_commission/', Recovery_Commission_view.as_view(), name='Recovery_Commission'),
    url('remark/', Remark_view.as_view(), name='Remark_view'),
    url('auto_defaulter_allocate/', Defaulter_Auto_Allocate_view.as_view(), name='Auto_defaulter_allocate'),
    url('auto_defaulter_allocate_static/', Defaulter_Auto_Allocate_view_static.as_view(),
        name='Auto_defaulter_allocate_static'),
    url('incentive_details/', MonthWiseIncentive.as_view(), name='incentive_details'),
    path('unallocated_defaulter_list', UnallocatedDefaulterList.as_view(), name='unallocated_defaulter'),

    # charts
    url('charts/', charts, name='charts'),
    url('recovery_amount/', recovery_amount, name='recovery_amount'),
    # url('getDefaulterslist_de/', getDefaulterslist_de, name='getDefaulterslist_de'),
    url(r'^get_completed_recovery_amount/', get_completed_recovery_amount, name='get_completed_recovery_amount'),
    url('distrbution_amount_update/', distrbution_amount_update, name='distrbution_amount_update'),
    url(r'^total_recovery_amount/', total_recovery_amount, name='total_recovery_amount'),
    url(r'^total_defaulter/', total_defaulter_pie_chart, name='total_defaulter'),
    url(r'^recovery_amount_v2/', recovery_amount2, name='recovery_amount_v2'),
    url(r'^reset_password/', ResetPassword.as_view(), name='reset_password'),
    url(r'^reset_password1/', ResetPassword1.as_view(), name='reset_password1'),
    url(r'^user_active_status/', User_Active_Status.as_view(), name='user_active_status'),
    url(r'^extra_defaulters/$', extra_defaulters, name='extra_defaulters'),

    # Side Bar
    url(r'^admin_self_recovery/', admin_defaulter.as_view(), name='admin_self_recovery'),
    url('distribution_limit_amount/', Distribution_Limit_Amount_view.as_view(), name='Distribution_Limit_Amount'),

    # Credit Verification
    path('recovery-user-all-details/<status>/', RecoveryLoanApplicationViewNew.as_view(),
         name='recovery_loan_application_list'),
    path('recovery-userloan-application-list/<slug:date>/<status>/', RecoveryLoanApplicationView.as_view(),
         name='recovery_userloan_application_list'),
    path(r'user-all-details/<int:id>/', RecoveryUserAllDetails.as_view(), name='recovery_user_all_details'),
    path(r'user_wise_loan_details/<username>/', UserWiseLoanDetailsView.as_view(), name='user_wise_loan_details'),
    path(r'rbl_save_data_process/', rbl_save_data_process, name='userloan_details_mobile'),

    # Group
    path(r'add-user/(?P<message>)/', AddUser.as_view(), name='Add_user'),
    path(r'add-user/', AddUser.as_view(), name='Add_user'),
    path(r'add-user-function/', AddUserFunction.as_view(), name='add_user_function'),

    # Recovery Business dashboard
    path('datewise_shared_data/', DatewiseSharedData.as_view()),
    path('userwise_shared_data/<date>/', UserwiseSharedData.as_view()),
    path('total_employee/', Total_employee.as_view()),
    path('employee_details/<user>/', EmployeeDetails.as_view()),
    path('employee_share_details/<user>/', EmployeeShareDetails.as_view()),
    path('employee_sub_details/<user>/', EmployeeSubDetails.as_view()),
    path('employee_notreg_details/<user>/', Employee_NotReg_Details.as_view()),
    path('employee_app_details/<user>/', EmployeeAppDetails.as_view()),

    # Accounts Dashboard report
    path(r'date-report/', DateReport.as_view(), name='date_report'),
    path(r'report-between-date/', ReportBetweenDates.as_view(), name='report-by-date'),
    path(r'download-csv/', CsvDownload.as_view(), name='Download_CSV'),
    # path('get_subscription_payment_data/', get_subscription_payment_data),

    path(r'user-contacts/<user>/', UserContactDetails.as_view(), name='user_contacts'),
    path(r'multiple-defaulters/', MultilpleDefaultersAllocate.as_view(), name='multiple_defaulters'),

    # urls for yelo leads from API
    path(r'yelo-all-details-datewise/<month>/', YeloApplicationViewNew.as_view(), name='yelo_all_details_datewise'),
    path(r'yelo-all-details-monthwise/', YeloMonthwise_Details.as_view(), name='yelo_all_details_datewise'),

    path(r'yelo-paid-amount-datewise/', YeloPaidAmountView.as_view(), name='yelo_all_details_datewise'),
    path(r'yelo-subscribed-user-datewise/', YeloSubscribedView.as_view(), name='yelo_all_details_datewise'),
    path(r'yelo-installed-leads-datewise/', YeloInstalledLeadsView.as_view(), name='yelo_all_details_datewise'),
    path(r'yelo-leads-datewise/<slug:date>/', YeloDatewiseLeadsView.as_view(), name='yelo_leads_datewise'),
    path(r'yelo-subs-datewise/<slug:date>/', YeloDatewiseSubsLeadsView.as_view(), name='yelo_subs_datewis'),

    # This urls are for DynamicDisbursed model Partner reports..
    path('monthwise-dynamic-registered/', MonthwiseDynamicRegistered.as_view()),
    path('daywise-dynamic-registered/<on_month>/', DaywiseDaynamicRegistered.as_view()),
    path('dynamic-registered-ondate/<on_date>/', DaynamicRegisteredOndate.as_view()),
    path('monthwise-dynamic-approved/', MonthwiseDynamicApproved.as_view()),
    path('daywise-dynamic-approved/<on_month>/', DaywiseDaynamicApproved.as_view()),
    path('dynamic-approved-ondate/<on_date>/', DaynamicApprovedOndate.as_view()),

    # fcm test
    path('notification_list/', notification_list),
    path('create_notification/', CreateNotification.as_view()),
    path('edit_notification/<id>/', EditNotification.as_view()),
    path('send_notification/', send_notification),
    path('create_notification_dropdown/', create_notification_dropdown),

    #Allocate Def Function for recovery
    path('allocate_def/', allocate_def)
 
]


