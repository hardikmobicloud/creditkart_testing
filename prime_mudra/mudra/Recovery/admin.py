from django.contrib import admin
# from django.apps import apps
# from .models import *


# # Register your models here.


class RecoveryAdmin(admin.ModelAdmin):
    list_display = (
        'user', 'order_details', 'installment', 'pending_installment_amount', 'recovery_status', 'created_date',
        'completed_date')
    list_filter = ('recovery_status', 'installment')
    search_fields = ('user__username', 'order_details__order_text')


# admin.site.register(Recovery, RecoveryAdmin)


# class RecoveryCommissionAdmin(admin.ModelAdmin):
#     list_display = ('commission_percent', 'status', 'created_date')
#     list_filter = ('commission_percent', 'status')
#     search_fields = ['commission_percent']


# admin.site.register(RecoveryCommission, RecoveryCommissionAdmin)


# class RecoveryInsentiveAdmin(admin.ModelAdmin):
#     list_display = ('recovery', 'payment', 'recovery_commission', 'insentive_amount', 'status', 'created_date')
#     list_filter = ('recovery', 'status')
#     search_fields = ['recovery_commission']


# admin.site.register(RecoveryInsentive, RecoveryInsentiveAdmin)


# class RemarkAdmin(admin.ModelAdmin):
#     list_display = ('remark_title', 'remark_desc', 'status', 'created_date')
#     list_filter = ('remark_title', 'remark_desc')
#     search_fields = ['status']


# admin.site.register(Remark, RemarkAdmin)


# class RecoveryCallHistoryAdmin(admin.ModelAdmin):
#     list_display = ('recovery', 'calling_status', 'remark', 'next_call_time', 'next_call_actual_Datetime',
#                     'call_desc',
#                     'next_call_status', 'expected_payment_date')
#     list_filter = ('recovery', 'remark')
#     search_fields = ['calling_status']


# admin.site.register(RecoveryCallHistory, RecoveryCallHistoryAdmin)

from django.contrib import admin
from .models import *


# class RecoveryAdmin(admin.ModelAdmin):
#     list_display = ('user', 'loan_transaction', 'recovery_status', 'insentive_amount', 'completed_date', 'created_date')
#     list_filter = ('recovery_status', 'completed_date', 'created_date')
#     search_fields = ('user__username', 'loan_transaction__loan_application_id',)


admin.site.register(Recovery, RecoveryAdmin)


class NotificationTemplateAdmin(admin.ModelAdmin):
    list_display = ('name', 'title', 'path', 'status', 'type', 'description', 'created_date')
    list_filter = ('name', 'title', 'status', 'type', 'created_date')
    search_fields = ('name', 'title',)


admin.site.register(NotificationTemplate, NotificationTemplateAdmin)

class RecoveryCommissionAdmin(admin.ModelAdmin):
    # list_display = ('__all__')
    list_display = ('commission_percent', 'status', 'created_date')

    # list_filter = ('name', 'title', 'status', 'type', 'created_date')
    # search_fields = ('name', 'title',)


admin.site.register(RecoveryCommission, RecoveryCommissionAdmin)

class NotificationTaskAdmin(admin.ModelAdmin):
    list_display = ('notification_template', 'total_count', 'delivered_count', 'status', 'remark', 'created_date')
    list_filter = (
        'notification_template', 'total_count', 'delivered_count', 'status', 'remark', 'created_date'
    )
    search_fields = ('notification_template',)


admin.site.register(NotificationTask, NotificationTaskAdmin)
