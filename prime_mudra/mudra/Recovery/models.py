from django.db import models
from django.contrib.auth.models import User
from ecom.models import OrderDetails
from LoanAppData.models import LoanApplication
from Payments.models import Payment
from mudra import constants
# from django.db.models.signals import pre_save
# from LoanAppData.models import LoanApplication


class Recovery(models.Model):
    """
    Model to store recovery details
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    order_details = models.ForeignKey(OrderDetails, on_delete=models.CASCADE, blank=True, null=True)
    loan_transaction = models.ForeignKey(LoanApplication, on_delete=models.CASCADE, blank=True, null=True)
    created_date = models.DateTimeField(db_index=True, auto_now_add=True)
    completed_date = models.DateTimeField(db_index=True, null=True, blank=True)
    installment = models.CharField(max_length=20, null=True, blank=True, choices=constants.INSTALLMENT_TYPE)
    pending_installment_amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    recovery_status = models.CharField(db_index=True, max_length=256, default='No', blank=True,
                                       null=True)  # Yes , No
    type = models.CharField(max_length=20, null=True, blank=True, choices=constants.RECOVERY_TYPE, default='Ecom')
    insentive_amount = models.FloatField(db_index=True, null=True, blank=True)
    class Meta:
        ordering = ['-created_date']


# class RecoveryCommission(models.Model):
#     """
#     Model for remoark template
#     """
#     commission_percent = models.CharField(max_length=1000)
#     status = models.CharField(default='Active', max_length=10)
#     created_date = models.DateTimeField(auto_now_add=True)

#     class Meta:
#         ordering = ['-created_date']




# class Remark(models.Model):
#     """
#     Model for remoark template
#     """
#     remark_title = models.CharField(max_length=500)
#     remark_desc = models.CharField(max_length=1000)
#     status = models.CharField(default='Active', max_length=10)
#     created_date = models.DateTimeField(auto_now_add=True)

#     class Meta:
#         ordering = ['-created_date']


# class RecoveryCallHistory(models.Model):
#     """
#     Model to store recovery history
#     """
#     recovery = models.ForeignKey(Recovery, on_delete=models.CASCADE, related_name='recovery_history')
#     created_date = models.DateTimeField(db_index=True, auto_now_add=True)
#     calling_status = models.CharField(default='Yes', max_length=256, blank=True, null=True)
#     remark = models.ForeignKey(Remark, on_delete=models.CASCADE)
#     next_call_time = models.TimeField(blank=True, null=True)
#     next_call_actual_Datetime = models.DateTimeField(blank=True, null=True)
#     call_desc = models.CharField(max_length=250, blank=True, null=True)
#     next_call_status = models.BooleanField(default=0)
#     expected_payment_date = models.DateTimeField(null=True, blank=True)

#     class Meta:
#         ordering = ['-created_date']


# class DistributionLimitAmount(models.Model):
#     """
#     Model for remoark template
#     """
#     amount = models.IntegerField()
#     status = models.CharField(default='Active', max_length=10)
#     created_date = models.DateTimeField(auto_now_add=True)

#     class Meta:
#         ordering = ['-created_date']


from django.db import models
from django.contrib.auth.models import User
from LoanAppData.models import LoanApplication
from django.db.models.signals import pre_save
from mudra import constants


# class Recovery(models.Model):
#     """
#     Model to store recovery details
#     """
#     user = models.ForeignKey(User, on_delete=models.CASCADE)
#     loan_transaction = models.ForeignKey(LoanApplication, on_delete=models.CASCADE, blank=True, null=True)
#     created_date = models.DateTimeField(db_index=True, auto_now_add=True)
#     completed_date = models.DateTimeField(db_index=True, null=True, blank=True)
#     insentive_amount = models.FloatField(db_index=True, null=True, blank=True)
#     recovery_status = models.CharField(db_index=True, max_length=256, default='No', blank=True,
#                                        null=True)  # Yes ,No,Forworded

#     class Meta:
#         ordering = ['-created_date']


class Remark(models.Model):
    """
    Model for remoark template
    """
    remark_title = models.CharField(max_length=500)
    remark_desc = models.CharField(max_length=1000)
    status = models.CharField(default='Active', max_length=10)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_date']


class DistributionLimitAmount(models.Model):
    """
    Model for remoark template
    """
    amount = models.IntegerField()
    status = models.CharField(default='Active', max_length=10)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_date']


class RecoveryCommission(models.Model):
    """
    Model for remoark template
    """
    commission_percent = models.CharField(max_length=1000)
    status = models.CharField(default='Active', max_length=10)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_date']


class RecoveryCallHistory(models.Model):
    """
    Model to store recovery history
    """
    recovery = models.ForeignKey(Recovery, on_delete=models.CASCADE, related_name='recovery_history')
    created_date = models.DateTimeField(db_index=True, auto_now_add=True)
    calling_status = models.CharField(default='Yes', max_length=256, blank=True, null=True)
    remark = models.ForeignKey(Remark, on_delete=models.CASCADE)
    next_call_time = models.TimeField(blank=True, null=True)
    next_call_actual_Datetime = models.DateTimeField(blank=True, null=True)
    call_desc = models.CharField(max_length=250, blank=True, null=True)
    next_call_status = models.BooleanField(default=0)
    expected_payment_date = models.DateTimeField(null=True, blank=True)

    class Meta:
        ordering = ['-created_date']


# Fcm Models

class NotificationTemplate(models.Model):
    name = models.CharField(max_length=250, null=True, blank=True)
    title = models.CharField(max_length=250, null=True, blank=True)
    path = models.ImageField(upload_to='fcm_images', max_length=256, null=True, blank=True, db_index=True)
    status = models.BooleanField(default=True)
    type = models.CharField(max_length=25, null=True, blank=True, choices=constants.USER_OFFER_TYPE)
    description = models.CharField(max_length=256, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_notification_created_by",
                                   null=True,
                                   blank=True)
    updated_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_notification_updated_by",
                                   null=True,
                                   blank=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class NotificationTask(models.Model):
    total_count = models.CharField(max_length=250, null=True, blank=True)
    delivered_count = models.CharField(max_length=250, null=True, blank=True)
    status = models.BooleanField(default=True)
    remark = models.CharField(max_length=250, null=True, blank=True)
    notification_template = models.ForeignKey(NotificationTemplate, on_delete=models.CASCADE,
                                              related_name="notification_template", blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_notification_task_created_by",
                                   null=True,
                                   blank=True)
    updated_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_notification_task_updated_by",
                                   null=True,
                                   blank=True)

    def __str__(self):
        return '%s %s' % (self.notification_template, self.status)

class RecoveryInsentive(models.Model):
    """
    Model to store recovery insentive amount
    """
    recovery = models.ForeignKey(Recovery, on_delete=models.CASCADE, related_name='recovery_data')
    payment = models.ForeignKey(Payment, related_name="recovery_payment", on_delete=models.CASCADE, null=True, blank=True)
    recovery_commission = models.ForeignKey(RecoveryCommission, on_delete=models.CASCADE, blank=True, null=True)
    insentive_amount = models.FloatField(db_index=True, null=True, blank=True)
    status = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
