import json
from django.contrib.auth.models import User
from django.apps.registry import apps
from django.shortcuts import render, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from datetime import datetime


from LoanAppData.loan_funcations import loan_amount_slab
from LoanAppData.loan_funcations import check_loan_default

Recovery = apps.get_model('Recovery', 'Recovery')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')


def get_recovery_team_data_for_loan(request):
    """
    get recovery team member names and default loan
    """
    ctx = {}
    already_exist = []
    default_loan = []

    recovery_team = User.objects.filter(groups__name="Recovery Team").values_list("username", flat=True)
    if recovery_team:
        ctx['recovery_team'] = recovery_team
    # get all default order details
    default_loan = check_loan_default()
    if default_loan:
        # check loan already exist in recovery table
        already_exist = Recovery.objects.filter(loan_application_id__in=default_loan, type='Loan').values_list(
            "loan_application_id__id", flat=True)

    # get loan are not in recovery table
    assign_list = default_loan - set(already_exist)
    if assign_list:
        loan_obj = LoanApplication.objects.filter(id__in=assign_list)
        if loan_obj:
            ctx['loan_obj'] = loan_obj

    return render(request, "recovery/Loan/assign_loan_defaulter.html", ctx)


@csrf_exempt
@login_required(login_url="/recovery/login/")
def get_assign_loan_list(request):
    """
    assign loan defaulter to recovery team member
    """
    ctx = {}
    msg = "something Went Wrong"
    today = datetime.now()
    today_date = str(today)[:11]
    if request.method == "POST":
        name = request.POST['member']
        loan_id_list = request.POST.getlist('id_list[]')
        user_name = User.objects.filter(username=name)
        if user_name:
            if loan_id_list:
                loan_obj = LoanApplication.objects.filter(id__in=loan_id_list)
                if loan_obj:
                    for loan in loan_obj:
                        slab_list = loan_amount_slab(loan.id)
                        if slab_list:
                            installment = 0
                            for data in slab_list:
                                installment += 1
                                if data['pay_status'] == 'unpaid' and today_date > data['date']:
                                    pending_installment_amount = data['remaining_amount']
                                    create_entry = Recovery.objects.create(user=user_name[0],
                                                                           installment=installment,
                                                                           pending_installment_amount=pending_installment_amount,
                                                                           loan_application_id=loan, type="Loan",
                                                                           created_date=today
                                                                           )
                        msg = "Success"
        else:
            msg = "Team Member Not Exist"
    ctx['msg'] = msg
    return HttpResponse(json.dumps(ctx))

@login_required(login_url="/recovery/login/")
def assign_already_loan_defaulters(request):
    """
    show already assign defaulter to other recovery member
    """
    ctx = {}
    all_installement_already_assign = Recovery.objects.filter(recovery_status="No", installment=3).values_list('loan_application_id', flat=True)
    all_installemnt = Recovery.objects.filter(recovery_status="No").values_list('loan_application_id', flat=True)
    remaining = list(set(all_installemnt) - set(all_installement_already_assign))
    recovery_obj = Recovery.objects.filter(loan_application_id__in=remaining).order_by('loan_application_id')
    if recovery_obj:
        ctx['recovery_obj'] = recovery_obj
    return render(request, 'recovery/Loan/assign_already_loan_defaulters.html', ctx)


@csrf_exempt
@login_required(login_url="/recovery/login/")
def create_already_present_loan_entry(request):
    """
    assign defaulters to other recovery team member
    """
    ctx = {}
    msg = "something Went Wrong"
    today = datetime.now()
    today_date = str(today)[:11]
    loan_application_id_list = Recovery.objects.values_list("loan_application_id__id", flat=True)
    if loan_application_id_list:
        loan_obj = LoanApplication.objects.filter(id__in=loan_application_id_list)
        if loan_obj:
            for loan in loan_obj:
                slab_list = loan_amount_slab(loan.id)
                if slab_list:
                    installment = 0
                    for data in slab_list:
                        installment += 1
                        if data['pay_status'] == 'unpaid' and today_date > data['date']:
                            already_entry = Recovery.objects.filter(installment__gte=installment,
                                                                    loan_application_id=loan_obj[0])
                            if already_entry:
                                msg = "success"
                                pass
                            else:
                                pending_installment_amount = data['remaining_amount']
                                assign_team_member = Recovery.objects.filter(loan_application_id=loan_obj[0])
                                if assign_team_member:
                                    user_name = User.objects.filter(id=assign_team_member[0].user.id)
                                    if user_name:
                                        create_entry = Recovery.objects.create(user=user_name[0],
                                                                               installment=installment,
                                                                               pending_installment_amount=pending_installment_amount,
                                                                               loan_application_id=loan_obj[0],
                                                                               created_date=today)
                                        msg = "success"

    ctx['msg'] = msg
    return HttpResponse(json.dumps(ctx))

