import json
import os
import sys
from datetime import datetime, timedelta
from functools import reduce
from django.db.models import Count
import pandas as pd
from IcomeExpense.models import SmsStringMaster
from LoanAppData.views import loan_application_dpd, PancardMatch, get_reference_details
from MApi import botofile
from MApi.email import email, sms
from django.apps.registry import apps
from django.conf import settings
from fcm_django.models import FCMDevice

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.contrib.auth.models import Group
from .models import *
from django.http import JsonResponse, HttpResponseRedirect
from django.contrib.auth.models import User, Permission
from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as auth_login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.contrib.sites.models import Site
from django.db.models import Count
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, HttpResponse, redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import generic
from mudra.constants import scheme
# from Recovery.models import Recovery
from crif.models import CrifIndvMaster, CrifScoreDetails
from .models import *
from MApi import botofile

RBLTransaction = apps.get_model('Payments', 'RBLTransaction')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
Reference = apps.get_model('User', 'Reference')
Bank = apps.get_model('User', 'Bank')
Payments = apps.get_model('Payments', 'Payment')
AppVersionCheck = apps.get_model('StaticData', 'AppVersionCheck')
Personal = apps.get_model('User', 'Personal')
Address = apps.get_model('User', 'Address')
contactmaster = apps.get_model('UserData', 'contactmaster')
Document = apps.get_model('User', 'Documents')
Professional = apps.get_model('User', 'Professional')
PincodeMaster = apps.get_model('StaticData', 'PincodeMaster')
AutoApproved = apps.get_model('LoanAppData', 'AutoApproved')
LoanAcknowledgement = apps.get_model('LoanAppData', 'LoanAcknowledgement')
DeviceData = apps.get_model('UserData', 'DeviceData')
Payment = apps.get_model('Payments', 'payment')
# Recovery = apps.get_model('Recovery', 'Recovery')
PartnerLeads = apps.get_model('lead', 'PartnerLeads')
Affiliates = apps.get_model('lead', 'Affiliates')
YeloLeads = apps.get_model('lead', 'YeloLeads')
Partners = apps.get_model('lead', 'Partners')

ShareAppMaster = apps.get_model('UserData', 'ShareAppMaster')
UserSharedData = apps.get_model('UserData', 'UserSharedData')
EquiFaxMaster = apps.get_model('Equfix', 'EquiFaxMaster')
EquiFaxScoreDetails = apps.get_model('Equfix', 'EquiFaxScoreDetails')
DynamicDisbursement = apps.get_model('lead', 'DynamicDisbursement')

from django.db.models import Sum


def user_login(request):
    template_name = "recovery/index.html"
    context = {}
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        print(user, 'user auth')
        if user:
            group_user = user.groups.filter(name='Customer Executive').exists()
            if group_user:
                auth_login(request, user)

                return HttpResponseRedirect(reverse('Customer_Dashboard'))
            else:
                auth_login(request, user)
                return HttpResponseRedirect(reverse('Dashboard'))
        else:
            context['error'] = "Provide valid Username and password"
            return render(request, template_name, context)
    return render(request, template_name, context)


def success(request):
    context = {}
    context['user'] = request.user
    return render(request, "success.html", context)


def user_logout(request):
    if request.method == "POST":
        logout(request)
        return HttpResponseRedirect(reverse('Login'))


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class DefaultersRecoveredDatewise(generic.ListView):
    template_name = 'recovery/total_recovery_on_date.html'

    def get(self, request, on_day, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            m = None
            total = 0
            date_list = on_day.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
            new_date = '{}-{}'.format(y, m)
            recovery_data = Recovery.objects.select_related("loan_transaction").filter(
                completed_date__startswith=new_date).filter(recovery_status='Yes')

            for user in recovery_data:
                total += int(user.loan_transaction.loan_scheme_id)
                data.update({user.id: [user.loan_transaction.user_personal.name, user.loan_transaction.mobile_number,
                                       user.loan_transaction.loan_application_id,
                                       user.loan_transaction.loan_scheme_id, user.insentive_amount,
                                       user.recovery_status]})

            ctx['data'] = data
            ctx['total'] = total
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class AdminDefaultersRecoveredDatewise(generic.ListView):
    template_name = 'admin/UserAccounts/admin_total_recovery_on_date.html'

    def get(self, request, on_day, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            m = None
            total = 0
            date_list = on_day.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
            new_date = '{}-{}'.format(y, m)
            # print('new date', new_date)
            recovery_data = Recovery.objects.select_related("loan_transaction").filter(
                completed_date__startswith=new_date).filter(recovery_status='Yes')

            for user in recovery_data:
                total += int(user.loan_transaction.loan_scheme_id)
                data.update({user.id: [user.loan_transaction.user_personal.name, user.loan_transaction.mobile_number,
                                       user.loan_transaction.loan_application_id,
                                       user.loan_transaction.loan_scheme_id, user.insentive_amount,
                                       user.recovery_status]})

            ctx['data'] = data
            ctx['total'] = total
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class RecoveryByTeamOnMonth(generic.ListView):
    template_name = 'recovery/recovery_by_team_on_month.html'

    def get(self, request, on_month, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            date_list = on_month.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])

            new_date = '{}-{}'.format(y, m)
            recovery_data = Recovery.objects.select_related("loan_transaction").filter(
                completed_date__startswith=new_date).filter(recovery_status='Yes')
            recovery_info_df = read_frame(recovery_data)
            no_of_defaulter = recovery_info_df["recovery_status"] == "No"
            no_of_recovered = recovery_info_df["recovery_status"] == "Yes"
            monthwise_assign_defaulters = \
                recovery_info_df.where(no_of_defaulter).groupby(recovery_info_df['user'])[
                    'loan_transaction'].count().sort_index(axis=0)
            month_wise_incentive = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['user'])[
                    'insentive_amount'].sum().sort_index(axis=0)
            month_wise_recovery_count = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['user'])[
                    'insentive_amount'].count().sort_index(axis=0)
            monthwise_assign_defaulters_dict = dict(monthwise_assign_defaulters)
            month_wise_incentive_dict = dict(month_wise_incentive)
            month_wise_recovery_count_dict = dict(month_wise_recovery_count)
            keys = monthwise_assign_defaulters_dict.keys()
            data = {key: [monthwise_assign_defaulters_dict.get(key), month_wise_incentive_dict.get(key),
                          month_wise_recovery_count_dict.get(key)] for key in keys}
            ctx['data'] = data
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class UserwiseDefaulters(generic.ListView):
    template_name = 'recovery/userwise_defaulters.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            recovery_data = set(Recovery.objects.values_list('user__username', flat=True))
            # print('recovery_data', recovery_data)
            for user in recovery_data:
                # print('user', user)
                defaulter_count = Recovery.objects.filter(user__username=user,
                                                          recovery_status="No").count()
                total_count = Recovery.objects.filter(user__username=user).count()
                recovered_count = Recovery.objects.filter(user__username=user,
                                                          recovery_status="Yes").count()
                count_list = [defaulter_count, total_count, recovered_count]
                # print('defaulter_count', defaulter_count)
                data.update({user: count_list})
            ctx['data'] = data
            # print('data', data)

            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class AdminRecoveryByTeamOnMonth(generic.ListView):
    template_name = 'admin/UserAccounts/admin_recovery_by_team_on_month.html'

    def get(self, request, on_month, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            date_list = on_month.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])

            new_date = '{}-{}'.format(y, m)
            recovery_data = Recovery.objects.select_related("loan_transaction").filter(
                completed_date__startswith=new_date).filter(recovery_status='Yes')
            recovery_info_df = read_frame(recovery_data)
            no_of_defaulter = recovery_info_df["recovery_status"] == "No"
            no_of_recovered = recovery_info_df["recovery_status"] == "Yes"
            monthwise_assign_defaulters = \
                recovery_info_df.where(no_of_defaulter).groupby(recovery_info_df['user'])[
                    'loan_transaction'].count().sort_index(axis=0)
            month_wise_incentive = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['user'])[
                    'insentive_amount'].sum().sort_index(axis=0)
            month_wise_recovery_count = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['user'])[
                    'insentive_amount'].count().sort_index(axis=0)
            monthwise_assign_defaulters_dict = dict(monthwise_assign_defaulters)
            month_wise_incentive_dict = dict(month_wise_incentive)
            month_wise_recovery_count_dict = dict(month_wise_recovery_count)
            keys = monthwise_assign_defaulters_dict.keys()
            data = {key: [monthwise_assign_defaulters_dict.get(key), month_wise_incentive_dict.get(key),
                          month_wise_recovery_count_dict.get(key)] for key in keys}
            ctx['data'] = data
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class DayRecovery(generic.ListView):
    template_name = 'recovery/day_recovery.html'

    def get(self, request, on_day, *args, **kwargs):
        ctx = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Team", id=request.user.id, is_active='t')
            user_id = request.user.id
            # ctx['user_group'] = user_group
            date_list = on_day.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
            if len(date_list[2]) > 1:
                d = date_list[2]
            else:
                d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)
            total = 0
            data = {}
            if user_group:
                recovery_data = RecoveryCallHistory.objects.filter(
                    created_date__startswith=new_date, calling_status='Yes', recovery__user=user_id)
                ctx['data'] = recovery_data
                # print('-------------------', ctx['data'])
                return render(request, self.template_name, ctx)
            else:
                # print('inside else-------------')
                recovery_data = RecoveryCallHistory.objects.filter(
                    created_date__startswith=new_date, calling_status='Yes')
                ctx['data'] = recovery_data
                # print('-------------------', ctx['data'])
                return render(request, self.template_name, ctx)

        except Exception as e:
            # print(e)
            # print("Ecx----", e)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            # print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class AdminDayRecovery(generic.ListView):
    template_name = 'admin/UserAccounts/admin_day_recovery.html'

    def get(self, request, on_day, *args, **kwargs):
        ctx = {}
        try:
            # user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            # ctx['user_group'] = user_group
            date_list = on_day.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
            if len(date_list[2]) > 1:
                d = date_list[2]
            else:
                d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)
            total = 0
            data = {}
            # print('new_date', new_date)
            recovery_data = Recovery.objects.filter(
                completed_date__startswith=new_date).filter(recovery_status='Yes')

            ctx['data'] = recovery_data
            return render(request, self.template_name, ctx)
        except Exception as e:
            # print(e)
            # print("Ecx----", e)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            # print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class All_Day_wise_Record(generic.ListView):
    template_name = 'recovery/all_day_wise.html'

    def get(self, request, on_month, *args, **kwargs):
        ctx = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            date_list = on_month.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
            on_month = '{}-{}'.format(y, m)
            recovery_info = Recovery.objects.values_list('created_date', 'loan_transaction', 'user_id',
                                                         'insentive_amount', 'recovery_status',
                                                         'completed_date').filter(completed_date__startswith=on_month)
            recovery_info_df = read_frame(recovery_info)
            no_of_defaulter = recovery_info_df["recovery_status"] == "No"
            no_of_recovered = recovery_info_df["recovery_status"] == "Yes"
            recovery_info_df['CreatedDate'] = pd.to_datetime(recovery_info_df['completed_date']).apply(
                lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
            monthwise_assign_defaulters = \
                recovery_info_df.where(no_of_defaulter).groupby(recovery_info_df['CreatedDate'])[
                    'loan_transaction'].count().sort_index(axis=0)
            month_wise_incentive = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['CreatedDate'])[
                    'insentive_amount'].sum().sort_index(axis=0)
            month_wise_recovery_count = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['CreatedDate'])[
                    'insentive_amount'].count().sort_index(axis=0)
            monthwise_assign_defaulters_dict = dict(monthwise_assign_defaulters)
            month_wise_incentive_dict = dict(month_wise_incentive)
            month_wise_recovery_count_dict = dict(month_wise_recovery_count)
            keys = monthwise_assign_defaulters_dict.keys()
            data = {key: [monthwise_assign_defaulters_dict.get(key), month_wise_incentive_dict.get(key),
                          month_wise_recovery_count_dict.get(key)] for key in keys}
            ctx['data'] = data
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class Admin_All_Day_wise_Record(generic.ListView):
    template_name = 'admin/UserAccounts/all_day_wise.html'

    def get(self, request, on_month, *args, **kwargs):
        ctx = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            date_list = on_month.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
            on_month = '{}-{}'.format(y, m)
            recovery_info = Recovery.objects.values_list('created_date', 'loan_transaction', 'user_id',
                                                         'insentive_amount', 'recovery_status',
                                                         'completed_date').filter(completed_date__startswith=on_month)
            recovery_info_df = read_frame(recovery_info)
            no_of_defaulter = recovery_info_df["recovery_status"] == "No"
            no_of_recovered = recovery_info_df["recovery_status"] == "Yes"
            recovery_info_df['CreatedDate'] = pd.to_datetime(recovery_info_df['completed_date']).apply(
                lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
            monthwise_assign_defaulters = \
                recovery_info_df.where(no_of_defaulter).groupby(recovery_info_df['CreatedDate'])[
                    'loan_transaction'].count().sort_index(axis=0)
            month_wise_incentive = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['CreatedDate'])[
                    'insentive_amount'].sum().sort_index(axis=0)
            month_wise_recovery_count = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['CreatedDate'])[
                    'insentive_amount'].count().sort_index(axis=0)
            monthwise_assign_defaulters_dict = dict(monthwise_assign_defaulters)
            month_wise_incentive_dict = dict(month_wise_incentive)
            month_wise_recovery_count_dict = dict(month_wise_recovery_count)
            keys = monthwise_assign_defaulters_dict.keys()
            data = {key: [monthwise_assign_defaulters_dict.get(key), month_wise_incentive_dict.get(key),
                          month_wise_recovery_count_dict.get(key)] for key in keys}
            ctx['data'] = data
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class RecoveryByTeamOnDate(generic.ListView):
    template_name = 'recovery/recovery_by_team.html'

    def get(self, request, on_day, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            date_list = on_day.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
            if len(date_list[2]) > 1:
                d = date_list[2]
            else:
                d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)
            recovery_data = Recovery.objects.select_related("loan_transaction").filter(
                completed_date__startswith=new_date).filter(recovery_status='Yes')
            recovery_info_df = read_frame(recovery_data)
            no_of_defaulter = recovery_info_df["recovery_status"] == "No"
            no_of_recovered = recovery_info_df["recovery_status"] == "Yes"
            monthwise_assign_defaulters = \
                recovery_info_df.where(no_of_defaulter).groupby(recovery_info_df['user'])[
                    'loan_transaction'].count().sort_index(axis=0)
            month_wise_incentive = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['user'])[
                    'insentive_amount'].sum().sort_index(axis=0)
            month_wise_recovery_count = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['user'])[
                    'insentive_amount'].count().sort_index(axis=0)
            monthwise_assign_defaulters_dict = dict(monthwise_assign_defaulters)
            month_wise_incentive_dict = dict(month_wise_incentive)
            month_wise_recovery_count_dict = dict(month_wise_recovery_count)
            keys = monthwise_assign_defaulters_dict.keys()
            data = {key: [monthwise_assign_defaulters_dict.get(key), month_wise_incentive_dict.get(key),
                          month_wise_recovery_count_dict.get(key)] for key in keys}
            ctx['data'] = data
            return render(request, self.template_name, ctx)
        except Exception as e:
            # print("Ecx----", e)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            # print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class AdminRecoveryByTeamOnDate(generic.ListView):
    template_name = 'admin/UserAccounts/admin_recovery_by_team.html'

    def get(self, request, on_day, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            date_list = on_day.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
            if len(date_list[2]) > 1:
                d = date_list[2]
            else:
                d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)
            recovery_data = Recovery.objects.select_related("loan_transaction").filter(
                completed_date__startswith=new_date).filter(recovery_status='Yes')
            recovery_info_df = read_frame(recovery_data)
            no_of_defaulter = recovery_info_df["recovery_status"] == "No"
            no_of_recovered = recovery_info_df["recovery_status"] == "Yes"
            monthwise_assign_defaulters = \
                recovery_info_df.where(no_of_defaulter).groupby(recovery_info_df['user'])[
                    'loan_transaction'].count().sort_index(axis=0)
            month_wise_incentive = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['user'])[
                    'insentive_amount'].sum().sort_index(axis=0)
            month_wise_recovery_count = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['user'])[
                    'insentive_amount'].count().sort_index(axis=0)
            monthwise_assign_defaulters_dict = dict(monthwise_assign_defaulters)
            month_wise_incentive_dict = dict(month_wise_incentive)
            month_wise_recovery_count_dict = dict(month_wise_recovery_count)
            keys = monthwise_assign_defaulters_dict.keys()
            data = {key: [monthwise_assign_defaulters_dict.get(key), month_wise_incentive_dict.get(key),
                          month_wise_recovery_count_dict.get(key)] for key in keys}
            ctx['data'] = data
            return render(request, self.template_name, ctx)
        except Exception as e:
            # print("Ecx----", e)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            # print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class All_Months_wise_Record(generic.ListView):
    template_name = 'recovery/all_month_wise.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            recovery_info = Recovery.objects.values_list('created_date', 'loan_transaction', 'user_id',
                                                         'insentive_amount', 'recovery_status', 'completed_date')
            recovery_info_df = read_frame(recovery_info)
            no_of_defaulter = recovery_info_df["recovery_status"] == "No"
            no_of_recovered = recovery_info_df["recovery_status"] == "Yes"
            recovery_info_df['YearMonth'] = pd.to_datetime(recovery_info_df['completed_date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            monthwise_assign_defaulters = \
                recovery_info_df.where(no_of_defaulter).groupby(recovery_info_df['YearMonth'])[
                    'loan_transaction'].count().sort_index(axis=0)
            month_wise_incentive = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['YearMonth'])[
                    'insentive_amount'].sum().sort_index(axis=0)
            month_wise_recovery_count = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['YearMonth'])[
                    'insentive_amount'].count().sort_index(axis=0)
            monthwise_assign_defaulters_dict = dict(monthwise_assign_defaulters)
            month_wise_incentive_dict = dict(month_wise_incentive)
            month_wise_recovery_count_dict = dict(month_wise_recovery_count)
            keys = monthwise_assign_defaulters_dict.keys()
            data = {key: [monthwise_assign_defaulters_dict.get(key), month_wise_incentive_dict.get(key),
                          month_wise_recovery_count_dict.get(key)] for key in keys}
            ctx['data'] = data
            ctx['non'] = 'nan-nan'
            return render(request, self.template_name, ctx)
        except Exception as e:
            # print("---All_Months_wise_Record exception", e)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class Admin_All_Months_wise_Record(generic.ListView):
    template_name = 'admin/UserAccounts/admin_all_month_wise.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            recovery_info = Recovery.objects.values_list('created_date', 'loan_transaction', 'user_id',
                                                         'insentive_amount', 'recovery_status', 'completed_date')
            recovery_info_df = read_frame(recovery_info)
            no_of_defaulter = recovery_info_df["recovery_status"] == "No"
            no_of_recovered = recovery_info_df["recovery_status"] == "Yes"
            recovery_info_df['YearMonth'] = pd.to_datetime(recovery_info_df['completed_date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            monthwise_assign_defaulters = \
                recovery_info_df.where(no_of_defaulter).groupby(recovery_info_df['YearMonth'])[
                    'loan_transaction'].count().sort_index(axis=0)
            month_wise_incentive = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['YearMonth'])[
                    'insentive_amount'].sum().sort_index(axis=0)
            month_wise_recovery_count = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['YearMonth'])[
                    'insentive_amount'].count().sort_index(axis=0)
            monthwise_assign_defaulters_dict = dict(monthwise_assign_defaulters)
            month_wise_incentive_dict = dict(month_wise_incentive)
            month_wise_recovery_count_dict = dict(month_wise_recovery_count)
            keys = monthwise_assign_defaulters_dict.keys()
            data = {key: [monthwise_assign_defaulters_dict.get(key), month_wise_incentive_dict.get(key),
                          month_wise_recovery_count_dict.get(key)] for key in keys}
            ctx['data'] = data
            ctx['non'] = 'nan-nan'
            print("data", data)
            return render(request, self.template_name, ctx)
        except Exception as e:
            # print("---All_Months_wise_Record exception", e)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class ContactDetails(generic.ListView):
    template_name = 'recovery/contact_details.html'

    def get(self, request, file_name, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            file_path = open(os.path.join(settings.BASE_DIR, file_name))
            if file_path.mode == 'r':
                contents = file_path.read()
                contents = contents.replace("+", " ")
                contents = contents.replace("=%2B", "  : ")
                contents = contents.replace("=", "  :  ")
                contents = contents.replace("&", ", &")
                contents = contents.replace("&", "\n")
                contents = contents.replace("\n", " ")
            contents_list = contents.split(',')
            ctx['contacts'] = contents_list
            return render(request, self.template_name, ctx)
        except:
            pass
            return render(request, self.template_name, ctx)


# Create your views here.
@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class forword_name(generic.ListView):

    def get(self, request, recover_id, *args, **kwargs):
        template_name = "recovery/forword_page.html"
        ctx = {}
        recovery = Recovery.objects.select_related("loan_transaction").filter(id=recover_id, recovery_status='No')[0]
        ctx['recovery'] = recovery
        user = User.objects.filter(groups__name__in=['Recovery Team', 'Recovery Supervisor'], is_active='t')
        ctx['users'] = user
        return render(request, template_name, ctx)

    def post(self, request, *args, **kwargs):
        template_name = 'recovery/defaulter_list.html'
        forward_user_id = request.POST['forward_user_id']
        forward_recovery_id = request.POST['forward_recovery_id']
        Recovery.objects.filter(id=forward_recovery_id).update(user_id=forward_user_id)
        ctx = {}
        user_id = request.user.id
        user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
        ctx['user_group'] = user_group
        if user_group:
            call_count_list = []
            user = Recovery.objects.select_related("loan_transaction").filter(recovery_status='No')
            for recover in user:
                call_count = RecoveryCallHistory.objects.filter(recovery=recover.id, calling_status='Yes').count()

                call_count_list.append(call_count)
            ctx['users'] = zip(user, call_count_list)
            remark = Remark.objects.all()
            ctx['remark'] = remark
        else:
            call_count_list = []
            user = Recovery.objects.select_related("loan_transaction").filter(user_id=user_id, recovery_status='No')
            for recover in user:
                call_count = RecoveryCallHistory.objects.filter(recovery=recover.id, calling_status='Yes').count()

                call_count_list.append(call_count)
            ctx['users'] = zip(user, call_count_list)
            remark = Remark.objects.all()
            ctx['remark'] = remark
            ctx['call_count'] = call_count

        ctx['msg'] = "Forward Sussessfuly ... !"
        return render(request, template_name, ctx)


@login_required(login_url="/recovery/login/")
def recovery_call_change_status(request):
    try:
        recovery_id = request.GET['recovery_id']
        status_recovery = request.GET['status_recovery']
        txtcom_payment_date = request.GET['txtcom_payment_date']
        recovery = Recovery.objects.filter(id=recovery_id).update(recovery_status=status_recovery,
                                                                  completed_date=txtcom_payment_date)

        data = {
            'is_taken': "Save Data ..!"
        }
        return JsonResponse(data)
    except:
        data = {
            'is_taken': "Not Save Data ..!"
        }
        return JsonResponse(data)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class ChangeStatusRecovery(generic.ListView):
    template_name = 'recovery/change_recovery_status.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        data = {}
        try:

            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            recovery_data = Recovery.objects.select_related("loan_transaction").all()
            for user in recovery_data:
                if user.loan_transaction:
                    user_personal = LoanApplication.objects.select_related("user_personal").filter(
                        id=user.loan_transaction.id).first()
                    data.update({user.id: [user_personal.user_personal.name, user_personal.user_personal.mobile_no,
                                           user.loan_transaction.loan_application_id,
                                           user.loan_transaction.loan_scheme_id, user.insentive_amount,
                                           user.recovery_status, user.id]})

            ctx['data'] = data

            return render(request, self.template_name, ctx)
        except Exception as e:
            # print("Ecx----", e)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            # print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@login_required(login_url="/recovery/login/")
def index(request):
    template_name = "recovery/index.html"
    title = 'User Details'
    ctx = {}
    return render(request, template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class Dashboard_view(generic.ListView):
    template_name = 'recovery/dashboard.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        Accounts = User.objects.filter(groups__name='Accounts', id=request.user.id)
        if Accounts:
            return HttpResponseRedirect(reverse('date_report'))

        user_id = request.user.id
        user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
        ctx['user_group'] = user_group
        today = datetime.datetime.today()
        newdate = str(today)
        # print(')))))))))))))))))))))', newdate[:7])
        user_id_list = User.objects.filter(groups__name="Recovery Team", is_active='t')
        # print('1111111111111111', user_id_list)
        loan_users_submitted = LoanApplication.objects.filter(status="SUBMITTED").count()
        loan_users_preapproved = LoanApplication.objects.filter(status="PRE_APPROVED").count()
        loan_users_process = LoanApplication.objects.filter(status="PROCESS").count()
        # print('loan_users', loan_users)
        ctx['loan_users_submitted'] = loan_users_submitted
        ctx['loan_users_preapproved'] = loan_users_preapproved
        ctx['loan_users_process'] = loan_users_process
        calling_top = {}
        if user_id_list:
            for i in user_id_list:
                recovery_call = Recovery.objects.values_list('id').filter(user_id=i.id)
                call_count = RecoveryCallHistory.objects.filter(calling_status='Yes', recovery__in=recovery_call,
                                                                created_date__startswith=newdate[:7]).count()
                if call_count:
                    calling_top.update({i.username: call_count})
                    calling_top_pd = pd.Series(calling_top, index=calling_top.keys())
                    user_calling_top = calling_top_pd.sort_values(ascending=False)
                    name_user = list(user_calling_top.keys())[0]
                    # print(calling_top, "----if ===++++")
                    ctx['calling_user_top'] = user_calling_top.get(name_user)
                    ctx['calling_user'] = name_user
                    # print("-------", name_user, user_calling_top.get(name_user))
        else:
            # print("------call cnt else")
            ctx['calling_user_top'] = 0
            ctx['calling_user'] = "No"

        recovery_data = Recovery.objects.filter(
            completed_date__startswith=newdate[:7]).filter(recovery_status='Yes')
        if recovery_data:
            recovery_info_df = read_frame(recovery_data)
            insentive_amount = recovery_info_df.groupby(recovery_info_df['user'])[
                'insentive_amount'].sum().sort_index(axis=0)
            data = insentive_amount.sort_values(ascending=False)
            data_dict = dict(data)
            name_insentive_top = pd.Series(data_dict, index=data_dict.keys())
            name_insentive_top_recover = name_insentive_top.sort_values(ascending=False)
            name_insentive_top_reco = list(name_insentive_top_recover.keys())[0]
            insentive_amount_top = data_dict.get(name_insentive_top_reco) * 100 / 2.5
            ctx['name_insentive_top'] = name_insentive_top_reco
            ctx['insentive_amount_top'] = insentive_amount_top
        else:
            ctx['name_insentive_top'] = "None"
            ctx['insentive_amount_top'] = 0

        if user_group:
            ctx['defaulter_count'] = Recovery.objects.filter(recovery_status='No').count()
            ctx['recovery_count'] = Recovery.objects.filter(recovery_status='Yes',
                                                            completed_date__startswith=newdate[:7]).count()
            ctx['call_count'] = RecoveryCallHistory.objects.filter(calling_status='Yes',
                                                                   created_date__startswith=newdate[:7]).count()
            recovery = Recovery.objects.filter(recovery_status='No')

            print(Recovery.objects.filter(recovery_status='No').count())
            amount = 0.0
            
            for i in recovery:
                print(i, 'recovery amount data')
                amout = LoanApplication.objects.filter(id=i.loan_transaction_id)
                if amout:
                    # amount += float(amout[0].loan_scheme_id)
                    amount += get_balance_amount(i.loan_transaction, i.loan_transaction.loan_scheme_id)


            ctx['defaulter_balance_amount'] = amount
            incentive_amount = Recovery.objects.filter(recovery_status='Yes',
                                                       completed_date__startswith=newdate[:7]).aggregate(
                Sum('insentive_amount'))
            if incentive_amount['insentive_amount__sum'] != None:
                ctx['incentive_amount'] = incentive_amount['insentive_amount__sum']
                ctx['recovery_amount'] = round(float(incentive_amount['insentive_amount__sum']) * 100 / 2.5, 2)
            else:
                ctx['incentive_amount'] = 0
                ctx['recovery_amount'] = 0
        else:
            ctx['defaulter_count'] = Recovery.objects.filter(user_id=user_id, recovery_status='No').count()
            ctx['recovery_count'] = Recovery.objects.filter(user_id=user_id, recovery_status='Yes',
                                                            completed_date__startswith=newdate[:7]).count()
            recovery = Recovery.objects.filter(user_id=user_id, recovery_status='No')
            amount = 0.0
            for i in recovery:
                # amount = float(i.loan_transaction.loan_scheme_id)
                amount += get_balance_amount(i.loan_transaction, i.loan_transaction.loan_scheme_id)
            print(amount, ' amount')
            ctx['defaulter_balance_amount'] = amount
            recovery_call = Recovery.objects.values_list('id').filter(user_id=user_id)
            id_list = list(recovery_call)
            ctx['call_count'] = RecoveryCallHistory.objects.filter(calling_status='Yes', recovery__in=id_list,
                                                                   created_date__startswith=newdate[:7]).count()
            incentive_amount = Recovery.objects.filter(user_id=user_id, recovery_status='Yes',
                                                       completed_date__startswith=newdate[:7]).aggregate(
                Sum('insentive_amount'))
            if incentive_amount['insentive_amount__sum'] != None:
                ctx['incentive_amount'] = incentive_amount['insentive_amount__sum']
                ctx['recovery_amount'] = float(incentive_amount['insentive_amount__sum']) * 100 / 2.5
            else:
                ctx['incentive_amount'] = 0
                ctx['recovery_amount'] = 0

        # partner = User.objects.filter(groups__name='Partner', id=request.user.id)
        partner = User.objects.filter(groups__name='Partner', id=request.user.id)
        if partner:
            part_name = partner[0].username
            # print('partner name', partner[0].username)
            part_type = Partners.objects.filter(partner_name=part_name, is_disbursed=True)
            if part_type and part_type is not None:
                ctx['part_name'] = part_name
                ctx['part_type'] = "is_disbursed"
                # print('here-----------------')
                lead_count = 0  # This is total registered users from dynamic urls.
                # This will give last shared unique leads.
                leads = PartnerLeads.objects.filter(aff_id__partner_id__partner_name=part_name).order_by(
                    'mobile_number', '-created_date').distinct('mobile_number')
                # print('leads', leads)
                if leads:
                    for lead in leads:
                        # print('lead ID', lead.id)
                        lead_date = lead.created_date
                        user = User.objects.filter(username=lead.mobile_number, date_joined__gte=lead_date)
                        if user:
                            lead_count += 1
                # This is for users whose first loan is approved within 48 hours.
                approved_users = DynamicDisbursement.objects.filter(is_approved=True,
                                                                    lead_id__aff_id__partner_id__partner_name=part_name).count()
                # print('approved_users', approved_users)
                ctx['lead_count'] = lead_count
                ctx['approved_users'] = approved_users
            else:
                lead_count = PartnerLeads.objects.filter(aff_id__partner_id__partner_name=part_name)
                total_affiliates = Affiliates.objects.filter(partner_id__partner_name=part_name).count()
                # print('+++++++++++++', total_affiliates)
                # print("----lc", lead_count)
                lead_cnt = len(lead_count)
                sub_cnt = lead_count.filter(status=True).count()
                ctx['lead_count'] = lead_cnt
                ctx['sub_count'] = sub_cnt
                ctx['total_affiliates'] = total_affiliates
                ctx['partner'] = partner[0]
                if part_name == "Yelo":
                    ctx['part_name'] = 'Yelo'
                    user_set = set()
                    yelo_user_set = set()
                    yelo_total_leads = YeloLeads.objects.all().count()
                    yelo_total_sub = YeloLeads.objects.filter(is_subscribed=True).count()
                    yelo_total_paid = YeloLeads.objects.filter(is_subscribed=True).aggregate(Sum('paidToYelo'))[
                        'paidToYelo__sum']
                    user = User.objects.values_list('username')
                    yelo_user = YeloLeads.objects.values_list('mobile')
                    Common = set(user) & set(yelo_user)
                    count = len(Common)
                    ctx['yelo_total_leads'] = yelo_total_leads
                    ctx['yelo_total_sub'] = yelo_total_sub
                    ctx['yelo_total_paid'] = yelo_total_paid
                    ctx['count'] = count
        try:
            user_group = User.objects.filter(groups__name="Business Associate", id=request.user.id)
            if user_group:
                # print('if user_group----------------')
                Total_employee = ShareAppMaster.objects.values_list('user', flat=True)
                Total_employee = len(set(Total_employee))
                ctx['Total_B_A'] = Total_employee

                unique_share1 = ShareAppMaster.objects.values_list('number', flat=True)
                unique_share = len(set(unique_share1))
                ctx['unique_share_B_A'] = unique_share

                total_BA_sub = UserSharedData.objects.values_list('id', flat=True)
                total_BA_sub = len(set(total_BA_sub))
                ctx['total_BA_sub'] = total_BA_sub

                total_BA_sub_amt = UserSharedData.objects.values_list('id', flat=True)
                total_BA_sub_amt = len(set(total_BA_sub_amt)) * 59
                ctx['total_BA_sub_amt'] = total_BA_sub_amt

                total_BA_app = UserSharedData.objects.filter(is_approved=True, ).count()
                total_BA_app_amt = total_BA_app * 59
                ctx['total_BA_app_amt'] = total_BA_app_amt
                ctx['total_BA_app'] = total_BA_app
                reg_share = set(unique_share1)
                cnt = 0
                for number in reg_share:
                    # print(number)
                    user = User.objects.filter(username=number)
                    if user:
                        share = ShareAppMaster.objects.filter(number=number).order_by('id')
                        if share:
                            if share[0].created_date <= user[0].date_joined:
                                cnt += 1
                ctx['reg_share'] = cnt
            return render(request, self.template_name, ctx)
        except Exception as e:
            # print('inside exception', e)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class CallDetails(generic.ListView):
    template_name = 'recovery/day_recovery.html'

    def get(self, request, loan_application_id, *args, **kwargs):
        ctx = {}
        try:
            user_id = request.user.id
            loan_application = LoanApplication.objects.filter(loan_application_id=loan_application_id)
            if loan_application:
                user_group = User.objects.filter(groups__name="Recovery Team", id=request.user.id, is_active='t')
                ctx['user_group'] = user_group
                if user_group:
                    recovery_data = Recovery.objects.filter(loan_transaction=loan_application[0].id,
                                                            user=user_id)
                    if recovery_data:
                        recovery_info = RecoveryCallHistory.objects.filter(calling_status='Yes',
                                                                           recovery=recovery_data[0])
                        ctx['data'] = recovery_info
                        return render(request, self.template_name, ctx)
                        # recovery_info_df = read_frame(recovery_info)
                        # recovery_info_df['YearMonth'] = pd.to_datetime(recovery_info_df['created_date']).apply(
                        #     lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
                        # monthwise_call_count = \
                        #     recovery_info_df.groupby(recovery_info_df['YearMonth'])[
                        #         'YearMonth'].count()
                        #
                        # monthwise_call_count_dict = dict(monthwise_call_count)
                        # keys = monthwise_call_count.keys()
                        # data = {key: [monthwise_call_count.get(key)] for key in keys}

                else:
                    recovery_data = Recovery.objects.filter(loan_transaction=loan_application[0].id)
                    if recovery_data:
                        recovery_info = RecoveryCallHistory.objects.filter(calling_status='Yes',
                                                                           recovery=recovery_data[0])
                        ctx['data'] = recovery_info
                        return render(request, self.template_name, ctx)

            return render(request, self.template_name, ctx)
        except Exception as e:
            print("---- loan cancelled err", e)
            print("-------rbl save data-------", e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class DayWiseCallDetails(generic.ListView):
    template_name = 'recovery/day_wise_call.html'

    def get(self, request, on_month, *args, **kwargs):
        ctx = {}
        try:
            date_list = on_month.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
            on_month = '{}-{}'.format(y, m)
            user_id = request.user.id
            user_group = User.objects.filter(groups__name="Recovery Team", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            if user_group:
                recovery_info = RecoveryCallHistory.objects.values_list('created_date').filter(
                    created_date__startswith=on_month, recovery__user=user_id)
                # print('recovery_info', recovery_info)
            else:
                recovery_call = Recovery.objects.values_list('id').filter(user_id=user_id)
                id_list = list(recovery_call)
                recovery_info = RecoveryCallHistory.objects.values_list('created_date').filter(
                    created_date__startswith=on_month, recovery__in=id_list)
            recovery_info_df = read_frame(recovery_info)
            recovery_info_df['CreatedDate'] = pd.to_datetime(recovery_info_df['created_date']).apply(
                lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
            daywise_call_details = \
                recovery_info_df.groupby(recovery_info_df['CreatedDate'])[
                    'CreatedDate'].count().sort_index(axis=0)
            daywise_call_details_dict = dict(daywise_call_details)
            keys = daywise_call_details_dict.keys()
            data = {key: [daywise_call_details_dict.get(key)] for key in keys}
            ctx['data'] = data
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


def get_balance_amount(loan_obj, loan_amount):
    balance_amt = float(loan_amount)
    if loan_obj:
        if loan_obj.repayment_amount is not None:
            get_total_paid_amt = loan_obj.repayment_amount.filter(status__icontains='success').aggregate(Sum('amount'))[
                'amount__sum']
            if get_total_paid_amt:
                balance_amt = balance_amt - get_total_paid_amt
                balance_amt = float(balance_amt)
    return balance_amt


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class Overdue_Details_view(generic.ListView):
    template_name = 'recovery/defaulter_list.html'

    def get(self, request, *args, **kwargs):
        print(request.user.id, 'user id')
        ctx = {}
        user_id = request.user.id
        repayment_balance_list = []
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            print(user_group,'rcvry spervsr')
            ctx['user_group'] = user_group
            if user_group:

                recovery_agent_name = []
                call_count_list = []
                user = Recovery.objects.select_related("loan_transaction").filter(recovery_status='No')
                print(user, 'user from rcvry')
                if user:
                    for recover in user:
                        print(recover, 'recovery object 1134 in recovry.views')

                        print(recover.loan_transaction, 'recovery object 1134 in recovry.views')
                        if recover.loan_transaction.loan_scheme_id:
                            # print('recover', recover)
                            get_balance_amt = get_balance_amount(recover.loan_transaction,
                                                                 recover.loan_transaction.loan_scheme_id)
                            repayment_balance_list.append(get_balance_amt)
                            call_count = RecoveryCallHistory.objects.filter(recovery=recover.id,
                                                                            calling_status='Yes').count()
                            call_count_list.append(call_count)
                            username = User.objects.filter(id=recover.user_id)
                            recovery_agent_name.append(username[0])
                        ctx['users'] = zip(user, call_count_list, recovery_agent_name, repayment_balance_list)
                        remark = Remark.objects.all()
                        ctx['remark'] = remark
                    else:
                        pass
            else:
                recovery_agent_name = []
                call_count_list = []
                user = Recovery.objects.select_related("loan_transaction").filter(user_id=user_id, recovery_status='No')
                if user:
                    for recover in user:
                        get_balance_amt = get_balance_amount(recover.loan_transaction,
                                                             recover.loan_transaction.loan_scheme_id)
                        repayment_balance_list.append(get_balance_amt)

                        call_count = RecoveryCallHistory.objects.filter(recovery=recover.id,
                                                                        calling_status='Yes').count()

                        call_count_list.append(call_count)
                        username = User.objects.filter(id=recover.user_id)
                        recovery_agent_name.append(username[0])
                        ctx['users'] = zip(user, call_count_list, recovery_agent_name, repayment_balance_list)

                    remark = Remark.objects.all()
                    ctx['remark'] = remark
                    ctx['call_count'] = call_count
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class Overdue_Details_view1(generic.ListView):
    template_name = 'recovery/defaulter_list_1.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        user_id = request.user.id
        print(user_id, 'user id')
        repayment_balance_list = []
        try:
            user = User.objects.filter(groups__name__in=['Recovery Team', 'Recovery Supervisor'], is_active='t')
            ctx['users_list'] = user
            print(user, 'user')
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            print(user_group, 'user grp')
            ctx['user_group'] = user_group
            if user_group:
                recovery_agent_name = []
                call_count_list = []
                user = Recovery.objects.select_related("loan_transaction").filter(recovery_status='No')
                if user:
                    for recover in user:
                        if recover.loan_transaction.loan_scheme_id:
                            print(recover.loan_transaction.loan_scheme_id, 'recover')
                            print(recover, 'recover')

                            get_balance_amt = get_balance_amount(recover.loan_transaction,
                                                                 recover.loan_transaction.approved_amount)
                            print(get_balance_amount, 'balance amount')
                            repayment_balance_list.append(get_balance_amt)
                            call_count = RecoveryCallHistory.objects.filter(recovery=recover.id,
                                                                            calling_status='Yes').count()
                            call_count_list.append(call_count)
                            username = User.objects.filter(id=recover.user_id)
                            recovery_agent_name.append(username[0])
                        ctx['users'] = zip(user, call_count_list, recovery_agent_name, repayment_balance_list)
                        remark = Remark.objects.all()
                        ctx['remark'] = remark
                    else:
                        pass
            else:
                recovery_agent_name = []
                call_count_list = []
                user = Recovery.objects.select_related("loan_transaction").filter(user_id=user_id, recovery_status='No')
                if user:
                    for recover in user:
                        get_balance_amt = get_balance_amount(recover.loan_transaction,
                                                             recover.loan_transaction.approved_amount)
                        repayment_balance_list.append(get_balance_amt)

                        call_count = RecoveryCallHistory.objects.filter(recovery=recover.id,
                                                                        calling_status='Yes').count()

                        call_count_list.append(call_count)
                        username = User.objects.filter(id=recover.user_id)
                        recovery_agent_name.append(username[0])
                        ctx['users'] = zip(user, call_count_list, recovery_agent_name, repayment_balance_list)

                    remark = Remark.objects.all()
                    ctx['remark'] = remark
                    ctx['call_count'] = call_count
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class PerticularAmountwiseDefaulters(generic.ListView):
    template_name = 'recovery/particular_amountwise_defaulters.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        user_id = request.user.id
        amount = None
        data = {}
        # print("---keg", kwargs)
        month = kwargs['month']
        user = kwargs['user']
        amount = kwargs['amount']

        month = kwargs['month'] + '-' + kwargs['day']
        # print('====================', month, user, amount)
        new_date = ""
        date_list = month.split('-')
        y = date_list[0]
        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}'.format(y, m)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}'.format(y, m)
            # print('new_date', new_date)

        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            if user_group:
                users = Recovery.objects.values_list('loan_transaction', flat=True).filter(
                    created_date__startswith=new_date, recovery_status="No",
                    loan_transaction__loan_scheme_id=amount,
                    user__username=user)
                if users:
                    loans = LoanApplication.objects.filter(id__in=users)
                    if loans:
                        ctx['loans'] = loans
                    else:
                        ctx['loans'] = []
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class Overdue_Details_view_Status(generic.ListView):
    template_name = 'recovery/defaulter_list_status.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        user_id = request.user.id
        user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
        ctx['user_group'] = user_group
        if user_group:
            call_count_list = []
            user = Recovery.objects.select_related("loan_transaction").filter(recovery_status='No')
            for recover in user:
                call_count = RecoveryCallHistory.objects.filter(recovery=recover.id, calling_status='Yes').count()
                call_count_list.append(call_count)
            ctx['users'] = zip(user, call_count_list)
            remark = Remark.objects.all()
            ctx['remark'] = remark
        return render(request, self.template_name, ctx)


def get_pincodewise_analysis(pin_code):
    today = datetime.datetime.now()
    user_counter = 0
    overdue_counter = 0
    default_counter = 0
    loan_users = LoanApplication.objects.filter(pin_code__startswith=pin_code).filter(
        status__in=['APPROVED', 'COMPLETED'])
    for user in loan_users:
        user_counter += 1
        if user.status != 'COMPLETED':
            return_date = user.created_date + timedelta(int(user.tenure))
            over_due_limit_date = user.created_date + timedelta(int(user.tenure) * 2)
            if (today > return_date) & (today <= over_due_limit_date):
                overdue_counter += 1
            elif (today > over_due_limit_date):
                default_counter += 1

    overdue_counter_list = round(
        (get_div_by_zero(overdue_counter, user_counter) + get_div_by_zero(default_counter, user_counter)), 2)
    users = []
    users.append(user_counter)
    users.append(overdue_counter_list)
    return users


def get_div_by_zero(x, y):
    try:
        return (x / y) * 100
    except:
        return 0.0


import datetime


def customerdpd(user_id):
    Today_date = datetime.datetime.now()
    int_dbd_total = 0
    try:
        user = LoanApplication.objects.filter(user_id=user_id, status__in=['APPROVED', 'COMPLETED'])
        for users in user:
            payment_return = Payments.objects.filter(user_loan_application=users.loan_application_id,
                                                     status='success').order_by('-id').first()

            if users.status == 'APPROVED':
                loan_end_date = users.created_date + timedelta(int(users.tenure))
                defaulted_by_date = Today_date - loan_end_date
                int_dbd = defaulted_by_date.days
                if int_dbd > 0:
                    int_dbd_total += int_dbd
            else:
                loan_end_date = users.created_date + timedelta(int(users.tenure))
                defaulted_by_date = payment_return.created_date - loan_end_date
                int_dbd = defaulted_by_date.days
                if int_dbd > 0:
                    int_dbd_total += int_dbd

    except:
        pass
    return int_dbd_total


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class UserAllDetails_Recovery(generic.ListView):
    template_name = "recovery/credit_user_all_details.html"
    title = 'User Details'
    login_required = True
    pancount = 0

    def get(self, request, id):

        ctx = {}
        try:

            loan_users = LoanApplication.objects.select_related('verified_by', 'user', 'user_professional',
                                                                'user_personal', 'aadhar_id', 'bank_sts_id',
                                                                'salary_slip_id', 'company_id', 'self_video',
                                                                'current_address', 'permanent_address', 'reference_id',
                                                                'bank_id', 'cibil_id', 'device_data_id',
                                                                'disbursed_amount').get(id=id)
            try:
                company_id = loan_users.company_id.path
                company_id = botofile.get_url(company_id)
            except:
                company_id = None

            try:
                pan = loan_users.pancard_id.path
                pan = botofile.get_url(pan)
            except:
                pan = None

            try:
                salary_slip = loan_users.salary_slip_id.path
                salary_slip = botofile.get_url(salary_slip)
            except:
                salary_slip = None

            try:
                profile_images = loan_users.self_video.path
                profile_images = botofile.get_url(profile_images)
            except:
                profile_images = None

            overduestring = SmsStringMaster.objects.filter(user_id=loan_users.user_id,
                                                           sms_string__contains='overdue').count()

            smscount = SmsStringMaster.objects.filter(user_id=loan_users.user_id).count()

            loan_count = 0
            repayment_txc_count = 0
            loan_ids = LoanApplication.objects.all().order_by(
                'loan_application_id').distinct('loan_application_id').filter(user_id=loan_users.user_id,
                                                                              status__in=['APPROVED', 'COMPLETED'])
            loan_count = len(loan_ids)
            try:
                for id in loan_ids:
                    if id is not None:
                        loan_payment_count = id.repayment_amount.filter(
                            status='success').count()
                        repayment_txc_count += loan_payment_count
                    else:
                        repayment_txc_count = 0
                        # TODO Raise Error
                        pass
            except Exception as e:
                pass
            if overduestring:
                overdue = "This profile is under Overdue with Count :  " + str(overduestring)
            else:
                overdue = ""
            cibil_score = 0
            user_dpd = loan_application_dpd(loan_users.user_id)

            # Seraches pancard for this number
            pancount = PancardMatch(loan_users.user_personal.pan_number)
            current_address = loan_users.current_address
            ctx['user_count'] = None
            ctx['overdue'] = None
            # if current_address:
            #     pincode = get_pincodewise_analysis(current_address.pin_code[:3])
            #     if pincode:
            #         ctx['user_count'] = pincode[0]
            #         ctx['overdue'] = pincode[1]
            #     else:
            #         ctx['user_count'] = None
            #         ctx['overdue'] = None

            ctx['pan'] = pan
            ctx['company_id'] = company_id
            ctx['salary_slip'] = salary_slip
            ctx['pancount'] = pancount
            ctx['users'] = loan_users
            ctx['users_images'] = profile_images
            ctx['user_cibil_score'] = cibil_score
            ctx['overdue_count'] = overdue

            ctx['smscount'] = smscount
            ctx['user_dpd'] = user_dpd
            user_ref_list = []
            default_users = []

            # Generating user dpd list
            dpd_list = []
            loan_userss = LoanApplication.objects.filter(user=loan_users.user_id)
            for i in loan_userss:
                dpd_list.append([i.loan_application_id, i.dpd, i.loan_scheme_id])

            ctx['user_loancount_list'] = loan_count
            ctx['user_transaction_list'] = repayment_txc_count
            ctx['balance'] = 0
            ctx['dpd_list'] = dpd_list
            ctx['NOT_FOUND'] = get_reference_details(str(loan_users.loan_application_id))

            return render(request, self.template_name, ctx)


        except Exception as e:

            exc_type, exc_obj, exc_tb = sys.exc_info()

            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]

            print(exc_type, fname, exc_tb.tb_lineno)

            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class AdminUserAllDetails_Recovery(generic.ListView):
    template_name = "admin/UserAccounts/admin_credit_user_all_details.html"
    title = 'User Details'
    login_required = True
    pancount = 0

    def get(self, request, id):

        ctx = {}
        try:

            loan_users = LoanApplication.objects.select_related('verified_by', 'user', 'user_professional',
                                                                'user_personal', 'aadhar_id', 'bank_sts_id',
                                                                'salary_slip_id', 'company_id', 'self_video',
                                                                'current_address', 'permanent_address', 'reference_id',
                                                                'bank_id', 'cibil_id', 'device_data_id',
                                                                'disbursed_amount').get(id=id)
            try:
                company_id = loan_users.company_id.path
            except:
                company_id = None
            try:
                pan = loan_users.pancard_id.path
            except:
                pan = None
            try:
                salary_slip = loan_users.salary_slip_id.path
            except:
                salary_slip = None
            try:
                profile_images = loan_users.self_video.path
            except:
                profile_images = None

            overduestring = SmsStringMaster.objects.filter(user_id=loan_users.user_id,
                                                           sms_string__contains='overdue').count()

            smscount = SmsStringMaster.objects.filter(user_id=loan_users.user_id).count()

            loan_count = 0
            repayment_txc_count = 0
            loan_ids = LoanApplication.objects.all().order_by(
                'loan_application_id').distinct('loan_application_id').filter(user_id=loan_users.user_id,
                                                                              status__in=['APPROVED', 'COMPLETED'])
            loan_count = len(loan_ids)
            try:
                for id in loan_ids:
                    if id is not None:
                        loan_payment_count = id.repayment_amount.filter(
                            status='success').count()
                        repayment_txc_count += loan_payment_count
                    else:
                        repayment_txc_count = 0
                        # TODO Raise Error
                        pass
            except Exception as e:
                pass
            if overduestring:
                overdue = "This profile is under Overdue with Count :  " + str(overduestring)
            else:
                overdue = ""
            cibil_score = 0
            user_dpd = loan_application_dpd(loan_users.user_id)

            # Seraches pancard for this number
            pancount = PancardMatch(loan_users.user_personal.pan_number)
            current_address = loan_users.current_address
            ctx['user_count'] = None
            ctx['overdue'] = None
            # if current_address:
            #     pincode = get_pincodewise_analysis(current_address.pin_code[:3])
            #     if pincode:
            #         ctx['user_count'] = pincode[0]
            #         ctx['overdue'] = pincode[1]
            #     else:
            #         ctx['user_count'] = None
            #         ctx['overdue'] = None

            ctx['pan'] = pan
            ctx['company_id'] = company_id
            ctx['salary_slip'] = salary_slip
            ctx['pancount'] = pancount
            ctx['users'] = loan_users
            ctx['users_images'] = profile_images
            ctx['user_cibil_score'] = cibil_score
            ctx['overdue_count'] = overdue

            ctx['smscount'] = smscount
            ctx['user_dpd'] = user_dpd
            user_ref_list = []
            default_users = []

            # Generating user dpd list
            dpd_list = []
            loan_userss = LoanApplication.objects.filter(user=loan_users.user_id)
            for i in loan_userss:
                dpd_list.append([i.loan_application_id, i.dpd, i.loan_scheme_id])

            ctx['user_loancount_list'] = loan_count
            ctx['user_transaction_list'] = repayment_txc_count
            ctx['balance'] = 0
            ctx['dpd_list'] = dpd_list
            ctx['NOT_FOUND'] = get_reference_details(str(loan_users.loan_application_id))

            return render(request, self.template_name, ctx)

        except Exception as e:

            return render(request, self.template_name, ctx)


@login_required(login_url="/recovery/login/")
def recovery_call_history(request):
    try:
        recovery_id = request.GET['recovery_id']
        ex_payment_date = request.GET['ex_payment_date']
        next_call_time = request.GET['next_call_time']
        call_desc = request.GET['call_desc']
        cmd_remark = request.GET['cmd_remark']
        recovery = Recovery.objects.filter(id=recovery_id).last()
        print("recovery instance", recovery)
        if recovery.insentive_amount:
            print('inside if recovery.insentive_amount',recovery.insentive_amount)
            pass
        else:
            print('in else')
            incentive_amount = commision_cluc(recovery.loan_transaction_id)
            print('incentive amount', incentive_amount)
            Recovery.objects.filter(id=recovery_id).update(insentive_amount=incentive_amount)
        recovery_call_history = RecoveryCallHistory.objects.filter(recovery_id=recovery_id).order_by('-id')
        print(recovery_call_history, 'recovery_call_history instance')
        
        if recovery_call_history:
            date = datetime.datetime.now()
            RecoveryCallHistory.objects.filter(id=recovery_call_history[0].id).update(next_call_actual_Datetime=date,
                                                                                      next_call_status=1)

        RecoveryCallHistory.objects.create(remark_id=cmd_remark, next_call_time=next_call_time,
                                           expected_payment_date=ex_payment_date, call_desc=call_desc,
                                           recovery_id=recovery_id)
        data = {
            'is_taken': "Data Saved..!"
        }
        return JsonResponse(data)
    except Exception as e:
        print(e)
        data = {
            'is_taken': "Could Not Save the Data ..!"
        }
        return JsonResponse(data)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class CallReminder(generic.ListView):
    template_name = 'recovery/defaulter_reminder_recovery_on_date.html'

    def get(self, request, on_day, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            date_list = on_day.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
            d = date_list[2]
            new_date = '{}-{}-{}'.format(y, m, d)
            user_id = request.user.id
            all_user = RecoveryCallHistory.objects.select_related('recovery').filter(
                expected_payment_date__startswith=new_date).filter(next_call_time__isnull=False).filter(
                next_call_time__isnull=False)
            for user in all_user:
                if user.recovery.user_id == user_id:
                    status = str(user.recovery.recovery_status)
                    if status != 'Yes':
                        d = str(user.created_date)
                        next_call_status = user.next_call_status
                        call_status = 'Called' if next_call_status > 0 else 'Not Called'
                        last_call = d[10:18]
                        data.update({user.id: [user.recovery.loan_transaction.user_personal.name,
                                               user.recovery.loan_transaction.user.username,
                                               user.recovery.loan_transaction.loan_repayment_amount, last_call,
                                               user.next_call_time, user.recovery.insentive_amount,
                                               user.recovery.recovery_status, call_status]})
                ctx['data'] = data
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class CallReminder_Today(generic.ListView):
    template_name = 'recovery/defaulter_reminder_recovery_on_date.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        data = {}
        on_day_str = datetime.datetime.now()
        on_day = str(on_day_str)
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            date_list = on_day[:10].split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
            d = date_list[2]
            new_date = '{}-{}-{}'.format(y, m, d)
            user_id = request.user.id
            all_user = RecoveryCallHistory.objects.select_related('recovery').filter(
                expected_payment_date__startswith=new_date).filter(next_call_time__isnull=False).filter(
                next_call_time__isnull=False)
            for user in all_user:
                if user.recovery.user_id == user_id:
                    status = str(user.recovery.recovery_status)
                    if status != 'Yes':
                        d = str(user.created_date)
                        next_call_status = user.next_call_status
                        call_status = 'Called' if next_call_status > 0 else 'Not Called'
                        last_call = d[10:18]
                        data.update({user.id: [user.recovery.loan_transaction.user_personal.name,
                                               user.recovery.loan_transaction.user.username,
                                               user.recovery.loan_transaction.loan_repayment_amount, last_call,
                                               user.next_call_time, user.recovery.insentive_amount,
                                               user.recovery.recovery_status, call_status]})
                ctx['data'] = data
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class UserReferanceView_Recovery(generic.ListView):
    template_name = "recovery/user_referance_details.html"
    title = 'User Reference Details'

    def get(self, request, loan_application_id, *args, **kwargs):
        ctx = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            user = LoanApplication.objects.filter(loan_application_id=loan_application_id).first()
            # print(user.reference_id.relationships)
            ctx['reference'] = user
            # print(ctx)
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class expected_payment_on_date_bydefaulter(generic.ListView):
    template_name = 'recovery/expected_payment_on_date.html'

    def get(self, request, on_day, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            user_id = request.user.id
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            # on_day='2019-05-30'
            recovery_id = Recovery.objects.values_list('id').filter(user_id=user_id)
            recovery_id_set = set(recovery_id)
            all_data = RecoveryCallHistory.objects.values_list('recovery').filter(
                expected_payment_date__startswith=on_day).filter(calling_status='Yes')
            all_data_set = set(all_data)
            final_set = recovery_id_set.intersection(all_data_set)
            id_list = []
            for id in final_set:
                id_list.append(id[0])
            all = Recovery.objects.filter(id__in=id_list)
            for user in all:
                data.update({user.id: [user.loan_transaction.user_personal.name, user.loan_transaction.user.username,
                                       user.loan_transaction.loan_application_id,
                                       user.loan_transaction.loan_repayment_amount, user.recovery_status]})

            ctx['data'] = data
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class User_Active_Status(generic.ListView):
    template_name = 'recovery/allocation.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
        ctx['user_group'] = user_group
        user_group = User.objects.filter(groups__name="Recovery Team")

        ctx['users'] = user_group

        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        ctx = {}

        # try:

        # user_id = request.user.id
        username = request.POST['txtid']
        status = request.POST['cmdst']
        user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
        ctx['user_group'] = user_group
        User.objects.filter(username=username).update(is_active=status)
        message = "Status Changed successfuly of user " + username + " status: " + status

        user_group = User.objects.filter(groups__name="Recovery Team")

        ctx['users'] = user_group
        ctx['message'] = message
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class Recovery_Commission_view(generic.ListView):
    template_name = 'recovery/recoverycommission.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
        ctx['user_group'] = user_group
        if user_group:
            commission = RecoveryCommission.objects.all()
            ctx['commission'] = commission
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        ctx = {}
        user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
        ctx['user_group'] = user_group
        if user_group:
            commission = RecoveryCommission.objects.all()
            ctx['commission'] = commission
            data = request.POST
            RecoveryCommission.objects.create(commission_percent=data['txtcommssion_percent'])
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class Distribution_Limit_Amount_view(generic.ListView):
    template_name = 'recovery/distribution_limit_amount.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
        ctx['user_group'] = user_group
        Amount_setp = []
        if user_group:
            DistributionLimit = DistributionLimitAmount.objects.all()
            ctx['DistributionLimitAmount'] = DistributionLimit
            # Amount_setp = MudraScoreMaster.objects.all().order_by('id')
            for i in scheme:
                Amount_setp.append(i)
            ctx['Amount_setp'] = Amount_setp
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        ctx = {}
        user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
        ctx['user_group'] = user_group
        if user_group:
            DistributionLimit = DistributionLimitAmount.objects.all()
            ctx['DistributionLimitAmount'] = DistributionLimit
            data = request.POST
            DistributionLimitAmount.objects.create(amount=data['txtDis_amount'])
        return render(request, self.template_name, ctx)


@login_required(login_url="/recovery/login/")
def distrbution_amount_update(request):
    template_name = 'recovery/distribution_limit_amount.html'
    ctx = {}
    Amount_setp = []

    user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
    ctx['user_group'] = user_group
    if user_group:
        DistributionLimit = DistributionLimitAmount.objects.all()
        ctx['DistributionLimitAmount'] = DistributionLimit
        id = request.POST.get('txtid1', False)
        amount = request.POST.get('txtDis_amount1', False)
        status = request.POST.get('cmdst', False)
        DistributionLimitAmount.objects.filter(id=id).update(amount=amount,
                                                             status=status)
        for i in scheme:
            Amount_setp.append(i)
        ctx['Amount_setp'] = Amount_setp
    return render(request, template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class Remark_view(generic.ListView):
    template_name = 'recovery/remark.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
        ctx['user_group'] = user_group
        if user_group:
            remark = Remark.objects.all()
            ctx['remark'] = remark

        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        ctx = {}
        user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
        ctx['user_group'] = user_group
        if user_group:
            remark = Remark.objects.all()
            ctx['remark'] = remark
            data = request.POST
            Remark.objects.create(remark_title=data['txtremark_title'], remark_desc=data['txtremark_desc'])
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class Defaulter_Auto_Allocate_view_static(generic.ListView):

    def get(self, request, *args, **kwargs):
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            if user_group:
                recovery_data = Recovery.objects.all()
                if recovery_data:
                    # limit_amount = DistributionLimitAmount.objects.filter(status="Active")[0]
                    # last_date = Recovery.objects.latest('created_date')
                    # defaulter = getDefaulterslist_date_wise(last_date.created_date, limit_amount.amount)
                    # len_defaulter = len(defaulter)
                    # user_group = User.objects.filter(groups__name="Recovery Team", is_active='t')
                    # user_list = []
                    # for i in user_group:
                    #     user_list.append(i.id)
                    #
                    # user_list_len = len(user_list)
                    # total_recovery_count = len_defaulter / user_list_len
                    # count = total_recovery_count - 1
                    # i = 0
                    # for j in range(len_defaulter):
                    #     if j < count:
                    #         Recovery.objects.create(loan_transaction_id=defaulter[j],
                    #                                 user_id=user_list[i])
                    #     else:
                    #         i += 1
                    #         Recovery.objects.create(loan_transaction_id=defaulter[j],
                    #                                 user_id=user_list[i])
                    #         count += total_recovery_count

                    limit_amount = DistributionLimitAmount.objects.filter(status="Active")[0]
                    if limit_amount:
                        pass
                    else:
                        DistributionLimitAmount.objects.create(amount=3000)
                        limit_amount = DistributionLimitAmount.objects.filter(status="Active")[0]
                    mudraamount = []
                    # Amount_setp = MudraScoreMaster.objects.all().order_by('id')
                    for i in scheme:
                        amount = scheme[i]['ms_amount']
                        if amount <= limit_amount.amount:
                            mudraamount.append(amount)
                    defaulter = getDefaulterslist1(mudraamount)
                    len_defaulter = len(defaulter)
                    user_group = User.objects.filter(groups__name="Recovery Team", is_active='t')
                    len_user = len(user_group)
                    commission_percent = RecoveryCommission.objects.filter(status="Active")[0]
                    if commission_percent:
                        pass
                    else:
                        RecoveryCommission.objects.create(commission_percent='2.5')
                    for i in range(len_defaulter):
                        defaulter_total_len = len(defaulter[i])
                        user_div_len = defaulter_total_len / len_user
                        count = user_div_len
                        user_count = 0

                        for j in range(defaulter_total_len):
                            if j >= count:
                                count += user_div_len
                                user_count += 1

                            Recovery.objects.create(loan_transaction_id=defaulter[i][j],
                                                    user_id=user_group[user_count].id)

        except:
            pass
        return HttpResponse('ok')


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class Defaulter_Auto_Allocate_view(generic.ListView):

    def get(self, request, *args, **kwargs):
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            if user_group:
                recovery_data = Recovery.objects.all()
                # print("----Recovery objects", recovery_data)
                if recovery_data:
                    limit_amount = DistributionLimitAmount.objects.filter(status="Active")[0]
                    # print("----Recovery limit_amount", limit_amount)

                    last_date = Recovery.objects.latest('created_date')
                    defaulter = getDefaulterslist_date_wise(last_date.created_date, limit_amount.amount)
                    len_defaulter = len(defaulter)
                    user_group = User.objects.filter(groups__name="Recovery Team", is_active='t')
                    user_list = []
                    for i in user_group:
                        user = []
                        count = Recovery.objects.filter(user_id=i.id, recovery_status="No").count()
                        user.append(i.id)
                        user.append(count)
                        user_list.append(user)
                    total_count = Recovery.objects.filter(recovery_status="No").count()
                    count = 0
                    user_list_len = len(user_list)
                    total_recovery_count = (total_count + len_defaulter) / user_list_len
                    for i in range(user_list_len):
                        user_id = user_list[i][0]
                        user_defaulter_count = user_list[i][1]
                        defaulter_count = int(total_recovery_count) - user_defaulter_count
                        for j in range(defaulter_count):
                            if defaulter:
                                Recovery.objects.create(loan_transaction_id=defaulter[count],
                                                        user_id=user_id)
                                count += 1


                else:
                    limit_amount = DistributionLimitAmount.objects.filter(status="Active")[0]
                    if limit_amount:
                        pass
                    else:
                        DistributionLimitAmount.objects.create(amount=3000)
                        limit_amount = DistributionLimitAmount.objects.filter(status="Active")[0]
                    mudraamount = []
                    # Amount_setp = MudraScoreMaster.objects.all().order_by('id')
                    for i in scheme:
                        # print("---,", scheme[i]['ms_amount'])
                        amount = int(scheme[i]['ms_amount'])
                        repayment_amount = int(scheme[i]['ms_amount'])
                        # print("amount", amount)
                        if amount <= limit_amount.amount:
                            mudraamount.append(repayment_amount)
                    defaulter = getDefaulterslist(mudraamount)
                    # print(defaulter)
                    len_defaulter = len(defaulter)
                    user_group = User.objects.filter(groups__name="Recovery Team", is_active='t')
                    len_user = len(user_group)
                    commission_percent = RecoveryCommission.objects.filter(status="Active").first()
                    if commission_percent:
                        pass
                    else:
                        RecoveryCommission.objects.create(commission_percent='2.5')
                    for i in range(len_defaulter):
                        defaulter_total_len = len(defaulter[i])
                        user_div_len = defaulter_total_len / len_user
                        count = user_div_len
                        user_count = 0

                        for j in range(defaulter_total_len):
                            if j >= count:
                                count += user_div_len
                                user_count += 1

                            Recovery.objects.create(loan_transaction_id=defaulter[i][j],
                                                    user_id=user_group[user_count].id)
        except Exception as e:
            # print("Ecx----", e)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            # print(exc_type, fname, exc_tb.tb_lineno)
            pass

        return HttpResponse('ok')


def commision_cluc(loan_transaction_id):
    print(loan_transaction_id,'commission percent')
    commission_percent = RecoveryCommission.objects.filter(status="Active").last()
    print(commission_percent,'commission percent')
    user_loan = LoanApplication.objects.filter(id=loan_transaction_id)[0]
    print(user_loan, 'user_loan')
    payment_instance_amount = 0
    total_payment = 0
    try:
        # print("try", user_loan.loan_application_id)
        payment_instance = user_loan.repayment_amount.filter(status='success').aggregate(Sum('amount'))
        total_payment = payment_instance['amount__sum']
    except Exception as e:
        # print("commision_cluc", e)
        pass
    if total_payment == None:
        payment_instance_amount = 0
    else:
        payment_instance_amount = total_payment
    for i in scheme:
        if i == user_loan.loan_scheme_id:
            repayment_amount = scheme[i]['ms_amount']
    insentive_amount = (float(repayment_amount) - float(
        payment_instance_amount)) * float(commission_percent.commission_percent) / 100

    return insentive_amount


def getDefaulterslist(mudraamount):
    defaulter_list = []
    try:
        today = datetime.datetime.now()
        len_amount = len(mudraamount)
        for i in range(len_amount):
            amount = mudraamount[i]
            # print("+++________ rep amt", amount)
            loan_users = LoanApplication.objects.filter(status='APPROVED', loan_scheme_id=amount)
            # get_repayment_detail = loan_users.repayment_amount.filter(amount=mudraamount[i])
            defaulter = []
            for user in loan_users:
                return_date = user.loan_end_date
                # print("return_date")
                # print(return_date)
                if return_date:
                    if today > return_date:
                        defaulter.append(user.id)
                else:
                    #  TODO raise error
                    pass

            defaulter_list.append(defaulter)
        # print("defauter", defaulter_list)
        return defaulter_list
    except Exception as e:
        # print(e)
        return defaulter_list


def getDefaulterslist1(mudraamount):
    defaulter_list = []
    try:
        today = datetime.datetime.now()
        len_amount = len(mudraamount)
        for i in range(len_amount):
            loan_users = LoanApplication.objects.filter(status='APPROVED', loan_repayment_amount=mudraamount[i])
            defaulter = []
            for user in loan_users:
                return_date = user.loan_application_start_date + timedelta(int(user.tenure))
                recovery = Recovery.objects.filter(loan_transaction_id=user.id)
                if not recovery:
                    if today > return_date:
                        defaulter.append(user.id)
            defaulter_list.append(defaulter)
        return defaulter_list
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        # print(exc_type, fname, exc_tb.tb_lineno)
        return defaulter_list


def getDefaulterslist_date_wise(last_date, limit_amount):
    try:
        today = datetime.datetime.now()

        loan_users = LoanApplication.objects.filter(status='APPROVED', loan_repayment_amount__lt=limit_amount)
        defaulter = []
        last_date_str = str(last_date)
        for user in loan_users:
            return_date = user.loan_application_start_date + timedelta(int(user.tenure))
            recovery = Recovery.objects.filter(loan_transaction_id=user.id)
            if recovery:
                pass
            else:
                if today > return_date:
                    defaulter.append(user.id)
        return defaulter
    except:
        return defaulter


from django_pandas.io import read_frame


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class Months_wise_Record(generic.ListView):
    template_name = 'recovery/month_wise.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            user_id = request.user.id
            if user_group:
                recovery_info = Recovery.objects.values_list('created_date', 'loan_transaction', 'user_id',
                                                             'insentive_amount', 'recovery_status')
            else:
                recovery_info = Recovery.objects.values_list('created_date', 'loan_transaction', 'user_id',
                                                             'insentive_amount', 'recovery_status').filter(
                    user_id=user_id)
            recovery_info_df = read_frame(recovery_info)
            no_of_defaulter = recovery_info_df["recovery_status"] == "No"

            no_of_recovered = recovery_info_df["recovery_status"] == "Yes"
            recovery_info_df['YearMonth'] = pd.to_datetime(recovery_info_df['created_date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            monthwise_assign_defaulters = \
                recovery_info_df.where(no_of_defaulter).groupby(recovery_info_df['YearMonth'])[
                    'loan_transaction'].count().sort_index(axis=0)
            month_wise_incentive = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['YearMonth'])[
                    'insentive_amount'].sum().sort_index(axis=0)
            month_wise_recovery_count = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['YearMonth'])[
                    'insentive_amount'].count().sort_index(axis=0)

            monthwise_assign_defaulters_dict = dict(monthwise_assign_defaulters)
            month_wise_incentive_dict = dict(month_wise_incentive)
            month_wise_recovery_count_dict = dict(month_wise_recovery_count)
            keys = monthwise_assign_defaulters_dict.keys()
            data = {key: [monthwise_assign_defaulters_dict.get(key), month_wise_incentive_dict.get(key),
                          month_wise_recovery_count_dict.get(key)] for key in keys}
            ctx['data'] = data
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class Months_wise_Record_comp(generic.ListView):
    template_name = 'recovery/month_wise_comp.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            user_id = request.user.id
            recovery_info = Recovery.objects.values_list('completed_date', 'loan_transaction', 'user_id',
                                                         'insentive_amount', 'recovery_status').filter(user_id=user_id)
            recovery_info_df = read_frame(recovery_info)
            no_of_defaulter = recovery_info_df["recovery_status"] == "No"

            no_of_recovered = recovery_info_df["recovery_status"] == "Yes"
            recovery_info_df['YearMonth'] = pd.to_datetime(recovery_info_df['completed_date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            monthwise_assign_defaulters = \
                recovery_info_df.where(no_of_defaulter).groupby(recovery_info_df['YearMonth'])[
                    'loan_transaction'].count().sort_index(axis=0)
            month_wise_incentive = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['YearMonth'])[
                    'insentive_amount'].sum().sort_index(axis=0)
            month_wise_recovery_count = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['YearMonth'])[
                    'insentive_amount'].count().sort_index(axis=0)

            monthwise_assign_defaulters_dict = dict(monthwise_assign_defaulters)
            month_wise_incentive_dict = dict(month_wise_incentive)
            month_wise_recovery_count_dict = dict(month_wise_recovery_count)
            keys = monthwise_assign_defaulters_dict.keys()
            data = {key: [monthwise_assign_defaulters_dict.get(key), month_wise_incentive_dict.get(key),
                          month_wise_recovery_count_dict.get(key)] for key in keys}
            ctx['data'] = data
            ctx['non'] = 'nan-nan'
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class DefaultersRecoveredByDate_view(generic.ListView):
    template_name = 'recovery/defaulter_recovery_on_date_view.html'

    def get(self, request, on_day, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            date_list = on_day.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
            if len(date_list[2]) > 1:
                d = date_list[2]
            else:
                d = '0' + str(date_list[2])

            new_date = '{}-{}-{}'.format(y, m, d)

            user_id = request.user.id
            recovery_data = Recovery.objects.select_related("loan_transaction").filter(user_id=user_id).filter(
                completed_date__startswith=new_date).filter(recovery_status='Yes')
            for user in recovery_data:
                if user.completed_date and user.completed_date is not None:
                    completed_date = user.completed_date
                else:
                    completed_date = None
                data.update(
                    {user.id: [user.loan_transaction.user_personal.name, user.loan_transaction.user_personal.mobile_no,
                               user.loan_transaction.loan_application_id,
                               user.loan_transaction.loan_scheme_id, user.insentive_amount,
                               user.recovery_status, completed_date]})

            ctx['data'] = data

            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class SelfRecovery(generic.ListView):
    template_name = 'recovery/defaulter_recovery_on_date_view.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group

            user_id = request.user.id
            recovery_data = Recovery.objects.select_related("loan_transaction").filter(user_id=user_id).filter(
                recovery_status='Yes')
            for user in recovery_data:
                if user.completed_date and user.completed_date is not None:
                    completed_date = user.completed_date
                else:
                    completed_date = None
                data.update(
                    {user.id: [user.loan_transaction.user_personal.name, user.loan_transaction.user_personal.mobile_no,
                               user.loan_transaction.loan_application_id,
                               user.loan_transaction.loan_scheme_id, user.insentive_amount,
                               user.recovery_status, completed_date]})

            ctx['data'] = data

            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class IncentiveOnMonth(generic.ListView):
    template_name = 'recovery/IncentiveOnMonth.html'

    def get(self, request, on_month, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            date_list = on_month.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])

            new_date = '{}-{}'.format(y, m)
            recovery_data = Recovery.objects.select_related("loan_transaction").filter(
                completed_date__startswith=new_date).filter(recovery_status='Yes')
            recovery_info_df = read_frame(recovery_data)
            no_of_defaulter = recovery_info_df["recovery_status"] == "No"
            no_of_recovered = recovery_info_df["recovery_status"] == "Yes"
            monthwise_assign_defaulters = \
                recovery_info_df.where(no_of_defaulter).groupby(recovery_info_df['user'])[
                    'loan_transaction'].count().sort_index(axis=0)
            month_wise_incentive = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['user'])[
                    'insentive_amount'].sum().sort_index(axis=0)
            month_wise_recovery_count = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['user'])[
                    'insentive_amount'].count().sort_index(axis=0)
            monthwise_assign_defaulters_dict = dict(monthwise_assign_defaulters)
            month_wise_incentive_dict = dict(month_wise_incentive)
            month_wise_recovery_count_dict = dict(month_wise_recovery_count)
            keys = monthwise_assign_defaulters_dict.keys()
            data = {key: [monthwise_assign_defaulters_dict.get(key), month_wise_incentive_dict.get(key),
                          month_wise_recovery_count_dict.get(key)] for key in keys}
            ctx['data'] = data
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class MonthWiseIncentive(generic.ListView):
    template_name = 'recovery/incentive_details.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        try:

            recovery_info = Recovery.objects.values_list('completed_date', 'created_date', 'loan_transaction',
                                                         'user_id', 'insentive_amount', 'recovery_status')
            recovery_info_df = read_frame(recovery_info)
            no_of_defaulter = recovery_info_df["recovery_status"] == "No"
            no_of_recovered = recovery_info_df["recovery_status"] == "Yes"
            recovery_info_df['YearMonth'] = pd.to_datetime(recovery_info_df['completed_date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            monthwise_assign_defaulters = \
                recovery_info_df.where(no_of_defaulter).groupby(recovery_info_df['YearMonth'])[
                    'loan_transaction'].count().sort_index(axis=0)
            month_wise_incentive = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['YearMonth'])[
                    'insentive_amount'].sum().sort_index(axis=0)
            month_wise_recovery_count = \
                recovery_info_df.where(no_of_recovered).groupby(recovery_info_df['YearMonth'])[
                    'insentive_amount'].count().sort_index(axis=0)
            monthwise_assign_defaulters_dict = dict(monthwise_assign_defaulters)
            month_wise_incentive_dict = dict(month_wise_incentive)
            month_wise_recovery_count_dict = dict(month_wise_recovery_count)
            keys = monthwise_assign_defaulters_dict.keys()
            data = {key: [monthwise_assign_defaulters_dict.get(key), month_wise_incentive_dict.get(key),
                          month_wise_recovery_count_dict.get(key)] for key in keys}
            ctx['data'] = data
            ctx['non'] = 'nan-nan'
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class DefaultersRecoveredByDate(generic.ListView):
    template_name = 'recovery/defaulter_recovery_on_date.html'

    def get(self, request, on_day, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            date_list = on_day.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
            if len(date_list[2]) > 1:
                d = date_list[2]
            else:
                d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)
            user_id = request.user.id
            if user_group:
                recovery_data = Recovery.objects.select_related("loan_transaction").filter(
                    created_date__startswith=new_date).filter(recovery_status='Yes')

                # print('recovery_data', recovery_data)
            else:
                recovery_data = Recovery.objects.select_related("loan_transaction").filter(user_id=user_id).filter(
                    created_date__startswith=new_date).filter(recovery_status='Yes')

            for user in recovery_data:
                data.update({user.id: [user.loan_transaction.user_personal.name,
                                       user.loan_transaction.user.username,
                                       user.loan_transaction.loan_application_id,
                                       user.loan_transaction.loan_scheme_id, user.insentive_amount,
                                       user.recovery_status]})

            ctx['data'] = data

            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class UserParticularLoanWiseDetailsRecoveryView(generic.ListView):
    template_name = "recovery/user_particular_loan_details.html"
    title = 'Active User'

    def get(self, request, loan_application_id, *args, **kwargs):
        ctx = {}
        try:
            user_loan_details = LoanApplication.objects.filter(loan_application_id=loan_application_id)

            loan_payment = []
            for user in user_loan_details:
                if user.repayment_amount:
                    rep_amount = user.repayment_amount.filter(
                        status='success')
                    loan_payment.append(rep_amount)

            ctx['user_loan_details'] = rep_amount
            ctx['loan_application_id'] = loan_application_id
            # print('++++++++++++++++', ctx)
            return render(request, self.template_name, ctx)


        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class AdminUserParticularLoanWiseDetailsRecoveryView(generic.ListView):
    template_name = "admin/UserAccounts/admin_user_particular_loan_details.html"
    title = 'Active User'

    def get(self, request, loan_application_id, *args, **kwargs):
        ctx = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group

            user_loan = LoanApplication.objects.filter(loan_application_id=loan_application_id,
                                                       status__in=['APPROVED', 'COMPLETED'])
            for i in user_loan:
                payment = i.repayment_amount.all().filter(status="success")
                # print(i)
            # print(payment)
            loan_user_df = read_frame(payment)
            ctx['user_loan_application'] = loan_application_id
            # ctx['user']=request.GET['user']

            return render(request, self.template_name, ctx)


        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class UserParticularLoanWiseCallHistoryView(generic.ListView):
    template_name = "recovery/user_particular_loan_details_call_history.html"
    title = 'Active User'

    def get(self, request, recovery_id, *args, **kwargs):
        ctx = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            call_hostory = RecoveryCallHistory.objects.filter(recovery_id=recovery_id)
            ctx['recovery_call_hostory'] = call_hostory
            # ctx['user']=request.GET['user']
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class UserWiseLoanDetailsRecoveryView(generic.ListView):
    template_name = "recovery/user_loan_details_list.html"
    title = 'Active User'

    def get(self, request, username, *args, **kwargs):
        ctx = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group

            user = User.objects.get(username=username)
            loan_users = LoanApplication.objects.filter(user=user).filter(status__in=['APPROVED', 'COMPLETED'])

            ctx['user_loan'] = loan_users

            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class recoverydata_on_day(generic.ListView):
    template_name = 'recovery/onday_recovery.html'

    def get(self, request, on_day, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            user_id = request.user.id
            date_list = on_day.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
            if len(date_list[2]) > 1:
                d = date_list[2]
            else:
                d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)
            data = {}
            if user_group:
                recovery = Recovery.objects.select_related("loan_transaction").filter(
                    created_date__startswith=new_date,
                    recovery_status='No')
            else:
                recovery = Recovery.objects.select_related("loan_transaction").filter(user_id=user_id,
                                                                                      created_date__startswith=new_date,
                                                                                      recovery_status='No')

            for user in recovery:
                calling_count = RecoveryCallHistory.objects.filter(recovery=user.id).filter(
                    calling_status='Yes').count()
                if calling_count:
                    pass
                else:
                    calling_count = 0
                payment_amount = 0
                balance = user.loan_transaction.loan_repayment_amount
                try:
                    payment = Payments.objects.filter(
                        user_loan_application=user.loan_transaction.loan_application_id,
                        status='success',
                    ).aggregate(Sum('amount'))
                    payment_amount = payment['amount__sum']
                    balance = user.loan_transaction.loan_repayment_amount - payment_amount

                except:
                    pass
                data.update({user.id: [user.loan_transaction.user_personal.name,
                                       user.loan_transaction.user.username,
                                       user.loan_transaction.loan_application_id,
                                       user.loan_transaction.loan_repayment_amount, user.loan_transaction.tenure,
                                       user.recovery_status, calling_count, balance, user.id,
                                       user.loan_transaction.user_id, user.loan_transaction.id]})
            remark = Remark.objects.all()
            ctx['remark'] = remark
            ctx['data'] = data
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class recoverydata_on_day1(generic.ListView):
    template_name = 'recovery/onday_recovery.html'

    def get(self, request, on_day, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            user_id = request.user.id
            date_list = on_day.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
            if len(date_list[2]) > 1:
                d = date_list[2]
            else:
                d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)
            if user_group:
                recovery = Recovery.objects.select_related("loan_transaction").filter(
                    created_date__startswith=new_date,
                    recovery_status='No')
            else:
                recovery = Recovery.objects.select_related("loan_transaction").filter(user_id=user_id,
                                                                                      created_date__startswith=new_date,
                                                                                      recovery_status='No')
            if recovery:
                for user in recovery:
                    calling_count = RecoveryCallHistory.objects.filter(recovery=user.id).filter(
                        calling_status='Yes').count()
                    if calling_count:
                        pass
                    else:
                        calling_count = 0
                    # print('user.loan_transaction.loan_application_id', user.loan_transaction)
                    # print('user.loan_transaction.loan_scheme_id', user.loan_transaction.loan_scheme_id)
                    balance = get_balance_amount(user.loan_transaction,
                                                 user.loan_transaction.loan_scheme_id)

                    data.update(
                        {user.id: [user.loan_transaction.user_personal.name, user.loan_transaction.user.username,
                                   user.loan_transaction.loan_application_id,
                                   user.loan_transaction.loan_scheme_id, user.loan_transaction.loan_scheme_id,
                                   user.user, calling_count, balance, user.id,
                                   user.loan_transaction.user_id, user.loan_transaction.id,
                                   user.loan_transaction.loan_start_date, user.loan_transaction.loan_end_date]})
            remark = Remark.objects.all()
            ctx['remark'] = remark
            ctx['data'] = data
            return render(request, self.template_name, ctx)
        except Exception as e:
            print("----------inside333333333333333333333")
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            print("------------Exception ", e)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class recoverydata_on_month_view(generic.ListView):
    template_name = 'recovery/daywise_recovery_view.html'

    def get(self, request, year_month, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            user_id = request.user.id
            recovery_info = Recovery.objects.values_list('created_date', 'completed_date', 'loan_transaction',
                                                         'user_id',
                                                         'insentive_amount', 'recovery_status').filter(user_id=user_id)

            recovery_info_dfs = read_frame(recovery_info)
            recovery_info_dfs['YearMonth'] = recovery_info_dfs['completed_date'].apply(
                lambda x: '{}-{}'.format(x.year, x.month))
            recovery_info_dfs['created_date'] = recovery_info_dfs['created_date'].apply(
                lambda x: '{}-{}-{}'.format(x.year, x.month, x.day))
            recovery_info_dfs['completed_date'] = recovery_info_dfs['completed_date'].apply(
                lambda x: '{}-{}-{}'.format(x.year, x.month, x.day))
            day_wise_recovery_count = recovery_info_dfs.where(recovery_info_dfs["YearMonth"] == year_month)

            month_wise_recovery_count_dict = day_wise_recovery_count.to_dict()
            date_list = list(day_wise_recovery_count['completed_date'])
            date_set = set(date_list)
            daywise_assign_defaulters = 0
            for demo_date in date_set:
                demo_date_str = str(demo_date)
                if demo_date_str != 'nan':
                    date_list = demo_date_str.split('-')
                    y = date_list[0]
                    if len(date_list[1]) > 1:
                        m = date_list[1]
                    else:
                        m = '0' + str(date_list[1])
                    d = date_list[2]
                    new_date = '{}-{}-{}'.format(y, m, d)
                    daywise_assign_defaulters = Recovery.objects.values_list('created_date').filter(user_id=user_id,
                                                                                                    created_date__startswith=new_date).count()

                filter1 = day_wise_recovery_count['completed_date'] == demo_date
                filter2 = day_wise_recovery_count['recovery_status'] == 'Yes'
                filter3 = day_wise_recovery_count['recovery_status'] == 'No'
                recovery_count = \
                    day_wise_recovery_count.where(filter1 & filter2)['loan_transaction'].count()
                incentive = \
                    day_wise_recovery_count.where(filter1 & filter2)['insentive_amount'].sum()

                data.update({demo_date: [daywise_assign_defaulters, recovery_count, incentive]})

            ctx['data'] = data
            return render(request, self.template_name, ctx)
        except Exception as e:
            # print("Ecx----", e)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class Call_history_data_on_day(generic.ListView):
    template_name = 'recovery/daywise_recovery_view.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            on_day_str = datetime.datetime.today()
            on_day = str(on_day_str)
            on_date = on_day[:7]
            if on_day[5:7] < str(10):
                year = on_day[:4]
                mon = on_day[6:7]
                on_date = str(year) + '-' + str(mon)
            year_month = on_date
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            user_id = request.user.id
            recovery_info = RecoveryCallHistory.objects.all()

            recovery_info_dfs = read_frame(recovery_info)
            recovery_info_dfs['YearMonth'] = recovery_info_dfs['expected_payment_date'].apply(
                lambda x: '{}-{}'.format(x.year, x.month))
            recovery_info_dfs['expected_payment_date'] = recovery_info_dfs['expected_payment_date'].apply(
                lambda x: '{}-{}-{}'.format(x.year, x.month, x.day))
            day_wise_recovery_count = recovery_info_dfs.where(recovery_info_dfs["YearMonth"] == year_month)
            month_wise_recovery_count_dict = day_wise_recovery_count.to_dict()
            date_list = list(day_wise_recovery_count['expected_payment_date'])
            date_set = set(date_list)
            daywise_assign_call = 0
            for demo_date in date_set:
                demo_date_str = str(demo_date)
                if demo_date_str != 'nan':
                    date_list = demo_date_str.split('-')
                    y = date_list[0]
                    if len(date_list[1]) > 1:
                        m = date_list[1]
                    else:
                        m = '0' + str(date_list[1])
                    d = date_list[2]
                    new_date = '{}-{}-{}'.format(y, m, d)
                    daywise_assign_defaulters = RecoveryCallHistory.objects.values_list('expected_payment_date').filter(
                        user_id=user_id, created_date__startswith=new_date).count()

                filter1 = day_wise_recovery_count['completed_date'] == demo_date
                filter2 = day_wise_recovery_count['recovery_status'] == 'Yes'
                filter3 = day_wise_recovery_count['recovery_status'] == 'No'
                recovery_count = \
                    day_wise_recovery_count.where(filter1 & filter2)['loan_transaction'].count()
                incentive = \
                    day_wise_recovery_count.where(filter1 & filter2)['insentive_amount'].sum()

                data.update({demo_date: [daywise_assign_defaulters, recovery_count, incentive]})

            ctx['data'] = data
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class recoverydata_on_month(generic.ListView):
    template_name = 'recovery/daywise_recovery.html'

    def get(self, request, year_month, *args, **kwargs):
        ctx = {}
        data = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            user_id = request.user.id
            if user_group:
                recovery_info = Recovery.objects.values_list('created_date', 'loan_transaction', 'user_id',
                                                             'insentive_amount', 'recovery_status')
            else:
                recovery_info = Recovery.objects.values_list('created_date', 'loan_transaction', 'user_id',
                                                             'insentive_amount', 'recovery_status').filter(
                    user_id=user_id)

            recovery_info_dfs = read_frame(recovery_info)
            recovery_info_dfs['YearMonth'] = recovery_info_dfs['created_date'].apply(
                lambda x: '{}-{}'.format(x.year, x.month))
            recovery_info_dfs['created_date'] = recovery_info_dfs['created_date'].apply(
                lambda x: '{}-{}-{}'.format(x.year, x.month, x.day))
            day_wise_recovery_count = recovery_info_dfs.where(recovery_info_dfs["YearMonth"] == year_month)
            month_wise_recovery_count_dict = day_wise_recovery_count.to_dict()
            date_list = list(day_wise_recovery_count['created_date'])
            date_set = set(date_list)
            for demo_date in date_set:
                filter1 = day_wise_recovery_count['created_date'] == demo_date
                filter2 = day_wise_recovery_count['recovery_status'] == 'Yes'
                filter3 = day_wise_recovery_count['recovery_status'] == 'No'
                daywise_assign_defaulters = \
                    day_wise_recovery_count.where(filter1)['loan_transaction'].count()
                recovery_count = \
                    day_wise_recovery_count.where(filter1 & filter2)['loan_transaction'].count()
                incentive = \
                    day_wise_recovery_count.where(filter1 & filter2)['insentive_amount'].sum()

                data.update({demo_date: [daywise_assign_defaulters, recovery_count, incentive]})

            ctx['data'] = data
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


# Charts

from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def charts(request):
    ctx = {}
    user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
    ctx['user_group'] = user_group

    template_name = 'recovery/Charts.html'

    return render(request, template_name, ctx)


import calendar
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def recovery_amount(request):
    """
        renders bar graph for recoverd amount
    :return:
    """
    today = datetime.datetime.today()
    year = today.year
    mnth = today.month
    # mnth = today - relativedelta(months=30)
    month_days = calendar.Calendar()
    ctx = {}

    days_list = []

    total_recovery_amount_list = []
    color_list = ["#3e95cd", "#7292df", "#ad89e1", "#e27ccf", "#ff71ac", "#ff747e", "#ff884c", "#ffa600", "#3cba9f",
                  "#e8c3b9", "#c45850", "#3e95cd", "#7292df", "#ad89e1", "#e27ccf", "#ff71ac", "#ff747e", "#ff884c",
                  "#ffa600", "#3cba9f", "#e8c3b9", "#c45850", "#3e95cd", "#7292df", "#ad89e1", "#e27ccf", "#ff71ac",
                  "#ff747e", "#ff884c", "#ffa600", "#3cba9f", "#e8c3b9"]

    try:
        date = str(today)[:7]
        user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
        ctx['user_group'] = user_group
        if user_group:
            recovery_amount = Recovery.objects.filter(recovery_status='Yes', completed_date__startswith=date)


        else:
            recovery_amount = Recovery.objects.filter(user_id=request.user.id, recovery_status='Yes',
                                                      completed_date__startswith=date)
        if recovery_amount:
            # date_list = [1, 2, 3, 4, 5, 6, 7]
            # iteratign with itermonthdays

            for days in month_days.itermonthdays(year, mnth):

                if days != 0:
                    total = 0.0
                    days_list.append(days)
                    for i in recovery_amount:
                        if i.completed_date:
                            if i.completed_date.day == days and i.recovery_status == 'Yes':
                                if i.insentive_amount:
                                    total += i.insentive_amount

                            else:
                                total += 0
                        else:
                            total += 0
                    total_amount = total * 100 / 2.5
                    total_recovery_amount_list.append(total_amount)


    except Exception as e:
        # pass
        print(e)
    # total_recovery_amount_list1=[1000,2000,5000,0,0,1000,2000,3000,8000,0,0,0,3555,466,244,15,5,377,8,488,5000,0,0,1000,2000,3000,10000,0,2000,300]
    data = {
        'labels': days_list,
        'datasets': [{
            'label': '',
            'data':
                total_recovery_amount_list
            ,
            'backgroundColor': color_list,
            'borderWidth': 1
        }]
    }
    return JsonResponse(json.dumps(data), safe=False)


def total_recovery_amount(request):
    """
        renders bar graph for total recoverd amount
    :return:
    """
    today = datetime.today()
    year = today.year
    mnth = today.month
    month_days = calendar.Calendar()
    ctx = {}
    total = 0
    date_list = []
    total_recovery_amount_list = []
    try:
        recovery_amount = Recovery.objects.all().order_by('completed_date')
        # recovery_amount = Recovery.objects.all()
        date_list = list(recovery_amount.completed_date)
        # print('date_list', date_list)
        # iteratign with itermonthdays
        for days in month_days.itermonthdays(year, mnth):
            # print(days)
            days_list = days
            for i in recovery_amount:
                # if recovery_amount.completed_date.day ==ys and i.recovery_status == 'Yes':
                if i.recovery_status == 'No':
                    total += days.insentive_amount
                else:
                    total = 0
            # TO calculate the loan amount
            total_recovery_amount_list.appennd(total * 40)
        ctx[days] = zip(recovery_amount.user, recovery_amount.completed_date, total)


    except Exception as e:
        print(e)

    data = {
        'labels': date_list,
        'datasets': [{
            'label': '',
            'data': [
                total_recovery_amount_list
            ],
            'backgroundColor': [
                '#5295b0'
            ],
            'borderWidth': 1
        }]
    }
    return JsonResponse(json.dumps(data), safe=False)


def total_defaulter_pie_chart(request):
    """
        renders pie chart for total recoverd amount
    :return:
    """

    total_recovery_amount_list = []
    today = datetime.datetime.today()
    loan_disbursement_list = []
    defaulter_amount_count = []
    # defaulter_amount_count_1 = [1,2,3,4,5,6,7,8,9,1,2]
    color_list_2 = []
    color_list = ["#3e95cd", "#7292df", "#ad89e1", "#e27ccf", "#ff71ac", "#ff747e", "#ff884c", "#ffa600", "#3cba9f",
                  "#e8c3b9", "#c45850"]
    amount_list = []
    try:
        temp_date = ""
        for i in scheme.keys():
            amount_list.append(i)
        for amount in amount_list:
            loan_disbursement_list.append(amount)
            defaulter = LoanApplication.objects.filter(status='APPROVED', loan_scheme_id=amount,
                                                       loan_end_date__gte=today).count()
            defaulter_amount_count.append(defaulter)

        color_list_count = len(defaulter_amount_count)
        # Getting the number of colors as number of defaulters to avoid issues in graph
        color_list_2 = color_list[:color_list_count]


    except Exception as e:
        # print("---- loan cancelled err", e)
        # print("-------rbl save data-------", e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        # print(exc_type, fname, exc_tb.tb_lineno)

    data = {
        'labels': loan_disbursement_list,
        'datasets': [{
            'label': '',
            'data': [
                defaulter_amount_count
            ],
            'backgroundColor': color_list_2,
            'borderWidth': 1
        }]
    }
    return JsonResponse(json.dumps(data), safe=False)


def recovery_amount2(request):
    """
        renders bar graph for Total recoverd amount
    :return:
    """
    today = datetime.today()
    year = today.year
    mnth = today.month - 1
    month_days = calendar.Calendar()

    days_list = []
    total_recovery_amount_list = []
    color_list = ["#3e95cd", "#7292df", "#ad89e1", "#e27ccf", "#ff71ac", "#ff747e", "#ff884c", "#ffa600", "#3cba9f",
                  "#e8c3b9", "#c45850", "#3e95cd", "#7292df", "#ad89e1", "#e27ccf", "#ff71ac", "#ff747e", "#ff884c",
                  "#ffa600", "#3cba9f", "#e8c3b9", "#c45850", "#3e95cd", "#7292df", "#ad89e1", "#e27ccf", "#ff71ac",
                  "#ff747e", "#ff884c", "#ffa600", "#3cba9f"]

    try:
        recovery_amount = Recovery.objects.all()
        if recovery_amount:
            # iteratign with itermonthdays
            for days in month_days.itermonthdays(year, mnth):
                #  for days in date_list:
                if days != 0:
                    total = 0.0
                    days_list.append(days)
                    for i in recovery_amount:
                        if i.completed_date:
                            if i.completed_date.day == days and i.recovery_status == 'No':
                                if i.insentive_amount:
                                    total += i.insentive_amount

                            else:
                                total += 0
                        else:
                            total += 0

                    total_recovery_amount_list.append(total * 40)
    except Exception as e:
        print(e)

    total_recovery_amount_list1 = [1000, 2000, 5000, 0, 0, 1000, 2000, 3000, 8000, 0, 0, 0, 3555, 466, 244, 15, 5, 377,
                                   8, 488, 5000, 0, 0, 1000, 2000, 3000, 10000, 0, 2000, 300]

    data = {
        'labels': days_list,
        'datasets': [{
            'label': 'Amount in Rupees',
            'data':
                total_recovery_amount_list
            ,
            'backgroundColor': color_list,
            'borderWidth': 1
        }]
    }
    # print("recivery amt 2", data)
    return JsonResponse(json.dumps(data), safe=False)


def get_completed_recovery_amount(request):
    """
        renders bar graph for completed loans for defaulters
    :return:
    """

    today = datetime.today()
    year = today.year
    mnth = today.month - 1
    month_days = calendar.Calendar()

    days_list = []
    total_recovery_amount_list = []
    color_list = ["#3e95cd", "#7292df", "#ad89e1", "#e27ccf", "#ff71ac", "#ff747e", "#ff884c", "#ffa600", "#3cba9f",
                  "#e8c3b9", "#c45850", "#3e95cd", "#7292df", "#ad89e1", "#e27ccf", "#ff71ac", "#ff747e", "#ff884c",
                  "#ffa600", "#3cba9f", "#e8c3b9", "#c45850", "#3e95cd", "#7292df", "#ad89e1", "#e27ccf", "#ff71ac",
                  "#ff747e", "#ff884c", "#ffa600", "#3cba9f"]

    try:
        recovery_amount = Recovery.objects.all()
        if recovery_amount:
            # iteratign with itermonthdays
            for days in month_days.itermonthdays(year, mnth):
                #  for days in date_list:
                if days != 0:
                    total = 0
                    days_list.append(days)
                    try:
                        for i in recovery_amount:
                            if i.completed_date:
                                if i.completed_date.day == days:
                                    if i.recovery_status == 'Yes':
                                        total += 1

                        total_recovery_amount_list.append(total)
                    except Exception as e:
                        pass
                        # print("get_completed_recovery_amount", e)

    except Exception as e:
        pass
        # print("get_completed_recovery_amount", e)

    data = {
        'labels': days_list,
        'datasets': [{
            'label': 'Loan Completed Count ',
            'data':
                total_recovery_amount_list
            ,
            'backgroundColor': color_list,
            'borderWidth': 1
        }]
    }
    # print("get_completed_recovery_amount", data)
    return JsonResponse(json.dumps(data), safe=False)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class ResetPassword(generic.ListView):
    template_name = 'recovery/reset_password.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        ctx = {}
        user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
        ctx['user_group'] = user_group
        user_id = request.user.id
        current_password = request.POST['current_password']
        new_password = request.POST['new_password']
        u = User.objects.get(id__exact=user_id)
        password_verified = u.check_password(current_password)
        if password_verified:
            u.set_password(new_password)
            u.save()
            message = "Password changed successfully"
        else:
            message = "Incorrect current Password"

        ctx['message'] = message

        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class ResetPassword1(generic.ListView):
    template_name = 'recovery/reset_password1.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        ctx = {}

        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            # user_id = request.user.id
            username = request.POST['current_password']
            new_password = request.POST['new_password']
            u = User.objects.get(username__exact=username)
            if u:
                # password_verified = u.check_password(current_password)
                # print(password_verified)
                try:
                    u.set_password(new_password)
                    u.save()
                    message = "Password changed successfully"
                except:
                    message = "Something went wrong"

                ctx['message'] = message

                return render(request, self.template_name, ctx)
            else:
                message = "Didn't Find username"
                ctx['message'] = message
                return render(request, self.template_name, ctx)
        except:
            message = "Something went wrong"

            ctx['message'] = message
            return render(request, self.template_name, ctx)


import csv


@login_required(login_url='/login/')
def extra_defaulters(request):
    """
        Defaulters whose recovery status is No in recovery but loan is completed
    :param request:
    :return:
    """

    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="auto_delete_pending.csv"'
    writer = csv.writer(response)
    writer.writerow(
        ['App id', 'status', 'REcovery status'])

    try:
        recovery = Recovery.objects.filter(recovery_status="No")
        # print("Recovery count", len(recovery))
        for users in recovery:
            # print(users)
            user_loan = LoanApplication.objects.filter(id=users.loan_transaction_id)
            # print(user_loan)
            for i in user_loan:
                if i.status == "COMPLETED":
                    # print("Loan comp", i.loan_application_id)
                    writer.writerow([i.loan_application_id, i.status, users.recovery_status])
                # else:
                #     # print("Loan app", i.loan_application_id)
                #     writer.writerow([i.loan_application_id, i.status])

        return response

    except Exception as e:
        # print("Exception----------", e.args)
        return HttpResponse(e)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class admin_defaulter(generic.ListView):
    template_name = 'recovery/admins_defaulter.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        user_id = request.user.id

        ctx = {}
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            if user_group:
                call_count_list = []

                user = Recovery.objects.select_related("loan_transaction").filter(user_id=user_id, recovery_status='No')
                for recover in user:
                    call_count = RecoveryCallHistory.objects.filter(recovery=recover.id, calling_status='Yes').count()

                    call_count_list.append(call_count)

                ctx['users'] = zip(user, call_count_list)

                remark = Remark.objects.all()
                ctx['remark'] = remark
                ctx['call_count'] = call_count
        except:
            pass
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class RecoveryLoanApplicationViewNew(generic.ListView):
    '''
    Template : recovery_userloan_application_list_new.html
    shows user loan applications by date
    '''
    template_name = "recovery/recovery_userloan_application_list_new.html"
    title = ' User Loan Application new'

    def get(self, request, *args, **kwargs):
        try:
            ctx = {}
            loan_users = LoanApplication.objects.filter(status="SUBMITTED")
            dataframe = read_frame(loan_users)
            dataframe['YearMonthDay'] = pd.to_datetime(dataframe['created_date']).apply(
                lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
            day_wise_application_count = \
                dataframe.groupby(dataframe['YearMonthDay'])[
                    'loan_application_id'].count().sort_index(axis=0)
            day_wise_application_count_dict = dict(day_wise_application_count)
        except:
            pass
        ctx['application_details'] = day_wise_application_count_dict
        return render(request, self.template_name, ctx)


from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class RecoveryLoanApplicationView(generic.ListView):
    '''
    Template : revovery_userloan_applications_list.html
    shows user loan applications in details
    '''
    template_name = "recovery/recovery_userloan_applications_list.html"
    title = ' Recovery User Loan Application'

    def get(self, request, date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        date_list = date.split('-')
        y = date_list[0]

        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}-{}'.format(y, m, d)
        if len(date_list[2]) == 2:
            d = date_list[2]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)

        loan_users = LoanApplication.objects.filter(status="SUBMITTED").filter(
            created_date__startswith=new_date).order_by('-id')
        loan_users_count = LoanApplication.objects.filter(status="SUBMITTED").filter(
            created_date__startswith=new_date).count()
        paginator = Paginator(loan_users, loan_users_count)  # Show 25 contacts per page
        page = request.GET.get('page')
        try:
            users = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            users = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            users = paginator.page(paginator.num_pages)
        # return render(request, 'list.html', {'contacts': contacts})
        ctx['users'] = users
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class RecoveryUserAllDetails(generic.ListView):
    """
        Template : credit_user_all_details.html
        shows user loan application
    """
    login_required = True
    template_name = "recovery/credit_user_all_details.html"
    title = 'User Details'
    pancount = 0

    def get(self, request, id):
        ctx = {}
        try:
            loan_users = LoanApplication.objects.select_related('verified_by', 'user', 'user_professional',
                                                                'user_personal', 'aadhar_id', 'bank_sts_id',
                                                                'salary_slip_id', 'company_id', 'self_video',
                                                                'current_address', 'permanent_address', 'reference_id',
                                                                'bank_id', 'cibil_id', 'device_data_id',
                                                                'disbursed_amount').get(id=id)

            try:
                company_id = loan_users.company_id.path
                company_id = botofile.get_url(company_id)
            except:
                company_id = None

            try:
                pan = loan_users.pancard_id.path
                pan = botofile.get_url(pan)
            except:
                pan = None

            try:
                salary_slip = loan_users.salary_slip_id.path
                salary_slip = botofile.get_url(salary_slip)
            except:
                salary_slip = None

            try:
                profile_images = loan_users.self_video.path
                profile_images = botofile.get_url(profile_images)
            except:
                profile_images = None

            equfix_score = 0
            # user_value = EquiFaxMaster.objects.values_list('id').filter(user_id=loan_users.user_id).order_by('-id')
            # if user_value:
            #     score_value = EquiFaxScoreDetails.objects.values_list('score_value').filter(
            #         equifax_id=user_value[0][0]).order_by('-id')
            #     if score_value:
            #         equfix_score = score_value[0][0]

            user_value = CrifIndvMaster.objects.values_list('id').filter(user_id=loan_users.user_id).order_by('-id')
            if user_value:
                score_value = CrifScoreDetails.objects.values_list('score_value').filter(
                    equifax_id=user_value[0][0]).order_by('-id')
                if score_value:
                    equfix_score = score_value[0][0]

            loan_count = 0
            repayment_txc_count = 0
            loan_ids = LoanApplication.objects.values_list('id', 'loan_application_id', 'repayment_amount').order_by(
                'loan_application_id').distinct('loan_application_id').filter(user_id=loan_users.user_id,
                                                                              status__in=['APPROVED', 'COMPLETED'])
            loan_count = len(loan_ids)
            try:
                for id in loan_ids:
                    if id[2] is not None:
                        loan_payment_count = id[2].repayment_amount.filter(
                            status='success').count()
                        repayment_txc_count += loan_payment_count
                    else:
                        repayment_txc_count = 0
                        # TODO Raise Error
                        pass
            except:
                pass
            if loan_users.overdue:
                overdue = "This profile is under Overdue with Count :  " + str(loan_users.overdue)
            else:
                overdue = ""

            # Seraches pancard for this number
            pancount = PancardMatch(loan_users.user_personal.pan_number)

            current_address = loan_users.current_address

            # if current_address is not None:
            #     pincode = get_pincodewise_analysis(current_address.pin_code)
            #     try:
            #         if pincode:
            #             ctx['user_count'] = pincode[0]
            #             ctx['overdue'] = pincode[1]
            #         else:
            #             ctx['user_count'] = None
            #             ctx['overdue'] = None
            #     except:
            #         ctx['user_count'] = None
            #         ctx['overdue'] = None

            ctx['pan'] = pan
            ctx['company_id'] = company_id
            ctx['salary_slip'] = salary_slip

            ctx['pancount'] = pancount

            ctx['users'] = loan_users
            ctx['users_images'] = profile_images
            ctx['overdue_count'] = overdue

            ctx['smscount'] = loan_users.total_sms
            ctx['user_dpd'] = loan_users.dpd
            ctx['equfix_score'] = equfix_score
            user_ref_list = []
            default_users = []
            # Generating user dpd list
            dpd_list = []
            loan_userss = LoanApplication.objects.values_list('loan_application_id', 'dpd', 'loan_scheme_id',
                                                              'status').filter(user=loan_users.user_id,
                                                                               status__in=['APPROVED', 'COMPLETED'])
            for i in loan_userss:
                dpd_list.append([i[0], i[1], i[2], i[3]])
            if dpd_list is None:
                dpd_list = None
            ctx['user_loancount_list'] = loan_count
            ctx['user_transaction_list'] = repayment_txc_count
            ctx['balance'] = 0
            ctx['dpd_list'] = dpd_list
            ctx['NOT_FOUND'] = get_reference_details(str(loan_users.loan_application_id))

            return render(request, self.template_name, ctx)

        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class AddUser(generic.ListView):
    template_name = "recovery/add_user.html"
    title = "Add User"

    def get(self, request, *args, **kwargs):
        ctx = {}
        teams = Group.objects.all()
        if 'message' in kwargs:
            ctx['msg'] = kwargs['message']
        ctx['teams'] = teams
        return render(request, 'recovery/add_user.html', ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class AddUserFunction(generic.ListView):
    def post(self, request, *args, **kwargs):
        message = "This user is already exists.."
        username = request.POST['username']
        password = request.POST['password']
        team = request.POST['team']
        user = User.objects.filter(username=username)
        if user:
            return HttpResponseRedirect(reverse('Add_user', kwargs={'message': "This User is already registered"}))
        else:
            user = User.objects.create_user(username=username, password=password, is_superuser="True", is_staff="True")
            my_group = Group.objects.get(name=team)
            if my_group:
                my_group.user_set.add(user)
                user.save()
            return HttpResponseRedirect(reverse('Dashboard'))


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class LoanDetails(generic.ListView):
    template_name = "/loan_details.html"
    title = 'loan Details'

    def get(self, request, loan_id, *args, **kwargs):
        ctx = {}
        try:
            user_loan = LoanApplication.objects.filter(loan_application_id=loan_id)
            if user_loan:
                payment = user_loan[0].repayment_amount.all()
                ctx['payment'] = payment
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


# @register.filter(name='has_group2')
# def has_group2(user, group_name):
#     return user.groups.filter(name=group_name).exists()

# Groups
@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class AddUser(generic.ListView):
    template_name = "recovery/add_user.html"
    title = "Add User"

    def get(self, request, *args, **kwargs):
        ctx = {}
        teams = Group.objects.all()
        if 'message' in kwargs:
            ctx['msg'] = kwargs['message']
        ctx['teams'] = teams
        return render(request, 'recovery/add_user.html', ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class AddUserFunction(generic.ListView):
    def post(self, request, *args, **kwargs):
        message = "This user is already exists.."
        username = request.POST['username']
        password = request.POST['password']
        team = request.POST['team']
        user = User.objects.filter(username=username)
        if user:
            return HttpResponseRedirect(reverse('Add_user', kwargs={'message': "This User is already registered"}))
        else:
            user = User.objects.create_user(username=username, password=password, is_superuser="True", is_staff="True")
            my_group = Group.objects.get(name=team)
            if my_group:
                my_group.user_set.add(user)
                user.save()
            return HttpResponseRedirect(reverse('Dashboard'))


# Credit Verification
@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class LoanDetails(generic.ListView):
    template_name = "/loan_details.html"
    title = 'loan Details'

    def get(self, request, loan_id, *args, **kwargs):
        ctx = {}
        try:
            user_loan = LoanApplication.objects.filter(loan_application_id=loan_id)
            if user_loan:
                payment = user_loan[0].repayment_amount.all()
                ctx['payment'] = payment
            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class RecoveryLoanApplicationViewNew(generic.ListView):
    '''
    Template : recovery_userloan_application_list_new.html
    shows user loan applications by date
    '''
    template_name = "recovery/recovery_userloan_application_list_new.html"
    title = ' User Loan Application new'

    def get(self, request, status, *args, **kwargs):
        try:
            ctx = {}
            loan_users = ''
            if status == "SUBMITTED":
                loan_users = LoanApplication.objects.filter(status="SUBMITTED")
                ctx['status'] = status
            if status == "PRE_APPROVED":
                loan_users = LoanApplication.objects.filter(status="PRE_APPROVED")
                ctx['status'] = status

            if status == "PROCESS":
                loan_users = LoanApplication.objects.filter(status="PROCESS")
                ctx['status'] = status

            dataframe = read_frame(loan_users)
            dataframe['YearMonthDay'] = pd.to_datetime(dataframe['created_date']).apply(
                lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
            print(dataframe)
            day_wise_application_count = \
                dataframe.groupby(dataframe['YearMonthDay'])[
                    'loan_application_id'].count().sort_index(axis=0)
            day_wise_application_count_dict = dict(day_wise_application_count)
        except:
            pass

        ctx['application_details'] = day_wise_application_count_dict
        return render(request, self.template_name, ctx)


def rbl_save_data_process(request):
    '''
    Change Status
        if Status = Approved:
            create RBLTransaction object
            call rbl_worker()
            update LoanApplication object
            update auto approve
    :param request:
    :return:
    '''
    UpdateMessage = ""
    # try:
    loan_application_id = request.POST['user_loan_application_id']
    status = request.POST['id_status']
    # calcelReason = request.POST['calcelReason']
    calcelReason = None
    reason = request.POST['Reason']
    reason_text = request.POST['ReasonText']
    verified_id = request.user.id
    if request.user is None:
        if request.user.id is None:
            return HttpResponse("Invalid User")
        return HttpResponse("Invalid User")
    verified_id = int(verified_id)
    save_reason = None
    if (reason and reason is not None and reason != "None") and (reason_text and reason_text is not None):
        save_reason = reason + ":" + reason_text
    elif (reason and reason is not None and reason != "None"):
        save_reason = reason
    elif (reason_text and reason_text is not None):
        save_reason = reason_text
    onhold_time = request.POST['onhold_time']
    updatedate = datetime.datetime.now()
    obj = LoanApplication.objects.select_related('user', 'user_professional', 'user_personal', 'aadhar_id',
                                                 'bank_sts_id', 'salary_slip_id', 'company_id', 'self_video').get(
        loan_application_id=loan_application_id)
    date = str(obj.created_date)[:10]

    email_id = None
    UpdateMessage = ""
    if obj.status == status:
        UpdateMessage = "Already changed ... !"
        pass

    else:
        if status == 'APPROVED':
            pass

        elif status == 'PRE_APPROVED':

            UpdateMessage = "LOAN PRE APPROVED"
            # fcm_parameter_notification(**fmp_kwargs)

            email_id_1 = 'test@mudrakwik.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'MudraKwik - User PRE APPROVED In',

                'to_email': email_id_1,

                'email_template': 'emails/loan/rejected_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User Rejected In',

                },

            }

            # email(**email_kwargs_1)

            LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
                                                                                           updated_date=updatedate,
                                                                                           loan_start_date=updatedate,
                                                                                           reason=save_reason,
                                                                                           verified_date=updatedate,
                                                                                           verified_by_id=verified_id)




        elif status == 'CANCELLED':
            # print("==----Cancelled callec")

            # Cancel Reson
            # Company I'd
            # Salary slip
            # Insufficient_credit_history

            try:
                tempid = None
                if reason == "Company I'd":

                    reason = "Company i'd is missing/not clear"
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled because your " + reason + ". Please upload the clear company I'd and re-apply."
                    temid = "1007984542471492035"


                elif reason == 'Salary slip':

                    reason = "Salary slip is Missing/Unclear/Not latest"
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled because your " + reason + ". Please upload clear and Latest Salary slip and then reapply."
                    tempid = "1007585047187254857"

                elif reason == 'Insufficient_credit_history':

                    # reason = "Insufficient Credit History"
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled because your registered mobile doesn't have enough banking transaction history. Please keep on using mudrakwik expense income tracker for next 15 days and then reapply."
                    tempid = "1007477764292243140"

                elif reason == 'Selfie':

                    reason = "Selfie is missing/unclear"
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled because your " + reason + ". Please upload the clear Selfie and reapply."
                    tempid = "1007585047187254857"


                else:
                    reason = ''
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled."

                cancelled_date = datetime.datetime.now()

                sms_kwrgs = {

                    'sms_message': sms_message,

                    # 'number': '7875583679'
                    'number': str(obj.user.username),
                    "tempid": tempid

                }

                sms(**sms_kwrgs)

                email_kwargs = {

                    'subject': 'MudraKwik - Loan Cancelled',

                    'to_email': email_id,

                    'email_template': 'emails/loan/cancelled_loan.html',

                    'data': {

                        'domain': Site.objects.get_current(),

                        'loan_instance': obj,

                        'cancelled_date': cancelled_date,

                        'reason': sms_message,

                    },

                }

                email(**email_kwargs)

                fmp_kwargs = {

                    'user_id': obj.user.id,

                    'title': 'Loan Cancelled',

                    'message': sms_message,

                    'type': 'loan_cancelled',

                    'to_store': True,

                    'notification_id': obj.id,

                    'notification_key': 'loan_id',

                }

                UpdateMessage = "LOAN CANCELLED"

                email_id_1 = 'test@mudrakwik.com'
                date_1 = datetime.datetime.now()
                email_kwargs_1 = {

                    'subject': 'MudraKwik - User Cancelled',

                    'to_email': email_id_1,

                    'email_template': 'emails/loan/cancelled_loan.html',

                    'data': {

                        'domain': Site.objects.get_current(),

                        'loan_instance': obj,

                        'cancelled_date': date_1,

                        'reason': 'User Cancelled',

                    },

                }
                # print("-----loan cancelled")
                # email(**email_kwargs_1)

                # fcm_parameter_notification(**fmp_kwargs)
                # print("-----Cancel reason", calcelReason)
                # if calcelReason:
                #     LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
                #                                                                                    updated_date=updatedate,
                #                                                                                    loan_start_date=updatedate,
                #                                                                                    reason=calcelReason)
                #
                #     pass
                # else:
                LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
                                                                                               updated_date=updatedate,
                                                                                               loan_start_date=updatedate,
                                                                                               reason=save_reason,
                                                                                               verified_date=updatedate,
                                                                                               verified_by_id=verified_id)
                # print("---cancelled obj updated")
            except Exception as e:
                # print("---- loan cancelled err", e)
                # print("-------rbl save data-------", e.args)
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                pass

        elif status == 'ON_HOLD':
            tempid = None
            updatedate = datetime.datetime.now()

            email_kwargs = {
                'subject': 'MudraKwik - Loan ON_HOLD',
                'to_email': email_id,
                'email_template': 'emails/loan/pending_loan.html',
                'data': {
                    'domain': Site.objects.get_current(),
                    'loan_instance': obj,
                    'rejection_date': updatedate,
                },
            }

            email(**email_kwargs)
            sms_message = "Dear valued customer,your profile doesn't match with our eligibility criteria, please re apply after " + onhold_time + " days.You may continue using other features."

            sms_kwrgs = {
                'sms_message': sms_message,
                'number': str(obj.user.username),
                "tempid": tempid
            }

            sms(**sms_kwrgs)

            fmp_kwargs = {
                'user_id': obj.user.id,
                'title': 'Loan ON_HOLD',
                'message': sms_message,
                'type': 'ON_HOLD',
                'to_store': True,
                'notification_id': obj.id,
                'notification_key': 'loan_id',
            }
            UpdateMessage = "LOAN On Hold"
            # fcm_parameter_notification(**fmp_kwargs)

            email_id_1 = 'test@mudrakwik.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'MudraKwik - User ON_HOLD In',

                'to_email': email_id_1,

                'email_template': 'emails/loan/pending_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User ON_HOLD In',

                },

            }

            # email(**email_kwargs_1)
            # print(status, updatedate, onhold_time, updatedate)
            # print("----", onhold_time)

            if onhold_time is None:
                LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
                                                                                               updated_date=updatedate,
                                                                                               hold_days=0,
                                                                                               loan_start_date=updatedate,
                                                                                               reason=save_reason,
                                                                                               verified_date=updatedate,
                                                                                               verified_by_id=verified_id)
            else:
                LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
                                                                                               updated_date=updatedate,
                                                                                               hold_days=onhold_time,
                                                                                               loan_start_date=updatedate,
                                                                                               reason=save_reason,
                                                                                               verified_date=updatedate,
                                                                                               verified_by_id=verified_id)

        elif status == 'REJECTED':
            tempid = None
            rejection_date = datetime.datetime.now()

            email_kwargs = {
                'subject': 'MudraKwik - Loan Rejected',
                'to_email': email_id,
                'email_template': 'emails/loan/rejected_loan.html',
                'data': {
                    'domain': Site.objects.get_current(),
                    'loan_instance': obj,
                    'rejection_date': rejection_date,
                },
            }

            email(**email_kwargs)
            sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been rejected.  \nFor more details, please contact us on 8956039999 or email us on hello@mudrakwik.com "
            tempid = "1007508819500485055"
            sms_kwrgs = {
                'sms_message': sms_message,
                'number': str(obj.user.username),
                'tempid': tempid
            }

            sms(**sms_kwrgs)
            fmp_kwargs = {
                'user_id': obj.user.id,
                'title': 'Loan Rejected',
                'message': sms_message,
                'type': 'loan_rejected',
                'to_store': True,
                'notification_id': obj.id,
                'notification_key': 'loan_id',
            }
            UpdateMessage = "LOAN REJECTED"
            # fcm_parameter_notification(**fmp_kwargs)

            email_id_1 = 'test@mudrakwik.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'MudraKwik - User Rejected In',

                'to_email': email_id_1,

                'email_template': 'emails/loan/rejected_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User Rejected In',

                },

            }

            # email(**email_kwargs_1)

            LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
                                                                                           updated_date=updatedate,
                                                                                           loan_start_date=updatedate,
                                                                                           reason=save_reason,
                                                                                           verified_date=updatedate,
                                                                                           verified_by_id=verified_id)


        elif status == 'PENDING':
            tempid = None

            email_kwargs = {
                'subject': 'MudraKwik - Loan Pending',
                'to_email': email_id,
                'email_template': 'emails/loan/pending_loan.html',
                'data': {
                    'domain': Site.objects.get_current(),
                    'loan_instance': obj,
                },
            }

            email(**email_kwargs)

            sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + " is pending."

            sms_kwrgs = {
                'sms_message': sms_message,
                'number': str(obj.user.username),
                'tempid': tempid
            }

            sms(**sms_kwrgs)

            fmp_kwargs = {
                'user_id': obj.user.id,
                'title': 'Loan Pending',
                'message': sms_message,
                'type': 'loan_pending',
                'to_store': True,
                'notification_id': obj.id,
                'notification_key': 'loan_id',
            }
            UpdateMessage = "LOAN PENDING"

            email_id_1 = 'test@mudrakwik.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'MudraKwik - User Pending',

                'to_email': email_id_1,

                'email_template': 'emails/loan/pending_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User Pending',

                },

            }

            # email(**email_kwargs_1)

            # fcm_parameter_notification(**fmp_kwargs)
            LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
                                                                                           updated_date=updatedate,
                                                                                           loan_start_date=updatedate,
                                                                                           reason=save_reason,
                                                                                           verified_date=updatedate,
                                                                                           verified_by_id=verified_id)


        elif status == 'COMPLETED':

            sms_message = "We are happy to inform you that your loan application number " + obj.loan_application_id + " is completed.  \nFor more details, please contact us on 8956039999 or email us on hello@mudrakwik.com "

            fmp_kwargs = {
                'user_id': obj.user.id,
                'title': 'Loan Completed',
                'message': sms_message,
                'type': 'loan_completed',
                'to_store': True,
                'notification_id': obj.id,
                'notification_key': 'loan_id',
            }
            UpdateMessage = "LOAN COMPLETED"
            # fcm_parameter_notification(**fmp_kwargs)
            email_id_1 = 'test@mudrakwik.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'MudraKwik - User COMPLETED',

                'to_email': email_id_1,

                'email_template': 'emails/loan/pending_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User COMPLETED',

                },

            }

            # email(**email_kwargs_1)

            LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
                                                                                           updated_date=updatedate,
                                                                                           reason=save_reason,
                                                                                           verified_date=updatedate,
                                                                                           verified_by_id=verified_id)
    # dashboard_source = request.POST['recovery']
    # if dashboard_source:
    #     template_name = 'Dashboard'
    #     return HttpResponseRedirect(reverse(template_name))

    return HttpResponseRedirect(
        reverse('recovery_loan_application_list', kwargs={'status': "SUBMITTED"}))


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class RecoveryLoanApplicationView(generic.ListView):
    '''
    Template : revovery_userloan_applications_list.html
    shows user loan applications in details
    '''
    template_name = "recovery/recovery_userloan_applications_list.html"
    title = ' Recovery User Loan Application'

    def get(self, request, date, status, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        print("*************************************8date", date, type(date))
        date_list = date.split('-')
        y = date_list[0]

        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}-{}'.format(y, m, d)
        if len(date_list[2]) == 2:
            d = date_list[2]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)

        loan_users = LoanApplication.objects.filter(status__in=["SUBMITTED", "PROCESS"]).filter(
            created_date__startswith=new_date).order_by('-id')
        loan_users_count = LoanApplication.objects.filter(status=status).filter(
            created_date__startswith=new_date).count()
        paginator = Paginator(loan_users, loan_users_count)  # Show 25 contacts per page
        page = request.GET.get('page')
        try:
            users = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            users = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            users = paginator.page(paginator.num_pages)
        # return render(request, 'list.html', {'contacts': contacts})
        ctx['users'] = users
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class RecoveryUserAllDetails(generic.ListView):
    """
        Template : credit_user_all_details.html
        shows user loan application
    """
    login_required = True
    template_name = "recovery/credit_user_all_details.html"
    title = 'User Details'
    pancount = 0

    def get(self, request, id):
        ctx = {}
        try:
            loan_users = LoanApplication.objects.select_related('verified_by', 'user', 'user_professional',
                                                                'user_personal', 'aadhar_id', 'bank_sts_id',
                                                                'salary_slip_id', 'company_id', 'self_video',
                                                                'current_address', 'permanent_address', 'reference_id',
                                                                'bank_id', 'cibil_id', 'device_data_id',
                                                                'disbursed_amount').get(id=id)

            try:
                company_id = loan_users.company_id.path
                company_id = botofile.get_url(company_id)
            except:
                company_id = None

            try:
                pan = loan_users.pancard_id.path
                pan = botofile.get_url(pan)
            except:
                pan = None

            try:
                salary_slip = loan_users.salary_slip_id.path
                salary_slip = botofile.get_url(salary_slip)
            except:
                salary_slip = None

            try:
                profile_images = loan_users.self_video.path
                profile_images = botofile.get_url(profile_images)
            except:
                profile_images = None

            equfix_score = 0
            # user_value = EquiFaxMaster.objects.values_list('id').filter(user_id=loan_users.user_id).order_by('-id')
            # if user_value:
            #     score_value = EquiFaxScoreDetails.objects.values_list('score_value').filter(
            #         equifax_id=user_value[0][0]).order_by('-id')
            #     if score_value:
            #         equfix_score = score_value[0][0]

            user_value = CrifIndvMaster.objects.values_list('id').filter(user_id=loan_users.user_id).order_by('-id')
            if user_value:
                score_value = CrifScoreDetails.objects.values_list('score_value').filter(
                    equifax_id=user_value[0][0]).order_by('-id')
                if score_value:
                    equfix_score = score_value[0][0]

            loan_count = 0
            repayment_txc_count = 0
            loan_ids = LoanApplication.objects.values_list('id', 'loan_application_id', 'repayment_amount').order_by(
                'loan_application_id').distinct('loan_application_id').filter(user_id=loan_users.user_id,
                                                                              status__in=['APPROVED', 'COMPLETED'])
            loan_count = len(loan_ids)
            try:
                for id in loan_ids:
                    if id[2] is not None:
                        loan_payment_count = id[2].repayment_amount.filter(
                            status='success').count()
                        repayment_txc_count += loan_payment_count
                    else:
                        repayment_txc_count = 0
                        # TODO Raise Error
                        pass
            except:
                pass
            if loan_users.overdue:
                overdue = "This profile is under Overdue with Count :  " + str(loan_users.overdue)
            else:
                overdue = ""

            # Seraches pancard for this number
            pancount = PancardMatch(loan_users.user_personal.pan_number)

            current_address = loan_users.current_address

            # if current_address is not None:
            #     pincode = get_pincodewise_analysis(current_address.pin_code)
            #     try:
            #         if pincode:
            #             ctx['user_count'] = pincode[0]
            #             ctx['overdue'] = pincode[1]
            #         else:
            #             ctx['user_count'] = None
            #             ctx['overdue'] = None
            #     except:
            #         ctx['user_count'] = None
            #         ctx['overdue'] = None

            ctx['pan'] = pan
            ctx['company_id'] = company_id
            ctx['salary_slip'] = salary_slip

            ctx['pancount'] = pancount

            ctx['users'] = loan_users
            ctx['users_images'] = profile_images
            ctx['overdue_count'] = overdue

            ctx['smscount'] = loan_users.total_sms
            ctx['user_dpd'] = loan_users.dpd
            ctx['equfix_score'] = equfix_score
            user_ref_list = []
            default_users = []
            # Generating user dpd list
            dpd_list = []
            loan_userss = LoanApplication.objects.values_list('loan_application_id', 'dpd', 'loan_scheme_id',
                                                              'status').filter(user=loan_users.user_id,
                                                                               status__in=['APPROVED', 'COMPLETED'])
            for i in loan_userss:
                dpd_list.append([i[0], i[1], i[2], i[3]])
            if dpd_list is None:
                dpd_list = None
            ctx['user_loancount_list'] = loan_count
            ctx['user_transaction_list'] = repayment_txc_count
            ctx['balance'] = 0
            ctx['dpd_list'] = dpd_list
            ctx['NOT_FOUND'] = get_reference_details(str(loan_users.loan_application_id))

            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@login_required(login_url="/login/")
def Sharedata_Dashboard(request):
    template_name = "business_associate/BA_index_rec.html"
    ctx = {}

    Total_employee = ShareAppMaster.objects.values_list('user', flat=True)
    Total_employee = len(set(Total_employee))
    ctx['Total_B_A'] = Total_employee

    unique_share1 = ShareAppMaster.objects.values_list('number', flat=True)
    unique_share = len(set(unique_share1))
    ctx['unique_share_B_A'] = unique_share

    total_BA_sub = UserSharedData.objects.values_list('id', flat=True)
    total_BA_sub = len(set(total_BA_sub))
    ctx['total_BA_sub'] = total_BA_sub

    total_BA_sub_amt = UserSharedData.objects.values_list('id', flat=True)
    total_BA_sub_amt = len(set(total_BA_sub_amt)) * 59
    ctx['total_BA_sub_amt'] = total_BA_sub_amt

    total_BA_app = UserSharedData.objects.filter(is_approved=True, ).count()
    total_BA_app_amt = total_BA_app * 59
    ctx['total_BA_app_amt'] = total_BA_app_amt
    ctx['total_BA_app'] = total_BA_app

    reg_share = set(unique_share1)
    cnt = 0
    for number in reg_share:
        # print(number)
        user = User.objects.filter(username=number)
        if user:
            share = ShareAppMaster.objects.filter(number=number).order_by('id')
            if share:
                if share[0].created_date <= user[0].date_joined:
                    cnt += 1
    ctx['reg_share'] = cnt

    return render(request, template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class DatewiseSharedData(generic.ListView):
    '''
    Template : business_associate/datewiseshare_data.html
    '''
    template_name = "business_associate/datewiseshare_data_rec.html"
    title = 'Datewise Shared Data '

    def get(self, request, *args, **kwargs):
        day_wise_shared_count_dict = {}
        ctx = {}
        total_shared = ShareAppMaster.objects.all()
        sub_data = UserSharedData.objects.filter(shareapp_id__in=total_shared).values('shareapp')

        sub_dataframe = read_frame(sub_data)
        dataframe = read_frame(total_shared)
        dataframe['YearMonthDay'] = pd.to_datetime(dataframe['created_date']).apply(
            lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
        day_wise_shared_count = \
            dataframe.groupby(dataframe['YearMonthDay'])['number'].count().sort_index(axis=0)
        day_wise_shared_count_dict = dict(day_wise_shared_count)

        ctx['shared_details'] = day_wise_shared_count_dict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class UserwiseSharedData(generic.ListView):
    '''
    Template : lendinguser_application_list_new.html
    shows lending user applications by date
    '''
    template_name = "business_associate/userwise_share_data_rec.html"
    title = 'Datewise Shared Data '

    def get(self, request, date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''

        date_list = date.split('-')
        y = date_list[0]
        if len(date_list[1]) > 1:
            m = date_list[1]
        else:
            m = '0' + str(date_list[1])
            # new_date = '{}-{}-{}'.format(y, m, d)
        if len(date_list[2]) > 1:
            d = date_list[2]
        else:
            d = '0' + str(date_list[2])
            # new_date = '{}-{}-{}'.format(y, m, d)
        new_date = '{}-{}-{}'.format(y, m, d)
        shared_users = ShareAppMaster.objects.filter(created_date__startswith=new_date).values('user', 'number', 'id')

        dataframe = read_frame(shared_users)
        shared_users2 = ShareAppMaster.objects.filter(created_date__startswith=new_date)
        share_data = UserSharedData.objects.filter(shareapp_id__in=shared_users2, ).values('shareapp_id',
                                                                                           'shareapp__user',
                                                                                           'shareapp__id')
        sub_dataframe = read_frame(share_data)
        sub_dataframe['user'] = sub_dataframe['shareapp__user']
        sub_dataframe['id'] = sub_dataframe['shareapp__id']
        result = pd.merge(dataframe, sub_dataframe[['shareapp_id', 'id']], on='id', how='outer')

        day_wise_shared_count = \
            dataframe.groupby(result['user'])['number'].count().sort_index(axis=0)

        day_wise_shared_count_dict = dict(day_wise_shared_count)

        day_wise_sub_count = \
            result.groupby(result['user'])['shareapp_id'].count().sort_index(axis=0)

        day_wise_sub_count_dict = dict(day_wise_sub_count)

        BothDicts = [day_wise_shared_count_dict, day_wise_sub_count_dict]
        MainDict = {}
        for k in day_wise_shared_count_dict.keys():
            MainDict[k] = list(MainDict[k] for MainDict in BothDicts)
        ctx['MainData'] = MainDict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class Total_employee(generic.ListView):
    template_name = "business_associate/total_userwise_data_rec.html"
    title = 'Userwise Shared Data '

    def get(self, request, *args, **kwargs):
        ctx = {}

        total_share = ShareAppMaster.objects.all()
        dataframe = read_frame(total_share)
        total_sub = UserSharedData.objects.filter(shareapp_id__in=total_share, ).values('shareapp_id',
                                                                                        'shareapp__user',
                                                                                        'shareapp__id', 'is_approved')
        sub_dataframe = read_frame(total_sub)
        sub_dataframe['user'] = sub_dataframe['shareapp__user']
        sub_dataframe['id'] = sub_dataframe['shareapp__id']
        result = pd.merge(dataframe, sub_dataframe[['shareapp_id', 'id', 'is_approved']], on='id', how='outer')
        result['is_approved'] = (result['is_approved'] == True).astype(int)
        user_wise_shared_count = \
            dataframe.groupby(result['user'])['number'].count().sort_index(axis=0)

        user_wise_shared_count_dict = dict(user_wise_shared_count)

        user_wise_sub_count = \
            result.groupby(result['user'])['shareapp_id'].count().sort_index(axis=0)

        user_wise_sub_count_dict = dict(user_wise_sub_count)

        user_wise_app_count = \
            result.groupby(result['user'])['is_approved'].sum().sort_index(axis=0)

        user_wise_app_count_dict = dict(user_wise_app_count)

        BothDicts = [user_wise_shared_count_dict, user_wise_sub_count_dict, user_wise_app_count_dict]
        MainDict = {}
        for k in user_wise_shared_count_dict.keys():
            MainDict[k] = list(MainDict[k] for MainDict in BothDicts)
        ctx['MainData'] = MainDict

        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class EmployeeDetails(generic.ListView):
    template_name = "business_associate/employee_details_rec.html"
    title = 'Userwise Shared Data '

    def get(self, request, user, *args, **kwargs):

        '''

        :param request:
        {
            "mobile_number":"7875583671",

        }

        '''
        exist = 0
        sub_count = 0
        approved_count = 0
        not_registered = 0
        recovery = 0
        incentive = 0
        income = 0
        total_subscrption = 0
        total_approved = 0
        total_sub_paid = 0
        total_app_paid = 0
        ctx = {}

        user1 = User.objects.filter(username=user)
        user_id = user1[0].id

        all_shared = ShareAppMaster.objects.filter(user_id=user_id).count()
        all_data = ShareAppMaster.objects.filter(user_id=user_id)
        all_sub = UserSharedData.objects.filter(is_subscribed=True)
        all_approved = UserSharedData.objects.filter(is_approved=True)
        for subs in all_sub:
            if (subs.shareapp.user.id == user_id):
                sub_count += 1
                total_subscrption += subs.subscription_amount
                if subs.is_paid_forsubscription == True:
                    total_sub_paid += subs.subscription_amount

        for approved in all_approved:
            if (approved.shareapp.user.id == user_id):
                approved_count += 1
                total_approved += approved.Approved_amount
                if approved.is_paid_forapproval == True:
                    total_app_paid += approved.Approved_amount

        for number in all_data:
            usercheck = User.objects.filter(username=number.number)
            if usercheck:
                existcheck = ShareAppMaster.objects.filter(number=usercheck[0].username)
                if existcheck:
                    if usercheck[0].date_joined < existcheck[0].created_date:
                        exist += 1
            else:
                not_registered += 1

        paid = total_sub_paid + total_app_paid
        incentive = (total_subscrption - total_sub_paid) + (total_approved - total_app_paid)
        total_income = total_subscrption + total_approved
        ctx['all_shared'] = all_shared
        ctx['user'] = user
        ctx['exist'] = exist
        ctx['sub_count'] = sub_count
        ctx['approved_count'] = approved_count
        ctx['not_registered'] = not_registered
        ctx['recovery'] = recovery
        ctx['incentive'] = incentive
        ctx['paid'] = paid
        ctx['total_income'] = total_income
        ctx['total_subscrption'] = total_subscrption
        ctx['total_approved'] = total_approved
        ctx['total_sub_paid'] = total_sub_paid
        ctx['total_app_paid'] = total_app_paid

        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class EmployeeShareDetails(generic.ListView):
    template_name = "business_associate/employee_share_details_rec.html"
    title = 'Userwise Shared Data '

    def get(self, request, user, *args, **kwargs):
        '''

        :param request:
        {
            "mobile_number":"7875583671",

        }

        '''
        ctx = {}
        user1 = User.objects.filter(username=user)
        user_id = user1[0].id

        all_data = ShareAppMaster.objects.filter(user_id=user_id)

        ctx['all_data'] = all_data
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class EmployeeSubDetails(generic.ListView):
    template_name = "business_associate/employee_sub_details_rec.html"
    title = 'Userwise Shared Data '

    def get(self, request, user, *args, **kwargs):
        '''
        :param request:
        {
            "mobile_number":"7875583671",
        }
        '''
        ctx = {}
        user1 = User.objects.filter(username=user)
        user_id = user1[0].id
        all_sub = UserSharedData.objects.filter(is_subscribed=True, shareapp__user=user_id)
        ctx['all_data'] = all_sub
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class EmployeeAppDetails(generic.ListView):
    template_name = "business_associate/employee_app_details_rec.html"
    title = 'Userwise Shared Data '

    def get(self, request, user, *args, **kwargs):
        '''
        :param request:
        {
            "mobile_number":"7875583671",
        }
        '''
        ctx = {}
        user1 = User.objects.filter(username=user)
        user_id = user1[0].id
        all_app = UserSharedData.objects.filter(is_approved=True, shareapp__user=user_id)
        ctx['all_data'] = all_app
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class Employee_NotReg_Details(generic.ListView):
    template_name = "business_associate/employee_notreg_details_rec.html"
    title = 'Userwise Shared Data '

    def get(self, request, user, *args, **kwargs):
        user1 = User.objects.filter(username=user)
        user_id = user1[0].id
        ctx = {}
        data_list = []
        all_shared = ShareAppMaster.objects.filter(user_id=user_id)
        for number in all_shared:
            data = {}
            numbercheck = User.objects.filter(username=number.number)
            if numbercheck:
                pass
            else:
                data['name'] = number.name
                data['number'] = number.number
                data_list.append(data)
        ctx["user"] = data_list
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class DateReport(generic.ListView):
    def get(self, request, *args, **kwargs):
        template_name = 'recovery/report.html'
        return render(request, template_name)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class ReportBetweenDates(generic.ListView):
    def post(self, request, *args, **kwargs):
        template_name = 'recovery/new_dashboard.html'
        amount_dict = {}
        data = {}
        ctx = {}
        amount_list = []
        try:
            # print(request.user)
            if request.method == 'POST':
                start_date = request.POST['start_date']
                end_date = request.POST['end_date']
                payment_type = request.POST['type']
                Type = request.POST['Type']
                data['start_date'] = start_date
                data['end_date'] = end_date
                data['payment_type'] = payment_type
                data['Type'] = Type
                ctx['data'] = data
                s = datetime.datetime.strptime(start_date, "%Y-%m-%d").date()
                e = datetime.datetime.strptime(end_date, "%Y-%m-%d").date() + timedelta(days=1)
                print('type', data['Type'])

                if payment_type == "Disbursed":
                    users = Payments.objects.filter(date__range=(s, e))
                    if users:
                        for user in users:
                            amount = LoanApplication.objects.filter(disbursed_amount_id=user.id)
                            rrn = RBLTransaction.objects.filter(payment_id=user.id)
                            if amount:
                                amount_dict = {}
                                amount_dict['Name'] = amount[0].user_personal.name
                                amount_dict['Mobile_number'] = amount[0].user.username
                                amount_dict['LoanApplicationId'] = amount[0].loan_application_id
                                amount_dict['Loan_Amount'] = amount[0].loan_scheme_id
                                amount_dict['Amount'] = user.amount
                                amount_dict['date'] = user.date
                                amount_dict['bank_acc_no'] = amount[0].bank_id.account_number
                                amount_dict['stauts'] = user.status
                                amount_dict['RRN'] = None
                                if rrn:

                                    if rrn[0].rrn:
                                        amount_dict['RRN'] = rrn[0].rrn
                                else:
                                    amount_dict['RRN'] = None
                                amount_dict['order_ID'] = user.order_id
                                amount_dict['transaction_id'] = user.transaction_id
                                amount_dict['pg_transaction_id'] = user.pg_transaction_id
                                amount_list.append(amount_dict)
                else:
                    if Type == "Account":
                        users = Payments.objects.filter(date__range=(s, e), status__icontains="Success",
                                                        category='Loan', type='Account')
                    else:
                        users = Payments.objects.filter(date__range=(s, e), status__icontains="Success")
                    if users:
                        for user in users:
                            amounts = LoanApplication.objects.filter(repayment_amount=user.id)
                            if amounts:
                                for amount in amounts:
                                    amount_dict = {}
                                    amount_dict['Name'] = amount.user_personal.name
                                    amount_dict['Mobile_number'] = amount.user.username
                                    amount_dict['LoanApplicationId'] = amount.loan_application_id
                                    amount_dict['Loan_Amount'] = amount.loan_scheme_id
                                    amount_dict['Amount'] = user.amount
                                    amount_dict['date'] = user.date
                                    amount_dict['bank_acc_no'] = amount.bank_id.account_number
                                    amount_dict['stauts'] = user.status
                                    rrn = RBLTransaction.objects.filter(payment_id=user.id)
                                    if rrn:
                                        amount_dict['RRN'] = rrn[0].rrn
                                    else:
                                        amount_dict['RRN'] = None
                                    amount_dict['order_ID'] = user.order_id
                                    amount_dict['transaction_id'] = user.transaction_id
                                    amount_dict['pg_transaction_id'] = user.pg_transaction_id
                                    amount_list.append(amount_dict)
            ctx['user'] = amount_list
            return render(request, template_name, ctx)
        except Exception as e:
            print("------Save rbl Except-----", e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class CsvDownload(generic.ListView):
    def post(self, request, *args, **kwargs):
        # Create the HttpResponse object with the appropriate CSV header.
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="Loan_details_2019.csv"'
        writer = csv.writer(response)
        writer.writerow(
            ['Name', 'Mobile number', 'LoanApplicationId', 'Loan Amount', 'Amount', 'RRN', 'Order_ID',
             'Transaction ID', 'PG_transaction_ID', 'date', 'Bank Acc.No.',
             'Status'])
        if request.method == 'POST':
            start_date = request.POST['start_date']
            end_date = request.POST['end_date']
            payment_type = request.POST['payment_type']
            Type = request.POST['Type']
            # print('++++++++++++++++', payment_type)
            s = datetime.datetime.strptime(start_date, "%Y-%m-%d").date()
            e = datetime.datetime.strptime(end_date, "%Y-%m-%d").date() + timedelta(days=1)
            try:
                if payment_type == "Disbursed":
                    users = Payments.objects.filter(date__range=(s, e))
                    if users:
                        for user in users:
                            amount = LoanApplication.objects.filter(disbursed_amount_id=user.id)
                            rrn = RBLTransaction.objects.filter(payment_id=user.id)
                            if amount:
                                rr = None
                                if rrn:
                                    if rrn[0].rrn:
                                        rr = rrn[0].rrn
                                else:
                                    rr = None
                                writer.writerow([amount[0].user_personal.name,
                                                 amount[0].user.username,
                                                 amount[0].loan_application_id,
                                                 amount[0].loan_scheme_id,
                                                 user.amount,
                                                 rr,
                                                 user.order_id,
                                                 user.transaction_id,
                                                 user.pg_transaction_id,
                                                 user.date,
                                                 amount[0].bank_id.account_number,
                                                 user.status])
                else:
                    if Type == "Account":
                        users = Payments.objects.filter(date__range=(s, e), status__icontains="Success",
                                                        category='Loan', type='Account')
                    else:
                        users = Payments.objects.filter(date__range=(s, e), status__icontains="Success")
                    if users:
                        for user in users:
                            amount = LoanApplication.objects.filter(repayment_amount=user.id)
                            if amount:
                                writer.writerow([amount[0].user_personal.name,
                                                 amount[0].user.username,
                                                 amount[0].loan_application_id,
                                                 amount[0].loan_scheme_id,
                                                 user.amount,
                                                 "",
                                                 user.order_id,
                                                 user.transaction_id,
                                                 user.pg_transaction_id,
                                                 user.date,
                                                 amount[0].bank_id.account_number,
                                                 user.status])

            except:
                pass
            return response


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class UserContactDetails(generic.ListView):
    template_name = "recovery/user_contact_details.html"
    title = ' User Contacts Details '

    def get(self, request, user, *args, **kwargs):
        ctx = {}
        user = user
        contacts = contactmaster.objects.filter(user=user)
        ctx['contacts'] = contacts
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class UserWiseLoanDetailsView(generic.ListView):
    template_name = "recovery/user_loan_details_list.html"
    title = 'Active User'

    def get(self, request, username, *args, **kwargs):
        ctx = {}
        try:
            user = User.objects.get(username=username)
            loan_users = LoanApplication.objects.filter(user=user).filter(status__in=['APPROVED', 'COMPLETED'])

            ctx['user_loan'] = loan_users

            return render(request, self.template_name, ctx)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class MonthwiseDefaultersAllocated(generic.ListView):
    template_name = 'recovery/monthwise_defaulters_allocated.html'

    def get(self, request, user, *args, **kwargs):
        ctx = {}
        data_dict = {}
        recovered_amount = 0
        try:
            user_group = User.objects.filter(groups__name="Recovery Supervisor", id=request.user.id, is_active='t')
            ctx['user_group'] = user_group
            # print('userrrrrrrrrrr0', user)

            recovery_data = Recovery.objects.filter(user__username=user)
            # print('recovery_data', recovery_data, len(recovery_data))
            recovery_dataframe = read_frame(recovery_data)

            recovery_dataframe['YearMonth'] = pd.to_datetime(recovery_dataframe['created_date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            monthwise_allocated_ = \
                recovery_dataframe.groupby(recovery_dataframe['YearMonth'])['id'].count().sort_index(axis=0)
            monthwise_allocated__dict = dict(monthwise_allocated_)
            # print('monthwise_allocated__dict', monthwise_allocated__dict)

            for month in monthwise_allocated__dict:

                data_list = []
                # print('month', month)
                new_date = ""
                date_list = month.split('-')
                y = date_list[0]
                if len(date_list[1]) > 1:
                    m = date_list[1]
                    new_date = '{}-{}'.format(y, m)
                else:
                    m = '0' + str(date_list[1])
                    new_date = '{}-{}'.format(y, m)

                # print('new month', new_date)
                total_allocated = Recovery.objects.values_list('id', flat=True).filter(
                    created_date__startswith=new_date,
                    user__username=user)
                # print('total_allocated', '++++', len(total_allocated), '--------', total_allocated)

                recovered_count = Recovery.objects.filter(completed_date__startswith=new_date,
                                                          recovery_status="Yes", user__username=user).count()

                # print('recovered_count', recovered_count)
                # remaining_count = len(total_allocated) - recovered_count
                remaining_count = Recovery.objects.filter(id__in=total_allocated).filter(
                    created_date__startswith=new_date,
                    recovery_status="No", user__username=user).count()
                # print('remaining_count', remaining_count)
                insentive_amount = Recovery.objects.filter(id__in=total_allocated).filter(
                    completed_date__startswith=new_date,
                    recovery_status="Yes", user__username=user).aggregate(
                    Sum('insentive_amount'))['insentive_amount__sum']

                total_amount = Recovery.objects.values_list('loan_transaction__loan_scheme_id',
                                                            flat=True).filter(id__in=total_allocated).filter(
                    recovery_status="No",
                    created_date__startswith=new_date,
                    user__username=user)
                # print('total_amount', total_amount)
                if total_amount and total_amount is not None:
                    total_amount = reduce(lambda a, b: int(a) + int(b), total_amount)
                else:
                    total_amount = 0
                # print('lambdaaaaaaaaaaaaaaaaaaaaaaaaaaa', total_amount)

                loan_id = Recovery.objects.values_list('loan_transaction', flat=True).filter(
                    id__in=total_allocated).filter(recovery_status="No", created_date__startswith=new_date,
                                                   user__username=user)
                if loan_id:
                    # print('loan_id', loan_id)
                    loans = LoanApplication.objects.filter(id__in=loan_id)
                    # print('loansssssssssssssssssss', loans)
                    repayment_amount = 0
                    if loans:
                        for id in loans:
                            rep_amount = id.repayment_amount.filter(
                                status='success').aggregate(Count('amount'))['amount__count']
                            repayment_amount += rep_amount
                    final_remaining_amount = int(total_amount) - repayment_amount

                    # print('total_amount', total_amount)
                    # print('final_remaining_amount', final_remaining_amount)
                    # # print('amount', recovered_amount)
                    if insentive_amount and insentive_amount is not None:
                        recovered_amount = insentive_amount * 40
                    data_list.extend((user, monthwise_allocated__dict[month], recovered_count, remaining_count,
                                      insentive_amount, recovered_amount, final_remaining_amount))
                    # print('data_list', data_list)
                    data_dict.update({month: data_list})
            # print('final output0000000000000000000', data_dict)
            ctx['data'] = data_dict
            return render(request, self.template_name, ctx)
        except Exception as e:
            print("------exc", e)
            print('-----------in exception----------')
            print(e.args)
            import os
            import sys
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class AmountWiseDefaulters(generic.ListView):
    template_name = 'recovery/amountwise_defaulters.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        data = {}
        # print("---keg", kwargs)
        # month = "2019-10"
        user = kwargs['user']
        month = kwargs['month'] + '-' + kwargs['day']
        # print('====================', month)
        new_date = ""
        date_list = month.split('-')
        y = date_list[0]
        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}'.format(y, m)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}'.format(y, m)
        try:
            # print('new_date', new_date)
            # print('inside try')
            defaulters = Recovery.objects.values_list('loan_transaction', flat=True).filter(
                created_date__startswith=new_date, recovery_status="No",
                user__username=user)
            if defaulters:
                loans = LoanApplication.objects.filter(id__in=defaulters)
                loans_dataframe = read_frame(loans)

                loans_amount = loans_dataframe.groupby(loans_dataframe['loan_scheme_id'])['id'].count().sort_index(
                    axis=0)
                loans_amount_dict = dict(loans_amount)
                # print('loans_amount_dict', loans_amount_dict)

                # print('defaulters', defaulters)
                ctx['data'] = loans_amount_dict
                ctx['user'] = user
                ctx['month'] = new_date
                # print('data', data)
            return render(request, self.template_name, ctx)
        except Exception as e:
            print("------exc", e)
            print('-----------in exception----------')
            print(e.args)
            import os
            import sys
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


# def allocate_defaulters_to_agent():
#     try:
#         recovery_data = Recovery.objects.all()
#         if recovery_data:
#             limit_amount = DistributionLimitAmount.objects.filter(status="Active")
#             if limit_amount:
#                 limit_amount = limit_amount[0]
#             else:
#                 DistributionLimitAmount.objects.create(amount=3000)
#                 limit_amount = DistributionLimitAmount.objects.filter(status="Active")[0]
#             print("---limit mt", limit_amount)
#             mudraamount = []
#             for i in scheme:
#                 amount = scheme[i]['ms_amount']
#                 if int(amount) <= int(limit_amount.amount):
#                     mudraamount.append(amount)
#             defaulter = getDefaulterslist(mudraamount)
#             len_defaulter = len(defaulter)
#             user_group = User.objects.filter(groups__name="Recovery Team", username='Amar_k')
#             if user_group:
#                 len_user = len(user_group)
#             else:
#                 len_user = 0
#             commission_percent = RecoveryCommission.objects.filter(status="Active")
#             if commission_percent:
#                 commission_percent = commission_percent[0]
#             else:
#                 RecoveryCommission.objects.create(commission_percent='2.5')
#             for i in range(len_defaulter):
#                 defaulter_total_len = len(defaulter[i])
#                 user_div_len = defaulter_total_len / len_user
#                 count = user_div_len
#                 user_count = 0
#
#                 for j in range(defaulter_total_len):
#                     if j >= count:
#                         count += user_div_len
#                         user_count += 1
#
#                     defaulter_check = Recovery.objects.filter(loan_transaction_id=defaulter[i][j])
#                     print("---------------rec alreay", defaulter_check)
#                     if not defaulter_check:
#                         Recovery.objects.create(loan_transaction_id=defaulter[i][j],
#                                                 user_id=user_group[user_count].id)
#
#         return True
#     except Exception as e:
#         print("-------Exception in Dafaulter Allocate in 48 hrs-------", e.args)
#         exc_type, exc_obj, exc_tb = sys.exc_info()
#         fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
#         print(exc_type, fname, exc_tb.tb_lineno)
#         return False

def allocate_def(self):
    mudraamount = []
    msg = ''
    for i in scheme:
        amount = scheme[i]['ms_amount']
        mudraamount.append(amount)

    today = datetime.datetime.now()
    user_group = User.objects.filter(groups__name="Recovery Team", username='Amar_k')
    for i in mudraamount:
        amount = i
        # print("+++________ rep amt", amount)
        loan_users = LoanApplication.objects.filter(status='APPROVED', loan_scheme_id=amount)
        defaulter = []
        for user in loan_users:
            try:
                return_date = user.loan_end_date
                # print("return_date")
                # print(return_date)
                if return_date:
                    if today > return_date:
                        defaulter_check = Recovery.objects.filter(loan_transaction_id=user.id)
                        # print("---------------rec already exists", defaulter_check)
                        msg = 'Record already Exists.'
                        if not defaulter_check:
                            Recovery.objects.create(loan_transaction_id=user.id,
                                                    user_id=user_group[0].id)
                            print('New recovery record created')
                            msg = 'New Records Created'
                else:
                    #  ToDo raise error
                    pass
            except Exception as e:
                print("------USer all details exc", e)
                print('-----------in exception----------')
                print(e.args)
                import os
                import sys
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                pass
    return HttpResponse(msg)


def auto_allocate_def():
    get_last_all_date = Recovery.objects.values_list('created_date').order_by('-created_date').first()

    get_last_all_date = get_last_all_date[0]
    today = datetime.datetime.now()
    date_diff = today - get_last_all_date

    if date_diff >= timedelta(hours=48):
        # if date_diff >= timedelta(minutes=1):
        allocate_def()
        return True
    else:
        return False


# from apscheduler.schedulers.background import BackgroundScheduler
#
# try:
#     scheduler = BackgroundScheduler()
#     allocate = scheduler.add_job(auto_allocate_def, 'cron', day='1-31', hour='9', minute='30')
#     scheduler.start()
# except Exception as e:
#     pass


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class MultilpleDefaultersAllocate(generic.ListView):
    template_name = 'recovery/amountwise_defaulters.html'

    def post(self, request, *args, **kwargs):
        # print('inside post')
        forwarded_by = request.user
        # print('forwarded_by', forwarded_by)
        if request.method == "POST":
            data = request.POST.getlist('checkbox')
            user = request.POST['forward_user_id']
            forwarded_to = User.objects.filter(id=user)
            # print('forwarded_to', forwarded_to)
            # print('data', data)
            # print('user', user)
            defaulters = Recovery.objects.filter(loan_transaction__in=data)
            # print('defaulters', defaulters)
            if defaulters:
                update = Recovery.objects.filter(id__in=defaulters).update(user=user)
                # print('update', update)
            return HttpResponseRedirect(reverse('Overdue_Details_view'))


# Following functions are copied from lead/views.py for yelo leads from API.
@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class YeloMonthwise_Details(generic.ListView):
    '''
        Template : yelouser_application_list_new.html
        shows yelo user applications by date
        '''
    template_name = "recovery/yelo_user_details_monthwise.html"
    title = ' yelo UserApplication new'

    def get(self, request, *args, **kwargs):
        try:
            ctx = {}
            yelo_leads = YeloLeads.objects.all()
            dataframe = read_frame(yelo_leads)
            dataframe['Month'] = pd.to_datetime(dataframe['click_date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            month_wise_application_count = \
                dataframe.groupby(dataframe['Month'])['mobile'].count().sort_index(axis=0)
            month_wise_application_count_dict = dict(month_wise_application_count)
        except:
            pass
        ctx['application_details'] = month_wise_application_count_dict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class YeloApplicationViewNew(generic.ListView):
    '''
    Template : yelouser_application_list_new.html
    shows yelo user applications by date
    '''
    template_name = "recovery/yelouser_application_list_new.html"
    title = ' yelo UserApplication new'

    def get(self, request, month, *args, **kwargs):
        try:
            ctx = {}
            new_month = ''
            m = ''
            month_list = month.split('-')
            y = month_list[0]
            # print(y)
            if len(month_list[1]) > 1:
                m = month_list[1]
            else:
                m = '0' + str(month_list[1])
                # new_date = '{}-{}-{}'.format(y, m, d
                # new_date = '{}-{}-{}'.format(y, m, d)
            new_date = '{}-{}'.format(y, m, )
            ctx = {}
            yelo_leads = YeloLeads.objects.filter(click_date__startswith=new_date)
            dataframe = read_frame(yelo_leads)
            dataframe['YearMonthDay'] = pd.to_datetime(dataframe['click_date']).apply(
                lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
            day_wise_application_count = \
                dataframe.groupby(dataframe['YearMonthDay'])['mobile'].count().sort_index(axis=0)
            day_wise_application_count_dict = dict(day_wise_application_count)

        except:
            pass
        ctx['application_details'] = day_wise_application_count_dict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class YeloSubscribedView(generic.ListView):
    '''
    Template : yelo_Subscribed_user_list.html
    shows yelo Subscribed user applications by date
    '''
    template_name = "recovery/yelo_Subscribed_user_list.html"
    title = ' yelo Subscribed View'

    def get(self, request, *args, **kwargs):
        ctx = {}
        try:
            yelo_sub_leads = YeloLeads.objects.filter(is_subscribed=True)
            sub_dataframe = read_frame(yelo_sub_leads)
            # print('sub_dataframe', sub_dataframe)
            sub_dataframe['YearMonthDay'] = pd.to_datetime(sub_dataframe['click_date']).apply(
                lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
            day_wise_application_sub_count = \
                sub_dataframe.groupby(sub_dataframe['YearMonthDay'])['is_subscribed'].count().sort_index(axis=0)
            day_wise_application_sub_count_dict = dict(day_wise_application_sub_count)
        except:
            pass
        ctx['yelo_sub_total'] = day_wise_application_sub_count_dict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class YeloPaidAmountView(generic.ListView):
    '''
        Template : yelo_paid_amount.html
        shows yelo amount to be paid  by date
        '''

    template_name = "recovery/yelo_paid_amount.html"
    title = ' yelo Subscribed View'

    def get(self, request, *args, **kwargs):
        day_wise_amount_dict = {}
        ctx = {}
        try:
            yelo_sub_leads = YeloLeads.objects.filter(is_subscribed=True)
            amount_dataframe = read_frame(yelo_sub_leads)
            # print('amount_dataframe', amount_dataframe)
            amount_dataframe['YearMonthDay'] = pd.to_datetime(amount_dataframe['click_date']).apply(
                lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
            # day_wise_amount = \
            # amount_dataframe.groupby(amount_dataframe['YearMonthDay']).aggregate(Sum('paidToYelo'))['paidToYelo__sum']

            day_wise_amount = \
                amount_dataframe.groupby(amount_dataframe['YearMonthDay'])['paidToYelo'].sum().sort_index(axis=0)
            day_wise_amount_dict = dict(day_wise_amount)
        except:
            pass
        ctx['yelo_sub_Amount'] = day_wise_amount_dict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class YeloInstalledLeadsView(generic.ListView):
    pass


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class YeloDatewiseLeadsView(generic.ListView):
    '''
    Template : yelo_user_details_datewise.html
    shows yelo user details in details
    '''

    template_name = "recovery/yelo_user_details_datewise.html"
    title = ' Yelo User Details'

    def get(self, request, date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        date_list = date.split('-')
        y = date_list[0]
        # print(y)
        if len(date_list[1]) > 1:
            m = date_list[1]
        else:
            m = '0' + str(date_list[1])
            # new_date = '{}-{}-{}'.format(y, m, d)
        if len(date_list[2]) > 1:
            d = date_list[2]
        else:
            d = '0' + str(date_list[2])
            # new_date = '{}-{}-{}'.format(y, m, d)
        new_date = '{}-{}-{}'.format(y, m, d)
        loan_users = YeloLeads.objects.filter(click_date__startswith=new_date).order_by('-id')
        loan_users_count = YeloLeads.objects.filter(click_date__startswith=new_date).count()
        # print(loan_users)
        paginator = Paginator(loan_users, loan_users_count)  # Show 25 contacts per page
        page = request.GET.get('page')
        try:
            users = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            users = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            users = paginator.page(paginator.num_pages)
        # return render(request, 'list.html', {'contacts': contacts})
        ctx['users'] = users
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class YeloDatewiseSubsLeadsView(generic.ListView):
    '''
    Template : Leads_Subscribed_datewise.html
    shows yelo Subscribed user details
    '''
    template_name = "recovery/Leads_Subscribed_datewise.html"
    title = ' Yelo Subscriptions Details'

    def get(self, request, date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        date_list = date.split('-')
        y = date_list[0]

        if len(date_list[1]) > 1:
            m = date_list[1]
        else:
            m = '0' + str(date_list[1])
            # new_date = '{}-{}-{}'.format(y, m, d)
        if len(date_list[2]) > 1:
            d = date_list[2]
        else:
            d = '0' + str(date_list[2])
            # new_date = '{}-{}-{}'.format(y, m, d)
        new_date = '{}-{}-{}'.format(y, m, d)
        loan_users = YeloLeads.objects.filter(click_date__startswith=new_date).filter(is_subscribed=True).order_by(
            '-id')
        loan_users_count = YeloLeads.objects.filter(click_date__startswith=new_date).filter(is_subscribed=True).count()
        # print(loan_users)
        paginator = Paginator(loan_users, loan_users_count)  # Show 25 contacts per page
        page = request.GET.get('page')
        try:
            users = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            users = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            users = paginator.page(paginator.num_pages)
        # return render(request, 'list.html', {'contacts': contacts})
        ctx['users'] = users
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class MonthwiseDynamicRegistered(generic.ListView):
    template_name = "recovery/monthwise_total_registered.html"
    title = ' Monthwise Dynamic Registered '

    def get(self, request, *args, **kwargs):
        ctx = {}
        user = request.user
        print('user', user.username)
        dataframe = pd.DataFrame(columns=['Username', 'date_joined'])
        leads = PartnerLeads.objects.filter(aff_id__partner_id__partner_name=user.username).order_by(
            'mobile_number', '-created_date').distinct('mobile_number')

        for lead in leads:
            user = User.objects.filter(username=lead.mobile_number)
            if user:
                if lead.created_date <= user[0].date_joined:
                    dataframe = dataframe.append({"Username": user[0].username, "date_joined": user[0].date_joined},
                                                 ignore_index=True)
        # print('hkdsbvkhbsakhbvhb', dataframe)
        dataframe['YearMonth'] = pd.to_datetime(dataframe['date_joined']).apply(
            lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
        monthwise_total_registered = \
            dataframe.groupby(dataframe['YearMonth'])['Username'].count().sort_index(axis=0)
        monthwise_total_registered = dict(monthwise_total_registered)
        # print('monthwise_total_registered', monthwise_total_registered)
        ctx['MainData'] = monthwise_total_registered
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class DaywiseDaynamicRegistered(generic.ListView):
    template_name = 'recovery/daywise_dynamic_registered.html'

    def get(self, request, on_month, *args, **kwargs):
        ctx = {}
        user = request.user
        date_list = on_month.split('-')
        y = date_list[0]
        if len(date_list[1]) > 1:
            m = date_list[1]
        else:
            m = '0' + str(date_list[1])

        new_date = '{}-{}'.format(y, m)
        dataframe = pd.DataFrame(columns=['Username', 'date_joined'])
        leads = PartnerLeads.objects.filter(aff_id__partner_id__partner_name=user.username).order_by(
            'mobile_number', '-created_date').distinct('mobile_number')

        for lead in leads:
            user = User.objects.filter(username=lead.mobile_number, date_joined__startswith=new_date)
            if user:
                if lead.created_date <= user[0].date_joined:
                    dataframe = dataframe.append({"Username": user[0].username, "date_joined": user[0].date_joined},
                                                 ignore_index=True)
        # print('hkdsbvkhbsakhbvhb', dataframe)
        dataframe['YearMonthDay'] = pd.to_datetime(dataframe['date_joined']).apply(
            lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
        daywise_total_registered = \
            dataframe.groupby(dataframe['YearMonthDay'])['Username'].count().sort_index(axis=0)
        daywise_total_registered = dict(daywise_total_registered)
        # print('monthwise_total_registered', daywise_total_registered)
        ctx['MainData'] = daywise_total_registered
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class DaynamicRegisteredOndate(generic.ListView):
    template_name = 'recovery/dynamic_registered_ondate.html'

    def get(self, request, on_date, *args, **kwargs):
        ctx = {}
        user = request.user
        data_dict = {}
        date_list = on_date.split('-')
        y = date_list[0]
        if len(date_list[1]) > 1:
            m = date_list[1]
        else:
            m = '0' + str(date_list[1])
        if len(date_list[2]) > 1:
            d = date_list[2]
        else:
            d = '0' + str(date_list[2])

        new_date = '{}-{}-{}'.format(y, m, d)

        new_date = '{}-{}'.format(y, m)
        leads = PartnerLeads.objects.filter(aff_id__partner_id__partner_name=user.username).order_by(
            'mobile_number', '-created_date').distinct('mobile_number')

        for lead in leads:
            user = User.objects.filter(username=lead.mobile_number, date_joined__startswith=new_date)
            if user:
                if lead.created_date <= user[0].date_joined:
                    # print('hereeeeeeeeeeeeeee')
                    data_dict.update({user[0].username: user[0].date_joined})
        # print('data_dict', data_dict)
        ctx['MainData'] = data_dict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class MonthwiseDynamicApproved(generic.ListView):
    template_name = "recovery/monthwise_total_approved.html"
    title = ' Monthwise Dynamic Approved '

    def get(self, request, *args, **kwargs):
        ctx = {}
        user = request.user
        approved_loans = DynamicDisbursement.objects.filter(is_approved=True,
                                                            lead_id__aff_id__partner_id__partner_name=user.username)
        # print('hkdsbvkhbsakhbvhb', approved_loans)
        dataframe = read_frame(approved_loans)
        dataframe['YearMonth'] = pd.to_datetime(dataframe['created_date']).apply(
            lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
        monthwise_total_approved = \
            dataframe.groupby(dataframe['YearMonth'])['id'].count().sort_index(axis=0)
        monthwise_total_approved = dict(monthwise_total_approved)
        # print('monthwise_total_approved', monthwise_total_approved)
        ctx['MainData'] = monthwise_total_approved
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class DaywiseDaynamicApproved(generic.ListView):
    template_name = 'recovery/daywise_dynamic_approved.html'

    def get(self, request, on_month, *args, **kwargs):
        ctx = {}
        user = request.user
        date_list = on_month.split('-')
        y = date_list[0]
        if len(date_list[1]) > 1:
            m = date_list[1]
        else:
            m = '0' + str(date_list[1])

        new_date = '{}-{}'.format(y, m)
        approved_loans = DynamicDisbursement.objects.filter(is_approved=True,
                                                            lead_id__aff_id__partner_id__partner_name=user.username)
        # print('hkdsbvkhbsakhbvhb', approved_loans)
        dataframe = read_frame(approved_loans)
        dataframe['YearMonthDay'] = pd.to_datetime(dataframe['created_date']).apply(
            lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
        daywise_total_approved = \
            dataframe.groupby(dataframe['YearMonthDay'])['id'].count().sort_index(axis=0)
        daywise_total_approved = dict(daywise_total_approved)
        # print('monthwise_total_approved', daywise_total_approved)
        ctx['MainData'] = daywise_total_approved
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class DaynamicApprovedOndate(generic.ListView):
    template_name = 'recovery/dynamic_approved_ondate.html'

    def get(self, request, on_date, *args, **kwargs):
        ctx = {}
        user = request.user
        data_dict = {}
        date_list = on_date.split('-')
        y = date_list[0]
        if len(date_list[1]) > 1:
            m = date_list[1]
        else:
            m = '0' + str(date_list[1])
        if len(date_list[2]) > 1:
            d = date_list[2]
        else:
            d = '0' + str(date_list[2])
        new_date = '{}-{}-{}'.format(y, m, d)
        leads = DynamicDisbursement.objects.filter(is_approved=True,
                                                   lead_id__aff_id__partner_id__partner_name=user.username,
                                                   created_date__startswith=new_date).values_list(
            'lead_id', flat=True)
        # print('leads', leads)
        partner_leads = PartnerLeads.objects.filter(id__in=leads)
        # print('partner_leads', partner_leads)
        if partner_leads:
            for lead in partner_leads:
                user = User.objects.filter(username=lead.mobile_number)
                if user:
                    limit_date = user[0].date_joined + timedelta(days=2)
                    # print('limit_date', limit_date)
                    # print('userrrrrrrrrrrrrrr', user[0].id)
                    if user[0].date_joined > lead.created_date:
                        # print('inside if----------')
                        # checking 1st loan or not
                        loans = LoanApplication.objects.filter(user_id=user[0].id, status="APPROVED",
                                                               loan_scheme_id="1000").order_by(
                            'loan_start_date').last()
                        # print('loans', loans.id)
                        if loans:
                            previous_loans = LoanApplication.objects.filter(user_id=user[0].id).exclude(
                                loan_application_id=loans.loan_application_id)
                            # print('previous_loans', previous_loans)
                            if not previous_loans:
                                # print('inside not previous_loans')
                                if loans.loan_start_date <= limit_date:
                                    # print('hereeeeeeeeeeeeeeeeeee')
                                    data_dict.update({user[0].username: [loans.loan_application_id,
                                                                         loans.loan_start_date, loans.status]})
                        # print('data dict', data_dict)
        ctx['MainData'] = data_dict
        return render(request, self.template_name, ctx)


class UnallocatedDefaulterList(generic.ListView):
    template_name = 'recovery/new_defaulter_list.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        allocated_list = []
        default_counter = 0
        today = datetime.datetime.now()
        yesterday = datetime.datetime.now() - timedelta(days=1)
        before_yesterday = datetime.datetime.now() - timedelta(days=2)
        users = LoanApplication.objects.filter(status="APPROVED")
        if users:
            for user in users:
                if user.status != 'COMPLETED':
                    for i in scheme:
                        if i == user.loan_scheme_id:
                            if user.loan_end_date:
                                return_date = user.loan_end_date + timedelta(int(scheme[i]['ms_tenure']))
                                if (yesterday - return_date).days == 1:
                                    default_counter += 1
                                return_date = user.loan_end_date
                                if return_date:
                                    # day before yesterdays defaulter
                                    # if str(return_date)[:10] == str(today)[:10]:
                                    #     # Don't allocate today's defaulters
                                    #     pass
                                    # elif str(before_yesterday)[:10] >= str(return_date)[:10]:
                                    # All Dafaulters
                                    if today > return_date:
                                        # users in list
                                        defaulter_check = Recovery.objects.filter(loan_transaction_id=user.id)
                                        if not defaulter_check:
                                            if user.repayment_amount is None or not user.repayment_amount:
                                                repayment_amount = 0
                                            else:
                                                repayment_amount = \
                                                    user.repayment_amount.filter(status__icontains='success').aggregate(
                                                        Sum('amount'))['amount__sum']
                                            if repayment_amount is None or not repayment_amount:
                                                repayment_amount = 0
                                            balance_amount = int(user.loan_scheme_id) - repayment_amount
                                            data_dict = {repayment_amount: [user, balance_amount]}
                                            allocated_list.append(data_dict)
                                            ctx['user'] = allocated_list
                                            break
        return render(request, self.template_name, ctx)


class CreateNotification(generic.ListView):
    """
       This class is for Adding Notification...
    """
    template_name = "recovery/create_notification.html"

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {})

    def post(self, request, *args, **kwargs):
        user = request.user
        mobile = None
        count = 0
        if user:
            if request.method == "POST":
                status = request.POST.get('status', '') == 'on'
                title = request.POST['title']
                name = request.POST['name']

                image = None
                if "notification_image" in request.FILES:
                    image = request.FILES['notification_image']
                type = request.POST['type']
                description = request.POST['description']
                obj = NotificationTemplate.objects.create(title=title, name=name, path=image, status=status, type=type,
                                                          description=description,
                                                          # created_by=user
                                                          )
                if image is not None:
                    base_path = settings.BASE_DIR + "/media/notification/"
                    file_path = "notification/"
                    new_base_path = base_path + str(image)
                    new_file_path = file_path + str(image)
                    # upload_image(new_base_path, new_file_path)

                if "mobile_number" in request.POST:

                    mobile = request.POST['mobile_number']

                    if mobile:
                        x = mobile.replace("\r", "")
                        mobile = x.split()
                        count = len(mobile)
                        NotificationTask.objects.create(total_count=count, notification_template=obj)

                    else:

                        mobile = FCMDevice.objects.count()
                        if mobile:
                            count = mobile

                        NotificationTask.objects.create(total_count=count, notification_template=obj)

        return redirect('/recovery/notification_list/')


class EditNotification(generic.ListView):
    template_name = "recovery/edit_notification.html"

    def get(self, request, *args, id, **kwargs):
        ctx = {}
        object = NotificationTemplate.objects.filter(id=id)
        if object:

            ctx["data"] = object[0]
        else:
            ctx["message"] = "No data found"

        return render(request, self.template_name, ctx)

    def post(self, request, *args, id, **kwargs):

        if request.method == "POST":
            title = request.POST['title']
            name = request.POST['name']
            type = request.POST['type']
            status = request.POST['status']
            obj_id = request.POST['id']
            description = request.POST['description']

            NotificationTemplate.objects.filter(id=obj_id).update(title=title, name=name, status=status, type=type,
                                                                  description=description,
                                                                  updated_date=datetime.datetime.now())

        return redirect('/recovery/notification_list/')


def notification_list(request):
    """
    This function is for Notification LIst...
    """
    ctx = {}
    data_dict = {}
    notification_obj = NotificationTemplate.objects.all()
    if notification_obj:
        for notification in notification_obj:
            notification_path = notification.path
            if notification_path:
                notification_image = botofile.get_url(notification_path)
            else:
                notification_image = None
            data_dict.update({notification: notification_image})
        ctx['notifications'] = data_dict
    return render(request, 'recovery/notification_list.html', ctx)


def send_notification(request):
    ctx = {}
    data_dict = {}
    noti_obj = NotificationTemplate.objects.all()
    if noti_obj:
        for data in noti_obj:
            data_dict.update({
                data.id: data.title
            })
    ctx["data"] = data_dict
    return render(request, "recovery/send_notification.html", ctx)


def create_notification_dropdown(request):
    user = request.user
    mobile = None
    count = 0
    if user:
        if request.method == "POST":
            id = request.POST['notification']
            mobile = request.POST['mobile_number']
            if id:
                obj = NotificationTemplate.objects.filter(id=id)
                if obj:
                    if mobile:
                        x = mobile.replace("\r", "")
                        mobile_list = x.split()
                        count = len(mobile_list)
                        devices = FCMDevice.objects.filter(user__username__in=mobile_list)

                        NotificationTask.objects.create(total_count=count, notification_template=obj[0])

                    else:
                        devices = FCMDevice.objects.all()
                        devices_count = len(devices)
                        if devices_count:
                            count = devices_count
                        NotificationTask.objects.create(total_count=count, notification_template=obj[0])

                    # send notification
                    notification_resp = devices.send_message(title=obj[0].title, body=obj[0].description)
                    print("notification_resp", notification_resp)

    return redirect('/recovery/notification_list/')
