# Generated by Django 2.2.4 on 2022-02-18 11:29

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('Recovery', '0006_auto_20210309_1345'),
    ]

    operations = [
        migrations.RenameField(
            model_name='recovery',
            old_name='loan_application_id',
            new_name='loan_transaction',
        ),
        migrations.RemoveField(
            model_name='recovery',
            name='installment',
        ),
        migrations.RemoveField(
            model_name='recovery',
            name='order_details',
        ),
        migrations.RemoveField(
            model_name='recovery',
            name='pending_installment_amount',
        ),
        migrations.RemoveField(
            model_name='recovery',
            name='type',
        ),
        migrations.AddField(
            model_name='recovery',
            name='insentive_amount',
            field=models.FloatField(blank=True, db_index=True, null=True),
        ),
        migrations.AlterField(
            model_name='recovery',
            name='user',
            field=models.ForeignKey(default=17022022, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.CreateModel(
            name='NotificationTemplate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=250, null=True)),
                ('title', models.CharField(blank=True, max_length=250, null=True)),
                ('path', models.ImageField(blank=True, db_index=True, max_length=256, null=True, upload_to='fcm_images')),
                ('status', models.BooleanField(default=True)),
                ('type', models.CharField(blank=True, choices=[('App', 'App'), ('Web', 'Web'), ('Both', 'Both')], max_length=25, null=True)),
                ('description', models.CharField(blank=True, max_length=256, null=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_notification_created_by', to=settings.AUTH_USER_MODEL)),
                ('updated_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_notification_updated_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='NotificationTask',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('total_count', models.CharField(blank=True, max_length=250, null=True)),
                ('delivered_count', models.CharField(blank=True, max_length=250, null=True)),
                ('status', models.BooleanField(default=True)),
                ('remark', models.CharField(blank=True, max_length=250, null=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_notification_task_created_by', to=settings.AUTH_USER_MODEL)),
                ('notification_template', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='notification_template', to='Recovery.NotificationTemplate')),
                ('updated_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_notification_task_updated_by', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
