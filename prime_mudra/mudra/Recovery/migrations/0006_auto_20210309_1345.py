# Generated by Django 2.2.4 on 2021-03-09 13:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('LoanAppData', '0022_auto_20210309_1345'),
        ('Recovery', '0005_auto_20210113_1414'),
    ]

    operations = [
        migrations.AddField(
            model_name='recovery',
            name='loan_application_id',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='LoanAppData.LoanApplication'),
        ),
        migrations.AddField(
            model_name='recovery',
            name='type',
            field=models.CharField(blank=True, choices=[('Ecom', 'Ecom'), ('Loan', 'Loan'), ('NPCI', 'NPCI')], default='Ecom', max_length=20, null=True),
        ),
    ]
