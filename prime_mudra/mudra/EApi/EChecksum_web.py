import datetime
import json
import random

import requests
from MApi.checksum_web import generate_checksum
from django.apps.registry import apps
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Sum
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics
from rest_framework.authentication import *
from rest_framework.permissions import *
from rest_framework.response import Response
from rest_framework.views import APIView
from math import ceil

from .credit import *
from .ecom_email import order_sms
from .wallet import user_wallet_sub, user_wallet_update

# from .order import get_orders_delivery_charge, get_order_details_delivery_charge

# Paytm Testing Credentials
# ECOM_PROD_MERCHANT_MID = settings.ECOM_TEST_MERCHANT_MID
# ECOM_PROD_MERCHANT_KEY = settings.ECOM_TEST_MERCHANT_KEY
# ECOM_INDUSTRY_TYPE_ID = settings.ECOM_TEST_INDUSTRY_TYPE_ID
# ECOM_CHANNEL_ID = settings.ECOM_TEST_CHANNEL_ID
# ECOM_WEBSITE_CHANNEL_ID = settings.ECOM_WEBSITE_TEST_CHANNEL_ID
# ECOM_WEBSITE = settings.ECOM_TEST_WEBSITE

# Paytm Production Credentials
ECOM_PROD_MERCHANT_MID = settings.ECOM_PROD_MERCHANT_MID
ECOM_PROD_MERCHANT_KEY = settings.ECOM_PROD_MERCHANT_KEY
ECOM_INDUSTRY_TYPE_ID = settings.ECOM_INDUSTRY_TYPE_ID
ECOM_CHANNEL_ID = settings.ECOM_CHANNEL_ID
ECOM_WEBSITE_CHANNEL_ID = settings.ECOM_WEBSITE_CHANNEL_ID
ECOM_WEBSITE = settings.ECOM_WEBSITE
ECOM_WEBSITE_WEBSITE = settings.ECOM_WEBSITE_WEBSITE

# website_base_url = "http://35.154.28.133:3000/"
website_base_url = "https://thecreditkart.com/"

Payment = apps.get_model('Payments', 'Payment')
OrderDetails = apps.get_model('ecom', 'OrderDetails')
Order = apps.get_model('ecom', 'Order')
Credit = apps.get_model('ecom', 'Credit')
ReturnOrder = apps.get_model('ecom', 'ReturnOrder')
OrderStatus = apps.get_model('ecom', 'OrderStatus')
UserWallet = apps.get_model('ecom', 'UserWallet')
Seller = apps.get_model('ecom', 'Seller')


def get_cashback_amount(amount):
    """
    description = This function calculate cashback amount and return it.
    """
    cashback_amount = 0
    if amount:
        cashback_amount = round((amount * 15) / 100)
    return cashback_amount


def get_order_details_cashback_amount(order_details_id, discount_percentage=None):
    """
    description =  This function return cashback amount by calculating order details payment amount.
    """
    cashback_amount = 0
    if order_details_id is not None:
        get_order_details_obj = OrderDetails.objects.filter(id=order_details_id).first()
        if get_order_details_obj:
            total_order_amount = 0
            total_wallet_paid = 0
            order_amount = get_order_details_obj.order_id.aggregate(Sum('total_amount'))['total_amount__sum']
            if order_amount:
                total_order_amount = order_amount
            wallet_paid = get_order_details_obj.payment.filter(category="Order", status__icontains="success",
                                                               type="Ecom_Wallet").aggregate(Sum('amount'))[
                'amount__sum']

            if wallet_paid:
                total_wallet_paid = wallet_paid

            amount = total_order_amount - total_wallet_paid
            if amount != 0 and amount > 0:
                cashback_amount = amount
                if discount_percentage == "fifty":
                    cashback_amount = ceil((cashback_amount * 50) / 100)

    return cashback_amount


def get_orders_cashback_amount(order_id, discount_percentage=None):
    orders_cashback_amount = 0
    if order_id is not None:
        get_order_obj = Order.objects.filter(id=order_id)
        if get_order_obj:
            cashback_amount = 0
            total_order_amount = 0
            get_order_details = OrderDetails.objects.filter(order_id=order_id)
            if get_order_details:
                order_details_id = get_order_details[0].id
                order_amount = get_order_details[0].order_id.aggregate(Sum('total_amount'))['total_amount__sum']
                if order_amount:
                    total_order_amount = order_amount
                cashback_amount = get_order_details_cashback_amount(order_details_id, discount_percentage)
            exact_order_amount = get_order_obj[0].total_amount
            amount = 0
            try:
                amount = round((exact_order_amount * cashback_amount) / total_order_amount)
            except Exception as e:
                print("inside Exception of get_orders_cashback_amount", e)

            if amount != 0 and amount > 0:
                orders_cashback_amount = amount

    return orders_cashback_amount


def change_successful_orders_status(order_details_obj, wallet_type=None):
    """
    description = This function is used to change order status of order details after successful payment..
    """
    if order_details_obj is not None:
        user = order_details_obj.user
        # updating status of order to placed..
        get_all_orders = OrderDetails.objects.filter(
            id=order_details_obj.id).values_list(
            'order_id', flat=True)

        status = "placed"
        if get_all_orders:
            all_orders = Order.objects.filter(id__in=get_all_orders)
            if all_orders:

                # Get delivery charge object..
                for order in all_orders:

                    order_obj = Order.objects.filter(id=order.id, status="initiated")
                    if order_obj:
                        order_obj[0].status = status
                        order_obj[0].updated_date = datetime.datetime.now()
                        order_obj[0].save()

                    # Here we change the status of Order_status table..
                    add_order_status = OrderStatus.objects.create(order=order,
                                                                  date=datetime.datetime.now(),
                                                                  status=status)

                    if status == "placed":
                        try:
                            seller_obj = Seller.objects.filter(user=order.product_details.user).order_by(
                                'id').last()
                            if seller_obj:
                                osms_kwrgs = {
                                    'sms_type': "seller",
                                    'number': str(seller_obj.mobile_no)
                                }
                                order_sms(**osms_kwrgs)
                        except Exception as e:
                            print("error in seller order msg send")

            order_details_obj = OrderDetails.objects.filter(id=order_details_obj.id,
                                                            status="initiated")
            if order_details_obj:
                order_details_obj[0].status = status
                order_details_obj[0].wallet_type = wallet_type
                order_details_obj[0].updated_date = datetime.datetime.now()
                order_details_obj[0].save()

            try:
                from EApi.order import make_coupon_true
                update_coupon = make_coupon_true(order_details_obj[0].id)
            except Exception as e:
                print("inside Exception of MAke coupon True in Checksum web", e)

            try:
                number = user.username
                osms_kwrgs = {
                    'sms_type': status,
                    'number': str(number)
                }
                order_sms(**osms_kwrgs)
            except Exception as e:
                print("error in user order msg send")

    return True


class ChecksumGenerateEcomPaytmOrder(APIView):
    """
        Paytm Order Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None
        try:
            shopping_order_id = str(request.data.get('shopping_order_id'))
            wallet_condition = request.data.get('wallet_condition')
            random_number = random.sample(range(99999), 1)
            ORDER_ID = str(shopping_order_id) + 'e' + str(random_number[0])
            user = request.user

            if wallet_condition == 1:
                merchant_key = ECOM_PROD_MERCHANT_KEY
                MID = ECOM_PROD_MERCHANT_MID
                INDUSTRY_TYPE_ID = ECOM_INDUSTRY_TYPE_ID
                CHANNEL_ID = ECOM_CHANNEL_ID
                WEBSITE = ECOM_WEBSITE
                # Callback_url = "https://ecom.thecreditkart.com/ecomapi/callback_order_url/"
                # Callback_url = "http://13.126.93.37:8007/ecomapi/callback_order_url/"
                # Callback_url = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="
                Callback_url = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="

                # shopping_order_id = "123456789"
                CUST_ID = ""
                user_id = request.user.id
                if user_id:
                    user = User.objects.filter(id=user_id)
                    if user:
                        CUST_ID = user[0].username

                TXN_AMOUNT = str(request.data.get('TXN_AMOUNT'))

                # Considering shopping order_id, CUST_ID and TXN_AMOUNT is getting.

                CALLBACK_URL = str(Callback_url) + '' + str(ORDER_ID)
                dict_param = {
                    'MID': MID,
                    'ORDER_ID': ORDER_ID,
                    'CUST_ID': CUST_ID,
                    'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                    'CHANNEL_ID': CHANNEL_ID,
                    'TXN_AMOUNT': TXN_AMOUNT,
                    'WEBSITE': WEBSITE,
                    'CALLBACK_URL': CALLBACK_URL,
                }
                return_checksum = generate_checksum(dict_param, merchant_key, salt=None)

                if return_checksum:
                    payment = Payment.objects.create(status='initiated',
                                                     order_id=str(ORDER_ID), amount=float(TXN_AMOUNT), category='Order',
                                                     product_type="Ecom", date=datetime.datetime.now())
                    # After the payment objects creation, it is needed to add this payment to respected order ID's
                    # entry in orders table.
                    if payment and payment is not None:
                        order_details = OrderDetails.objects.filter(order_text=shopping_order_id)
                        if order_details:
                            order_details[0].payment.add(payment)
                    message = "Successfully Done"
                    status = 1
                else:
                    message = "Something Went Wrong"
                    status = 0

                return Response(
                    {
                        'status': status,
                        'message': message,
                        'checksum': return_checksum,
                        'mid': MID,
                        'orderId': ORDER_ID,
                        'industryType': INDUSTRY_TYPE_ID,
                        'website': WEBSITE,
                        'channel': CHANNEL_ID,
                        'callback': CALLBACK_URL,
                        'amount': TXN_AMOUNT,
                        'cust_id': CUST_ID
                    }
                )

            if wallet_condition == 0:
                # This means no extra amount paid by customer..
                # Only updating all initiated entries to plaed for Order..

                order_details_obj = OrderDetails.objects.filter(order_text=shopping_order_id)
                if order_details_obj:
                    get_payment_obj = order_details_obj[0].payment.filter(product_type="Ecom",
                                                                          category='Order', type="Ecom_Wallet",
                                                                          mode="PPI", status="initiated")

                    if get_payment_obj:
                        update_payment = Payment.objects.filter(id=get_payment_obj[0].id).update(status="success",
                                                                                                 update_date=datetime.datetime.now())

                    # Updating Wallet object to success..
                    get_wallet_obj = UserWallet.objects.filter(order_details=order_details_obj[0], status="initiated")
                    if get_wallet_obj:
                        update_wallet_obj = UserWallet.objects.filter(order_details=order_details_obj[0],
                                                                      status="initiated").update(status="success",
                                                                                                 updated_date=datetime.datetime.now())

                        # Add cashback amount in user wallet..
                        payment_mode = order_details_obj[0].payment_mode
                        if payment_mode == "cash":
                            discount = 0
                            order_date = str(order_details_obj[0].created_date)[:10]
                            is_big_offer = get_big_offer_status(order_date)
                            if is_big_offer:
                                discount = 0
                            else:
                                cashback_amount = order_details_obj[0].total_amount
                                # This function is for 15% cashback..
                                discount = get_cashback_amount(cashback_amount)

                            if discount > 0:
                                # Adding cashback amount for the order in user wallet..
                                status = "cashback_amount"
                                order_obj = None
                                add_wallet = user_wallet_update(order_details_obj[0].user.id, discount,
                                                                order_details_obj[0], order_obj,
                                                                status)

                    #  Updating credit entry to success
                    if order_details_obj[0].credit:
                        get_credit_obj = Credit.objects.filter(id=order_details_obj[0].credit.id,
                                                               status="initiated").order_by('id').last()
                        if get_credit_obj:
                            update_credit_obj = Credit.objects.filter(id=get_credit_obj.id,
                                                                      status="initiated").update(status="success",
                                                                                                 updated_date=datetime.datetime.now())

                    # updating status of order to placed..
                    try:
                        update_status = change_successful_orders_status(order_details_obj[0])
                    except Exception as e:
                        print(
                            "Inside Exception of change order status ChecksumGenerateEcomPaytmOrder", e)
                    status = 2
                    message = 'Transaction successful.'

            return Response(
                {
                    'status': status,
                    'message': message
                }
            )

        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


def get_big_offer_status(order_date):
    start_date = "2020-10-31"
    end_date = "2020-11-16"
    if start_date < order_date < end_date:
        return True
    else:
        return False


def get_big_offer_status_new(order_date):
    start_date = "2020-12-24"
    end_date = "2021-01-01"
    if start_date < order_date < end_date:
        return True
    else:
        return False


class ChecksumGenerateEcomPaytmOrderWebsite(APIView):
    """
        Paytm Order Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None
        try:
            shopping_order_id = str(request.data.get('shopping_order_id'))
            wallet_condition = request.data.get('wallet_condition')
            random_number = random.sample(range(99999), 1)
            ORDER_ID = str(shopping_order_id) + 'e' + str(random_number[0])
            user = request.user

            if wallet_condition == 1:
                merchant_key = ECOM_PROD_MERCHANT_KEY
                MID = ECOM_PROD_MERCHANT_MID
                INDUSTRY_TYPE_ID = ECOM_INDUSTRY_TYPE_ID
                CHANNEL_ID = ECOM_WEBSITE_CHANNEL_ID
                WEBSITE = ECOM_WEBSITE_WEBSITE
                # Callback_url = "https://ecom.thecreditkart.com/ecomapi/callback_order_url/"
                Callback_url = "https://ecom.thecreditkart.com/ecomapi/callback_order_url/"
                # Callback_url = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="
                # Callback_url = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="

                CUST_ID = ""
                user_id = request.user.id
                if user_id:
                    user = User.objects.filter(id=user_id)
                    if user:
                        CUST_ID = user[0].username

                TXN_AMOUNT = str(request.data.get('TXN_AMOUNT'))
                # Considering shopping order_id, CUST_ID and TXN_AMOUNT is getting.

                CALLBACK_URL = str(Callback_url) + '' + str(ORDER_ID)
                dict_param = {
                    'MID': MID,
                    'ORDER_ID': ORDER_ID,
                    'CUST_ID': CUST_ID,
                    'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                    'CHANNEL_ID': CHANNEL_ID,
                    'TXN_AMOUNT': TXN_AMOUNT,
                    'WEBSITE': WEBSITE,
                    'CALLBACK_URL': CALLBACK_URL,
                }
                return_checksum = generate_checksum(dict_param, merchant_key, salt=None)

                if return_checksum:
                    payment = Payment.objects.create(status='initiated',
                                                     order_id=str(ORDER_ID), amount=float(TXN_AMOUNT), category='Order',
                                                     product_type="Ecom", date=datetime.datetime.now())
                    # After the payment objects creation, it is needed to add this payment to respected order ID's
                    # entry in orders table.
                    if payment and payment is not None:
                        order_details = OrderDetails.objects.filter(order_text=shopping_order_id)
                        if order_details:
                            order_details[0].payment.add(payment)
                    message = "Successfully Done"
                    status = 1
                else:
                    message = "Something Went Wrong"
                    status = 0
                return Response(
                    {
                        'status': status,
                        'message': message,
                        'checksum': return_checksum,
                        'mid': MID,
                        'orderId': ORDER_ID,
                        'industryType': INDUSTRY_TYPE_ID,
                        'website': WEBSITE,
                        'channel': CHANNEL_ID,
                        'callback': CALLBACK_URL,
                        'amount': TXN_AMOUNT,
                        'cust_id': CUST_ID
                    }
                )

            if wallet_condition == 0:
                # This means no extra amount paid by customer..
                # Only updating all initiated entries to plaed for Order..
                order_details_obj = OrderDetails.objects.filter(order_text=shopping_order_id)
                if order_details_obj:
                    get_payment_obj = order_details_obj[0].payment.filter(product_type="Ecom",
                                                                          category='Order', type="Ecom_Wallet",
                                                                          mode="PPI", status="initiated")

                    if get_payment_obj:
                        update_payment = Payment.objects.filter(id=get_payment_obj[0].id).update(status="success",
                                                                                                 update_date=datetime.datetime.now())

                    # Updating Wallet object to success..
                    get_wallet_obj = UserWallet.objects.filter(order_details=order_details_obj[0], status="initiated")
                    if get_wallet_obj:
                        update_wallet_obj = UserWallet.objects.filter(order_details=order_details_obj[0],
                                                                      status="initiated").update(status="success",
                                                                                                 updated_date=datetime.datetime.now())

                        # Add cashback amount in user wallet..
                        payment_mode = order_details_obj[0].payment_mode
                        if payment_mode == "cash":
                            discount = 0
                            order_date = str(order_details_obj[0].created_date)[:10]
                            is_big_offer = get_big_offer_status(order_date)
                            if is_big_offer:
                                discount = 0
                            else:
                                cashback_amount = order_details_obj[0].total_amount
                                # This function is for 15% cashback..
                                discount = get_cashback_amount(cashback_amount)

                            if discount > 0:
                                # Adding cashback amount for the order in user wallet..
                                status = "cashback_amount"
                                order_obj = None
                                add_wallet = user_wallet_update(order_details_obj[0].user.id, discount,
                                                                order_details_obj[0], order_obj,
                                                                status)

                    #  Updating credit entry to success
                    if order_details_obj[0].credit:
                        get_credit_obj = Credit.objects.filter(id=order_details_obj[0].credit.id,
                                                               status="initiated").order_by('id').last()
                        if get_credit_obj:
                            update_credit_obj = Credit.objects.filter(id=get_credit_obj.id,
                                                                      status="initiated").update(status="success",
                                                                                                 updated_date=datetime.datetime.now())

                    # updating status of order to placed..
                    try:
                        update_status = change_successful_orders_status(order_details_obj[0])
                    except Exception as e:
                        print(
                            "Inside Exception of change order status ChecksumGenerateEcomPaytmOrderWebsite", e)
                    status = 2
                    message = 'Transaction successful.'

            if wallet_condition == 2:
                # This is the condition of cod..
                # updating status of order to placed..
                order_details_obj = OrderDetails.objects.filter(order_text=shopping_order_id)
                if order_details_obj:
                    try:
                        update_status = change_successful_orders_status(order_details_obj[0])
                    except Exception as e:
                        print(
                            "Inside Exception of change order status ChecksumGenerateEcomPaytmOrder", e)

                    status = 2
                    message = 'Transaction successful.'

            return Response(
                {
                    'status': status,
                    'message': message
                }
            )

        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


@csrf_exempt
def callback_order_response(request, order_id):
    # redirect_url = 'http://15.207.21.5:3000/account/process_payment?order_id='
    redirect_url = website_base_url + "account/process_payment?order_id="
    if order_id is not None:
        Order_transaction_Status_Check_auto(order_id)
        redirect_with_oid = redirect_url + str(order_id)
        return redirect(redirect_with_oid)
    return redirect(website_base_url + "account/payment")


@csrf_exempt
def callback_repayment_response(request, order_id):
    # redirect_url = 'http://15.207.21.5:3000/account/process_payment?order_id='
    redirect_url = website_base_url + "account/process_payment?order_id="
    if order_id is not None:
        Repayment_transaction_Status_Check_auto(order_id)
        redirect_with_oid = redirect_url + str(order_id)
        return redirect(redirect_with_oid)
    return redirect(website_base_url + "account/payment")


@csrf_exempt
def callback_return_delivery_amount(request, order_id):
    # redirect_url = 'http://15.207.21.5:3000/account/process_payment?order_id='
    redirect_url = website_base_url + "account/process_payment?order_id="
    if order_id is not None:
        Return_Order_Delivery_Charge_Status_Check_(order_id)
        redirect_with_oid = redirect_url + str(order_id)
        return redirect(redirect_with_oid)
    return redirect(website_base_url + "account/payment")


def add_fk_in_order(payment, shopping_order_id):
    order_details = OrderDetails.objects.filter(order_text=shopping_order_id)
    if order_details:
        order_details[0].payment.add(payment)
        return True
    return False


def repayment_from_wallet(order_id, shopping_order_id, amount, user_id):
    """
    description =
    """
    from EApi.credit import get_user_wallet_amount
    status = "repayment"
    wallet_amount = 0
    payable_amount = 0
    used_wallet = 0
    amount = float(amount)
    wallet_dict = get_user_wallet_amount(user_id)
    if wallet_dict:
        wallet_amount = wallet_dict['balance_amount']

    if wallet_amount != 0:
        if wallet_amount >= amount:
            used_wallet = amount
            payable_amount = 0
        else:
            used_wallet = wallet_amount
            payable_amount = amount - wallet_amount

    else:
        payable_amount = amount

    order_details = OrderDetails.objects.filter(order_text=shopping_order_id).first()
    order_obj = Order.objects.filter(order_id=shopping_order_id).first()

    if used_wallet != 0:
        # if difference is <=0 then amount deducted from wallet will be "amount"
        payment = Payment.objects.create(status='success', order_id=str(order_id), amount=float(used_wallet),
                                         category='Repayment', type="Ecom_Wallet", mode="PPI",
                                         product_type="Ecom", date=datetime.datetime.now())

        add_fk_in_order(payment, shopping_order_id)

        # Creating new wallet entry
        user_wallet_sub(user_id, used_wallet, order_details, order_obj, status)

        # Adding credit to users credit..
        status = "repayment"
        import time
        sleep_time = random.random()
        time.sleep(sleep_time)
        sleep_time = random.random()
        time.sleep(sleep_time)
        get_credit_repayment = order_details.credit_history.filter(
            status=status, repayment_add=used_wallet,
            created_date__startswith=str(datetime.datetime.now())[:16])
        if get_credit_repayment:
            pass
        else:
            # No credit entry, so credit not updated
            # Updating User credit balance..

            add_credit = credit_balance_update(user_id, used_wallet, status)
            if add_credit:
                # Adding new credit entry in Credit history of Order Details Table..
                order_details.credit_history.add(add_credit)

        order_credit_status = check_order_details_credit_status(order_details.id)

        return payable_amount
    else:
        return payable_amount


class ChecksumGenerateEcomPaytmRepayment(APIView):
    """
        Paytm Repayment Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None

        try:
            merchant_key = ECOM_PROD_MERCHANT_KEY
            MID = ECOM_PROD_MERCHANT_MID
            INDUSTRY_TYPE_ID = ECOM_INDUSTRY_TYPE_ID
            CHANNEL_ID = ECOM_CHANNEL_ID
            WEBSITE = ECOM_WEBSITE
            # Callback_url = "https://ecom.thecreditkart.com/ecomapi/callback_repayment_url/"
            # Callback_url = "http://13.126.93.37:8007/ecomapi/callback_repayment_url/"
            # Callback_url = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="
            Callback_url = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="
            random_number = random.sample(range(99999), 1)
            shopping_order_id = str(request.data.get('shopping_order_id'))
            TXN_AMOUNT = str(request.data.get('TXN_AMOUNT'))

            # Considering shopping order_id, CUST_ID and TXN_AMOUNT is getting.
            ORDER_ID = str(shopping_order_id) + 'e' + str(random_number[0])

            CUST_ID = ""
            user_id = request.user.id
            if user_id:
                user = User.objects.filter(id=user_id)
                if user:
                    CUST_ID = user[0].username

                # Repayment from wallet
                # use_wallet: boolean
                use_wallet = True
                if "use_wallet" in request.data:
                    use_wallet = request.data.get('use_wallet')

                if use_wallet:
                    payable_amount = repayment_from_wallet(ORDER_ID, shopping_order_id, TXN_AMOUNT, user_id)
                    if payable_amount == 0:

                        number = user[0].username
                        osms_kwrgs = {
                            'sms_type': "repayment",
                            'number': str(number)
                        }
                        order_sms(**osms_kwrgs)

                        message = "Payment Successful from wallet"
                        status = 6
                        return Response(
                            {
                                'status': status,
                                'message': message
                            }
                        )
                    else:
                        TXN_AMOUNT = str(payable_amount)

            CALLBACK_URL = str(Callback_url) + '' + str(ORDER_ID)
            dict_param = {
                'MID': MID,
                'ORDER_ID': ORDER_ID,
                'CUST_ID': CUST_ID,
                'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                'CHANNEL_ID': CHANNEL_ID,
                'TXN_AMOUNT': TXN_AMOUNT,
                'WEBSITE': WEBSITE,
                'CALLBACK_URL': CALLBACK_URL,
            }
            return_checksum = generate_checksum(dict_param, merchant_key, salt=None)

            if return_checksum:
                payment = Payment.objects.create(status='initiated',
                                                 order_id=str(ORDER_ID), amount=float(TXN_AMOUNT), category='Repayment',
                                                 product_type="Ecom", date=datetime.datetime.now())
                # After the payment objects creation, it is needed to add this payment to respected order ID's
                # entry in orders table.
                if payment and payment is not None:
                    order_details = OrderDetails.objects.filter(order_text=shopping_order_id)
                    if order_details:
                        order_details[0].payment.add(payment)

                message = "Successfully Done"
                status = 1
            else:
                message = "Something Went Wrong"
                status = 0

            return Response(
                {
                    'status': status,
                    'message': message,
                    'checksum': return_checksum,
                    'mid': MID,
                    'orderId': ORDER_ID,
                    'industryType': INDUSTRY_TYPE_ID,
                    'website': WEBSITE,
                    'channel': CHANNEL_ID,
                    'callback': CALLBACK_URL,
                    'amount': TXN_AMOUNT,
                    'cust_id': CUST_ID
                }
            )

        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


class ChecksumGenerateEcomPaytmRepaymentWebsite(APIView):
    """
        Paytm Repayment Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None

        try:
            merchant_key = ECOM_PROD_MERCHANT_KEY
            MID = ECOM_PROD_MERCHANT_MID
            INDUSTRY_TYPE_ID = ECOM_INDUSTRY_TYPE_ID
            CHANNEL_ID = ECOM_WEBSITE_CHANNEL_ID
            WEBSITE = ECOM_WEBSITE_WEBSITE
            # Callback_url = "https://ecom.thecreditkart.com/ecomapi/callback_repayment_url/"
            Callback_url = "https://ecom.thecreditkart.com/ecomapi/callback_repayment_url/"
            # Callback_url = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="
            # Callback_url = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="
            random_number = random.sample(range(99999), 1)
            shopping_order_id = str(request.data.get('shopping_order_id'))
            TXN_AMOUNT = str(request.data.get('TXN_AMOUNT'))

            # Considering shopping order_id, CUST_ID and TXN_AMOUNT is getting.
            ORDER_ID = str(shopping_order_id) + 'e' + str(random_number[0])

            # shopping_order_id = "123456789"
            CUST_ID = ""
            user_id = request.user.id
            if user_id:
                user = User.objects.filter(id=user_id)
                if user:
                    CUST_ID = user[0].username

                # Repayment from wallet
                # use_wallet: boolean
                use_wallet = True
                if "use_wallet" in request.data:
                    use_wallet = request.data.get('use_wallet')

                if use_wallet:
                    payable_amount = repayment_from_wallet(ORDER_ID, shopping_order_id, TXN_AMOUNT, user_id)
                    if payable_amount == 0:

                        number = user[0].username
                        osms_kwrgs = {
                            'sms_type': "repayment",
                            'number': str(number)
                        }
                        order_sms(**osms_kwrgs)

                        message = "Payment Successful from wallet"
                        status = 6
                        return Response(
                            {
                                'status': status,
                                'message': message
                            }
                        )
                    else:
                        TXN_AMOUNT = str(payable_amount)

            CALLBACK_URL = str(Callback_url) + '' + str(ORDER_ID)
            dict_param = {
                'MID': MID,
                'ORDER_ID': ORDER_ID,
                'CUST_ID': CUST_ID,
                'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                'CHANNEL_ID': CHANNEL_ID,
                'TXN_AMOUNT': TXN_AMOUNT,
                'WEBSITE': WEBSITE,
                'CALLBACK_URL': CALLBACK_URL,
            }
            return_checksum = generate_checksum(dict_param, merchant_key, salt=None)

            if return_checksum:
                payment = Payment.objects.create(status='initiated',
                                                 order_id=str(ORDER_ID), amount=float(TXN_AMOUNT), category='Repayment',
                                                 product_type="Ecom", date=datetime.datetime.now())
                # After the payment objects creation, it is needed to add this payment to respected order ID's
                # entry in orders table.
                if payment and payment is not None:
                    order_details = OrderDetails.objects.filter(order_text=shopping_order_id)
                    if order_details:
                        order_details[0].payment.add(payment)

                message = "Successfully Done"
                status = 1
            else:
                message = "Something Went Wrong"
                status = 0

            return Response(
                {
                    'status': status,
                    'message': message,
                    'checksum': return_checksum,
                    'mid': MID,
                    'orderId': ORDER_ID,
                    'industryType': INDUSTRY_TYPE_ID,
                    'website': WEBSITE,
                    'channel': CHANNEL_ID,
                    'callback': CALLBACK_URL,
                    'amount': TXN_AMOUNT,
                    'cust_id': CUST_ID
                }
            )

        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


def Repayment_transaction_Status_Check_auto(order_id):
    import requests
    status = None
    message = None
    amount = 0.0
    date_strip = None
    try:
        if order_id:
            if 'e' in order_id:
                o_id = order_id
                order_data = str(order_id).split("e")
                order_text = ''
                if order_data:
                    order_text = order_data[0]
                paytm_status = ""
                merchant_key = ECOM_PROD_MERCHANT_KEY
                m_id = ECOM_PROD_MERCHANT_MID
                dict_param = {
                    'MID': m_id,
                    'ORDERID': o_id,
                }

                checksum = str(generate_checksum(dict_param, merchant_key))

                dict_param["CHECKSUMHASH"] = checksum
                url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                # url = 'https://securegw-stage.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                r = requests.get(url=url)
                if r.status_code == 200:

                    paytm_responce = r.json()

                    if paytm_responce['STATUS'] == 'TXN_SUCCESS':
                        paytm_status = 'success'
                    elif paytm_responce['STATUS'] == 'TXN_FAILURE':
                        paytm_status = 'failed'
                    elif paytm_responce['STATUS'] == 'PENDING':
                        paytm_status = 'pending'

                    if 'TXNAMOUNT' in paytm_responce:
                        amount = paytm_responce['TXNAMOUNT']
                        if amount is None or amount == '':
                            amount = 0.0
                    if 'TXNDATE' in paytm_responce:
                        date_strip = paytm_responce['TXNDATE']
                        if date_strip is None or date_strip == '':
                            date_strip = None
                    mode = ''
                    if 'PAYMENTMODE' in paytm_responce:
                        if paytm_responce['PAYMENTMODE']:
                            mode = paytm_responce['PAYMENTMODE']
                    Payment.objects.filter(product_type="Ecom", order_id=o_id,
                                           category='Repayment').update(status=paytm_status,
                                                                        transaction_id=paytm_responce[
                                                                            'TXNID'],
                                                                        amount=amount,
                                                                        mode=mode,
                                                                        type='App',
                                                                        response=paytm_responce,
                                                                        date=date_strip,
                                                                        update_date=datetime.datetime.now())

                    order_details_obj = OrderDetails.objects.filter(order_text=order_text)
                    if order_details_obj:
                        user_id = order_details_obj[0].user.id
                        if paytm_status == 'success':
                            order_credit_status = check_order_details_credit_status(order_details_obj[0].id)
                            # Validating
                            if order_credit_status:
                                update_status = OrderDetails.objects.filter(id=order_details_obj[0].id).update(
                                    credit_status=order_credit_status)

                            # check if credit exist
                            credit_status = "repayment"
                            import time
                            sleep_time = random.random()
                            time.sleep(sleep_time)
                            sleep_time = random.random()
                            time.sleep(sleep_time)
                            get_credit_repayment = order_details_obj[0].credit_history.filter(
                                status=credit_status, repayment_add=amount,
                                created_date__startswith=str(datetime.datetime.now())[:16])
                            if get_credit_repayment:
                                pass

                            else:
                                # No credit entry, so credit not updated
                                # Updating User credit balance..

                                update_credit = credit_balance_update(user_id, amount, credit_status)
                                if update_credit:
                                    # Adding new credit entry in Credit history of Order Details Table..
                                    order_details_obj[0].credit_history.add(update_credit)

                            try:
                                number = order_details_obj[0].user.username
                                osms_kwrgs = {
                                    'sms_type': "repayment",
                                    'number': str(number)
                                }
                                order_sms(**osms_kwrgs)
                            except Exception as e:
                                print("error in repayment")

                            status = '1'
                            message = 'Transaction successful.'

                        elif paytm_status == 'failed':

                            status = '2'
                            message = 'Transaction failed'

                        elif paytm_status == 'pending':

                            status = '3'
                            message = 'Transaction pending'

                return Response(
                    {
                        'status': status,
                        'message': message
                    }
                )

            else:
                return Response(
                    {
                        'status': status,
                        'message': "order id is not valid"
                    }
                )

        else:
            return Response(
                {
                    'status': status,
                    'message': "order id could not be none"
                }
            )

    except Exception as e:

        import sys
        import os

        print('-----------in exception----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)


def pass_order_id_order(request, order_id):
    if order_id is not None:
        data = Order_transaction_Status_Check_auto(order_id)
    return HttpResponse("ok")


def pass_order_id_repayment(request, order_id):
    if order_id is not None:
        data = Repayment_transaction_Status_Check_auto(order_id)
    return "OKay"


def Order_transaction_Status_Check_auto(order_id):
    import requests
    status = None
    message = None
    amount = 0.0
    date_strip = None
    try:
        if order_id:
            if 'e' in order_id:
                o_id = order_id
                order_data = str(order_id).split("e")
                order_text = ''
                if order_data:
                    order_text = order_data[0]
                paytm_status = ""
                merchant_key = ECOM_PROD_MERCHANT_KEY
                m_id = ECOM_PROD_MERCHANT_MID
                dict_param = {
                    'MID': m_id,
                    'ORDERID': o_id,
                }

                checksum = str(generate_checksum(dict_param, merchant_key))

                dict_param["CHECKSUMHASH"] = checksum
                url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                # url = 'https://securegw-stage.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                r = requests.get(url=url)
                if r.status_code == 200:

                    paytm_responce = r.json()

                    if paytm_responce['STATUS'] == 'TXN_SUCCESS':
                        paytm_status = 'success'
                    elif paytm_responce['STATUS'] == 'TXN_FAILURE':
                        paytm_status = 'failed'
                    elif paytm_responce['STATUS'] == 'PENDING':
                        paytm_status = 'pending'

                    if 'TXNAMOUNT' in paytm_responce:
                        amount = paytm_responce['TXNAMOUNT']
                        if amount is None or amount == '':
                            amount = 0.0
                    if 'TXNDATE' in paytm_responce:
                        date_strip = paytm_responce['TXNDATE']
                        if date_strip is None or date_strip == '':
                            date_strip = None
                    mode = ''
                    if 'PAYMENTMODE' in paytm_responce:
                        if paytm_responce['PAYMENTMODE']:
                            mode = paytm_responce['PAYMENTMODE']
                    Payment.objects.filter(order_id=o_id,
                                           category='Order').update(status=paytm_status,
                                                                    transaction_id=paytm_responce[
                                                                        'TXNID'],
                                                                    amount=amount,
                                                                    mode=mode,
                                                                    type='App',
                                                                    response=paytm_responce,
                                                                    date=date_strip,
                                                                    update_date=datetime.datetime.now())

                    order_details_obj = OrderDetails.objects.filter(order_text=order_text)
                    if order_details_obj:
                        if paytm_status == 'success':
                            from EApi.order import get_order_details_delivery_charge
                            get_wallet_obj = UserWallet.objects.filter(order_details=order_details_obj[0],
                                                                       status="initiated")
                            if get_wallet_obj:
                                update_wallet = UserWallet.objects.filter(order_details=order_details_obj[0],
                                                                          status="initiated").update(
                                    status="success")

                            get_payment_obj = Payment.objects.filter(product_type="Ecom", category="Order",
                                                                     order_id__startswith=order_text,
                                                                     type="Ecom_Wallet",
                                                                     mode="PPI", status="initiated")
                            if get_payment_obj:
                                update_payment_obj = Payment.objects.filter(id=get_payment_obj[0].id).update(
                                    status="success")

                            total_delivery_charges = get_order_details_delivery_charge(order_details_obj[0].id)
                            total_amount_paid = Payment.objects.filter(product_type="Ecom", category="Order",
                                                                       order_id__startswith=order_text,
                                                                       status__icontains="success",
                                                                       ).aggregate(Sum('amount'))['amount__sum']

                            total_order_amount = order_details_obj[0].pay_amount
                            if total_amount_paid >= total_order_amount:
                                if order_details_obj[0].credit:
                                    # Updating credit entries..

                                    success_update = Credit.objects.filter(id=order_details_obj[0].credit.id,
                                                                           status="initiated",
                                                                           ).update(status="success")

                                    update_order_details_credit_status = OrderDetails.objects.filter(
                                        id=order_details_obj[0].id,
                                        credit_status="success")

                                # updating status of order to placed..
                                try:
                                    update_status = change_successful_orders_status(order_details_obj[0])
                                except Exception as e:
                                    print(
                                        "Inside Exception of change order status Order_transaction_Status_Check_auto",
                                        e)

                                try:
                                    payment_mode = order_details_obj[0].payment_mode
                                    if payment_mode == "cash":
                                        discount = 0
                                        order_date = str(order_details_obj[0].created_date)[:10]
                                        is_big_offer = get_big_offer_status(order_date)
                                        if is_big_offer:
                                            discount = get_order_details_cashback_amount(order_details_obj[0].id)
                                        is_big_offer_new = get_big_offer_status_new(order_date)
                                        if is_big_offer_new:
                                            discount_percentage = "fifty"
                                            discount = get_order_details_cashback_amount(order_details_obj[0].id,
                                                                                         discount_percentage)
                                        else:
                                            cashback_amount = order_details_obj[0].total_amount
                                            # This function is for 15% cashback..
                                            discount = get_cashback_amount(cashback_amount)

                                        if discount > 0:
                                            # Adding cashback amount for the order in user wallet..
                                            status = "cashback_amount"
                                            order_obj = None
                                            add_wallet = user_wallet_update(order_details_obj[0].user.id, discount,
                                                                            order_details_obj[0], order_obj,
                                                                            status)
                                except Exception as e:
                                    print("inside Exception of cashback amount", e)

                            status = '1'
                            message = 'Transaction successful.'

                        elif paytm_status == 'failed':

                            status = '2'
                            message = 'Transaction failed'

                        elif paytm_status == 'pending':

                            status = '3'
                            message = 'Transaction pending'

                return Response(
                    {
                        'status': status,
                        'message': message
                    }
                )

            else:
                return Response(
                    {
                        'status': status,
                        'message': "order id is not valid"
                    }
                )

        else:
            return Response(
                {
                    'status': status,
                    'message': "order id could not be none"
                }
            )

    except Exception as e:
        import sys
        import os
        print('-----------in exception----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)


class Order_transaction_Status_Check_auto_Application(generics.ListAPIView):
    """
    This Api is to Check Order ransaction status check and update the info after order payment..
    Url: ecomapi/user_orders
    Authentication token required
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User toke is required..
    method = get
    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    def post(self, request, *args, **kwargs):
        order_id = request.data.get("order_id")
        import requests
        status = None
        message = None
        amount = 0.0
        date_strip = None
        try:
            if order_id:
                if 'e' in order_id:
                    o_id = order_id
                    order_data = str(order_id).split("e")
                    order_text = ''
                    if order_data:
                        order_text = order_data[0]
                    paytm_status = ""
                    merchant_key = ECOM_PROD_MERCHANT_KEY
                    m_id = ECOM_PROD_MERCHANT_MID
                    dict_param = {
                        'MID': m_id,
                        'ORDERID': o_id,
                    }

                    checksum = str(generate_checksum(dict_param, merchant_key))

                    dict_param["CHECKSUMHASH"] = checksum
                    url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                    # url = 'https://securegw-stage.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                    r = requests.get(url=url)
                    if r.status_code == 200:

                        paytm_responce = r.json()

                        if paytm_responce['STATUS'] == 'TXN_SUCCESS':
                            paytm_status = 'success'
                        elif paytm_responce['STATUS'] == 'TXN_FAILURE':
                            paytm_status = 'failed'
                        elif paytm_responce['STATUS'] == 'PENDING':
                            paytm_status = 'pending'

                        if 'TXNAMOUNT' in paytm_responce:
                            amount = paytm_responce['TXNAMOUNT']
                            if amount is None or amount == '':
                                amount = 0.0
                        if 'TXNDATE' in paytm_responce:
                            date_strip = paytm_responce['TXNDATE']
                            if date_strip is None or date_strip == '':
                                date_strip = None
                        mode = ''
                        if 'PAYMENTMODE' in paytm_responce:
                            if paytm_responce['PAYMENTMODE']:
                                mode = paytm_responce['PAYMENTMODE']
                        Payment.objects.filter(order_id=o_id,
                                               category='Order').update(status=paytm_status,
                                                                        transaction_id=paytm_responce[
                                                                            'TXNID'],
                                                                        amount=amount,
                                                                        mode=mode,
                                                                        type='App',
                                                                        response=paytm_responce,
                                                                        date=date_strip,
                                                                        update_date=datetime.datetime.now())

                        order_details_obj = OrderDetails.objects.filter(order_text=order_text)
                        if order_details_obj:
                            if paytm_status == 'success':
                                from EApi.order import get_order_details_delivery_charge
                                get_wallet_obj = UserWallet.objects.filter(order_details=order_details_obj[0],
                                                                           status="initiated")
                                if get_wallet_obj:
                                    update_wallet = UserWallet.objects.filter(order_details=order_details_obj[0],
                                                                              status="initiated").update(
                                        status="success")

                                get_payment_obj = Payment.objects.filter(product_type="Ecom", category="Order",
                                                                         order_id__startswith=order_text,
                                                                         type="Ecom_Wallet",
                                                                         mode="PPI", status="initiated")
                                if get_payment_obj:
                                    update_payment_obj = Payment.objects.filter(id=get_payment_obj[0].id).update(
                                        status="success")

                                total_delivery_charges = get_order_details_delivery_charge(order_details_obj[0].id)
                                total_amount_paid = Payment.objects.filter(product_type="Ecom", category="Order",
                                                                           order_id__startswith=order_text,
                                                                           status__icontains="success",
                                                                           ).aggregate(Sum('amount'))['amount__sum']
                                total_order_amount = order_details_obj[0].pay_amount
                                if total_amount_paid >= total_order_amount:
                                    # Updating credit status..
                                    if order_details_obj[0].credit:
                                        success_update = Credit.objects.filter(id=order_details_obj[0].credit.id,
                                                                               status="initiated",
                                                                               ).update(status="success")

                                        update_order_details_credit_status = OrderDetails.objects.filter(
                                            id=order_details_obj[0].id,
                                            credit_status="success")

                                    # updating status of order to placed..
                                    try:
                                        update_status = change_successful_orders_status(order_details_obj[0])
                                    except Exception as e:
                                        print(
                                            "Inside Exception of change order status \
                                            Order_transaction_Status_Check_auto_Application",
                                            e)

                                    try:
                                        payment_mode = order_details_obj[0].payment_mode
                                        if payment_mode == "cash":
                                            discount = 0
                                            order_date = str(order_details_obj[0].created_date)[:10]
                                            is_big_offer = get_big_offer_status(order_date)
                                            if is_big_offer:
                                                discount = get_order_details_cashback_amount(order_details_obj[0].id)
                                            is_big_offer_new = get_big_offer_status_new(order_date)
                                            if is_big_offer_new:
                                                discount_percentage = "fifty"
                                                discount = get_order_details_cashback_amount(order_details_obj[0].id,
                                                                                             discount_percentage)
                                            else:
                                                cashback_amount = order_details_obj[0].total_amount
                                                # This function is for 15% cashback..
                                                discount = get_cashback_amount(cashback_amount)

                                            if discount > 0:
                                                # Adding cashback amount for the order in user wallet..
                                                status = "cashback_amount"
                                                order_obj = None
                                                add_wallet = user_wallet_update(order_details_obj[0].user.id, discount,
                                                                                order_details_obj[0], order_obj,
                                                                                status)
                                    except Exception as e:
                                        print("inside Exception of cashback amount", e)

                                status = '1'
                                message = 'Transaction successful.'

                            elif paytm_status == 'failed':

                                status = '2'
                                message = 'Transaction failed'

                            elif paytm_status == 'pending':

                                status = '3'
                                message = 'Transaction pending'

                    return Response(
                        {
                            'status': status,
                            'message': message
                        }
                    )

                else:
                    return Response(
                        {
                            'status': status,
                            'message': "order id is not valid"
                        }
                    )

            else:
                return Response(
                    {
                        'status': status,
                        'message': "order id could not be none"
                    }
                )


        except Exception as e:
            import sys
            import os
            print('-----------in exception----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)


class Repayment_transaction_Status_Check_auto_Application(generics.ListAPIView):
    """
    This API is to check repayments for Selected order..
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        import requests
        status = None
        message = None
        amount = 0.0
        date_strip = None
        order_id = request.data.get('order_id')
        try:
            if order_id:
                if 'e' in order_id:
                    o_id = order_id
                    order_data = str(order_id).split("e")
                    order_text = ''
                    if order_data:
                        order_text = order_data[0]
                    paytm_status = ""
                    merchant_key = ECOM_PROD_MERCHANT_KEY
                    m_id = ECOM_PROD_MERCHANT_MID
                    dict_param = {
                        'MID': m_id,
                        'ORDERID': o_id,
                    }

                    checksum = str(generate_checksum(dict_param, merchant_key))

                    dict_param["CHECKSUMHASH"] = checksum
                    url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                    # url = 'https://securegw-stage.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                    r = requests.get(url=url)
                    if r.status_code == 200:

                        paytm_responce = r.json()

                        if paytm_responce['STATUS'] == 'TXN_SUCCESS':
                            paytm_status = 'success'
                        elif paytm_responce['STATUS'] == 'TXN_FAILURE':
                            paytm_status = 'failed'
                        elif paytm_responce['STATUS'] == 'PENDING':
                            paytm_status = 'pending'

                        if 'TXNAMOUNT' in paytm_responce:
                            amount = paytm_responce['TXNAMOUNT']
                            if amount is None or amount == '':
                                amount = 0.0
                        if 'TXNDATE' in paytm_responce:
                            date_strip = paytm_responce['TXNDATE']
                            if date_strip is None or date_strip == '':
                                date_strip = None
                        mode = ''
                        if 'PAYMENTMODE' in paytm_responce:
                            if paytm_responce['PAYMENTMODE']:
                                mode = paytm_responce['PAYMENTMODE']
                        Payment.objects.filter(product_type="Ecom", order_id=o_id,
                                               category='Repayment').update(status=paytm_status,
                                                                            transaction_id=paytm_responce[
                                                                                'TXNID'],
                                                                            amount=amount,
                                                                            mode=mode,
                                                                            type='App',
                                                                            response=paytm_responce,
                                                                            date=date_strip,
                                                                            update_date=datetime.datetime.now())

                        order_details_obj = OrderDetails.objects.filter(order_text=order_text)
                        if order_details_obj:
                            user_id = order_details_obj[0].user.id
                            if paytm_status == 'success':
                                order_credit_status = check_order_details_credit_status(order_details_obj[0].id)
                                # Validating
                                if order_credit_status:
                                    update_status = OrderDetails.objects.filter(id=order_details_obj[0].id).update(
                                        credit_status=order_credit_status)

                                # Updating User credit balance..
                                credit_status = "repayment"
                                import time
                                sleep_time = random.random()
                                time.sleep(sleep_time)
                                sleep_time = random.random()
                                time.sleep(sleep_time)
                                get_credit_repayment = order_details_obj[0].credit_history.filter(
                                    status=credit_status, repayment_add=amount,
                                    created_date__startswith=str(datetime.datetime.now())[:16])
                                if get_credit_repayment:
                                    pass

                                else:
                                    # No credit entry, so credit not updated
                                    # Updating User credit balance..
                                    update_credit = credit_balance_update(user_id, amount, credit_status)
                                    if update_credit:
                                        # Adding new credit entry in Credit history of Order Details Table..
                                        order_details_obj[0].credit_history.add(update_credit)

                                try:
                                    number = order_details_obj[0].user.username
                                    osms_kwrgs = {
                                        'sms_type': "repayment",
                                        'number': str(number)
                                    }
                                    order_sms(**osms_kwrgs)
                                except Exception as e:
                                    print("error in repayment")

                                status = '1'
                                message = 'Transaction successful.'

                            elif paytm_status == 'failed':

                                status = '2'
                                message = 'Transaction failed'

                            elif paytm_status == 'pending':

                                status = '3'
                                message = 'Transaction pending'

                    return Response(
                        {
                            'status': status,
                            'message': message
                        }
                    )

                else:
                    return Response(
                        {
                            'status': status,
                            'message': "order id is not valid"
                        }
                    )

            else:
                return Response(
                    {
                        'status': status,
                        'message': "order id could not be none"
                    }
                )

        except Exception as e:

            import sys
            import os

            print('-----------in exception----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)


class Return_Order_Delivery_Charge_Status_Check_Application(generics.ListAPIView):
    """
    This API is to check repayments for Selected order..
    """

    def post(self, request, *args, **kwargs):
        import requests
        status = "10"
        message = "Something went wrong"
        amount = 0.0
        date_strip = None
        order_id = request.data.get('order_id')
        try:
            if order_id:
                if 'e' in order_id:
                    o_id = order_id
                    order_data = str(order_id).split("e")
                    return_id = ''
                    if order_data:
                        return_id = order_data[0]
                    paytm_status = ""
                    merchant_key = ECOM_PROD_MERCHANT_KEY
                    m_id = ECOM_PROD_MERCHANT_MID
                    dict_param = {
                        'MID': m_id,
                        'ORDERID': o_id,
                    }

                    checksum = str(generate_checksum(dict_param, merchant_key))

                    dict_param["CHECKSUMHASH"] = checksum
                    url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                    # url = 'https://securegw-stage.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                    r = requests.get(url=url)
                    if r.status_code == 200:

                        paytm_responce = r.json()

                        if paytm_responce['STATUS'] == 'TXN_SUCCESS':
                            paytm_status = 'success'
                        elif paytm_responce['STATUS'] == 'TXN_FAILURE':
                            paytm_status = 'failed'
                        elif paytm_responce['STATUS'] == 'PENDING':
                            paytm_status = 'pending'

                        if 'TXNAMOUNT' in paytm_responce:
                            amount = paytm_responce['TXNAMOUNT']
                            if amount is None or amount == '':
                                amount = 0.0
                        if 'TXNDATE' in paytm_responce:
                            date_strip = paytm_responce['TXNDATE']
                            if date_strip is None or date_strip == '':
                                date_strip = None
                        mode = ''
                        if 'PAYMENTMODE' in paytm_responce:
                            if paytm_responce['PAYMENTMODE']:
                                mode = paytm_responce['PAYMENTMODE']
                        update_app_payment = Payment.objects.filter(order_id=o_id,
                                                                    category='Order_Return').exclude(
                            type="Ecom_Wallet").update(
                            status=paytm_status,
                            transaction_id=paytm_responce[
                                'TXNID'],
                            amount=amount,
                            mode=mode,
                            type='App',
                            response=paytm_responce,
                            date=date_strip,
                            update_date=datetime.datetime.now())

                        get_return_order_obj = ReturnOrder.objects.filter(return_id=return_id)
                        if get_return_order_obj:
                            if paytm_status == 'success':
                                # Here the actual return request order is created
                                update_return_order = ReturnOrder.objects.filter(return_id=return_id,
                                                                                 verify_return_status="return_request",
                                                                                 payment_status="not_paid").update(
                                    payment_status="success")

                                # update status in Order table
                                Order.objects.filter(id=get_return_order_obj[0].order.id).update(status="return",
                                                                                                 updated_date=datetime.datetime.now())

                                # Update status in OrderStatus table
                                add_order_status = OrderStatus.objects.create(order=get_return_order_obj[0].order,
                                                                              status="return_request",
                                                                              date=datetime.datetime.now())

                                # Update wallet payment if available..
                                get_wallet_payment = Payment.objects.filter(order_id=o_id,
                                                                            category='Order_Return', mode="PPI",
                                                                            type="Ecom_Wallet")

                                if get_wallet_payment:
                                    update_wallet_paymet = Payment.objects.filter(id=get_wallet_payment[0].id).update(
                                        status=paytm_status,
                                        transaction_id=paytm_responce[
                                            'TXNID'],
                                        amount=amount,
                                        mode=mode,
                                        type='App',
                                        response=paytm_responce,
                                        date=date_strip,
                                        update_date=datetime.datetime.now())

                                # Update status of wallet used in Return Order..
                                get_wallet_obj = UserWallet.objects.filter(order=get_return_order_obj[0].order.id,
                                                                           status="return_delivery_charge_initiated").order_by(
                                    'id').last()

                                if get_wallet_obj:
                                    update_wallet = UserWallet.objects.filter(id=get_wallet_obj.id).update(
                                        status="return_delivery_charge_success", updated_date=datetime.datetime.now())

                                get_order_details_obj = OrderDetails.objects.filter(
                                    order_id=get_return_order_obj[0].order.id)

                                try:
                                    number = get_order_details_obj[0].user.username
                                    osms_kwrgs = {
                                        'sms_type': "return",
                                        'number': str(number)
                                    }
                                    order_sms(**osms_kwrgs)
                                except Exception as e:
                                    print("error in Order return message")

                                status = '1'
                                message = 'Transaction successful.'

                            elif paytm_status == 'failed':

                                status = '2'
                                message = 'Transaction failed'

                            elif paytm_status == 'pending':

                                status = '3'
                                message = 'Transaction pending'

                        return Response(
                            {
                                'status': status,
                                'message': message
                            }
                        )

        except Exception as e:
            import sys
            import os
            print('-----------in exception----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


class PaymentTransactionStatusApi(APIView):
    """
       This API shows the transaction status of the payment..
    """

    def post(self, request, *args, **kwargs):
        import requests
        status = None
        message = None
        amount = 0.0
        date_strip = None
        try:
            order_id = request.data.get('order_id')
            if order_id:
                if 'e' in order_id:
                    o_id = order_id
                    order_data = str(order_id).split("e")
                    order_text = ''
                    if order_data:
                        order_text = order_data[0]
                    paytm_status = ""
                    merchant_key = ECOM_PROD_MERCHANT_KEY
                    m_id = ECOM_PROD_MERCHANT_MID
                    dict_param = {
                        'MID': m_id,
                        'ORDERID': o_id,
                    }

                    checksum = str(generate_checksum(dict_param, merchant_key))
                    dict_param["CHECKSUMHASH"] = checksum
                    url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                    # url = 'https://securegw-stage.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                    r = requests.get(url=url)
                    if r.status_code == 200:
                        paytm_responce = r.json()
                        if paytm_responce['STATUS'] == 'TXN_SUCCESS':
                            paytm_status = 'success'
                        elif paytm_responce['STATUS'] == 'TXN_FAILURE':
                            paytm_status = 'failed'
                        elif paytm_responce['STATUS'] == 'PENDING':
                            paytm_status = 'pending'

                        if 'TXNAMOUNT' in paytm_responce:
                            amount = paytm_responce['TXNAMOUNT']
                            if amount is None or amount == '':
                                amount = 0.0
                        if 'TXNDATE' in paytm_responce:
                            date_strip = paytm_responce['TXNDATE']
                            if date_strip is None or date_strip == '':
                                date_strip = None
                        mode = ''
                        if 'PAYMENTMODE' in paytm_responce:
                            if paytm_responce['PAYMENTMODE']:
                                mode = paytm_responce['PAYMENTMODE']

                        return Response(
                            {'status_code': 1,
                             'message': "Order fetch Successfully",
                             'ecom_order_id': order_text,
                             'paytm_order_id': order_id,
                             'transaction_id': paytm_responce['TXNID'],
                             'status': paytm_status,
                             'date': date_strip,
                             'amount': amount,
                             }
                        )

                    else:
                        return Response(
                            {'status_code': 2,
                             'message': "Something went wrong",
                             'ecom_order_id': order_text,
                             'paytm_order_id': order_id,
                             'transaction_id': None,
                             'status': paytm_status,
                             'date': date_strip,
                             'amount': amount,
                             }
                        )

        except Exception as e:
            import sys
            import os
            print('-----------in exception----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)


def ChecksumGenerateReturnDeliveryAmount(user_id, return_id, use_wallet):
    """
        Paytm Order Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"
    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    status = None
    message = None
    salt = None
    return_dict = {}
    try:
        merchant_key = ECOM_PROD_MERCHANT_KEY
        MID = ECOM_PROD_MERCHANT_MID
        INDUSTRY_TYPE_ID = ECOM_INDUSTRY_TYPE_ID
        CHANNEL_ID = ECOM_CHANNEL_ID
        WEBSITE = ECOM_WEBSITE
        # Callback_url = "https://ecom.thecreditkart.com/ecomapi/callback_return_delivery_amount_url/"
        # Callback_url = "http://13.126.93.37:8007/ecomapi/callback_return_delivery_amount_url/"
        # Callback_url = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="
        Callback_url = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="
        random_number = random.sample(range(99999), 1)
        CUST_ID = ""
        user = None
        if user_id:
            user = User.objects.filter(id=user_id)
            if user:
                CUST_ID = user[0].username

        get_return_order_obj = ReturnOrder.objects.filter(return_id=return_id)
        from .order import get_orders_delivery_charge
        get_delivery_amount = get_orders_delivery_charge(get_return_order_obj[0].order.id)
        ORDER_ID = str(get_return_order_obj[0].return_id) + 'e' + str(random_number[0])
        CALLBACK_URL = str(Callback_url) + '' + str(ORDER_ID)
        pay_amount = 0

        if use_wallet == "True":
            from EApi.order import get_user_wallet_amount, get_wallet_payable_amount
            wallet_dict = get_user_wallet_amount(user_id)
            wallet_amount = 0
            if wallet_dict:
                wallet_amount = wallet_dict["balance_amount"]

            data_dict = get_wallet_payable_amount(wallet_amount, get_delivery_amount)
            if data_dict:
                used_wallet = data_dict['used_wallet']
                pay_amount = data_dict['pay_amount']

                if used_wallet != 0:
                    wallet_status = ""
                    payment_status = ""
                    if pay_amount > 0:
                        # This means user is using his wallet to pay delivery charges..
                        # Removing wallet balance and adding wallet entry..
                        wallet_status = "return_delivery_charge_initiated"
                        payment_status = "initiated"
                    else:
                        wallet_status = "return_delivery_charge_success"
                        payment_status = "success"

                    update_wallet = user_wallet_sub(user_id, used_wallet, None, get_return_order_obj[0].order,
                                                    wallet_status)

                    initiate_payment = Payment.objects.create(order_id=str(ORDER_ID),
                                                              amount=float(used_wallet),
                                                              date=datetime.datetime.now(),
                                                              category='Order_Return', type="Ecom_Wallet", mode="PPI",
                                                              product_type="Ecom", status=payment_status)

                    if initiate_payment and initiate_payment is not None:
                        get_order_details_obj = OrderDetails.objects.filter(order_id=get_return_order_obj[0].order.id)
                        if get_order_details_obj:
                            get_order_details_obj[0].payment.add(initiate_payment)

                    if payment_status == "success":
                        # Update order status in order table..
                        order_id = get_return_order_obj[0].order.id
                        get_order_obj = Order.objects.filter(id=order_id)
                        if get_order_obj:
                            update_order = Order.objects.filter(id=order_id).update(status="return",
                                                                                    updated_date=datetime.datetime.now())

                            update_order_status = OrderStatus.objects.create(status="return_request",
                                                                             order=get_order_obj[0])

                        # Update ReturnOrder table status for particular order..
                        update_return_order = ReturnOrder.objects.filter(id=get_return_order_obj[0].id).update(
                            verify_return_status="return_request", payment_status="success",
                            updated_date=datetime.datetime.now())

                        status = 1
                        return_dict = {"status": "1", "message": "Return request accepted successfully"}
                        try:
                            number = user[0].username
                            osms_kwrgs = {
                                'sms_type': "return",
                                'number': str(number)
                            }
                            order_sms(**osms_kwrgs)
                        except Exception as e:
                            print("error in order return message")

                        return return_dict

                    if payment_status == "initiated":
                        TXN_AMOUNT = str(pay_amount)
                        dict_param = {
                            'MID': MID,
                            'ORDER_ID': ORDER_ID,
                            'CUST_ID': CUST_ID,
                            'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                            'CHANNEL_ID': CHANNEL_ID,
                            'TXN_AMOUNT': TXN_AMOUNT,
                            'WEBSITE': WEBSITE,
                            'CALLBACK_URL': CALLBACK_URL,
                        }
                        return_checksum = generate_checksum(dict_param, merchant_key, salt=None)

                        if return_checksum:
                            payment_add = Payment.objects.create(status='initiated',
                                                                 order_id=str(ORDER_ID), amount=float(TXN_AMOUNT),
                                                                 category='Order_Return',
                                                                 product_type="Ecom", date=datetime.datetime.now())

                            if initiate_payment and initiate_payment is not None:
                                get_order_details_obj = OrderDetails.objects.filter(
                                    order_id=get_return_order_obj[0].order.id)
                                if get_order_details_obj:
                                    get_order_details_obj[0].payment.add(payment_add)

                            response = {
                                'checksum': return_checksum,
                                'mid': MID,
                                'orderId': ORDER_ID,
                                'industryType': INDUSTRY_TYPE_ID,
                                'website': WEBSITE,
                                'channel': CHANNEL_ID,
                                'callback': CALLBACK_URL,
                                'amount': TXN_AMOUNT,
                                'cust_id': CUST_ID
                            }

                            return_dict = {"status": "2", "message": "Amount to pay", "checksum_response": response}
                            return return_dict

                        else:
                            return_dict = {"status": "4", "message": "Something went wrong"}
                            return return_dict

        else:
            pay_amount = get_delivery_amount

        if pay_amount != 0:
            TXN_AMOUNT = str(pay_amount)
            # TXN_AMOUNT = "1000"
            # Considering shopping order_id, CUST_ID and TXN_AMOUNT is getting.
            dict_param = {
                'MID': MID,
                'ORDER_ID': ORDER_ID,
                'CUST_ID': CUST_ID,
                'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                'CHANNEL_ID': CHANNEL_ID,
                'TXN_AMOUNT': TXN_AMOUNT,
                'WEBSITE': WEBSITE,
                'CALLBACK_URL': CALLBACK_URL,
            }
            return_checksum = generate_checksum(dict_param, merchant_key, salt=None)

            if return_checksum:
                payment_add = Payment.objects.create(status='initiated',
                                                     order_id=str(ORDER_ID), amount=float(TXN_AMOUNT),
                                                     category='Order_Return',
                                                     product_type="Ecom", date=datetime.datetime.now())

                # After the payment objects creation, it is needed to add this payment to respected order ID's
                # entry in orders table.
                if payment_add and payment_add is not None:
                    get_order_details_obj = OrderDetails.objects.filter(order_id=get_return_order_obj[0].order.id)
                    if get_order_details_obj:
                        get_order_details_obj[0].payment.add(payment_add)

                response = {
                    'checksum': return_checksum,
                    'mid': MID,
                    'orderId': ORDER_ID,
                    'industryType': INDUSTRY_TYPE_ID,
                    'website': WEBSITE,
                    'channel': CHANNEL_ID,
                    'callback': CALLBACK_URL,
                    'amount': TXN_AMOUNT,
                    'cust_id': CUST_ID
                }

                return_dict = {"status": "3", "message": "Amount to pay", "checksum_response": response}
                return return_dict

            else:
                return_dict = {"status": "4", "message": "Something went wrong"}
                return return_dict

    except Exception as e:
        print("-----Exception----in checksum web", e)
        import sys
        import os
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        return return_dict


def ChecksumGenerateReturnDeliveryAmountWebsite(user_id, return_id, use_wallet):
    """
        Paytm Order Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"
    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    status = None
    message = None
    salt = None
    return_dict = {}
    try:
        # data = request.data
        merchant_key = ECOM_PROD_MERCHANT_KEY
        MID = ECOM_PROD_MERCHANT_MID
        INDUSTRY_TYPE_ID = ECOM_INDUSTRY_TYPE_ID
        CHANNEL_ID = ECOM_WEBSITE_CHANNEL_ID
        WEBSITE = ECOM_WEBSITE_WEBSITE
        # Callback_url = "https://ecom.thecreditkart.com/ecomapi/callback_return_delivery_amount_url/"
        Callback_url = "https://ecom.thecreditkart.com/ecomapi/callback_return_delivery_amount_url/"
        # Callback_url = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="
        # Callback_url = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="
        random_number = random.sample(range(99999), 1)
        CUST_ID = ""
        user = None
        if user_id:
            user = User.objects.filter(id=user_id)
            if user:
                CUST_ID = user[0].username

        get_return_order_obj = ReturnOrder.objects.filter(return_id=return_id)
        from .order import get_orders_delivery_charge
        get_delivery_amount = get_orders_delivery_charge(get_return_order_obj[0].order.id)
        ORDER_ID = str(get_return_order_obj[0].return_id) + 'e' + str(random_number[0])
        CALLBACK_URL = str(Callback_url) + '' + str(ORDER_ID)
        pay_amount = 0

        if use_wallet == "True":
            from EApi.order import get_user_wallet_amount, get_wallet_payable_amount
            wallet_dict = get_user_wallet_amount(user_id)
            wallet_amount = 0
            if wallet_dict:
                wallet_amount = wallet_dict["balance_amount"]

            data_dict = get_wallet_payable_amount(wallet_amount, get_delivery_amount)
            if data_dict:
                used_wallet = data_dict['used_wallet']
                pay_amount = data_dict['pay_amount']

                if used_wallet != 0:
                    wallet_status = ""
                    payment_status = ""
                    if pay_amount > 0:
                        # This means user is using his wallet to pay delivery charges..
                        # Removing wallet balance and adding wallet entry..
                        wallet_status = "return_delivery_charge_initiated"
                        payment_status = "initiated"
                    else:
                        wallet_status = "return_delivery_charge_success"
                        payment_status = "success"

                    update_wallet = user_wallet_sub(user_id, used_wallet, None, get_return_order_obj[0].order,
                                                    wallet_status)

                    initiate_payment = Payment.objects.create(order_id=str(ORDER_ID),
                                                              amount=float(used_wallet),
                                                              date=datetime.datetime.now(),
                                                              category='Order_Return', type="Ecom_Wallet", mode="PPI",
                                                              product_type="Ecom", status=payment_status)

                    if initiate_payment and initiate_payment is not None:
                        get_order_details_obj = OrderDetails.objects.filter(order_id=get_return_order_obj[0].order.id)
                        if get_order_details_obj:
                            get_order_details_obj[0].payment.add(initiate_payment)

                    if payment_status == "success":
                        # Update order status in order table..
                        order_id = get_return_order_obj[0].order.id
                        get_order_obj = Order.objects.filter(id=order_id)
                        if get_order_obj:
                            update_order = Order.objects.filter(id=order_id).update(status="return",
                                                                                    updated_date=datetime.datetime.now())

                            update_order_status = OrderStatus.objects.create(status="return_request",
                                                                             order=get_order_obj[0])

                        # Update ReturnOrder table status for particular order..
                        update_return_order = ReturnOrder.objects.filter(id=get_return_order_obj[0].id).update(
                            verify_return_status="return_request", payment_status="success",
                            updated_date=datetime.datetime.now())

                        status = 1
                        return_dict = {"status": "1", "message": "Return request accepted successfully"}
                        try:
                            number = user[0].username
                            osms_kwrgs = {
                                'sms_type': "return",
                                'number': str(number)
                            }
                            order_sms(**osms_kwrgs)
                        except Exception as e:
                            print("error in order return message")

                        return return_dict

                    if payment_status == "initiated":
                        TXN_AMOUNT = str(pay_amount)
                        dict_param = {
                            'MID': MID,
                            'ORDER_ID': ORDER_ID,
                            'CUST_ID': CUST_ID,
                            'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                            'CHANNEL_ID': CHANNEL_ID,
                            'TXN_AMOUNT': TXN_AMOUNT,
                            'WEBSITE': WEBSITE,
                            'CALLBACK_URL': CALLBACK_URL,
                        }
                        return_checksum = generate_checksum(dict_param, merchant_key, salt=None)

                        if return_checksum:
                            payment_add = Payment.objects.create(status='initiated',
                                                                 order_id=str(ORDER_ID), amount=float(TXN_AMOUNT),
                                                                 category='Order_Return',
                                                                 product_type="Ecom", date=datetime.datetime.now())

                            if initiate_payment and initiate_payment is not None:
                                get_order_details_obj = OrderDetails.objects.filter(
                                    order_id=get_return_order_obj[0].order.id)
                                if get_order_details_obj:
                                    get_order_details_obj[0].payment.add(payment_add)

                            response = {
                                'checksum': return_checksum,
                                'mid': MID,
                                'orderId': ORDER_ID,
                                'industryType': INDUSTRY_TYPE_ID,
                                'website': WEBSITE,
                                'channel': CHANNEL_ID,
                                'callback': CALLBACK_URL,
                                'amount': TXN_AMOUNT,
                                'cust_id': CUST_ID
                            }

                            return_dict = {"status": "2", "message": "Amount to pay", "checksum_response": response}
                            return return_dict

                        else:
                            return_dict = {"status": "4", "message": "Something went wrong"}
                            return return_dict

        else:
            pay_amount = get_delivery_amount

        if pay_amount != 0:
            TXN_AMOUNT = str(pay_amount)
            # TXN_AMOUNT = "1000"
            # Considering shopping order_id, CUST_ID and TXN_AMOUNT is getting.
            dict_param = {
                'MID': MID,
                'ORDER_ID': ORDER_ID,
                'CUST_ID': CUST_ID,
                'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                'CHANNEL_ID': CHANNEL_ID,
                'TXN_AMOUNT': TXN_AMOUNT,
                'WEBSITE': WEBSITE,
                'CALLBACK_URL': CALLBACK_URL,
            }
            return_checksum = generate_checksum(dict_param, merchant_key, salt=None)

            if return_checksum:
                payment_add = Payment.objects.create(status='initiated',
                                                     order_id=str(ORDER_ID), amount=float(TXN_AMOUNT),
                                                     category='Order_Return',
                                                     product_type="Ecom", date=datetime.datetime.now())

                # After the payment objects creation, it is needed to add this payment to respected order ID's
                # entry in orders table.
                if payment_add and payment_add is not None:
                    get_order_details_obj = OrderDetails.objects.filter(order_id=get_return_order_obj[0].order.id)
                    if get_order_details_obj:
                        get_order_details_obj[0].payment.add(payment_add)
                        # print('here==========')

                response = {
                    'checksum': return_checksum,
                    'mid': MID,
                    'orderId': ORDER_ID,
                    'industryType': INDUSTRY_TYPE_ID,
                    'website': WEBSITE,
                    'channel': CHANNEL_ID,
                    'callback': CALLBACK_URL,
                    'amount': TXN_AMOUNT,
                    'cust_id': CUST_ID
                }

                return_dict = {"status": "3", "message": "Amount to pay", "checksum_response": response}
                return return_dict

            else:
                return_dict = {"status": "4", "message": "Something went wrong"}
                return return_dict

    except Exception as e:
        print("-----Exception----in checksum web", e)
        import sys
        import os
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        return return_dict


def Return_Order_Delivery_Charge_Status_Check_(order_id):
    """
        description : Paytm Order Checksum API for exchange product
        :param
            {
                "order_id":"",
            }

    """
    import requests
    status = "10"
    message = "Something went wrong"
    amount = 0.0
    date_strip = None
    try:
        if order_id:
            if 'e' in order_id:
                o_id = order_id
                order_data = str(order_id).split("e")
                return_id = ''
                if order_data:
                    return_id = order_data[0]
                paytm_status = ""
                merchant_key = ECOM_PROD_MERCHANT_KEY
                m_id = ECOM_PROD_MERCHANT_MID
                dict_param = {
                    'MID': m_id,
                    'ORDERID': o_id,
                }

                checksum = str(generate_checksum(dict_param, merchant_key))

                dict_param["CHECKSUMHASH"] = checksum
                url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                # url = 'https://securegw-stage.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                r = requests.get(url=url)
                if r.status_code == 200:

                    paytm_responce = r.json()

                    if paytm_responce['STATUS'] == 'TXN_SUCCESS':
                        paytm_status = 'success'
                    elif paytm_responce['STATUS'] == 'TXN_FAILURE':
                        paytm_status = 'failed'
                    elif paytm_responce['STATUS'] == 'PENDING':
                        paytm_status = 'pending'

                    if 'TXNAMOUNT' in paytm_responce:
                        amount = paytm_responce['TXNAMOUNT']
                        if amount is None or amount == '':
                            amount = 0.0
                    if 'TXNDATE' in paytm_responce:
                        date_strip = paytm_responce['TXNDATE']
                        if date_strip is None or date_strip == '':
                            date_strip = None
                    mode = ''
                    if 'PAYMENTMODE' in paytm_responce:
                        if paytm_responce['PAYMENTMODE']:
                            mode = paytm_responce['PAYMENTMODE']
                    update_app_payment = Payment.objects.filter(order_id=o_id,
                                                                category='Order_Return').exclude(
                        type="Ecom_Wallet").update(
                        status=paytm_status,
                        transaction_id=paytm_responce[
                            'TXNID'],
                        amount=amount,
                        mode=mode,
                        type='App',
                        response=paytm_responce,
                        date=date_strip,
                        update_date=datetime.datetime.now())

                    get_return_order_obj = ReturnOrder.objects.filter(return_id=return_id)
                    if get_return_order_obj:
                        if paytm_status == 'success':
                            # Here the actual return request order is created
                            update_return_order = ReturnOrder.objects.filter(return_id=return_id,
                                                                             verify_return_status="return_request",
                                                                             payment_status="not_paid").update(
                                payment_status="success")

                            # update status in Order table
                            Order.objects.filter(id=get_return_order_obj[0].order.id).update(status="return",
                                                                                             updated_date=datetime.datetime.now())

                            # Update status in OrderStatus table
                            add_order_status = OrderStatus.objects.create(order=get_return_order_obj[0].order,
                                                                          status="return_request",
                                                                          date=datetime.datetime.now())

                            # Update wallet payment if available..
                            get_wallet_payment = Payment.objects.filter(order_id=o_id,
                                                                        category='Order_Return', mode="PPI",
                                                                        type="Ecom_Wallet")

                            if get_wallet_payment:
                                update_wallet_paymet = Payment.objects.filter(id=get_wallet_payment[0].id).update(
                                    status=paytm_status,
                                    transaction_id=paytm_responce[
                                        'TXNID'],
                                    amount=amount,
                                    mode=mode,
                                    type='App',
                                    response=paytm_responce,
                                    date=date_strip,
                                    update_date=datetime.datetime.now())

                            # Update status of wallet used in Return Order..
                            get_wallet_obj = UserWallet.objects.filter(order=get_return_order_obj[0].order.id,
                                                                       status="return_delivery_charge_initiated").order_by(
                                'id').last()

                            if get_wallet_obj:
                                update_wallet = UserWallet.objects.filter(id=get_wallet_obj.id).update(
                                    status="return_delivery_charge_success", updated_date=datetime.datetime.now())

                            get_order_details_obj = OrderDetails.objects.filter(
                                order_id=get_return_order_obj[0].order.id)

                            try:
                                number = get_order_details_obj[0].user.username
                                osms_kwrgs = {
                                    'sms_type': "return",
                                    'number': str(number)
                                }
                                order_sms(**osms_kwrgs)
                            except Exception as e:
                                print("error in Order return message")

                            status = '1'
                            message = 'Transaction successful.'

                        elif paytm_status == 'failed':

                            status = '2'
                            message = 'Transaction failed'

                        elif paytm_status == 'pending':

                            status = '3'
                            message = 'Transaction pending'

                    return Response(
                        {
                            'status': status,
                            'message': message
                        }
                    )

    except Exception as e:
        import sys
        import os
        print('-----------in exception----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        return Response(
            {
                'status': status,
                'message': message
            }
        )


class ChecksumGenerateExchange(APIView):
    """
        description : Paytm Order Checksum API for exchange product
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"
        :param
            {
                "order_id":"",
                "amount":""
            }

    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    def post(self, request, *args, **kwargs):
        message = "Something Went Wrong"
        status = 0
        salt = None
        try:
            merchant_key = ECOM_PROD_MERCHANT_KEY
            MID = ECOM_PROD_MERCHANT_MID
            INDUSTRY_TYPE_ID = ECOM_INDUSTRY_TYPE_ID
            CHANNEL_ID = ECOM_CHANNEL_ID
            WEBSITE = ECOM_WEBSITE
            # Callback_url = "https://ecom.thecreditkart.com/ecomapi/callback_order_url/"
            # Callback_url = "http://13.126.93.37:8007/ecomapi/callback_order_url/"
            # Callback_url = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="
            Callback_url = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="

            random_number = random.sample(range(99999), 1)
            order_id = str(request.data.get('order_id'))  # order_id
            amount = str(request.data.get('amount'))  # order_id

            if "order_id" in request.data and "amount" in request.data:
                CUST_ID = ""
                user_id = 176
                if user_id:
                    user = User.objects.filter(id=user_id)
                    if user:
                        CUST_ID = user[0].username
                get_order_obj = Order.objects.filter(id=order_id)
                TXN_AMOUNT = str(amount)
                # Considering shopping order_id, CUST_ID and TXN_AMOUNT is getting.
                ORDER_ID = str(get_order_obj[0].unique_order_id) + 'e' + str(random_number[0])
                CALLBACK_URL = str(Callback_url) + '' + str(ORDER_ID)
                dict_param = {
                    'MID': MID,
                    'ORDER_ID': ORDER_ID,
                    'CUST_ID': CUST_ID,
                    'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                    'CHANNEL_ID': CHANNEL_ID,
                    'TXN_AMOUNT': TXN_AMOUNT,
                    'WEBSITE': WEBSITE,
                    'CALLBACK_URL': CALLBACK_URL,
                }
                # print('dict_parameter', dict_param)
                return_checksum = generate_checksum(dict_param, merchant_key, salt=None)
                if return_checksum:
                    payment = Payment.objects.create(status='initiated',
                                                     order_id=str(ORDER_ID), amount=float(TXN_AMOUNT),
                                                     category='Order_Exchange',
                                                     product_type="Ecom", date=datetime.datetime.now())
                    # After the payment objects creation, it is needed to add this payment to respected order ID's
                    # entry in orders table.
                    if payment and payment is not None:
                        get_order_details_obj = OrderDetails.objects.filter(order_id=order_id)
                        if get_order_details_obj:
                            get_order_details_obj[0].payment.add(payment)
                    message = "Successfully Done"
                    status = 1
                else:
                    message = "Something Went Wrong"
                    status = 0
                return Response(
                    {
                        'status': status,
                        'message': message,
                        'checksum': return_checksum,
                        'mid': MID,
                        'orderId': ORDER_ID,
                        'industryType': INDUSTRY_TYPE_ID,
                        'website': WEBSITE,
                        'channel': CHANNEL_ID,
                        'callback': CALLBACK_URL,
                        'amount': TXN_AMOUNT,
                        'cust_id': CUST_ID
                    }
                )
            else:
                return Response(
                    {
                        'status': 4,
                        'message': "Amount and Order id are mandatory"
                    }
                )
        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            f_name = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, f_name, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


def exchange_order_status_check(order_id):
    """
    :description = fetches the status of exchange order

    :param order_id:
    :return:
    """
    status = None
    message = None
    amount = 0.0
    date_strip = None
    try:
        if order_id:
            if 'e' in order_id:
                o_id = order_id
                order_data = str(order_id).split("e")
                unique_order_id = ''
                if order_data:
                    unique_order_id = order_data[0]
                paytm_status = ""
                merchant_key = ECOM_PROD_MERCHANT_KEY
                m_id = ECOM_PROD_MERCHANT_MID
                dict_param = {
                    'MID': m_id,
                    'ORDERID': o_id,
                }
                checksum = str(generate_checksum(dict_param, merchant_key))
                dict_param["CHECKSUMHASH"] = checksum
                url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                # url = 'https://securegw-stage.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                r = requests.get(url=url)
                if r.status_code == 200:
                    paytm_responce = r.json()
                    if paytm_responce['STATUS'] == 'TXN_SUCCESS':
                        paytm_status = 'success'
                    elif paytm_responce['STATUS'] == 'TXN_FAILURE':
                        paytm_status = 'failed'
                    elif paytm_responce['STATUS'] == 'PENDING':
                        paytm_status = 'pending'
                    if 'TXNAMOUNT' in paytm_responce:
                        amount = paytm_responce['TXNAMOUNT']
                        if amount is None or amount == '':
                            amount = 0.0
                    if 'TXNDATE' in paytm_responce:
                        date_strip = paytm_responce['TXNDATE']
                        if date_strip is None or date_strip == '':
                            date_strip = None
                    mode = ''
                    if 'PAYMENTMODE' in paytm_responce:
                        if paytm_responce['PAYMENTMODE']:
                            mode = paytm_responce['PAYMENTMODE']
                    Payment.objects.filter(order_id=o_id,
                                           category='Order_Exchange').update(status=paytm_status,
                                                                             transaction_id=paytm_responce[
                                                                                 'TXNID'],
                                                                             amount=amount,
                                                                             mode=mode,
                                                                             type='App',
                                                                             response=paytm_responce,
                                                                             date=date_strip,
                                                                             update_date=datetime.datetime.now())
                    get_orders_obj = Order.objects.filter(id=order_id)
                    if get_orders_obj:
                        if paytm_status == 'success':
                            # Here the actual return request order is created
                            update_return_order = ReturnOrder.objects.filter(order=order_id, status="exchange_request",
                                                                             payment_status="not_paid").update(
                                payment_status="success")
                            return True
    except Exception as e:
        import sys
        import os
        print('-----------in exception----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
