from datetime import timedelta
from datetime import datetime
import razorpay
import hmac
import hashlib
import random
from django.shortcuts import render, HttpResponse

from rest_framework.response import Response
from rest_framework.views import APIView
# from mudra.User.models import Personal
from django.apps.registry import apps
from django.contrib.auth.models import User
from MApi.razorpay import payment_status
import json

Payments = apps.get_model('Payments', 'Payment')

# Test cred
Key_id = 'rzp_test_FIEv6FzTK4cKxr'
Key_secret = 'AUAgWyzansR434zsZDLVzTOw'


# Prod cred
# Key_id = 'rzp_live_Y5lFYWm2huQpwi'
# Key_secret = 'guqrrTgcrdYvM5OapnCd1SR5'


class ERazorPayOrder(APIView):
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        """
        Generates razor pay's order id for a transaction
        And pass to Android / React APP
        :param
        Authentication token required
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
        :resp
        {
            "status": "1",
            "message": "Success",
            "data": {
                "order_id": "order_E0IwU12atgSak8",
                "amount": 100,
                "status": "created",
                "created_at": 1578120322,
                "currency": "INR",
                "image_url": "",
                "key_id": "rzp_test_FIEv6FzTK4cKxr"
            }
        }
        """
        ctx = {}
        resp_data = {}
        status = '3'
        message = 'Something went wrong'

        shopping_order_id = "hagsbdhay"
        random_number = random.sample(range(99999), 1)
        ORDER_ID = str(shopping_order_id[0]) + 'e' + str(random_number[0])
        amount = 100 * 100
        data = {'amount': amount, 'currency': 'INR', 'receipt': ORDER_ID, 'payment_capture': '1'}
        client = razorpay.Client(auth=(Key_id, Key_secret))
        order = client.order.create(data)
        if order:
            ctx['order'] = order
            resp_data = {
                'order_id': order['id'],
                'amount': str(order['amount']),
                'status': order['status'],
                'created_at': order['created_at'],
                'currency': order['currency'],
                'image_url': '',
                'key_id': Key_id
            }
            timestamp = order['created_at']
            try:
                timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')

            except Exception as e:
                print(e)
                pass

            real_amount = order['amount'] / 100

            Payments.objects.create(order_id=ORDER_ID, pg_transaction_id=resp_data['order_id'], \
                                    status="initiated", amount=real_amount, \
                                    category="Ecom", date=timestamp, return_status="rstatus", pay_source="Rpay")

            status = '1'
            message = 'Success'

        return Response(
            {
                'status': status,
                'message': message,
                'data': resp_data
            }
        )


def ecom_order_status(razorpay_order_id):
    """
    pg_transaction = razorpay's  Order Id
    transaction id = razorpay's  payment id

    :param request:
    :return:
    """
    resp_data = {}
    # razorpay_order_id = 'order_EFO6xKtWZ2vAad'
    # razorpay_order_id = 'order_EG5z0HmTnKg0yZ'
    # razorpay_order_id = 'order_EGTfjHRgclxSNZ'
    razorpay_order_id = razorpay_order_id
    if razorpay_order_id and razorpay_order_id is not None:
        order_id = razorpay_order_id
        client = razorpay.Client(auth=(Key_id, Key_secret))
        payment_data = client.order.payments(order_id)
        '''
        sample order response:
        {
            "status": 1,
            "message": "Success",
            "data": {
                "entity": "collection",
                "count": 3,
                "items": [
                    {
                        "id": "pay_E2fWoivS2dSL8W",
                        "entity": "payment",
                        "amount": 100,
                        "currency": "INR",
                        "status": "captured",
                        "order_id": "order_E2fVQot1GO6Od3",
                        "invoice_id": null,
                        "international": false,
                        "method": "card",
                        "amount_refunded": 0,
                        "refund_status": null,
                        "captured": true,
                        "description": "Pay to Mudrakwik",
                        "card_id": "card_E2fWonmuwO1p8P",
                        "bank": null,
                        "wallet": null,
                        "vpa": null,
                        "email": "mohandholu@gmail.com",
                        "contact": "+916352820504",
                        "notes": [],
                        "fee": 2,
                        "tax": 0,
                        "error_code": null,
                        "error_description": null,
                        "created_at": 1578636542
                    },
                    {
                        "id": "pay_E2fW33el9LYXLO",
                        "entity": "payment",
                        "amount": 100,
                        "currency": "INR",
                        "status": "failed",
                        "order_id": "order_E2fVQot1GO6Od3",
                        "invoice_id": null,
                        "international": false,
                        "method": "card",
                        "amount_refunded": 0,
                        "refund_status": null,
                        "captured": false,
                        "description": "Pay to Mudrakwik",
                        "card_id": "card_E2fW39nabhRTfD",
                        "bank": null,
                        "wallet": null,
                        "vpa": null,
                        "email": "mohandholu@gmail.com",
                        "contact": "+916352820504",
                        "notes": [],
                        "fee": null,
                        "tax": null,
                        "error_code": "BAD_REQUEST_ERROR",
                        "error_description": "Payment failed",
                        "created_at": 1578636498
                    },
                ]
            }
        }
        '''
        if "items" in payment_data:
            payments = payment_data['items']
            if payments is not None:
                for payment in payments:
                    payment_id = payment['id']
                    status_code = payment['status']

                    category = "Ecom"
                    type = 'App'
                    return_status = "rstatus"
                    pay_source = "Rpay"

                    # This is to save status of payment
                    if status_code == "captured":
                        status = "success"
                    elif status_code == "failed":
                        status = "failed"
                    elif status == "authorized":
                        status = "pending"

                    created_at = payment['created_at']

                    # This is to save mode of the payment
                    if payment['method'] == 'card':
                        mode = 'DC'
                    elif payment['method'] == 'upi':
                        mode = 'UPI'
                    elif payment['method'] == 'netbanking':
                        mode = 'NB'
                    else:
                        mode = 'RWT'

                    # This is to save description
                    if payment['error_description'] is None:
                        description = ""
                    else:
                        description = payment['error_description']

                    # This will convert timestamp to timedelta with datetime
                    timestamp = datetime.fromtimestamp(int(created_at)).strftime('%Y-%m-%d %H:%M:%S')
                    date = datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S')
                    # check for 1st attempt
                    # In 1st attempt status will be initiated and RazorPay order id will be present
                    # in db in pg_transaction_id column
                    payment_obj = Payments.objects.filter(pg_transaction_id=order_id,
                                                          status="initiated")
                    if payment_obj:
                        # 1st Attempt
                        # Update payment object
                        payment_update = payment_obj.update(transaction_id=payment_id,
                                                            status=status,
                                                            date=date,
                                                            response=payment,
                                                            mode=mode,
                                                            description=description,
                                                            category=category,
                                                            type=type,
                                                            return_status=return_status,
                                                            pay_source=pay_source,
                                                            update_date=datetime.now())
                        pay_check = Payments.objects.filter(transaction_id=payment_id, pg_transaction_id=order_id)

                    else:
                        # multiple attempts
                        # verify id the transaction is already present with Rpay payment id (unique)

                        get_payment_obj = Payments.objects.filter(transaction_id=payment_id, pg_transaction_id=order_id)
                        if not get_payment_obj:
                            # This payment with transaction_id (Rpay payment id) is not present
                            # Get the payment obj from pg_transaction_id (Rpay order id)
                            payment_obj = Payments.objects.filter(pg_transaction_id=order_id).last()
                            if payment_obj:
                                order_id = payment_obj[0].order_id
                                order_id_list = order_id.split('e')
                                shopping_order_id = order_id_list[0]

                                random_number = random.sample(range(99999), 1)
                                ORDER_ID = str(shopping_order_id) + 'e' + str(random_number[0])
                                amount = int(payment['amount']) / 100

                                paycheck = Payments.objects.create(order_id=ORDER_ID,
                                                                   transaction_id=payment_id,
                                                                   pg_transaction_id=order_id,
                                                                   status=status,
                                                                   amount=amount,
                                                                   category="Ecom",
                                                                   mode=mode, type='App',
                                                                   response=payment, date=date,
                                                                   description=description,
                                                                   return_status=return_status,
                                                                   pay_source="Rpay")

    return True


class ERazorPaySignCheck(APIView):
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    '''
    :param
    {
        'razorpay_payment_id'="sdsadwd"
        'razorpay_order_id' = ""
        'razorpay_signature = ''
    }
    '''

    def post(self, request, *args, **kwargs):
        message = ''
        mode = ''
        status = 0
        shopping_order_id = 123456789
        razorpay_payment_id = request.data.get('razorpay_payment_id')
        razorpay_order_id = request.data.get('razorpay_order_id')
        razorpay_signature = request.data.get('razorpay_signature')
        # generated_signature = hmac_sha256(razorpay_order_id + "|" + razorpay_payment_id, secret);
        client = razorpay.Client(auth=(Key_id, Key_secret))
        params_dict = {
            'razorpay_order_id': razorpay_order_id,
            'razorpay_payment_id': razorpay_payment_id,
            'razorpay_signature': razorpay_signature
        }
        msg = "{}|{}".format(str(params_dict.get('razorpay_order_id')), str(params_dict.get('razorpay_payment_id')))

        secret = str(Key_secret)

        key = bytes(secret, 'utf-8')
        body = bytes(msg, 'utf-8')

        dig = hmac.new(key=key,
                       msg=body,
                       digestmod=hashlib.sha256)

        generated_signature = dig.hexdigest()
        result = hmac.compare_digest(generated_signature, razorpay_signature)
        if result == True:

            order_resp_data = ecom_order_status(razorpay_order_id)
            if order_resp_data and order_resp_data is not None:
                status = 1
                message = " Sign Verification Success"

            payment_data = payment_status(razorpay_payment_id)
            if payment_data and payment_data is not None:

                if payment_data['method'] == 'card':
                    mode = 'DC'
                elif payment_data['method'] == 'upi':
                    mode = 'UPI'
                elif payment_data['method'] == 'netbanking':
                    mode = 'NB'
                else:
                    mode = 'RWT'

                timestamp = payment_data['created_at']
                try:
                    timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')

                except Exception as e:
                    print(e)
                    pass

                first_success = Payments.objects.filter(transaction_id=None, pg_transaction_id=razorpay_order_id,
                                                        status='initiated')

                if first_success:
                    updated_pay = Payments.objects.filter(pg_transaction_id=razorpay_order_id)
                    updated_pay.update(
                        transaction_id=payment_data['payment_id'], mode=mode, type='App',
                        response=payment_data, category="Ecom", status='success', date=timestamp)

                else:
                    random_number = random.sample(range(99999), 1)
                    ORDER_ID = str(shopping_order_id) + 'e' + str(random_number[0])
                    payment_amount = int(payment_data['amount']) / 100
                    paycheck = Payments.objects.create(order_id=ORDER_ID, transaction_id=payment_data['payment_id'],
                                                       pg_transaction_id=razorpay_order_id,
                                                       status="success", amount=payment_amount,
                                                       category="Ecom", mode=mode, type='App',
                                                       response=payment_data, date=timestamp, return_status="rstatus",
                                                       pay_source="Rpay")

            return Response(
                {
                    'status': status,
                    'message': message,
                    'data': order_resp_data['status']
                }
            )
        else:
            order_resp_data = ecom_order_status(razorpay_order_id)
            if order_resp_data and order_resp_data is not None:
                status = 2
                message = " Sign Verification Failed"

            payment_data = payment_status(razorpay_payment_id)
            if payment_data and payment_data is not None:
                if payment_data['method'] == 'card':
                    mode = 'DC'
                elif payment_data['method'] == 'upi':
                    mode = 'UPI'
                elif payment_data['method'] == 'netbanking':
                    mode = 'NB'
                else:
                    mode = 'RWT'
                timestamp = payment_data['created_at']
                try:
                    timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')


                except Exception as e:
                    print(e)
                    pass

                datetime_object = datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S')
                exp_date = datetime_object + timedelta(days=89)
                if payment_data['status'] == 'captured':
                    stat = 'success'

                elif payment_data['status'] == 'authorized':
                    stat = 'pending'

                else:
                    stat = 'failed'

                first_success = Payments.objects.filter(transaction_id=None, pg_transaction_id=razorpay_order_id,
                                                        status='initiated')
                if first_success:
                    updated_pay = Payments.objects.filter(pg_transaction_id=razorpay_order_id)
                    updated_pay.update(
                        transaction_id=payment_data['payment_id'], mode=mode, type='App',
                        response=payment_data, status=stat, category="Ecom", date=timestamp,
                        description=payment_data['error_description'])
                    pay_back = Payments.objects.filter(transaction_id=payment_data['payment_id'],
                                                       pg_transaction_id=razorpay_order_id)

                else:

                    random_number = random.sample(range(99999), 1)
                    ORDER_ID = str(shopping_order_id) + 'e' + str(random_number[0])
                    payment_amount = int(payment_data['amount']) / 100

                    paycheck = Payments.objects.create(order_id=ORDER_ID, transaction_id=payment_data['payment_id'],
                                                       pg_transaction_id=razorpay_order_id,
                                                       status="success", amount=payment_amount,
                                                       category="Ecom", mode=mode, type='App',
                                                       response=payment_data, date=timestamp,
                                                       description=payment_data['error_description'],
                                                       return_status="rstatus", pay_source="Rpay")

        return Response(
            {
                'status': status,
                'message': message,
                'data': order_resp_data['status']
            }
        )


class ERazorpayWebhookOrderNew(APIView):
    """
    -- Sample response:
                        {
                          "entity":"event",
                          "account_id":"acc_BFQ7uQEaa7j2z7",
                          "event":"order.paid",
                          "contains":[
                            "payment",
                            "order"
                          ],
                          "payload":{
                            "payment":{
                              "entity":{
                                "id":"pay_DESlfW9H8K9uqM",
                                "entity":"payment",
                                "amount":100,
                                "currency":"INR",
                                "status":"captured",
                                "order_id":"order_DESlLckIVRkHWj",
                                "invoice_id":null,
                                "international":false,
                                "method":"netbanking",
                                "amount_refunded":0,
                                "refund_status":null,
                                "captured":true,
                                "description":null,
                                "card_id":null,
                                "bank":"HDFC",
                                "wallet":null,
                                "vpa":null,
                                "email":"gaurav.kumar@example.com",
                                "contact":"+919876543210",
                                "notes":[],
                                "fee":2,
                                "tax":0,
                                "error_code":null,
                                "error_description":null,
                                "created_at":1567674599
                              }
                            },
                            "order":{
                              "entity":{
                                "id":"order_DESlLckIVRkHWj",
                                "entity":"order",
                                "amount":100,
                                "amount_paid":100,
                                "amount_due":0,
                                "currency":"INR",
                                "receipt":"rcptid #1",
                                "offer_id":null,
                                "status":"paid",
                                "attempts":1,
                                "notes":[],
                                "created_at":1567674581
                                }
                              }
                            },
                           "created_at":1567674606
                        }

    """

    def post(self, request, *args, **kwargs):
        request_data = {}
        status = "3"
        message = 'Something Went Wrong'

        headers = request.META
        request_data = request.data

        client = razorpay.Client(auth=(Key_id, Key_secret))
        webhook_signature = headers['HTTP_X_RAZORPAY_SIGNATURE']
        webhook_secret = 'Mudrakwik@123'
        payload_body = json.dumps(request.data, separators=(',', ':'))

        sign_result = client.utility.verify_webhook_signature(payload_body, webhook_signature, webhook_secret)
        # Sign_result returns error if the signature doesn't match.
        if 'payload' in request_data:
            payload_data = request_data['payload']
            if 'payment' in payload_data:
                if 'entity' in payload_data['payment']:
                    entity_data = request_data['payload']['payment']['entity']
                    if entity_data is not None:
                        order_id = entity_data['order_id']
                        get_all_orders = ecom_order_status(order_id)
