import base64
import datetime
import hashlib
import json
import os
import random
import string
import sys
from datetime import timedelta

import requests
# from Crypto.Cipher import AES
from django.apps.registry import apps
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Sum
from django.shortcuts import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import *
from rest_framework.permissions import *
from rest_framework.response import Response
from rest_framework.views import APIView
from MApi.checksum_web import generate_checksum

from rest_framework import status

Payment = apps.get_model('Payments', 'Payment')
OrderDetails = apps.get_model('ecom', 'OrderDetails')


class EcomPaytmPaymentApiView(APIView):
    """
    This Api takes Paytm response and save te response data.
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        status = None
        message = None

        try:
            # paytm response
            o_id = request.data.get('oid')
            shopping_data = str(o_id).split("m")
            shopping_order_id = ''
            if shopping_data:
                shopping_order_id = shopping_data[0]

            paytm_status = ""
            merchant_key = settings.PROD_REP_MERCHANT_KEY
            m_id = settings.PROD_REP_MERCHANT_MID
            dict_param = {
                'MID': m_id,
                'ORDERID': o_id,

            }

            checksum = str(generate_checksum(dict_param, merchant_key))

            dict_param["CHECKSUMHASH"] = checksum

            url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
            r = requests.get(url=url)
            paytm_responce = r.json()

            if paytm_responce['STATUS'] == 'TXN_SUCCESS':
                paytm_status = 'success'
            elif paytm_responce['STATUS'] == 'TXN_FAILURE':
                paytm_status = 'failed'
            elif paytm_responce['STATUS'] == 'PENDING':
                paytm_status = 'pending'

            amount = paytm_responce['TXNAMOUNT']
            date_strip = paytm_responce['TXNDATE']
            mode = ''

            for name in paytm_responce.keys():
                if name == "PAYMENTMODE":
                    if paytm_responce['PAYMENTMODE']:
                        mode = paytm_responce['PAYMENTMODE']
            if amount is None or not amount:
                amount = 0.0

            if date_strip is None or not date_strip:
                date_strip = None

            payment = Payment.objects.filter(order_id=o_id)
            if payment and payment is not None:
                payment_update = Payment.objects.filter(order_id=o_id).update(status=paytm_status,
                                                                              transaction_id=paytm_responce['TXNID'],
                                                                              amount=float(amount),
                                                                              category='Ecom',
                                                                              mode=mode,
                                                                              type='App',
                                                                              pay_source="Ptm",
                                                                              return_status="ps2s",
                                                                              response_code=paytm_responce['RESPCODE'],
                                                                              description=paytm_responce['RESPMSG'],
                                                                              response=paytm_responce,
                                                                              date=date_strip
                                                                              )

                if payment_update:
                    order_details = OrderDetails.objects.filter(order_text=shopping_order_id)
                    if order_details:
                        order_details[0].payment.add(payment)

        except Exception as e:
            import sys
            import os
            print('-----------in exception----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
