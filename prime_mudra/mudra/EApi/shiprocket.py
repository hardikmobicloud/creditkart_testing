import datetime
import json

import requests
from MApi.email import sms
from django.apps.registry import apps
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics
from rest_framework.response import Response

SHIPROCKET_EMAIL = "keval.thakkar@mudrakwik.com"
SHIPROCKET_PASSWORD = "keval@1234"

Order = apps.get_model('ecom', 'Order')
OrderDetails = apps.get_model('ecom', 'OrderDetails')
OrderDetailsOrder = apps.get_model('ecom', 'orderdetails_order_id')
LogisticOrder = apps.get_model('ecom', 'LogisticOrder')
SellerWarehouse = apps.get_model('ecom', 'SellerWarehouse')
Personal = apps.get_model('User', 'Personal')
OrderStatus = apps.get_model('ecom', 'OrderStatus')
PincodeMaster = apps.get_model('StaticData', 'PincodeMaster')
OrderLocation = apps.get_model('ecom', 'OrderLocation')
Images = apps.get_model('ecom', 'Images')
ShiprocketToken = apps.get_model('ecom', 'ShiprocketToken')
ShiprocketOrder = apps.get_model('ecom', 'ShiprocketOrder')
Seller = apps.get_model('ecom', 'Seller')
UserWallet = apps.get_model('ecom', 'UserWallet')
ProductDetails = apps.get_model('ecom', 'ProductDetails')
ReturnOrder = apps.get_model('ecom', 'ReturnOrder')
ShiprocketShipmentPickup = apps.get_model('ecom', 'ShiprocketShipmentPickup')
ShiprocketLabel = apps.get_model('ecom', 'ShiprocketLabel')
ShiprocketManifest = apps.get_model('ecom', 'ShiprocketManifest')
ShiprocketApiInfo = apps.get_model('ecom', 'ShiprocketApiInfo')


def call_shiprocket_api(url, payload, method):
    """
    params: url , payload , method
    This function returns API response of shiprocket api.
    """
    get_token = get_sr_token()
    authorization = "Bearer " + get_token
    headers = {
        'Content-Type': 'application/json',
        'Authorization': authorization
    }
    response = requests.request(method, url, headers=headers, data=json.dumps(payload))
    response = response.text
    response = json.loads(response)
    return response


def sr_generate_token_api():
    """
     This  api generate token. In payload it pass email id and password

    :return:
    success resp:
    {
        "id": 786501,
        "first_name": "API",
        "last_name": "USER",
        "email": "keval.thakkar@mudrakwik.com",
        "company_id": 722134,
        "created_at": "2020-09-08 11:08:00",
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjc4NjUwMSwiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1OTk4ODg4NTIsImV4cCI6MTYwMDc1Mjg1MiwibmJmIjoxNTk5ODg4ODUyLCJqdGkiOiJWdXVOeUlMVjZ0NVJ6VjdvIn0.iguxAKVITctYPDoyilEHgI40Ll22Wvrm_nuYywLYMj8"
    }
    """
    email = SHIPROCKET_EMAIL
    password = SHIPROCKET_PASSWORD
    url = "https://apiv2.shiprocket.in/v1/external/auth/login"
    headers = {
        'Content-Type': 'application/json'
    }
    payload = {"email": email, "password": password}

    response = requests.request("POST", url, headers=headers, data=json.dumps(payload))
    response = response.text
    response = json.loads(response)

    return response


def create_token(resp):
    """
    params: resp
    This api create entry in ship rocket token of resp token,resp first_name,resp last name , resp
    email.
    """
    expiry_date = resp["created_at"]
    expiry_date = get_expiry_date(expiry_date)
    create_obj = ShiprocketToken.objects.create(token=resp["token"], first_name=resp["first_name"],
                                                last_name=resp["last_name"],
                                                email_id=resp["email"], company_id=str(resp["company_id"]),
                                                date=resp["created_at"], expiry_date=expiry_date, status=True)
    return create_obj


def get_expiry_date(date):
    """
    params:date
    This function return exp_date by adding 10 days in a input date parameter.
    """
    date_time_obj = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
    exp_date = date_time_obj + datetime.timedelta(days=10)
    return exp_date


def update_token(resp):
    """
    params : resp
    This api get last entry from ship rocket token and get expiry date by using get_expiry_date and
    update ship rocket lat entry by parameter resp.This function return update ship rocket obj and
    return created token.
    """
    updated_date = datetime.datetime.now()
    get_token_obj = ShiprocketToken.objects.filter(status=True).order_by('id').last()
    if get_token_obj:
        expiry_date = resp["created_at"]
        expiry_date = get_expiry_date(expiry_date)
        update_obj = ShiprocketToken.objects.filter(id=get_token_obj.id)
        if update_obj:
            update_obj[0].token = resp["token"]
            update_obj[0].first_name = resp["first_name"]
            update_obj[0].last_name = resp["last_name"]
            update_obj[0].email_id = resp["email"]
            update_obj[0].company_id = str(resp["company_id"])
            update_obj[0].date = resp["created_at"]
            update_obj[0].updated_date = updated_date
            update_obj[0].expiry_date = expiry_date
            update_obj[0].status = True
            update_obj[0].save()
        return update_obj
    else:
        tok_resp = create_token(resp)
        return tok_resp


def generate_token():
    """
    This function generate token by using sr_generate_token_api and update it by update_token
    """
    resp = sr_generate_token_api()
    if resp:
        return update_token(resp)
    else:
        return False


def get_sr_token():
    """
    This function get ship rocket token last entry and expiry date from it.If tomorrow date is greater than
    equal to expiry date and return updated token.
    """
    token = None
    tomorrow_date = datetime.datetime.now() + datetime.timedelta(days=1)
    yesterday = (tomorrow_date - datetime.timedelta(days=1)).strftime('%Y-%m-%d')
    get_token = ShiprocketToken.objects.order_by('id').last()
    if get_token:
        expiry_date = get_token.expiry_date
        if expiry_date:
            before_expiry_date = expiry_date - datetime.timedelta(days=1)
            if tomorrow_date >= before_expiry_date:
                # This is the time to regenerate Token..
                update_token = generate_token()
                if update_token:
                    # getting latest success token entry from table..
                    get_token = ShiprocketToken.objects.filter(status=True).order_by('id').last()
                    if get_token:
                        token = get_token.token
            else:
                token = get_token.token
    else:
        update_token = generate_token()
        if update_token:
            # getting latest success token entry from table..
            get_token = ShiprocketToken.objects.filter(status=True).order_by('id').last()
            if get_token:
                token = get_token.token

    return token


def is_service_available(pickup_postcode, delivery_postcode, weight, cod, declared_value, is_return):
    """
    params:pickup_postcode,delivery_postcode,weight,cod,declared_value,is_return
    This function returns service available for the given parameters and returns the delivery partner company ID.
    """

    service_available = False
    pickup_company_id = None
    pickup_company_name = None

    return_data = {}
    if None not in (pickup_postcode, delivery_postcode, weight, cod, is_return):
        is_returnable = None
        if is_return == "Yes":
            is_returnable = "1"
        if is_return == "No":
            is_returnable = "0"
        payload = {
            "pickup_postcode": pickup_postcode,
            "delivery_postcode": delivery_postcode,
            "weight": weight,
            "cod": cod,
            "declared_value": declared_value,
            "is_return": is_returnable
        }
        # Calling Service availability API
        url = "https://apiv2.shiprocket.in/v1/external/courier/serviceability/"
        method = "GET"

        get_service = call_shiprocket_api(url, payload, method)
        if get_service:
            status = None
            if "status" in get_service:
                status = get_service["status"]
            if "status_code" in get_service:
                status = get_service["status_code"]

            if status == 200:
                # This is success data..
                if "data" in get_service:
                    if "recommended_courier_company_id" in get_service["data"]:
                        service_available = True
                        pickup_company_id = get_service["data"]["recommended_courier_company_id"]

    print("fwd pickup_company_id", pickup_company_id)
    return_data["service_available"] = service_available
    return_data["pickup_company_id"] = pickup_company_id

    return return_data


def get_nickname(seller_name):
    """
    params: seller_name
    This function return nickname by getting number from seller warehouse last entry and add it with input parameter
    seller_name.
    """
    if seller_name is not None:
        nickname = ""
        short_name = seller_name[:4]
        warehouse_obj = SellerWarehouse.objects.filter(type="shiprocket").order_by('id').last()
        if warehouse_obj:
            number = int(warehouse_obj.nickname[-4:])
            new_number = str(number + 1)
            nickname = short_name + new_number
        else:
            nickname = short_name + "1001"
        return nickname


@method_decorator([login_required(login_url="/ecom/login/")], name='dispatch')
class AddShiprocketWarehouse(generic.ListView):
    template_name = "Ecom/logistics/Add_shiprocket_warehouse.html"

    def get(self, request, *args, **kwargs):
        ctx = {}
        if kwargs:
            msg = kwargs['message']
            ctx['msg'] = msg
        sellers = User.objects.filter(groups__name="Seller")
        if sellers:
            # print('sellers', sellers)
            ctx['sellers'] = sellers

        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        ctx = {}
        seller = request.POST["seller"]
        name = request.POST["shop_name"]
        email = request.POST["email"]
        mobile_no = request.POST["mobile_no"]
        address = request.POST["address"]
        address_2 = request.POST["address_2"]
        city = request.POST["city"]
        state = request.POST["state"]
        country = request.POST["country"]
        pin_code = request.POST["pincode"]
        user = User.objects.filter(username=seller)

        if None not in (name, email, mobile_no, address,
                        address_2, city, state, country, pin_code, user[0]):

            # Getting nickname for this Seller name..
            seller_nickname = ""
            nickname = get_nickname(user[0].username)
            if nickname:
                seller_nickname = nickname

            # creating entry in sellers warehouse model..
            add_warehouse = SellerWarehouse.objects.create(seller=user[0],
                                                           nickname=seller_nickname,
                                                           company_name=name, primary_address=address,
                                                           secondary_address=address_2, mobile_no=mobile_no,
                                                           pin_code=pin_code, city_id=city,
                                                           state_id=state, country_id=country,
                                                           type="shiprocket",
                                                           approval_status="pending")

            # Creating payload for creating pickup location API..
            url = "https://apiv2.shiprocket.in/v1/external/settings/company/addpickup"
            method = "POST"
            payload = {
                "pickup_location": seller_nickname,
                "name": name,
                "email": email,
                "phone": mobile_no,
                "address": address,
                "address_2": address_2,
                "city": city,
                "state": state,
                "country": country,
                "pin_code": pin_code
            }

            # Calling Add pickup location API..
            success = False
            adding_status = False
            message = ""
            add_pickup = call_shiprocket_api(url, payload, method)
            if add_pickup:
                if "success" in add_pickup:
                    success = add_pickup["success"]
                if "errors" in add_pickup:
                    success = False

                    message = str(add_pickup["errors"])
                    message = message.replace('/', '')

            if success:
                # Update warehouse in Seller Warehouse data..
                adding_status = True
                message = "Warehouse added Successfully"

            # Updating entry in Warehouse table for this response..
            warehouse_obj = SellerWarehouse.objects.filter(seller=user[0], adding_status=False,
                                                           approval_status__icontains="pending").order_by('id').last()
            if warehouse_obj:
                warehouse_obj.adding_status = adding_status
                warehouse_obj.response = add_pickup
                warehouse_obj.save()

            return HttpResponseRedirect(
                reverse('add_shiprocket_warehouse', kwargs={"message": message}))

        else:
            message = "All fields are mandatory"
            return HttpResponseRedirect(
                reverse('add_shiprocket_warehouse', kwargs={'message': message}))


def generate_shipment_awb(shipment_id, courier_id, is_return):
    """
    This function assign awb number for a shipment..
    response = {
        status = 1,
        message = "success",
        awb_number = "109139884724",
        courior_company_id = "10",
        courior_name = "Delhivery"
    }
    """
    response_data = {}
    awb_number = None
    courior_company_id = None
    courior_name = None
    status = None
    order_mode = None
    message = None
    if shipment_id is not None:
        is_returnable = None
        payload = None
        if is_return == "Yes":
            is_returnable = "1"
            order_mode = "Reverse"
            payload = {
                "shipment_id": shipment_id,
                "courier_id": courier_id,
                "is_return": is_returnable
            }
        else:
            order_mode = "Delivery"
            payload = {
                "shipment_id": shipment_id,
                "courier_id": courier_id
            }

        url = "https://apiv2.shiprocket.in/v1/external/courier/assign/awb"
        method = "POST"
        order_obj = None
        awb_number = ""
        courior_name = ""
        get_shipment_obj = ShiprocketOrder.objects.filter(shipment_id=shipment_id)
        if get_shipment_obj:
            order_obj = get_shipment_obj[0].order

        # Here we save the logistics order details..
        check_logistic_order = LogisticOrder.objects.filter(type="shiprocket", order=order_obj, order_mode=order_mode,
                                                            status="success")

        add_logistic_order = None
        if not check_logistic_order:
            add_logistic_order = LogisticOrder.objects.create(order=order_obj, waybill_no=awb_number,
                                                              logistics=courior_name,
                                                              order_mode=order_mode, type="shiprocket")

        # Calling Request for Shipment pickup API..
        generate_awb = call_shiprocket_api(url, payload, method)
        status_code = None
        if generate_awb:
            awb_generate = False
            if "status_code" in generate_awb:
                status_code = generate_awb["status_code"]
                if status_code == 422:
                    status = 2
                    awb_generate = False
                    if "message" in generate_awb:
                        message = generate_awb["message"]

                        # Updating data in ShiprocketOrder table..

                        update_shiprocket_order = ShiprocketOrder.objects.filter(id=get_shipment_obj[0].id)
                        if update_shiprocket_order:
                            update_shiprocket_order[0].courier_company_id = courior_company_id
                            update_shiprocket_order[0].courier_name = courior_name
                            update_shiprocket_order[0].awb_code = awb_number
                            update_shiprocket_order[0].response = generate_awb
                            update_shiprocket_order[0].save()

                        # Updating data in LogisticOrder data..
                        if add_logistic_order:
                            update_logistic_order = LogisticOrder.objects.filter(id=add_logistic_order.id)
                            if update_logistic_order:
                                update_logistic_order[0].waybill_no = awb_number
                                update_logistic_order[0].logistics = courior_name
                                update_logistic_order[0].remark = generate_awb
                                update_logistic_order[0].status = "error"
                                update_logistic_order[0].save()

                if status_code == 400:
                    """
                    response = {'message': 'Oops! Cannot reassign courier for this shipment for 24 hour(s).',
                    'status_code': 400}
                    """
                    status = 1
                    awb_generate = True

                    # Updating data in ShiprocketOrder table..
                    update_shiprocket_order = ShiprocketOrder.objects.filter(id=get_shipment_obj[0].id)
                    if update_shiprocket_order:
                        update_shiprocket_order[0].courier_company_id = courior_company_id
                        update_shiprocket_order[0].courier_name = courior_name
                        update_shiprocket_order[0].awb_code = awb_number
                        update_shiprocket_order[0].response = generate_awb
                        update_shiprocket_order[0].save()

                    # Updating data in LogisticOrder data..
                    if add_logistic_order:
                        update_logistic_order = LogisticOrder.objects.filter(id=add_logistic_order.id)
                        if update_logistic_order:
                            update_logistic_order[0].waybill_no = awb_number
                            update_logistic_order[0].logistics = courior_name
                            update_logistic_order[0].remark = generate_awb
                            update_logistic_order[0].status = "error"
                            update_logistic_order[0].save()

            elif "awb_assign_status" in generate_awb:
                assign_status = generate_awb["awb_assign_status"]
                if assign_status == 0:
                    status = 2
                    awb_generate = False
                    # Updating data in Shiprocket Order table..

                    update_shiprocket_order = ShiprocketOrder.objects.filter(id=get_shipment_obj[0].id)
                    if update_shiprocket_order:
                        update_shiprocket_order[0].courier_company_id = courior_company_id
                        update_shiprocket_order[0].courier_name = courior_name
                        update_shiprocket_order[0].awb_code = awb_number
                        update_shiprocket_order[0].response = generate_awb
                        update_shiprocket_order[0].save()

                    # Updating data in LogisticOrder data..
                    if add_logistic_order:
                        update_logistic_order = LogisticOrder.objects.filter(id=add_logistic_order.id)
                        if update_logistic_order:
                            update_logistic_order[0].waybill_no = awb_number
                            update_logistic_order[0].logistics = courior_name
                            update_logistic_order[0].remark = generate_awb
                            update_logistic_order[0].status = "error"
                            update_logistic_order[0].save()

                else:
                    if "response" in generate_awb:
                        if "data" in generate_awb["response"]:
                            status = 1
                            awb_generate = True
                            message = "success"
                            if "courier_company_id" in generate_awb["response"]["data"]:
                                courior_company_id = generate_awb["response"]["data"]["courier_company_id"]
                                awb_number = generate_awb["response"]["data"]["awb_code"]

                            if "courier_name" in generate_awb["response"]:
                                courior_name = generate_awb["response"]["courier_name"]

                            # Updating data in ShiprocketOrder table..

                            update_shiprocket_order = ShiprocketOrder.objects.filter(id=get_shipment_obj[0].id)
                            if update_shiprocket_order:
                                update_shiprocket_order[0].courier_company_id = courior_company_id
                                update_shiprocket_order[0].courier_name = courior_name
                                update_shiprocket_order[0].awb_code = awb_number
                                update_shiprocket_order[0].save()

                            # Updating data in LogisticOrder data..
                            if add_logistic_order:
                                update_logistic_order = LogisticOrder.objects.filter(id=add_logistic_order.id)
                                if update_logistic_order:
                                    update_logistic_order[0].waybill_no = awb_number
                                    update_logistic_order[0].logistics = courior_name
                                    update_logistic_order[0].remark = generate_awb
                                    update_logistic_order[0].status = "success"
                                    update_logistic_order[0].save()

            else:
                if "response" in generate_awb:
                    if "data" in generate_awb["response"]:
                        status = 1
                        awb_generate = True
                        message = "success"
                        if "courier_company_id" in generate_awb["response"]["data"]:
                            courior_company_id = generate_awb["response"]["data"]["courier_company_id"]
                            awb_number = generate_awb["response"]["data"]["awb_code"]

                        if "courier_name" in generate_awb["response"]:
                            courior_name = generate_awb["response"]["courier_name"]

                        # Updating data in ShiprocketOrder table..

                        update_shiprocket_order = ShiprocketOrder.objects.filter(id=get_shipment_obj[0].id)
                        if update_shiprocket_order:
                            update_shiprocket_order[0].courier_company_id = courior_company_id
                            update_shiprocket_order[0].courier_name = courior_name
                            update_shiprocket_order[0].awb_code = awb_number
                            update_shiprocket_order[0].save()

                        # Updating data in LogisticOrder data..
                        if add_logistic_order:
                            update_logistic_order = LogisticOrder.objects.filter(id=add_logistic_order.id)
                            if update_logistic_order:
                                update_logistic_order[0].waybill_no = awb_number
                                update_logistic_order[0].logistics = courior_name
                                update_logistic_order[0].remark = generate_awb
                                update_logistic_order[0].status = "success"
                                update_logistic_order[0].save()

            # Adding API response in ShiprocketApiInfo model..
            api_name = "awb_generate"
            try:
                save_response = shiprocket_api_response_save(order_obj.id, api_name, awb_generate, generate_awb,
                                                             order_mode)
            except Exception as e:
                print("Inside Exception of shiprocket_api_response_save for awb_generate", e)

        response_data["status"] = status
        response_data["message"] = message
        response_data["awb_number"] = awb_number
        response_data["courior_company_id"] = courior_company_id
        response_data["courior_name"] = courior_name

    return response_data


def generate_order_details_shipment_awb(shipment_id, courier_id, is_return):
    """
    params: shipment_id, courier_id,is_return
    This function assign awb number for a shipment..
    success response = {
        status = 1,
        message = "success",
        awb_number = "109139884724",
        courior_company_id = "10",
        courior_name = "Delhivery"
    }
    error response = {'message': 'Oops! Cannot reassign courier for this shipment for 24 hour(s).',
                    'status_code': 400}
    """
    response_data = {}
    awb_number = None
    courior_company_id = None
    courior_name = None
    status = None
    order_mode = None
    message = None
    if shipment_id is not None:
        is_returnable = None
        payload = None
        if is_return == "Yes":
            is_returnable = "1"
            order_mode = "Reverse"
            payload = {
                "shipment_id": shipment_id,
                "courier_id": courier_id,
                "is_return": is_returnable
            }
        else:
            order_mode = "Delivery"
            payload = {
                "shipment_id": shipment_id,
                "courier_id": courier_id
            }

        url = "https://apiv2.shiprocket.in/v1/external/courier/assign/awb"
        method = "POST"
        order_details_obj = None
        awb_number = ""
        courior_name = ""
        get_shipment_obj = ShiprocketOrder.objects.filter(shipment_id=shipment_id)
        if get_shipment_obj:
            order_details_obj = get_shipment_obj[0].order_details

        # Here we save the logistics order details..
        check_logistic_order = LogisticOrder.objects.filter(type="shiprocket", order_details=order_details_obj,
                                                            order_mode=order_mode, status="success")

        add_logistic_order = None
        if not check_logistic_order:
            add_logistic_order = LogisticOrder.objects.create(order_details=order_details_obj,
                                                              waybill_no=awb_number,
                                                              logistics=courior_name,
                                                              order_mode=order_mode, type="shiprocket")

        # Calling Request for Shipment pickup API..
        generate_awb = call_shiprocket_api(url, payload, method)
        status_code = None
        if generate_awb:
            awb_generate = False
            if "status_code" in generate_awb:
                status_code = generate_awb["status_code"]
                if status_code == 422:
                    status = 2
                    awb_generate = False
                    if "message" in generate_awb:
                        message = generate_awb["message"]

                        # Updating data in ShiprocketOrder table..
                        update_shiprocket_order = ShiprocketOrder.objects.filter(id=get_shipment_obj[0].id)
                        if update_shiprocket_order:
                            update_shiprocket_order[0].courier_company_id = courior_company_id
                            update_shiprocket_order[0].courier_name = courior_name
                            update_shiprocket_order[0].awb_code = awb_number
                            update_shiprocket_order[0].response = generate_awb
                            update_shiprocket_order[0].save()

                        # Updating data in LogisticOrder data..
                        if add_logistic_order:
                            update_logistic_order = LogisticOrder.objects.filter(id=add_logistic_order.id)
                            if update_logistic_order:
                                update_logistic_order[0].waybill_no = awb_number
                                update_logistic_order[0].logistics = courior_name
                                update_logistic_order[0].remark = generate_awb
                                update_logistic_order[0].status = "error"
                                update_logistic_order[0].save()

                if status_code == 400:
                    """
                    response = {'message': 'Oops! Cannot reassign courier for this shipment for 24 hour(s).',
                    'status_code': 400}
                    """
                    status = 1
                    awb_generate = True

                    # Updating data in ShiprocketOrder table..
                    update_shiprocket_order = ShiprocketOrder.objects.filter(id=get_shipment_obj[0].id)
                    if update_shiprocket_order:
                        update_shiprocket_order[0].courier_company_id = courior_company_id
                        update_shiprocket_order[0].courier_name = courior_name
                        update_shiprocket_order[0].awb_code = awb_number
                        update_shiprocket_order[0].response = generate_awb
                        update_shiprocket_order[0].save()

                    # Updating data in LogisticOrder data..
                    if add_logistic_order:
                        update_logistic_order = LogisticOrder.objects.filter(id=add_logistic_order.id)
                        if update_logistic_order:
                            update_logistic_order[0].waybill_no = awb_number
                            update_logistic_order[0].logistics = courior_name
                            update_logistic_order[0].remark = generate_awb
                            update_logistic_order[0].status = "error"
                            update_logistic_order[0].save()

            elif "awb_assign_status" in generate_awb:
                assign_status = generate_awb["awb_assign_status"]
                if assign_status == 0:
                    status = 2
                    awb_generate = False
                    # Updating data in Shiprocket Order table..

                    update_shiprocket_order = ShiprocketOrder.objects.filter(id=get_shipment_obj[0].id)
                    if update_shiprocket_order:
                        update_shiprocket_order[0].courier_company_id = courior_company_id
                        update_shiprocket_order[0].courier_name = courior_name
                        update_shiprocket_order[0].awb_code = awb_number
                        update_shiprocket_order[0].response = generate_awb
                        update_shiprocket_order[0].save()

                    # Updating data in LogisticOrder data..
                    if add_logistic_order:
                        update_logistic_order = LogisticOrder.objects.filter(id=add_logistic_order.id)
                        if update_logistic_order:
                            update_logistic_order[0].waybill_no = awb_number
                            update_logistic_order[0].logistics = courior_name
                            update_logistic_order[0].remark = generate_awb
                            update_logistic_order[0].status = "error"
                            update_logistic_order[0].save()

                else:
                    if "response" in generate_awb:
                        if "data" in generate_awb["response"]:
                            status = 1
                            awb_generate = True
                            message = "success"
                            if "courier_company_id" in generate_awb["response"]["data"]:
                                courior_company_id = generate_awb["response"]["data"]["courier_company_id"]
                                awb_number = generate_awb["response"]["data"]["awb_code"]

                            if "courier_name" in generate_awb["response"]:
                                courior_name = generate_awb["response"]["courier_name"]

                            # Updating data in ShiprocketOrder table..

                            update_shiprocket_order = ShiprocketOrder.objects.filter(id=get_shipment_obj[0].id)
                            if update_shiprocket_order:
                                update_shiprocket_order[0].courier_company_id = courior_company_id
                                update_shiprocket_order[0].courier_name = courior_name
                                update_shiprocket_order[0].awb_code = awb_number
                                update_shiprocket_order[0].save()

                            # Updating data in LogisticOrder data..
                            if add_logistic_order:
                                update_logistic_order = LogisticOrder.objects.filter(id=add_logistic_order.id)
                                if update_logistic_order:
                                    update_logistic_order[0].waybill_no = awb_number
                                    update_logistic_order[0].logistics = courior_name
                                    update_logistic_order[0].remark = generate_awb
                                    update_logistic_order[0].status = "success"
                                    update_logistic_order[0].save()

            else:
                if "response" in generate_awb:
                    if "data" in generate_awb["response"]:
                        status = 1
                        awb_generate = True
                        message = "success"
                        if "courier_company_id" in generate_awb["response"]["data"]:
                            courior_company_id = generate_awb["response"]["data"]["courier_company_id"]
                            awb_number = generate_awb["response"]["data"]["awb_code"]

                        if "courier_name" in generate_awb["response"]:
                            courior_name = generate_awb["response"]["courier_name"]

                        # Updating data in ShiprocketOrder table..
                        update_shiprocket_order = ShiprocketOrder.objects.filter(id=get_shipment_obj[0].id)
                        if update_shiprocket_order:
                            update_shiprocket_order[0].courier_company_id = courior_company_id
                            update_shiprocket_order[0].courier_name = courior_name
                            update_shiprocket_order[0].awb_code = awb_number
                            update_shiprocket_order[0].save()

                        # Updating data in LogisticOrder data..
                        if add_logistic_order:
                            update_logistic_order = LogisticOrder.objects.filter(id=add_logistic_order.id)
                            if update_logistic_order:
                                update_logistic_order[0].waybill_no = awb_number
                                update_logistic_order[0].logistics = courior_name
                                update_logistic_order[0].remark = generate_awb
                                update_logistic_order[0].status = "success"
                                update_logistic_order[0].save()

            # Adding API response in ShiprocketApiInfo model..
            api_name = "awb_generate"
            try:
                save_response = shiprocket_order_details_api_response_save(order_details_obj.id, api_name, awb_generate,
                                                                           generate_awb,
                                                                           order_mode)
            except Exception as e:
                print("Inside Exception of shiprocket_api_response_save for awb_generate", e)

        response_data["status"] = status
        response_data["message"] = message
        response_data["awb_number"] = awb_number
        response_data["courior_company_id"] = courior_company_id
        response_data["courior_name"] = courior_name

    return response_data


def request_shipment_pickup_api(shiprocket_order_obj):
    """
    sample success resp
    {
      "pickup_status": 1,
      "response": {
        "pickup_scheduled_date": {
          "date": "2019-07-01 12:27:50.112667",
          "timezone_type": 3,
          "timezone": "Asia/Kolkata"
        },
        "pickup_token_number": "Reference No: 1111_BIGFOOT 11111_11111111",
        "status": 3,
        "pickup_generated_date": {
          "date": "2019-07-01 12:27:50.112693",
          "timezone_type": 3,
          "timezone": "Asia/Kolkata"
        },
        "data": "Pickup is confirmed by Xpressbees Surface For AWB :- 11111111111111"
      }
    }


    :param shipment_id:
    :return:
    """
    shipment_id = None
    order_id = None
    get_sr_order = ShiprocketOrder.objects.filter(id=shiprocket_order_obj.id)
    if get_sr_order:
        shipment_id = get_sr_order[0].shipment_id
        if get_sr_order[0].order:
            order_id = get_sr_order[0].order.id

    if shipment_id is not None:
        get_sr_pickup_obj = ShiprocketShipmentPickup.objects.create(sr_order=get_sr_order[0], status="initiated")

        url = 'https://apiv2.shiprocket.in/v1/external/courier/generate/pickup'
        payload = {
            "shipment_id": shipment_id,
        }
        method = 'POST'
        resp = call_shiprocket_api(url, payload, method)
        if resp:
            type = "Delivery"
            pickup_schedule = False
            api_name = "pickup_schedule"
            if "pickup_status" in resp:
                pickup_status = resp['pickup_status']
                if pickup_status == 1:
                    pickup_schedule = True
                    response = resp['response']
                    pickup_scheduled_date = None
                    if "pickup_scheduled_date" in response:
                        pickup_scheduled_date = response['pickup_scheduled_date']

                    pickup_token_number = None
                    if "pickup_token_number" in response:
                        pickup_token_number = str(response['pickup_token_number'])

                    status = None
                    if "status" in response:
                        status = response['status']

                    data = None
                    if "data" in response:
                        data = response['data']
                    try:

                        update_shipment_pickup = ShiprocketShipmentPickup.objects.filter(
                            id=get_sr_pickup_obj.id)
                        if update_shipment_pickup:
                            update_shipment_pickup[0].pickup_token_number = pickup_token_number
                            update_shipment_pickup[0].pickup_status = pickup_status
                            update_shipment_pickup[0].data = data
                            update_shipment_pickup[0].scheduled_date = pickup_scheduled_date
                            update_shipment_pickup[0].sr_status = str(status)
                            update_shipment_pickup[0].status = "success"
                            update_shipment_pickup[0].response = resp
                            update_shipment_pickup[0].save()

                        # Adding API status in ShiprocketApiStatus Model..
                        try:
                            add_status = shiprocket_api_response_save(order_id, api_name, pickup_schedule, resp, type)
                        except Exception as e:
                            print("inside Exception of shiprocket_api_response_save of create order", e)

                    except Exception as e:
                        import os
                        import sys
                        print('-----------in exception request_shipment_pickup_api----------')
                        print(e.args)
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                        print(exc_type, fname, exc_tb.tb_lineno)

                        update_shipment_pickup = ShiprocketShipmentPickup.objects.filter(
                            id=get_sr_pickup_obj.id)
                        if update_shipment_pickup:
                            update_shipment_pickup[0].status = "success"
                            update_shipment_pickup[0].response = resp
                            update_shipment_pickup[0].save()
                    return True
                else:
                    pickup_schedule = False

                    update_shipment_pickup = ShiprocketShipmentPickup.objects.filter(id=get_sr_pickup_obj.id)
                    if update_shipment_pickup:
                        update_shipment_pickup[0].status = "error"
                        update_shipment_pickup[0].response = resp
                        update_shipment_pickup[0].save()

            else:
                pickup_schedule = False

                update_shipment_pickup = ShiprocketShipmentPickup.objects.filter(id=get_sr_pickup_obj.id)
                if update_shipment_pickup:
                    update_shipment_pickup[0].status = "error"
                    update_shipment_pickup[0].response = resp
                    update_shipment_pickup[0].save()

            # Adding API status in ShiprocketApiStatus Model..
            try:
                add_status = shiprocket_api_response_save(order_id, api_name, pickup_schedule, resp, type)
            except Exception as e:
                print("inside Exception of shiprocket_api_response_save of create order", e)

    return False


def request_order_details_shipment_pickup_api(shiprocket_order_obj):
    """
    params: shiprocket_order_obj
    sample success resp
    {
      "pickup_status": 1,
      "response": {
        "pickup_scheduled_date": {
          "date": "2019-07-01 12:27:50.112667",
          "timezone_type": 3,
          "timezone": "Asia/Kolkata"
        },
        "pickup_token_number": "Reference No: 1111_BIGFOOT 11111_11111111",
        "status": 3,
        "pickup_generated_date": {
          "date": "2019-07-01 12:27:50.112693",
          "timezone_type": 3,
          "timezone": "Asia/Kolkata"
        },
        "data": "Pickup is confirmed by Xpressbees Surface For AWB :- 11111111111111"
      }
    }


    :param shipment_id:
    :return:
    """
    shipment_id = None
    order_details_id = None
    get_sr_order = ShiprocketOrder.objects.filter(id=shiprocket_order_obj.id)
    if get_sr_order:
        shipment_id = get_sr_order[0].shipment_id
        if get_sr_order[0].order_details:
            order_details_id = get_sr_order[0].order_details.id

    if shipment_id is not None:
        get_sr_pickup_obj = ShiprocketShipmentPickup.objects.create(sr_order=get_sr_order[0], status="initiated")

        url = 'https://apiv2.shiprocket.in/v1/external/courier/generate/pickup'
        payload = {
            "shipment_id": shipment_id,
        }
        method = 'POST'
        resp = call_shiprocket_api(url, payload, method)
        if resp:
            type = "Delivery"
            pickup_schedule = False
            api_name = "pickup_schedule"
            if "pickup_status" in resp:
                pickup_status = resp['pickup_status']
                if pickup_status == 1:
                    pickup_schedule = True
                    response = resp['response']
                    pickup_scheduled_date = None
                    if "pickup_scheduled_date" in response:
                        pickup_scheduled_date = response['pickup_scheduled_date']
                        # scheduled_date = response['pickup_scheduled_date']['date']

                    pickup_token_number = None
                    if "pickup_token_number" in response:
                        pickup_token_number = str(response['pickup_token_number'])

                    status = None
                    if "status" in response:
                        status = response['status']

                    data = None
                    if "data" in response:
                        data = response['data']
                    try:

                        update_shipment_pickup = ShiprocketShipmentPickup.objects.filter(
                            id=get_sr_pickup_obj.id)
                        if update_shipment_pickup:
                            update_shipment_pickup[0].pickup_token_number = pickup_token_number
                            update_shipment_pickup[0].pickup_status = pickup_status
                            update_shipment_pickup[0].data = data
                            update_shipment_pickup[0].scheduled_date = pickup_scheduled_date
                            update_shipment_pickup[0].sr_status = str(status)
                            update_shipment_pickup[0].status = "success"
                            update_shipment_pickup[0].response = resp
                            update_shipment_pickup[0].save()

                        # Adding API status in ShiprocketApiStatus Model..
                        try:
                            add_status = shiprocket_order_details_api_response_save(order_details_id, api_name,
                                                                                    pickup_schedule, resp, type)
                        except Exception as e:
                            print("inside Exception of shiprocket_api_response_save of create order", e)

                    except Exception as e:
                        import os
                        import sys
                        print('-----------in exception request_shipment_pickup_api----------')
                        print(e.args)
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                        print(exc_type, fname, exc_tb.tb_lineno)

                        update_shipment_pickup = ShiprocketShipmentPickup.objects.filter(
                            id=get_sr_pickup_obj.id)
                        if update_shipment_pickup:
                            update_shipment_pickup[0].status = "success"
                            update_shipment_pickup[0].response = resp
                            update_shipment_pickup[0].save()
                    return True
                else:
                    pickup_schedule = False
                    update_shipment_pickup = ShiprocketShipmentPickup.objects.filter(id=get_sr_pickup_obj.id)
                    if update_shipment_pickup:
                        update_shipment_pickup[0].status = "error"
                        update_shipment_pickup[0].response = resp
                        update_shipment_pickup[0].save()
            else:
                pickup_schedule = False
                update_shipment_pickup = ShiprocketShipmentPickup.objects.filter(id=get_sr_pickup_obj.id)
                if update_shipment_pickup:
                    update_shipment_pickup[0].status = "error"
                    update_shipment_pickup[0].response = resp
                    update_shipment_pickup[0].save()

            # Adding API status in ShiprocketApiStatus Model..
            try:
                add_status = shiprocket_order_details_api_response_save(order_details_id, api_name, pickup_schedule,
                                                                        resp, type)
            except Exception as e:
                print("inside Exception of shiprocket_api_response_save of create order", e)

    return False


def get_approved_sellers_pickup_location(seller_id):
    """
    param: seller_id
    This function return pickup location and warehouse object from the seller warehouse last seller entry

    """
    return_data = {}
    pickup_location = None
    warehouse_obj = SellerWarehouse.objects.filter(seller_id=seller_id, type="shiprocket",
                                                   approval_status__icontains="approved",
                                                   adding_status=True).order_by('id').last()
    if warehouse_obj:
        pickup_location = warehouse_obj.nickname

    # print('pickup_location', pickup_location)
    return_data["pickup_location"] = pickup_location
    return_data["warehouse_obj"] = warehouse_obj
    return return_data


def get_orders_payment_mode(order_id=None, order_details_id=None):
    """
      param: seller_id
      This function return payment_mode.

      """
    payment_mode = "Prepaid"
    if order_id is not None:
        get_order_details_obj = OrderDetails.objects.filter(order_id=order_id)
        if get_order_details_obj:
            if get_order_details_obj[0].payment_mode:
                if get_order_details_obj[0].payment_mode == "cod":
                    payment_mode = "COD"
        return payment_mode

    if order_details_id is not None:
        get_order_details_obj = OrderDetails.objects.filter(id=order_details_id).first()
        if get_order_details_obj.payment_mode:
            if get_order_details_obj.payment_mode == "cod":
                payment_mode = "COD"
        return payment_mode

    return payment_mode


def get_orders_cod_amount(order_id):
    """
    params: order_id
    This function return total_amount by getting order first entry by order id.
    """
    total_amount = 0
    if order_id:
        get_order_obj = Order.objects.filter(id=order_id).first()
        if get_order_obj:
            total_amount += get_order_obj.total_amount
            if get_order_obj.delivery_charges:
                total_amount += get_order_obj.delivery_charges.amount
            if get_order_obj.convenient_fees:
                total_amount += get_order_obj.convenient_fees.amount

    return total_amount


def get_order_related_data(order_id):
    """
        returns order related data from a particular order for shiprocket api
    :param unique_order_id:
    :return:
    """
    payload = {}
    channel_id = "883772"  # This is a Custom channel ID
    billing_customer_name = None
    billing_last_name = None
    billing_address = None
    billing_city = None
    billing_pincode = None
    billing_state = None
    billing_email = None
    billing_phone = None
    payment_method = "Prepaid"
    if order_id is not None:
        get_order_obj = Order.objects.filter(id=order_id).first()
        if get_order_obj:
            quantity = str(get_order_obj.quantity)
            weight_in_gm = get_order_obj.product_details.weight
            weight = (weight_in_gm / 1000)
            product_name = get_order_obj.product_details.product_id.name
            get_order_details_obj = OrderDetails.objects.filter(order_id=get_order_obj.id).first()
            if get_order_details_obj:
                f_name = ""
                l_name = ""
                if get_order_details_obj.address.personal:
                    name = get_order_details_obj.address.personal.name
                    temp_name = name.split(" ")
                    if len(temp_name) >= 2:
                        f_name = temp_name[0]
                        l_name = temp_name[-1]
                    billing_address = get_order_details_obj.address.address
                    billing_email = get_order_details_obj.address.personal.email_id
                    billing_phone = get_order_details_obj.address.personal.mobile_no
                billing_customer_name = f_name
                billing_last_name = l_name
                billing_city = get_order_details_obj.address.city
                billing_pincode = get_order_details_obj.address.pin_code
                billing_state = get_order_details_obj.address.state

            sku = 'RAMDOM_SKU'
            if get_order_obj.product_details.sku:
                try:
                    import re
                    sku_data = get_order_obj.product_details.sku
                    result = re.sub("[+ /  * % , ^]", "_", sku_data)
                    sku = result.replace("\\", "_")
                    if len(sku) < 3:
                        sku = sku + "CK_SKU"
                    if len(sku) > 50:
                        sku = sku[:49]
                except Exception as e:
                    sku = 'RAMDOM_SKU'
                    print("inside exception of SKU", e)

            shipping_charges = 0
            sub_total = get_order_obj.total_amount
            selling_price = get_order_obj.amount
            mode = get_orders_payment_mode(order_id)
            if mode:
                payment_method = mode
            if mode == "COD":
                sub_total = get_orders_cod_amount(order_id)

            seller_id = get_order_obj.product_details.user.id
            pickup_location = ""
            pickup_location_data = get_approved_sellers_pickup_location(seller_id)
            if pickup_location_data:
                pickup_location = pickup_location_data["pickup_location"]
            # pickup_location = "mudra"
            length = 0.5
            breadth = 0.5
            height = 0.5

            payload = {
                "order_id": get_order_obj.unique_order_id,
                "order_date": str(get_order_obj.created_date),
                "pickup_location": pickup_location,
                "channel_id": channel_id,
                "comment": "",
                "billing_customer_name": billing_customer_name,
                "billing_last_name": billing_last_name,
                "billing_address": billing_address,
                "billing_address_2": "",
                "billing_city": billing_city,
                "billing_pincode": billing_pincode,
                "billing_state": billing_state,
                "billing_country": "India",
                "billing_email": billing_email,
                "billing_phone": billing_phone,
                "shipping_is_billing": "1",
                "shipping_customer_name": "",
                "shipping_last_name": "",
                "shipping_address": "",
                "shipping_address_2": "",
                "shipping_city": "",
                "shipping_pincode": "",
                "shipping_country": "",
                "shipping_state": "",
                "shipping_email": "",
                "shipping_phone": "",
                "order_items": [
                    {
                        "name": product_name,
                        "sku": sku,
                        "units": quantity,
                        "selling_price": selling_price,
                        "discount": "",
                        "tax": "",
                        "hsn": ""
                    }
                ],
                "payment_method": payment_method,
                "shipping_charges": 0,
                "giftwrap_charges": 0,
                "transaction_charges": 0,
                "total_discount": 0,
                "sub_total": sub_total,
                "length": length,
                "breadth": breadth,
                "height": height,
                "weight": weight,
            }
    return payload


def get_box_packing_dimensions(length_list, breadth_list, height_list):
    """
    params:length_list,breadth_list,height_list
    This function return data dict containing maximum length, breadth and height.
    """
    data_dict = {}
    length = 0.5
    breadth = 0.5
    height = 0.5
    if length_list and breadth_list and height_list is not None:
        length = max(length_list)
        breadth = max(breadth_list)
        height = sum(height_list)
        data_dict = {
            "length": length,
            "breadth": breadth,
            "height": height
        }
    return data_dict


def get_order_details_related_data(order_details_id):
    """
        returns order related data from a particular order for shiprocket api
    :param unique_order_id:
    :return:
    """
    payload = {}
    channel_id = "883772"  # This is a Custom channel ID
    billing_customer_name = None
    billing_last_name = None
    billing_address = None
    billing_city = None
    billing_pincode = None
    billing_state = None
    billing_email = None
    billing_phone = None
    total_weight = 0
    total_amount = 0
    extra_amount = 0
    total_quantity = 0
    payment_method = "Prepaid"
    order_items_list = []
    length_list = []
    breadth_list = []
    height_list = []
    length = 0.5
    breadth = 0.5
    height = 0.5
    if order_details_id is not None:
        try:
            get_order_details_obj = OrderDetails.objects.filter(id=order_details_id).first()
            if get_order_details_obj:
                get_orders = get_order_details_obj.combine_order.filter(status="placed").all()
                if get_orders:
                    for order in get_orders:
                        get_length = 0.5
                        get_breadth = 0.5
                        get_height = 0.5
                        product_name = order.product_details.product_id.name
                        quantity = order.quantity
                        total_quantity += quantity
                        weight_in_gm = order.product_details.weight
                        weight = (weight_in_gm / 1000)
                        total_weight += weight
                        if order.product_details.Length:
                            get_length = order.product_details.Length
                        length_list.append(float(get_length))
                        if order.product_details.width:
                            get_breadth = order.product_details.width
                        breadth_list.append(float(get_breadth))
                        if order.product_details.height:
                            get_height = order.product_details.height
                        height_list.append(float(get_height))
                        selling_price = order.amount
                        total_amount += order.total_amount
                        mode = get_orders_payment_mode(order.id)
                        if mode:
                            payment_method = mode
                        if mode == "COD":
                            cod_amount = get_orders_cod_amount(order.id)
                            total_amount += cod_amount - order.total_amount
                        sku = 'RAMDOM_SKU'
                        if order.product_details.sku:
                            try:
                                import re
                                sku_data = order.product_details.sku
                                result = re.sub("[+ /  * % , ^]", "_", sku_data)
                                sku = result.replace("\\", "_")
                                if len(sku) < 3:
                                    sku = sku + "CK_SKU"
                                if len(sku) > 50:
                                    sku = sku[:49]
                            except Exception as e:
                                sku = 'RAMDOM_SKU'
                                print("inside exception of SKU", e)
                        order_dict = {
                            "name": product_name,
                            "sku": sku,
                            "units": quantity,
                            "selling_price": selling_price,
                            "discount": "",
                            "tax": "",
                            "hsn": ""
                        }
                        order_items_list.append(order_dict)
                dimensions = get_box_packing_dimensions(length_list, breadth_list, height_list)
                if dimensions:
                    length = dimensions['length']
                    breadth = dimensions['breadth']
                    height = dimensions['height']
                f_name = ""
                l_name = ""
                if get_order_details_obj.address.personal:
                    name = get_order_details_obj.address.personal.name
                    temp_name = name.split(" ")
                    if len(temp_name) >= 2:
                        f_name = temp_name[0]
                        l_name = temp_name[-1]
                    billing_address = get_order_details_obj.address.address
                    billing_email = get_order_details_obj.address.personal.email_id
                    billing_phone = get_order_details_obj.address.personal.mobile_no
                billing_customer_name = f_name
                billing_last_name = l_name
                billing_city = get_order_details_obj.address.city
                billing_pincode = get_order_details_obj.address.pin_code
                billing_state = get_order_details_obj.address.state
                shipping_charges = 0
                sub_total = total_amount + extra_amount
                seller_id = None
                seller_obj = User.objects.filter(username="MFPL").first()
                if seller_obj:
                    seller_id = seller_obj.id
                pickup_location = ""
                pickup_location_data = get_approved_sellers_pickup_location(seller_id)
                if pickup_location_data:
                    pickup_location = pickup_location_data["pickup_location"]
                payload = {
                    "order_id": get_order_details_obj.order_text,
                    "order_date": str(get_order_details_obj.created_date),
                    "pickup_location": pickup_location,
                    "channel_id": channel_id,
                    "comment": "",
                    "billing_customer_name": billing_customer_name,
                    "billing_last_name": billing_last_name,
                    "billing_address": billing_address,
                    "billing_address_2": "",
                    "billing_city": billing_city,
                    "billing_pincode": billing_pincode,
                    "billing_state": billing_state,
                    "billing_country": "India",
                    "billing_email": billing_email,
                    "billing_phone": billing_phone,
                    "shipping_is_billing": "1",
                    "shipping_customer_name": "",
                    "shipping_last_name": "",
                    "shipping_address": "",
                    "shipping_address_2": "",
                    "shipping_city": "",
                    "shipping_pincode": "",
                    "shipping_country": "",
                    "shipping_state": "",
                    "shipping_email": "",
                    "shipping_phone": "",
                    "order_items": order_items_list,
                    "payment_method": payment_method,
                    "shipping_charges": 0,
                    "giftwrap_charges": 0,
                    "transaction_charges": 0,
                    "total_discount": 0,
                    "sub_total": sub_total,
                    "length": length,
                    "breadth": breadth,
                    "height": height,
                    "weight": total_weight,
                }
        except Exception as e:
            print("------USer all details exc", e)
            print('-----------in exception----------')
            print(e.args)
            import os
            import sys
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)

    # print("order details payload", payload)
    return payload


def get_return_order_related_data(order_id):
    """
    returns order related data from a particular order for shiprocket api
    :param unique_order_id:
    :return:
    """
    data = {}
    order_date = None
    channel_id = "883772"  # This is a Custom channel ID
    pickup_customer_name = ""
    pickup_last_name = ""
    pickup_address = ""
    pickup_address_2 = ""
    pickup_city = ""
    pickup_state = ""
    pickup_country = "India"
    pickup_pincode = None
    pickup_email = ""
    pickup_phone = ""
    pickup_isd_code = None
    pickup_location_id = None
    shipping_customer_name = ""
    shipping_last_name = ""
    shipping_address = ""
    shipping_address_2 = ""
    shipping_city = ""
    shipping_country = "India"
    shipping_pincode = None
    shipping_state = ""
    shipping_email = ""
    shipping_isd_code = None
    shipping_phone = ""
    sku = None
    product_name = None
    units = None
    selling_price = 0
    discount = 0
    hsn = ""
    payment_method = "Prepaid"
    total_discount = "0"
    sub_total = None
    length = 0.5
    breadth = 0.5
    height = 0.5
    weight = None

    if order_id is not None:
        get_order_obj = Order.objects.filter(id=order_id).first()
        if get_order_obj:
            return_order_id = ""
            return_order_obj = ReturnOrder.objects.filter(order=get_order_obj, verify_return_status="return_request",
                                                          payment_status="success").order_by('id').last()
            if return_order_obj:
                return_order_id = return_order_obj.return_id

            order_id = return_order_id
            order_date = str(get_order_obj.created_date)
            units = str(get_order_obj.quantity)
            weight_in_gm = get_order_obj.product_details.weight
            weight = (weight_in_gm / 1000)
            product_name = get_order_obj.product_details.product_id.name
            get_order_details_obj = OrderDetails.objects.filter(order_id=get_order_obj.id).first()
            if get_order_details_obj:
                f_name = ""
                l_name = ""
                if get_order_details_obj.address.personal:
                    name = get_order_details_obj.address.personal.name
                    temp_name = name.split(" ")
                    if len(temp_name) >= 2:
                        f_name = temp_name[0]
                        l_name = temp_name[-1]

                    pickup_email = get_order_details_obj.address.personal.email_id
                    pickup_phone = get_order_details_obj.address.personal.mobile_no
                pickup_customer_name = f_name
                pickup_last_name = l_name
                pickup_address = get_order_details_obj.address.address
                pickup_city = get_order_details_obj.address.city
                pickup_pincode = get_order_details_obj.address.pin_code
                pickup_state = get_order_details_obj.address.state

            sku = 'RAMDOM_SKU'
            seller_id = None
            if get_order_obj.product_details.sku:
                try:
                    import re
                    sku_data = get_order_obj.product_details.sku
                    result = re.sub("[+ /  * % , ^]", "_", sku_data)
                    sku = result.replace("\\", "_")
                    if len(sku) < 3:
                        sku = sku + "CK_SKU"
                    if len(sku) > 50:
                        sku = sku[:49]
                except Exception as e:
                    sku = 'RAMDOM_SKU'
                    print("inside exception of SKU", e)

            seller_id = get_order_obj.product_details.user.id
            sub_total = get_order_obj.total_amount
            if get_order_obj.order_details_amount:
                selling_price = get_order_obj.order_details_amount.admin_amount
            # seller_id = "301309"  # TODO change
            pickup_location = ""
            warehouse_obj = None
            pickup_location_data = get_approved_sellers_pickup_location(seller_id)
            if pickup_location_data:
                pickup_location = pickup_location_data["pickup_location"]
                warehouse_obj = pickup_location_data["warehouse_obj"]

            # Calling get pickup id of Sellers pickup location..
            pickup_location_id = get_pickup_id(pickup_location)
            seller_obj = Seller.objects.filter(user_id=seller_id).order_by('id').last()
            if seller_obj:
                shipping_customer_name = seller_obj.name
                shipping_email = seller_obj.email_id

            if warehouse_obj:
                shipping_city = warehouse_obj.city_id
                shipping_state = warehouse_obj.state_id
                shipping_pincode = warehouse_obj.pin_code
                shipping_phone = warehouse_obj.mobile_no
                shipping_address = warehouse_obj.primary_address
                shipping_address_2 = warehouse_obj.secondary_address

        data = {
            "order_id": order_id,
            "order_date": order_date,
            "channel_id": channel_id,
            "pickup_customer_name": pickup_customer_name,
            "pickup_last_name": pickup_last_name,
            "pickup_address": pickup_address,
            "pickup_address_2": pickup_address_2,
            "pickup_city": pickup_city,
            "pickup_state": pickup_state,
            "pickup_country": pickup_country,
            "pickup_pincode": pickup_pincode,
            "pickup_email": pickup_email,
            "pickup_phone": pickup_phone,
            "pickup_isd_code": pickup_isd_code,
            "pickup_location_id": pickup_location_id,
            "shipping_customer_name": shipping_customer_name,
            "shipping_last_name": shipping_last_name,
            "shipping_address": shipping_address,
            "shipping_address_2": shipping_address_2,
            "shipping_city": shipping_city,
            "shipping_country": shipping_country,
            "shipping_pincode": shipping_pincode,
            "shipping_state": shipping_state,
            "shipping_email": shipping_email,
            "shipping_isd_code": "91",
            "shipping_phone": shipping_phone,
            "order_items": [
                {
                    "sku": sku,
                    "name": product_name,
                    "units": units,
                    "selling_price": selling_price,
                    "discount": discount,
                    "hsn": hsn
                }
            ],
            "payment_method": payment_method,
            "total_discount": total_discount,
            "sub_total": sub_total,
            "length": length,
            "breadth": breadth,
            "height": height,
            "weight": weight
        }
    return data


def shiprocket_create_order_api(order_id):
    """
        create shiprocket order

    :param unique_order_id:
    :return:
    """
    order_obj = Order.objects.filter(id=order_id).first()
    get_payload = get_order_related_data(order_id)
    if get_payload:
        sr_order_obj = None
        create_sr_order = ShiprocketOrder.objects.create(order=order_obj, status="initiated", order_mode="Delivery")

        url = 'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc'
        method = 'POST'
        resp = call_shiprocket_api(url, get_payload, method)
        """
        sample success response:    
            {
              "order_id": 16161616,
              "shipment_id": 15151515,
              "status": "NEW",
              "status_code": 1,
              "onboarding_completed_now": 0,
              "awb_code": null,
              "courier_company_id": null,
              "courier_name": null
            }
    
        sample missing field data
            {
              "message": "Oops! Invalid Data.",
              "errors": {
                "order_id": [
                  "The order id field is required."
                ]
              },
              "status_code": 422
            }
    
        sample invalid data
            {
              "message": "Given channel id does not exist",
              "status_code": 400
            }
    
        """
        return_data = None
        if resp:
            create_order = False
            api_name = "create_order"
            type = "Delivery"
            status_code = resp['status_code']
            if status_code == 1:
                order_id = resp['order_id']
                shipment_id = resp['shipment_id']
                status = resp['status']
                onboarding_completed_now = resp['onboarding_completed_now']

                get_sr_order = ShiprocketOrder.objects.filter(id=create_sr_order.id).order_by('id').last()
                if get_sr_order:
                    sr_order_obj = get_sr_order

                    shiprocket_obj = ShiprocketOrder.objects.filter(id=create_sr_order.id)
                    if shiprocket_obj:
                        shiprocket_obj[0].shiprocket_order_id = order_id
                        shiprocket_obj[0].shipment_id = shipment_id
                        shiprocket_obj[0].sr_status = status
                        shiprocket_obj[0].status_code = status_code
                        shiprocket_obj[0].onboarding_completed_now = onboarding_completed_now
                        shiprocket_obj[0].response = resp
                        shiprocket_obj[0].status = "success"
                        shiprocket_obj[0].order = order_obj
                        shiprocket_obj[0].save()

                # Adding API status in ShiprocketApiStatus Model..
                create_order = True
                try:
                    add_status = shiprocket_api_response_save(order_obj.id, api_name, create_order, resp, type)
                except Exception as e:
                    print("inside Exception of shiprocket_api_response_save of create order", e)

                return_data = {
                    "shipment_id": shipment_id,
                    "sr_order_obj": sr_order_obj
                }

                return return_data

            elif status_code == 3 or status_code == 4:
                '''
                {"order_id":58910405,"shipment_id":58569907,"status":"READY TO SHIP","status_code":3,
                "onboarding_completed_now":0,"awb_code":"109169863300","courier_company_id":10,
                "courier_name":"Delhivery Air"}
                '''
                order_id = None
                shipment_id = None
                status = None
                awb_code = None
                courier_name = None
                courier_company_id = None
                onboarding_completed_now = None
                if "order_id" in resp:
                    order_id = resp['order_id']
                if "shipment_id" in resp:
                    shipment_id = resp['shipment_id']
                if "status" in resp:
                    status = resp['status']
                if "awb_code" in resp:
                    awb_code = resp['awb_code']
                if "courier_company_id" in resp:
                    courier_company_id = resp['courier_company_id']
                if "courier_name" in resp:
                    courier_name = resp['courier_name']

                get_sr_order = ShiprocketOrder.objects.filter(id=create_sr_order.id).first()
                if get_sr_order:
                    sr_order_obj = get_sr_order

                    shiprocket_obj = ShiprocketOrder.objects.filter(id=create_sr_order.id)
                    if shiprocket_obj:
                        shiprocket_obj[0].shiprocket_order_id = order_id
                        shiprocket_obj[0].shipment_id = shipment_id
                        shiprocket_obj[0].sr_status = status
                        shiprocket_obj[0].status_code = status_code
                        shiprocket_obj[0].onboarding_completed_now = onboarding_completed_now
                        shiprocket_obj[0].response = resp
                        shiprocket_obj[0].status = "success"
                        shiprocket_obj[0].order = order_obj
                        shiprocket_obj[0].courier_company_id = courier_company_id
                        shiprocket_obj[0].courier_name = courier_name
                        shiprocket_obj[0].awb_code = awb_code
                        shiprocket_obj[0].save()

                # Adding API status in ShiprocketApiStatus Model..
                create_order = True
                try:
                    # Adding status for create order..
                    add_status = shiprocket_api_response_save(order_obj.id, api_name, create_order, resp, type)

                    # Adding status for awb generate response..
                    api_name = "awb_generate"
                    add_status = shiprocket_api_response_save(order_obj.id, api_name, create_order, resp, type)
                except Exception as e:
                    print("inside Exception of shiprocket_api_response_save of create order", e)

                return_data = {
                    "shipment_id": shipment_id,
                    "sr_order_obj": sr_order_obj
                }

                return return_data

            elif status_code == 422:
                create_order = False
                # missing field
                shiprocket_obj = ShiprocketOrder.objects.filter(id=create_sr_order.id)
                if shiprocket_obj:
                    shiprocket_obj[0].status_code = status_code
                    shiprocket_obj[0].status = "error"
                    shiprocket_obj[0].response = resp
                    shiprocket_obj[0].save()
                pass

            elif status_code == 400:
                create_order = False
                # invalid data

                shiprocket_obj = ShiprocketOrder.objects.filter(id=create_sr_order.id)
                if shiprocket_obj:
                    shiprocket_obj[0].status_code = status_code
                    shiprocket_obj[0].status = "error"
                    shiprocket_obj[0].response = resp
                    shiprocket_obj[0].save()
                pass
            else:
                create_order = False
                pass

            # Adding API status in ShiprocketApiStatus Model..
            try:
                add_status = shiprocket_api_response_save(order_obj.id, api_name, create_order, resp, type)
            except Exception as e:
                print("inside Exception of shiprocket_api_response_save of create order", e)

        return return_data


def shiprocket_create_order_details_api(order_details_id):
    """
        create shiprocket order

    :param unique_order_id:
    :return:
    """
    order_details_obj = OrderDetails.objects.filter(id=order_details_id).first()
    get_payload = get_order_details_related_data(order_details_id)
    if get_payload:
        sr_order_obj = None
        create_sr_order = ShiprocketOrder.objects.create(order_details=order_details_obj, status="initiated",
                                                         order_mode="Delivery")

        url = 'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc'
        method = 'POST'
        resp = call_shiprocket_api(url, get_payload, method)
        """
        sample success response:    
            {
              "order_id": 16161616,
              "shipment_id": 15151515,
              "status": "NEW",
              "status_code": 1,
              "onboarding_completed_now": 0,
              "awb_code": null,
              "courier_company_id": null,
              "courier_name": null
            }

        sample missing field data
            {
              "message": "Oops! Invalid Data.",
              "errors": {
                "order_id": [
                  "The order id field is required."
                ]
              },
              "status_code": 422
            }

        sample invalid data
            {
              "message": "Given channel id does not exist",
              "status_code": 400
            }

        """
        return_data = None
        if resp:
            create_order = False
            api_name = "create_order"
            type = "Delivery"
            status_code = resp['status_code']
            if status_code == 1:
                order_id = resp['order_id']
                shipment_id = resp['shipment_id']
                status = resp['status']
                onboarding_completed_now = resp['onboarding_completed_now']

                get_sr_order = ShiprocketOrder.objects.filter(id=create_sr_order.id).order_by('id').last()
                if get_sr_order:
                    sr_order_obj = get_sr_order
                    shiprocket_obj = ShiprocketOrder.objects.filter(id=create_sr_order.id)
                    if shiprocket_obj:
                        shiprocket_obj[0].shiprocket_order_id = order_id
                        shiprocket_obj[0].shipment_id = shipment_id
                        shiprocket_obj[0].sr_status = status
                        shiprocket_obj[0].status_code = status_code
                        shiprocket_obj[0].onboarding_completed_now = onboarding_completed_now
                        shiprocket_obj[0].response = resp
                        shiprocket_obj[0].status = "success"
                        shiprocket_obj[0].order_details = order_details_obj
                        shiprocket_obj[0].save()
                # Adding API status in ShiprocketApiStatus Model..
                create_order = True
                try:
                    add_status = shiprocket_order_details_api_response_save(order_details_obj.id, api_name,
                                                                            create_order, resp, type)
                except Exception as e:
                    print("inside Exception of shiprocket_api_response_save of create order", e)

                return_data = {
                    "shipment_id": shipment_id,
                    "sr_order_obj": sr_order_obj
                }

                return return_data

            elif status_code == 3 or status_code == 4:
                '''
                {"order_id":58910405,"shipment_id":58569907,"status":"READY TO SHIP","status_code":3,
                "onboarding_completed_now":0,"awb_code":"109169863300","courier_company_id":10,
                "courier_name":"Delhivery Air"}
                '''
                order_id = None
                shipment_id = None
                status = None
                awb_code = None
                courier_name = None
                courier_company_id = None
                onboarding_completed_now = None
                if "order_id" in resp:
                    order_id = resp['order_id']
                if "shipment_id" in resp:
                    shipment_id = resp['shipment_id']
                if "status" in resp:
                    status = resp['status']
                if "awb_code" in resp:
                    awb_code = resp['awb_code']
                if "courier_company_id" in resp:
                    courier_company_id = resp['courier_company_id']
                if "courier_name" in resp:
                    courier_name = resp['courier_name']

                get_sr_order = ShiprocketOrder.objects.filter(id=create_sr_order.id).first()
                if get_sr_order:
                    sr_order_obj = get_sr_order
                    shiprocket_obj = ShiprocketOrder.objects.filter(id=create_sr_order.id)
                    if shiprocket_obj:
                        shiprocket_obj[0].shiprocket_order_id = order_id
                        shiprocket_obj[0].shipment_id = shipment_id
                        shiprocket_obj[0].sr_status = status
                        shiprocket_obj[0].status_code = status_code
                        shiprocket_obj[0].onboarding_completed_now = onboarding_completed_now,
                        shiprocket_obj[0].response = resp
                        shiprocket_obj[0].status = "success"
                        shiprocket_obj[0].order_details = order_details_obj
                        shiprocket_obj[0].courier_company_id = courier_company_id
                        shiprocket_obj[0].courier_name = courier_name
                        shiprocket_obj[0].awb_code = awb_code
                        shiprocket_obj[0].save()

                # Adding API status in ShiprocketApiStatus Model..
                create_order = True
                try:
                    # Adding status for create order..
                    add_status = shiprocket_order_details_api_response_save(order_details_obj.id, api_name,
                                                                            create_order, resp, type)

                    # Adding status for awb generate response..
                    api_name = "awb_generate"
                    add_status = shiprocket_order_details_api_response_save(order_details_obj.id, api_name,
                                                                            create_order, resp, type)
                except Exception as e:
                    print("inside Exception of shiprocket_api_response_save of create order", e)

                return_data = {
                    "shipment_id": shipment_id,
                    "sr_order_obj": sr_order_obj
                }

                return return_data

            elif status_code == 422:
                create_order = False
                # missing field

                shiprocket_obj = ShiprocketOrder.objects.filter(id=create_sr_order.id)
                if shiprocket_obj:
                    shiprocket_obj[0].status_code = status_code
                    shiprocket_obj[0].status = "error"
                    shiprocket_obj[0].response = resp
                    shiprocket_obj[0].save()

                pass

            elif status_code == 400:
                create_order = False
                # invalid data

                shiprocket_obj = ShiprocketOrder.objects.filter(id=create_sr_order.id)
                if shiprocket_obj:
                    shiprocket_obj[0].status_code = status_code
                    shiprocket_obj[0].status = "error"
                    shiprocket_obj[0].response = resp
                    shiprocket_obj[0].save()
                pass
            else:
                create_order = False
                pass

            # Adding API status in ShiprocketApiStatus Model..
            try:
                add_status = shiprocket_order_details_api_response_save(order_details_obj.id, api_name, create_order,
                                                                        resp, type)
            except Exception as e:
                print("inside Exception of shiprocket_api_response_save of create order", e)

        return return_data


def shiprocket_api_response_save(order_id, api_name, status, response, order_type):
    """
    This function is to save API response of Shiprocket.
    """
    if None not in (order_id, api_name):
        shiprocket_api_info_obj = ShiprocketApiInfo.objects.filter(order=order_id, type=order_type).order_by(
            'id').last()
        if shiprocket_api_info_obj:
            response_dict = {}
            if shiprocket_api_info_obj.response:
                response_text = shiprocket_api_info_obj.response
                if response_text:
                    response_dict = eval(response_text)
            response_dict.update({api_name: response})

            if api_name == "create_order":
                update_data = ShiprocketApiInfo.objects.filter(id=shiprocket_api_info_obj.id)
                if update_data:
                    update_data[0].create_order = status
                    update_data[0].response = response_dict
                    update_data[0].updated_date = datetime.datetime.now()
                    update_data[0].save()

            if api_name == "invoice_generate":

                update_data = ShiprocketApiInfo.objects.filter(id=shiprocket_api_info_obj.id)
                if update_data:
                    update_data[0].invoice_generate = status
                    update_data[0].response = response_dict
                    update_data[0].updated_date = datetime.datetime.now()
                    update_data[0].save()

            if api_name == "awb_generate":

                update_data = ShiprocketApiInfo.objects.filter(id=shiprocket_api_info_obj.id)
                if update_data:
                    update_data[0].awb_generate = status
                    update_data[0].response = response_dict
                    update_data[0].updated_date = datetime.datetime.now()
                    update_data[0].save()

            if api_name == "manifest_generate":

                update_data = ShiprocketApiInfo.objects.filter(id=shiprocket_api_info_obj.id)
                if update_data:
                    update_data[0].manifest_generate = status
                    update_data[0].response = response_dict
                    update_data[0].save()

            if api_name == "pickup_schedule":

                update_data = ShiprocketApiInfo.objects.filter(id=shiprocket_api_info_obj.id)
                if update_data:
                    update_data[0].pickup_schedule = status
                    update_data[0].response = response_dict
                    update_data[0].updated_date = datetime.datetime.now()
                    update_data[0].save()

            if api_name == "label_generate":

                update_data = ShiprocketApiInfo.objects.filter(id=shiprocket_api_info_obj.id)
                if update_data:
                    update_data[0].label_generate = status
                    update_data[0].response = response_dict
                    update_data[0].updated_date = datetime.datetime.now()
                    update_data[0].save()

    return True


def shiprocket_order_details_api_response_save(order_details_id, api_name, status, response, order_type):
    """
    params: order_details_id, api_name, status,response,order_type
    This function is to save API response of Shiprocket.
    """
    if None not in (order_details_id, api_name):
        shiprocket_api_info_obj = ShiprocketApiInfo.objects.filter(order_details=order_details_id,
                                                                   type=order_type).order_by('id').last()

        if shiprocket_api_info_obj:
            response_dict = {}
            if shiprocket_api_info_obj.response:
                response_text = shiprocket_api_info_obj.response
                if response_text:
                    response_dict = eval(response_text)
            response_dict.update({api_name: response})

            if api_name == "create_order":

                update_data = ShiprocketApiInfo.objects.filter(id=shiprocket_api_info_obj.id)
                if update_data:
                    update_data[0].create_order = status
                    update_data[0].response = response_dict
                    update_data[0].updated_date = datetime.datetime.now()
                    update_data[0].save()

            if api_name == "invoice_generate":

                update_data = ShiprocketApiInfo.objects.filter(id=shiprocket_api_info_obj.id)
                if update_data:
                    update_data[0].invoice_generate = status
                    update_data[0].response = response_dict
                    update_data[0].updated_date = datetime.datetime.now()
                    update_data[0].save()

            if api_name == "awb_generate":

                update_data = ShiprocketApiInfo.objects.filter(id=shiprocket_api_info_obj.id)
                if update_data:
                    update_data[0].awb_generate = status
                    update_data[0].response = response_dict
                    update_data[0].updated_date = datetime.datetime.now()
                    update_data[0].save()

            if api_name == "manifest_generate":

                update_data = ShiprocketApiInfo.objects.filter(id=shiprocket_api_info_obj.id)
                if update_data:
                    update_data[0].manifest_generate = status
                    update_data[0].response = response_dict
                    update_data[0].save()

            if api_name == "pickup_schedule":

                update_data = ShiprocketApiInfo.objects.filter(id=shiprocket_api_info_obj.id)
                if update_data:
                    update_data[0].pickup_schedule = status
                    update_data[0].response = response_dict
                    update_data[0].updated_date = datetime.datetime.now()
                    update_data[0].save()

            if api_name == "label_generate":

                update_data = ShiprocketApiInfo.objects.filter(id=shiprocket_api_info_obj.id)
                if update_data:
                    update_data[0].label_generate = status
                    update_data[0].response = response_dict
                    update_data[0].updated_date = datetime.datetime.now()
                    update_data[0].save()

    return True


def generate_return_order(order_id):
    """
    This is the main function where return process executes..
    """
    status = None
    # Creating return Order..
    order_obj = Order.objects.filter(id=order_id).first()
    # Create ShiprocketApiInfo objects for order..
    shiprocket_api_info_obj = ShiprocketApiInfo.objects.create(order=order_obj, type="Reverse")
    data = shiprocket_create_return_order_api(order_id)
    if data:
        # This means shiprocket return order successfully created..
        # Creating awb for return order shipment id..
        if "shipment_id" in data:
            shipment_id = data["shipment_id"]
            # Assign awb
            # Getting courior if from get_order_courior_id function..
            is_return = "Yes"
            courier_id = get_orders_pickup_courier_id(order_id, is_return)
            if None not in (shipment_id, courier_id):
                generate_awb = generate_shipment_awb(shipment_id, courier_id, is_return)

            get_sr_api_info_obj = ShiprocketApiInfo.objects.filter(id=shiprocket_api_info_obj.id).first()
            if get_sr_api_info_obj.create_order and get_sr_api_info_obj.awb_generate:
                status = "1"
            else:
                status = "2"

            return status

    else:
        return status

    return status


def shiprocket_create_return_order_api(order_id):
    """
        create shiprocket order

    :param unique_order_id:
    :return:
    """
    payload = {}
    sr_order_obj = None
    get_order_obj = Order.objects.filter(id=order_id)
    if get_order_obj:
        payload = get_return_order_related_data(get_order_obj[0].id)
        if payload:
            create_sr_order = ShiprocketOrder.objects.create(order=get_order_obj[0], order_mode="Reverse",
                                                             status="initiated")

            url = 'https://apiv2.shiprocket.in/v1/external/orders/create/return'
            method = 'POST'
            resp = call_shiprocket_api(url, payload, method)
            """
            sample success response:    
            {
              "order_id": 16525854,
              "shipment_id": 16370682,
              "status": "RETURN PENDING",
              "status_code": 21
            }
            sample missing field data
                {
                  "message": "Oops! Invalid Data.",
                  "errors": {
                    "order_id": [
                      "The order id field is required."
                    ]
                  },
                  "status_code": 422
                }
        
            sample invalid data
                {
                  "message": "Given channel id does not exist",
                  "status_code": 400
                }
        
            """
            return_data = None
            status_code = None
            order_id = None
            shipment_id = None
            status = None
            awb_code = None
            courier_name = None
            courier_company_id = None
            onboarding_completed_now = None

            if resp:
                create_order = False
                api_name = "create_order"
                order_type = "Reverse"
                if "status_code" in resp:
                    status_code = resp['status_code']
                    if status_code == 21:
                        create_order = True
                        order_id = resp['order_id']
                        shipment_id = resp['shipment_id']
                        status = resp['status']

                        get_sr_order = ShiprocketOrder.objects.filter(id=create_sr_order.id).first()
                        if get_sr_order:
                            sr_order_obj = get_sr_order

                            shiprocket_obj = ShiprocketOrder.objects.filter(id=create_sr_order.id)
                            if shiprocket_obj:
                                shiprocket_obj[0].shiprocket_order_id = order_id
                                shiprocket_obj[0].shipment_id = shipment_id
                                shiprocket_obj[0].sr_status = status
                                shiprocket_obj[0].status_code = status_code
                                shiprocket_obj[0].response = resp
                                shiprocket_obj[0].status = "success"
                                shiprocket_obj[0].updated_date = datetime.datetime.now()
                                shiprocket_obj[0].save()

                            # Adding API response in ShiprocketApiInfo model..
                            try:
                                save_response = shiprocket_api_response_save(get_order_obj[0].id, api_name,
                                                                             create_order,
                                                                             resp, order_type)

                            except Exception as e:
                                print("Inside Exception of shiprocket_api_response_save for awb_generate", e)

                        return_data = {
                            "shipment_id": shipment_id,
                            "sr_order_obj": sr_order_obj
                        }

                        return return_data

                if "errors" in resp:
                    status_code = None
                    if "error_code" in resp:
                        create_order = False
                        status_code = resp["error_code"]
                    get_sr_order = ShiprocketOrder.objects.filter(id=create_sr_order.id).first()
                    if get_sr_order:
                        shiprocket_obj = ShiprocketOrder.objects.filter(id=create_sr_order.id)
                        if shiprocket_obj:
                            shiprocket_obj[0].status = "error"
                            shiprocket_obj[0].status_code = status_code
                            shiprocket_obj[0].response = resp
                            shiprocket_obj[0].save()

                        # Adding API response in ShiprocketApiInfo model..
                        try:
                            print("hereeeeeeeeeeeeeee444444444444444444444eeeeeeeeeeeeeeeeee", get_order_obj[0].id,
                                  api_name, create_order, resp,
                                  order_type)
                            save_response = shiprocket_api_response_save(get_order_obj[0].id, api_name, create_order,
                                                                         resp, order_type)

                        except Exception as e:
                            print("Inside Exception of shiprocket_api_response_save for awb_generate", e)
            return return_data


def get_seller_warehouse_pincode(seller_id):
    """
    params: seller_id
    This function return pickup postcode from  seller warehouse last entry of seller id.
    """
    pickup_postcode = None
    warehouse_obj = SellerWarehouse.objects.filter(seller_id=seller_id, type="shiprocket",
                                                   approval_status__icontains="approved",
                                                   adding_status=True).order_by('id').last()
    if warehouse_obj:
        pickup_postcode = warehouse_obj.pin_code

    return pickup_postcode


def get_orders_pickup_courier_id(order_id, is_return):
    """
    params: order_id,is_return
    This function gets all required data for is service available function
    and returns courier id for that order..
    """
    pickup_postcode = None
    delivery_postcode = None
    weight = ""
    cod = "0"
    declared_value = 0
    courier_id = None
    get_order_obj = Order.objects.filter(id=order_id).first()
    if get_order_obj:
        seller_id = get_order_obj.product_details.user.id
        weight_in_gm = get_order_obj.product_details.weight
        weight = (weight_in_gm / 1000)
        pickup_postcode = get_seller_warehouse_pincode(seller_id)

        get_order_details_obj = OrderDetails.objects.filter(order_id=get_order_obj).first()
        if get_order_details_obj:
            if get_order_details_obj.address:
                delivery_postcode = get_order_details_obj.address.pin_code

    if None not in (delivery_postcode, pickup_postcode):
        # check serviceability
        serviceability_data = is_service_available(pickup_postcode, delivery_postcode, weight, cod, declared_value,
                                                   is_return)
        if serviceability_data is not None:
            service_available = False
            if "service_available" in serviceability_data:
                service_available = serviceability_data["service_available"]
            if service_available:
                courier_id = serviceability_data["pickup_company_id"]

    return courier_id


def check_return_order_delivery_availability(product_details_obj, pickup_pincode):
    if None not in (product_details_obj, pickup_pincode):
        seller_id = product_details_obj.user.id
        weight_in_gm = product_details_obj.weight
        weight = (weight_in_gm / 1000)
        delivery_postcode = get_seller_warehouse_pincode(seller_id)
        is_return = "Yes"
        cod = "0"
        declared_value = 0
        courier_id = None

        if None not in (delivery_postcode, pickup_pincode):
            # check serviceability
            serviceability_data = is_service_available(pickup_pincode, delivery_postcode, weight, cod, declared_value,
                                                       is_return)
            if serviceability_data is not None:
                service_available = False
                if "service_available" in serviceability_data:
                    service_available = serviceability_data["service_available"]
                if service_available:
                    courier_id = serviceability_data["pickup_company_id"]

        print("return courier_id", courier_id)
        return courier_id


def get_order_details_pickup_courier_id(order_details_id, is_return):
    """
    params:order_details_id,is_return
    This function gets all required data for is service available function
    and returns courier id for that order.
    """
    pickup_postcode = None
    delivery_postcode = None
    weight = ""
    cod = "0"
    total_weight = 0
    declared_value = 0
    courier_id = None
    seller_id = None
    user_obj = User.objects.filter(username="MFPL").first()
    if user_obj:
        seller_id = user_obj.id
    pickup_postcode = get_seller_warehouse_pincode(seller_id)
    get_order_details_obj = OrderDetails.objects.filter(id=order_details_id).first()
    if get_order_details_obj:
        get_combined_orders = get_order_details_obj.combine_order.filter(status="placed").all()
        if get_combined_orders:
            for order in get_combined_orders:
                weight_in_gm = order.product_details.weight
                total_weight += weight_in_gm
        weight = (total_weight / 1000)
        pickup_postcode = get_seller_warehouse_pincode(seller_id)
        delivery_postcode = get_order_details_obj.address.pin_code

    if None not in (delivery_postcode, pickup_postcode):
        # check serviceability
        serviceability_data = is_service_available(pickup_postcode, delivery_postcode, weight, cod, declared_value,
                                                   is_return)
        if serviceability_data is not None:
            service_available = False
            if "service_available" in serviceability_data:
                service_available = serviceability_data["service_available"]
            if service_available:
                courier_id = serviceability_data["pickup_company_id"]

    return courier_id


def create_order(order_id):
    """
        checks serviceability -> creates order -> assigns awb to order -> schedules pickup
    :param unique_order_id:
    :return:
    """
    if order_id is not None:
        status = None
        order_obj = Order.objects.filter(id=order_id).first()
        # Create ShiprocketApiInfo objects for order..
        shiprocket_api_info_obj = ShiprocketApiInfo.objects.create(order=order_obj, type="Delivery")
        # Create Shiprocket Order
        data = shiprocket_create_order_api(order_id)
        if data and data is not None:
            shipment_id = data['shipment_id']
            shiprocket_order_obj = data['sr_order_obj']
            # Assign awb
            # Getting courier if from get_order_courior_id function..
            is_return = "No"
            courier_id = get_orders_pickup_courier_id(order_id, is_return)
            if courier_id:
                generate_awb = generate_shipment_awb(shipment_id, courier_id, is_return)

                # Request for Shipment Pickup
                request_shipment_pickup_api(shiprocket_order_obj)

                get_sr_api_info_obj = ShiprocketApiInfo.objects.filter(id=shiprocket_api_info_obj.id).first()
                if get_sr_api_info_obj.create_order and get_sr_api_info_obj.awb_generate:
                    status = "1"
                else:
                    status = "2"

        return status


def mfpl_create_order(order_details_id):
    """
        checks serviceability -> creates order -> assigns awb to order -> schedules pickup
    :param unique_order_id:
    :return:
    """
    if order_details_id is not None:
        status = None
        order_details_obj = OrderDetails.objects.filter(id=order_details_id).first()
        # Create ShiprocketApiInfo objects for order..
        shiprocket_api_info_obj = ShiprocketApiInfo.objects.create(order_details=order_details_obj, type="Delivery")
        # Create Shiprocket Order
        data = shiprocket_create_order_details_api(order_details_id)
        if data and data is not None:
            shipment_id = data['shipment_id']
            shiprocket_order_obj = data['sr_order_obj']
            # Assign awb
            # Getting courier if from get_order_courier_id function..
            is_return = "No"
            courier_id = get_order_details_pickup_courier_id(order_details_id, is_return)
            if courier_id:
                generate_awb = generate_order_details_shipment_awb(shipment_id, courier_id, is_return)

                # Request for Shipment Pickup
                request_order_details_shipment_pickup_api(shiprocket_order_obj)

                get_sr_api_info_obj = ShiprocketApiInfo.objects.filter(id=shiprocket_api_info_obj.id).first()
                if get_sr_api_info_obj.create_order and get_sr_api_info_obj.awb_generate:
                    status = "1"
                else:
                    status = "2"

        return status


def get_all_pickup_location():
    """
    This function get response from call_shiprocket_api by passing url and method.
    """
    url = "https://apiv2.shiprocket.in/v1/external/settings/company/pickup"
    method = "GET"
    resp = call_shiprocket_api(url, {}, method)
    data = resp['data']
    shipping_address = data["shipping_address"]
    for i in shipping_address:
        print("", i)


def get_pickup_id(pickup_name):
    """
    params: pickup_name
    This function contains get all pickup locations API and it returns particular pickup locations pickup id..
    """
    url = "https://apiv2.shiprocket.in/v1/external/settings/company/pickup"
    method = "GET"
    payload = {}

    return_data = call_shiprocket_api(url, payload, method)
    pickup_id = None
    if return_data:
        if pickup_name is not None:
            if "data" in return_data:
                if "shipping_address" in return_data["data"]:
                    location_list = return_data["data"]["shipping_address"]
                    for location in location_list:
                        if pickup_name in location["pickup_location"]:
                            name = location["pickup_location"]
                            if name == pickup_name:
                                pickup_id = location["id"]

    return pickup_id


def generate_label_api(shipment_id):
    """
    params: shipment_id
    This function get shiprocket order last entry by shipment_id from that it get label_url.
    It get response from call_shiprocket_api function.
    """
    label_url = ""
    # Checking manifest already generated or not..
    get_shiprocket_object = ShiprocketOrder.objects.filter(shipment_id=shipment_id, status="success",
                                                           order_mode="Delivery").order_by('id').last()
    if get_shiprocket_object:
        get_label_object = ShiprocketLabel.objects.filter(sr_order__shipment_id=shipment_id,
                                                          status="success").order_by('id').last()
        if get_label_object:
            label_url = get_label_object.label_url
        else:
            create_label = ShiprocketLabel.objects.create(sr_order=get_shiprocket_object, status="initiated")
            url = "https://apiv2.shiprocket.in/v1/external/courier/generate/label"
            method = "POST"
            payload = {
                "shipment_id": [shipment_id]
            }
            resp = call_shiprocket_api(url, payload, method)
            """
            success: resp
                {
                  "label_created": 1,
                  "label_url": "https://kr-shipmultichannel.s3.ap-southeast-1.amazonaws.com/25149/labels/shipping-label-16104408-788830567028.pdf",
                  "response": "Label has been created and uploaded successfully!",
                  "not_created": []
                }
            """
            if "label_url" in resp:
                label_url = resp['label_url']
                if label_url != "":
                    get_label_obj = ShiprocketLabel.objects.filter(id=create_label.id)
                    if get_label_obj:
                        get_label_obj[0].label_url = label_url
                        get_label_obj[0].status = "success"
                        get_label_obj[0].response = resp
                        get_label_obj[0].save()

                else:
                    ShiprocketLabel.objects.filter(id=create_label.id, status="error", response=resp)
            else:
                ShiprocketLabel.objects.filter(id=create_label.id, status="error", response=resp)

    return label_url


def generate_shiprocket_manifest_api(shipment_id):
    """
    params: shipment_id
    This function generates manifest of Shipment and returns manifest url..
    """
    manifest_url = ""
    # Checking manifest already generated or not..
    get_shiprocket_object = ShiprocketOrder.objects.filter(shipment_id=shipment_id, status="success",
                                                           order_mode="Delivery").order_by('id').last()
    if get_shiprocket_object:
        manifest_obj = ShiprocketManifest.objects.filter(sr_order__shipment_id=shipment_id, status="success").order_by(
            'id').last()
        if manifest_obj:
            manifest_url = manifest_obj.manifest_url

        else:
            create_shiprocket = ShiprocketManifest.objects.create(sr_order=get_shiprocket_object, status="initiated")

            url = "https://apiv2.shiprocket.in/v1/external/manifests/generate"
            method = "POST"
            payload = {
                "shipment_id": [shipment_id]
            }
            # Calling Generate Manifest API..
            manifest_url = ""
            return_data = call_shiprocket_api(url, payload, method)
            if return_data:

                if "manifest_url" in return_data:
                    manifest_url = return_data["manifest_url"]
                    if manifest_url and manifest_url != "":

                        shiprocket_obj = ShiprocketManifest.objects.filter(id=create_shiprocket.id)
                        if shiprocket_obj:
                            shiprocket_obj[0].manifest_url = manifest_url
                            shiprocket_obj[0].status = "success"
                            shiprocket_obj[0].response = return_data
                            shiprocket_obj[0].save()

                else:

                    shiprocket_obj = ShiprocketManifest.objects.filter(id=create_shiprocket.id)
                    if shiprocket_obj:
                        shiprocket_obj[0].status = "error"
                        shiprocket_obj[0].response = return_data
                        shiprocket_obj[0].save()

    return manifest_url


def print_shiprocket_manifest_api(shipment_id):
    """
    params: shipment_id
    This function generates manifest of Shipment and returns manifest url..
    """
    url = "https://apiv2.shiprocket.in/v1/external/manifests/print"
    method = "POST"
    payload = {
        "order_ids": [shipment_id]
    }
    # Calling Generate Manifest API..
    manifest_url = ""
    return_data = call_shiprocket_api(url, payload, method)
    if return_data:
        if "manifest_url" in return_data:
            manifest_url = return_data["manifest_url"]
    return manifest_url


@csrf_exempt
def print_shiprocket_manifest_api_req(request):
    """
    params: unique_order_id
    This function print shiprocket manifest by using print_shiprocket_manifest_api.
    """
    url = ""
    unique_order_id = request.POST['unique_order_id']
    if unique_order_id:
        shiprocket_order = ShiprocketOrder.objects.filter(order__unique_order_id=unique_order_id,
                                                          status="success").first()
        if shiprocket_order:
            shipment_id = shiprocket_order.shipment_id
            url = print_shiprocket_manifest_api(shipment_id)
    return_data = {"url": url}
    return HttpResponse(json.dumps(return_data))


@csrf_exempt
def generate_label_api_req(request):
    """
    params: unique_order_id
    This function generate shipment  by using generate_label_api by passing shipment id to the
    shirocketorder first entry.
    """
    url = ""
    unique_order_id = request.POST['unique_order_id']
    if unique_order_id:
        shiprocket_order = ShiprocketOrder.objects.filter(order__unique_order_id=unique_order_id,
                                                          status="success").first()
        if shiprocket_order:
            shipment_id = shiprocket_order.shipment_id
            url = generate_label_api(shipment_id)
    return_data = {"url": url}
    return HttpResponse(json.dumps(return_data))


@csrf_exempt
def print_shiprocket_order_details_manifest_api_req(request):
    """
    params: order_details_id
    This function  return shirocket url from shiprocketorder first entry  by order details id.
    """
    url = ""
    order_details_id = request.POST['order_details_id']
    if order_details_id:
        shiprocket_order = ShiprocketOrder.objects.filter(order_details=order_details_id,
                                                          status="success").first()
        if shiprocket_order:
            shipment_id = shiprocket_order.shipment_id
            url = generate_shiprocket_manifest_api(shipment_id)
    return_data = {"url": url}
    return HttpResponse(json.dumps(return_data))


@csrf_exempt
def generate_order_details_label_api_req(request):
    """
       params: order_details_id
       This function return shirocket url from shiprocketorder first entry  by order details id.
    """
    url = ""
    order_details_id = request.POST['order_details_id']
    if order_details_id:
        shiprocket_order = ShiprocketOrder.objects.filter(order_details=order_details_id,
                                                          status="success").first()
        if shiprocket_order:
            shipment_id = shiprocket_order.shipment_id
            url = generate_label_api(shipment_id)
    return_data = {"url": url}
    return HttpResponse(json.dumps(return_data))


def generate_invoice_api(order_id):
    """
       params: order_id
       This function return invoice url from call_shiprocket_api by passing url, payload ,method.
    """

    url = "https://apiv2.shiprocket.in/v1/external/orders/print/invoice"
    method = "POST"
    payload = {"ids": [order_id]}
    resp = call_shiprocket_api(url, payload, method)
    """
    success: resp
    {
      "is_invoice_created": true,
      "invoice_url": "https://s3-ap-southeast-1.amazonaws.com/kr-shipmultichannel/25149/invoices/KD101019281564656872.pdf",
      "not_created": []
    }
    """
    invoice_url = None
    if "invoice_url" in resp:
        invoice_url = resp['invoice_url']
    return invoice_url


class ShiprocketIsDeliveryAvailableAPI(generics.ListAPIView):
    """
        To validate pin-code

        :parameter:

            Parameter

            {code live and testing
                "pincode": "CharField",                    # 422401
            }

            Response

            {
                "status": "CharField",                      # 1
                "message": "CharField", 1                   # Valid pincode
            }

            Note

            Guest token required

            "guest-token-pincode" : "dgjkvcllvdrfpjfogxsomifrihmwlxqv"

            status 1 : Valid pin code
            status 2 : Invalid pin code ( not present in pin code master)
            status 10 : Something went wrong (Django Error)
            status 11 : Enter valid data

        """

    def post(self, request, *args, **kwargs):

        weight = ""
        cod = "0"
        is_return = "0"
        declared_value = 0
        available_list = []
        not_available_list = []
        is_all_available = False
        delivery_postcode = request.data.get('pincode')
        product_details_id_list = request.data.get('product_details_id_list')

        if product_details_id_list is not None:
            for product_details_id in product_details_id_list:
                pickup_postcode = None
                category = ""
                get_product_details_obj = ProductDetails.objects.filter(id=product_details_id).first()
                if get_product_details_obj:
                    category = get_product_details_obj.product_id.base_category.sub_category.category.name
                    seller_id = get_product_details_obj.user.id
                    pickup_postcode = get_seller_warehouse_pincode(seller_id)
                    weight_in_gm = get_product_details_obj.weight
                    weight = str((weight_in_gm / 1000))

                if None not in (delivery_postcode, pickup_postcode):
                    # check serviceability
                    serviceability_data = is_service_available(pickup_postcode, delivery_postcode, weight, cod,
                                                               declared_value,
                                                               is_return)
                    if serviceability_data is not None:
                        if "service_available" in serviceability_data:
                            is_delivery_available = serviceability_data["service_available"]
                            if is_delivery_available:
                                available_list.append(product_details_id)
                                # This means delivery is available for this product..
                                # if category != "Grocery":
                                #     is_returnable = check_return_order_delivery_availability(get_product_details_obj,
                                #                                                              delivery_postcode)
                                #     if is_returnable:
                                #         available_list.append(product_details_id)
                                #
                                #     else:
                                #         is_all_available = False
                                #         not_available_list.append(product_details_id)
                                #
                                # else:
                                #     available_list.append(product_details_id)

                            else:
                                is_all_available = False
                                not_available_list.append(product_details_id)

                else:
                    is_all_available = False
                    not_available_list.append(product_details_id)

        # Checking all products delivery available or not..
        if len(product_details_id_list) == len(available_list):
            is_all_available = True
        return Response(
            {
                "available_list": available_list,
                "not_available_list": not_available_list,
                "is_all_available": is_all_available
            }
        )


def shiprocket_cancel_order_api(order_id):
    """
    parameter: order_id
    This function will cancel Shiprocket Order and returns amount and credit to users wallet..
    """
    sr_order_id = None
    get_sr_order_id = ShiprocketOrder.objects.filter(order_id=order_id, status="success",
                                                     order_mode="Delivery").order_by('id').last()
    if get_sr_order_id:
        sr_order_id = get_sr_order_id.shiprocket_order_id

    if sr_order_id is not None:

        url = "https://apiv2.shiprocket.in/v1/external/orders/cancel"
        method = "POST"
        payload = {"ids": [int(sr_order_id)]}
        resp = call_shiprocket_api(url, payload, method)
        """
        success: resp
        {
            "status_code": 200,
            "message": "Order cancelled successfully."
        }
            
         NEW,INVOICED  
        {
            "status_code": 200,
            "message": "Order cancelled successfully."
        }
        
         READY TO SHIP
        {
            "status": 200,
            "message": "Your request to cancel order id 8481604424 has been taken. Since the AWB has already been assigned,  the freight amount against that order is blocked and it will be added back automatically in to your balance in 3-4 working days after the verification by the concerned team."
        }
        
        PICKUP EXCEPTION,PICKUP SCHEDULED,OUT FOR PICKUP,PICKUP EXCEPTION
        {
            "status": 200,
            "message": "Your request to cancel order id 4747416953 has been taken. Since the order's pick-up has already been scheduled, the freight amount against that order is blocked and it will be added back automatically in to Your balance in 3-4 working days after the verification by the concerned team."
        }
        
        DELIVERED
        {
            "message": "Cannot cancel order when shipment status is Delivered",
            "status_code": 400
        }
    
        """
        status_code = None
        status = None
        if "status_code" in resp:
            status_code = resp['status_code']
        if "status" in resp:
            status = resp['status']
        if status_code == "200" or status_code == 200:
            # cancel the order
            return True
        elif status_code == "400" or status_code == 400:
            """
            400 (already Cancel)
            response {'message': 'Cannot cancel order when shipment status is Cancellation Requested',
            'status_code': 400}
            """
            return True
        elif status in ["200", 200, "400", 400]:
            return True

        else:
            return False

    return False


def shiprocket_cancel_mfpl_order_details_api(order_details_id):
    """
    parameter: order_details_id
    This function will cancel Shiprocket Order and returns amount and credit to users wallet..
    """
    sr_order_id = None
    get_sr_order_id = ShiprocketOrder.objects.filter(order_details=order_details_id, status="success",
                                                     order_mode="Delivery").order_by('id').last()
    if get_sr_order_id:
        sr_order_id = get_sr_order_id.shiprocket_order_id

    if sr_order_id is not None:

        url = "https://apiv2.shiprocket.in/v1/external/orders/cancel"
        method = "POST"
        payload = {"ids": [int(sr_order_id)]}
        resp = call_shiprocket_api(url, payload, method)
        """
        success: resp
        {
            "status_code": 200,
            "message": "Order cancelled successfully."
        }

         NEW,INVOICED  
        {
            "status_code": 200,
            "message": "Order cancelled successfully."
        }

         READY TO SHIP
        {
            "status": 200,
            "message": "Your request to cancel order id 8481604424 has been taken. Since the AWB has already been assigned,  the freight amount against that order is blocked and it will be added back automatically in to your balance in 3-4 working days after the verification by the concerned team."
        }

        PICKUP EXCEPTION,PICKUP SCHEDULED,OUT FOR PICKUP,PICKUP EXCEPTION
        {
            "status": 200,
            "message": "Your request to cancel order id 4747416953 has been taken. Since the order's pick-up has already been scheduled, the freight amount against that order is blocked and it will be added back automatically in to Your balance in 3-4 working days after the verification by the concerned team."
        }

        DELIVERED
        {
            "message": "Cannot cancel order when shipment status is Delivered",
            "status_code": 400
        }

        """
        status_code = None
        status = None
        if "status_code" in resp:
            status_code = resp['status_code']
        if "status" in resp:
            status = resp['status']
        if status_code == "200" or status_code == 200:
            # cancel the order
            return True
        elif status_code == "400" or status_code == 400:
            """
            400 (already Cancel)
            response {'message': 'Cannot cancel order when shipment status is Cancellation Requested',
            'status_code': 400}
            """
            return True
        elif status in ["200", 200, "400", 400]:
            return True
        else:
            return False

    return False


def get_sr_logistics_status(status):
    """
    parameter : status
    converts logistics status and returns status from our project
    :return:

    ignore this status
    6 	Shipped  : shipped
    7 	Delivered : delivered
    8 	Cancelled : cancelled
    17 	Out For Delivery : in_transit
    18 	In Transit : in_transit
    19 	Out For Pickup
    20 	Pickup Exception
    21 	Undelivered : undelivered
    22 	Delayed

    # Status
    OUT_FOR_PICKUP
    PICKED_UP            -  dispatched
    PICKUP_EXCEPTION
    SHIPPED
    IN_TRANSIT           -  in_transit
    OUT_FOR_PICKUP
    UNDELIVERED          -  undelivered
    DELAYED
    MISROUTED
    AT_DESTINATION_HUB
    RTO_INITIATED
    RTO_OFD
    OUT_FOR_DELIVERY
    DELIVERED            -  delivered
    RTO_DELIVERED

    """
    our_status = None
    status_dict = {
        "Delivered": "delivered",
        "DELIVERED": "delivered",
        "delivered": "delivered",

        "Cancelled": "cancelled",
        "cancelled": "cancelled",

        "Returned": "return",
        "returned": "return",

        "Return Cancelled": "",
        "Return Delivered": "",

        "Picked Up": "dispatched",
        "PICKED_UP": "dispatched",
        "picked up": "dispatched",
        "picked_up": "dispatched",
        "PICKED UP": "dispatched",
        "PICK UP": "dispatched",

        "IN_TRANSIT": "in_transit",
        "In Transit": "in_transit",
        "in transit": "in_transit",
        "in_transit": "in_transit",
        "IN TRANSIT": "in_transit",

        "Undelivered": "undelivered",
        "UNDELIVERED": "undelivered",
        "undelivered": "undelivered",
    }

    # print(status)
    if status in status_dict:
        our_status = status_dict[status]
    return our_status


def shiprocket_update_current_order_status(current_status, order_id):
    """
    updates current status in order table
    :param current_status:
    :param order_id:
    :return:
    """
    get_order_obj = Order.objects.filter(id=order_id)
    if get_order_obj:
        if current_status is not None:
            update_order_status = Order.objects.filter(id=order_id)
            if update_order_status:
                update_order_status[0].status = current_status
                update_order_status[0].updated_date = datetime.datetime.now()
                update_order_status[0].save()
                return True

    return False


def sm():
    sms_message = "sr web"
    sms_kwrgs = {
        'sms_message': sms_message,
        'number': '7875583679'
    }
    sms(**sms_kwrgs)

    pass


class ShiprocketWebhook(generics.ListAPIView):

    def post(self, request, *args, **kwargs):
        """
        sample resp:
        {
            "awb": 123456,
            "current_status": "Delivered",
            "current_status_id": 7,
            "shipment_status": "Delivered",
            "shipment_status_id": 7,
            "current_timestamp": "2020-09-22 12:46:22",
            "order_id": "dummpy shiprocket order id 123",
            "channel_order_id": "dummy your order id 123",
            "channel": "dummy your channel name",
            "courier_name": "dummy courier_name",
            "etd": "22 Sep 2020",
            "scans": [
                {
                    "date": "2019-06-18 15:33:14",
                    "activity": "Manifested - Consignment Manifested",
                    "location": "Mumbai_Chndivli_PC (Maharashtra)"
                },
                {
                    "date": "2019-06-19 13:30:12",
                    "activity": "Manifested - Out for Pickup",
                    "location": "Mumbai_Chndivli_PC (Maharashtra)"
                }
            ]
        }
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        response = request.data
        try:
            # Sending Email to the customer for giving credentials..
            sub = "pass"
            if response:
                if "order_id" in response:
                    if "UNI" in response["order_id"]:
                        sub = "uni"
                    elif "ORD" in response["order_id"]:
                        sub = "ord"
                    else:
                        sub = "none"
            sub = "CreditKart -wb --" + sub
            to_email = "keval.thakkar@mudrakwik.com"
            email_kwargs = {
                'subject': sub,
                'to_email': [to_email],
                'cc_mail': ["test@thecreditkart.com"],
                'email_template': 'Ecom/emails/testing_email.html',
                'data': {
                    'data': response
                },
            }
            from EApi.ecom_email import order_email
            order_email(**email_kwargs)
        except Exception as e:
            print("--email exc in webhook", e)
            pass
        if response:
            try:
                awb = None
                if "awb" in response:
                    awb = response['awb']

                sr_current_status = None
                current_status = None
                if "current_status" in response:
                    sr_current_status = response['current_status']
                    current_status = response['current_status']
                    if current_status is not None or not current_status:
                        current_status = get_sr_logistics_status(current_status)

                current_status_id = None
                if "current_status_id" in response:
                    current_status_id = response['current_status_id']

                shipment_status = None
                if "shipment_status" in response:
                    shipment_status = response['shipment_status']

                shipment_status_id = None
                if "shipment_status_id" in response:
                    shipment_status_id = response['shipment_status_id']

                order_id = None
                if "order_id" in response:
                    order_id = response['order_id']
                    if order_id:
                        order_id = str(order_id)

                channel_order_id = None
                if "channel_order_id" in response:
                    channel_order_id = response['channel_order_id']

                channel = None
                if "channel" in response:
                    channel = response['channel']

                courier_name = None
                if "courier_name" in response:
                    courier_name = response['courier_name']

                etd = None
                if "etd" in response:
                    etd = response['etd']

                if order_id is not None:
                    if "UNI" in order_id:
                        get_order = ShiprocketOrder.objects.filter(order__unique_order_id=order_id).first()
                        if get_order:
                            get_order = ShiprocketOrder.objects.filter(id=get_order.id).first()
                            order_id = get_order.order_id
                            order_obj = get_order.order
                            # Updating Order's status
                            shiprocket_update_current_order_status(current_status, order_id)

                            # if "RTO" in sr_current_status or sr_current_status.lower() == "undelivered":
                            if sr_current_status in ["RTO", "rto", "rto_completed", "rto_created", "RTO Delivered",
                                                     "RTO DELIVERED", "RTO INITIATED", "rto_in_transit",
                                                     "RTO In Transit",
                                                     "RTO_NDR", "RTO Notified", "RTO Notified RTO_Attempted shipment",
                                                     "RTO_OFD", "RTO Out For Delivery", "RTO Pending", "RTO Processing",
                                                     "rto_recieved", "RTO - RETURN Accepted"]:
                                get_return_order = ReturnOrder.objects.filter(order_id=order_id,
                                                                              verify_return_status="return_confirm")
                                if not get_return_order:
                                    our_status = "undelivered"
                                    previous_undelivered = OrderStatus.objects.filter(order=order_id,
                                                                                      status="undelivered")
                                    if not previous_undelivered:
                                        from EApi.order import handel_undelivered_order
                                        try:
                                            handel_undelivered_condition = handel_undelivered_order(order_id)
                                        except Exception as e:
                                            print("Inside Exception of handel_undelivered_condition", e)

                            # Return cancel order
                            if sr_current_status in ['REV Cancelled', "rev cancelled", "rev_cancelled",
                                                     "return_cancelled"]:
                                ret_cancel_status = "return_cancelled"
                                get_return_cancel_order = OrderStatus.objects.filter(order=order_id,
                                                                                     status=ret_cancel_status)
                                if not get_return_cancel_order:

                                    update_status = Order.objects.filter(id=order_id)
                                    if update_status:
                                        update_status[0].status = ret_cancel_status
                                        update_status[0].updated_date = datetime.datetime.now()
                                        update_status[0].save()

                                    current_status_data = {"status": ret_cancel_status,
                                                           "date": datetime.datetime.now()}

                                    from EApi.logistics import save_current_logistics_status
                                    save_current_logistics_status(order_obj, current_status_data)

                                    order_details_obj = OrderDetails.objects.filter(
                                        order_id=order_obj.id).first()
                                    if order_details_obj:
                                        return_cancel_status = "return_cancel_delivery_charge"

                                        from EApi.logistics import refund_delivery_charge_to_wallet
                                        refund_delivery_charge_to_wallet(order_obj, order_details_obj,
                                                                         return_cancel_status)

                            # Lost Order..
                            if sr_current_status in ["lost", "Lost", "LOST"]:
                                get_lost_order = OrderStatus.objects.filter(order=order_id,
                                                                            status="lost")

                                if not get_lost_order:
                                    current_status_data = {"status": "lost",
                                                           "date": datetime.datetime.now()}

                                    from EApi.logistics import save_current_logistics_status
                                    save_current_logistics_status(order_obj, current_status_data)

                                    # Checking already given cancel money or not..
                                    already_given_wallet = UserWallet.objects.filter(order=order_id,
                                                                                     status__in=["order_cancel",
                                                                                                 "convenience_charge",
                                                                                                 "delivery_charge"])

                                    if not already_given_wallet:
                                        from EApi.order import cancel_order
                                        # This will cancel Creditkart order and return amount to his credit and wallet..
                                        calling_order_cancel = cancel_order(order_id)

                            # Creating OrderStatus
                            from EApi.logistics import save_order_status
                            order_status_data = {"status": sr_current_status, "scan_date_time": datetime.datetime.now()}

                            save_order_status(order_obj, order_status_data)

                        scans = None
                        existing_status_list = []
                        order_list = ['dispatched', 'in_transit', 'delivered', 'return']

                        logistic_obj = LogisticOrder.objects.filter(order=order_id).exclude(order__status='cancelled')
                        if logistic_obj:
                            order_obj = logistic_obj[0].order

                            if "scans" in response:
                                scans = response['scans']

                                get_existing_status_list = OrderStatus.objects.filter(order=order_id).values_list(
                                    'status',
                                    flat=True)
                                if get_existing_status_list:
                                    existing_status_list = get_existing_status_list

                                get_sr_order = ShiprocketOrder.objects.filter(order_id=order_id).first()
                                if get_sr_order:
                                    get_order_status = Order.objects.filter(id=get_sr_order.order_id).first()
                                    if get_order_status:
                                        for data in scans:
                                            date = data['date']
                                            activity = data['activity']
                                            location = data['location']
                                            get_transist_status = OrderStatus.objects.filter(status=activity,
                                                                                             order=order_obj).first()
                                            if not get_transist_status:
                                                add_status = OrderStatus.objects.create(status=activity,
                                                                                        order=logistic_obj[0].order,
                                                                                        date=date)
                        # adding in between status status

                        if current_status not in existing_status_list or existing_status_list == [] or existing_status_list is None:
                            if current_status in order_list:
                                get_last_index = order_list.index(current_status)
                                get_last_index += 1
                                for j in range(0, get_last_index):
                                    new_status = order_list[j]
                                    get_status = OrderStatus.objects.filter(status=new_status,
                                                                            order=logistic_obj[0].order)
                                    if not get_status or get_status is None:
                                        add_status = OrderStatus.objects.create(status=new_status,
                                                                                order=logistic_obj[0].order,
                                                                                date=datetime.datetime.now())

                    if "ORD" in order_id:
                        get_order = ShiprocketOrder.objects.filter(order_details__order_text=order_id).first()
                        if get_order:
                            get_order_details_obj = OrderDetails.objects.filter(order_text=order_id).first()
                            if get_order_details_obj:
                                get_combine_orders = get_order_details_obj.combine_order.exclude(status="cancelled")
                                if get_combine_orders:
                                    for order in get_combine_orders:
                                        order_id = order.id
                                        order_obj = order

                                        # Updating Order's status
                                        shiprocket_update_current_order_status(current_status, order_id)

                                        if sr_current_status in ["RTO", "rto", "rto_completed", "rto_created",
                                                                 "RTO Delivered",
                                                                 "RTO DELIVERED", "RTO INITIATED", "rto_in_transit",
                                                                 "RTO In Transit",
                                                                 "RTO_NDR", "RTO Notified",
                                                                 "RTO Notified RTO_Attempted shipment",
                                                                 "RTO_OFD", "RTO Out For Delivery", "RTO Pending",
                                                                 "RTO Processing",
                                                                 "rto_recieved", "RTO - RETURN Accepted"]:

                                            get_return_order = ReturnOrder.objects.filter(order_id=order_id,
                                                                                          verify_return_status="return_confirm")
                                            if not get_return_order:
                                                our_status = "undelivered"
                                                previous_undelivered = OrderStatus.objects.filter(order=order_id,
                                                                                                  status="undelivered")
                                                if not previous_undelivered:
                                                    from EApi.order import handel_undelivered_order
                                                    try:
                                                        handel_undelivered_condition = handel_undelivered_order(
                                                            order_id)
                                                    except Exception as e:
                                                        print("Inside Exception of handel_undelivered_condition", e)

                                        # Return cancel order
                                        if sr_current_status in ['REV Cancelled', "rev cancelled", "rev_cancelled",
                                                                 "return_cancelled"]:
                                            ret_cancel_status = "return_cancelled"
                                            get_return_cancel_order = OrderStatus.objects.filter(order=order_id,
                                                                                                 status=ret_cancel_status)
                                            if not get_return_cancel_order:
                                                update_status = Order.objects.filter(id=order_id)
                                                if update_status:
                                                    update_status[0].status = ret_cancel_status
                                                    update_status[0].updated_date = datetime.datetime.now()
                                                    update_status[0].save()

                                                current_status_data = {"status": ret_cancel_status,
                                                                       "date": datetime.datetime.now()}

                                                from EApi.logistics import save_current_logistics_status
                                                save_current_logistics_status(order_obj, current_status_data)

                                                order_details_obj = OrderDetails.objects.filter(
                                                    order_id=order_obj.id).first()
                                                if order_details_obj:
                                                    return_cancel_status = "return_cancel_delivery_charge"

                                                    from EApi.logistics import refund_delivery_charge_to_wallet
                                                    refund_delivery_charge_to_wallet(order_obj, order_details_obj,
                                                                                     return_cancel_status)

                                        # Lost Order..
                                        if sr_current_status in ["lost", "Lost", "LOST"]:
                                            get_lost_order = OrderStatus.objects.filter(order=order_id,
                                                                                        status="lost")

                                            if not get_lost_order:
                                                current_status_data = {"status": "lost",
                                                                       "date": datetime.datetime.now()}

                                                from EApi.logistics import save_current_logistics_status
                                                save_current_logistics_status(order_obj,
                                                                              current_status_data)

                                                # Checking already given cancel money or not..
                                                already_given_wallet = UserWallet.objects.filter(
                                                    order=order_id,
                                                    status__in=["order_cancel",
                                                                "convenience_charge",
                                                                "delivery_charge"])

                                                if not already_given_wallet:
                                                    from EApi.order import cancel_order
                                                    # This will cancel Creditkart order and return amount
                                                    # to his credit and wallet..
                                                    calling_order_cancel = cancel_order(order_id)

                                        # Creating OrderStatus
                                        from EApi.logistics import save_order_status
                                        order_status_data = {"status": sr_current_status,
                                                             "scan_date_time": datetime.datetime.now()}
                                        save_order_status(order_obj, order_status_data)

                                        scans = None
                                        existing_status_list = []
                                        order_list = ['dispatched', 'in_transit', 'delivered', 'return']

                                        if "scans" in response:
                                            scans = response['scans']

                                            get_existing_status_list = OrderStatus.objects.filter(
                                                order=order_id).values_list(
                                                'status',
                                                flat=True)
                                            if get_existing_status_list:
                                                existing_status_list = get_existing_status_list

                                            for data in scans:
                                                # print("-----", data)
                                                date = data['date']
                                                activity = data['activity']
                                                location = data['location']
                                                get_transist_status = OrderStatus.objects.filter(status=activity,
                                                                                                 order=order_obj).first()
                                                if not get_transist_status:
                                                    add_status = OrderStatus.objects.create(status=activity,
                                                                                            order=order_obj,
                                                                                            date=date)
                                        # adding in between status status

                                        if current_status not in existing_status_list or existing_status_list == [] or existing_status_list is None:
                                            if current_status in order_list:
                                                get_last_index = order_list.index(current_status)
                                                get_last_index += 1
                                                for j in range(0, get_last_index):
                                                    new_status = order_list[j]
                                                    get_status = OrderStatus.objects.filter(status=new_status,
                                                                                            order=order_obj)
                                                    if not get_status or get_status is None:
                                                        add_status = OrderStatus.objects.create(status=new_status,
                                                                                                order=order_obj,
                                                                                                date=datetime.datetime.now())

            except Exception as e:
                print('-----------in exception- webhook shiprocket---------')
                print(e.args)
                import os
                import sys
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)

        return Response({
            'status': 1,
            'message': "ok"
        })


def process_shiprocket_tracking_data(response, shipment_id):
    """
    params : response
    params: shipment_id

    This function get shiprocketorder first entry and get  order from shiprocketorder
    and get track_order,shipment_track and current_status from track_order then create
    entry in orderstatus.

    """
    order_obj = None
    shiprocket_order = ShiprocketOrder.objects.filter(shipment_id=shipment_id).first()
    if shiprocket_order:
        order_obj = shiprocket_order.order

        if response:
            if "tracking_data" in response:
                tracking_data = track_order["track_order"]
                if "shipment_track" in tracking_data:
                    shipment_track = tracking_data["shipment_track"]
                    if "current_status" in shipment_track:
                        current_status = shipment_track["current_status"]

                if "shipment_track_activities" in tracking_data:
                    shipment_track_activities = tracking_data["shipment_track_activities"]
                    for i in shipment_track_activities:
                        date = i["date"]
                        status = i["status"]
                        activity = i["activity"]
                        location = i["location"]
                        get_order_status = OrderStatus.objects.filter(order=order_obj, status=status)
                        if not get_order_status:
                            add_status = OrderStatus.objects.create(status=status, order=order_obj,
                                                                    date=date)


def track_order(awb):
    """
    parameter:awb

    This function get response from call_shiprocket_api by using url,payload and method.then pass that
    response to the process_shiprocket_tracking_data
    """
    if awb is not None:
        url = "https://apiv2.shiprocket.in/v1/external/courier/track/awb/" + awb
        method = "GET"
        payload = {}
        response = call_shiprocket_api(url, payload, method)

        process_shiprocket_tracking_data(response)


def track_order_with_shipment_id(shipment_id):
    """
       parameter:shipment_id

       This function get response from call_shiprocket_api by using url,payload and method.then pass that
       response to the process_shiprocket_tracking_data
       """

    if shipment_id is not None:
        url = "https://apiv2.shiprocket.in/v1/external/courier/track/shipment/" + shipment_id
        method = "GET"
        payload = {}
        response = call_shiprocket_api(url, payload, method)

        process_shiprocket_tracking_data(response)
