from django.urls import path

from .insurance import CreateUserAPIView, InsuranceAPIView

from .EChecksum_web import ChecksumGenerateEcomPaytmOrder, \
    ChecksumGenerateEcomPaytmOrderWebsite, \
    ChecksumGenerateEcomPaytmRepayment, callback_order_response, ChecksumGenerateEcomPaytmRepaymentWebsite, \
    callback_repayment_response, PaymentTransactionStatusApi, pass_order_id_order, callback_return_delivery_amount, \
    Order_transaction_Status_Check_auto_Application, Repayment_transaction_Status_Check_auto_Application, \
    Return_Order_Delivery_Charge_Status_Check_Application

from .auth import *
from .order import UserOrdersApi, UserOrderDetailsApi, OrderDetailsApi, OrderPlaceApi, OrderPlaceApiNew, OrderReturnApi, \
    OrderCancellationApi, OrderReturnApiWebsite, OrderReturnApiWebsitePhonePe, return_wallet_amount_cancel_order, \
    Invoices, InvoiceDetails, \
    OrderPaymentDeliveryChargesDetail, UserOrderHistoryApi, ReturnOrderDetailsList, ReturnOrderList, \
    ReturnSellerOrderList, SellerReturnRequestOrder, AdminOrderDetailsVerify, SellerOrderReturnVerify, OrderVariantApi, \
    SellerReturnOrderHistory, SellerExchangeOrderHistory, CheckOutApi, seller_amount_calculations, \
    SellerReturnOrderdata, CalculateExchangeAmountAPIView, SellerExchangeOrderdata, \
    return_wallet_amount_undelivered_order, UniqueIdStatus

from .product import *
from .EPayment_auth import *
from .ERazorpay import *
from .cart import *
from .logistics import *
from .credit import *
from .ecom_email import ContactUsMailSend, CareerPortalMailSend, test_email
from .wallet import *
from .shiprocket import *
from .razorpay import *

urlpatterns = [

    # insurance

    path("create_user/", CreateUserAPIView.as_view(), name="create_user"),
    path("get_insurance_data/", InsuranceAPIView.as_view(), name="get_insurance_data"),

    # path('dashboard_products', DashboardProductsApi.as_view(), name="dashboard_products"),
    path('product_details_api', ProductDetailsApi.as_view(), name="Product_Details_Api"),
    path('dashboard_products_new', DashboardProductsApiNew.as_view(), name="dashboard_products_new"),
    path('category_list', CategoryListAPI.as_view(), name="category_list"),
    path('new_category_list', NewCategoryListAPI.as_view(), name="new_category_list"),
    path('similar_products', SimilarProductsAPI.as_view(), name="similar_products"),
    path('products_for_category', ProductsForCategory.as_view(), name="products_for_category"),
    path('products_for_category_new', ProductsForCategoryNew.as_view(), name="products_for_category_new"),
    path('colours_for_product', ColoursForProduct.as_view(), name="colours_for_product"),
    path('size_for_product', SizeForProduct.as_view(), name="size_for_product"),
    path('particular_product_info', ParticularProductInfoAPI.as_view(), name="particular_product_info"),
    path('product_details_info', ProductDetailsInfoAPI.as_view(), name="product_details_info"),
    path('search_product', SearchProducts.as_view(), name="search_products"),
    path('search_suggestion', SearchSuggestionAPI.as_view()),
    path('search_suggestion_web', SearchSuggestionAPI_web.as_view()),
    path('product_all_details_api', ProductAllDetailsAPI.as_view()),
    path('add_product_rating/', ProductReviewsRatingAPI.as_view()),
    path('product_reviews/', ProductReviewsAPI.as_view()),
    # path('return_category/', ReturnCategoryApi.as_view()),
    path('return_category/', ShopByCategoryAPI.as_view()),
    path('product_meta/', ProductMetaAPI.as_view()),
    path('product/', ProductAPI.as_view()),

    path("ProductsForCategoryPagination/", ProductsForCategoryPagination.as_view()),
    path("banner_api/", BannerApi.as_view()),
    path("offer_banner_api/", OfferBannerApi.as_view()),
    path("offer_banner_api_web/", OfferBannerApiWeb.as_view()),
    path("user_offer_api/", UserOfferApi.as_view()),
    path("user_offer_is_read_api/", UserOfferIsReadApi.as_view()),

    # Orders related urls
    path('user_orders', UserOrdersApi.as_view(), name="user_orders"),
    path('user_order_details', UserOrderDetailsApi.as_view(), name="user_order_details"),
    path('order-place', OrderPlaceApi.as_view()),
    path('order-place-new', OrderPlaceApiNew.as_view()),
    path('order-return', OrderReturnApi.as_view()),
    path('order-return-web/', OrderReturnApiWebsite.as_view()),
    path('order-return-phonepe/', OrderReturnApiWebsitePhonePe.as_view()),
    path('order-cancel', OrderCancellationApi.as_view()),
    path("unique_id_status/", UniqueIdStatus.as_view()),
    path('Invoices', Invoices.as_view()),
    path('invoice_details/', InvoiceDetails.as_view(), name='invoice_details'),
    path("order_payment_delivery_charges_detail/", OrderPaymentDeliveryChargesDetail.as_view(),
         name='order_payment_delivery_charges_detail'),
    path('user-order-history-api', UserOrderHistoryApi.as_view()),
    path('order_details_api/', OrderDetailsApi.as_view()),

    path('return_order_details_list/', ReturnOrderDetailsList.as_view(), name="return_order_details_list"),
    path('return_order_list/<seller>/', ReturnOrderList.as_view(), name="return_order_list"),
    path('return_seller_order_list/', ReturnSellerOrderList.as_view(), name="return_seller_order_list"),
    path('seller_return_request_order/', SellerReturnRequestOrder.as_view()),
    path('seller_return_order_data/<seller>/<status>/', SellerReturnOrderdata.as_view()),

    path('admin-order-return-verify/', AdminOrderDetailsVerify),
    path('seller-order-return-verify/', SellerOrderReturnVerify),

    # Order Exchange API..
    path("get_order_variant/", OrderVariantApi.as_view(), name="get_order_variant"),
    path("calculate_exchange_amount/", CalculateExchangeAmountAPIView.as_view(), name="calculate_exchange_amount"),

    path('seller_return_order_history/<seller>/', SellerReturnOrderHistory.as_view(),
         name="seller_return_order_history"),
    path('seller_return_order_data/<seller>/<status>/<type>/', SellerReturnOrderdata.as_view()),
    path('seller_return_order_history/', SellerReturnOrderHistory.as_view(),
         name="seller_return_order_history"),
    path('seller_exchange_order_history/', SellerExchangeOrderHistory.as_view(),
         name="seller_exchange_order_history"),
    path('seller_exchange_order_data/<seller>/<status>/', SellerExchangeOrderdata.as_view()),

    # Auth related urls
    path('user_login', UserLoginApi.as_view(), name="user_login"),
    path('reset_pin/', ResetPin.as_view(), name="reset_pin"),
    path('user-ecom-address', UserEcomAddresses.as_view(), name="user_ecom_address"),
    path('user_ecom_updated_address/', UserEcomUpdatedAddress.as_view(), name="user_ecom_updated_address"),
    path('user_ecom_shipping_address/', UserEcomShippingAddress.as_view(), name="user_ecom_shipping_address"),
    path('check_out/', CheckOutApi.as_view(), name='check_out'),
    path('user_personal_details_add/', UserPersonalAddressAdd.as_view()),
    path('validate_user/', ValidateToken.as_view(), name="validate_user"),
    path('user_personal_address_info/', UserPersonalInfoDetails.as_view()),
    path("store_email/", StoreEmail.as_view()),

    # ecom_email related urls..
    path('contact_us_mail/', ContactUsMailSend.as_view()),
    path('career_portal_mail/', CareerPortalMailSend.as_view()),

    # Paytm urls..

    path('checksum_generate_ecom_paytm_order', ChecksumGenerateEcomPaytmOrder.as_view()),
    path('checksum_generate_ecom_paytm_order_web/', ChecksumGenerateEcomPaytmOrderWebsite.as_view()),
    path('checksum_generate_ecom_paytm_repayment', ChecksumGenerateEcomPaytmRepayment.as_view()),
    path('checksum_generate_ecom_paytm_repayment_web/', ChecksumGenerateEcomPaytmRepaymentWebsite.as_view()),
    # path('checksum_generate_return_delivery_amount', ChecksumGenerateReturnDeliveryAmount.as_view()),

    path('ecom_paytm_payment', EcomPaytmPaymentApiView.as_view()),
    path('callback_order_url/<order_id>', callback_order_response),
    path('callback_repayment_url/<order_id>', callback_repayment_response),

    path('callback_return_delivery_amount_url/<order_id>', callback_return_delivery_amount),

    path('paytm_transaction_status_api', PaymentTransactionStatusApi.as_view()),

    # Razorpay urls..
    path('erazorpay/', ERazorPayOrder.as_view()),
    path('erazorpay_signcheck/', ERazorPaySignCheck.as_view()),

    # new Razorpay urls ...
    path('razorpay_order_website/', RazorpayEcomOrderWebsite.as_view()),
    path('razorpay_order_website_phonepe/', RazorpayEcomOrderWebsitePhonePe.as_view()),
    path('callback_order_razorpay/', CallbackOrderRazorpay.as_view()),
    path('razorpay_transaction_status/', RazorpayPaymentTransactionStatusApi.as_view()),

    path('razorpay_repayment_website/', RazorpayEcomRepaymentWebsite.as_view()),
    path('razorpay_repayment_website_phonepe/', RazorpayEcomRepaymentWebsitePhonePe.as_view()),
    path('callback_repayment_razorpay/', CallbackRepaymentRazorpay.as_view()),
    path('callback_return_delivery_razorpay/', CallbackReturnDeliveryRazorpay.as_view()),

    path("check_status_of_payment/<payment_id>/", check_status_of_payment),

    path('razorpay_order_app/', RazorpayEcomOrderApp.as_view()),
    path('razorpay_repayment_app/', RazorpayEcomRepaymentApp.as_view()),
    path('razorpay_transaction_status_order_app/', Order_transaction_Status_Check_auto_app.as_view()),
    path('razorpay_transaction_status_repayment_app/', Repayment_transaction_Status_Check_auto_App.as_view()),

    path('ewebhook_order/', ERazorpayWebhookOrderNew.as_view()),

    path('razorpay_repayment_all/', RazorpayEcomRepaymentAll.as_view()),
    path('repayment_transaction_status_check_auto_all/', Repayment_transaction_Status_Check_auto_All.as_view()),

    # Cart related urls..
    path('cart-product-add/', CartProductAddApi.as_view()),
    path('cart-product-remove/', CartProductRemoveApi.as_view()),
    path('cart-product-edit/', CartProductEditApi.as_view()),
    path('user-cart/', UserCartDetailsApi.as_view()),

    path('seller_calculations/', seller_amount_calculations),

    # Logistics related urls..
    path('ecom_validate_pin_code/', EcomValidatePincodeAPIView.as_view()),
    path('get_logistics_agent/', get_logistics_agent),
    path('data/', get_product_details_price),
    path('track_order/', track_order),
    path("add_warehouse/", Add_Warehouse.as_view(), name="add_warehouse"),
    path("add_warehouse/(?P<message>)/", Add_Warehouse.as_view(), name="add_warehouse"),
    path("add_warehouse_api/", Add_WarehouseAPI.as_view()),
    path("country_state_pincode/", country_state_pincode),
    path("warehouse", get_warehouse_status),
    path('get_warehouse_approval/', get_approval),
    path('order_tracking_api', OrderTrackingApiV2.as_view(), name="order_tracking_api"),
    path("logistic_orders/<seller>/<status>/", LogisticOrderDetails.as_view(), name="logistic_order_details"),
    path("logistic_delivery_reverse_count/", LogisticsDeliveryReverseCount.as_view(), name="logistics_delivery_count"),
    path("update_order_status/", update_order_status),

    path("seller_warehouse_details/", SellerWareHouseDetails.as_view(), name="seller_ware_house_details"),
    path("seller_shiprocket_warehouse_details/", SellerShipRocketWareHouseDetails.as_view(),
         name="seller_shiprocket_warehouse_details"),
    path("logistic_orders/", LogisticOrderDetails.as_view(), name="logistic_order_details"),
    path('update_warehouse_status/<ware_house_id>/', update_warehouse_status),
    path('update_shiprocket_warehouse_status/<ware_house_id>/', update_shiprocket_warehouse_status),
    path('print_shipment_label_api/', print_shipment_label),
    path('print_manifest_api/', print_manifest_api),
    path('print_shiprocket_manifest_api_req/', print_shiprocket_manifest_api_req),
    path('generate_label_api_req/', generate_label_api_req),
    path('print_shiprocket_order_details_manifest_api_req/', print_shiprocket_order_details_manifest_api_req),
    path('generate_order_details_label_api_req/', generate_order_details_label_api_req),

    path('externally_create_logistic_order/', ExternallyCreateLogisticOrder.as_view()),

    # Shiprocket urls..
    path("add_shiprocket_warehouse/", AddShiprocketWarehouse.as_view(), name="add_shiprocket_warehouse"),
    path("add_shiprocket_warehouse/(?P<message>)/", AddShiprocketWarehouse.as_view(), name="add_shiprocket_warehouse"),

    # Credit.py related urls..
    path('user-credit-add/', user_credit_add),
    path('checkout-calculations/', CheckoutCalculations.as_view()),
    path('checkout-calculations-new/', CheckoutCalculationsNew.as_view()),
    path('user_credit_details/', UserCreditDetails.as_view()),
    path('payment_mode_selection_api/', PaymentModeSelectionApi.as_view()),

    # Wallet.py related urls..
    path('user_wallet_details/', UserWalletDetails.as_view(), name='user_wallet_details'),
    path('user_wallet_amount_details/', UserWalletAmountDetails.as_view()),
    path('user_wallet_amount_details/<user_info>/', UserWalletAmountDetails.as_view()),
    path('user_wallet_amount_details/<order_details_id>/<order_details_text>/', UserWalletAmountDetails.as_view()),

    path('pass_order/<order_id>/', pass_order_id_order),
    path('pass_repayment/<order_id>/', pass_order_id_order),
    path('test/', create_reverse_order_data),

    path('test_email/', test_email),

    # scheduler function
    path('fetch_logistics_status_request/', fetch_logistics_status_request),

    # Echecksum web urls..
    path('order_trancation_status_check_application/', Order_transaction_Status_Check_auto_Application.as_view()),
    path('repayment_trancation_status_check_application/',
         Repayment_transaction_Status_Check_auto_Application.as_view()),
    path('return_order_delivery_charge_status_check_application/',
         Return_Order_Delivery_Charge_Status_Check_Application.as_view()),

    # need to remove after script running..
    path('update_multiple_warehouse_status/', update_multiple_warehouse_status),
    path('update_shiprocket_warehouse_status/', update_shiprocket_warehouse_status),
    path('update_multiple_shiprocket_warehouse_status/', update_multiple_shiprocket_warehouse_status),
    path('get_token/', get_sr_token),

    # Shiprocket API's
    path('sr_is_delivery_available/', ShiprocketIsDeliveryAvailableAPI.as_view(), name="is_delivery_available"),
    path('shiprocket_webhook/', ShiprocketWebhook.as_view(), name="shiprocket_webhook"),

    path('dashboard_products_new_priority/', DashboardProductsApiNewWithPriority.as_view(),
         name="dashboard_products_new"),

    path('shop_by_category_api/', ShopByCategoryAPI.as_view()),

    # direct hit urls..
    path('attach_credit/<payment_order_id>/<amount>/', attach_credit_to_orderdetails),
    path('attach_loan_repayment/<payment_order_id>/<loan_status>/', attach_loan_bank_update_repayment),
    path('order_cancel_to_return_wallet/<unique_order_id>/', return_wallet_amount_cancel_order),
    path('order_undelivered_to_return_wallet/<unique_order_id>/', return_wallet_amount_undelivered_order),
    path('add_default_wallet_amount/', add_default_wallet_amount_script),
    path('add_return_wallet_amount/<unique_order_id>/<amount>/', add_wallet_balance_return_issue),

    # PhonePe urls..
    path('get_access_token/', GetAccessTokenAPI.as_view()),
    path('get_user_details/', GetUserDetailsAPI.as_view()),

    path('razorpay_repayment_all/', RazorpayEcomRepaymentAll.as_view()),
    path('repayment_transaction_status_check_auto_all/', Repayment_transaction_Status_Check_auto_All.as_view()),

    # COD status check
    path("cod_check_status", CodCheckStatus.as_view(), name="cod_check_status"),

]
