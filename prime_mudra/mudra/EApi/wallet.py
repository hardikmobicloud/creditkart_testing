import csv

from User.models import *
from django.apps import apps
from django.contrib.auth.models import User
from django.db.models import Sum
from django.http import HttpResponse
from django.shortcuts import render
from rest_framework import generics
from rest_framework.authentication import *
from rest_framework.permissions import *
from rest_framework.response import Response

Product = apps.get_model('ecom', 'Product')
ProductDetails = apps.get_model('ecom', 'ProductDetails')
Credit = apps.get_model('ecom', 'Credit')
Order = apps.get_model('ecom', 'Order')
OrderDetails = apps.get_model('ecom', 'OrderDetails')
OrderDetailsOrder = apps.get_model('ecom', 'orderdetails_order_id')
OrderStatus = apps.get_model('ecom', 'OrderStatus')
ReturnOrder = apps.get_model('ecom', 'ReturnOrder')
UserWallet = apps.get_model('ecom', 'UserWallet')
Wallet = apps.get_model('ecom', 'Wallet')
DefaultWalletAmount = apps.get_model('ecom', 'DefaultWalletAmount')
UserPersonal = apps.get_model('User', 'Personal')


def user_wallet_update(user_id, wallet_amount, order_details_obj, order_obj, status):
    """
    The input user_id will filter in UserWallet table, get last balance amount and return order amount will added
    in balance amount by creating new UserWallet entry.
    """
    import random
    import time
    add_user_wallet = None
    if user_id and wallet_amount is not None:
        user = User.objects.filter(id=user_id)
        get_wallet_obj = UserWallet.objects.filter(user=user_id).exclude(
            status__in=["initiated", "return_delivery_charge_initiated", "recharge_initiated"]).order_by(
            'id').last()
        if get_wallet_obj:
            last_balance_amount = get_wallet_obj.balance_amount
            new_balance_amount = last_balance_amount + wallet_amount
            if status in ["cashback_amount", "refund"]:
                get_obj = UserWallet.objects.filter(user=user_id, status=status,
                                                    order_details=order_details_obj)
                if not get_obj:
                    add_user_wallet = UserWallet.objects.create(amount_add=wallet_amount,
                                                                balance_amount=new_balance_amount,
                                                                order_details=order_details_obj, user=user[0],
                                                                order=order_obj,
                                                                status=status)
                    if add_user_wallet:
                        sleep_time = random.random()
                        time.sleep(sleep_time)
                        return add_user_wallet

            else:
                add_user_wallet = UserWallet.objects.create(amount_add=wallet_amount, balance_amount=new_balance_amount,
                                                            order_details=order_details_obj, user=user[0],
                                                            order=order_obj,
                                                            status=status)
                if add_user_wallet:
                    sleep_time = random.random()
                    time.sleep(sleep_time)
                    return add_user_wallet

        else:
            if status in ["cashback_amount", "refund"]:
                get_obj = UserWallet.objects.filter(user=user_id, status=status,
                                                    order_details=order_details_obj)
                if not get_obj:
                    add_user_wallet = UserWallet.objects.create(amount_add=wallet_amount, balance_amount=wallet_amount,
                                                                order_details=order_details_obj, user=user[0],
                                                                order=order_obj, status=status)

                    if add_user_wallet:
                        sleep_time = random.random()
                        time.sleep(sleep_time)
                        return add_user_wallet

            else:
                add_user_wallet = UserWallet.objects.create(amount_add=wallet_amount, balance_amount=wallet_amount,
                                                            order_details=order_details_obj, user=user[0],
                                                            order=order_obj,
                                                            status=status)
                if add_user_wallet:
                    sleep_time = random.random()
                    time.sleep(sleep_time)
                    return add_user_wallet

    return add_user_wallet


def user_wallet_sub(user_id, wallet_amount, order_details_obj, order_obj, status, auto_adjust=False):
    """
    This function give new balance amount by subtract wallet amount from last balance amount by
    creating new UserWallet entry.
    """
    if user_id and wallet_amount is not None:
        user = User.objects.filter(id=user_id)
        get_wallet_obj = UserWallet.objects.filter(user=user_id).exclude(
            status__in=["initiated", "return_delivery_charge_initiated", "recharge_initiated"]).order_by(
            'id').last()
        last_balance_amount = 0
        if get_wallet_obj:
            last_balance_amount = get_wallet_obj.balance_amount
        else:
            last_balance_amount = 0

        new_balance_amount = last_balance_amount - wallet_amount
        sub_user_wallet = UserWallet.objects.create(used_amount=wallet_amount, balance_amount=new_balance_amount,
                                                    order_details=order_details_obj, user=user[0],
                                                    status=status, order=order_obj, auto_adjust=auto_adjust)
        if sub_user_wallet:
            return sub_user_wallet


def user_wallet_sub_new(user_id, wallet_amount, order_details_obj, order_obj, status, auto_adjust=False):
    """
    This function give new balance amount by subtract wallet amount from last balance amount only if wallet
    amount is greater than zero by creating new UserWallet entry.
    """
    if user_id and wallet_amount is not None:
        user = User.objects.filter(id=user_id)
        get_wallet_obj = UserWallet.objects.filter(user=user_id).exclude(
            status__in=["initiated", "return_delivery_charge_initiated", "recharge_initiated"]).order_by(
            'id').last()
        last_balance_amount = 0
        if get_wallet_obj:
            last_balance_amount = get_wallet_obj.balance_amount
        else:
            last_balance_amount = 0
        new_balance_amount_1 = last_balance_amount - wallet_amount
        if new_balance_amount_1 >= 0:
            new_balance_amount = new_balance_amount_1
        else:
            new_balance_amount = 0
            wallet_amount = last_balance_amount
        if wallet_amount > 0:
            sub_user_wallet = UserWallet.objects.create(used_amount=wallet_amount, balance_amount=new_balance_amount,
                                                        order_details=order_details_obj, user=user[0],
                                                        status=status, order=order_obj, auto_adjust=auto_adjust)
    return wallet_amount


class UserWalletDetails(generics.ListAPIView):
    """
    This API gives information about user wallet details.
    method : get
    Response :
    [{
        "amount_add": 100,
        "used_amount": 50,
        "balance_amount": 50,
        "status": 'delivery_charge',
        "order_details": 102,
        "created_date": 2021-03-12
    }]

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        wallet_info = []
        user_id = request.user.id
        if user_id is not None:
            user_wallet_obj = UserWallet.objects.filter(user=user_id).exclude(status="initiated").order_by('-id')
            if user_wallet_obj:
                for user_wallet_data in user_wallet_obj:
                    order_details = ""
                    if user_wallet_data.order_details:
                        order_details = user_wallet_data.order_details.order_text
                    wallet_dict = {
                        "amount_add": user_wallet_data.amount_add,
                        "used_amount": user_wallet_data.used_amount,
                        "balance_amount": user_wallet_data.balance_amount,
                        "status": user_wallet_data.status,
                        "order_details": order_details,
                        "created_date": str(user_wallet_data.created_date)[:10]
                    }
                    wallet_info.append(wallet_dict)
                return Response(
                    {
                        "Status": "1",
                        "Wallet_Data": wallet_info
                    }
                )
            return Response(
                {
                    "Status": "9",
                    "Message": "User Not Found",
                    "Wallet_Data": wallet_info
                }
            )
        return Response(
            {
                "Status": "9",
                "Message": "Invalid User",
                "Wallet_Data": wallet_info
            }
        )

    def post(self, request, *args, **kwargs):
        """
        According to request status the UserWallet data return in response
         method : post
        Response :
        [{
            "amount_add": 100,
            "used_amount": 50,
            "balance_amount": 50,
            "status": 'delivery_charge',
            "order_details": 102,
            "created_date": 2021-03-12
        }]
        """

        status = request.data
        user_id = request.user.id
        if status["status"] == "All":
            wallet_info = []
            if user_id is not None:
                user_wallet_obj = UserWallet.objects.filter(user=user_id).exclude(status="initiated").order_by('-id')
                if user_wallet_obj:
                    for user_wallet_data in user_wallet_obj:
                        order_details = ""
                        if user_wallet_data.order_details:
                            order_details = user_wallet_data.order_details.order_text
                        wallet_dict = {
                            "amount_add": user_wallet_data.amount_add,
                            "used_amount": user_wallet_data.used_amount,
                            "balance_amount": user_wallet_data.balance_amount,
                            "status": user_wallet_data.status,
                            "order_details": order_details,
                            "created_date": str(user_wallet_data.created_date)[:10]
                        }
                        wallet_info.append(wallet_dict)
                    return Response(
                        {
                            "Status": "1",
                            "Wallet_Data": wallet_info
                        }
                    )
                return Response(
                    {
                        "Status": "9",
                        "Message": "User Not Found",
                        "Wallet_Data": wallet_info
                    }
                )
            return Response(
                {
                    "Status": "9",
                    "Message": "Invalid User",
                    "Wallet_Data": wallet_info
                }
            )

        if status["status"] == "wallet":
            wallet_info = []
            if user_id is not None:
                user_wallet_obj = UserWallet.objects.filter(user=user_id).exclude(status="Loan_Repayment").exclude(status="loan_repayment").exclude(status="disbursed").exclude(status="loan_disbursed").exclude(status="initiated").order_by('-id')
                if user_wallet_obj:
                    for user_wallet_data in user_wallet_obj:
                        order_details = ""
                        if user_wallet_data.order_details:
                            order_details = user_wallet_data.order_details.order_text
                        wallet_dict = {
                            "amount_add": user_wallet_data.amount_add,
                            "used_amount": user_wallet_data.used_amount,
                            "balance_amount": user_wallet_data.balance_amount,
                            "status": user_wallet_data.status,
                            "order_details": order_details,
                            "created_date": str(user_wallet_data.created_date)[:10]
                        }
                        wallet_info.append(wallet_dict)
                    return Response(
                        {
                            "Status": "1",
                            "Wallet_Data": wallet_info
                        }
                    )
                return Response(
                    {
                        "Status": "9",
                        "Message": "User Not Found",
                        "Wallet_Data": wallet_info
                    }
                )
            return Response(
                {
                    "Status": "9",
                    "Message": "Invalid User",
                    "Wallet_Data": wallet_info
                }
            )
        if status["status"] == "withdraw":
            wallet_info = []
            status =["Loan_Repayment","loan_disbursed","loan_repayment","disbursed"]
            user_wallet_obj = UserWallet.objects.filter(user=user_id,status__in=status).exclude(status="initiated").order_by('-id')
            if user_wallet_obj:
                for user_wallet_data in user_wallet_obj:
                    order_details = ""
                    if user_wallet_data.order_details:
                        order_details = user_wallet_data.order_details.order_text
                    wallet_dict = {
                        "amount_add": user_wallet_data.amount_add,
                        "used_amount": user_wallet_data.used_amount,
                        "balance_amount": user_wallet_data.balance_amount,
                        "status": user_wallet_data.status,
                        "order_details": order_details,
                        "created_date": str(user_wallet_data.created_date)[:10]
                    }
                    wallet_info.append(wallet_dict)
                return Response(
                    {
                        "Status": "1",
                        "Wallet_Data": wallet_info
                    }
                )
            return Response(
                {
                    "Status": "9",
                    "Message": "User Not Found",
                    "Wallet_Data": wallet_info
                }
            )
        wallet_info = []
        return Response(
            {
                "Status": "9",
                "Message": "Invalid User",
                "Wallet_Data": wallet_info
            }
        )


class UserWalletAmountDetails(generics.ListAPIView):
    """
    This API give wallet amount, user personal details, wallet information and order from
    user wallet .
    """
    template_name = "Ecom/user_wallet_amount_details.html"

    def get(self, request, *args, **kwargs):
        ctx = {}
        data_dict = {}
        name = ""
        if "user_info" not in kwargs and "order_details_id" not in kwargs and "order_details_text" not in kwargs:
            user_list = UserWallet.objects.exclude(
                status__in=["initiated", "return_delivery_charge_initiated", "recharge_initiated"]).values_list(
                "user__username", flat=True)
            if user_list:
                for users in set(user_list):
                    personal_name = UserPersonal.objects.filter(user__username=users)
                    if personal_name:
                        name = personal_name[0].name
                    else:
                        name = ""
                    wallet_amount = 0
                    wallet_obj = UserWallet.objects.filter(user__username=users).exclude(
                        status__in=["initiated", "return_delivery_charge_initiated", "recharge_initiated"]).order_by(
                        'created_date').last()
                    if wallet_obj:
                        wallet_amount = wallet_obj.balance_amount
                    data_dict.update({users: [name, wallet_amount, ]})
                    ctx["wallet_amount"] = data_dict
        if "user_info" in kwargs:
            user = kwargs["user_info"]
            wallet_info = UserWallet.objects.filter(user__username=user).exclude(
                status__in=["initiated", "return_delivery_charge_initiated", "recharge_initiated"]).order_by(
                'created_date')
            user_personal = UserPersonal.objects.filter(user__username=user)
            if user_personal:
                ctx['user_personal'] = user_personal[0].name

            ctx['info'] = wallet_info

        if "order_details_id" in kwargs and "order_details_text" in kwargs:
            order_details_id = kwargs["order_details_id"]
            order_details_info = OrderDetails.objects.filter(id=order_details_id).values_list("order_id", flat=True)
            order = Order.objects.filter(id__in=order_details_info, status="cancelled")
            ctx['order'] = order

        return render(request, self.template_name, ctx)


def add_default_wallet_amount_script(request):
    """
    This script is return csv with create DefaultWalletAmount entry if it is not present in
    .DefaultWalletAmount.
    """
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="Default_wallet_amount.csv"'
    writer = csv.writer(response)
    writer.writerow(
        ['Username', 'amount', 'Status'])
    number_list = []
    for username in number_list:
        status = ""
        amount = 250  # Amount to be change
        type = "customer"
        get_default_wallet_obj = DefaultWalletAmount.objects.filter(mobile_no=username, amount=amount)
        if not get_default_wallet_obj:
            add_wallet = DefaultWalletAmount.objects.create(mobile_no=username,
                                                            amount=amount, type=type)
            if add_wallet:
                status = "default success"
        else:
            status = "already_given"

        writer.writerow([username, amount, status])
    return response


def add_default_wallet_amount(mobile_no):
    """
    This function is to add default wallet amount. The amount is get from DefaultWalletAmount table
    Using user_wallet_update the wallet amount is update.
    """
    if mobile_no:
        status = ""
        amount = 0
        get_amount = DefaultWalletAmount.objects.filter(mobile_no=mobile_no).order_by('-id')
        if get_amount:
            for amount_obj in get_amount:
                amount = amount_obj.amount
                if amount != 0:
                    get_user = User.objects.filter(username=mobile_no)
                    if get_user:
                        get_wallet_entry = UserWallet.objects.filter(user=get_user[0], amount_add=amount,
                                                                     status="default")
                        if not get_wallet_entry:
                            add_wallet = user_wallet_update(get_user[0].id, amount, None, None, "default")
                            if add_wallet:
                                status = True
                        else:
                            status = False
                    else:
                        status = False
                else:
                    status = False
        else:
            status = False
        return status


def add_wallet_balance_return_issue(request, unique_order_id, amount):
    """
    The return OrderDetails wallet amount is update in this function for this user_wallet_update
    is used.
    """
    if None not in (unique_order_id, amount):
        get_order_obj = Order.objects.filter(unique_order_id=unique_order_id).first()
        if get_order_obj:
            get_order_details_obj = OrderDetails.objects.filter(order_id=get_order_obj.id)
            if get_order_details_obj:
                status = "order_return"
                user_id = get_order_details_obj[0].user.id
                add_wallet = user_wallet_update(user_id, amount, get_order_details_obj[0], get_order_obj, status)

                if add_wallet:
                    return HttpResponse("Okay====================")

            else:
                return HttpResponse("Order details not found")

        else:
            return HttpResponse("Order not found")
