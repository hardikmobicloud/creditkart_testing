import datetime
from datetime import timedelta
from .auth import check_personal_info
from .views import *
from .models import *
from Equfix.views import *
from User.models import *
from django.apps.registry import apps
from django.contrib.auth.models import User
from django.db.models import Sum
from ecom.ecom_payments import check_user_defaulter_status
from rest_framework import generics
from rest_framework.authentication import *
from rest_framework.permissions import *
from rest_framework.response import Response
from django.shortcuts import render, HttpResponse, redirect
from math import ceil
from rest_framework.views import APIView

from .product import get_product_details_price

Category = apps.get_model('ecom', 'Category')
SubCategory = apps.get_model('ecom', 'SubCategory')
Colour = apps.get_model('ecom', 'Colour')
Size = apps.get_model('ecom', 'Size')
SGST = apps.get_model('ecom', 'SGST')
CGST = apps.get_model('ecom', 'CGST')
Product = apps.get_model('ecom', 'Product')
ProductDetails = apps.get_model('ecom', 'ProductDetails')
Seller = apps.get_model('ecom', 'Seller')
Credit = apps.get_model('ecom', 'Credit')
Stock = apps.get_model('ecom', 'Stock')
Order = apps.get_model('ecom', 'Order')
OrderDetails = apps.get_model('ecom', 'OrderDetails')
OrderDetailsOrder = apps.get_model('ecom', 'orderdetails_order_id')
Images = apps.get_model('ecom', 'Images')
EquiFaxMaster = apps.get_model('Equfix', 'EquiFaxMaster')
EquiFaxScoreDetails = apps.get_model('Equfix', 'EquiFaxScoreDetails')
EcomDocuments = apps.get_model('ecom', 'EcomDocuments')
Address = apps.get_model('User', 'Address')
DeliveryCharges = apps.get_model('ecom', 'DeliveryCharges')
Personal = apps.get_model('User', 'Personal')
OrderStatus = apps.get_model('ecom', 'OrderStatus')
ReturnOrder = apps.get_model('ecom', 'ReturnOrder')
SellerAmount = apps.get_model('ecom', 'SellerAmount')
SellerAccount = apps.get_model('ecom', 'SellerAccount')
ConvenientFees = apps.get_model('ecom', 'ConvenientFees')
UserWallet = apps.get_model('ecom', 'UserWallet')
LoanDefaulter = apps.get_model('ecom', 'LoanDefaulter')
Payment = apps.get_model('Payments', 'Payment')


def get_valid_user_equifax_data(user_id):
    today = datetime.datetime.today()
    exp_date = datetime.datetime.today() - datetime.timedelta(days=90)
    score = None
    """
    This function will return None in three cases.
    1. If We have equifax data not more than 90 days.
    2. If Equifax doesn't have data for this user.
    3. If Equifax master and new equifax data is same. 
    Otherwise it returns Score.
    """
    score_data = {}
    score_type = "default"
    score_detail = EquiFaxScoreDetails.objects.filter(equifax__user_id=user_id).order_by('id').last()
    if score_detail:
        last_updated_date = score_detail.date_update
        if last_updated_date < exp_date:
            # Score is before 90days
            get_score = get_equifax_score(user_id)
            if get_score:
                # Checking new get_score and previous score is not same..
                # checking new credit and old credit is same or not..
                score_data["score"] = get_score
                score_data["score_type"] = "fetched"

        else:
            # Score is not more than 90 days..
            # We have to add this score if only users data is not present in credit table..
            # get_last_credit = Credit.objects.filter(user=user_id)
            # if not get_last_credit:
            score = score_detail.score_value
            score_data["score"] = score
            score_data["score_type"] = score_type

    else:
        # This means user's data is not in Equifax table..
        # We have to check if we have gives default credit to user or not before this..
        get_score = get_equifax_score(user_id)
        if get_score:
            # Checking new get_score and previous score is not same..
            # checking new credit and old credit is same or not..

            score_data["score"] = get_score
            score_data["score_type"] = "fetched"

        else:
            # This means recently we have given him default credit..
            # So need not to change anything..
            score_data["score"] = 0
            score_data["score_type"] = score_type

    return score_data


def check_order_details_credit_status(order_details_id):
    if order_details_id:
        credit_status = "Initiated"
        order_details_obj = OrderDetails.objects.filter(id=order_details_id)
        if order_details_obj:
            from ecom.ecom_payments import get_order_details_used_credit
            credit = get_order_details_used_credit(order_details_id)
            # calculating order details total amount paid..
            if credit == 0:
                credit_status = "Failed"
            else:
                rep_amount = order_details_obj[0].payment.filter(product_type="Ecom", status='success',
                                                                 category="Repayment").aggregate(

                    Sum('amount'))['amount__sum']
                if rep_amount:
                    if 0 < rep_amount < credit:
                        credit_status = "Pay"
                    if rep_amount >= credit:
                        credit_status = "Paid"
            return credit_status


def credit_balance_update(user_id, amount, credit_status):
    """
    This function gets amount and user id and when repayments done by this user with particular amount,
    it adjust his credit by calculating the amount and create his new credit.
    """
    if user_id and user_id is not None:
        add_credit = None
        status = ""
        if credit_status == "repayment":
            status = "repayment"
        if credit_status == "return":
            status = "order_return"
        if credit_status == "cancel":
            status = "order_cancel"
        if credit_status == "undelivered":
            status = "order_undelivered"
        if credit_status == "recharge_success":
            status = "recharge_success"
        if credit_status == "recharge_failed":
            status = "recharge_failed"
        if credit_status == "refund":
            status = "refund"

        user = User.objects.filter(id=user_id)
        if user:
            defaulter_status = is_defaulter_status(user[0].username)
            if amount and amount is not None:
                credit_obj = Credit.objects.filter(user=user[0].id).exclude(
                    status__in=["initiated", "recharge_initiated"]).order_by('id').last()
                if credit_obj:
                    # last entry details
                    credit = credit_obj.credit
                    used_amount = credit_obj.used_credit
                    balance_credit = credit_obj.balance_credit
                    # changing credit entry according to new amount..
                    if status == "recharge_success":
                        new_used_amount = used_amount + float(str(amount))
                        actual_used = float(str(amount))
                        new_balance_credit = credit - new_used_amount
                        add_credit = Credit.objects.create(credit=credit, actual_used=actual_used,
                                                           used_credit=new_used_amount,
                                                           balance_credit=new_balance_credit,
                                                           user=user[0], status=status)

                    else:
                        update_credit = 0
                        update_balance_credit = 0
                        equfix_score_data = get_valid_user_equifax_data(user_id)
                        if equfix_score_data:
                            get_score = equfix_score_data["score"]
                            credit_limit = 0
                            if get_score:
                                new_credit = get_credit_score_for_checkout(get_score)  # get new credit score for user
                                if not new_credit:
                                    new_credit = 0

                            else:
                                new_credit = 0

                        else:
                            new_credit = 0

                        if credit_obj:
                            defaulter_obj = Credit.objects.filter(user=user[0], status="suspicious")
                            if defaulter_obj:
                                new_credit = 0

                            loan_entry = LoanApplication.objects.filter(user=user[0], status="APPROVED")
                            if loan_entry:
                                new_credit = 0

                            flag = False
                            credit = credit_obj.credit  # get credit got to user

                            used_credit = credit_obj.used_credit  # get used_credit of user
                            used_credit -= float(amount)

                            if used_credit > 0:
                                # if used credit is non negative

                                flag = True

                                if used_credit <= new_credit:
                                    # used_credit is less than new credit assign to user then user new credit will be new
                                    # credit got from equifax function and balance credit will be difference of new_credit
                                    # and used credit
                                    update_credit = new_credit
                                    update_balance_credit = update_credit - used_credit
                                    if defaulter_status:
                                        update_balance_credit = 0

                                else:
                                    # otherwise credit assign to user will be it's used credit
                                    update_credit = used_credit
                                    update_balance_credit = update_credit - update_credit
                                    if defaulter_status:
                                        update_balance_credit = 0

                            elif used_credit == 0:
                                # if user's used credit is 0 ,then new credit will be the credit assign to user
                                # and balance credit will be difference of new_credit and used credit

                                flag = True

                                update_credit = new_credit
                                update_balance_credit = update_credit - used_credit
                                if defaulter_status:
                                    update_balance_credit = 0

                            else:
                                flag = False

                            if flag:
                                add_credit = Credit.objects.create(credit=update_credit, used_credit=used_credit,
                                                                   balance_credit=update_balance_credit,
                                                                   repayment_add=amount,
                                                                   user_id=user_id, status=status)

                    if add_credit:
                        return add_credit
        return add_credit


def get_credit_score_for_checkout(equfix_score):
    credit = None
    new_equfix_score = equfix_score
    try:
        new_equfix_score = int(equfix_score)
    except Exception as e:
        print("Insde Exception if get_credit_score_for_checkout", e)

    if new_equfix_score == -1:
        credit = 500
    elif 1 <= new_equfix_score <= 6:
        credit = 500
    elif new_equfix_score >= 650:
        credit = 1000

    return credit


def user_credit_add(user_id):
    """
    This function Checks the exuifax data and add credit according to the score..
    status code - 1: 'credit Added'
                  2: 'Equifax data Expired'
                  3: 'No score details found'
                  4: 'No user Exuifax data available'
    """
    today = datetime.datetime.now()
    data = {}
    resp_total_credit = 0
    resp_used_credit = 0
    resp_balance_credit = 0
    user = User.objects.filter(id=user_id)
    credit = 0
    if user:
        # Checking Users contacts valid or not..
        from UserData.views import is_users_contacts_valid
        valid_contacts = is_users_contacts_valid(user[0].id)
        # valid_contacts = True
        if not valid_contacts:
            data = {
                "resp_total_credit": resp_total_credit,
                "resp_used_credit": resp_used_credit,
                "resp_balance_credit": resp_balance_credit
            }

            return data

        pan_card = None
        validate_pancard = False
        project_type = "CK"
        validate_pancard = True
        # Get users last entry
        get_last_credit = None
        status = None
        if validate_pancard:
            # Get equfix score
            equfix_score_data = get_valid_user_equifax_data(user[0].id)
            if equfix_score_data:
                equfix_score = equfix_score_data["score"]
                status = equfix_score_data["score_type"]
                # Checking slabs here..
                credit = get_credit_score_for_checkout(equfix_score)
                if credit:
                    get_last_credit = Credit.objects.filter(user=user[0]).exclude(status="initiated").order_by(
                        'id').last()

                else:
                    credit = 0
                    get_last_credit = Credit.objects.filter(user=user[0]).exclude(status="initiated").order_by(
                        'id').last()

            else:
                credit = 0
                get_last_credit = Credit.objects.filter(user=user[0]).exclude(status="initiated").order_by(
                    'id').last()

            if get_last_credit:
                update_credit = 0
                update_used_credit = 0
                update_balance_credit = 0

                if get_last_credit.status == "default" and get_last_credit.balance_credit == 0 and get_last_credit.used_credit == 0 and str(
                        get_last_credit.created_date)[:10] < "2021-02-26":
                    pass

                else:
                    defaulter_obj = Credit.objects.filter(user=user[0], status="suspicious")
                    if defaulter_obj:
                        credit = 0

                    loan_entry = LoanApplication.objects.filter(user=user[0], status="APPROVED")
                    if loan_entry:
                        credit = 0

                    last_used_credit = get_last_credit.used_credit

                    flag = False
                    # if used credit is 0 then credit will be new credit
                    if last_used_credit == 0:
                        update_credit = credit
                        update_used_credit = last_used_credit
                        update_balance_credit = credit
                        flag = True

                    # if used credit greater than 0
                    elif last_used_credit > 0:
                        # if used credit greater than 1000 then credit will be used credit
                        if last_used_credit >= 1000:
                            update_credit = last_used_credit
                            update_used_credit = last_used_credit
                            update_balance_credit = 0
                        else:
                            # if used credit less than 1000 and new credit is greater than used credit
                            # then credit will be new credit
                            if credit >= last_used_credit:
                                update_credit = credit
                                update_used_credit = last_used_credit
                                update_balance_credit = credit - last_used_credit
                            # if used credit greater than 1000 and new credit is less than used credit
                            # then credit will be used credit
                            else:
                                update_credit = last_used_credit
                                update_used_credit = last_used_credit
                                update_balance_credit = 0

                        flag = True

                    # It means credit is less than 0
                    else:
                        flag = False

                    if flag:

                        user_last_entry = Credit.objects.filter(user=user[0]).exclude(status="initiated").order_by(
                            'id').last()
                        if user_last_entry:
                            if user_last_entry.credit == update_credit and user_last_entry.used_credit == update_used_credit and user_last_entry.balance_credit == update_balance_credit and user_last_entry.status == status:
                                resp_total_credit = user_last_entry.credit
                                resp_used_credit = user_last_entry.used_credit
                                resp_balance_credit = user_last_entry.balance_credit

                            else:
                                credit_add = Credit.objects.create(credit=update_credit, used_credit=update_used_credit,
                                                                   balance_credit=update_balance_credit,
                                                                   user=user[0], status=status)
                                if credit_add:
                                    resp_total_credit = credit_add.credit
                                    resp_used_credit = credit_add.used_credit
                                    resp_balance_credit = credit_add.balance_credit

                        else:
                            credit_add = Credit.objects.create(credit=update_credit, used_credit=update_used_credit,
                                                               balance_credit=update_balance_credit,
                                                               user=user[0], status=status)
                            if credit_add:
                                resp_total_credit = credit_add.credit
                                resp_used_credit = credit_add.used_credit
                                resp_balance_credit = credit_add.balance_credit

            else:
                defaulter_status = is_defaulter_status(user[0].username)
                if not defaulter_status:
                    if credit and credit > 0:
                        credit_add = Credit.objects.create(credit=credit, used_credit=0,
                                                           balance_credit=credit,
                                                           user=user[0], status=status)
                        if credit_add:
                            resp_total_credit = credit_add.credit
                            resp_used_credit = credit_add.used_credit
                            resp_balance_credit = credit_add.balance_credit

        data = {
            "resp_total_credit": resp_total_credit,
            "resp_used_credit": resp_used_credit,
            "resp_balance_credit": resp_balance_credit
        }
        return data


def get_delivery_amount(product_details_list):
    """
    This function is to calculate Delivery charges of product.
    """
    total_delivery_charge = 0
    if product_details_list is not None:
        charge_type = "normal"
        total_amount = 0
        for product_details_obj in product_details_list:
            total_amount += product_details_obj.amount
            category = product_details_obj.product_id.category.name
            if category == "Grocery":
                charge_type = "Grocery"

        from EApi.order import get_delivery_charge
        delivery_charge = get_delivery_charge(total_amount, charge_type)
        if delivery_charge:
            total_delivery_charge = delivery_charge.amount

    return total_delivery_charge


class CheckoutCalculations(generics.ListAPIView):
    """
    This Api is to give All addresses of a user saved for Ecom delivery address.
    Url: ecomapi/user-ecom-address
    Authentication token required
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..TODO
    method = get
    parameter = {
                    'orders': [
                                {
                                    'product_details_id': 2,
                                    'quantity': 2
                                },
                                {
                                    'product_details_id': 3,
                                    'quantity': 1
                                }
                            ],
                    'user_id': 176
                }
    Response:
            {
                "status": "1",
                "address_list": [
                            {
                                "address_id": 35568,
                                "address": "Murungapakkam Near Government Hospital Backside"
                            },
                            {
                                "address_id": 35571,
                                "address": "Opp Punjab National Bank Sakinaka"
                            }
                        ],
            }
    Empty Response:
            {
                "status": "2",
                "address": [],
            }
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        today_date = datetime.datetime.now()
        order_data = request.data

        # generating unique Order ID here..
        orders = order_data['orders']
        user_id = request.user.id
        detail_amount = 0
        detail_total_amount = 0
        total_quantity = 0
        pay_amount = 0
        total_used_credit = 0

        # Checking users credit limit
        if orders is not None:
            product_details_list = []
            for order in orders:
                amount = 0
                product_details_id = order['id']
                quantity = order['quantity']
                total_quantity += quantity
                product_details_obj = ProductDetails.objects.filter(id=product_details_id,
                                                                    status="APPROVED")
                if product_details_obj:
                    product_details_list.append(product_details_obj[0])
                    amount = product_details_obj[0].amount
                    data = get_product_details_price(product_details_obj[0].id)
                    if data:
                        amount = data['minimum_amount']
                    detail_amount += amount
                    total_amount = quantity * amount
                    detail_total_amount += total_amount

            # Here we get the Credit data..
            last_total_credit = 0
            last_balance_credit = 0
            last_used_credit = 0
            data = user_credit_add(user_id)
            if data:
                last_total_credit = data['resp_total_credit']
                last_balance_credit = data['resp_balance_credit']
                last_used_credit = data['resp_used_credit']

            # Here we get Total Delivery amount..
            delivery_amount = get_delivery_amount(product_details_list)

            # Adding convenience fees..
            convenience_fees = 25
            get_fee = ConvenientFees.objects.filter(status="active").order_by('id').last()
            if get_fee:
                convenience_fees = get_fee.amount

            # Checking user's any payment is overdue or not..
            status = check_user_defaulter_status(user_id)
            if status == "False":
                status = 1
                # Here are the credit calculations
                payable_amount = detail_total_amount - last_balance_credit
                if payable_amount > 0:
                    pay_amount = payable_amount
                    used_credit = last_balance_credit
                else:
                    used_credit = detail_total_amount

                total_used_credit = used_credit
                balance_credit = last_balance_credit - total_used_credit

                return Response(
                    {
                        "status": 1,
                        "data": {
                            "message": "credit is used",
                            "used_credit": total_used_credit,
                            "available_credit": balance_credit,
                            "total_amount": detail_total_amount,
                            "payable_amount": pay_amount,
                            "delivery_charges": delivery_amount,
                            "convenience_fees": convenience_fees
                        }
                    }
                )

            if status == "True":
                status = 2
                # Here are the credit calculations
                pay_amount = detail_total_amount
                total_used_credit = 0
                balance_credit = last_balance_credit

                return Response(
                    {
                        "status": 2,
                        "data": {
                            "message": "your can't use your balance credit Rs. " + str(
                                balance_credit) + " because of pending payment",
                            "used_credit": total_used_credit,
                            "available_credit": balance_credit,
                            "total_amount": detail_total_amount,
                            "payable_amount": pay_amount,
                            "delivery_charges": delivery_amount,
                            "convenience_fees": convenience_fees
                        }
                    }
                )
        else:
            return Response(
                {
                    "status": 3,
                    "message": "something went wrong"
                }
            )


class CheckoutCalculationsNew(generics.ListAPIView):
    """
    This Api is to give All addresses of a user saved for Ecom delivery address.
    Url: ecomapi/user-ecom-address
    Authentication token required
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..
    method = get
    parameter = {
                    'orders': [
                                {
                                    'product_details_id': 2,
                                    'quantity': 2
                                },
                                {
                                    'product_details_id': 3,
                                    'quantity': 1
                                }
                            ],
                    'user_id': 176
                }
    Response:
            {
                "status": "1",
                "address_list": [
                            {
                                "address_id": 35568,
                                "address": "Murungapakkam Near Government Hospital Backside"
                            },
                            {
                                "address_id": 35571,
                                "address": "Opp Punjab National Bank Sakinaka"
                            }
                        ],
            }
    Empty Response:
            {
                "status": "2",
                "address": [],
            }
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        today_date = datetime.datetime.now()
        order_data = request.data

        # generating unique Order ID here..
        orders = order_data['orders']
        payment_mode = request.data.get("payment_mode")
        platform = False
        if "platform" in request.data:
            platform = True
        user_id = request.user.id
        detail_amount = 0
        detail_total_amount = 0
        detail_grocery_amount = 0
        total_quantity = 0
        pay_amount = 0
        used_credit = 0
        used_wallet = 0
        wallet_amount = 0
        convenience_fees = 0
        delivery_amount = 0
        total_used_credit = 0
        balance_credit = 0
        address_available = "False"

        # Checking users credit limit
        if orders is not None:
            for order in orders:
                amount = 0
                product_details_id = order['id']
                quantity = order['quantity']
                total_quantity += quantity
                product_details_obj = ProductDetails.objects.filter(id=product_details_id,
                                                                    status="APPROVED")
                if product_details_obj:
                    category = None
                    charge_type = "normal"
                    amount = product_details_obj[0].amount
                    try:
                        if product_details_obj[0].product_id:
                            category = product_details_obj[0].product_id.base_category.sub_category.category.name
                    except Exception as e:
                        print("inside Exception of get category in CheckoutCalculationsNew")
                    data = get_product_details_price(product_details_obj[0].id)
                    if data:
                        amount = data['minimum_amount']
                    detail_amount += amount
                    total_amount = quantity * amount
                    detail_total_amount += total_amount
                    if category == "Grocery":
                        detail_grocery_amount += total_amount
                        charge_type = "Grocery"

                    # Here we get Total Delivery amount..
                    from EApi.order import get_delivery_charge
                    delivery_obj = get_delivery_charge(total_amount, charge_type)
                    if delivery_obj:
                        delivery_amount += delivery_obj.amount

                    # Adding convenient fees..
                    convenient_entry = None
                    # Get orders convenient fees..
                    from EApi.order import get_orders_convenient_fees
                    order_convenience_fees = get_orders_convenient_fees(total_amount)
                    if order_convenience_fees != 0:
                        convenience_fees += order_convenience_fees

            if 0 < detail_grocery_amount < 500:
                return Response(
                    {
                        "status": 6,
                        "message": "Minimum order value for Grocery is Rs 500"
                    }
                )

            # Adding convenience fees..

            # Checking Wallet amount is available for this user or not..
            wallet_dict = get_user_wallet_amount(user_id, platform)
            if wallet_dict:
                wallet_amount = wallet_dict["balance_amount"]

            if payment_mode == "credit":
                # Checking all the personal information of user available or not..
                check_address_availability = check_personal_info(user_id, payment_mode)
                if check_address_availability:
                    address_available = "True"
                else:
                    address_available = "False"
                    return Response(
                        {
                            "status": 1,
                            "data": {
                                "message": "Address not available",
                                "used_credit": total_used_credit,
                                "available_credit": balance_credit,
                                "total_amount": detail_total_amount,
                                "payable_amount": pay_amount,
                                "used_wallet": used_wallet,
                                "delivery_charges": delivery_amount,
                                "convenience_fees": convenience_fees,
                                "address_available": address_available
                            }
                        }
                    )

                # Here we get the Credit data..
                last_total_credit = 0
                last_balance_credit = 0
                last_used_credit = 0
                data = user_credit_add(user_id)
                if data:
                    last_total_credit = data['resp_total_credit']
                    last_balance_credit = data['resp_balance_credit']
                    last_used_credit = data['resp_used_credit']

                # Checking user's any payment is overdue or not..
                status = check_user_defaulter_status(user_id)
                if status == "False":
                    status = 1
                    # Here are the credit calculations
                    payable_amount = detail_total_amount - last_balance_credit
                    if payable_amount > 0:
                        total_used_credit = last_balance_credit
                        # Total payable amount is remaining payable amount and delivery and convenient fees..
                        payable_amount = payable_amount + delivery_amount + convenience_fees

                        if wallet_amount != 0:
                            # User has some amount in his wallet..
                            if wallet_amount >= payable_amount:
                                # This means all payable amount is used from wallet balance no need to pay any amount..
                                used_wallet = payable_amount
                                pay_amount = 0  # No need to pay anything..
                            else:
                                # Total payable amount is remaining payable amount and delivery and convenient fees..
                                pay_amount = payable_amount - wallet_amount
                                used_wallet = wallet_amount
                        else:
                            # This means User have 0 amount in his wallet..
                            # So all amount should have to pay..
                            pay_amount = payable_amount
                    else:
                        # This is where total amount is deducted from credit..
                        # For Delivery charge and convenient fees need to check users wallet..
                        extra_fees = delivery_amount + convenience_fees

                        if wallet_amount and wallet_amount != 0:
                            if wallet_amount >= extra_fees:
                                pay_amount = 0
                                used_wallet = extra_fees
                            else:
                                pay_amount = extra_fees - wallet_amount
                                used_wallet = wallet_amount
                        else:
                            pay_amount = extra_fees

                        total_used_credit = detail_total_amount

                    if total_used_credit > 0:
                        message = "credit is used"
                    else:
                        message = "credit is not used"
                    balance_credit = last_balance_credit - total_used_credit

                    return Response(
                        {
                            "status": 1,
                            "data": {
                                "message": message,
                                "used_credit": total_used_credit,
                                "available_credit": balance_credit,
                                "total_amount": detail_total_amount,
                                "payable_amount": pay_amount,
                                "used_wallet": used_wallet,
                                "delivery_charges": delivery_amount,
                                "convenience_fees": convenience_fees,
                                "address_available": address_available
                            }
                        }
                    )

                if status == "True":
                    # This means USer have its payment overdue..
                    # So he can not use his credit but can use his wallet amount if available..
                    status = 2
                    # Here are the credit calculations
                    detail_total_amount = detail_total_amount + delivery_amount + convenience_fees

                    if wallet_amount and wallet_amount != 0:
                        if wallet_amount >= detail_total_amount:
                            pay_amount = 0
                            used_wallet = detail_total_amount
                        else:
                            pay_amount = detail_total_amount - wallet_amount
                            used_wallet = wallet_amount

                    else:
                        pay_amount = detail_total_amount

                    total_used_credit = 0
                    balance_credit = last_balance_credit

                    return Response(
                        {
                            "status": 2,
                            "data": {
                                "message": "your can't use your balance credit Rs. " + str(
                                    balance_credit) + " because of pending payment",
                                "used_credit": total_used_credit,
                                "available_credit": balance_credit,
                                "total_amount": detail_total_amount,
                                "payable_amount": pay_amount,
                                "used_wallet": used_wallet,
                                "delivery_charges": delivery_amount,
                                "convenience_fees": convenience_fees,
                                "address_available": address_available
                            }
                        }
                    )

            if payment_mode == "cash":
                # Checking all the personal information of user available or not..
                check_address_availability = check_personal_info(user_id, payment_mode)
                if check_address_availability:
                    address_available = "True"
                else:
                    address_available = "False"

                # Here are the credit calculations
                showable_total_amount = detail_total_amount
                detail_total_amount = detail_total_amount + delivery_amount + convenience_fees
                if wallet_amount and wallet_amount != 0:
                    if wallet_amount >= detail_total_amount:
                        pay_amount = 0
                        used_wallet = detail_total_amount
                    else:
                        pay_amount = detail_total_amount - wallet_amount
                        used_wallet = wallet_amount
                else:
                    pay_amount = detail_total_amount

                return Response(
                    {
                        "status": 3,
                        "data": {
                            "message": "success",
                            "used_credit": total_used_credit,
                            "available_credit": balance_credit,
                            "total_amount": showable_total_amount,
                            "payable_amount": pay_amount,
                            "used_wallet": used_wallet,
                            "delivery_charges": delivery_amount,
                            "convenience_fees": convenience_fees,
                            "address_available": address_available
                        }
                    }
                )

            if payment_mode == "cod":
                # Checking all the personal information of user available or not..
                check_address_availability = check_personal_info(user_id, payment_mode)
                if check_address_availability:
                    address_available = "True"
                else:
                    address_available = "False"

                # Here are the credit calculations
                showable_total_amount = detail_total_amount
                pay_amount = detail_total_amount + delivery_amount + convenience_fees

                return Response(
                    {
                        "status": 5,
                        "data": {
                            "message": "success",
                            "used_credit": total_used_credit,
                            "available_credit": balance_credit,
                            "total_amount": showable_total_amount,
                            "payable_amount": pay_amount,
                            "used_wallet": used_wallet,
                            "delivery_charges": delivery_amount,
                            "convenience_fees": convenience_fees,
                            "address_available": address_available
                        }
                    }
                )

        else:
            return Response(
                {
                    "status": 4,
                    "message": "something went wrong"
                }
            )


class UserCreditDetails(generics.ListAPIView):
    """
    This Api is to show user credit details
    Url: ecomapi/user_credit_details
    Authentication token required
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..
    method = get
    parameter = {
                    'user_id': 1
                }
    Response:
                    {
            "Status": 1,
            "Data": {
                "User": "Seller",
                "Credit Data": [
                    {
                        "Credit": 1000.0,
                        "Used Credit": 800.0,
                        "Balance Credit": 200.0,
                        "Actual Used": 800.0,
                        "Repayment Add": 0.0,
                        "Status": "success",
                        "Created Date": "2020-05-12T16:10:52.029545",
                        "Updated Date": "2020-05-12T16:10:52.029545"
                    },
                    {
                        "Credit": null,
                        "Used Credit": null,
                        "Balance Credit": null,
                        "Actual Used": 0.0,
                        "Repayment Add": 1500.0,
                        "Status": "Repayment",
                        "Created Date": "2020-06-19T00:00:00",
                        "Updated Date": "2020-06-09T00:00:00"
                    }
                ]
            }
        }
             """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        credit_list = []
        user_id = request.user.id
        user = User.objects.filter(id=user_id)
        if user:
            credit_obj = Credit.objects.filter(user=user[0]).exclude(
                status__in=["initiated", "recharge_initiated"]).order_by('-id')
            if credit_obj:
                for credit in credit_obj:
                    credit_data = {
                        'Credit': credit.credit,
                        'Used_Credit': credit.used_credit,
                        'Balance_Credit': credit.balance_credit,
                        'Actual_Used': credit.actual_used,
                        'Repayment_Add': credit.repayment_add,
                        'Status': credit.status,
                        'Created_Date': str(credit.created_date)[:19],
                    }
                    credit_list.append(credit_data)
                return Response(
                    {
                        'Status': 1,
                        'massage': "Success",
                        'Data': credit_list
                    }
                )
            else:
                return Response(
                    {
                        'Status': 2,
                        'massage': "credit is not used yet",
                        'Data': credit_list
                    }
                )

        return Response(
            {
                'Status': 3,
                'message': "user is invalid",
                'Data': credit_list
            }
        )


def get_order_details_return_cancel_amount(order_details_id):
    """
    This function will return total order cancel and return amount and total repayment amount paid for that order details..
    """
    amount_dict = {}
    total_cancel_return_amount = 0
    if order_details_id is not None:
        get_order_details_obj = OrderDetails.objects.filter(id=order_details_id)
        if get_order_details_obj:
            total_amount = get_order_details_obj[0].credit_history.filter(
                status__in=["order_cancel", "order_return", "order_undelivered"]).aggregate(

                Sum('repayment_add'))['repayment_add__sum']
            if total_amount:
                total_cancel_return_amount = total_amount

    return total_cancel_return_amount


class PaymentModeSelectionApi(generics.ListAPIView):
    """
    This Api is to show credit details and cash details
    Url: ecomapi/
    Authentication token required
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..
    method = post
    parameter = {
                    'amount': 100
                }
    Response:
                        {
            "Status": 1,
            "Data": {
                "cash": {
                    "cash_amount": 20.0
                },
                "credit": {
                    "2020-09-24 10:31:21.813713": 34,
                    "2020-10-24 10:31:21.813713": 33,
                    "2020-11-23 10:31:21.813713": 33
                }
            }
        }
             """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        cash_dict = {}
        cod_dict = {}
        credit_dict = {}
        credit_list = []
        user_id = request.user.id
        cash_amount = 0
        total_amount = int(request.data.get('amount'))
        today_date = str(datetime.datetime.now())[:10]
        platform = False
        if "platform" in request.data:
            platform = True
        # from lead.views import is_cod_customer
        cod_amount = None
        # queryset = Payment.objects.filter()[:-10]
        # print(queryset, 'queryset for payments')
        cod_customer = True  # is_cod_customer(user_id)
        if cod_customer:
            cod_dict.update({'cod_amount': total_amount, 'description': "", "payment_mode": "cod"})

        from EApi.EChecksum_web import get_big_offer_status, get_big_offer_status_new
        is_big_offer = get_big_offer_status(today_date)
        if is_big_offer:
            balance_amount = 0
            wallet_data = get_user_wallet_amount(user_id, platform)
            if wallet_data:
                balance_amount = wallet_data["balance_amount"]

            if balance_amount < total_amount:
                cash_amount = total_amount - balance_amount

        is_big_offer_new = get_big_offer_status_new(today_date)
        if is_big_offer_new:
            balance_amount = 0
            wallet_data = get_user_wallet_amount(user_id)
            if wallet_data:
                balance_amount = wallet_data["balance_amount"]

            if balance_amount < total_amount:
                get_cash_amount = total_amount - balance_amount
                if get_cash_amount > 0:
                    cash_amount = ceil((get_cash_amount * 50) / 100)

        else:
            from EApi.EChecksum_web import get_cashback_amount
            cash_amount = get_cashback_amount(total_amount)

        cash_dict.update({'cashback': cash_amount, 'description': "", "payment_mode": "cash"})

        # Checking user defaulter or not..
        is_defaulter = check_user_defaulter_status(user_id)
        if is_defaulter == "False":
            credit = credit_slabs(total_amount)
            if credit:
                for date, amount in credit.items():
                    from MApi.loan import convert_date_into_readable_format
                    date = convert_date_into_readable_format(date)
                    credit_dict = {'date': date, 'amount': amount, 'description': ""}
                    credit_list.append(credit_dict)
        else:
            is_defaulter = "False"

        return Response(
            {
                'Status': 1,
                'credit_status': is_defaulter,
                'total_amount': total_amount,
                'cod_amount': cod_dict,
                'pay_now': cash_dict,
                'credit': credit_list
            }
        )


def credit_slabs(amount):
    """
    This function takes order_details_id and gives slabs of installments according
    to amount used for this order..
    """
    slabs_dict = {}
    if amount:
        today_date = datetime.datetime.now()
        first_installment_date = today_date + datetime.timedelta(days=30)
        second_installment_date = today_date + datetime.timedelta(days=60)
        third_installment_date = today_date + datetime.timedelta(days=90)
        dates = [first_installment_date, second_installment_date, third_installment_date]
        if amount != 0:
            parts = 3
            equal_installments = [amount // parts + (1 if x < amount % parts else 0) for x in range(parts)]
            slabs_dict = dict(zip(dates, equal_installments))
            return slabs_dict
        else:
            return slabs_dict


def get_user_wallet_amount(user_id, platform=False):
    """
    description = This function returns users last available wallet balance..
    """
    wallet_dict = {}
    balance_amount = 0
    wallet_obj = None
    if not platform:
        if user_id is not None:
            get_wallet_obj = UserWallet.objects.filter(user=user_id).exclude(
                status__in=["initiated", "return_delivery_charge_initiated", "recharge_initiated"]).order_by(
                'id').last()
            if get_wallet_obj:
                balance_amount = get_wallet_obj.balance_amount
                wallet_obj = get_wallet_obj
    wallet_dict["balance_amount"] = balance_amount
    wallet_dict["wallet_obj"] = wallet_obj
    return wallet_dict


def check_defaulter_status_mk(mobile_number):
    """
    description = This function check user defaulter status by its mobile number for mk.
    """
    import requests
    url = "https://admin.anandfin.com/api/is_defaulter_api/"
    data = {"username": mobile_number}
    a = requests.get(url, data=data)
    response = a.json()
    if response:
        if "status" in response:
            defaulter_status = response["status"]
            if str(defaulter_status) == "1":
                return True
            else:
                return False
    return False


def check_defaulter_status_rk(mobile_number):
    """
        description = This function check user defaulter status by its mobile number for rk.
    """
    import requests
    url = "https://admin.thecreditkart.com/api/is_defaulter_api/"
    data = {"username": mobile_number}
    a = requests.get(url, data=data)
    response = a.json()
    if response:
        if "status" in response:
            defaulter_status = response["status"]
            if str(defaulter_status) == "1":
                return True
            else:
                return False
    return False


def check_defaulter_status_insta(mobile_number):
    """
        description = This function check user defaulter status by its mobile number for rk.
    """
    import requests
    url = "https://admin.instarupee.co.in/api/is_defaulter_api/"
    data = {"username": mobile_number}
    a = requests.get(url, data=data)
    response = a.json()
    if response:
        if "status" in response:
            defaulter_status = response["status"]
            if str(defaulter_status) == "1":
                return True
            else:
                return False
    return False

def attach_credit_to_orderdetails(request, payment_order_id, amount):
    """
    description = This function is scripted..
    """
    if None not in (payment_order_id, amount):
        user_id = None
        order_data = str(payment_order_id).split("e")
        order_text = ''
        if order_data:
            order_text = order_data[0]
        get_order_details_obj = OrderDetails.objects.filter(order_text=order_text).first()
        if get_order_details_obj:
            user_id = get_order_details_obj.user.id
            if user_id:
                # Update users credit..
                status = "repayment"
                update = credit_balance_update(user_id, amount, status)
                if update:
                    get_order_details_obj.credit_history.add(update)

            get_payment_obj = Payment.objects.filter(order_id=payment_order_id).first()
            if get_payment_obj:
                get_order_details_obj.payment.add(get_payment_obj)

            return HttpResponse("Done")

    return HttpResponse("Not Done")


def attach_loan_bank_update_repayment(request, order_id, loan_status="APPROVED"):
    get_payment = Payment.objects.filter(order_id=order_id, status="success", category="Loan_Repayment", type="loan",
                                         product_type="Ecom").order_by('id').last()
    if get_payment:
        if "l" in order_id:
            loan_list = order_id.split('l')
            if loan_list:
                loan_id = loan_list[0]

                if loan_id is not None:
                    get_loan = LoanApplication.objects.filter(loan_application_id=loan_id)
                    if get_loan:
                        if loan_status in ["COMPLETED", "APPROVED"]:
                            LoanApplication.objects.filter(id=get_loan[0].id).update(status=loan_status,
                                                                                     updated_date=datetime.datetime.now())

                            get_loan[0].repayment_amount.add(get_payment)
                            return HttpResponse("Bank update Successfully..")
                        else:
                            return HttpResponse("Invalid loan status..")
                    else:
                        return HttpResponse("Loan ID is Invalid..")
        else:
            return HttpResponse("You have entered invalid order id.. It must contain Loan ID + 'l + random number'")
    else:
        return HttpResponse("Payment not found.. Please make sure that you have entered correct payment details.."
                            "status=success, category=Loan_Repayment, type=loan, product_type=Ecom")


def validate_pancard_in_applications(pan_card, project_type):
    """
    description = This function validate pancard
    """
    import json
    if None not in (pan_card, project_type):
        dict_param = {
            'pan_card': pan_card,
            'app': project_type,
            'username': "creditkartpan",
            'password': "credit@123",
        }
        json_body = json.dumps(dict_param)
        # TODO url of API
        prod_url = 'http://15.206.113.44:8003/api/pancard-validate-status/'
        # prod_url = 'https://mudrakwik.anandfin.com/api/pancard-validate-status/'

        # prod_url = "https://app.thecreditkart.com/api/pancard-validate-creditkart/"
        headers = {
            'content-type': "application/json",
            'cache-control': "no-cache",
        }
        response = requests.post(prod_url, data=json_body, headers=headers, verify=False)
        if response:
            response = response.json()
            app_type = None
            if "app" in response:
                app_type = response["app"]

            if app_type != "CK":
                return True

            elif app_type == "ALL":
                return False

            else:
                return False

        else:
            return False


def limit_repayment_credit(user_id, amount):
    """
    description = this function checks the credit history of user and limit the credit assign to the user

    parameters :
                user_id = primary kry of User
                amount = amount of repayment (int)

    return :
            new credit object

    """

    add_credit = None
    repayment_add = 0
    update_credit = 0
    used_credit = 0
    update_balance_credit = 0
    user_credit_obj = None
    new_credit = 0

    if user_id and user_id is not None:
        get_score = get_equifax_score(user_id)

        credit_limit = 0

        if get_score:
            new_credit = get_credit_score_for_checkout(get_score)  # get new credit score for user
            if new_credit and new_credit is not None:
                user_credit_obj = Credit.objects.exclude(status="initiated").filter(
                    user__id=user_id).last()  # check last credit entry

            else:
                new_credit = 0

        else:
            new_credit = 0

        if user_credit_obj:
            flag = False
            credit = user_credit_obj.credit  # get credit got to user

            used_credit = user_credit_obj.used_credit  # get used_credit of user
            used_credit -= amount

            if used_credit > 0:
                # if used credit is non negative

                flag = True

                if used_credit <= new_credit:
                    # used_credit is less than new credit assign to user then user new credit will be new
                    # credit got from equifax function and balance credit will be difference of new_credit
                    # and used credit
                    update_credit = new_credit
                    update_balance_credit = update_credit - used_credit

                else:
                    # otherwise credit assign to user will be it's used credit
                    update_credit = used_credit
                    update_balance_credit = update_credit - update_credit

            elif used_credit == 0:
                # if user's used credit is 0 ,then new credit will be the credit assign to user
                # and balance credit will be difference of new_credit and used credit

                flag = True

                update_credit = new_credit
                update_balance_credit = update_credit - used_credit
            else:
                flag = False

            if flag:
                add_credit = Credit.objects.create(credit=update_credit, used_credit=used_credit,
                                                   balance_credit=update_balance_credit, repayment_add=amount,
                                                   user_id=user_id, status="repayment")

    return add_credit

def is_defaulter_status(username):
    print('from EApi.credit import is_defaulter_status called 1497')
    count = 0
    amt = 0
    rep_amt = 0
    data = {}
    try:
        today = datetime.datetime.now()
        get_loan = LoanApplication.objects.filter(status='APPROVED', user__username=username).first()
        if get_loan:
            if get_loan.loan_end_date:
                return_date = get_loan.loan_end_date + timedelta(int(90))
                if today > return_date:
                    rep_amount = get_loan.repayment_amount.filter(
                        status='success', category='Loan').aggregate(Sum('amount'))['amount__sum']
                    if rep_amount is not None:
                        rep_amt += rep_amount
                    amt += int(get_loan.loan_scheme_id)
                    count += 1
                    return True
        return False

    except Exception as e:
        import sys
        import os
        print('-----------in exception----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        return False

class CodCheckStatus(APIView):
    authentication_classes = (TokenAuthentication,)

    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        cod = 0
        credit_status = 0
        message = None
        user_id = request.user.id
        from ecom.views import get_order_details_remaining_amount
        repaymet_dict = get_order_details_remaining_amount(user_id)
        if repaymet_dict:
            for key, value in repaymet_dict.items():
                if key == "Amount":
                    TXN_AMOUNT = value
                    if TXN_AMOUNT > 0:
                        credit_status = 1

        orderdetails = OrderDetails.objects.filter(user_id=user_id, payment_mode="cod").exclude(
            status='initiated').first()

        if orderdetails:
            order = orderdetails.order_id.filter(status="undelivered")
            if order:
                cod = 1

        return Response(
            {

                "credit_status": credit_status,
                "cod_status": cod

            }
        )

