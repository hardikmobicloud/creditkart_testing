from User.models import *
from django.apps import apps
import random
from django.db.models import Q
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authentication import *
from rest_framework.permissions import *

Category = apps.get_model('ecom', 'Category')
SubCategory = apps.get_model('ecom', 'SubCategory')
BaseCategory = apps.get_model('ecom', 'BaseCategory')
Colour = apps.get_model('ecom', 'Colour')
Size = apps.get_model('ecom', 'Size')
SGST = apps.get_model('ecom', 'SGST')
CGST = apps.get_model('ecom', 'CGST')
Product = apps.get_model('ecom', 'Product')
ProductDetails = apps.get_model('ecom', 'ProductDetails')
Seller = apps.get_model('ecom', 'Seller')
Credit = apps.get_model('ecom', 'Credit')
Stock = apps.get_model('ecom', 'Stock')
Order = apps.get_model('ecom', 'Order')
OrderDetails = apps.get_model('ecom', 'OrderDetails')
OrderDetailsOrder = apps.get_model('ecom', 'orderdetails_order_id')
Images = apps.get_model('ecom', 'Images')
EcomDocuments = apps.get_model('ecom', 'EcomDocuments')
ProductDetailsAmount = apps.get_model('ecom', 'ProductDetailsAmount')
ProductMinMaxAMount = apps.get_model('ecom', 'ProductMinMaxAMount')
ProductPriority = apps.get_model('ecom', 'ProductPriority')
Banner = apps.get_model('ecom', 'Banner')
OfferBanner = apps.get_model('ecom', 'OfferBanner')
UserOffer = apps.get_model('ecom', 'UserOffer')
Offer = apps.get_model('ecom', 'Offer')
ProductRating = apps.get_model('ecom', 'ProductRating')
ProductMinMaxDiscount = apps.get_model('ecom', 'ProductMinMaxDiscount')

from ecom.views import image_thumbnail

product_url = "https://thecreditkart.com/product/"


class ProductDetailsApi(generics.ListAPIView):
    """
    This Api is to show particular products details.

    Url: ecomapi/product_details_api

    Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]

    method = post
    parameters:
        {
            'product_id': 44
        }

    Response:
            {
                "status": 1,
                "data": {
                    "description": [
                        {
                            "head": "description for Jaiswal One Side Handbag For Women (Black)",
                            "info": ""
                        }
                    ],
                    "specification": [
                        {
                            "head": "size",
                            "info": "Medium"
                        },
                        {
                            "head": "colour",
                            "info": "Yellow"
                        },
                        {
                            "head": "length",
                            "info": ""
                        },
                        {
                            "head": "height",
                            "info": ""
                        },
                        {
                            "head": "width",
                            "info": ""
                        },
                        {
                            "head": "weight",
                            "info": 200.0
                        }
                    ]
                }
            }
    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        product_id = request.data.get('product_id')
        data_dict = {}
        description_list = []
        specification_list = []
        get_product_obj = Product.objects.filter(id=product_id)
        if get_product_obj:
            data_dict = get_minimum_amount_with_details_id(get_product_obj[0].id)
            if data_dict:
                product_details_id = data_dict['product_details_id']
                if product_details_id != "":
                    product = ProductDetails.objects.filter(id=product_details_id)
                    if product:
                        description = product[0].description
                        if description is None:
                            description = ""
                        description_list = [
                            {
                                "head": "description for " + product[0].product_id.name,
                                "info": description

                            }
                        ]

                        specification_list = [
                            {
                                "head": "size",
                                "info": product[0].size_id.name
                            },
                            {
                                "head": "colour",
                                "info": product[0].colour_id.name
                            },
                            {
                                "head": "length",
                                "info": product[0].Length
                            },
                            {
                                "head": "height",
                                "info": product[0].height
                            },
                            {
                                "head": "width",
                                "info": product[0].width
                            },
                            {
                                "head": "weight",
                                "info": product[0].weight
                            }
                        ]

                        data_dict = {"description": description_list, "specification": specification_list}
                        return Response(
                            {
                                'status': 1,
                                'data': data_dict
                            }
                        )
        else:
            return Response(
                {
                    'status': 2,
                    'data': data_dict
                }
            )


def get_product_details_unit_combination(product_details_obj):
    """
    params: product_details_obj
    description :This function returns unit combination of unit and unit count of the product details
    """
    unit_combination = ""
    if product_details_obj:
        unit_count = None
        unit = None
        if product_details_obj.unit_count:
            unit_count = product_details_obj.unit_count
        if product_details_obj.unit:
            unit = product_details_obj.unit.name

        if None not in (unit_count, unit):
            unit_combination = unit_count + unit

    return unit_combination


def get_minimum_amount(product_id):
    # This function is to get minimum amount for a product..
    """
      params: product_id
      description :This function data dictionary containing minimum amount,mrp and discount from the product min max amount.
    """
    minimum_amount = 0
    mrp = 0
    discount = 0
    get_min_max_obj = ProductMinMaxAMount.objects.filter(product_id=product_id, re_status=True)
    if get_min_max_obj:
        minimum_amount = get_min_max_obj[0].min_amount
        if get_min_max_obj[0].min_product_details:
            mrp = get_min_max_obj[0].min_product_details.mrp
            discount = get_min_max_obj[0].min_product_details.discount
    else:
        get_product_details_objects = ProductDetails.objects.filter(product_id=product_id, status="APPROVED")
        if get_product_details_objects:
            product_details_amount_obj = ProductDetailsAmount.objects.filter(
                product_details__in=get_product_details_objects,
                product_details__status="APPROVED").order_by('product_details', '-id').distinct('product_details')
            if product_details_amount_obj:
                min_amount_obj = ProductDetailsAmount.objects.filter(id__in=product_details_amount_obj).order_by(
                    'admin_amount').first()
                if min_amount_obj:
                    minimum_amount = min_amount_obj.admin_amount
                    mrp = min_amount_obj.mrp
                    discount = min_amount_obj.discount

    data_dict = {
        "minimum_amount": minimum_amount,
        "mrp": mrp,
        "discount": discount,
    }
    return data_dict


def get_minimum_amount_with_details_id(product_id):
    """
      params: product_id
      description :This function data dictionary containing minimum amount,product_details_id,mrp and discount from the product details amount.
    """
    # This function is to get minimum amount for a product and the product details ID for which minimum ID is selected
    get_product_details_objects = ProductDetails.objects.filter(product_id=product_id, status="APPROVED")
    minimum_amount = 0
    mrp = 0
    discount = 0
    data_dict = {}
    if get_product_details_objects:
        product_details_amount_obj = ProductDetailsAmount.objects.filter(
            product_details__in=get_product_details_objects,
            product_details__status="APPROVED").order_by('product_details', '-id').distinct('product_details')
        if product_details_amount_obj:
            min_amount = ProductDetailsAmount.objects.filter(id__in=product_details_amount_obj).order_by(
                'admin_amount').first()
            if min_amount:
                data_dict = {
                    "minimum_amount": min_amount.admin_amount,
                    "product_details_id": min_amount.product_details.id,
                    "mrp": min_amount.mrp,
                    "discount": min_amount.discount,

                }
        else:
            data_dict = {
                "minimum_amount": minimum_amount,
                "product_details_id": None,
                "mrp": mrp,
                "discount": discount
            }

    return data_dict


def get_product_variants(product_id):
    # This function is to get all product details images..
    """
      params: product_id
      description :This function return variants_list with id and thumbnail from the image.
    """
    variants_list = []
    product_details_objects = ProductDetails.objects.filter(product_id=product_id, status="APPROVED")
    if product_details_objects:
        for product_details_obj in product_details_objects:
            image_obj = Images.objects.filter(type="other", product_details=product_details_obj.id).order_by('id')
            if image_obj:
                image = image_thumbnail(image_obj[0].path)
                var_dict = {
                    "id": str(product_details_obj.id),
                    "thumbnail": str(image)
                }
                variants_list.append(var_dict)

            else:
                image = Images.objects.filter(type="main", product=product_id).order_by('id').last()
                if image:
                    image = image_thumbnail(image.path)
                    var_dict = {
                        "id": str(product_details_obj.id),
                        "thumbnail": str(image)
                    }
                    variants_list.append(var_dict)

    return variants_list


def get_product_details_variants(product_details_id):
    """
      params: product_details_id
      description :This function return variants_list with id and thumbnail from the image.
    """
    variants_list = []
    product_details_id = product_details_id
    if product_details_id is not None:
        image_objects = Images.objects.filter(type="other", product_details__status="APPROVED",
                                              product_details=product_details_id).order_by('id')
        if image_objects:
            for image in image_objects:
                image = image_thumbnail(image.path)
                var_dict = {
                    "id": str(product_details_id),
                    "thumbnail": str(image)
                }
                variants_list.append(var_dict)
        else:
            product_details_obj = ProductDetails.objects.filter(id=product_details_id)
            if product_details_obj:
                image = Images.objects.filter(type="main",
                                              product=product_details_obj[0].product_id).order_by('id').last()
                if image:
                    image = image_thumbnail(image.path)
                    var_dict = {
                        "id": str(product_details_id),
                        "thumbnail": str(image)
                    }
                    variants_list.append(var_dict)
    return variants_list


def get_product_details_price(product_details_id):
    """
         params: product_details_id
         description :This function data_dict with minimum amount,product detail obj, mrp and dicount
          of product details amount.
    """
    amount = 0
    product_details_amount_obj = None
    mrp = 0
    discount = 0
    get_product_details_amount_obj = ProductDetailsAmount.objects.filter(product_details=
                                                                         product_details_id,
                                                                         product_details__status=
                                                                         "APPROVED").order_by('id').last()
    if get_product_details_amount_obj:
        amount = get_product_details_amount_obj.admin_amount
        product_details_amount_obj = get_product_details_amount_obj
        mrp = get_product_details_amount_obj.mrp
        discount = get_product_details_amount_obj.discount

    data_dict = {
        "minimum_amount": amount,
        "product_details_amount_obj": product_details_amount_obj,
        "mrp": mrp,
        "discount": discount
    }
    return data_dict


class DashboardProductsApiNew(generics.ListAPIView):
    """
    This Api is for Showing category wise products on the main Dashboard.

    Url: ecomapi/dashboard_products

    Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]

    method = get

    Response:
        {
    "status": "1",
    "products": {
        "Men": [
            {
                "id": "36",
                "variants": [],
                "thumbnail": "image",
                "title": "asdfgh",
                "vendor": "",
                "price": 1350.0,
                "rating": "True",
                "ratingCount": "5",
                "badge": [],
                "categories": [
                    {
                        "id": "1",
                        "value": "Men",
                        "name": "Men"
                    }
                ],
                "brand": []
            },
            {
                "id": "35",
                "variants": [],
                "thumbnail": "image",
                "title": "najshrj",
                "vendor": "",
                "price": 1200.0,
                "rating": "True",
                "ratingCount": "5",
                "badge": [],
                "categories": [
                    {
                        "id": "1",
                        "value": "Men",
                        "name": "Men"
                    }
                ],
                "brand": []
            },
        ],
    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        category_name = request.data.get('category_name')
        categorywise_dict = {}  # Main output dict contains all categorywise list of product dict.
        product_dict = {}  # This dict contains product details
        category_id = ""
        category_value = ""
        show_category_name = ""
        if category_name:
            product_dict_list = []  # This list contains product dicts..
            product_id_list = []
            get_category_obj = None
            if category_name == "Sabse_Sasta_Store":
                category_id = None
                category_value = "Sabse_Sasta_Store"
                show_category_name = "Sabse_Sasta_Store"
                product_id_list = list(ProductDetails.objects.filter(
                    product_id__seller__username="MFPL", status="APPROVED").exclude(
                    product_id__base_category__sub_category__category__name="Grocery").values_list(
                    'product_id', flat=True).distinct())

                if product_id_list:
                    product_id_list = random.sample(product_id_list, len(product_id_list))
            else:
                get_category_obj = Category.objects.filter(name=category_name)
                if get_category_obj:
                    category_id = ""
                    category_value = get_category_obj[0].name
                    category_name = get_category_obj[0].name
                    product_id_list = list(ProductDetails.objects.filter(
                        product_id__base_category__sub_category__category__name=get_category_obj[0].name,
                        status="APPROVED", product_id__dashboard_status=True).values_list(
                        'product_id', flat=True).distinct())

                    if product_id_list:
                        product_id_list = random.sample(product_id_list, len(product_id_list))

            if product_id_list:
                products = Product.objects.filter(id__in=product_id_list).order_by('priority')
                if products:
                    for product in products:
                        product_dict = {}
                        # Getting Product Image here..
                        image = ""
                        product_image = Images.objects.filter(type="main", product=product.id).order_by(
                            'id').last()
                        if product_image:
                            image = image_thumbnail(product_image.path)

                        # getting minimum amount for product details..
                        minimum_data_dict = get_minimum_amount(product.id)
                        product_minimum_amount = 0
                        mrp = 0
                        discount = 0
                        if minimum_data_dict:
                            product_minimum_amount = minimum_data_dict['minimum_amount']
                            mrp = minimum_data_dict['mrp']
                            discount = minimum_data_dict['discount']

                        # getting variants list..
                        variants_list = get_product_variants(product.id)

                        rating = 0
                        rating_data = get_product_rating(product.id)
                        if rating_data:
                            rating = rating_data["rating"]

                        product_dict = {"id": str(product.id),
                                        "variants": variants_list,
                                        "thumbnail": str(image),
                                        "title": product.name,
                                        "vendor": product.seller.username,
                                        "price": product_minimum_amount,
                                        "mrp": mrp,
                                        "discount": discount,
                                        "rating": "True",
                                        "ratingCount": rating,
                                        "badge": [],
                                        "product_url": product_url + str(product.id) + "/",
                                        "categories": [{
                                            "id": category_id,
                                            "value": category_value,
                                            "name": show_category_name
                                        }],
                                        "brand": []
                                        }
                        product_dict_list.append(product_dict)
            # categorywise_dict.update({get_category_obj[0].name: product_dict_list})
            categorywise_dict.update({category_name: product_dict_list})
        return Response(
            {
                'status': '1',
                'products': categorywise_dict
            }
        )


def get_product_response_data(product):
    """
    params: product
    description : This function takes product obj and gives product all data in return

    """
    if product is not None:
        product_dict = {}
        # Getting Product Image here..
        image = ""
        product_image = Images.objects.filter(type="main", product=product.id).order_by('id').last()
        if product_image:
            image = image_thumbnail(product_image.path)

        # getting minimum amount for product details..
        minimum_data_dict = get_minimum_amount(product.id)
        product_minimum_amount = 0
        mrp = 0
        discount = 0
        if minimum_data_dict:
            product_minimum_amount = minimum_data_dict['minimum_amount']
            mrp = minimum_data_dict['mrp']
            discount = minimum_data_dict['discount']

        # getting variants list..
        variants_list = get_product_variants(product.id)

        # getting rating data..
        rating = 0
        rating_data = get_product_rating(product.id)
        if rating_data:
            rating = rating_data["rating"]

        product_dict = {"id": str(product.id),
                        "variants": variants_list,
                        "thumbnail": str(image),
                        "title": product.name,
                        "vendor": product.seller.username,
                        "price": product_minimum_amount,
                        "mrp": mrp,
                        "discount": discount,
                        "rating": "True",
                        "ratingCount": rating,
                        "badge": [],
                        "product_url": product_url + str(product.id) + "/",
                        "categories": [{
                            "id": str(product.base_category.sub_category.category.id),
                            "value": product.base_category.sub_category.category.name,
                            "name": product.base_category.sub_category.category.name
                        }],
                        "brand": []
                        }
        return product_dict


class CategoryListAPI(generics.ListAPIView):
    """
    This Api is for Showing list of categories with its related image.

    Url: ecomapi/category_list

    Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]

    method = get

    Response:
        {
    "status": "1",
    "category": [
                {
                    "name": "Men",
                    "image": "/static/img/men.jpg"
                },
                {
                    "name": "Women",
                    "image": "/static/img/women.jpg"
                },
                {
                    "name": "Sport",
                    "image": "/static/img/sport.jpeg"
                }
              ]
            }

    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        category_list = []
        category_names = ProductDetails.objects.filter(status="APPROVED").order_by(
            'product_id__base_category__sub_category__category__priority').values_list(
            'product_id__base_category__sub_category__category__name', flat=True).distinct()

        if category_names:
            categories = Category.objects.filter(name__in=category_names, status=True)
            if categories:
                for category in categories:
                    image = ""
                    image_obj = Images.objects.filter(type="main",
                                                      product_id__base_category__sub_category__category__name=category.name).order_by(
                        'id').last()
                    if image_obj:
                        image = image_thumbnail(image_obj)
                    category_dict = {"name": category.name, "image": str(image)}
                    category_list.append(category_dict)
        return Response(
            {
                "status": 1,
                "category": category_list
            }
        )


class SimilarProductsAPI(generics.ListAPIView):
    """
    This Api is to show Similar Products.

    Url: ecomapi/similar_products_api

    Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]

    method = post
    parameters:
        {
            'product_details_id' = 12
        }

    Response:
            {
            "status":1,
            "data":{
                "relatedproducts": [
                {
                    "id": "1",
                    "thumbnail": "/static/img/products/shop/1.jpg",
                    "variants": [
                        {
                            "id": "1",
                            "thumbnail": "/static/img/products/variants/1a.jpg"
                        },
                        {
                            "id": "2",
                            "thumbnail": "/static/img/products/variants/1b.jpg"
                        },
                        {
                            "id": "3",
                            "thumbnail": "/static/img/products/variants/1c.jpg"
                        }
                    ],
                    "title": "Apple iPhone Retina 6s Plus 32GB",
                    "vendor": "",
                    "price": "640.00",
                    "sale": "False",
                    "categories": [
                        {
                            "id": "1",
                            "value": "phone",
                            "name": "Phones & Accessories"
                        }
                    ],
                    "brand": [
                    ]
                },

            ]
        },

    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        product_details_id = request.data.get('product_id')
        related_products_list = []
        get_product_obj = Product.objects.filter(id=product_details_id)
        if get_product_obj:
            category_obj = Category.objects.filter(name=get_product_obj[0].base_category.sub_category.category.name)
            sub_category_obj = SubCategory.objects.filter(name=get_product_obj[0].base_category.sub_category.name)
            minimum_data_dict = get_minimum_amount(get_product_obj[0])
            price = 0
            if minimum_data_dict:
                price = minimum_data_dict['minimum_amount']

            # This is filter out the same products withing similar price range..
            percent_price = (price * 20) / 100
            less_price = price - percent_price
            more_price = price + percent_price
            products_objects_list = ProductDetailsAmount.objects.filter(product_details__status="APPROVED",
                                                                        product_details__product_id__base_category__sub_category__category=
                                                                        category_obj[0],
                                                                        product_details__product_id__base_category__sub_category=
                                                                        sub_category_obj[0],
                                                                        admin_amount__range=(
                                                                            less_price, more_price)).order_by(
                'product_details', 'date').distinct('product_details').values_list('product_details__product_id',
                                                                                   flat=True)

            if products_objects_list:
                similar_products = Product.objects.filter(id__in=products_objects_list)[:25]
                if similar_products:
                    for product in similar_products:
                        product_dict = {}
                        # Getting Product Image here..
                        image = ""
                        product_image = Images.objects.filter(type="main", product=product.id).order_by('id').last()
                        if product_image:
                            image = image_thumbnail(product_image.path)

                        # getting minimum amount for product details..
                        minimum_data_dict = get_minimum_amount_with_details_id(product.id)
                        product_minimum_amount = 0
                        mrp = 0
                        discount = 0
                        product_stock = 0
                        if minimum_data_dict:
                            product_minimum_amount = minimum_data_dict['minimum_amount']
                            mrp = minimum_data_dict['mrp']
                            discount = minimum_data_dict['discount']
                            product_details_id = minimum_data_dict['product_details_id']

                            if product_details_id is not None:
                                # getting product details stock..
                                stock = get_product_details_stock(product_details_id)
                                if stock:
                                    product_stock = stock

                        # getting variants list..
                        variants_list = get_product_variants(product.id)

                        # getting rating data..
                        rating = 0
                        rating_data = get_product_rating(product.id)
                        if rating_data:
                            rating = rating_data["rating"]

                        product_dict = {"id": str(product.id),
                                        "variants": variants_list,
                                        "thumbnail": str(image),
                                        "title": product.name,
                                        "vendor": product.seller.username,
                                        "price": product_minimum_amount,
                                        "mrp": mrp,
                                        "discount": discount,
                                        "rating": "True",
                                        "ratingCount": rating,
                                        "badge": [],
                                        "product_url": product_url + str(product.id) + "/",
                                        "product_stock": product_stock,
                                        "categories": [{
                                            "id": str(category_obj[0].id),
                                            "value": category_obj[0].name,
                                            "name": category_obj[0].name
                                        }],
                                        "brand": []
                                        }
                        related_products_list.append(product_dict)
                    return Response(
                        {"status": 1,
                         "data": related_products_list}
                    )

            else:
                return Response(
                    {"status": 2,
                     "data": related_products_list}
                )

        else:
            return Response(
                {"status": 3,
                 "data": related_products_list}
            )


# This API will be used after..
class FooterCatLinks(APIView):
    """
    This Api is to Show category and sub_category lists..

    Url: ecomapi/similar_products_api

    Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]

    method = post
    parameters:
        {
            'product_details_id' = 12
        }

    Response:
    """

    def get(self, request, *args, **kwargs):
        data_list = [
            {
                "category": "cat 1",
                "subs": [
                    {
                        "text": "sub1",
                        "url": "/shop"
                    },
                    {
                        "text": "sub2",
                        "url": "/shop"
                    },
                    {
                        "text": "sub3",
                        "url": "/shop"
                    },
                    {
                        "text": "sub4",
                        "url": "/shop"
                    }
                ]
            },
            {
                "category": "cat 2",
                "subs": [
                    {
                        "text": "sub1",
                        "url": "/shop"
                    },
                    {
                        "text": "sub2",
                        "url": "/shop"
                    },
                    {
                        "text": "sub3",
                        "url": "/shop"
                    },
                    {
                        "text": "sub4",
                        "url": "/shop"
                    },
                    {
                        "text": "sub5",
                        "url": "/shop"
                    }
                ]
            },
            {
                "category": "cat 3",
                "subs": [
                    {
                        "text": "sub1",
                        "url": "/shop"
                    },
                    {
                        "text": "sub2",
                        "url": "/shop"
                    },
                    {
                        "text": "sub3",
                        "url": "/shop"
                    }
                ]
            }
        ]
        return Response(
            {
                "data": data_list
            }
        )


class HomepageAds(APIView):
    def get(self, request, *args, **kwargs):
        homeads_list = [
            {
                "ads": "/static/img/mudrakwik.png",
            },
            {
                "ads": "/static/img/mudrakwik.png",
            },
            {
                "ads": "/static/img/mudrakwik.png",
            }
        ]
        homeadscol_list = [
            {
                "ads": "/static/img/mudrakwik.png",
            },
            {
                "ads": "/static/img/mudrakwik.png",
            },
            {
                "ads": "/static/img/mudrakwik.png",
            }
        ]
        return Response(
            {
                "homeads": homeads_list,
                "homeadscol": homeadscol_list
            }
        )


class ProductsForCategory(generics.ListAPIView):
    """
    This Api is for Showing Products for perticular category.
​
    Url: ecomapi/products_for_category
​
    Authentication token required
​
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
​
    method = post
    parameters:
        {
            'category_name' = "Mens"
        }
​
    Response:
        {
    "status": "1",
    "data":
            {
                "id": "36",
                "variants": [],
                "thumbnail": "image",
                "title": "asdfgh",
                "vendor": "",
                "price": 1350.0,
                "rating": "True",
                "ratingCount": "5",
                "badge": [],
                "categories": [
                    {
                        "id": "1",
                        "value": "Men",
                        "name": "Men"
                    }
                ],
                "brand": []
            },
            {
                "id": "35",
                "variants": [],
                "thumbnail": "image",
                "title": "najshrj",
                "vendor": "",
                "price": 1200.0,
                "rating": "True",
                "ratingCount": "5",
                "badge": [],
                "categories": [
                    {
                        "id": "1",
                        "value": "Men",
                        "name": "Men"
                    }
                ],
                "brand": []
            },
        ],

        """

    def post(self, request, *args, **kwargs):
        product_dict_list = []  # This list contains product dicts.
        input = request.data.get('category')
        if input is not None:
            if "!" in input:
                input_list = input.split('!')
                category = input_list[0]
                category_obj = Category.objects.filter(name=category)
                sub_category = input_list[1]
                base_name = input_list[2]
                product_id_list = ProductDetails.objects.filter(status="APPROVED",
                                                                product_id__base_category__sub_category__category__name=category,
                                                                product_id__base_category__sub_category__name=sub_category,
                                                                product_id__base_category__base_name=base_name).values_list(
                    'product_id', flat=True).distinct()
                if product_id_list:
                    products = Product.objects.filter(id__in=product_id_list)
                    if products:
                        for product in products:
                            product_dict = {}
                            # Getting Product Image here..
                            image = ""
                            product_image = Images.objects.filter(type="main", product=product.id).order_by('id').last()
                            # product_image = True
                            if product_image:
                                image = image_thumbnail(product_image.path)

                            # getting minimum amount for product details..
                            minimum_data_dict = get_minimum_amount(product.id)
                            product_minimum_amount = 0
                            mrp = 0
                            discount = 0
                            if minimum_data_dict:
                                product_minimum_amount = minimum_data_dict['minimum_amount']
                                mrp = minimum_data_dict['mrp']
                                discount = minimum_data_dict['discount']

                            # getting variants list..
                            variants_list = get_product_variants(product.id)

                            # getting rating data..
                            rating = 0
                            rating_data = get_product_rating(product.id)
                            if rating_data:
                                rating = rating_data["rating"]

                            product_dict = {"id": str(product.id),
                                            "variants": variants_list,
                                            "thumbnail": str(image),
                                            "title": product.name,
                                            "vendor": product.seller.username,
                                            "price": product_minimum_amount,
                                            "mrp": mrp,
                                            "discount": discount,
                                            "rating": "True",
                                            "ratingCount": rating,
                                            "badge": [],
                                            "product_url": product_url + str(product.id) + "/",
                                            "categories": [{
                                                "id": str(category_obj[0].id),
                                                "value": category_obj[0].name,
                                                "name": category_obj[0].name
                                            }],
                                            "brand": []
                                            }
                            product_dict_list.append(product_dict)
                    return Response(
                        {
                            'status': '1',
                            'data': product_dict_list
                        }
                    )
                else:
                    return Response(
                        {
                            'status': '2',
                            'data': product_dict_list
                        }
                    )
            else:
                category_name = input
                categorywise_dict = {}  # Main output dict contains all categorywise list of product dict.
                product_dict = {}  # This dict contains product details
                if category_name:
                    get_category_obj = Category.objects.filter(name=category_name)
                    product_dict_list = []  # This list contains product dicts..
                    product_id_list = ProductDetails.objects.filter(
                        product_id__base_category__sub_category__category__name=get_category_obj[0].name,
                        status="APPROVED").values_list(
                        'product_id', flat=True).distinct()
                    if product_id_list:
                        products = Product.objects.filter(id__in=product_id_list)
                        if products:
                            for product in products:
                                product_dict = {}
                                # Getting Product Image here..
                                image = ""
                                product_image = Images.objects.filter(type="main", product=product.id).order_by(
                                    'id').last()
                                if product_image:
                                    image = image_thumbnail(product_image.path)

                                # getting minimum amount for product details..
                                minimum_data_dict = get_minimum_amount(product.id)
                                product_minimum_amount = 0
                                mrp = 0
                                discount = 0
                                if minimum_data_dict:
                                    product_minimum_amount = minimum_data_dict['minimum_amount']
                                    mrp = minimum_data_dict['mrp']
                                    discount = minimum_data_dict['discount']

                                # getting variants list..
                                variants_list = get_product_variants(product.id)

                                # getting rating data..
                                rating = 0
                                rating_data = get_product_rating(product.id)
                                if rating_data:
                                    rating = rating_data["rating"]

                                product_dict = {"id": str(product.id),
                                                "variants": variants_list,
                                                "thumbnail": str(image),
                                                "title": product.name,
                                                "vendor": product.seller.username,
                                                "price": product_minimum_amount,
                                                "rating": "True",
                                                "ratingCount": rating,
                                                "badge": [],
                                                "product_url": product_url + str(product.id) + "/",
                                                "categories": [{
                                                    "id": str(get_category_obj[0].id),
                                                    "value": get_category_obj[0].name,
                                                    "name": get_category_obj[0].name
                                                }],
                                                "brand": []
                                                }
                                product_dict_list.append(product_dict)

                return Response(
                    {
                        'status': '1',
                        'data': product_dict_list
                    }
                )
        else:
            return Response(
                {
                    'status': '3',
                    'message': 'Please provide category'
                }
            )


class ProductsForCategoryNew(generics.ListAPIView):
    """
    Created new API for getting particular Category API..
    """

    def post(self, request, *args, **kwargs):
        category_name = request.data.get('category_name')
        categorywise_dict = {}  # Main output dict contains all categorywise list of product dict.
        product_dict = {}  # This dict contains product details
        if category_name:
            product_dict_list = []  # This list contains product dicts..
            product_id_list = []
            category_id = ""
            category_value = ""
            category_name_show = ""
            if category_name == "Sabse_Sasta_Store":
                category_id = ""
                category_value = "Sabse_Sasta_Store"
                category_name_show = "Sabse_Sasta_Store"
                product_id_list = list(ProductDetails.objects.filter(
                    product_id__seller__username="MFPL", status="APPROVED").exclude(
                    product_id__base_category__sub_category__category__name="Grocery").values_list(
                    'product_id', flat=True).distinct())

                if product_id_list:
                    product_id_list = random.sample(product_id_list, len(product_id_list))

            else:
                get_category_obj = Category.objects.filter(name=category_name)
                if get_category_obj:
                    category_id = str(get_category_obj[0].id)
                    category_value = get_category_obj[0].name
                    category_name_show = get_category_obj[0].name
                    product_id_list = ProductDetails.objects.filter(
                        product_id__base_category__sub_category__category__name=get_category_obj[0].name,
                        status="APPROVED").values_list(
                        'product_id', flat=True).distinct()

            if product_id_list:
                products = Product.objects.filter(id__in=product_id_list)
                if products:
                    for product in products:
                        product_dict = {}
                        # Getting Product Image here..
                        image = ""
                        product_image = Images.objects.filter(type="main", product=product.id).order_by(
                            'id').last()
                        if product_image:
                            image = image_thumbnail(product_image.path)

                        # getting minimum amount for product details..
                        minimum_data_dict = get_minimum_amount(product.id)
                        product_minimum_amount = 0
                        mrp = 0
                        discount = 0
                        if minimum_data_dict:
                            product_minimum_amount = minimum_data_dict['minimum_amount']
                            mrp = minimum_data_dict['mrp']
                            discount = minimum_data_dict['discount']

                        # getting variants list..
                        variants_list = get_product_variants(product.id)

                        # getting rating data..
                        rating = 0
                        rating_data = get_product_rating(product.id)
                        if rating_data:
                            rating = rating_data["rating"]

                        product_dict = {"id": str(product.id),
                                        "variants": variants_list,
                                        "thumbnail": str(image),
                                        "title": product.name,
                                        "vendor": product.seller.username,
                                        "price": product_minimum_amount,
                                        "mrp": mrp,
                                        "discount": discount,
                                        "rating": "True",
                                        "ratingCount": rating,
                                        "badge": [],
                                        "product_url": product_url + str(product.id) + "/",
                                        "categories": [{
                                            "id": category_id,
                                            "value": category_value,
                                            "name": category_name_show
                                        }],
                                        "brand": []
                                        }
                        product_dict_list.append(product_dict)
                categorywise_dict.update({category_name_show: product_dict_list})

        return Response(
            {
                'status': '1',
                'products': categorywise_dict
            }
        )


class ColoursForProduct(generics.ListAPIView):
    """
    This Api is for Showing colour for particular product.
​
    Url: ecomapi/colours_for_product
​
    method = post
    parameters:
         input:
        {
	        "product_id":"12"
        }
​
​
    Response:
​
    output:
    {
        "colour": [
        {
            "colour": "Red"
        },
        {
            "colour": "white"
        },
        {
            "colour": "blue"
        }
        ]
    }
    """

    def post(self, request, *args, **kwargs):
        data = {}
        data_list = []
        product_id = request.data.get('product_id')
        product = Product.objects.filter(id=product_id)
        product_details_objects = set(
            ProductDetails.objects.filter(product_id=product_id).values_list('colour_id__name',
                                                                             flat=True))
        if product_details_objects:
            for colour in product_details_objects:
                data_dict = {'colour': colour}
                data_list.append(data_dict)

        data.update({"colour": data_list})
        return Response(
            {
                'status': '1',
                'data': data
            }
        )


class SizeForProduct(generics.ListAPIView):
    """
    This Api is for Showing size for particular product.
​
    Url: ecomapi/size_for_product
​
    method = post
    parameters:
         input:
            {
                "product_id":"12",
                "colour":"Red"
            }
​
    Response:
​
        {
            "status": "1",
            "message": "Success",
            "data": {
                "size": [
                    {
                        "size": "Medium"
                    },
                    {
                        "size": "Small"
                    }
                ]
            }
        }
    """

    def post(self, request, *args, **kwargs):
        data = {}
        data_list = []
        product_id = request.data.get('product_id')
        colour = request.data.get('colour')
        if product_id and colour is not None:
            colour_obj = Colour.objects.filter(name=colour)
            if colour_obj:
                product_details_objects = ProductDetails.objects.filter(product_id=product_id,
                                                                        colour_id=colour_obj[0]).values_list(
                    'size_id__name', flat=True)
                if product_details_objects:
                    for size in product_details_objects:
                        data_dict = {'size': size}
                        data_list.append(data_dict)
                    data.update({"size": data_list})
                    return Response(
                        {
                            'status': '1',
                            'message': "Success",
                            'data': data
                        }
                    )
                else:
                    return Response(
                        {
                            'status': '2',
                            'message': "Success",
                            'data': data
                        }
                    )
            else:
                return Response(
                    {
                        'status': '3',
                        'message': "Enter valid colour",
                        'data': data
                    }
                )
        else:
            return Response(
                {
                    'status': '4',
                    'message': "Provide all data",
                    'data': data
                }
            )


def get_product_details_stock(product_details_id):
    """
    This function gives product details remaining stock..
    """
    if product_details_id is not None:
        product_stock = 0
        stock = Stock.objects.filter(product_details=product_details_id).order_by('id').last()
        if stock:
            product_stock = stock.stock_balance
        return product_stock


class ParticularProductInfoAPI(generics.ListAPIView):
    """
        This Api is for Showing category wise products on the main Dashboard.
        Url: ecomapi/dashboard_products
        Authentication token required
                [
                    {
                        "key":"Authorization",
                        "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                    }
                ]
        method = post
        Response:
            {
    "products": [
        {
            "id": "97",
            "thumbnail": "/static/img/products/shop/1.jpg",
            "variants": [
                {
                    "id": "1",
                    "thumbnail": "/static/img/products/variants/1a.jpg"
                },
                {
                    "id": "2",
                    "thumbnail": "/static/img/products/variants/1b.jpg"
                },
                {
                    "id": "3",
                    "thumbnail": "/static/img/products/variants/1c.jpg"
                }
            ],
            "title": "++++Apple iPhone Retina 6s Plus 32GB",
            "vendor": "Mohan Dholu",
            "price": "640.00",
            "sale": "false",
            "categories": [
                {
                    "id": "1",
                    "value": "phone",
                    "name": "Phones & Accessories"
                }
            ],
            "brand": []
        }
    ]
}
        """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    def post(self, request, *args, **kwargs):
        # print("inside post")
        product_id = request.data.get('product_id')
        colour_select = request.data.get('colour')
        size_select = request.data.get('size')
        unit_combination = None
        if "unit_combination" in request.data:
            unit_combination = request.data.get('unit_combination')
        if unit_combination:
            if unit_combination == "unit_combination":
                if colour_select == "colour" and size_select == "size":
                    if product_id and product_id is not None:
                        product = Product.objects.filter(id=product_id)
                        product_dict = {}
                        if product:
                            # Getting Product Image here..
                            image = ""
                            product_image = Images.objects.filter(type="main", product=product[0].id).order_by(
                                'id').last()
                            if product_image:
                                image = image_thumbnail(product_image.path)

                            # getting minimum amount for product details..
                            minimum_data_dict = get_minimum_amount_with_details_id(product[0].id)
                            product_minimum_amount = 0
                            mrp = 0
                            discount = 0
                            product_stock = 0
                            get_product_details_obj = None
                            size_colour_data = None
                            product_details_id = None
                            if minimum_data_dict:
                                product_minimum_amount = minimum_data_dict['minimum_amount']
                                mrp = minimum_data_dict['mrp']
                                discount = minimum_data_dict['discount']
                                product_details_id = minimum_data_dict['product_details_id']

                                if product_details_id is not None:
                                    # getting product details stock..
                                    get_product_details_obj = ProductDetails.objects.filter(id=product_details_id)
                                    stock = get_product_details_stock(product_details_id)
                                    if stock:
                                        product_stock = stock

                                # getting colours, sizes and specifications available for Product..
                                size_colour_data = get_sizes_colours_specifications(product_details_id)

                            # getting variants list..
                            variants_list = get_product_details_variants(product_details_id)

                            # getting rating data..
                            rating = 0
                            rating_length = 0
                            review_length = 0
                            rating_data = get_product_rating(product[0].id)
                            if rating_data:
                                rating = rating_data["rating"]
                                rating_length = rating_data["rating_length"]
                                review_length = rating_data["review_length"]

                            review_list = get_product_rating_and_review(product[0].id, "minimum")
                            product_dict = {"id": str(product[0].id),
                                            "variants": variants_list,
                                            "thumbnail": str(image),
                                            "title": product[0].name,
                                            "vendor": product[0].seller.username,
                                            "price": product_minimum_amount,
                                            "mrp": mrp,
                                            "discount": discount,
                                            "rating": "True",
                                            "ratingCount": rating,
                                            "rating_length": rating_length,
                                            "review_length": review_length,
                                            "review_list": review_list,
                                            "badge": [],
                                            "product_url": product_url + str(product[0].id) + "/",
                                            "product_details_id": str(product_details_id),
                                            "d_colour": size_colour_data["colour_list"],
                                            "specifications": size_colour_data["specifications"],
                                            "description": size_colour_data["description"],
                                            "d_unit_combinations": size_colour_data["unit_combination_list"],
                                            "product_stock": product_stock,
                                            "categories": [{
                                                "id": str(product[0].base_category.sub_category.category.id),
                                                "value": product[0].base_category.sub_category.category.name,
                                                "name": product[0].base_category.sub_category.category.name
                                            }],
                                            "sub_category": product[0].base_category.sub_category.name,
                                            "category": product[0].base_category.sub_category.category.name,
                                            "base_category": product[0].base_category.base_name,
                                            "brand": []
                                            }
                        return Response(
                            {
                                'status': "1",
                                'products': [product_dict]
                            }
                        )
                    else:
                        return Response(
                            {
                                'status': "2",
                                'message': 'please provide product_id'
                            }
                        )

                else:
                    if product_id and product_id is not None:
                        product_dict = {}
                        product_details_obj = ProductDetails.objects.filter(product_id=product_id,
                                                                            colour_id__name=colour_select,
                                                                            size_id__name=size_select)
                        if product_details_obj:
                            # Getting product details image..
                            image = ""
                            product_details_image = Images.objects.filter(type="other",
                                                                          product_details=product_details_obj[
                                                                              0]).order_by(
                                'id').last()
                            if product_details_image:
                                image = image_thumbnail(product_details_image.path)

                            else:
                                product_image = Images.objects.filter(type="main", product=product_id).order_by(
                                    'id').last()
                                if product_image:
                                    image = image_thumbnail(product_image.path)

                            # getting minimum amount for product details..
                            product_amount = product_details_obj[0].amount
                            mrp = product_details_obj[0].mrp
                            discount = product_details_obj[0].discount
                            data = get_product_details_price(product_details_obj[0].id)
                            if data:
                                product_amount = data['minimum_amount']
                                mrp = data["mrp"]
                                discount = data["discount"]

                            # getting product details stock..
                            product_stock = 0
                            stock = get_product_details_stock(product_details_obj[0].id)
                            if stock:
                                product_stock = stock

                            # getting colours, sizes and specifications available for Product..
                            size_colour_data = get_sizes_colours_specifications(product_details_obj[0].id)

                            # getting variants list..
                            variants_list = get_product_details_variants(product_details_obj[0].id)

                            # getting rating data..
                            rating = 0
                            rating_length = 0
                            review_length = 0
                            rating_data = get_product_rating(product_details_obj[0].product_id.id)
                            if rating_data:
                                rating = rating_data["rating"]
                                rating_length = rating_data["rating_length"]
                                review_length = rating_data["review_length"]

                            review_list = get_product_rating_and_review(product_details_obj[0].product_id.id, "minimum")

                            product_dict = {"id": str(product_id),
                                            "variants": variants_list,
                                            "thumbnail": str(image),
                                            "title": product_details_obj[0].product_id.name,
                                            "vendor": product_details_obj[0].user.username,
                                            "price": product_amount,
                                            "mrp": mrp,
                                            "discount": discount,
                                            "rating": "True",
                                            "ratingCount": rating,
                                            "rating_length": rating_length,
                                            "review_length": review_length,
                                            "review_list": review_list,
                                            "badge": [],
                                            "product_url": product_url + str(
                                                product_details_obj[0].product_id.id) + "/",
                                            "product_details_id": str(product_details_obj[0].id),
                                            "d_colour": size_colour_data["colour_list"],
                                            "specifications": size_colour_data["specifications"],
                                            "description": size_colour_data["description"],
                                            "d_unit_combinations": size_colour_data["unit_combination_list"],
                                            "product_stock": product_stock,
                                            "categories": [{
                                                "id": str(
                                                    product_details_obj[
                                                        0].product_id.base_category.sub_category.category.id),
                                                "value": product_details_obj[
                                                    0].product_id.base_category.sub_category.category.name,
                                                "name": product_details_obj[
                                                    0].product_id.base_category.sub_category.category.name,
                                            }],
                                            "sub_category": product_details_obj[
                                                0].product_id.base_category.sub_category.name,
                                            "category": product_details_obj[
                                                0].product_id.base_category.sub_category.category.name,
                                            "base_category": product_details_obj[0].product_id.base_category.base_name,
                                            "brand": []
                                            }
                            return Response(
                                {
                                    'status': '1',
                                    'products': [product_dict]
                                }
                            )
                        else:
                            return Response(
                                {
                                    'status': '2',
                                    'products': [product_dict]
                                }
                            )
                    else:
                        return Response(
                            {
                                'status': "2",
                                'message': 'please provide product_id'
                            }
                        )

            else:
                if product_id and product_id is not None:
                    unit_count = None
                    unit = None
                    if unit_combination is not None:
                        unit_combinations = unit_combination.split("%")
                        if unit_combinations:
                            unit_count = unit_combinations[0]
                            unit = unit_combinations[1]
                    product_dict = {}
                    product_details_obj = ProductDetails.objects.filter(product_id=product_id,
                                                                        unit_count=unit_count,
                                                                        unit__name=unit)
                    if product_details_obj:
                        # Getting product details image..
                        image = ""
                        product_details_image = Images.objects.filter(type="other",
                                                                      product_details=product_details_obj[0]).order_by(
                            'id').last()
                        if product_details_image:
                            image = image_thumbnail(product_details_image.path)

                        else:
                            product_image = Images.objects.filter(type="main", product=product_id).order_by('id').last()
                            if product_image:
                                image = image_thumbnail(product_image.path)

                        # getting minimum amount for product details..
                        product_amount = product_details_obj[0].amount
                        mrp = product_details_obj[0].mrp
                        discount = product_details_obj[0].discount
                        data = get_product_details_price(product_details_obj[0].id)
                        if data:
                            product_amount = data['minimum_amount']
                            mrp = data["mrp"]
                            discount = data["discount"]

                        # getting product details stock..
                        product_stock = 0
                        stock = get_product_details_stock(product_details_obj[0].id)
                        if stock:
                            product_stock = stock

                        # getting colours, sizes and specifications available for Product..
                        size_colour_data = get_sizes_colours_specifications(product_details_obj[0].id)

                        # getting variants list..
                        variants_list = get_product_details_variants(product_details_obj[0].id)

                        # getting rating data..
                        rating = 0
                        rating_length = 0
                        review_length = 0
                        rating_data = get_product_rating(product_details_obj[0].product_id.id)
                        if rating_data:
                            rating = rating_data["rating"]
                            rating_length = rating_data["rating_length"]
                            review_length = rating_data["review_length"]

                        review_list = get_product_rating_and_review(product_details_obj[0].product_id.id, "minimum")

                        product_dict = {"id": str(product_id),
                                        "variants": variants_list,
                                        "thumbnail": str(image),
                                        "title": product_details_obj[0].product_id.name,
                                        "vendor": product_details_obj[0].user.username,
                                        "price": product_amount,
                                        "mrp": mrp,
                                        "discount": discount,
                                        "rating": "True",
                                        "ratingCount": rating,
                                        "rating_length": rating_length,
                                        "review_length": review_length,
                                        "review_list": review_list,
                                        "badge": [],
                                        "product_url": product_url + str(product_details_obj[0].product_id.id) + "/",
                                        "product_details_id": str(product_details_obj[0].id),
                                        "d_colour": size_colour_data["colour_list"],
                                        "specifications": size_colour_data["specifications"],
                                        "description": size_colour_data["description"],
                                        "d_unit_combinations": size_colour_data["unit_combination_list"],
                                        "product_stock": product_stock,
                                        "categories": [{
                                            "id": str(
                                                product_details_obj[
                                                    0].product_id.base_category.sub_category.category.id),
                                            "value": product_details_obj[
                                                0].product_id.base_category.sub_category.category.name,
                                            "name": product_details_obj[
                                                0].product_id.base_category.sub_category.category.name,
                                        }],
                                        "sub_category": product_details_obj[
                                            0].product_id.base_category.sub_category.name,
                                        "category": product_details_obj[
                                            0].product_id.base_category.sub_category.category.name,
                                        "base_category": product_details_obj[0].product_id.base_category.base_name,
                                        "brand": []
                                        }
                        return Response(
                            {
                                'status': '1',
                                'products': [product_dict]
                            }
                        )
                    else:
                        return Response(
                            {
                                'status': '2',
                                'products': [product_dict]
                            }
                        )
                else:
                    return Response(
                        {
                            'status': "2",
                            'message': 'please provide product_id'
                        }
                    )

        else:
            if colour_select == "colour" and size_select == "size":
                if product_id and product_id is not None:
                    product = Product.objects.filter(id=product_id)
                    product_dict = {}
                    if product:
                        # Getting Product Image here..
                        image = ""
                        product_image = Images.objects.filter(type="main", product=product[0].id).order_by('id').last()
                        if product_image:
                            image = image_thumbnail(product_image.path)

                        # getting minimum amount for product details..
                        minimum_data_dict = get_minimum_amount_with_details_id(product[0].id)
                        product_minimum_amount = 0
                        mrp = 0
                        discount = 0
                        product_stock = 0
                        get_product_details_obj = None
                        size_colour_data = None
                        product_details_id = None
                        if minimum_data_dict:
                            product_minimum_amount = minimum_data_dict['minimum_amount']
                            mrp = minimum_data_dict['mrp']
                            discount = minimum_data_dict['discount']
                            product_details_id = minimum_data_dict['product_details_id']

                            if product_details_id is not None:
                                # getting product details stock..
                                get_product_details_obj = ProductDetails.objects.filter(id=product_details_id)
                                stock = get_product_details_stock(product_details_id)
                                if stock:
                                    product_stock = stock

                            # getting colours, sizes and specifications available for Product..
                            size_colour_data = get_sizes_colours_specifications(product_details_id)

                        # getting variants list..
                        variants_list = get_product_details_variants(product_details_id)

                        # getting rating data..
                        rating = 0
                        rating_length = 0
                        review_length = 0
                        rating_data = get_product_rating(product[0].id)
                        if rating_data:
                            rating = rating_data["rating"]
                            rating_length = rating_data["rating_length"]
                            review_length = rating_data["review_length"]

                        review_list = get_product_rating_and_review(product[0].id, "minimum")
                        product_dict = {"id": str(product[0].id),
                                        "variants": variants_list,
                                        "thumbnail": str(image),
                                        "title": product[0].name,
                                        "vendor": product[0].seller.username,
                                        "price": product_minimum_amount,
                                        "mrp": mrp,
                                        "discount": discount,
                                        "rating": "True",
                                        "ratingCount": rating,
                                        "rating_length": rating_length,
                                        "review_length": review_length,
                                        "review_list": review_list,
                                        "badge": [],
                                        "product_url": product_url + str(product[0].id) + "/",
                                        "product_details_id": str(product_details_id),
                                        "d_colour": size_colour_data["colour_list"],
                                        "specifications": size_colour_data["specifications"],
                                        "description": size_colour_data["description"],
                                        "d_unit_combinations": size_colour_data["unit_combination_list"],
                                        "product_stock": product_stock,
                                        "categories": [{
                                            "id": str(product[0].base_category.sub_category.category.id),
                                            "value": product[0].base_category.sub_category.category.name,
                                            "name": product[0].base_category.sub_category.category.name
                                        }],
                                        "sub_category": product[0].base_category.sub_category.name,
                                        "category": product[0].base_category.sub_category.category.name,
                                        "base_category": product[0].base_category.base_name,
                                        "brand": []
                                        }
                    return Response(
                        {
                            'status': "1",
                            'products': [product_dict]
                        }
                    )
                else:
                    return Response(
                        {
                            'status': "2",
                            'message': 'please provide product_id'
                        }
                    )
            else:
                if product_id and product_id is not None:
                    product_dict = {}
                    product_details_obj = ProductDetails.objects.filter(product_id=product_id,
                                                                        colour_id__name=colour_select,
                                                                        size_id__name=size_select)
                    if product_details_obj:
                        # Getting product details image..
                        image = ""
                        product_details_image = Images.objects.filter(type="other",
                                                                      product_details=product_details_obj[0]).order_by(
                            'id').last()
                        if product_details_image:
                            image = image_thumbnail(product_details_image.path)

                        else:
                            product_image = Images.objects.filter(type="main", product=product_id).order_by('id').last()
                            if product_image:
                                image = image_thumbnail(product_image.path)

                        # getting minimum amount for product details..
                        product_amount = product_details_obj[0].amount
                        mrp = product_details_obj[0].mrp
                        discount = product_details_obj[0].discount
                        data = get_product_details_price(product_details_obj[0].id)
                        if data:
                            product_amount = data['minimum_amount']
                            mrp = data["mrp"]
                            discount = data["discount"]

                        # getting product details stock..
                        product_stock = 0
                        stock = get_product_details_stock(product_details_obj[0].id)
                        if stock:
                            product_stock = stock

                        # getting colours, sizes and specifications available for Product..
                        size_colour_data = get_sizes_colours_specifications(product_details_obj[0].id)

                        # getting variants list..
                        variants_list = get_product_details_variants(product_details_obj[0].id)

                        # getting rating data..
                        rating = 0
                        rating_length = 0
                        review_length = 0
                        rating_data = get_product_rating(product_details_obj[0].product_id.id)
                        if rating_data:
                            rating = rating_data["rating"]
                            rating_length = rating_data["rating_length"]
                            review_length = rating_data["review_length"]

                        review_list = get_product_rating_and_review(product_details_obj[0].product_id.id, "minimum")

                        product_dict = {"id": str(product_id),
                                        "variants": variants_list,
                                        "thumbnail": str(image),
                                        "title": product_details_obj[0].product_id.name,
                                        "vendor": product_details_obj[0].user.username,
                                        "price": product_amount,
                                        "mrp": mrp,
                                        "discount": discount,
                                        "rating": "True",
                                        "ratingCount": rating,
                                        "rating_length": rating_length,
                                        "review_length": review_length,
                                        "review_list": review_list,
                                        "badge": [],
                                        "product_url": product_url + str(product_details_obj[0].product_id.id) + "/",
                                        "product_details_id": str(product_details_obj[0].id),
                                        "d_colour": size_colour_data["colour_list"],
                                        "specifications": size_colour_data["specifications"],
                                        "description": size_colour_data["description"],
                                        "d_unit_combinations": size_colour_data["unit_combination_list"],
                                        "product_stock": product_stock,
                                        "categories": [{
                                            "id": str(
                                                product_details_obj[
                                                    0].product_id.base_category.sub_category.category.id),
                                            "value": product_details_obj[
                                                0].product_id.base_category.sub_category.category.name,
                                            "name": product_details_obj[
                                                0].product_id.base_category.sub_category.category.name,
                                        }],
                                        "sub_category": product_details_obj[
                                            0].product_id.base_category.sub_category.name,
                                        "category": product_details_obj[
                                            0].product_id.base_category.sub_category.category.name,
                                        "base_category": product_details_obj[0].product_id.base_category.base_name,
                                        "brand": []
                                        }
                        return Response(
                            {
                                'status': '1',
                                'products': [product_dict]
                            }
                        )
                    else:
                        return Response(
                            {
                                'status': '2',
                                'products': [product_dict]
                            }
                        )
                else:
                    return Response(
                        {
                            'status': "2",
                            'message': 'please provide product_id'
                        }
                    )


def get_sizes_colours_specifications(product_details_id):
    if product_details_id is not None:
        specifications = {}
        colour_list = []
        size_list = []
        unit_combination_list = []
        get_product_details_obj = ProductDetails.objects.filter(id=product_details_id, status="APPROVED")
        if get_product_details_obj:
            # getting colours available for Product..
            colour_list = []
            colours = ProductDetails.objects.filter(product_id=get_product_details_obj[0].product_id.id).values_list(
                'colour_id__name', flat=True).distinct()
            if colours:
                colour_list = colours

            # getting sizes available..
            size_list = []
            sizes = ProductDetails.objects.filter(product_id=get_product_details_obj[0].product_id.id,
                                                  colour_id__name=get_product_details_obj[
                                                      0].colour_id.name).values_list('size_id__name',
                                                                                     flat=True).distinct()
            if sizes:
                size_list = sizes

            try:
                # getting unit combinations..
                unit_combinations = ProductDetails.objects.filter(product_id=get_product_details_obj[0].product_id.id)
                if unit_combinations:
                    for combination in unit_combinations:
                        unit_combination = ProductDetails.unit_combination(combination)
                        if unit_combination:
                            unit_combination_list.append(unit_combination)
            except Exception as e:
                print("Inside Exception of getting unit combination=================", e)

            colour_name = ""
            if get_product_details_obj[0].colour_id:
                colour_name = get_product_details_obj[0].colour_id.name

            size_name = ""
            if get_product_details_obj[0].size_id:
                size_name = get_product_details_obj[0].size_id.name
            # getting Specifications..
            specifications = {
                "colour": colour_name,
                "height": get_product_details_obj[0].height,
                "length": get_product_details_obj[0].Length,
                "width": get_product_details_obj[0].width,
                "weight": get_product_details_obj[0].weight,
                "size": size_name,
                "unit_combination": ProductDetails.unit_combination(get_product_details_obj[0]),
                "d_size": size_list
            }

            if get_product_details_obj[0].specification:
                extra_specification = get_product_details_obj[0].specification
                if extra_specification:
                    specifications = {**specifications, **extra_specification}

        return {
            "specifications": specifications,
            "colour_list": colour_list,
            "size_list": size_list,
            "unit_combination_list": unit_combination_list,
            "description": get_product_details_obj[0].description,
        }


class ProductDetailsInfoAPI(generics.ListAPIView):
    """
    This Api is for Showing Products for particular colour and size.
​
    Url: ecomapi/product_details
​
    Authentication token required
​
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
​
    method = post
    input:
    {
    	"id":"1",
    	"colour":"red",
    	"size":"12"
    }
    output:
    {
        "data": {
            "id": "3",
            "variants": [
                {
                    "id": "1",
                    "thumbnail": "/static/img/products/detail/fullwidth/1.jpg"
                },
                {
                    "id": "2",
                    "thumbnail": "/static/img/products/detail/fullwidth/2.jpg"
                },
                {
                    "id": "3",
                    "thumbnail": "/static/img/products/detail/fullwidth/3.jpg"
                }
            ],
            "thumbnail": "/static/img/products/shop/2.jpg",
            "title": "Marshall Kilburn Portable Wireless Speaker",
            "vendor": "Go Pro",
            "price": "42.99",
            "sale": "false",
            "categories": [
                {
                    "id": "2",
                    "value": "electronic",
                    "name": "Consumer Electrics"
                }
            ],
            "brand": [
                {
                    "id": "2",
                    "value": "marshall",
                    "text": "Marshall"
                }
            ]
        }
    }

    """

    def post(self, request, *args, **kwargs):
        product_id = request.data.get('product_id')
        colour = request.data.get('colour')
        size = request.data.get('size')
        product_dict = {}
        if None not in (product_id, colour, size):
            product_details_obj = ProductDetails.objects.filter(product_id=product_id, colour_id__name=colour,
                                                                size_id__name=size)
            if product_details_obj:
                # Getting product details image..
                image = ""
                product_details_image = Images.objects.filter(type="other",
                                                              product_details=product_details_obj[0]).order_by(
                    'id').last()
                if product_details_image:
                    image = image_thumbnail(product_details_image.path)

                else:
                    product_image = Images.objects.filter(type="main", product=product_id).order_by('id').last()
                    if product_image:
                        image = image_thumbnail(product_image.path)

                # getting minimum amount for product details..
                product_amount = product_details_obj[0].amount
                mrp = product_details_obj[0].mrp
                discount = product_details_obj[0].discount
                data = get_product_details_price(product_details_obj[0].id)
                if data:
                    product_amount = data['minimum_amount']
                    mrp = data['mrp']
                    discount = data['discount']

                # getting product details stock..
                product_stock = 0
                stock = get_product_details_stock(product_details_obj[0].id)
                if stock:
                    product_stock = stock

                # getting variants list..
                variants_list = get_product_details_variants(product_details_obj[0].id)

                # getting rating data..
                rating = 0
                rating_length = 0
                review_length = 0
                rating_data = get_product_rating(product_details_obj[0].product_id.id)
                if rating_data:
                    rating = rating_data["rating"]
                    rating_length = rating_data["rating_length"]
                    review_length = rating_data["review_length"]

                review_list = get_product_rating_and_review(product_details_obj[0].product_id.id, "minimum")

                product_dict = {"id": str(product_details_obj[0].id),
                                "variants": variants_list,
                                "thumbnail": str(image),
                                "title": product_details_obj[0].product_id.name,
                                "vendor": product_details_obj[0].user.username,
                                "price": product_amount,
                                "mrp": mrp,
                                "discount": discount,
                                "rating": "True",
                                "quantity": "1",
                                "ratingCount": rating,
                                "rating_length": rating_length,
                                "review_length": review_length,
                                "review_list": review_list,
                                "badge": [],
                                "product_url": product_url + str(product_details_obj[0].product_id.id) + "/",
                                "product_stock": product_stock,
                                "categories": [{
                                    "id": str(
                                        product_details_obj[0].product_id.base_category.sub_category.category.id),
                                    "value": product_details_obj[
                                        0].product_id.base_category.sub_category.category.name,
                                    "name": product_details_obj[
                                        0].product_id.base_category.sub_category.category.name,
                                }],
                                "sub_category": product_details_obj[0].product_id.base_category.sub_category.name,
                                "brand": []
                                }

                return Response(
                    {
                        'status': '1',
                        'message': 'success',
                        'output': product_dict
                    }
                )
            else:
                return Response(
                    {
                        'status': '2',
                        'message': 'success',
                        'output': product_dict
                    }
                )
        else:
            return Response(
                {
                    'status': '3',
                    'message': 'provide all data',
                    'output': product_dict

                }
            )


class SearchProducts(generics.ListAPIView):
    """
    This Api is for Showing search products .
    Url: ecomapi/dashboard_products
    Authentication token required
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    method = get
    input = {
            "input": "Mens-Clothes"
    }
    Response:
        {
    "status": "1",
    "products": [
        {
            "id": "99",
            "variants": [],
            "thumbnail": "https://mudrakwik-storage.s3.amazonaws.com/ecom/watch_2.jpeg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA3AOPBD3KG46TBT62%2F20200624%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20200624T041132Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=fab2fa68902179ddc08caabf139c7a5a5c21a88603cca8f470e4f9ab31b8889c",
            "title": "Titan watch",
            "vendor": "Seller",
            "price": 0,
            "rating": "True",
            "ratingCount": "5",
            "badge": [],
            "categories": [
                {
                    "id": 17,
                    "value": "Mens",
                    "name": "Mens"
                }
            ],
            "brand": []
        }
    ]
    }
    """

    def post(self, request, *args, **kwargs):
        final_product_list = []
        product = 0
        input = request.data.get('input')
        if input is not None:
            serach_data = input.split("-")
            products = 0
            category_list = 0
            subcategory_list = 0
            basecategory_list = 0
            # for data in serach_data:
            #     products = Product.objects.filter(name__icontains=data).values_list('name', flat=True)
            #     category_list = Category.objects.filter(name=data).values_list("name", flat=True)
            #     subcategory_list = SubCategory.objects.filter(name__icontains=data).values_list("name", flat=True)
            #     basecategory_list = BaseCategory.objects.filter(base_name__icontains=data).values_list("base_name",
            #                                                                                            flat=True)
            # products = ProductDetails.objects.filter(status="APPROVED").filter(
            #     Q(product_id__base_category__base_name__in=basecategory_list) |
            #     Q(product_id__base_category__sub_category__category__name__in=category_list) |
            #     Q(product_id__base_category__sub_category__name__in=subcategory_list) |
            #     Q(product_id__name__in=products)
            # ).values_list('product_id', flat=True)
            print(serach_data,'in search product api')
            for data in serach_data:
                products = Product.objects.filter(name__icontains=data).values_list('name', flat=True)[:15]
            print(len(products), 'products')
            products = ProductDetails.objects.filter(status="APPROVED").filter(
                Q(product_id__name__in=products)
            ).values_list('product_id', flat=True)
            if products:
                product_obj = Product.objects.filter(id__in=products)
                for product in product_obj:
                    product_dict = {}
                    image = ""
                    product_image = Images.objects.filter(type="main", product=product.id).order_by(
                        'id').last()
                    if product_image:
                        image = image_thumbnail(product_image.path)
                    minimum_data_dict = get_minimum_amount(product.id)
                    product_minimum_amount = 0
                    mrp = 0
                    discount = 0
                    if minimum_data_dict:
                        product_minimum_amount = minimum_data_dict['minimum_amount']
                        mrp = minimum_data_dict['mrp']
                        discount = minimum_data_dict['discount']
                    variants_list = get_product_variants(product.id)

                    # getting rating data..
                    rating = 0
                    rating_data = get_product_rating(product.id)
                    if rating_data:
                        rating = rating_data["rating"]

                    product_dict = {"id": str(product.id),
                                    "variants": variants_list,
                                    "thumbnail": str(image),
                                    "title": product.name,
                                    "vendor": product.seller.username,
                                    "price": product_minimum_amount,
                                    "mrp": mrp,
                                    "discount": discount,
                                    "rating": "True",
                                    "ratingCount": rating,
                                    "badge": [],
                                    "product_url": product_url + str(product.id) + "/",
                                    "categories": [{
                                        "id": product.base_category.sub_category.category.id,
                                        "value": product.base_category.sub_category.category.name,
                                        "name": product.base_category.sub_category.category.name
                                    }],
                                    "brand": []
                                    }
                    final_product_list.append(product_dict)
                    
        return Response(
            {
                'status': '1',
                'products': final_product_list
            }
        )


class SearchSuggestionAPI(generics.ListAPIView):
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    def post(self, request, *args, **kwargs):
        product_lists = []
        search_keyword = request.data.get('search_keyword')
        suggestion_list = []
        if search_keyword is not None:
            category_list = []
            sub_category_list = []
            base_category_list = []
            product_name_list = []
            keywords = search_keyword.split()
            # getting categories and sub categories according to keywords..
            for keyword in keywords:
                category_list = Category.objects.filter(name__icontains=keyword).values_list('name', flat=True)
                if not category_list:
                    sub_category_list = SubCategory.objects.filter(name__icontains=keyword).values_list('name',
                                                                                                        flat=True)
                    if not sub_category_list:
                        base_category_list = BaseCategory.objects.filter(base_name__icontains=keyword).values_list(
                            'base_name', flat=True)
                        if not base_category_list:
                            product_name_list = Product.objects.filter(name__icontains=keyword).values_list('name',
                                                                                                            flat=True)

            # base_category_objects = Product.objects.filter(
            #     Q(name__in=product_name_list) |
            #     Q(base_category__base_name__in=base_category_list) |
            #     Q(base_category__sub_category__name__in=sub_category_list) |
            #     Q(base_category__sub_category__category__name__in=category_list))

            base_category_objects = Product.objects.filter(
                Q(name__in=product_name_list) |
                Q(base_category__base_name__in=base_category_list))

            if base_category_objects:
                for suggestion in base_category_objects:

                    data = suggestion.name
                    suggestion_list.append({"item": data})

                return Response(
                    {
                        'status': '1',
                        'suggestions': suggestion_list
                    }
                )

            else:
                return Response(
                    {
                        'status': '2',
                        'suggestions': suggestion_list
                    }
                )


from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector


class SearchSuggestionAPI_web(generics.ListAPIView):
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    def post(self, request, *args, **kwargs):
        product_lists = []
        search_keyword = request.data.get('search_keyword')
        suggestion_list = []
        if search_keyword is not None:

            product_objects = Product.objects.filter(
                Q(name__search=search_keyword) |
                Q(base_category__base_name__search=search_keyword) |
                Q(base_category__sub_category__name__search=search_keyword) |
                Q(base_category__sub_category__category__name__search=search_keyword) |
                Q(seller__username__search=search_keyword))

            if product_objects:
                for suggestion in product_objects:
                    data = {"title": suggestion.name, "id": suggestion.id}
                    suggestion_list.append(data)

                return Response(
                    {
                        'status': '1',
                        'suggestions': suggestion_list
                    }
                )

            else:
                return Response(
                    {
                        'status': '2',
                        'suggestions': suggestion_list
                    }
                )


import math


class ProductsForCategoryPagination(generics.ListAPIView):
    """
    This Api is for Showing Products for perticular category.
​
    Url: ecomapi/products_for_category
​
    Authentication token required
​
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
​
    method = post
    parameters:
        {
            'category_name' = "Mens"
        }
​
    Response:
        {
    "status": "1",
    "data":
            {
                "id": "36",
                "variants": [],
                "thumbnail": "image",
                "title": "asdfgh",
                "vendor": "",
                "price": 1350.0,
                "rating": "True",
                "ratingCount": "5",
                "badge": [],
                "categories": [
                    {
                        "id": "1",
                        "value": "Men",
                        "name": "Men"
                    }
                ],
                "brand": []
            },
            {
                "id": "35",
                "variants": [],
                "thumbnail": "image",
                "title": "najshrj",
                "vendor": "",
                "price": 1200.0,
                "rating": "True",
                "ratingCount": "5",
                "badge": [],
                "categories": [
                    {
                        "id": "1",
                        "value": "Men",
                        "name": "Men"
                    }
                ],
                "brand": []
            },
        ],

        """

    def post(self, request, *args, **kwargs):
        product_dict_list = []  # This list contains product dicts.
        previous = False
        next = False
        input = request.data.get('category')
        # input = "Fashion_Store"
        sort_condition = request.data.get('sort_condition')
        # sort_condition = "normal"
        if sort_condition == "normal":
            sort_condition = None
        if "page_id" not in request.data:
            page_count = 0
        else:
            page_count = request.data.get('page_id')
        if page_count is None or not page_count or page_count == 0:
            start_point = 0
            end_point = start_point + 10
        else:
            end_point = page_count * 10
            start_point = end_point - 10

        total_page_count = 0
        divided_count = 1  # This default divided page count..
        if input is not None:
            if "page_id" not in request.data:
                page_count = 0
            else:
                page_count = request.data.get('page_id')
            if page_count is None or not page_count or page_count == 0:
                start_point = 0
                end_point = start_point + 10
            else:
                end_point = page_count * 10
                start_point = end_point - 10

            if "!" in input:
                input_list = input.split('!')
                category = input_list[0]
                product_id_list = []
                mfpl_list = []
                others_list = []
                product_id_list_cnt = None
                category_obj = Category.objects.filter(name=category)
                if category_obj:
                    sub_category = input_list[1]
                    base_name = input_list[2]

                    mfpl_list = list(ProductDetails.objects.filter(product_id__seller__username="MFPL",
                                                              status="APPROVED",
                                                              product_id__base_category__sub_category__category__name=category,
                                                              product_id__base_category__sub_category__name=sub_category,
                                                              product_id__base_category__base_name=base_name).values_list(
                        'product_id', flat=True).distinct())

                    others_list = list(ProductDetails.objects.exclude(product_id__seller__username="MFPL").filter(
                        status="APPROVED",
                        product_id__base_category__sub_category__category__name=category,
                        product_id__base_category__sub_category__name=sub_category,
                        product_id__base_category__base_name=base_name).values_list(
                        'product_id', flat=True).distinct())

                    product_id_list = mfpl_list + others_list
                    product_id_list_cnt = len(product_id_list)


                    if product_id_list_cnt is not None:
                        divided_count = math.ceil(product_id_list_cnt / 10)

                    if product_id_list:
                        sorted_list = []
                        if sort_condition:
                            sorted_list = get_sorted_product_list(product_id_list, sort_condition)
                        else:
                            sorted_list = product_id_list

                        if page_count <= 1:
                            next = True
                            previous = False
                        if page_count == divided_count:
                            previous = True
                            next = False
                        if page_count <= 1 and page_count == divided_count:
                            previous = False
                            next = False
                        if 1 < page_count < divided_count:
                            previous = True
                            next = True

                        if sorted_list:
                            for product_id in sorted_list[start_point:end_point]:
                                product = Product.objects.filter(id=product_id)
                                data = get_product_response_data(product[0])
                                if data:
                                    product_dict_list.append(data)
                            return Response(
                                {
                                    'status': '1',
                                    'data': product_dict_list,
                                    "total_page_count": divided_count,
                                    "all_products_count": product_id_list_cnt,
                                    "previous": previous,
                                    "next": next
                                }
                            )
                    else:
                        return Response(
                            {
                                'status': '2',
                                'message': 'No products found'
                            }
                        )
                else:
                    return Response(
                        {
                            'status': '3',
                            'message': 'Category not found'
                        }
                    )

            else:
                # print("inside else")
                category_name = input
                category_list = ["Fashion_Store", "Flea_Store", "Luxury_Store", "Must_Haves", "Just_Arrived"]
                categorywise_dict = {}  # Main output dict contains all categorywise list of product dict.
                product_dict = {}  # This dict contains product details
                if category_name in category_list:
                    product_id_list = []
                    product_id_list_cnt = None
                    product_id_list = ProductPriority.objects.filter(text_view=category_name,
                                                                     view_type__in=['Both', 'Back'],
                                                                     dashboard_status=True).order_by(
                        "priority").values_list("product", flat=True).distinct()
                    if product_id_list:
                        product_dict_list = []  # This list contains product dicts..
                        product_id_list = ProductDetails.objects.filter(
                            product_id__in=product_id_list,
                            status="APPROVED").values_list(
                            'product_id', flat=True).distinct()
                        product_id_list_cnt = ProductDetails.objects.filter(
                            product_id__in=product_id_list,
                            status="APPROVED").values_list(
                            'product_id', flat=True).distinct().count()

                        if product_id_list_cnt is not None:
                            divided_count = math.ceil(product_id_list_cnt / 10)

                        if product_id_list:
                            sorted_list = []
                            if sort_condition:
                                sorted_list = get_sorted_product_list(product_id_list, sort_condition)
                            else:
                                sorted_list = product_id_list

                            if page_count <= 1:
                                next = True
                                previous = False
                            if page_count == divided_count:
                                previous = True
                                next = False
                            if page_count <= 1 and page_count == divided_count:
                                previous = False
                                next = False
                            if 1 < page_count < divided_count:
                                previous = True
                                next = True

                            if sorted_list:
                                for product_id in sorted_list[start_point:end_point]:
                                    product = Product.objects.filter(id=product_id)
                                    data = get_product_response_data(product[0])
                                    if data:
                                        product_dict_list.append(data)

                        return Response(
                            {
                                'status': '1',
                                'message': "success",
                                'data': product_dict_list,
                                "total_page_count": divided_count,
                                "all_products_count": product_id_list_cnt,
                                "previous": previous,
                                "next": next
                            }
                        )
                    else:
                        return Response(
                            {
                                'status': '2',
                                'message': 'Category not found'
                            }
                        )
                else:
                    product_id_list = []
                    product_id_list_cnt = None
                    # print('category_list_values', category_list_values)
                    if category_name == "Sabse_Sasta_Store":
                        product_id_list = list(ProductDetails.objects.filter(
                            product_id__seller__username="MFPL", status="APPROVED").exclude(
                            product_id__base_category__sub_category__category__name="Grocery").values_list(
                            'product_id', flat=True).distinct())

                        if product_id_list:
                            product_id_list = random.sample(product_id_list, len(product_id_list))


                        product_id_list_cnt = len(product_id_list)

                    else:
                        get_category_obj = Category.objects.filter(name=category_name)
                        if get_category_obj:
                            product_dict_list = []  # This list contains product dicts..
                            mfpl_list = []
                            others_list = []

                            mfpl_list = list(ProductDetails.objects.filter(product_id__seller__username="MFPL",
                                                                      product_id__base_category__sub_category__category__name=
                                                                      get_category_obj[0].name,
                                                                      status="APPROVED").values_list(
                                'product_id', flat=True).distinct())

                            others_list = list(ProductDetails.objects.exclude(product_id__seller__username="MFPL").filter(
                                product_id__base_category__sub_category__category__name=get_category_obj[0].name,
                                status="APPROVED").values_list(
                                'product_id', flat=True).distinct())

                            product_id_list = mfpl_list + others_list
                            product_id_list_cnt = len(product_id_list)



                    if product_id_list_cnt is not None:
                        divided_count = math.ceil(product_id_list_cnt / 10)

                    if product_id_list:
                        sorted_list = []
                        if sort_condition:
                            sorted_list = get_sorted_product_list(product_id_list, sort_condition)
                        else:
                            sorted_list = product_id_list

                        if page_count <= 1:
                            next = True
                            previous = False
                        if page_count == divided_count:
                            previous = True
                            next = False
                        if page_count <= 1 and page_count == divided_count:
                            previous = False
                            next = False
                        if 1 < page_count < divided_count:
                            previous = True
                            next = True

                        if sorted_list:
                            for product_id in sorted_list[start_point:end_point]:
                                product = Product.objects.filter(id=product_id)
                                data = get_product_response_data(product[0])
                                if data:
                                    product_dict_list.append(data)

                    return Response(
                        {
                            'status': '1',
                            'message': "success",
                            'data': product_dict_list,
                            "total_page_count": divided_count,
                            "all_products_count": product_id_list_cnt,
                            "previous": previous,
                            "next": next
                        }
                    )
        else:
            return Response(
                {
                    'status': '3',
                    'message': 'Please provide category'
                }
            )


class ProductAllDetailsAPI(generics.ListAPIView):
    """
        This Api is to show all data of the product.
        Url: ecomapi/dashboard_products
        Authentication token required
                [
                    {
                        "key":"Authorization",
                        "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                    }
                ]
        method = post
        Response:
            {
    "status": "1",
    "products": [
                {
                    "id": "96",
                    "variants": [
                        {
                            "id": "58",
                            "thumbnail": "https://mudrakwik-storage.s3.amazonaws.com/ecom/watch_1.jpeg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA3AOPBD3KG46TBT62%2F20200723%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20200723T042852Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=1c550687e0e5bf2179ba00f725e3b4f49bf2bb061a4c79159c933b1f46e33114"
                        },
                        {
                            "id": "54",
                            "thumbnail": "https://mudrakwik-storage.s3.amazonaws.com/ecom/hp_white.jpeg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA3AOPBD3KG46TBT62%2F20200723%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20200723T042852Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=906db467869dd79932c591d1ae7f8ba4ce0bfc4cd2c3a84e681a909123893900"
                        }
                    ],
                    "thumbnail": "https://mudrakwik-storage.s3.amazonaws.com/ecom/watch_1.jpeg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA3AOPBD3KG46TBT62%2F20200723%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20200723T042852Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=1c550687e0e5bf2179ba00f725e3b4f49bf2bb061a4c79159c933b1f46e33114",
                    "title": "Fast-track watch 001",
                    "vendor": "Seller",
                    "price": 7.0,
                    "rating": "True",
                    "ratingCount": "5",
                    "badge": [],
                    "colour": "blue",
                    "description": "",
                    "height": null,
                    "length": null,
                    "width": null,
                    "weight": 16.0,
                    "product_stock": 0,
                    "categories": [
                        {
                            "id": "18",
                            "value": "Women",
                            "name": "Women"
                        }
                    ],
                    "sub_category": "Shoes",
                    "brand": []
                }
            ]
        }
        """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    def post(self, request, *args, **kwargs):
        product_id = request.data.get('product_id')
        if product_id and product_id is not None:
            product = Product.objects.filter(id=product_id)
            product_dict = {}
            if product:
                # Getting Product Image here..
                image = ""
                product_image = Images.objects.filter(type="main", product=product[0].id).order_by('id').last()
                if product_image:
                    image = image_thumbnail(product_image.path)

                # getting minimum amount for product details..
                minimum_data_dict = get_minimum_amount_with_details_id(product[0].id)
                product_minimum_amount = 0
                mrp = 0
                discount = 0
                product_stock = 0
                get_product_details_obj = None
                if minimum_data_dict:
                    product_minimum_amount = minimum_data_dict['minimum_amount']
                    mrp = minimum_data_dict['mrp']
                    discount = minimum_data_dict['discount']
                    product_details_id = minimum_data_dict['product_details_id']

                    if product_details_id is not None:
                        # getting product details stock..
                        get_product_details_obj = ProductDetails.objects.filter(id=product_details_id)
                        stock = get_product_details_stock(product_details_id)
                        if stock:
                            product_stock = stock

                # getting variants list..
                variants_list = get_product_variants(product[0].id)

                # getting rating data..
                rating = 0
                rating_length = 0
                review_length = 0
                rating_data = get_product_rating(product[0].id)
                if rating_data:
                    rating = rating_data["rating"]
                    rating_length = rating_data["rating_length"]
                    review_length = rating_data["review_length"]

                review_list = get_product_rating_and_review(product[0].id, "minimum")

                product_dict = {"id": str(product[0].id),
                                "variants": variants_list,
                                "thumbnail": str(image),
                                "title": product[0].name,
                                "vendor": product[0].seller.username,
                                "price": product_minimum_amount,
                                "rating": "True",
                                "ratingCount": rating,
                                "rating_length": rating_length,
                                "review_length": review_length,
                                "review_list": review_list,
                                "badge": [],
                                "product_url": product_url + str(product[0].id) + "/",
                                "colour": get_product_details_obj[0].colour_id.name,
                                "description": get_product_details_obj[0].description,
                                "height": get_product_details_obj[0].height,
                                "length": get_product_details_obj[0].Length,
                                "width": get_product_details_obj[0].width,
                                "weight": get_product_details_obj[0].weight,
                                "product_stock": product_stock,
                                "categories": [{
                                    "id": str(product[0].base_category.sub_category.category.id),
                                    "value": product[0].base_category.sub_category.category.name,
                                    "name": product[0].base_category.sub_category.category.name
                                }],
                                "sub_category": product[0].base_category.sub_category.name,
                                "brand": []
                                }
            return Response(
                {
                    'status': "1",
                    'products': [product_dict]
                }
            )
        else:
            return Response(
                {
                    'status': "2",
                    'message': 'please provide product_id'
                }
            )


class NewCategoryListAPI(generics.ListAPIView):
    """
    This Api is for Showing list of categories with its related image.

    Url: ecomapi/new_category_list

    method = get

    Response:
        {
    "status": 1,
    "category_list": [
        {
            "sub_category": {
                "Footwear": [
                    "Men!Footwear!Shoes"
                ],
                "Cloth": [
                    "Men!Cloth!Men_Cloths"
                ],
                "Watches": [
                    "Men!Watches!Watches"
                ]
            },
            "category": "Men"
        },
        {
            "sub_category": {
                "Shoes": [
                    "Women!Shoes!Sandal"
                ],
                "Laptop": [
                    "Women!Laptop!Sandal"
                ]
            },
            "category": "Women"
        },
        {
            "category": "Electronics"
        },
        {
            "sub_category": {
                "Mobiles": [
                    "Home!Mobiles!Basicphone"
                ]
            },
            "category": "Home"
        },
        {
            "sub_category": {
                "Demo_Sub": [
                    "Kids_Wear!Demo_Sub!Demo_Subs"
                ]
            },
            "category": "Kids_Wear"
        }
    ]
}
    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        category_list = []
        categories = ProductDetails.objects.filter(status="APPROVED").order_by(
            'product_id__base_category__sub_category__category__priority').values_list(
            'product_id__base_category__sub_category__category__name', flat=True).distinct()
        if categories:
            for category in categories:
                category_dict = {}
                get_sub_categories = SubCategory.objects.filter(category__name=category).values_list('name', flat=True)
                if get_sub_categories:
                    sub_category_dict_list = []
                    for sub_category in get_sub_categories:
                        sub_category_dict = {}
                        base_category_values_list = []
                        get_base_category = BaseCategory.objects.filter(sub_category__name=sub_category,
                                                                        sub_category__category__name=category).values_list(
                            'base_name', flat=True)
                        if get_base_category:
                            for base_category in get_base_category:
                                base_category_dict = {}
                                base_category_name = category + "!" + sub_category + "!" + base_category
                                base_category_dict["value"] = base_category
                                base_category_values_list.append(base_category_dict)
                        
                        if base_category_values_list:
                            sub_category_dict["id"] = sub_category
                            sub_category_dict["value"] = base_category_values_list
                            sub_category_dict_list.append(sub_category_dict)
                            category_dict["sub_category"] = sub_category_dict_list

                category_dict["category"] = category
                category_list.append(category_dict)

        return Response(
            {
                "status": 1,
                "category_list": category_list
            }
        )


def get_sorted_product_list(product_id_list, sorted_condition):
    get_sorted_products = []
    sorted_id_list = []
    if product_id_list:
        try:
            get_product_obj = []
            if sorted_condition == "low":
                get_product_obj = ProductMinMaxAMount.objects.filter(product__in=product_id_list).order_by(
                    'min_amount').values_list('product', flat=True)
            if sorted_condition == "high":
                get_product_obj = ProductMinMaxAMount.objects.filter(product__in=product_id_list).order_by(
                    '-max_amount').values_list('product', flat=True)
            if get_product_obj:
                sorted_id_list = get_product_obj
            return sorted_id_list

        except Exception as e:
            print('inside get_sorted_product_list', e)
            return sorted_id_list


class DashboardProductsApiNewWithPriority(generics.ListAPIView):
    """
    This Api is for Showing priority wise products on the main Dashboard.

    Url: ecomapi/dashboard_products_new_priority/

    Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]

    method = post

    Response:
        {
    "status": "1",
    "products": {
        "Men": [
            {
                "id": "36",
                "variants": [],
                "thumbnail": "image",
                "title": "asdfgh",
                "vendor": "",
                "price": 1350.0,
                "rating": "True",
                "ratingCount": "5",
                "badge": [],
                "categories": [
                    {
                        "id": "1",
                        "value": "Men",
                        "name": "Men"
                    }
                ],
                "brand": []
            },
            {
                "id": "35",
                "variants": [],
                "thumbnail": "image",
                "title": "najshrj",
                "vendor": "",
                "price": 1200.0,
                "rating": "True",
                "ratingCount": "5",
                "badge": [],
                "categories": [
                    {
                        "id": "1",
                        "value": "Men",
                        "name": "Men"
                    }
                ],
                "brand": []
            },
        ],
    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        category_name = request.data.get('category_name')
        categorywise_dict = {}  # Main output dict contains all categorywise list of product dict.
        product_dict = {}  # This dict contains product details
        if category_name:
            product_dict_list = []  # This list contains product dicts..
            # To get prioritywise product used ProductPriority
            product_priority = ProductPriority.objects.filter(dashboard_status=True,
                                                              text_view=category_name,
                                                              view_type__in=["Front", "Both"]).values_list("product",
                                                                                                           flat=True).order_by(
                'priority')
            if product_priority:
                for products in product_priority:
                    product_id_list = ProductDetails.objects.filter(product_id=products, status="APPROVED").values_list(
                        "product_id",
                        flat=True)
                    if product_id_list:
                        products = Product.objects.filter(id__in=product_id_list)
                        if products:
                            for product in products:
                                product_dict = {}
                                # Getting Product Image here..
                                image = ""
                                product_image = Images.objects.filter(type="main", product=product.id).order_by(
                                    'id').last()
                                if product_image:
                                    image = image_thumbnail(product_image.path)

                                # getting minimum amount for product details..
                                minimum_data_dict = get_minimum_amount(product.id)
                                product_minimum_amount = 0
                                mrp = 0
                                discount = 0
                                if minimum_data_dict:
                                    product_minimum_amount = minimum_data_dict['minimum_amount']
                                    mrp = minimum_data_dict['mrp']
                                    discount = minimum_data_dict['discount']

                                # getting variants list..
                                variants_list = get_product_variants(product.id)

                                # getting rating data..
                                rating = 0
                                rating_data = get_product_rating(product.id)
                                if rating_data:
                                    rating = rating_data["rating"]

                                product_dict = {"id": str(product.id),
                                                "variants": variants_list,
                                                "thumbnail": str(image),
                                                "title": product.name,
                                                "vendor": product.seller.username,
                                                "price": product_minimum_amount,
                                                "mrp": mrp,
                                                "discount": discount,
                                                "rating": "True",
                                                "ratingCount": rating,
                                                "badge": [],
                                                "product_url": product_url + str(product.id) + "/",
                                                "categories": [{
                                                    "id": str(product.base_category.sub_category.category.id),
                                                    "value": product.base_category.sub_category.category.name,
                                                    "name": product.base_category.sub_category.category.name
                                                }],
                                                "brand": []
                                                }
                                product_dict_list.append(product_dict)
            categorywise_dict.update({category_name: product_dict_list})
            return Response(
                {
                    'status': '1',
                    'products': categorywise_dict
                }
            )
        return Response(
            {
                'status': '2',
                'products': categorywise_dict
            }
        )


class BannerApi(generics.ListAPIView):
    """
       This Api is to show banner details.
       This Api is for Showing priority wise products on the main Dashboard.
       Url: ecomapi/banner_api/
       Url: ecomapi/dashboard_products_new_priority/
       Authentication token required
   ]
       method = post
       parameters:{
        'text_view': 'Home'
       }
   Response:
       {
       "status": 1,
       "data": [
           {
               "path": "banner_images/women_top.jpeg",
               "product": "99",
           },
           {
               "path": "banner_images/01_NF9eInU.png",
               "product": "",
                   }
               ]
           }
           """

    def post(self, request, *args, **kwargs):
        banner_dict = {}
        banner_list = []
        text_view = request.data.get('text_view')
        # text_view = "Home1"
        banner_obj = Banner.objects.filter(status=True, text_view=text_view).order_by("priority")
        if banner_obj:
            for banner in banner_obj:
                product_id = ''
                image_path = ""
                if banner.product:
                    product_id = banner.product.id
                if banner.path:
                    image_path = image_thumbnail(banner.path)
                banner_dict = {
                    'path': str(image_path),
                    'product': product_id,
                }
                banner_list.append(banner_dict)
            return Response(
                {
                    'status': 1,
                    'data': banner_list
                }
            )
        else:
            return Response(
                {
                    'status': 2,
                    'data': banner_list
                }
            )


class OfferBannerApi(generics.ListAPIView):

    def get(self, request, *args, **kwargs):
        banner_dict = {}
        banner_list = []
        import datetime

        current_date = datetime.datetime.now()

        offer_banner_obj = OfferBanner.objects.filter(type__in=["App", "Both"], status=True,
                                                      start_date__lte=current_date,
                                                      end_date__gte=current_date).order_by("priority")
        if offer_banner_obj:
            for banner in offer_banner_obj:
                image_path = ""


                if banner.path:
                    image_path = image_thumbnail(banner.path)
                offer_data = {
                    "redirect": False,
                    "redirect_to": None,
                    "category": None
                }
                # print("banner", banner.priority)
                if banner.priority == 9:
                    offer_data = {
                        "redirect": True,
                        "redirect_to": "category",
                        "category": "Grocery"
                    }
                if banner.priority == 8:
                    offer_data = {
                        "redirect": True,
                        "redirect_to": "category",
                        "category": "Sabse_Sasta_Store"
                    }
                offer_data["path"] = image_path
                banner_list.append(offer_data)

            return Response(
                {
                    'status': 1,
                    'data': banner_list
                }
            )
        else:
            return Response(
                {
                    'status': 2,
                    'data': banner_list
                }
            )


class OfferBannerApiWeb(generics.ListAPIView):

    def get(self, request, *args, **kwargs):
        banner_dict = {}
        banner_list = []
        import datetime

        current_date = datetime.datetime.now()

        offer_banner_obj = OfferBanner.objects.filter(type__in=["Web", "Both"], status=True,
                                                      start_date__lte=current_date,
                                                      end_date__gte=current_date).order_by("priority")
        if offer_banner_obj:
            for banner in offer_banner_obj:
                image_path = ""

                if banner.path:
                    image_path = image_thumbnail(banner.path)

                banner_list.append({"path": image_path})
            return Response(
                {
                    'status': 1,
                    'data': banner_list
                }
            )
        else:
            return Response(
                {
                    'status': 2,
                    'data': banner_list
                }
            )


class UserOfferApi(generics.ListAPIView):
    """
    method : get
    response: {
    "status": 1,
    "data": [
        {
            "title": "women",
            "path": "",
            "status": "True",
            "start_date": "2020-10-27T00:00:00",
            "end_date": "2020-10-27T00:00:00",
            "description": "offer"
        }
     ]
    }

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        user_offer_dict = {}
        user_offer_list = []
        user = request.user.id
        user_offer_obj = UserOffer.objects.filter(user=user, status=True, is_read=False)
        if user_offer_obj:
            for user_offer in user_offer_obj:
                image_path = ""

                if user_offer.offer.path:
                    image_path = image_thumbnail(user_offer.offer.path)

                user_offer_dict = {
                    "title": user_offer.offer.title,
                    "offer_id": user_offer.offer.id,
                    "path": image_path,
                    "status": user_offer.offer.status,
                    "start_date": user_offer.offer.start_date,
                    "end_date": user_offer.offer.end_date,
                    "description": user_offer.offer.description

                }

                user_offer_list.append(user_offer_dict)
            return Response(
                {
                    'status': 1,
                    'data': user_offer_list
                }
            )
        else:
            return Response(
                {
                    'status': 2,
                    'data': user_offer_list
                }
            )


class UserOfferIsReadApi(generics.ListAPIView):
    """
    parameter : offer_id
    {
    "status": 1,
    "message": "success"
    }
    else
    {
    "status": 1,
    "message": "failed"

    }
    This api update useroffer by offer_id..
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        user_id = request.user.id
        offer_id = request.data.get('offer_id')
        if offer_id:
            user_offer_obj = UserOffer.objects.filter(offer__id=offer_id, user=user_id, status=True)
            if user_offer_obj:
                update_offer = UserOffer.objects.filter(id=user_offer_obj[0].id).update(is_read=True)
                if update_offer:
                    return Response(
                        {
                            'status': 1,
                            'message': "success"
                        }
                    )

            else:
                return Response(
                    {
                        'status': 2,
                        'message': "failed"
                    }
                )

        else:
            return Response(
                {
                    'status': 3,
                    'message': "offer_id is null"
                }
            )


class ProductReviewsRatingAPI(generics.ListAPIView):
    """
        method : post
        parameter : rating,review,product_id
        response :   success : {
                            'status': 1,
                            'message': "success"
                        }
                    error :  {
                            'status': 2,
                            'message': "Something went wrong"
                        }

                         {
                        'status': 3,
                        'message': "Product Id should not None"
                    }
        This API save review and Rating for Product..
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        user = request.user
        rating = request.data.get('rating')
        review = request.data.get('review')
        product_id = request.data.get('product_id')
        review_image = None
        if "review_image" in request.data:
            review_image = request.data.get('review_image')
        if product_id is not None:
            product_obj = Product.objects.filter(id=product_id).first()
            if product_obj:
                add_rating = None
                add_review = None
                if rating or review:
                    path = None
                    img_name = ""
                    if review_image is not None and review_image != "":
                        review_image = str(review_image)
                        try:
                            # Add Customer Return Products Image..
                            review_image = review_image.replace("data:image/jpeg;base64,", "")
                            p = "media/review_images/"
                            from ecom.views import generate_unique_id
                            img_name = "RV" + str(generate_unique_id()) + ".jpeg"
                            path = p + img_name
                            with open(path, "wb") as fh:
                                imgdata = base64.b64decode(review_image)
                                fh.write(imgdata)
                            p = "ecom/"
                            path = p + img_name

                        except Exception as e:
                            print("Inside Exception of Review Image upload 1111", e)
                            import sys
                            import os
                            print('-----Exception in bulk stock upload-----')
                            print(e.args)
                            exc_type, exc_obj, exc_tb = sys.exc_info()
                            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                            print(exc_type, fname, exc_tb.tb_lineno)

                    # if rating is not None and rating != 0:
                    # Adding Product Rating in ProductRating Table..
                    add_rating = ProductRating.objects.create(product=product_obj, rating=rating, review=review,
                                                              path=path, user=user)

                    if path is not None and path != "" and img_name != "":
                        try:
                            from django.conf import settings
                            base_path = settings.BASE_DIR + "/media/review_images/"
                            file_path = "review_images/"
                            new_base_path = base_path + str(img_name)
                            new_file_path = file_path + str(img_name)

                            from MApi.botofile import upload_image
                            upload_image(new_base_path, new_file_path)

                        except Exception as e:
                            print("Inside Exception of Review Image upload 2222", e)
                            import sys
                            import os
                            print('-----Exception in bulk stock upload-----')
                            print(e.args)
                            exc_type, exc_obj, exc_tb = sys.exc_info()
                            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                            print(exc_type, fname, exc_tb.tb_lineno)

                if add_rating or add_review:
                    return Response(
                        {
                            'status': 1,
                            'message': "success"
                        }
                    )

                else:
                    return Response(
                        {
                            'status': 2,
                            'message': "Something went wrong"
                        }
                    )

            else:
                return Response(
                    {
                        'status': 3,
                        'message': "Product Id should not None"
                    }
                )


def get_product_rating(product_id):
    """
    parameter: product_id
    This function return data with rating,rating_length and review_length by passing
    product id in ProductRating.
    """
    data = {}
    rating = 0
    rating_length = 0
    review_length = 0
    if product_id is not None:
        get_rating_obj = ProductRating.objects.filter(product=product_id)
        if get_rating_obj:
            rating = get_rating_obj[0].avg_rating()
            rating_length = get_rating_obj[0].no_of_rating()
            review_length = get_rating_obj[0].no_of_reviews()

    data["rating"] = rating
    data["rating_length"] = rating_length
    data["review_length"] = review_length

    return data


def get_product_rating_and_review(product_id, capacity):
    """
    parameter : product_id
    parameter : capacity
    This function returns products reviews and rating..
    """
    review_list = []
    if product_id is not None:
        try:
            get_rating_obj = None
            if capacity == "minimum":
                get_rating_obj = ProductRating.objects.filter(product=product_id, re_status=True)[:3]
            if capacity == "maximum":
                get_rating_obj = ProductRating.objects.filter(product=product_id, re_status=True)

            if get_rating_obj:
                for rating in get_rating_obj:
                    review_dict = {}
                    user_name = ""
                    review = ""
                    if rating.user:
                        from EApi.auth import get_users_personal_name
                        name = get_users_personal_name(rating.user.username)
                        if name:
                            user_name = name
                        else:
                            user_name = rating.user.username

                    if rating.review:
                        review = rating.review

                    review_dict["user"] = user_name
                    review_dict["rating"] = rating.rating
                    review_dict["review"] = review

                    review_list.append(review_dict)

        except Exception as e:
            print("Inside Exception of get_product_rating_and_review", e)

    return review_list


class ProductReviewsAPI(generics.ListAPIView):
    """
         method : post
         parameter : product_id
         response :  {
                "review_list": []
            }
         This API gives products revies list which is get by function get_product_rating_and_review.

    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        review_list = []
        product_id = request.data.get("product_id")
        if product_id:
            review_list = get_product_rating_and_review(product_id, "maximum")

        return Response(
            {
                "review_list": review_list
            }
        )


class ReturnCategoryApi(generics.ListAPIView):
    def get(self, request, *args, **kwargs):
        ctx = {}
        list1 = []
        category_name = None
        sub_category_name = None
        url = None
        megaItems = []
        Category = []
        product_list = ProductDetails.objects.filter(status="APPROVED").values_list('product_id', flat=True).order_by(
            '-product_id__base_category__sub_category__category__name')
        product_obj = Product.objects.filter(id__in=product_list).values_list(
            "base_category__sub_category__category__name", flat=True).distinct().order_by(
            '-base_category__sub_category__category__name')
        if product_obj:
            for category in product_obj:
                category_dict = {}
                megaContent = []
                category_name = category
                product_obj_1 = Product.objects.filter(id__in=product_list,
                                                       base_category__sub_category__category__name=category).values_list(
                    "base_category__sub_category__name", flat=True).distinct()
                if product_obj_1:
                    for sub_category in product_obj_1:
                        sub_category_dict = {}
                        megaItems = []
                        sub_category_name = sub_category
                        product_obj_2 = Product.objects.filter(id__in=product_list,
                                                               base_category__sub_category__name=sub_category).values_list(
                            "base_category__base_name", flat=True).distinct()
                        if product_obj_2:
                            for base_category in product_obj_2:
                                base_category_dict = {}
                                url = str(category) + "!" + str(sub_category) + "!" + str(base_category)
                                megaItems.append({
                                    "text": base_category,
                                    "url": url
                                })
                        sub_category_dict.update({
                            "heading": sub_category,
                            "url": sub_category,
                            "megaItems": megaItems
                        })
                        megaContent.append(sub_category_dict)
                category_dict.update({
                    "text": category,
                    "mega": "true",
                    "megaContent": megaContent
                })
                Category.append(category_dict)
        ctx["category"] = Category
        list1 = ctx
        return Response(
            {
                'status': 1,
                'data': list1
            }
        )


class ShopByCategoryAPI(generics.ListAPIView):
    """
    This Api is for Showing list of categories with its category, subactegory, basecategory.
    Url: ecomapi/shop_by_category_api/
    method = get
    Response:
    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    def get(self, request, *args, **kwargs):
        category_list = []
        categories = ProductDetails.objects.filter(status="APPROVED").order_by(
            'product_id__base_category__sub_category__category__priority').values_list(
            'product_id__base_category__sub_category__category__name', flat=True).distinct()
        if categories:
            for category in categories:
                sub_category_dict_list = []
                category_dict = {}
                get_sub_categories = Product.objects.filter(
                    base_category__sub_category__category__name=category).values_list(
                    'base_category__sub_category__name', flat=True).distinct()
                if get_sub_categories:
                    for sub_category in get_sub_categories:
                        sub_category_dict = {}
                        base_category_values_list = []
                        get_base_category = Product.objects.filter(base_category__sub_category__name=sub_category,
                                                                   base_category__sub_category__category__name=category).values_list(
                            'base_category__base_name', flat=True).distinct()
                        if get_base_category:
                            for base_category in get_base_category:
                                base_category_dict = {}
                                base_category_name = category + "!" + sub_category + "!" + base_category
                                base_category_dict = {
                                    "text": base_category,
                                    "url": base_category_name
                                }
                                base_category_values_list.append(base_category_dict)
                        sub_category_dict = {
                            "heading": sub_category,
                            "url": sub_category,
                            "megaItems": base_category_values_list
                        }
                        sub_category_dict_list.append(sub_category_dict)
                category_dict = {
                    "text": category,
                    "url": category,
                    "mega": "true",
                    "megaContent": sub_category_dict_list
                }
                category_list.append(category_dict)
        return Response(
            {
                "category": category_list
            }
        )


class ProductMetaAPI(generics.ListAPIView):
    """
    method: POST
    parameter: product_id
    response: {
                'data': {"name": ""}
            }
    This api return product name by passing product id in Product.
    """
    def post(self, request, *args, **kwargs):
        product_id = request.data.get('product_id')
        name = None
        if product_id and product_id is not None:
            product_id = product_id.split("/")[-1]
            product_id = int(product_id)
            name = None
            if product_id:
                product = Product.objects.filter(id=product_id)
                if product:
                    name = product[0].name
        return Response(
            {
                'data': {"name": name}
            }
        )


from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector

class ProductAPI(generics.ListAPIView):
    """
    method: post
    parameter: product_id
    response:
            success:   {
                    "status": "success",
                    'data': {"title": "",
                             "description": "",
                             "path": ""}
                }
            error:   {
                    "status": "error",
                    'data': {}
                }

    This api return image title , description and path from Images using product id.
    """
    def post(self, request, *args, **kwargs):
        if "product_id" in request.data:
            product_id = request.data.get('product_id')
            name = None
            description = None
            path = None
            status = "No data found"
            if product_id and product_id is not None:
                product = Images.objects.filter(product__product_id=product_id).values_list("product__name",
                                                                                            "product__description",
                                                                                            "product__product_id",
                                                                                            "path")
                if product:
                    status = "success"
                    name = product[0][0]
                    description = product[0][1]
                    path = product[0][3]
            return Response(
                {
                    "status": status,
                    'data': {"title": name,
                             "description": description,
                             "path": path}
                }
            )
        else:
            return Response(
                {
                    "status": "error",
                    'data': {}
                }
            )




