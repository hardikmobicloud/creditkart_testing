import json
import random
from datetime import datetime
from datetime import timedelta

from LoanAppData.views import snd_exc
from MApi import user_serializers
from django.apps.registry import apps
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics
from rest_framework.response import Response

from .ecom_email import email

Order = apps.get_model('ecom', 'Order')
OrderDetails = apps.get_model('ecom', 'OrderDetails')
OrderDetailsOrder = apps.get_model('ecom', 'orderdetails_order_id')
LogisticOrder = apps.get_model('ecom', 'LogisticOrder')
SellerWarehouse = apps.get_model('ecom', 'SellerWarehouse')
Personal = apps.get_model('User', 'Personal')
OrderStatus = apps.get_model('ecom', 'OrderStatus')
PincodeMaster = apps.get_model('StaticData', 'PincodeMaster')
OrderLocation = apps.get_model('ecom', 'OrderLocation')
Images = apps.get_model('ecom', 'Images')
ReturnOrder = apps.get_model('ecom', 'ReturnOrder')
ShiprocketOrder = apps.get_model('ecom', 'ShiprocketOrder')

# test_access_token = settings.test_access_token
# test_secret_key = settings.test_secret_key

# Testing credentials
# test_access_token = "a0b54163b3b5a3cfbd7ca7c0bc760d55"
# test_secret_key = "1b3920fe242c75550f688d1622e5aecc"

# Live credentials
test_access_token = "3bd44d2f08f9e9fb16b73920c4e34152"
test_secret_key = "f0987beea6b2933c4f44a1b1f1164bc7"


def get_logistics_agent(pin_code):
    """ params: pin_code

        Sample response:
         {
          "status": "success",
          "status_code": 200,
          "data":
                {
                    "400067":
                        {
                          "xpressbees":
                                         {
                                         "prepaid": "Y",
                                         "cod": "Y",
                                         "pickup": "Y",
                                         "district": "MAHARASHTRA",
                                         "state_code": "MAHARASHTRA",
                                         "sort_code": "MAHARASHTRA"
                                         },
                          "fedex":
                                 {
                                 "prepaid": "Y",
                                 "cod": "Y",
                                 "pickup": "Y",
                                 "district": "MH",
                                 "state_code": "MH",
                                 "sort_code": "MUMBAI"
                                 },
                          "delhivery":
                                     {
                                     "prepaid": "Y",
                                     "cod": "Y",
                                     "pickup": "Y",
                                     "district": "MUMBAI",
                                     "state_code": "MH",
                                     "sort_code": "MUM/MAL"
                                     }
                        }
                    }
                }
            }

        error response : {"status":"error","status_code":200,"message":"Invalid Request"}

        """

    url = "https://manage.ithinklogistics.com/api_v2/pincode/check.json"
    payload = {
        "data": {
            "pincode": pin_code,
            "access_token": test_access_token,
            "secret_key": test_secret_key
        }
    }

    headers = {
        'content-type': "application/json"
    }

    response = requests.request("POST", url, data=json.dumps(payload), headers=headers)

    delivery_pickup_list = []  # This is the delivery agents list that can give delivery and Pickup facility.
    delivery_list = []  # This is the delivery agents list that can give only delivery facility.
    logistics = None
    if response:
        resp_data = response.json()
        status = resp_data['status']
        if status == "success":
            delivery_agents_dict = resp_data['data'][pin_code]
            if delivery_agents_dict:
                delivery_agents_dict.pop('state_name', None)
                delivery_agents_dict.pop('city_name', None)
                delivery_agents = delivery_agents_dict.keys()
                for agent in delivery_agents:
                    if agent not in ["Shadowfax", "shadowfax"]:
                        # Here we check all the delivery agents with delivery and pickup with "Y".
                        prepaid = delivery_agents_dict[agent]['prepaid']
                        pickup = delivery_agents_dict[agent]['pickup']
                        if prepaid == pickup == "Y":
                            delivery_pickup_list.append(agent)
                        elif prepaid == "Y" and pickup == "N":
                            delivery_list.append(agent)

                if delivery_pickup_list:
                    # This means Pickup and Delivery is available for this Address..
                    logistics = random.choice(delivery_pickup_list)
                else:
                    logistics = random.choice(delivery_list)

                if logistics:
                    return logistics

                else:
                    return logistics


def DeliveryAvailability(pin_code):
    """
    This function gives
    """
    url = "https://manage.ithinklogistics.com/api_v2/pincode/check.json"
    payload = {
        "data": {
            "pincode": pin_code,
            "access_token": test_access_token,
            "secret_key": test_secret_key
        }
    }

    headers = {
        'content-type': "application/json"
    }

    response = requests.request("POST", url, data=json.dumps(payload), headers=headers)
    delivery_pickup_list = []  # This is the delivery agents list that can give delivery and Pickup facility.
    delivery_list = []  # This is the delivery agents list that can give only delivery facility.
    is_delivery = False
    if response:
        resp_data = response.json()
        status = resp_data['status']
        logistics = None
        if status == "success":
            delivery_agents_dict = resp_data['data'][pin_code]
            if delivery_agents_dict:
                delivery_agents_dict.pop('state_name', None)
                delivery_agents_dict.pop('city_name', None)
                delivery_agents = delivery_agents_dict.keys()
                for agent in delivery_agents:
                    # Here we check all the delivery agents with delivery and pickup with "Y".
                    if agent not in ["Shadowfax", "shadowfax"]:
                        if delivery_agents_dict[agent]:
                            prepaid = delivery_agents_dict[agent]['prepaid']
                            pickup = delivery_agents_dict[agent]['pickup']
                            if prepaid == pickup == "Y":
                                delivery_pickup_list.append(agent)
                            elif prepaid == "Y" and pickup == "N":
                                delivery_list.append(agent)

                if delivery_pickup_list:
                    # This means Pickup and Delivery is available for this Address..
                    logistics = random.choice(delivery_pickup_list)
                else:
                    if delivery_list:
                        logistics = random.choice(delivery_list)
                    else:
                        logistics = None

        if logistics:
            is_delivery = True
            return is_delivery

        else:
            is_delivery = False
            return is_delivery


class EcomValidatePincodeAPIView(generics.ListAPIView):
    """
        To validate pin-code

        :parameter:

            Parameter

            {code live and testing
                "pincode": "CharField",                    # 422401
            }

            Response

            {
                "status": "CharField",                      # 1
                "message": "CharField", 1                   # Valid pincode
            }

            Note

            Guest token required

            "guest-token-pincode" : "dgjkvcllvdrfpjfogxsomifrihmwlxqv"

            status 1 : Valid pin code
            status 2 : Invalid pin code ( not present in pin code master)
            status 10 : Something went wrong (Django Error)
            status 11 : Enter valid data

        """

    # permission_classes = (partial(GuestTokenPermissionPincodeCheck, ['GET', 'POST', 'HEAD']),)
    serializer_class = user_serializers.PincodeValidateSerializer

    def post(self, request, *args, **kwargs):

        status = None
        message = None
        taluka = None
        city = None
        state = None
        is_delivery_available = False

        pincode = request.data.get('pincode')
        pincode_serializer = self.serializer_class(data=request.data)

        if pincode_serializer.is_valid():
            try:
                is_delivery_available = DeliveryAvailability(pincode)
            except Exception as e:
                print('inside delivery availability', e)

            try:

                pincode_instance = PincodeMaster.objects.filter(pincode=pincode).order_by('-id').first()

                if pincode_instance:
                    taluka = pincode_instance.taluka
                    city = pincode_instance.city
                    state = pincode_instance.state
                    status = '1'
                    message = 'Valid pin code'

                else:

                    status = '2'
                    message = 'Invalid pin code'

            except Exception as e:

                status = '10'
                message = 'Something went wrong'

        else:

            status = '11'
            message = 'Enter valid data'

        return Response({
            'status': status,
            'message': message,
            'taluka': taluka,
            'city': city,
            'state': state,
            'is_delivery_available': is_delivery_available
        })


class ExternallyCreateLogisticOrder(generics.ListAPIView):
    """
    This API creates logistics order..
    parameter = {
        "unique_order_id": "UNI208282258169917033"
        "order_type": "Delivery" or "Reverse"
        "return_reason": "Product Faulty"
    }

    Return response = {
                            "status": "1",
                            "message": "Success"
                        }
    Error response =   1.{
                            "status": "3",
                            "message": "Something went wrong"
                        }
                       2.  {
                        "status": "4",
                        "message": "Order ID not found"
                    }


    """

    def post(self, request, *args, **kwargs):
        unique_order_id = request.data.get("unique_order_id")
        order_type = request.data.get("order_type")
        if unique_order_id is not None:
            get_order_obj = Order.objects.filter(unique_order_id=unique_order_id)
            if get_order_obj:
                create_order = None
                if order_type == "Delivery":
                    # calling function of creating order data..
                    create_order = create_order_data(get_order_obj[0].id)
                if order_type == "Reverse":
                    return_reason = request.data.get("return_reason")
                    # calling function of creating order data..
                    create_order = create_reverse_order_data(get_order_obj[0].id, return_reason)
                if create_order:
                    status = create_order["status"]
                    message = create_order["message"]
                    return Response(
                        {
                            "status": status,
                            "message": message
                        }
                    )

                else:
                    return Response(
                        {
                            "status": "3",
                            "message": "Something went wrong"
                        }
                    )

            else:
                return Response(
                    {
                        "status": "4",
                        "message": "Order ID not found"
                    }
                )


def create_order_data(order_id):

    print("This function is to create all post_data for order create API of Logistics..")
    """
       param : order_id
     {
      "data":    {
                 "shipments" : [
                                   {
                                   "waybill" : "",
                                   "order" : "GK0033",
                                   "sub_order" : "A",
                                   "order_date" : "31-01-2018",
                                   "total_amount" : "999",
                                   "name" : "Bharat",
                                   "add" : "104, Shreeji Sharan",
                                   "add2" : "",
                                   "add3" : "",
                                   "pin" : "400067",
                                   "city" : "Mumbai",
                                   "state" : "Maharashtra",
                                   "country" : "India",
                                   "phone" : "9876543210",
                                   "email" : "abc@gmail.com",
                                   "products" : "T-Shirt",
                                   "products_desc" : "Clothing",
                                   "quantity" : "1",
                                   "shipment_length" : "0",
                                   "shipment_width" : "0",
                                   "shipment_height" : "0",
                                   "weight" : "400.00",
                                   "cod_amount" : "999",
                                   "payment_mode" : "COD", For reverse Shipments: Prepaid Only
                                   "seller_tin" : "",
                                   "seller_cst" : "",
                                   "return_address_id" : "24",
                                   "product_sku" : "S01",
                                   "extra_parameters" :
                                                     {
                                                     "return_reason" : "",
                                                     "encryptedShipmentID" : ""
                                                     }
                                   }
                                ],
                 "pickup_address_id" : "24",
                 "access_token" : "8ujik47cea32ed386b1f65c85fd9aaaf", #You will get this from iThink Logistics Team
                 "secret_key" : "65tghjmads9dbcd892ad4987jmn602a7" #You will get this from iThink Logistics Team
                 "logistics" : "Delhivery" #Allowed values delhivery, fedex, xpressbees, ecom, ekart. For reverse Shipments: Delhivery Only
                 "s_type" : "" # If fedex than Allowed values standard, priority, ground.
                 "order_type" : "" If placing reverse shipment, pass 'reverse' else can be left blank.
                         }
      }
    """
    return_data = {}
    waybill = ""
    order = ""
    sub_order = ""
    order_date = ""
    total_amount = ""
    name = ""
    add = ""
    add2 = ""
    add3 = ""
    pin = ""
    city = ""
    state = ""
    country = "India"
    phone = ""
    email = ""
    products = ""
    products_desc = ""
    quantity = ""
    shipment_length = "0"
    shipment_width = "0"
    shipment_height = "0"
    weight = ""
    cod_amount = "0"
    payment_mode = "Prepaid"  # For reverse Shipments: Prepaid Only
    seller_tin = ""
    seller_cst = ""
    return_address_id = ""
    product_sku = ""
    pickup_address_id = ""  # This is warehouse id of particular seller.
    logistics = ""
    s_type = ""
    order_type = ""
    extra_parameters = {
        "return_reason": "",
        "encryptedShipmentID": ""
    }
    order_obj = Order.objects.filter(id=order_id)
    if order_obj:
        order = order_obj[0].unique_order_id
        order_date = order_obj[0].created_date
        products = order_obj[0].product_details.product_id.name
        products_desc = order_obj[0].product_details.product_id.base_category.sub_category.category.name
        quantity = str(order_obj[0].quantity)
        weight = order_obj[0].product_details.weight
        product_sku = order_obj[0].product_details.sku
        seller_id = order_obj[0].product_details.user.id
        warehouse_obj = SellerWarehouse.objects.filter(seller=seller_id, type="ithink",
                                                       approval_status__icontains="approved")
        if warehouse_obj:
            pickup_address_id = warehouse_obj[0].warehouse_id
        get_order_details_obj = OrderDetailsOrder.objects.filter(order=order_obj[0].id)
        if get_order_details_obj:
            order_details_id = get_order_details_obj[0].orderdetails.id
            order_details_obj = OrderDetails.objects.filter(id=order_details_id)
            if order_details_obj:
                user_personal_obj = Personal.objects.filter(user=order_details_obj[0].user)
                if user_personal_obj:
                    name = user_personal_obj[0].name
                phone = order_details_obj[0].user.username
                add = order_details_obj[0].address.address
                pin = order_details_obj[0].address.pin_code
                total_amount = order_details_obj[0].total_amount
                sub_order = order_details_obj[0].order_text
                if pin:
                    logistics = get_logistics_agent(pin)
                city = order_details_obj[0].address.city
                state = order_details_obj[0].address.state

        post_data = {
            "data": {
                "shipments": [
                    {
                        "waybill": waybill,
                        "order": order,
                        "sub_order": sub_order,
                        "order_date": str(order_date),
                        "total_amount": total_amount,
                        "name": name,
                        "add": add,
                        "add2": "",
                        "add3": "",
                        "pin": pin,
                        "city": city,
                        "state": state,
                        "country": country,
                        "phone": phone,
                        "email": email,
                        "products": products,
                        "products_desc": products_desc,
                        "quantity": quantity,
                        "shipment_length": shipment_length,
                        "shipment_width": shipment_width,
                        "shipment_height": shipment_height,
                        "weight": weight,
                        "cod_amount": cod_amount,
                        "payment_mode": payment_mode,
                        "seller_tin": seller_tin,
                        "seller_cst": seller_cst,
                        "return_address_id": pickup_address_id,
                        "product_sku": product_sku,
                        "extra_parameters": extra_parameters
                    }
                ],
                "pickup_address_id": pickup_address_id,
                "access_token": test_access_token,
                "secret_key": test_secret_key,
                "logistics": logistics,
                "s_type": "priority",
                "order_type": "",
            }
        }

        # Here we save the logistics order details..
        check_logistic_order = LogisticOrder.objects.filter(order=order_obj[0], type="ithink", order_mode="Delivery", status="success")
        if not check_logistic_order:
            add_logistic_order = LogisticOrder.objects.create(order=order_obj[0], waybill_no=waybill,
                                                              logistics=logistics, type="ithink",
                                                              order_mode="Delivery")

            url = "https://manage.ithinklogistics.com/api_v2/order/add.json"
            headers = {'content-type': "application/json"}
            response = requests.request("POST", url, data=json.dumps(post_data), headers=headers)
            if response:
                status = ""
                resp_data = response.json()
                if "status" in resp_data:
                    status = resp_data["status"]
                if status:
                    if "data" in resp_data:
                        return_response = resp_data['data']['1']
                        if return_response:
                            status = return_response['status']
                            remark = ""
                            waybill = ""
                            refnum = ""
                            if status == "success":
                                remark = return_response['remark']
                                waybill = return_response['waybill']
                                refnum = return_response['refnum']
                                # Here we save the logistics order details..
                                get_logistic_order_obj = LogisticOrder.objects.filter(order=order_obj[0],
                                                                                      order_mode="Delivery", type="ithink").order_by(
                                    'id').last()
                                if get_logistic_order_obj:
                                    update_logistic_order = LogisticOrder.objects.filter(
                                        id=get_logistic_order_obj.id).update(
                                        waybill_no=waybill,
                                        remark=remark, status=status, refnum=refnum, order_data_text=resp_data)

                                return_data["status"] = "1"
                                return_data["message"] = "Success"
                                return_data["waybill_no"] = waybill
                                return return_data

                            if status == "error":
                                remark = return_response['remark']
                                # Here we save the logistics order details..
                                get_logistic_order_obj = LogisticOrder.objects.filter(order=order_obj[0],
                                                                                      order_mode="Delivery").exclude(
                                    status="error").order_by('id').last()
                                if get_logistic_order_obj:
                                    update_logistic_order = LogisticOrder.objects.filter(
                                        id=get_logistic_order_obj.id).update(
                                        waybill_no=waybill,
                                        remark=remark, status=status, order_data_text=resp_data)

                                return_data["status"] = "3"
                                return_data["message"] = remark
                                return return_data
                    else:
                        if status == "error":
                            error_message = resp_data["html_message"]
                            # Here we save the logistics order details..
                            get_logistic_order_obj = LogisticOrder.objects.filter(order=order_obj[0],
                                                                                  order_mode="Delivery", type="ithink").exclude(
                                status="error").order_by('id').last()
                            if get_logistic_order_obj:
                                update_logistic_order = LogisticOrder.objects.filter(
                                    id=get_logistic_order_obj.id).update(
                                    waybill_no=waybill,
                                    remark=error_message, status=status, order_data_text=resp_data)

                            return_data["status"] = "2"
                            return_data["message"] = error_message
                            return return_data

    return return_data


def create_reverse_order_data(order_id, return_reason):
    print("This function is to create all post_data for order create API of Logistics..")
    """
     param: order_id,return_reason
     {
      "data":    {
                 "shipments" : [
                                   {
                                   "waybill" : "",
                                   "order" : "GK0033",
                                   "sub_order" : "A",
                                   "order_date" : "31-01-2018",
                                   "total_amount" : "999",
                                   "name" : "Bharat",
                                   "add" : "104, Shreeji Sharan",
                                   "add2" : "",
                                   "add3" : "",
                                   "pin" : "400067",
                                   "city" : "Mumbai",
                                   "state" : "Maharashtra",
                                   "country" : "India",
                                   "phone" : "9876543210",
                                   "email" : "abc@gmail.com",
                                   "products" : "T-Shirt",
                                   "products_desc" : "Clothing",
                                   "quantity" : "1",
                                   "shipment_length" : "0",
                                   "shipment_width" : "0",
                                   "shipment_height" : "0",
                                   "weight" : "400.00",
                                   "cod_amount" : "999",
                                   "payment_mode" : "COD", For reverse Shipments: Prepaid Only
                                   "seller_tin" : "",
                                   "seller_cst" : "",
                                   "return_address_id" : "24",
                                   "product_sku" : "S01",
                                   "extra_parameters" :
                                                     {
                                                     "return_reason" : "",
                                                     "encryptedShipmentID" : ""
                                                     }
                                   }
                                ],
                 "pickup_address_id" : "24",
                 "access_token" : "8ujik47cea32ed386b1f65c85fd9aaaf", #You will get this from iThink Logistics Team
                 "secret_key" : "65tghjmads9dbcd892ad4987jmn602a7" #You will get this from iThink Logistics Team
                 "logistics" : "Delhivery" #Allowed values delhivery, fedex, xpressbees, ecom, ekart. For reverse Shipments: Delhivery Only
                 "s_type" : "" # If fedex than Allowed values standard, priority, ground.
                 "order_type" : "" If placing reverse shipment, pass 'reverse' else can be left blank.
                         }
      }
    """
    return_data = {}
    waybill = ""
    order = ""
    sub_order = ""
    order_date = ""
    total_amount = ""
    name = ""
    add = ""
    add2 = ""
    add3 = ""
    pin = ""
    city = ""
    state = ""
    country = "India"
    phone = ""
    email = ""
    products = ""
    products_desc = ""
    quantity = ""
    shipment_length = "0"
    shipment_width = "0"
    shipment_height = "0"
    weight = ""
    cod_amount = "0"
    payment_mode = "Prepaid"  # For reverse Shipments: Prepaid Only
    seller_tin = ""
    seller_cst = ""
    return_address_id = ""
    product_sku = ""
    pickup_address_id = ""
    logistics = "Delhivery"  # For Reverse shipment only Delhivery agent is available..
    s_type = ""
    extra_parameters = {
        "return_reason": return_reason,
        "encryptedShipmentID": ""
    }
    order_obj = Order.objects.filter(id=order_id)
    if order_obj:
        order = order_obj[0].unique_order_id
        order_date = order_obj[0].created_date
        products = order_obj[0].product_details.product_id.name
        products_desc = order_obj[0].product_details.product_id.base_category.sub_category.category.name
        quantity = str(order_obj[0].quantity)
        weight = order_obj[0].product_details.weight
        seller_id = order_obj[0].product_details.user.id
        product_sku = order_obj[0].product_details.sku
        warehouse_obj = SellerWarehouse.objects.filter(seller=seller_id, approval_status__icontains="approved")
        if warehouse_obj:
            pickup_address_id = warehouse_obj[0].warehouse_id
        get_order_details_obj = OrderDetailsOrder.objects.filter(order=order_obj[0].id)
        if get_order_details_obj:
            order_details_id = get_order_details_obj[0].orderdetails.id
            order_details_obj = OrderDetails.objects.filter(id=order_details_id)
            if order_details_obj:
                user_personal_obj = Personal.objects.filter(user=order_details_obj[0].user)
                if user_personal_obj:
                    name = user_personal_obj[0].name
                phone = order_details_obj[0].user.username
                add = order_details_obj[0].address.address
                pin = order_details_obj[0].address.pin_code
                total_amount = order_details_obj[0].total_amount
                sub_order = order_details_obj[0].order_text
                city = order_details_obj[0].address.city
                state = order_details_obj[0].address.state

        post_data = {
            "data": {
                "shipments": [
                    {
                        "waybill": waybill,
                        "order": order,
                        "sub_order": sub_order,
                        "order_date": str(order_date),
                        "total_amount": total_amount,
                        "name": name,
                        "add": add,
                        "add2": "",
                        "add3": "",
                        "pin": pin,
                        "city": city,
                        "state": state,
                        "country": country,
                        "phone": phone,
                        "email": email,
                        "products": products,
                        "products_desc": products_desc,
                        "quantity": quantity,
                        "shipment_length": shipment_length,
                        "shipment_width": shipment_width,
                        "shipment_height": shipment_height,
                        "weight": weight,
                        "cod_amount": cod_amount,
                        "payment_mode": payment_mode,
                        "seller_tin": seller_tin,
                        "seller_cst": seller_cst,
                        "return_address_id": pickup_address_id,
                        "product_sku": product_sku,
                        "extra_parameters": extra_parameters
                    }
                ],
                "pickup_address_id": pickup_address_id,
                "access_token": test_access_token,
                "secret_key": test_secret_key,
                "logistics": "Delhivery",  # For Reverse shipment only Delhivery agent is available..
                "s_type": "priority",
                "order_type": "reverse",
            }
        }


        # Here we save the logistics order details..
        check_logistic_order = LogisticOrder.objects.filter(order=order_obj[0], order_mode="Reverse", status="success")
        if not check_logistic_order:
            add_logistic_order = LogisticOrder.objects.create(order=order_obj[0], waybill_no=waybill,
                                                              logistics=logistics,
                                                              order_mode="Reverse")

            url = "https://manage.ithinklogistics.com/api_v2/order/add.json"
            headers = {'content-type': "application/json"}
            response = requests.request("POST", url, data=json.dumps(post_data), headers=headers)
            if response:
                status = ""
                resp_data = response.json()
                if "status" in resp_data:
                    status = resp_data["status"]
                if status:
                    if "data" in resp_data:
                        return_response = resp_data['data']['1']
                        if return_response:
                            status = return_response['status']
                            remark = ""
                            waybill = ""
                            refnum = ""
                            if status == "success":
                                remark = return_response['remark']
                                waybill = return_response['waybill']
                                refnum = return_response['refnum']
                                # Here we save the logistics order details..
                                get_logistic_order_obj = LogisticOrder.objects.filter(order=order_obj[0],
                                                                                      order_mode="Reverse").exclude(
                                    status="error").order_by('id').last()
                                if get_logistic_order_obj:
                                    update_logistic_order = LogisticOrder.objects.filter(
                                        id=get_logistic_order_obj.id).update(
                                        waybill_no=waybill,
                                        remark=remark, status=status, refnum=refnum, order_data_text=resp_data)

                                return_data["status"] = "1"
                                return_data["message"] = "Success"
                                return return_data

                            if status == "error":
                                remark = return_response['remark']
                                # Here we save the logistics order details..
                                get_logistic_order_obj = LogisticOrder.objects.filter(order=order_obj[0],
                                                                                      order_mode="Reverse").order_by(
                                    'id').last()
                                if get_logistic_order_obj:
                                    update_logistic_order = LogisticOrder.objects.filter(
                                        id=get_logistic_order_obj.id).update(
                                        waybill_no=waybill,
                                        remark=remark, status=status, order_data_text=resp_data)

                                return_data["status"] = "3"
                                return_data["message"] = remark
                                return return_data

                else:
                    if status == "error":
                        error_message = resp_data["html_message"]
                        # Here we save the logistics order details..
                        get_logistic_order_obj = LogisticOrder.objects.filter(order=order_obj[0],
                                                                              order_mode="Reverse").order_by(
                            'id').last()
                        if get_logistic_order_obj:
                            update_logistic_order = LogisticOrder.objects.filter(id=get_logistic_order_obj.id).update(
                                waybill_no=waybill,
                                remark=error_message, status=status, order_data_text=resp_data)

                        return_data["status"] = "2"
                        return_data["message"] = error_message
                        return return_data

    return return_data


def track_order(waybill_no):
    """
    param :waybill_no
    
    
    sample response:
    
    {
      "status_code": 200,
      "data":
        {
         "1369010033902":
                       {
                         "message": "success",
                         "awb_no": "1369010033902",
                         "logistic": "Delhivery",
                         "order_type": "forward",
                         "current_status": "In Transit",
                         "current_status_code": "UD",
                         "return_tracking_no": "",
                         "last_scan_details":
                                           {
                                           "status": "In Transit",
                                           "status_code": "UD",
                                           "status_date_time": "2017-06-07 18:11:26",
                                           "scan_location": "Surat_Pandesra_Gateway (Gujarat)",
                                           "remark": "Shipment Picked Up from Client Location"
                                           },
                         "order_details":
                                           {
                                           "order_type": "COD",
                                           "order_number": "11812",
                                           "sub_order_number": "11812",
                                           "order_sub_order_number": "11812-11812",
                                           "phy_weight": "200.00",
                                           "net_payment": "775.00",
                                           "ship_length": "10.00",
                                           "ship_width": "10.00",
                                           "ship_height": "10.00"
                                           },
                         "order_date_time":
                                           {
                                           "manifest_date_time": "2017-06-07 13:34:36",
                                           "pickup_date": "2017-06-07",
                                           "delivery_date": "",
                                           "rto_delivered_date": ""
                                           },
                         "customer_details":
                                           {
                                           "customer_name": "John Doe",
                                           "customer_address1": "60/40 st shenoy nagar",
                                           "customer_address2": "",
                                           "customer_address3": "",
                                           "customer_city": "Chennai",
                                           "customer_state": "Tamil Nadu",
                                           "customer_country": "India",
                                           "customer_pincode": "600030",
                                           "customer_mobile": "9876543210",
                                           "customer_phone": "2156789098"
                                           },
                         "scan_details": [
                                           {
                                            "status": "Manifested",
                                            "status_code": "UD",
                                            "scan_location": "HQ (Haryana)",
                                            "remark": "Consignment Manifested",
                                            "scan_date_time": "2017-06-07 14:05:57"
                                            },
                                           {
                                            "status": "In Transit",
                                            "status_code": "UD",
                                            "scan_location": "Surat_Pandesra_Gateway (Gujarat)",
                                            "remark": "Shipment Picked Up from Client Location",
                                            "scan_date_time": "2017-06-07 18:11:26"
                                            }
                                         ]
                        }
                                                 
        }
      } 
    
    """
    url = "https://manage.ithinklogistics.com/api_v2/order/track.json"
    payload = {
        "data": {
            "awb_number_list": waybill_no,
            "access_token": test_access_token,
            "secret_key": test_secret_key
        }
    }
    headers = {
        'content-type': "application/json",
    }
    response_data = requests.request("POST", url, data=json.dumps(payload), headers=headers)

    if response_data:
        scan_details_dict = {}
        scan_details = []
        current_status = ""
        response = response_data.json()
        if "data" in response.keys():
            if response["data"]:
                keys = list(response["data"].keys())
                key = keys[0]
                if response["data"][key]:
                    if response["data"][key]["scan_details"]:
                        scan_details = response["data"][key]["scan_details"]
                    if response["data"][key]["current_status"]:
                        current_status = response["data"][key]["current_status"]

        scan_details_dict['scan_details'] = scan_details
        scan_details_dict['current_status'] = current_status
        return scan_details_dict


def call_api(url, data):
    """
    calls the API with given parameter and returns the response
    :param url:
    :param data:
    :return: response
    """
    # adding credentials
    if data is not None:
        pass

    method = "POST"
    headers = {
        'content-type': "application/json",
        # 'cache-control': "no-cache"
    }
    response = requests.request(method, url, data=json.dumps(data), headers=headers)
    return response


def get_all_city(state_code):
    """
    This function gives all city names from the state..
    """
    url = "https://manage.ithinklogistics.com/api_v2/city/get.json"
    data = {
        "state_id": state_code,
        "access_token": test_access_token,
        "secret_key": test_secret_key
    }
    req_dict = {"data": data}
    response_data = call_api(url, req_dict)
    if response_data:
        city_list = []
        response = response_data.json()
        data_dict = response["data"]
        for city in data_dict:
            city_list.append(city["city_name"])
        return city_list


def get_city_code(state_code, city_name):
    """
     param: state_code,city_name
     description:   This function gives all city names from the state..

    """
    url = "https://manage.ithinklogistics.com/api_v2/city/get.json"

    data = {
        "state_id": state_code,
        "access_token": test_access_token,
        "secret_key": test_secret_key
    }
    req_dict = {"data": data}
    response_data = call_api(url, req_dict)
    if response_data:
        city_code = ""
        response = response_data.json()
        data_dict = response["data"]
        for city in data_dict:
            if city["city_name"] == city_name:
                city_code = city["id"]
        return city_code


def get_state_code(state_name):
    """
    discription : This function give state id of the state.
    param: state_name
    response: state_id

    """
    state_id = ""
    if state_name == "Andaman and Nicobar Islands":
        state_id = "1"

    if state_name == "Andhra Pradesh":
        state_id = "2"

    if state_name == "Arunachal Pradesh":
        state_id = "3"

    if state_name == "Assam":
        state_id = "4"

    if state_name == "Bihar":
        state_id = "5"

    if state_name == "Chandigarh":
        state_id = "6"

    if state_name == "Chhattisgarh":
        state_id = "7"

    if state_name == "Dadra and Nagar Haveli":
        state_id = "8"

    if state_name == "Daman and Diu":
        state_id = "9",

    if state_name == "Delhi":
        state_id = "10"

    if state_name == "Goa":
        state_id = "11"

    if state_name == "Gujarat":
        state_id = "12"

    if state_name == "Haryana":
        state_id = "13"

    if state_name == "Himachal Pradesh":
        state_id = "14"

    if state_name == "Jammu and Kashmir":
        state_id = "15"

    if state_name == "Jharkhand":
        state_id = "16"

    if state_name == "Karnataka":
        state_id = "17"

    if state_name == "Kerala":
        state_id = "19"

    if state_name == "Lakshadweep":
        state_id = "20"

    if state_name == "Madhya Pradesh":
        state_id = "21"

    if state_name == "Maharashtra":
        state_id = "22"

    if state_name == "Manipur":
        state_id = "23"

    if state_name == "Meghalaya":
        state_id = "24"

    if state_name == "Mizoram":
        state_id = "25"

    if state_name == "Nagaland":
        state_id = "26"

    if state_name == "Odisha":
        state_id = "29"

    if state_name == "Pondicherry":
        state_id = "31"

    if state_name == "Punjab":
        state_id = "32"

    if state_name == "Rajasthan":
        state_id = "33"

    if state_name == "Sikkim":
        state_id = "34"

    if state_name == "Tamil Nadu":
        state_id = "35"

    if state_name == "Telangana":
        state_id = "36"

    if state_name == "Tripura":
        state_id = "37"

    if state_name == "Uttar Pradesh":
        state_id = "38"

    if state_name == "Uttarakhand":
        state_id = "39"

    if state_name == "West Bengal":
        state_id = "41"

    return state_id


def get_state_list():
    """
    This function gives states list..
    We can take this states list with the help of GET STATE API of Logistics
    but for now we arer taking this as static..
    """

    state_list = ['Andaman and Nicobar Islands', 'Andhra Pradesh', 'Arunachal Pradesh', 'Assam', 'Bihar', 'Chandigarh',
                  'Chhattisgarh', 'Dadra and Nagar Haveli', 'Daman and Diu', 'Delhi', 'Goa', 'Gujarat', 'Haryana',
                  'Himachal Pradesh', 'Jammu and Kashmir', 'Jharkhand', 'Karnataka', 'Kerala', 'Lakshadweep',
                  'Madhya Pradesh', 'Maharashtra', 'Manipur', 'Meghalaya', 'Mizoram', 'Nagaland', 'Odisha',
                  'Pondicherry', 'Punjab', 'Rajasthan', 'Sikkim', 'Tamil Nadu', 'Telangana', 'Tripura', 'Uttar Pradesh',
                  'Uttarakhand', 'West Bengal']

    return state_list


class Add_Warehouse(generic.ListView):
    """
      This view is send state data.
    """
    template_name = "Ecom/logistics/file.html"

    def get(self, request, *args, **kwargs):
        ctx = {}
        if 'message' in kwargs:
            ctx['msg'] = kwargs['message']
        state_list = get_state_list()
        sellers = User.objects.filter(groups__name="Seller")
        if sellers:
            ctx['sellers'] = sellers
        ctx["state_list"] = state_list
        return render(request, self.template_name, ctx)


@csrf_exempt
def country_state_pincode(request):
    """
    This view is to get state and send city list.
    """
    if request.method == "POST":
        state = request.POST['state']
        state_code = get_state_code(state)
        cities = get_all_city(state_code)
        return_data = {
            'status': "1",
            'city': cities
        }
        return HttpResponse(json.dumps(return_data))
    else:
        return_data = {
            'status': "2",
        }

    return HttpResponse(json.dumps(return_data))


class Add_WarehouseAPI(generic.ListView):
    """
      This view is send state data.
    """
    template_name = "Ecom/logistics/Add_warehouse.html"

    def post(self, request, *args, **kwargs):
        ctx = {}
        message = ""
        url = "https://manage.ithinklogistics.com/api_v2/warehouse/add.json"
        shop_name = request.POST['shop_name']
        seller = request.POST['seller']
        user = User.objects.filter(username=seller)
        country = request.POST['country']
        state = request.POST['state']
        city = request.POST['city']

        address_1 = request.POST['address_1']
        address_2 = request.POST['address_2']
        mobile_no = request.POST['mobile_no']
        pin_code = request.POST['pin_code']

        country_code = "101"
        state_code = get_state_code(state)
        city_code = get_city_code(state_code, city)

        data = {
            "data": {
                "company_name": shop_name,
                "address1": address_1,
                "address2": address_2,
                "mobile": mobile_no,
                "pincode": pin_code,
                "city_id": city_code,
                "state_id": state_code,
                "country_id": country_code,
                "access_token": test_access_token,
                "secret_key": test_secret_key
            }
        }

        response_data = call_api(url, data)
        warehouse_id = None
        if response_data:
            response = response_data.json()
            """
             {
              "status_code": 200,
              "html_message": "Warehouse Added Successfully. Awaiting for pending approval.",
              "warehouse_id": 94,
              "status": "success"
             }
            """
            warehouse_id = response["warehouse_id"]
            message = response["html_message"]
        if warehouse_id is not None:
            # creating entry in sellers warehouse model..
            add_warehouse = SellerWarehouse.objects.create(seller=user[0], warehouse_id=warehouse_id,
                                                           company_name=shop_name, primary_address=address_1,
                                                           secondary_address=address_2, mobile_no=mobile_no,
                                                           pin_code=pin_code, city_id=city_code,
                                                           state_id=state_code, country_id=country_code,
                                                           approval_status="pending")

        return HttpResponseRedirect(reverse('add_warehouse', kwargs={'message': message}))


def get_warehouse_status(warehouse_id):
    url = "https://manage.ithinklogistics.com/api_v2/warehouse/get.json"

    data = {
        "data": {
            "warehouse_id": warehouse_id,  # warehouse_id whose data is needed.
            "access_token": test_access_token,
            "secret_key": test_secret_key
        }
    }

    response_data = call_api(url, data)
    warehouse_data = {}
    if response_data:
        response = response_data.json()
        data = response["data"]
        if data:
            # warehouse_status = response["data"][0]["status"]
            warehouse_data = response["data"][0]
    return warehouse_data


class SellerWareHouseDetails(generic.ListView):
    """
       This Function is to show seller ware house details..
    """

    template_name = 'Ecom/logistics/seller_ware_house_details.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        ware_house_data = SellerWarehouse.objects.all()
        if ware_house_data:
            ctx['ware_house_data'] = ware_house_data
        return render(request, self.template_name, ctx)


class SellerShipRocketWareHouseDetails(generic.ListView):
    """
       This Function is to show seller ware house details..
    """

    template_name = 'Ecom/logistics/shiprocket_seller_ware_house_details.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        ware_house_data = SellerWarehouse.objects.filter(type="shiprocket", adding_status=True)
        if ware_house_data:
            ctx['ware_house_data'] = ware_house_data
        return render(request, self.template_name, ctx)


class LogisticOrderDetails(generic.ListView):
    """
       This Function is to show LogisticOrder details..
    """
    template_name = 'Ecom/logistics/logistic_order_details.html'

    def get(self, request, seller, status, *args, **kwargs):
        ctx = {}
        logistic_list = []
        data_dict = {}
        logistic_order = LogisticOrder.objects.filter(order__product_details__user__username=seller,
                                                      order_mode=status)
        if logistic_order:
            for logistic in logistic_order:
                image = ""
                product_image = Images.objects.filter(type="main",
                                                      product=logistic.order.product_details.product_id).order_by(
                    'id').last()
                if product_image:
                    from ecom.views import image_thumbnail
                    image = image_thumbnail(product_image.path)
                    data_dict.update({
                        logistic: image})
        else:
            data_dict = None
        ctx['logistic_order'] = data_dict
        return render(request, self.template_name, ctx)


class LogisticsDeliveryReverseCount(generic.ListView):
    """
       This Function is to show logistics delivery and reverse count..
    """
    template_name = 'Ecom/logistics/logistics_delivery_reverse_count.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        data_dict = {}
        user = User.objects.filter(id=request.user.id)
        seller_group = user[0].groups.filter(name='Seller').exists()
        if seller_group:
            delivery_logistics_order = LogisticOrder.objects.filter(order__product_details__user=user[0],
                                                                    order_mode='Delivery').count()
            reverse_logistics_order = LogisticOrder.objects.filter(order__product_details__user=user[0],
                                                                   order_mode='Reverse').count()
            data_dict.update({user[0].username: [delivery_logistics_order, reverse_logistics_order]})
            ctx['logistic_order'] = data_dict

        ecom_admin_group = user[0].groups.filter(name='EcomAdmin').exists()
        if ecom_admin_group:
            sellers = LogisticOrder.objects.values_list("order__product_details__user", flat=True)
            if sellers:
                logistics_seller = User.objects.filter(id__in=sellers)
                for seller in logistics_seller:
                    delivery_logistics_order = LogisticOrder.objects.filter(order__product_details__user=seller,
                                                                            order_mode='Delivery').count()
                    reverse_logistics_order = LogisticOrder.objects.filter(order__product_details__user=seller,
                                                                           order_mode='Reverse').count()
                    data_dict.update({seller.username: [delivery_logistics_order, reverse_logistics_order]})
                ctx['logistic_orders'] = data_dict
        return render(request, self.template_name, ctx)


def update_warehouse_status(request, ware_house_id):
    """
    This function calls get warehouse API and update sellers warehouse status..
    """
    if ware_house_id is not None:
        status = None
        warehouse_data = get_warehouse_status(ware_house_id)
        if warehouse_data:
            status = warehouse_data["status"]

        # Update warehouse details..
        warehouse_obj = SellerWarehouse.objects.filter(warehouse_id=ware_house_id)
        if warehouse_obj:
            update_status = SellerWarehouse.objects.filter(warehouse_id=ware_house_id).update(approval_status=status)
    return HttpResponseRedirect(reverse("seller_ware_house_details"))


def update_shiprocket_warehouse_status(request, ware_house_id):
    """
    This function calls get warehouse API and update sellers warehouse status..
    """
    if ware_house_id is not None:
        # Update warehouse details..
        warehouse_obj = SellerWarehouse.objects.filter(id=ware_house_id, type="shiprocket", adding_status=True)
        if warehouse_obj:
            update_status = SellerWarehouse.objects.filter(id=warehouse_obj[0].id).update(
                approval_status="approved")
    return HttpResponseRedirect(reverse("seller_shiprocket_warehouse_details"))


import requests


@csrf_exempt
def print_shipment_label(request):
    """
    This Api is to Print Shipment of the order..
    """
    return_data = {}
    order_id = request.POST['order_id']
    if order_id:
        order_mode = "Delivery"
        logistic = get_orders_logistic_partner(order_id, order_mode)
        if logistic == "ithink":
            waybill_no = None
            get_logistic_obj = LogisticOrder.objects.filter(order=order_id, status="success",
                                                            order_mode=order_mode).order_by('id').last()
            if get_logistic_obj:
                waybill_no = get_logistic_obj.waybill_no
            url = "https://manage.ithinklogistics.com/api_v2/shipping/label.json"
            # url = "https://pre-alpha.ithinklogistics.com/api_v2/shipping/label.json"

            if waybill_no:
                data = {
                    "awb_numbers": waybill_no,  # AWB Number whose data is needed.
                    "per_page": "4",
                    "page_size": "A4",
                    "access_token": test_access_token,
                    "secret_key": test_secret_key
                }

                req_dict = {"data": data}
                response_data = call_api(url, req_dict)
                if response_data:
                    response = response_data.json()
                    status = response["status"]
                    if status == "success":
                        file_path = response["file_name"]
                        url = file_path
                        if url:
                            return_data = {
                                'url': url,
                            }
                    else:
                        return_data = {
                            'url': "",
                        }

        if logistic == "shiprocket":
            get_shiprocket_order = ShiprocketOrder.objects.filter(order=order_id, status__contains="success",
                                                                  order_mode="Delivery").order_by('id').last()
            shipment_id = get_shiprocket_order.shipment_id
            if shipment_id:
                from EApi.shiprocket import generate_label_api
                url = generate_label_api(shipment_id)
                if url:
                    return_data = {
                        'url': url,
                    }

                else:
                    return_data = {
                        'url': "",
                    }

    return HttpResponse(json.dumps(return_data))


@csrf_exempt
def print_manifest_api(request):
    """

    description : This APi is to print manifest of the order.
    """
    return_data = {}
    order_id = request.POST['order_id']
    if order_id:
        order_mode = "Delivery"
        logistic = get_orders_logistic_partner(order_id, order_mode)
        if logistic == "ithink":
            waybill_no = None
            get_logistic_obj = LogisticOrder.objects.filter(order=order_id, status="success",
                                                            order_mode=order_mode).order_by('id').last()
            if get_logistic_obj:
                waybill_no = get_logistic_obj.waybill_no
            url = "https://manage.ithinklogistics.com/api_v2/shipping/manifest.json"
            # url = "https://pre-alpha.ithinklogistics.com/api_v2/shipping/manifest.json"

            data = {
                "awb_numbers": waybill_no,  # AWB Number whose data is needed.
                "access_token": test_access_token,
                "secret_key": test_secret_key
            }

            req_dict = {"data": data}
            response_data = call_api(url, req_dict)
            if response_data:
                response = response_data.json()
                status = response["status"]
                if status == "success":
                    file_path = response["file_name"]
                    url = file_path
                    if url:
                        return_data = {
                            'url': url,
                        }
                else:
                    return_data = {
                        'url': "",
                    }

        if logistic == "shiprocket":
            get_shiprocket_order = ShiprocketOrder.objects.filter(order=order_id, status__contains="success",
                                                                  order_mode="Delivery").order_by('id').last()
            shipment_id = get_shiprocket_order.shipment_id
            if shipment_id:
                from EApi.shiprocket import generate_shiprocket_manifest_api
                url = generate_shiprocket_manifest_api(shipment_id)
                if url:
                    return_data = {
                        'url': url,
                    }

                else:
                    return_data = {
                        'url': "",
                    }

    return HttpResponse(json.dumps(return_data))


class OrderTrackingApi(generics.ListAPIView):
    """
    This Api is to Users all orders
    Url: ecomapi/user_orders
    Authentication token required
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User toke is required..TODO
    method = get
    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    def post(self, request, *args, **kwargs):
        """
        order_id = request.data.get("order_id")
        location_list = []
        order_list = []
        data = {}
        # order_id = "UNI98467623"
        order_status_list = ["placed", "packed", "dispatched", "In Transit", "delivered"]
        order_status = OrderStatus.objects.filter(order__unique_order_id=order_id).order_by('date')
        print('order_status', order_status)
        print('order_status', order_status[0].id)
        if order_status:
            for order in order_status:
                print('ordes', order.id)

                if order.status in order_status_list:
                    data = {}

                    print("--------------------------------------", order.status)
                    print("order.status", order.status)
                    order_status_list.remove(order.status)
                    data['status'] = order.status
                    data[order.status] = True
                    data['city'] = None
                    data['date'] = str(order.date)
                    location_list.append(data)
                    print(location_list)
                    print("data[order.status]", data[str(order.status)])
                    print("---------------end--------------------")

            else:
                pass

        shift_order = OrderStatus.objects.filter(order__unique_order_id=order_id, status="packed")
        if shift_order:
            logistics_order_obj = LogisticOrder.objects.filter(order__unique_order_id=order_id)
            if logistics_order_obj:
                waybill_no = logistics_order_obj[0].waybill_no
                # scan_details = track_order(waybill_no)
                scan_details = [
                    {
                        "status": "Manifested",
                        "status_code": "UD",
                        "scan_location": "HQ (Haryana)",
                        "remark": "Consignment Manifested",
                        "scan_date_time": "2017-06-07 14:05:57"
                    },
                    {
                        "status": "In Transit",
                        "status_code": "UD",
                        "scan_location": "Surat_Pandesra_Gateway (Gujarat)",
                        "remark": "Shipment Picked Up from Client Location",
                        "scan_date_time": "2017-06-07 18:11:26"
                    }
                ]
                if scan_details:
                    for scan in scan_details:
                        print("-----scan_details------",scan)
                        print("-----scan_details2------",scan['status'])
                        if scan["status"] in order_status_list:
                            print("---scan_details-##########", scan["status"])
                            data['status'] = scan["status"]
                            data[scan["status"]] = True
                            data['city'] = [scan["scan_location"]]
                            data['date'] = scan["scan_date_time"]
                            location_list.append(data)
                            print(location_list)

            # if order_status_list:
            #     for status in order_status_list:
            #         data['status'] = status
            #         data[status] = False
            #         data['city'] = ""
            #         data['date'] = ""
            #         location_list.append(data)

            return Response(
                {
                    'status': 1,
                    'orders': order_list,
                    'orders_tracking': location_list,
                }
            )
        else:
            return Response(
                {
                    'status': 2,
                    'orders': order_list,
                    'orders_tracking': location_list,
                }
            )
        """
        location_list = []
        order_list = []
        data = {}
        order_status_list = ["placed", "packed", "dispatched", "In Transit", "delivered"]
        order_id = request.data.get("order_id")
        order_status = OrderStatus.objects.filter(order__unique_order_id=order_id).order_by('status', 'date').distinct(
            'status')
        if order_status:
            for order in order_status:
                if order.status in order_status_list:
                    data = {}

                    order_status_list.remove(order.status)
                    data['status'] = order.status
                    data['active'] = True
                    data['city'] = None
                    data['date'] = str(order.date)
                    location_list.append(data)
            else:
                pass

            shift_order = OrderStatus.objects.filter(order__unique_order_id=order_id, status="packed")
            if shift_order:
                logistics_order_obj = LogisticOrder.objects.filter(order__unique_order_id=order_id)
                if logistics_order_obj:
                    waybill_no = logistics_order_obj[0].waybill_no
                    scan_details_dict = track_order(waybill_no)

                    if scan_details_dict:
                        if "scan_details" in scan_details_dict.keys():
                            if scan_details_dict["scan_details"]:
                                scan_details = scan_details_dict["scan_details"]
                                for scan in scan_details:
                                    if scan["status"] in order_status_list:
                                        data = {"status": scan["status"], "active": True,
                                                "city": [scan["scan_location"]],
                                                "date": scan["scan_date_time"]}
                                        location_list.append(data)

            status_list = []
            for i in location_list:
                status_list.append(i['status'])

            if order_status_list:
                for i in order_status_list:
                    if i not in status_list:
                        data = {"status": i, "active": False}
                        location_list.append(data)

            order_status_list = ["placed", "packed", "dispatched", "In Transit", "delivered"]
            lists = []
            for data in order_status_list:
                index = next(item for item in location_list if item["status"] == data)
                lists.append(index)

            return Response(
                {
                    'status': 1,
                    'message': "Order found",
                    'orders': order_list,
                    'orders_tracking': lists,
                }
            )

        else:
            return Response(
                {
                    'status': 2,
                    'message': "Order Doesn't exist",
                    'orders': order_list,
                    'orders_tracking': location_list,
                }
            )


class OrderTrackingApiV2(generics.ListAPIView):
    """
    This Api is to Users all orders
    Url: ecomapi/user_orders
    Authentication token required
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User toke is required..TODO
    method = get
    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    def post(self, request, *args, **kwargs):
        location_list = []
        order_list = []
        new_list = []
        order_status_list = ["placed", "packed", "dispatched", "In Transit", "delivered"]
        order_id = request.data.get("order_id")
        order = Order.objects.filter(unique_order_id=order_id).first()
        if order:
            current_status = order.status

            if current_status == 'in_transit':
                current_status = "In Transit"

            if current_status == 'return':
                order_status_list.append("return_initiated")
                order_status_obj = OrderStatus.objects.filter(order__id=order.id)
                if order_status_obj:
                    orders_list = order_status_obj.values_list('status')
                    if "admin_return_reject" in orders_list:
                        order_status_list.append('return_rejected')
                    elif "admin_return_confirm" in orders_list:
                        if "REV In-Transit" in orders_list:
                            order_status_list.append("return_intransit")
                        elif "REV Delivered" in orders_list:
                            order_status_list.append("return_intransit")
                            order_status_list.append("return_delivered")

            if current_status in order_status_list:
                get_last_index = order_status_list.index(current_status)
                get_last_index += 1

                for j in range(0, get_last_index):
                    data = {}
                    new_status = order_status_list[j]
                    data['status'] = new_status
                    data['active'] = True
                    data['city'] = None
                    data['date'] = str(order.date)
                    new_list.append(new_status)
                    location_list.append(data)

            for i in order_status_list:
                if i not in new_list:
                    data = {}
                    data['status'] = i
                    data['active'] = False
                    data['city'] = None
                    data['date'] = None
                    location_list.append(data)

            return Response(
                {
                    'status': 1,
                    'message': "Order found",
                    'orders': order_list,
                    'orders_tracking': location_list,
                }
            )

        else:
            return Response(
                {
                    'status': 2,
                    'message': "Order Doesn't exist",
                    'orders': order_list,
                    'orders_tracking': location_list,
                }
            )


# @csrf_exempt
def get_approval(request):
    """
      param: data, warehouse_list

    """
    if request.method == "POST":
        ware_house_list = request.POST.getlist('warehouse_list')
        if ware_house_list:
            warehouse_list = ware_house_list[0].split(',')
            get_warehouse = SellerWarehouse.objects.filter(warehouse_id__in=warehouse_list,
                                                           approval_status__icontains="pending")
            if get_warehouse:
                email_id = "onboard@ithinklogistics.com"
                message_body = ""
                head_part = "Hello Logistics,\n\nWe have added Warehouses.\nPlease approve it.\n\n"
                message_body += head_part
                footer_part = "\n\nRegards,\nCreditKart"
                for warehouse in get_warehouse:
                    message = "Id" + " = " + str(warehouse.warehouse_id) + "  Company name" + " = " + str(
                        warehouse.company_name) + "\n"
                    message_body += message
                message_body += footer_part
                email_kwargs = {
                    'subject': 'Warehouse Approval',
                    'to_email': [email_id],
                    'cc_mail': ["jayvant@mudrakwik.com", "test@thecreditkart.com"],
                    'body': message_body
                }
                email(**email_kwargs)

    return redirect("/ecomapi/seller_warehouse_details/")


def save_current_logistics_status(order_obj, data):
    """
        saves logistics track order current status in OrderStatus
    :param order_obj:
    :param data:
    :return:
    """
    get_status = OrderStatus.objects.filter(status=data["status"], order=order_obj)
    if not get_status:
        add_status = OrderStatus.objects.create(status=data["status"], order=order_obj, date=data["date"])
        if add_status:
            return add_status
    return None


def save_order_status(order_obj, data):
    """
            param: data, order_obj , data
            description : This function save order status  using scan_date_time,order_obj .
    """
    get_status = OrderStatus.objects.filter(status=data["status"], order=order_obj)
    if not get_status:
        add_status = OrderStatus.objects.create(status=data["status"], order=order_obj, date=data["scan_date_time"])
        if add_status:
            return add_status
    return None


def create_order_location(data, order_obj, scan_status):
    """
        param: data, order_obj , scan_status
        description : This function save order status  using scan_status,remark and scan_location.
    """
    create_location = OrderLocation.objects.create(remark=data['remark'],
                                                   address=data['scan_location'],
                                                   date=data['scan_date_time'],
                                                   )
    add_status = OrderStatus.objects.filter(status=scan_status,
                                            order=order_obj,
                                            date=data["scan_date_time"]).order_by('-id').first()
    if not add_status:
        save_order_status(order_obj, data)
    if add_status:
        add_status.Location.add(create_location.id)
    if scan_status != "in_transit" and scan_status.lower() in ["in transit", "intransit", "in_transit"]:
        data["status"] = "in_transit"
        save_order_status(order_obj, data)


def handle_create_order_location(data, order_obj, scan_status):
    """
    param: data, order_obj , scan_status
    description : This function create location using scan_status,remark and scan_location.
    """
    get_order_location = OrderLocation.objects.filter(remark=data['remark'],
                                                      address=data['scan_location'],
                                                      date=data['scan_date_time'],
                                                      )
    if not get_order_location:
        create_order_location(data, order_obj, scan_status)


def refund_delivery_charge_to_wallet(order_obj, order_details_obj, status):
    """
    param: order_obj, order_detail_obj , status
    description : This function get delivery charge using get_orders_delivery_charge
                  and update wallet using user_wallet_update
    """
    try:

        user_id = order_details_obj.user.id
        order_id = order_obj.id
        from EApi.order import get_orders_delivery_charge
        delivery_charge = get_orders_delivery_charge(order_id)

        from EApi.wallet import user_wallet_update
        UserWallet = apps.get_model('ecom', 'UserWallet')
        get_user_wallet = UserWallet.objects.filter(order=order_obj, status=status)
        if not get_user_wallet:
            add_wallet = user_wallet_update(user_id, delivery_charge, order_details_obj, order_obj, status)

    except Exception as e:
        print('-----------in exception- refund_delivery_charge_to_wallet---------')
        print(e.args)
        import os
        import sys
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        snd_exc(str(e.args), str(exc_type), str(fname), str(exc_tb.tb_lineno))
        print("exc data", exc_type, fname, exc_tb.tb_lineno)


def update_order_status(order_id):
    if order_id is not None:
        logistic_obj = LogisticOrder.objects.filter(order=order_id).exclude(order__status='cancelled')
        if logistic_obj:
            order_list = ['dispatched', 'in_transit', 'delivered', 'return']
            existing_status_list = []
            order_obj = logistic_obj[0].order

            get_existing_status_list = OrderStatus.objects.filter(order=order_id).values_list('status', flat=True)
            if get_existing_status_list:
                existing_status_list = get_existing_status_list
            waybill_no = logistic_obj[0].waybill_no

            scan_details_dict = track_order(waybill_no)

            if scan_details_dict:
                # updating current status
                if "current_status" in scan_details_dict.keys():
                    if scan_details_dict["current_status"]:
                        current_status = scan_details_dict["current_status"]

                        # saving current status
                        current_status_data = {"status": current_status, "date": datetime.now()}
                        save_current_logistics_status(order_obj, current_status_data)

                        our_status = get_logistics_status(current_status)

                        get_order_obj = Order.objects.filter(id=order_id)
                        if get_order_obj:
                            if our_status is not None:
                                current_status = our_status
                                if current_status != get_order_obj[0].status:
                                    update_order_status = Order.objects.filter(id=order_id).update(
                                        status=current_status, updated_date=datetime.now())

                            # Checking for Undelivered
                            # check for RTO in current status from UNDELIVERED orders
                            if our_status is None:
                                if "RTO" in current_status or current_status.lower() == "undelivered":

                                    get_return_order = ReturnOrder.objects.filter(order_id=order_id,
                                                                                  verify_return_status="return_confirm")
                                    if not get_return_order:
                                        our_status = "undelivered"
                                        previous_undelivered = OrderStatus.objects.filter(order=order_id,
                                                                                          status="undelivered")
                                        if not previous_undelivered:
                                            from EApi.order import handel_undelivered_order
                                            try:
                                                handel_undelivered_condition = handel_undelivered_order(order_id)
                                            except Exception as e:
                                                print("Inside Exception of handel_undelivered_condition", e)

                            # Return cancel order
                            if current_status in ['REV Cancelled', "rev cancelled", "rev_cancelled",
                                                  "return_cancelled"]:
                                ret_cancel_status = "return_cancelled"
                                get_return_cancel_order = OrderStatus.objects.filter(order=order_id,
                                                                                     status=ret_cancel_status)
                                if not get_return_cancel_order:
                                    update_status = Order.objects.filter(id=order_id).update(
                                        status=ret_cancel_status, updated_date=datetime.now())

                                    current_status_data = {"status": ret_cancel_status, "date": datetime.now()}
                                    save_current_logistics_status(order_obj, current_status_data)

                                    order_details_obj = OrderDetails.objects.filter(order_id=order_obj.id).first()
                                    if order_details_obj:
                                        return_cancel_status = "return_cancel_delivery_charge"
                                        refund_delivery_charge_to_wallet(order_obj, order_details_obj,
                                                                         return_cancel_status)

                # Save status in OrderStatus
                if "scan_details" in scan_details_dict.keys():
                    if scan_details_dict["scan_details"]:
                        scan_details = scan_details_dict["scan_details"]
                        get_order_status = Order.objects.filter(id=order_id).first()
                        for scan in scan_details:
                            scan_status = scan["status"]
                            if scan_status not in existing_status_list:
                                if scan_status.lower() in ["in transit", "intransit", "in_transit"]:
                                    get_transit_status = OrderStatus.objects.filter(status=scan_status,
                                                                                    order=order_obj).first()
                                    if get_transit_status:
                                        handle_create_order_location(scan, order_obj, scan_status)
                                    else:
                                        handle_create_order_location(scan, order_obj, scan_status)
                                else:
                                    save_order_status(order_obj, scan)

                        # adding in between status status
                        last_status = get_order_status.status
                        if last_status not in existing_status_list or existing_status_list == [] or existing_status_list is None:
                            if last_status in order_list:
                                get_last_index = order_list.index(last_status)
                                get_last_index += 1
                                for j in range(0, get_last_index):
                                    new_status = order_list[j]
                                    get_status = OrderStatus.objects.filter(status=new_status,
                                                                            order=logistic_obj[0].order)
                                    if not get_status or get_status is None:
                                        add_status = OrderStatus.objects.create(status=new_status,
                                                                                order=logistic_obj[0].order,
                                                                                date=datetime.now())
    return HttpResponse("ok")


def get_logistics_status(status):
    """
    param: status
    description : converts logistics status and returns status from our project
    :return:
    """
    our_status = None
    status_list = ["manifested", "picked up", "in transit", "delivered", "cancelled", "rev manifest", "rev delivered"]
    status_dict = {
        'Manifested': 'packed',
        'Picked Up': 'dispatched',
        'In Transit': 'in_transit',
        'InTransit': 'in_transit',
        'In_Transit': 'in_transit',
        'Delivered': 'delivered',
        'Cancelled': 'cancelled',
        'REV Manifest': 'return_approved',
        'REV Delivered': 'return',
        'REV Cancelled': 'return_cancelled',

        'manifested': 'packed',
        'picked up': 'dispatched',
        'in transit': 'in_transit',
        'delivered': 'delivered',
        'cancelled': 'cancelled',
        'rev manifest': 'return_approved',
        'rev delivered': 'return',
        'rev cancelled': 'return_cancelled',

        'picked_up': 'dispatched',
        'in_transit': 'in_transit',
        'rev_manifest': 'return_approved',
        'rev_delivered': 'return',
        'rev_cancelled': 'return_cancelled',

    }
    if status in status_dict:
        our_status = status_dict[status]
    return our_status


def fetch_logistics_status_request(request):
    """
    description :This function is to call by scheduler to fetch all orders after day before yesterday..
    """
    fetch_logistics_status()
    return HttpResponse('Okay')


def fetch_logistics_status():
    """
    description : This function is to call by scheduler to fetch all orders after day before yesterday..
    """
    today = datetime.today()
    day_before_yesterday = today - timedelta(days=10)
    get_orders = LogisticOrder.objects.filter(created_date__gte=day_before_yesterday)
    if get_orders:
        for order in get_orders:
            status_update = update_order_status(order.order.id)


def fetch_logistics_status_2():
    """
    description : This function is to call by scheduler to fetch all orders after day before yesterday..
    """
    today = datetime.today()
    day_before_yesterday = today - timedelta(days=20)
    get_orders = LogisticOrder.objects.filter(created_date__gte=day_before_yesterday)
    if get_orders:
        for order in get_orders:
            status_update = update_order_status(order.order.id)


def update_multiple_warehouse_status(request):
    """
    description : This function calls get warehouse API and update sellers multiple warehouse status..
    """
    if request.method == "POST":
        ware_house_list = request.POST.getlist('warehouse_list')
        split_ware_house_list = ware_house_list[0].split(",")
        if split_ware_house_list:
            for ware_house_id in split_ware_house_list:
                status = None
                warehouse_data = get_warehouse_status(ware_house_id)
                if warehouse_data:
                    status = warehouse_data["status"]
                if status:
                    # Update warehouse details exclude status approved..
                    warehouse_obj = SellerWarehouse.objects.filter(warehouse_id=ware_house_id).exclude(
                        approval_status='approved')
                    if warehouse_obj:
                        update_status = SellerWarehouse.objects.filter(warehouse_id=ware_house_id).update(
                            approval_status=status)
    return HttpResponseRedirect(reverse("seller_ware_house_details"))


def update_multiple_shiprocket_warehouse_status(request):
    """
    description : This function calls get warehouse API and update sellers multiple warehouse status..
    """
    if request.method == "POST":
        ware_house_list = request.POST.getlist('warehouse_list')
        split_ware_house_list = ware_house_list[0].split(",")
        if split_ware_house_list:
            for ware_house_id in split_ware_house_list:
                # Update warehouse details exclude status approved..
                warehouse_obj = SellerWarehouse.objects.filter(id=ware_house_id, type="shiprocket")
                if warehouse_obj:
                    SellerWarehouse.objects.filter(id=warehouse_obj[0].id).update(approval_status='approved')

    return HttpResponseRedirect(reverse("seller_ware_house_details"))


def get_orders_logistic_partner(order_id, order_mode):
    """
        param : order_id, order_mode
        description : This function return logistic type of logistic order by filtering order and
        order mode
    """
    logistic = None
    if None not in (order_id, order_mode):
        get_logistic_order = LogisticOrder.objects.filter(order=order_id, status="success",
                                                          order_mode=order_mode).order_by('id').last()
        if get_logistic_order:
            logistic = get_logistic_order.type

    return logistic


def get_order_details_logistic_partner(order_details_id, order_mode):
    """
    param : order_details_id, order_mode
    description : This function return logistic type of logistic order by filtering order details and
    order mode
    """
    logistic = None
    if None not in (order_details_id, order_mode):
        get_logistic_order = LogisticOrder.objects.filter(order_details=order_details_id, status="success",
                                                          order_mode=order_mode).order_by('id').last()
        if get_logistic_order:
            logistic = get_logistic_order.type

    return logistic
