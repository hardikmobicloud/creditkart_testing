import requests
from django.conf import settings
from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.template.loader import render_to_string
from rest_framework import generics
from rest_framework.response import Response


def email(*args, **email_kwargs):
    """
    This function is used to send email. It contains to email, cc mail,
    subject, body and from email.
    """
    try:
        mail_subject = email_kwargs['subject']


        body = str(email_kwargs['body'])
        to_email = email_kwargs['to_email']
        cc_mail = email_kwargs['cc_mail']
        from_email = settings.FROM_EMAIL_ADDRESS
        email = EmailMessage(mail_subject, body, from_email, [to_email], cc=[cc_mail])
        # email.content_subtype = "text"
        email.send()
        return True

    except Exception as e:
        import os
        import sys
        print('inside else============', e)
        print("-------rbl save data-------", e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        return False


def order_email(*args, **email_kwargs):
    """
    This function is used to send order email. It contains subject, email_template,
    to email, from mail, email data.
    """
    try:
        mail_subject = email_kwargs['subject']
        email_template = email_kwargs['email_template']

        message = render_to_string(
            email_template,
            email_kwargs['data']
        )

        to_email = email_kwargs['to_email']

        from_email = settings.FROM_EMAIL_ADDRESS
        email = EmailMessage(mail_subject, message, from_email, [to_email])
        email.content_subtype = "html"
        email.send()
        return True

    except Exception as e:

        print('inside else============', e)
        return False


def sms(*args, **sms_kwrgs):
    """
    This function is used to send sms. It conatins message and number to which sms have to send.
    """
    try:

        sms_message = sms_kwrgs['sms_message']
        number = sms_kwrgs['number']
        if "tmpid" in sms_message and sms_message["tempid"] is not None:
            tempid = sms_message["tempid"]
        else:
            tempid = ""

        sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
            settings.SMS_USERNAME) + '&password=' + str(settings.SMS_PASSWORD) + \
                  '&type=0&dlr=1&destination=' + number + '&source=' + str(
            settings.SMS_SOURCE) + '&message=' + sms_message + '&tempid=' + tempid

        requests.get(sms_url)

        return True

    except:

        return False


def transaction_sms(*args, **tsms_kwrgs):
    '''

        Sr. No. Error Code Description
        1   1701
        Success, Message Submitted Successfully. In this case you will receive
        the response 1701|<CELL_NO>|<MESSAGE ID>. The message Id can
        then be used later to map the delivery reports to this message.
        2   1702
        Invalid URL Error. This means that one of the parameters was not
        provided or left blank.
        3 1703 Invalid value in username or password parameter.
        4 1704 Invalid value in type parameter.
        5 1705 Invalid message.
        6 1706 Invalid destination.
        7 1707 Invalid source (Sender).
        8 1708 Invalid value for dlr parameter.
        9 1709 User validation failed
        10 1710 Internal error.
        11 1025 Insufficient credit.
        12 1715 Response timeout.

    http://<server>:8080/bulksms/bulksms?
    username=XXXX&password=YYYYY&type=Y&dlr=Z&destination=QQQQQQQQQ&source=RRRR&message=
    SSSSSSSS<&url=KKKK>

    :param args:
    :param tsms_kwrgs:
    :return:

    '''

    # print("trsanaction called")
    SMS_USERNAME = str(settings.TSMS_USERNAME)
    SMS_PASSWORD = str(settings.TSMS_PASSWORD)
    SMS_SOURCE = str(settings.TSMS_SOURCE)
    SMS_SERVER = "sms6.rmlconnect.net"

    try:
        sms_message = tsms_kwrgs['sms_message']
        number = tsms_kwrgs['number']

        sms_url = 'http://' + SMS_SERVER + ':8080/bulksms/bulksms?username=' + SMS_USERNAME + '&password=' + SMS_PASSWORD + \
                  '&type=0&dlr=1&destination=' + number + '&source=' + SMS_SOURCE + '&message=' + sms_message

        sms_url2 = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
            settings.TSMS_USERNAME) + '&password=' + str(settings.TSMS_PASSWORD) + \
                   '&type=0&dlr=1&destination=' + number + '&source=' + str(
            settings.TSMS_SOURCE) + '&message=' + sms_message
        requests.get(sms_url)

        return True

    except Exception as e:
        print("======Exception T sms====", e)
        return False


class ContactUsMailSend(generics.ListAPIView):
    """
    This API is to send email for contact us from customer to get their queries..
    """

    def post(self, request, *args, **kwargs):
        try:
            name = request.data.get('name')
            mobile_no = request.data.get('mobile_no')
            email_id = request.data.get('email_id')
            subject = request.data.get('subject')
            message = request.data.get('message')
            to_email = "hello@thecreditkart.com"
            from_email = settings.FROM_EMAIL_ADDRESS
            body = "Name: " + name + "\nMobile No: " + mobile_no + "\nEmail ID: " + email_id + \
                   "\nSubject: " + subject + "\nMessage: " + message + "\n"
            email = EmailMessage(subject, body, from_email, [to_email])
            email.send()

            return Response(
                {
                    'status': 1,
                    'message': 'Email sent successfully',
                }
            )

        except Exception as e:
            print("======Exception T sms====", e)

            return Response(
                {
                    'status': 2,
                    'message': "Something went wrong"
                }
            )


class CareerPortalMailSend(generics.ListAPIView):
    """
    This API is to send email for career portal from customer to know about career information..
    """

    def post(self, request, *args, **kwargs):
        try:
            name = request.data.get('name')
            mobile_no = request.data.get('mobile_no')
            email_id = request.data.get('email_id')
            subject = request.data.get('subject')
            message = request.data.get('message')
            to_email = "hello@thecreditkart.com"
            from_email = settings.FROM_EMAIL_ADDRESS
            body = "Name: " + name + "\nMobile No: " + mobile_no + "\nEmail ID: " + email_id + \
                   "\nSubject: " + subject + "\nMessage: " + message + "\n"
            email = EmailMessage(subject, body, from_email, [to_email])
            email.send()

            return Response(
                {
                    'status': 1,
                    'message': 'Email sent successfully',
                }
            )

        except Exception as e:
            print("======Exception T sms====", e)

            return Response(
                {
                    'status': 2,
                    'message': "Something went wrong"
                }
            )


def ecom_email(*args, **email_kwargs):
    """
    This function is used to send ecom mail. It contains subject, email_template, to email, email data
    and cc email, to email and from email.
    """
    try:
        mail_subject = email_kwargs['subject']
        email_template = email_kwargs['email_template']

        message = render_to_string(
            email_template,
            email_kwargs['data']
        )

        to_email = email_kwargs['to_email']
        from_email = settings.FROM_EMAIL_ADDRESS

        if 'cc_mail' in email_kwargs:
            cc_mail = email_kwargs['cc_mail']
            email = EmailMessage(mail_subject, message, from_email, [to_email], cc=[cc_mail])

        email = EmailMessage(mail_subject, message, from_email, [to_email])
        email.content_subtype = "html"
        email.send()

        return True

    except:

        return False


def test_email(request):
    """
    This function is used to test email.
    """
    to_mail = 'keval.thakkar@mudrakwik.com'
    cc_mail = 'jayvant@mudrakwik.com'
    email_kwargs = {
        'subject': 'Creditkart - Test mail',
        'to_email': to_mail,
        'cc_mail': cc_mail,
        'email_template': 'Ecom/emails/testing_email.html',
        "body": "test",
        'data': {
            'domain': "Site.objects.get_current()",
            'a': "testing"
        },
    }

    ecom_email(**email_kwargs)
    return HttpResponse("ok")


from MApi.email import sms


def order_sms(*args, **osms_kwrgs):
    """
    This function is used to send order sms. It contains sms type, sms message and number of sender.
    Sms type is nothing but status of order. According to sms type the sms will send to sender.
    The sms message is in settings.py File.
    """
    if "sms_type" not in osms_kwrgs or "number" not in osms_kwrgs:
        return False

    sms_type = osms_kwrgs['sms_type']
    number = osms_kwrgs['number']
    if "tempid" in osms_kwrgs:
        tempid = osms_kwrgs["tempid"]
    else:
        tempid = None

    if sms_type == "placed":
        sms_message = settings.PLACED_MESSAGE
        tempid = settings.PLACED_MESSAGE_TEMPID

    elif sms_type == "cancelled":
        sms_message = settings.CANCELLATION_MESSAGE
        tempid = settings.CANCELLATION_MESSAGE_TEMPID

    elif sms_type == "phonepe_cancelled":
        sms_message = settings.PHONEPE_CANCELLATION_MESSAGE
        tempid = settings.CANCELLATION_MESSAGE_TEMPID

    elif sms_type == "return":
        sms_message = settings.RETURN_MESSAGE
        tempid = settings.RETURN_MESSAGE_TEMPID

    elif sms_type == "repayment":
        sms_message = settings.REPAYMENT_MESSAGE
        tempid = settings.REPAYMENT_MESSAGE_TEMPID

    elif sms_type == "seller":
        sms_message = settings.SELLER_PLACED_MESSAGE
        tempid = settings.SELLER_PLACED_MESSAGE_TEMPID

    elif sms_type == "pending":
        sms_message = settings.PENDING_MESSAGE


    else:
        return False

    sms_kwrgs = {
        'sms_message': sms_message,
        'number': str(number),
        'tempid': tempid
    }
    sms(**sms_kwrgs)
