import datetime
from functools import partial

import json
from hashlib import sha256

from MApi.permissions import GuestTokenPermission
from User.models import *
from django.apps import apps
from django.contrib.auth.models import User
from rest_framework import generics
from rest_framework.authentication import *
from rest_framework.authtoken.models import Token
from rest_framework.permissions import *
from rest_framework.response import Response
from rest_framework.views import APIView

Category = apps.get_model('ecom', 'Category')
SubCategory = apps.get_model('ecom', 'SubCategory')
Colour = apps.get_model('ecom', 'Colour')
Size = apps.get_model('ecom', 'Size')
SGST = apps.get_model('ecom', 'SGST')
CGST = apps.get_model('ecom', 'CGST')
Product = apps.get_model('ecom', 'Product')
ProductDetails = apps.get_model('ecom', 'ProductDetails')
Seller = apps.get_model('ecom', 'Seller')
Credit = apps.get_model('ecom', 'Credit')
Stock = apps.get_model('ecom', 'Stock')
Order = apps.get_model('ecom', 'Order')
OrderDetails = apps.get_model('ecom', 'OrderDetails')
OrderDetailsOrder = apps.get_model('ecom', 'orderdetails_order_id')
Images = apps.get_model('ecom', 'Images')
NewsLetter = apps.get_model('ecom', 'NewsLetter')
EcomDocuments = apps.get_model('ecom', 'EcomDocuments')
AuthenticationPin = apps.get_model('User', 'AuthenticationPin')
Subscription = apps.get_model('User', 'Subscription')
Address = apps.get_model('User', 'Address')
Personal = apps.get_model('User', 'Personal')


class UserLoginApi(generics.ListAPIView):
    """
    This Api is to check login and subscription of a user.

    Url: ecomapi/user_login

    Response status:
        1 : "Mobile no. is not registered"
        2 : "Incorrect Pin"
        3 : "User Not Subscribed yet"
        4 : "User Subscription Expired"
        5 : "User Subscribed"

    Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..TODO
    method = post
    parameter = {
                    'user': 1      This will get from user token
                    'pin_no': 1234
                }

    Response:

            {
            "status": "1",
            "message": "Pin verified successfully.",
            }
    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        mobile_no = request.data.get('username')
        pin = request.data.get('pin')
        today_date = datetime.datetime.now()
        # Check User instance or not
        user_instance = User.objects.filter(username=mobile_no)
        if user_instance:
            # Check user pin is valid or not.
            user_pin_instance = AuthenticationPin.objects.filter(user=user_instance[0])
            if user_pin_instance and user_pin_instance is not None:
                correct_pin = False
                for user_pin_no in user_pin_instance:
                    user_pin = user_pin_no.pin
                    if user_pin == pin:
                        correct_pin = True
                        break

                if correct_pin:
                    # Checking Users all personal details exists or not..
                    is_profile_filled = False
                    personal_data = Personal.objects.filter(user=user_instance[0].id)
                    address_data = Address.objects.filter(user=user_instance[0].id)
                    if personal_data and address_data:
                        is_profile_filled = True

                    try:
                        from EApi.wallet import add_default_wallet_amount
                        add_default_wallet_amount(mobile_no)
                    except Exception as e:
                        print("Inside Exception of add_default_wallet_amount in UserLogin API")

                    user_token = ""
                    # token = Token.objects.filter(user=user_instance[0].id)
                    token = Token.objects.get_or_create(user=user_instance[0])
                    if token:
                        user_token = token[0].key

                    return Response(
                        {
                            'status': 5,
                            'token': user_token,
                            'message': "User Subscribed",
                            'is_profile_filled': is_profile_filled
                        }
                    )

                else:
                    return Response(
                        {
                            'status': 2,
                            'message': "Incorrect Pin"
                        }
                    )

            else:
                return Response(
                    {
                        'status': 2,
                        'message': "Pin is not Set"
                    }
                )

        else:
            return Response(
                {
                    'status': 1,
                    'message': "Mobile no. is not registered"
                }
            )


class UserEcomAddresses(generics.ListAPIView):
    """
    This Api is to give All addresses of a user saved for Ecom delivery address.
    Url: ecomapi/user-ecom-address
    Authentication token required
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..TODO
    method = get
    parameter = {
                    'user': 1      This will get from user token
                }
    Response:
            {
                "status": "1",
                "address_list": [
                            {
                                "address_id": 35568,
                                "address": "Murungapakkam Near Government Hospital Backside"
                            },
                            {
                                "address_id": 35571,
                                "address": "Opp Punjab National Bank Sakinaka"
                            }
                        ],
            }
    Empty Response:
            {
                "status": "2",
                "address": [],
            }
    """

    authentication_classes = (TokenAuthentication,)

    # permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        data = {}
        address_biling = []
        address_shipping = []
        user_id = request.user.id
        user_instance = User.objects.filter(id=user_id)
        if user_instance:
            address_info = Address.objects.filter(user=user_instance[0], type='Billing').order_by('-id').first()
            if address_info:
                # for address_info in address_info:
                billing_address = {
                    "id": address_info.id,
                    "address": address_info.address,
                    "city": address_info.city,
                    "state": address_info.state,
                    "pin_code": address_info.pin_code,
                    "type": address_info.type,
                    "user": address_info.user_id
                }
                address_biling.append(billing_address)
            address = Address.objects.filter(user=user_instance[0], type='Shipping').order_by('-id').first()
            if address:
                # for address in address_information:
                shipping_address = {
                    "id": address.id,
                    "address": address.address,
                    "city": address.city,
                    "state": address.state,
                    "pin_code": address.pin_code,
                    "type": address.type,
                    "user": address.user_id
                }
                address_shipping.append(shipping_address)
            data = {
                'billing_addr': address_biling,
                'shipping_addr': address_shipping,
            }
        return Response(
            {
                'data': data,
            }
        )

    def post(self, request, *args, **kwargs):
        """
        This api is for create user's new address. It will create new address entry of user in Address table.
        """
        address = request.data['address']
        city = ""
        if 'city' in request.data:
            city = request.data['city']
        state = request.data['state']
        pin_code = request.data['pincode']
        addr_type = request.data['type']
        user = request.user.id
        name = request.data['name']
        email_id = request.data['email']
        mobile_no = request.data['mobile']
        # user = 75
        user_instance = User.objects.filter(id=user)
        if user_instance:
            already_exist = Address.objects.filter(user=user_instance[0], type=addr_type,
                                                   personal__type__in=["credit", "cash"], personal__name=name,
                                                   personal__email_id=email_id, personal__mobile_no=mobile_no,
                                                   address=address, city=city,
                                                   pin_code=pin_code, state=state)
            if already_exist:
                return Response(
                    {
                        'status': 2,
                        'address_id': already_exist[0].id,
                        'message': "Address Already Exist"
                    }
                )

            else:
                # Creating Personal Info in Personal Table..
                add_personal = Personal.objects.create(user=user_instance[0], name=name, mobile_no=mobile_no,
                                                       email_id=email_id, type="cash")
                if add_personal:
                    address = Address.objects.create(user=user_instance[0], type=addr_type, address=address, city=city,
                                                     pin_code=pin_code, state=state, personal=add_personal)
                else:
                    address = Address.objects.create(user=user_instance[0], type=addr_type, address=address, city=city,
                                                     pin_code=pin_code, state=state)

                if address:
                    return Response(
                        {
                            'status': 1,
                            'message': "Address created",
                            "data": {
                                'address_id': address.id,
                                "address": address.address,
                                "city": city,
                                "state": state,
                                "pin_code": pin_code,
                                "type": "Shipping",
                                "user": user,
                                "name": address.personal.name,
                                "email_id": address.personal.email_id,
                                "mobile_no": address.personal.mobile_no
                            }
                        }
                    )

        else:
            return Response(
                {
                    'status': 1,
                    'message': "User Not found"
                }
            )


class UserEcomShippingAddress(generics.ListAPIView):
    """
    This Api is to give only shipping addresses of a user saved for Ecom delivery address.
    Url: ecomapi/user-ecom-address
    Authentication token required
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..TODO
    method = get
    parameter = {
                    'user': 1      This will get from user token
                }
    Response:
               data = {
                'shipping_addr': [{
                "id": 9,
                "address": " Near Government Hospital Backside",
                "city": "Mumbai",
                "state": "Maharashtra",
                "pin_code": "411002",
                "type": "Shipping",
                "created_date": "2019-09-13T11:48:12.077717",
                "updated_date": "2019-09-13T11:48:12.077748",
                "user": 2
            }]
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        address_shipping = []
        personal_info_present = "False"
        user = request.user.id
        # user = 196
        status_code = '3'
        user_instance = User.objects.filter(id=user)
        if user_instance:
            personal_info_present = check_personal_info(user_instance[0].id, "cash")
            if personal_info_present:
                status_code = '2'
                address_information = Address.objects.filter(user=user_instance[0], type='Shipping',
                                                             personal__type__in=["cash", "credit"])
                if address_information and address_information is not None:
                    status_code = '1'
                    for address in address_information:
                        name = None
                        email = None
                        mobile = None
                        if address.personal:
                            name = address.personal.name
                            email = address.personal.email_id
                            mobile = address.personal.mobile_no
                            shipping_address = {
                                "address_id": address.id,
                                "address": address.address,
                                "city": address.city,
                                "state": address.state,
                                "pin_code": address.pin_code,
                                "type": address.type,
                                "user": address.user_id,
                                "name": name,
                                "email": email,
                                "mobile": mobile
                            }
                            address_shipping.append(shipping_address)

            else:
                return Response(
                    {
                        'status_code': status_code,
                        'address': address_shipping,
                        'personal_info_present': personal_info_present
                    }
                )

        return Response(
            {
                'status_code': status_code,
                'address': address_shipping,
                'personal_info_present': personal_info_present
            }
        )


class UserEcomUpdatedAddress(generics.ListAPIView):
    """
    This Api is to give only last addresses of a user saved for Ecom delivery address.
    Url: ecomapi/user-ecom-address
    Authentication token required
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..TODO
    method = get
    parameter = {
                    'user': 1      This will get from user token
                }
    Response:
               data = {'billing_addr': [{
            "id": 9,
            "address": "Murungapakkam Near Government Hospital Backside",
            "city": "Pune",
            "state": "Maharashtra",
            "pin_code": "411002",
            "type": "Billing",
            "created_date": "2019-09-13T11:48:12.077717",
            "updated_date": "2019-09-13T11:48:12.077748",
            "user": 2
        }],
            'shipping_addr': [{
                "id": 9,
                "address": " Near Government Hospital Backside",
                "city": "Mumbai",
                "state": "Maharashtra",
                "pin_code": "411002",
                "type": "Shipping",
                "created_date": "2019-09-13T11:48:12.077717",
                "updated_date": "2019-09-13T11:48:12.077748",
                "user": 2
            }]
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        data = {}
        address_biling = []
        address_shipping = []
        user = request.user.id
        user_instance = User.objects.filter(id=user)
        if user_instance:
            address_info = Address.objects.filter(user=user_instance[0], type='Billing').order_by('id').last()
            if address_info:
                billing_address = {
                    "id": address_info.id,
                    "address": address_info.address,
                    "city": address_info.city,
                    "state": address_info.state,
                    "pin_code": address_info.pin_code,
                    "type": address_info.type,
                    "user": address_info.user_id
                }
                address_biling.append(billing_address)
            address_information = Address.objects.filter(user=user_instance[0], type='Shipping').order_by('id').last()
            if address_information:
                shipping_address = {
                    "id": address_information.id,
                    "address": address_information.address,
                    "city": address_information.city,
                    "state": address_information.state,
                    "pin_code": address_information.pin_code,
                    "type": address_information.type,
                    "user": address_information.user_id
                }
                address_shipping.append(shipping_address)
                data = {
                    'billing_addr': address_biling,
                    'shipping_addr': address_shipping,
                }

        return Response(
            {
                'data': data,
            }
        )


class UserPersonalAddressAdd(generics.ListAPIView):
    """
    This API takes personal information and address information from user and saves it to the user data..
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        data = request.data
        personal = request.data['personal']
        address = request.data['addr']
        name = personal['name']
        birth_date = str(personal['birth_date'])[:10]
        marital_status = personal['marital_status'].upper()
        mobile_no = personal['mobile']
        alt_mobile = personal['alt_mobile']
        gender = personal['gender']
        email_id = personal['email']
        monthly_income = personal['income']
        pan_number = personal['pancard']
        user = User.objects.filter(id=request.user.id)
        user_address = address['address']
        city = address['city']
        state = address['state']
        pin_code = address['postalcode']

        if user:
            add_personal = Personal.objects.create(name=name, birthdate=birth_date, marital_status=marital_status,
                                                   mobile_no=mobile_no, alt_mobile_no=alt_mobile, gender=gender,
                                                   email_id=email_id,
                                                   monthly_income=monthly_income, pan_number=pan_number,
                                                   user=user[0], type="credit")

            add_address = Address.objects.create(address=user_address, city=city, state=state,
                                                 pin_code=pin_code, type="Permanent", user=user[0],
                                                 personal=add_personal)

            if add_personal and add_address:
                return Response(
                    {
                        "status": 1,
                        "message": "data added successfully"
                    }
                )

        return Response(
            {
                "status": 2,
                "message": "Something went wrong"
            }
        )


class ValidateToken(generics.ListAPIView):
    """
    This is used to validate token. It will return user is validate or not.
    """
    authentication_classes = (TokenAuthentication,)

    # permission_classes = (IsAuthenticated,)
    def get(self, request, *args, **kwargs):
        is_profile_filled = False
        if request.user:
            personal_data = Personal.objects.filter(user=request.user.id)
            address_data = Address.objects.filter(user=request.user.id)
            if personal_data and address_data:
                is_profile_filled = True

            return Response(
                {
                    "status": '1',
                    "username": request.user.username,
                    "message": "Valid User",
                    "is_profile_filled": is_profile_filled
                }
            )
        return Response(
            {
                "status": '2',
                "username": None,
                "message": "User is not Valid",
                "is_profile_filled": is_profile_filled
            }
        )


class UserPersonalInfoDetails(generics.ListAPIView):
    """
    This API gives Users Personal and Address information with all details.
    If details not found it will return something went wrong.

    parameter pass:
    {
    "address": "kharadi",
    "city": "Pune",
    "state": "Maharashtra",
    "pincode": "411014",
    "type": "P",
    "name": "Akash",
    "email": "akash@gmail.com",
    "mobile": "9373595121"

    }

    Response :
    {
    "status": 1,
    "message": "Address created",
    "data": {
        "address_id": 852,
        "address": "kharadi",
        "city": "Pune",
        "state": "Maharashtra",
        "pin_code": "411014",
        "type": "Shipping",
        "user": 1358,
        "name": "Akash",
        "email_id": "akash@gmail.com",
        "mobile_no": "9373595121"
    }
    }

    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        user_id = request.user.id
        user = User.objects.filter(id=user_id)

        personal_info = {}
        address_info = {}
        if user:
            personal_obj = Personal.objects.filter(user=user[0]).order_by('id').last()
            if personal_obj:
                personal_info = {
                    'name': personal_obj.name,
                    'birth_date': personal_obj.birthdate,
                    'pancard': personal_obj.pan_number,
                    'gender': personal_obj.gender,
                    'marital_status': (personal_obj.marital_status).title(),
                    'occupation': personal_obj.occupation,
                    'mobile_no': personal_obj.mobile_no,
                    'alt_mobile_no': personal_obj.alt_mobile_no,
                    'monthly_income': personal_obj.monthly_income,
                    'updated_date': personal_obj.updated_date,
                    'invitations': personal_obj.invitations,
                    'email_id': personal_obj.email_id,
                    'industry_type': personal_obj.industry_type,
                    'itr': personal_obj.itr,
                    'mobile_linked_aadhaar': personal_obj.mobile_linked_aadhaar,
                    'residence_type': personal_obj.residence_type,
                    'educational': personal_obj.educational
                }

            address_obj = Address.objects.filter(user=user[0], type="Permanent").order_by('id').last()
            if address_obj:
                address = address_obj.address
                address_info = {
                    'address': address,
                    'city': address_obj.city,
                    'state': address_obj.state,
                    'pin_code': address_obj.pin_code,
                    'type': address_obj.type,
                }

            return Response(
                {
                    "status": 1,
                    "data": {
                        'personal': personal_info,
                        'address': address_info
                    }
                }
            )

        return Response(
            {
                "status": 2,
                "message": "Something went wrong"
            }
        )


class ResetPin(APIView):
    """
    This api is used to reset pin.
      parameter pass:
    {
    "mobile_number": 5362527245,
    "pin": "6453",
    }

    Response :
     {
        'status': 1,
        'message': "User Pin reset successful",
    }

    """
    permission_classes = (partial(GuestTokenPermission, ['GET', 'POST', 'HEAD']),)

    def post(self, request, *args, **kwargs):
        if 'mobile_number' in request.data and 'pin' in request.data:
            mobile_number = request.data.get('mobile_number', )
            pin = request.data.get('pin')
            if mobile_number is not None and mobile_number and pin is not None and pin:
                user_instance = User.objects.filter(username=mobile_number)
                # print(user_instance)
                if user_instance is not None and user_instance:
                    user_pin_instance = AuthenticationPin.objects.filter(user_id=user_instance[0].id).order_by(
                        '-id').first()
                    if user_pin_instance:
                        user_pin_instance = AuthenticationPin.objects.filter(id=user_pin_instance.id).update(pin=pin)
                    else:
                        create_pin = AuthenticationPin.objects.create(user_id=user_instance[0].id, pin=pin)
                        sms_message = "Dear valued customer, your pin is reset successfully"
                        sms_kwrgs = {
                            'sms_message': sms_message,
                            'number': str(mobile_number)
                        }
                        sms(**sms_kwrgs)
                    return Response(
                        {
                            'status': 1,
                            'message': "User Pin reset successful",
                        }
                    )
                else:
                    return Response(
                        {'status': 2,
                         'message': "User not found",
                         }
                    )
            else:
                return Response(
                    {
                        'status': 3,
                        'message': "Mobile number and Pin are mandatory",
                    }
                )
        else:
            return Response(
                {
                    'status': 4,
                    'message': "Mobile number and Pin are mandatory",
                }
            )


def check_personal_info(user_id, type):
    """
    This function is used to check users personal info.
    """
    if type == "cash":
        get_personal_obj = Address.objects.filter(user_id=user_id, personal__type="credit").order_by('-id').first()
        if get_personal_obj:
            return True
        else:
            get_personal_obj = Address.objects.filter(user_id=user_id, personal__type="cash").order_by('-id').first()
            if get_personal_obj:
                return True
            else:
                return False

    elif type == "credit":
        get_personal_obj = Address.objects.filter(user_id=user_id, personal__type="credit").order_by('-id').first()
        if get_personal_obj:
            return True
        else:
            return False
    else:
        return False


class StoreEmail(generics.ListAPIView):
    """
    This Api is to store email in NewsLetter.
    Url: ecomapi/store_email/
    Authentication token required
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..TODO
    method = post
    parameter = {
                    'email_id': 1
                }
    Response:
        {
    "Status": "1",
    "message": "success"
}
    """
    def post(self, request, *args, **kwargs):
        email_id = request.data.get('email_id')
        if email_id and email_id is not None:
            create_obj = NewsLetter.objects.create(email_id=email_id)
            return Response(
                {
                    'Status': "1",
                    "message": "success"
                }
            )
        else:
            return Response(
                {
                    'Status': "2",
                    "message": "fail"
                }
            )


def get_users_personal_name(username):
    """
    The function is used to return users personal name.
    """
    name = None
    if username:
        personal_obj = Personal.objects.filter(user__username=username).first()
        if personal_obj:
            name = personal_obj.name
    return name


# salt_key = "5a776ed7-1ea0-4b02-9837-b18c3d326018"
# key_index = 1
# client_id = "CREDITKARTSWITCH_PP_EXTERNAL"
# phonepe_base_url = ""https://apps-uat.phonepe.com/"

salt_key = "f0d7fc0d-f967-414a-8b85-3c5ea10826a4"
key_index = 1
client_id = "CREDITKARTSWITCH"
phonepe_base_url = "https://apps.phonepe.com/"


class GetAccessTokenAPI(generics.ListAPIView):
    """
    This API hits the API of PhonePe and fetch the users access token..
    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        import requests
        grant_token = None
        payload = None
        token = request.data.get("grant_token")
        if token:
            grant_token = token

        token_dict = None
        if grant_token:
            token_dict = {
                "grantToken": grant_token
            }

        encoded_data = base64.urlsafe_b64encode(json.dumps(token_dict).encode()).decode()

        if encoded_data:
            payload = {
                "request": encoded_data
            }

        sha_data = encoded_data + "/v3/service/auth/access" + salt_key
        sha_encoded_data = sha256(sha_data.encode('utf-8')).hexdigest()
        url = phonepe_base_url + "v3/service/auth/access"
        headers = {
            "Content-Type": "application/json",
            "X-VERIFY": sha_encoded_data + "###" + str(key_index),
            "X-CLIENT-ID": client_id
        }

        response = requests.post(url, headers=headers, data=json.dumps(payload))
        """
        Sample response:
        {
            "success": true,
            "code": "SUCCESS",
            "data": {
                "accessToken": "AUTHca12ab41b97bea5268e65593ab2a1cc697338a656e8",
                "expiresInSeconds": 1800
            }
        }
        """
        access_token = None
        if response:
            response = response.json()
            if "success" in response:
                status = response["success"]
                if status or status == "True":
                    if "data" in response:
                        access_token = response["data"]["accessToken"]
                        if access_token:
                            return Response(
                                {
                                    "status": 1,
                                    "access_token": access_token
                                }
                            )

        return Response(
            {
                "status": 2,
                "access_token": access_token
            }
        )


class GetUserDetailsAPI(generics.ListAPIView):
    """
    This API hits the API of PhonePe and fetch the users access token..
    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        import requests
        auth_token = None
        payload = None
        token = request.data.get("auth_token")
        if token:
            auth_token = token

        sha_data = "/v3/service/userdetails" + salt_key
        sha_encoded_data = sha256(sha_data.encode('utf-8')).hexdigest()

        url = phonepe_base_url + "v3/service/userdetails"
        headers = {
            "Content-Type": "application/json",
            "X-VERIFY": sha_encoded_data + "###" + str(key_index),
            "X-CLIENT-ID": client_id,
            "X-ACCESS-TOKEN": auth_token
        }

        response = requests.get(url, headers=headers)

        """
        Sample response - 
        {
            "success": true,
            "code": "SUCCESS",
            "data": {
                "name": "Anurag",
                "phoneNumber": "9888888888"
            }
        }
        
        In case of email is shared -
        
        {
           "success": true,
           "code": "SUCCESS",
           "data": {
               "name": "Rohit Kumar",
               "phoneNumber": "9888888888",
               "primaryEmail": "test@gmail.com",
               "isEmailVerified": true
           }
        }
        
        """
        name = None
        mobile_no = None
        email_id = None
        if response:
            response = response.json()
            if "success" in response:
                status = response["success"]
                if status or status == "True":
                    if "data" in response:
                        if "name" in response["data"]:
                            name = response["data"]["name"]
                        if "phoneNumber" in response["data"]:
                            mobile_no = response["data"]["phoneNumber"]
                        if "primaryEmail" in response["data"]:
                            email_id = response["data"]["primaryEmail"]

                if mobile_no is not None:
                    # Check if user is already present in database or not..
                    get_user = User.objects.filter(username=mobile_no)
                    if get_user:
                        get_personal = Personal.objects.filter(user=get_user[0]).order_by('id').last()
                        if get_personal:
                            if not name:
                                name = get_personal.name
                            if not email_id:
                                email_id = get_personal.email_id

                        # User is already exist. return users auth token..
                        token, _ = Token.objects.get_or_create(user=get_user[0])
                        user_token = token.key

                        return Response(
                            {
                                "status": 1,
                                "message": "User is already exist",
                                "token": user_token,
                                "name": name,
                                "mobile_no": mobile_no,
                                "email_id": email_id
                            }
                        )

                    else:
                        # User is not present. create user and return auth token..
                        add_user = User.objects.create(username=mobile_no, is_active=True, is_staff=False)
                        if add_user:
                            add_personal = Personal.objects.create(user=add_user, name=name,
                                                                   mobile_no=mobile_no,
                                                                   email_id=email_id, type="cash")

                            token, _ = Token.objects.get_or_create(user=add_user)
                            user_token = token.key

                            return Response(
                                {
                                    "status": 2,
                                    "message": "User created",
                                    "token": user_token,
                                    "name": name,
                                    "mobile_no": mobile_no,
                                    "email_id": email_id
                                }
                            )

                else:
                    return Response(
                        {
                            "status": 3,
                            "message": "Unwanted response",
                            "token": None,
                        }
                    )

        return Response(
            {
                "status": 4,
                "message": "Something went wrong",
                "token": None,
            }
        )
