from datetime import timedelta
from datetime import datetime
import razorpay
import hmac
import hashlib
import random
from django.shortcuts import render, HttpResponse
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from django.apps.registry import apps
from django.contrib.auth.models import User
import json
from django.db.models import Sum
from math import ceil
from MApi.email import failed_transaction_sms

from .EChecksum_web import change_successful_orders_status, get_big_offer_status, get_cashback_amount, \
    user_wallet_update
from django.utils.decorators import method_decorator
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt

from .credit import credit_balance_update, check_order_details_credit_status
from .ecom_email import order_sms
from .wallet import user_wallet_sub, user_wallet_update, user_wallet_sub_new

website_base_url = "https://thecreditkart.com/"
# website_base_url = "https://ecom_test.thecreditkart.com/"
# callback_base_url = "https://test.thecreditkart.com/"
# callback_base_url = "http://15.206.113.44:8015/"
callback_base_url = "https://ecom.thecreditkart.com/"

Payment = apps.get_model('Payments', 'Payment')
OrderDetails = apps.get_model('ecom', 'OrderDetails')
Order = apps.get_model('ecom', 'Order')
Credit = apps.get_model('ecom', 'Credit')
ReturnOrder = apps.get_model('ecom', 'ReturnOrder')
OrderStatus = apps.get_model('ecom', 'OrderStatus')
UserWallet = apps.get_model('ecom', 'UserWallet')
Seller = apps.get_model('ecom', 'Seller')
Personal = apps.get_model('User', 'Personal')

# Test cred
# Key_id = 'rzp_test_p5riAqcJoED1JK'
# Key_secret = 'PQ26fiDi5sk9E1JCwRCI0wF5'


# Prod cred
Key_id = 'rzp_live_Euhr9VT4Z2O1U6'
Key_secret = 'EVNAbAEH9AKNJNVZVSRwz05n'


@method_decorator(csrf_exempt, name='dispatch')
class CallbackOrderRazorpay(APIView):
    """
    This api is used to get response after payment through razorpay. It will redirect
    thecreditkart url.

    Request parameter :
    {
        "razorpay_payment_id": "pay_DESlfW9H8K9uqM",
        "razorpay_order_id": "order_GhGAzafxmaY6J9",
        "razorpay_signature": "9ef4dffbfd84f1318f6739a3ce19f9d85851857ae648f114332d8401e0949a3d"
    }

    The razorpay_order_id is filtered from payment table.
    order_id, razorpay_payment_id, razorpay_order_id and pay_order_id is passed to
    Order_transaction_Status_Check_auto function.

    """

    def post(self, request, *args, **kwargs):
        # redirect_url = 'http://15.207.21.5:3000/account/process_payment?order_id='
        redirect_url = website_base_url + "account/process_payment?order_id="
        data = request.data
        razorpay_payment_id = None
        razorpay_order_id = None
        razorpay_signature = None
        if "razorpay_payment_id" in data:
            razorpay_payment_id = data["razorpay_payment_id"]
        if "razorpay_order_id" in data:
            razorpay_order_id = data["razorpay_order_id"]
        if "razorpay_signature" in data:
            razorpay_signature = data["razorpay_signature"]
        if None not in (razorpay_payment_id, razorpay_order_id, razorpay_signature):
            payment = Payment.objects.filter(pg_transaction_id=razorpay_order_id)
            if payment:
                order_id_s = str(payment[0].order_id).split("e")
                order_id = order_id_s[0]
                data = {
                    "order_id": order_id,
                    "razorpay_payment_id": razorpay_payment_id,
                    "razorpay_order_id": razorpay_order_id,
                    "razorpay_signature": razorpay_signature,
                    "pay_order_id": payment[0].order_id
                }
                if order_id is not None:
                    Order_transaction_Status_Check_auto(data)
                    redirect_with_oid = redirect_url + str(payment[0].order_id)
                    return redirect(redirect_with_oid)
        return redirect(website_base_url + "account/payment")


class RazorpayEcomOrderWebsite(APIView):
    """
        Paytm Order Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None
        ctx = {}
        try:
            shopping_order_id = str(request.data.get('shopping_order_id'))
            wallet_condition = request.data.get('wallet_condition')
            random_number = random.sample(range(99999), 1)
            ORDER_ID = str(shopping_order_id) + 'e' + str(random_number[0])

            if wallet_condition == 1:
                url = "https://ecom.thecreditkart.com/ecomapi/callback_order_razorpay/"
                TXN_AMOUNT = str(request.data.get('TXN_AMOUNT'))
                amount = float(TXN_AMOUNT) * 100
                data = {'amount': amount, 'currency': 'INR', 'receipt': ORDER_ID, 'payment_capture': '1',
                        "offers": ["offer_HgHszfBUCWmqDN"]}

                client = razorpay.Client(auth=(Key_id, Key_secret))
                order = client.order.create(data)
                personal = Personal.objects.filter(user_id=request.user.id).order_by('-id').first()
                name = None
                dec = None
                email_id = None
                mobile_no = None
                resp_data = None
                real_amount = order['amount'] / 100
                if personal:
                    name = personal.name
                    dec = personal.name
                    email_id = personal.email_id
                    mobile_no = personal.user.username
                if order:
                    ctx['order'] = order
                    resp_data = {
                        'order_id': order['id'],
                        'amount': str(real_amount),
                        'show_amount': real_amount,
                        'status': order['status'],
                        'created_at': order['created_at'],
                        'currency': order['currency'],
                        'image_url': '',
                        'key_id': Key_id,
                        'name': name,
                        'dec': dec,
                        'callbackurl': url,
                        'order_id_ck': ORDER_ID,
                        'email_id': email_id,
                        'mobile_no': mobile_no,

                    }

                    timestamp = order['created_at']
                    try:
                        timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
                    except Exception as e:
                        print(e)
                        pass

                    paym = Payment.objects.create(order_id=ORDER_ID, pg_transaction_id=resp_data['order_id'], \
                                                  status="initiated", amount=real_amount, \
                                                  category="Order", product_type="Ecom", date=timestamp,
                                                  return_status="rstatus", platform_type="Web",
                                                  pay_source="Rpay")
                    if paym and paym is not None:
                        order_details = OrderDetails.objects.filter(order_text=shopping_order_id)
                        if order_details:
                            order_details[0].payment.add(paym)

                    status = '1'
                    message = 'Success'
                else:
                    message = "Something Went Wrong"
                    status = 0
                return Response(
                    {
                        'status': status,
                        'message': message,
                        'data': resp_data
                    }
                )

            if wallet_condition == 0:
                # This means no extra amount paid by customer..
                # Only updating all initiated entries to plaed for Order..
                order_details_obj = OrderDetails.objects.filter(order_text=shopping_order_id)
                if order_details_obj:
                    get_payment_obj = order_details_obj[0].payment.filter(product_type="Ecom",
                                                                          category='Order', type="Ecom_Wallet",
                                                                          mode="PPI", status="initiated")

                    if get_payment_obj:
                        get_payment_obj[0].status = "success"
                        get_payment_obj[0].update_date = datetime.now()
                        get_payment_obj[0].save()

                    # Updating Wallet object to success..
                    get_wallet_obj = UserWallet.objects.filter(order_details=order_details_obj[0], status="initiated")
                    if get_wallet_obj:
                        if get_wallet_obj:
                            get_wallet_obj[0].status = "success"
                            get_wallet_obj[0].updated_date = datetime.now()
                            get_wallet_obj[0].save()

                        # Add cashback amount in user wallet..
                        payment_mode = order_details_obj[0].payment_mode
                        if payment_mode == "cash":
                            discount = 0
                            order_date = str(order_details_obj[0].created_date)[:10]
                            is_big_offer = get_big_offer_status(order_date)
                            if is_big_offer:
                                discount = 0
                            else:
                                cashback_amount = order_details_obj[0].total_amount
                                # This function is for 15% cashback..
                                discount = get_cashback_amount(cashback_amount)

                            if discount > 0:
                                # Adding cashback amount for the order in user wallet..
                                status = "cashback_amount"
                                order_obj = None
                                add_wallet = user_wallet_update(order_details_obj[0].user.id, discount,
                                                                order_details_obj[0], order_obj,
                                                                status)

                    #  Updating credit entry to success
                    if order_details_obj[0].credit:
                        get_credit_obj = Credit.objects.filter(id=order_details_obj[0].credit.id,
                                                               status="initiated").order_by('id').last()
                        if get_credit_obj:
                            get_credit_obj.status = "success"
                            get_credit_obj.updated_date = datetime.now()
                            get_credit_obj.save()

                    # updating status of order to placed..
                    try:
                        update_status = change_successful_orders_status(order_details_obj[0])
                    except Exception as e:
                        print(
                            "Inside Exception of change order status ChecksumGenerateEcomPaytmOrderWebsite", e)
                    status = 2
                    message = 'Transaction successful.'

            if wallet_condition == 2:
                # This is the condition of cod..
                # updating status of order to placed..
                order_details_obj = OrderDetails.objects.filter(order_text=shopping_order_id)
                if order_details_obj:
                    try:
                        update_status = change_successful_orders_status(order_details_obj[0])
                    except Exception as e:
                        print(
                            "Inside Exception of change order status ChecksumGenerateEcomPaytmOrder", e)

                    status = 2
                    message = 'Transaction successful.'

            return Response(
                {
                    'status': status,
                    'message': message
                }
            )

        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


import requests


class RazorpayEcomOrderWebsitePhonePe(APIView):
    """
        Paytm Order Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None
        ctx = {}
        try:
            shopping_order_id = str(request.data.get('shopping_order_id'))
            wallet_condition = request.data.get('wallet_condition')
            random_number = random.sample(range(99999), 1)
            ORDER_ID = str(shopping_order_id) + 'e' + str(random_number[0])

            if wallet_condition == 1:
                callback_url = callback_base_url + "ecomapi/callback_order_razorpay/"
                TXN_AMOUNT = str(request.data.get('TXN_AMOUNT'))
                amount = float(TXN_AMOUNT) * 100
                data = None

                tracking_url = website_base_url + "ordertracking/phonepe_order_track?order_id=" + str(
                    shopping_order_id)
                content = {"orderContext": {
                    "trackingInfo": {"type": "HTTPS", "url": tracking_url}}}
                content_data = json.dumps(content)

                data = {'amount': amount, 'currency': 'INR', 'receipt': ORDER_ID, 'payment_capture': '1',
                        "phonepe_switch_context": content_data}
                data = json.dumps(data)

                headers = {
                    'Content-Type': 'application/json',
                }
                url = "https://api.razorpay.com/v1/orders"
                response = requests.request(method="POST", url=url, headers=headers, auth=(Key_id, Key_secret),
                                            data=data)
                order = response.json()
                personal = Personal.objects.filter(user_id=request.user.id).order_by('-id').first()
                name = None
                dec = None
                email_id = None
                mobile_no = None
                resp_data = None
                real_amount = order['amount'] / 100
                if personal:
                    name = personal.name
                    dec = personal.name
                    email_id = personal.email_id
                    mobile_no = personal.user.username
                if order:
                    ctx['order'] = order
                    resp_data = {
                        'order_id': order['id'],
                        'amount': amount,
                        'show_amount': real_amount,
                        'status': order['status'],
                        'created_at': order['created_at'],
                        'currency': order['currency'],
                        'image_url': '',
                        'key_id': Key_id,
                        'name': name,
                        'dec': dec,
                        'callbackurl': callback_url,
                        'order_id_ck': ORDER_ID,
                        'email_id': email_id,
                        'mobile_no': mobile_no,

                    }
                    timestamp = order['created_at']
                    try:
                        timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
                    except Exception as e:
                        print(e)
                        pass

                    paym = Payment.objects.create(order_id=ORDER_ID, pg_transaction_id=resp_data['order_id'], \
                                                  status="initiated", amount=real_amount, \
                                                  category="Order", product_type="Ecom", date=timestamp,
                                                  return_status="rstatus", platform_type="Web",
                                                  pay_source="Rpay")
                    if paym and paym is not None:
                        order_details = OrderDetails.objects.filter(order_text=shopping_order_id)
                        if order_details:
                            order_details[0].payment.add(paym)

                    status = '1'
                    message = 'Success'
                else:
                    message = "Something Went Wrong"
                    status = 0
                return Response(
                    {
                        'status': status,
                        'message': message,
                        'data': resp_data
                    }
                )

            if wallet_condition == 0:
                # This means no extra amount paid by customer..
                # Only updating all initiated entries to plaed for Order..
                order_details_obj = OrderDetails.objects.filter(order_text=shopping_order_id)
                if order_details_obj:
                    get_payment_obj = order_details_obj[0].payment.filter(product_type="Ecom",
                                                                          category='Order', type="Ecom_Wallet",
                                                                          mode="PPI", status="initiated")

                    if get_payment_obj:
                        if get_payment_obj:
                            get_payment_obj[0].status = "success"
                            get_payment_obj[0].update_date = datetime.now()
                            get_payment_obj[0].save()

                    # Updating Wallet object to success..
                    get_wallet_obj = UserWallet.objects.filter(order_details=order_details_obj[0], status="initiated")
                    if get_wallet_obj:

                        if get_wallet_obj:
                            get_wallet_obj[0].status = "success"
                            get_wallet_obj[0].updated_date = datetime.now()
                            get_wallet_obj[0].save()

                        # Add cashback amount in user wallet..
                        payment_mode = order_details_obj[0].payment_mode
                        if payment_mode == "cash":
                            discount = 0
                            order_date = str(order_details_obj[0].created_date)[:10]
                            is_big_offer = get_big_offer_status(order_date)
                            if is_big_offer:
                                discount = 0
                            else:
                                cashback_amount = order_details_obj[0].total_amount
                                # This function is for 15% cashback..
                                discount = get_cashback_amount(cashback_amount)

                            if discount > 0:
                                # Adding cashback amount for the order in user wallet..
                                status = "cashback_amount"
                                order_obj = None
                                add_wallet = user_wallet_update(order_details_obj[0].user.id, discount,
                                                                order_details_obj[0], order_obj,
                                                                status)

                    #  Updating credit entry to success
                    if order_details_obj[0].credit:
                        get_credit_obj = Credit.objects.filter(id=order_details_obj[0].credit.id,
                                                               status="initiated").order_by('id').last()
                        if get_credit_obj:
                            get_credit_obj.status = "success"
                            get_credit_obj.updated_date = datetime.now()
                            get_credit_obj.save()

                    # updating status of order to placed..
                    try:
                        update_status = change_successful_orders_status(order_details_obj[0])
                    except Exception as e:
                        print(
                            "Inside Exception of change order status ChecksumGenerateEcomPaytmOrderWebsite", e)
                    status = 2
                    message = 'Transaction successful.'

            if wallet_condition == 2:
                # This is the condition of cod..
                # updating status of order to placed..
                order_details_obj = OrderDetails.objects.filter(order_text=shopping_order_id)
                if order_details_obj:
                    try:
                        update_status = change_successful_orders_status(order_details_obj[0])
                    except Exception as e:
                        print(
                            "Inside Exception of change order status ChecksumGenerateEcomPaytmOrder", e)

                    status = 2
                    message = 'Transaction successful.'

            return Response(
                {
                    'status': status,
                    'message': message
                }
            )

        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


def add_fk_in_order(payment, shopping_order_id):
    """
    This function is used to attach payment to order_details.
    """
    order_details = OrderDetails.objects.filter(order_text=shopping_order_id)
    if order_details:
        order_details[0].payment.add(payment)
        return True
    return False


def repayment_from_wallet(order_id, shopping_order_id, amount, user_id, platform=False):
    """
    This function is used dedicated amount from wallet. If amount is greater than last wallet balance
    amount then remaining amount will return function as a payable amount. The function will create entry
    in payment and payment type will be 'Ecom_wallet', this payment entry will attached to order details payment.
    """
    from .credit import get_user_wallet_amount
    status = "repayment"
    wallet_amount = 0
    payable_amount = 0
    used_wallet = 0
    amount = float(amount)
    if not platform:
        wallet_dict = get_user_wallet_amount(user_id)
        if wallet_dict:
            wallet_amount = wallet_dict['balance_amount']

    if wallet_amount != 0:
        if wallet_amount >= amount:
            used_wallet = amount
            payable_amount = 0
        else:
            used_wallet = wallet_amount
            payable_amount = amount - wallet_amount

    else:
        payable_amount = amount

    order_details = OrderDetails.objects.filter(order_text=shopping_order_id).first()
    order_obj = Order.objects.filter(order_id=shopping_order_id).first()

    if used_wallet != 0:
        import time
        sleep_time = random.random()
        time.sleep(sleep_time)
        sleep_time = random.random()
        time.sleep(sleep_time)
        # Creating new wallet entry
        used_wallet = user_wallet_sub_new(user_id, used_wallet, order_details, order_obj, status)
        # if difference is <=0 then amount deducted from wallet will be "amount"
        if used_wallet > 0:
            payment = Payment.objects.create(status='success', order_id=str(order_id), amount=float(used_wallet),
                                             category='Repayment', type="Ecom_Wallet", mode="PPI",
                                             product_type="Ecom", date=datetime.now())

            add_fk_in_order(payment, shopping_order_id)

            # Adding credit to users credit..
            status = "repayment"

            get_credit_repayment = order_details.credit_history.filter(
                status=status, repayment_add=used_wallet,
                created_date__startswith=str(datetime.now())[:16])
            if get_credit_repayment:
                print("get_credit_repayment qs", get_credit_repayment, ", id: ",
                      get_credit_repayment[0].id)

            else:
                # No credit entry, so credit not updated
                # Updating User credit balance..
                add_credit = credit_balance_update(user_id, used_wallet, status)
                if add_credit:
                    # Adding new credit entry in Credit history of Order Details Table..
                    order_details.credit_history.add(add_credit)

            order_credit_status = check_order_details_credit_status(order_details.id)

        return payable_amount
    else:
        return payable_amount


class RazorpayEcomRepaymentWebsite(APIView):
    """
        Paytm Repayment Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"

        Request Parameter :
        {
        "shopping_order_id": "order_GhGAzafxmaY6J9",
        "TXN_AMOUNT": "100",
        "use_wallet": "True"
        }

        If use_wallet then used repayment_from_wallet function it will return payable amount.
        The payable amount is have to pay user. Then razorpay order will create.

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None
        ctx = {}

        try:
            shopping_order_id = str(request.data.get('shopping_order_id'))
            TXN_AMOUNT = request.data.get('TXN_AMOUNT')
            random_number = random.sample(range(99999), 1)
            # Considering shopping order_id, CUST_ID and TXN_AMOUNT is getting.
            ORDER_ID = str(shopping_order_id) + 'e' + str(random_number[0])
            CUST_ID = ""
            user_id = request.user.id
            if user_id:
                user = User.objects.filter(id=user_id)
                if user:
                    CUST_ID = user[0].username

                # Repayment from wallet
                # use_wallet: boolean
                use_wallet = True
                if "use_wallet" in request.data:
                    use_wallet = request.data.get('use_wallet')

                if use_wallet:
                    payable_amount = repayment_from_wallet(ORDER_ID, shopping_order_id, TXN_AMOUNT, user_id)
                    if payable_amount == 0:

                        number = user[0].username
                        osms_kwrgs = {
                            'sms_type': "repayment",
                            'number': str(number)
                        }
                        order_sms(**osms_kwrgs)

                        message = "Payment Successful from wallet"
                        status = 6
                        return Response(
                            {
                                'status': status,
                                'message': message
                            }
                        )
                    else:
                        TXN_AMOUNT = payable_amount

            url = callback_base_url + "ecomapi/callback_repayment_razorpay/"
            amount = float(TXN_AMOUNT) * 100
            random_number = random.sample(range(99999), 1)
            ORDER_ID = str(shopping_order_id) + 'e' + str(random_number[0])
            data = {'amount': amount, 'currency': 'INR', 'receipt': ORDER_ID, 'payment_capture': '1'}
            client = razorpay.Client(auth=(Key_id, Key_secret))
            order = client.order.create(data)
            personal = Personal.objects.filter(user_id=request.user.id).order_by('-id').first()
            name = None
            dec = None
            email_id = None
            mobile_no = None
            real_amount = order['amount'] / 100
            if personal:
                name = personal.name
                dec = personal.name
                email_id = personal.email_id
                mobile_no = personal.user.username
            if order:
                ctx['order'] = order
                resp_data = {
                    'order_id': order['id'],
                    'amount': str(real_amount),
                    'show_amount': real_amount,
                    'status': order['status'],
                    'created_at': order['created_at'],
                    'currency': order['currency'],
                    'image_url': '',
                    'key_id': Key_id,
                    'name': name,
                    'dec': dec,
                    'callbackurl': url,
                    'order_id_ck': ORDER_ID,
                    'email_id': email_id,
                    'mobile_no': mobile_no,

                }

                timestamp = order['created_at']
                try:
                    timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
                except Exception as e:
                    print(e)
                    pass

                paym = Payment.objects.create(order_id=ORDER_ID, pg_transaction_id=resp_data['order_id'], \
                                              status="initiated", amount=real_amount, \
                                              category="Repayment", product_type="Ecom", date=timestamp,
                                              return_status="rstatus", platform_type="Web",
                                              pay_source="Rpay")

                if paym and paym is not None:
                    order_details = OrderDetails.objects.filter(order_text=shopping_order_id)
                    if order_details:
                        order_details[0].payment.add(paym)

                message = "Successfully Done"
                status = 1
            else:
                message = "Something Went Wrong"
                status = 0

            return Response(
                {
                    'status': status,
                    'message': message,
                    'data': resp_data
                }
            )

        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


class RazorpayEcomRepaymentWebsitePhonePe(APIView):
    """
        Paytm Repayment Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None
        ctx = {}

        try:
            shopping_order_id = str(request.data.get('shopping_order_id'))
            TXN_AMOUNT = request.data.get('TXN_AMOUNT')
            random_number = random.sample(range(99999), 1)
            platform = True
            # Considering shopping order_id, CUST_ID and TXN_AMOUNT is getting.
            ORDER_ID = str(shopping_order_id) + 'e' + str(random_number[0])
            CUST_ID = ""
            user_id = request.user.id
            if user_id:
                user = User.objects.filter(id=user_id)
                if user:
                    CUST_ID = user[0].username

                # Repayment from wallet
                # use_wallet: boolean
                use_wallet = True
                if "use_wallet" in request.data:
                    use_wallet = request.data.get('use_wallet')

                if use_wallet:
                    payable_amount = repayment_from_wallet(ORDER_ID, shopping_order_id, TXN_AMOUNT, user_id, platform)
                    if payable_amount == 0:

                        number = user[0].username
                        osms_kwrgs = {
                            'sms_type': "repayment",
                            'number': str(number)
                        }
                        order_sms(**osms_kwrgs)

                        message = "Payment Successful from wallet"
                        status = 6
                        return Response(
                            {
                                'status': status,
                                'message': message
                            }
                        )
                    else:
                        TXN_AMOUNT = payable_amount

            callback_url = callback_base_url + "ecomapi/callback_repayment_razorpay/"
            amount = float(TXN_AMOUNT) * 100
            random_number = random.sample(range(99999), 1)
            ORDER_ID = str(shopping_order_id) + 'e' + str(random_number[0])

            tracking_url = website_base_url + "ordertracking/phonepe_order_track?order_id=" + str(
                shopping_order_id)
            content = {"orderContext": {
                "trackingInfo": {"type": "HTTPS", "url": tracking_url}}}
            content_data = json.dumps(content)

            data = {'amount': amount, 'currency': 'INR', 'receipt': ORDER_ID, 'payment_capture': '1',
                    "phonepe_switch_context": content_data}
            data = json.dumps(data)

            headers = {
                'Content-Type': 'application/json',
            }
            url = "https://api.razorpay.com/v1/orders"
            response = requests.request(method="POST", url=url, headers=headers, auth=(Key_id, Key_secret),
                                        data=data)
            order = response.json()

            personal = Personal.objects.filter(user_id=request.user.id).order_by('-id').first()
            name = None
            dec = None
            email_id = None
            mobile_no = None
            resp_data = None
            real_amount = order['amount'] / 100
            if personal:
                name = personal.name
                dec = personal.name
                email_id = personal.email_id
                mobile_no = personal.user.username
            if order:
                ctx['order'] = order
                resp_data = {
                    'order_id': order['id'],
                    'amount': amount,
                    'show_amount': real_amount,
                    'status': order['status'],
                    'created_at': order['created_at'],
                    'currency': order['currency'],
                    'image_url': '',
                    'key_id': Key_id,
                    'name': name,
                    'dec': dec,
                    'callbackurl': callback_url,
                    'order_id_ck': ORDER_ID,
                    'email_id': email_id,
                    'mobile_no': mobile_no,
                }

                timestamp = order['created_at']
                try:
                    timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
                except Exception as e:
                    print(e)
                    pass

                paym = Payment.objects.create(order_id=ORDER_ID, pg_transaction_id=resp_data['order_id'], \
                                              status="initiated", amount=real_amount, \
                                              category="Repayment", product_type="Ecom", date=timestamp,
                                              return_status="rstatus", platform_type="Web",
                                              pay_source="Rpay")

                if paym and paym is not None:
                    order_details = OrderDetails.objects.filter(order_text=shopping_order_id)
                    if order_details:
                        order_details[0].payment.add(paym)

                message = "Successfully Done"
                status = 1
            else:
                message = "Something Went Wrong"
                status = 0

            return Response(
                {
                    'status': status,
                    'message': message,
                    'data': resp_data
                }
            )

        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


def update_payment_object_from_razorpay(payment_obj, status, data_dict):
    """
    This function is to update payment object with new data dict parameters..
    """
    if None not in (payment_obj, status, data_dict):
        mode = None
        transaction_id = None
        return_status = "rs2s"
        pay_source = "Rpay"
        type = "App"
        update_date = datetime.now()

        if "mode" in data_dict:
            mode = data_dict["mode"]
        if "transaction_id" in data_dict:
            transaction_id = data_dict["transaction_id"]

        try:
            if status == 'failed':
                mobile_number = None
                order_payment = payment_obj.orderdetails_payment.all()
                if order_payment:
                    if order_payment[0].user:
                        mobile_number = order_payment[0].user.username

                if mobile_number:
                    failed_transaction_sms(mobile_number, payment_obj.amount)
        except Exception as e:
            print('-----------in exception----------', e.args)

        payment_obj.status = status
        payment_obj.mode = mode
        payment_obj.transaction_id = transaction_id
        payment_obj.return_status = return_status
        payment_obj.pay_source = pay_source
        payment_obj.type = type
        payment_obj.update_date = update_date
        payment_obj.save()

        return True


def razorpay_payment_process(data_dict):
    """
    This method will check for razorpay web hook status and id's and according to that process payment
    parameters :
            data_dict = {
                            "pg_transaction_id": " R'pay payment id",
                            "transaction_id": " R'pay order id ",
                            "order_id": " R'pay receipt id ",
                            "status": " status of transc.",
                            "timestamp" : " time of trans",
                            "type" : ["app","web","hook"]

                        }
    """
    order_id = None
    order_text = None
    failed_payment_obj = None
    wallet_type = None

    if data_dict and data_dict is not None:
        update_payment_obj = None
        if "wallet_type" in data_dict:
            wallet_type = data_dict["wallet_type"]

        if "status" in data_dict:
            status = data_dict["status"]
            if data_dict["type"] == "hook":
                if status and status is not None:
                    if "pg_transaction_id" in data_dict and data_dict["pg_transaction_id"] is not None:
                        if data_dict["transaction_id"] is not None and data_dict["order_id"] is not None:
                            # check payment with pg_transaction_id , transaction_id and order_id
                            payment_obj = Payment.objects.filter(pg_transaction_id=data_dict["pg_transaction_id"],
                                                                 order_id=data_dict["order_id"],
                                                                 status__in=["initiated", "pending"])
                            if payment_obj:
                                # update previous payment entry
                                update_payment_obj = update_payment_object_from_razorpay(payment_obj[0], status,
                                                                                         data_dict)

                            else:
                                # check for failed payment entry
                                failed_payment_obj = Payment.objects.filter(
                                    pg_transaction_id=data_dict["pg_transaction_id"],
                                    order_id=data_dict["order_id"],
                                    status__in=["failed", "pending"])
                        elif data_dict["transaction_id"] is not None:
                            # check payment with pg_transaction_id and transaction_id
                            payment_obj = Payment.objects.filter(pg_transaction_id=data_dict["pg_transaction_id"],
                                                                 status__in=["initiated", "pending"])
                            if payment_obj:
                                # update previous payment entry

                                update_payment_obj = update_payment_object_from_razorpay(payment_obj[0], status,
                                                                                         data_dict)


                            else:
                                # check for failed payment entry
                                failed_payment_obj = Payment.objects.filter(
                                    pg_transaction_id=data_dict["pg_transaction_id"],
                                    transaction_id=data_dict["transaction_id"],
                                    status__in=["failed", "pending"])

                        elif data_dict["order_id"] is not None:
                            # check payment with pg_transaction_id  and order_id
                            payment_obj = Payment.objects.filter(pg_transaction_id=data_dict["pg_transaction_id"],
                                                                 order_id=data_dict["order_id"],
                                                                 status__in=["initiated", "pending"])
                            if payment_obj:
                                # update previous payment entry
                                update_payment_obj = update_payment_object_from_razorpay(payment_obj[0], status,
                                                                                         data_dict)

                            else:
                                # check for failed payment entry
                                failed_payment_obj = Payment.objects.filter(
                                    pg_transaction_id=data_dict["pg_transaction_id"],
                                    order_id=data_dict["order_id"],
                                    status__in=["failed", "pending"])
                        else:
                            # check payment with pg_transaction_id only
                            payment_obj = Payment.objects.filter(pg_transaction_id=data_dict["pg_transaction_id"],
                                                                 status__in=["initiated", "pending"])
                            if payment_obj:
                                # update previous payment entry
                                update_payment_obj = update_payment_object_from_razorpay(payment_obj[0], status,
                                                                                         data_dict)

                            else:
                                # check for failed payment entry
                                failed_payment_obj = Payment.objects.filter(
                                    pg_transaction_id=data_dict["pg_transaction_id"],
                                    status__in=["failed", "pending"])

            else:
                payment_obj = Payment.objects.filter(pg_transaction_id=data_dict["pg_transaction_id"],
                                                     order_id=data_dict["order_id"],
                                                     status__in=["initiated", "pending"])

                if payment_obj:
                    # update previous payment entry
                    update_payment_obj = update_payment_object_from_razorpay(payment_obj[0], status,
                                                                             data_dict)


                else:
                    # check for failed payment entry
                    failed_payment_obj = Payment.objects.filter(
                        pg_transaction_id=data_dict["pg_transaction_id"],
                        order_id=data_dict["order_id"],
                        status__in=["failed", "pending"])

            if update_payment_obj and update_payment_obj is not None:
                """
                payment status updated only if it's last status is 'initiated'

                """
                update_payment_obj = Payment.objects.filter(pg_transaction_id=data_dict["pg_transaction_id"],
                                                            order_id=data_dict["order_id"],
                                                            status__in=["success", "failed"]).last()
                if update_payment_obj:
                    order_id = update_payment_obj.order_id
                    order_id = order_id.split("e")
                    if order_id:
                        order_text = order_id[0]
                    order_details_obj = OrderDetails.objects.filter(order_text=order_text)
                    if order_details_obj:
                        payment_category = update_payment_obj.category
                        order_id = update_payment_obj.order_id
                        order_id_list = order_id.split('e')
                        order_text = order_id_list[0]
                        amount = update_payment_obj.amount
                        user_id = None
                        if payment_category == "Order":
                            if status == 'success':
                                check = success_razorpay_transaction(order_details_obj, "success", wallet_type)
                            elif status == "failed":
                                check = failed_razorpay_transaction(order_details_obj, "failed")
                        if payment_category == "Repayment":
                            razorpay_transaction_for_repayment(order_details_obj, status, amount)

            elif failed_payment_obj and failed_payment_obj is not None:
                """
                if last status of payment is not 'initiated' then create new entry for every status

                """
                order_id = failed_payment_obj[0].order_id
                order_id = order_id.split("e")
                payment_category = failed_payment_obj[0].category
                amount = failed_payment_obj[0].amount
                if order_id:
                    order_text = order_id[0]
                order_details_obj = OrderDetails.objects.filter(order_text=order_text)
                if order_details_obj:
                    new_payment_obj = Payment.objects.create(order_id=failed_payment_obj[0].order_id,
                                                             category=failed_payment_obj[0].category,
                                                             status=status, type="App",
                                                             pg_transaction_id=data_dict["pg_transaction_id"],
                                                             transaction_id=data_dict["transaction_id"],
                                                             mode=data_dict["mode"], amount=data_dict["amount"],
                                                             product_type="Ecom",
                                                             date=data_dict["timestamp"], pay_source="Rpay")
                    if new_payment_obj:
                        order_details_obj[0].payment.add(new_payment_obj)
                        if payment_category == "Order":
                            if status == 'success':
                                check = success_razorpay_transaction(order_details_obj, "success", wallet_type)
                            elif status == "failed":
                                check = failed_razorpay_transaction(order_details_obj, "failed")
                        if payment_category == "Repayment":
                            check = razorpay_transaction_for_repayment(order_details_obj, status, amount)


def Repayment_transaction_Status_Check_auto(data):
    """
    This function is used for repayment.
     Request parameter :
    {
        "razorpay_payment_id": "pay_DESlfW9H8K9uqM",
        "razorpay_order_id": "order_GhGAzafxmaY6J9",
        "razorpay_signature": "9ef4dffbfd84f1318f6739a3ce19f9d85851857ae648f114332d8401e0949a3d",
        "repay_amount: "100"
    }

    The razorpay_order_id is passed to order_status function to check order status and razorpay_payment_id
    is passed to payment_status to check payment is recieved or not. According to payment status the
    payment entry will update.
    If payment successfully, then create entry in recovery incentive using create_recovery_incentive_entry
    function.

    """
    status = None
    message = None
    amount = 0.0
    date_strip = None
    pay_status = None
    mode = None
    razorpay_payment_id = data['razorpay_payment_id']

    razorpay_order_id = data['razorpay_order_id']
    razorpay_signature = data['razorpay_signature']
    amount = data['repay_amount']
    client = razorpay.Client(auth=(Key_id, Key_secret))
    msg = "{}|{}".format(str(data['razorpay_order_id']), str(data['razorpay_payment_id']))

    secret = str(Key_secret)

    key = bytes(secret, 'utf-8')
    body = bytes(msg, 'utf-8')

    dig = hmac.new(key=key,
                   msg=body,
                   digestmod=hashlib.sha256)

    generated_signature = dig.hexdigest()
    result = hmac.compare_digest(generated_signature, razorpay_signature)
    order_text = data["order_id"]
    if result == True:
        pass

    order_resp_data = order_status(razorpay_order_id)
    if order_resp_data and order_resp_data is not None:
        status = 2
        message = " Sign Verification Failed"

    payment_data = payment_status(razorpay_payment_id)
    if payment_data and payment_data is not None:
        if payment_data['method'] == 'card':
            mode = 'DC'
        elif payment_data['method'] == 'upi':
            mode = 'UPI'
        elif payment_data['method'] == 'netbanking':
            mode = 'NB'
        else:
            mode = 'RWT'
        timestamp = payment_data['created_at']
        try:
            timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')

        except Exception as e:
            print(e)
            pass

        if payment_data['status'] == 'captured':
            pay_status = 'success'
            status = '1'
            message = 'Transaction successful.'

        elif payment_data['status'] == 'authorized':
            pay_status = 'pending'

            status = '3'
            message = 'Transaction pending'

        else:
            pay_status = 'failed'
            status = '2'
            message = 'Transaction failed'

    payment_obj = Payment.objects.filter(order_id=data["pay_order_id"],
                                         category='Repayment', status__in=["initiated", "pending"])

    if payment_obj:
        payment_obj[0].status = pay_status
        payment_obj[0].transaction_id = data["razorpay_payment_id"]
        payment_obj[0].mode = mode
        payment_obj[0].type = "App"
        payment_obj[0].update_date = datetime.now()
        payment_obj[0].save()

    try:
        if pay_status == 'failed':
            mobile_number = None
            order_payment = payment_obj[0].orderdetails_payment.all()
            if order_payment:
                if order_payment[0].user:
                    mobile_number = order_payment[0].user.username

            if mobile_number:
                failed_transaction_sms(mobile_number, payment_obj[0].amount)
    except Exception as e:
        print('-----------in exception----------')
        print(e.args)

    order_details_obj = OrderDetails.objects.filter(order_text=order_text)
    if order_details_obj:
        user_id = order_details_obj[0].user.id
        if pay_status == 'success':
            order_credit_status = check_order_details_credit_status(order_details_obj[0].id)
            # Validating
            if order_credit_status:
                update_status = OrderDetails.objects.filter(id=order_details_obj[0].id)
                if update_status:
                    update_status[0].credit_status = order_credit_status
                    update_status[0].updated_date = datetime.now()
                    update_status[0].save()

            # check if credit exist
            credit_status = "repayment"
            # print("In credit repayment")
            import time
            sleep_time = random.random()
            time.sleep(sleep_time)
            sleep_time = random.random()
            time.sleep(sleep_time)
            get_credit_repayment = order_details_obj[0].credit_history.filter(
                status=credit_status, repayment_add=amount,
                created_date__startswith=str(datetime.now())[:16])
            try:
                from Recovery.views import create_recovery_incentive_entry

                create_recovery_incentive_entry(order_details_obj[0].id)
            except:
                pass

            if get_credit_repayment:
                pass

            else:
                # No credit entry, so credit not updated
                # Updating User credit balance..
                update_credit = credit_balance_update(user_id, amount, credit_status)

                # from .credit import limit_repayment_credit
                if update_credit:
                    # Adding new credit entry in Credit history of Order Details Table..
                    order_details_obj[0].credit_history.add(update_credit)

            try:
                number = order_details_obj[0].user.username
                osms_kwrgs = {
                    'sms_type': "repayment",
                    'number': str(number)
                }
                order_sms(**osms_kwrgs)
            except Exception as e:
                print("error in repayment")

            status = '1'
            message = 'Transaction successful.'

        elif pay_status == 'failed':

            status = '2'
            message = 'Transaction failed'

        elif pay_status == 'pending':

            status = '3'
            message = 'Transaction pending'

        return Response(
            {
                'status': status,
                'message': message
            }
        )


@method_decorator(csrf_exempt, name='dispatch')
class CallbackRepaymentRazorpay(APIView):
    """
    This api is used for repayment using razorpay. If user repayment the amount then the razorpay return 3 parameters.
     It will redirect thecreditkart url.
    Request Parameter :
    {
        "razorpay_payment_id": "pay_DESlfW9H8K9uqM",
        "razorpay_order_id": "order_GhGAzafxmaY6J9",
        "razorpay_signature": "9ef4dffbfd84f1318f6739a3ce19f9d85851857ae648f114332d8401e0949a3d"
    }

    The razorpay_order_id is filtered from payment table.
    order_id, razorpay_payment_id, razorpay_order_id and pay_order_id is passed to
    Repayment_transaction_Status_Check_auto function.
    """

    def post(self, request, *args, **kwargs):
        # def callback_order_razorpay(request):
        # redirect_url = 'http://15.207.21.5:3000/account/process_payment?order_id='
        redirect_url = website_base_url + "account/process_payment?order_id="
        data = request.data
        razorpay_payment_id = None
        razorpay_order_id = None
        razorpay_signature = None
        if "razorpay_payment_id" in data:
            razorpay_payment_id = data["razorpay_payment_id"]
        if "razorpay_order_id" in data:
            razorpay_order_id = data["razorpay_order_id"]
        if "razorpay_signature" in data:
            razorpay_signature = data["razorpay_signature"]
        if None not in (razorpay_payment_id, razorpay_order_id, razorpay_signature):
            payment = Payment.objects.filter(pg_transaction_id=razorpay_order_id)
            if payment:
                order_id_s = str(payment[0].order_id).split("e")
                order_id = order_id_s[0]
                amount = payment[0].amount
                data = {
                    "order_id": order_id,
                    "razorpay_payment_id": razorpay_payment_id,
                    "razorpay_order_id": razorpay_order_id,
                    "razorpay_signature": razorpay_signature,
                    "pay_order_id": payment[0].order_id,
                    "repay_amount": amount
                }
                if order_id is not None:
                    Repayment_transaction_Status_Check_auto(data)
                    redirect_with_oid = redirect_url + str(payment[0].order_id)
                    return redirect(redirect_with_oid)
        return redirect(website_base_url + "account/payment")


def get_order_details_cashback_amount(order_details_id, discount_percentage=None):
    """
    This function is used to get order details cashback amount. The order amount is passed in get_cashback_amount and calculate cashback amount using formula.
    cashback amount formula = cashback_amount = round((amount * 15) / 100)
    """
    # order_details_id = 2
    cashback_amount = 0
    if order_details_id is not None:
        get_order_details_obj = OrderDetails.objects.filter(id=order_details_id).first()
        if get_order_details_obj:
            total_order_amount = 0
            total_wallet_paid = 0
            order_amount = get_order_details_obj.order_id.aggregate(Sum('total_amount'))['total_amount__sum']
            if order_amount:
                total_order_amount = order_amount
            wallet_paid = get_order_details_obj.payment.filter(category="Order", status__icontains="success",
                                                               type="Ecom_Wallet").aggregate(Sum('amount'))[
                'amount__sum']

            if wallet_paid:
                total_wallet_paid = wallet_paid

            amount = total_order_amount - total_wallet_paid
            if amount != 0 and amount > 0:
                cashback_amount = amount
                if discount_percentage == "fifty":
                    cashback_amount = ceil((cashback_amount * 50) / 100)

    return cashback_amount


def get_big_offer_status_new(order_date):
    """
    If order date is in offer start date and end date then offer apply on that order.
    """
    start_date = "2020-12-24"
    end_date = "2021-01-01"
    if start_date < order_date < end_date:
        return True
    else:
        return False


def get_zest_money_offer(order_details_obj):
    start_date = "2021-05-31"
    end_date = "2021-07-01"
    order_date = str(order_details_obj.created_date)[:10]
    if start_date < order_date < end_date:
        if order_details_obj.wallet_type in ["zestmoney", "ZestMoney", "ZESTMONEY"]:
            return True
        else:
            return False
    else:
        return False


def Order_transaction_Status_Check_auto(data):
    """
     This function is used for order amount transaction.
     Request parameter :
    {
        "razorpay_payment_id": "pay_DESlfW9H8K9uqM",
        "razorpay_order_id": "order_GhGAzafxmaY6J9",
        "razorpay_signature": "9ef4dffbfd84f1318f6739a3ce19f9d85851857ae648f114332d8401e0949a3d",
    }

    The razorpay_order_id is passed to order_status function to check order status and razorpay_payment_id
    is passed to payment_status to check payment is recieved or not. According to payment status the
    payment entry will update.
    If payment successfully, then create entry in recovery incentive using create_recovery_incentive_entry
    function.
    """
    message = ''
    mode = ''
    status = 0
    data_dict = None
    pay_status = None
    transaction_id = None
    mode = None
    timestamp = None
    razorpay_order_id = None
    razorpay_payment_id = None
    order_id = None
    wallet_type = None

    razorpay_payment_id = data['razorpay_payment_id']
    razorpay_order_id = data['razorpay_order_id']
    razorpay_signature = data["razorpay_signature"]

    pay_obj = Payment.objects.filter(pg_transaction_id=razorpay_order_id).last()
    if pay_obj:
        order_id = pay_obj.order_id

    # generated_signature = hmac_sha256(razorpay_order_id + "|" + razorpay_payment_id, secret);
    # client = razorpay.Client(auth=(Key_id, Key_secret))
    msg = "{}|{}".format(str(data['razorpay_order_id']), str(data['razorpay_payment_id']))

    # secret = str(Key_secret)

    # key = bytes(secret, 'utf-8')
    # body = bytes(msg, 'utf-8')

    # dig = hmac.new(key=key,
    #                msg=body,
    #                digestmod=hashlib.sha256)
    #
    # generated_signature = dig.hexdigest()
    # result = hmac.compare_digest(generated_signature, razorpay_signature)
    order_text = data["order_id"]

    user_id = None
    order_details_obj = OrderDetails.objects.filter(order_text=order_text)
    if order_details_obj:
        user_id = order_details_obj[0].user.id

        get_wallet_obj = UserWallet.objects.filter(user_id=user_id, order_details=order_details_obj[0],
                                                   status="initiated")
        if get_wallet_obj:
            get_wallet_obj[0].status = "success"
            get_wallet_obj[0].updated_date = datetime.now()
            get_wallet_obj[0].save()

        if order_details_obj[0].credit:
            # Updating credit entries..
            credit_obj = Credit.objects.filter(id=order_details_obj[0].credit.id,
                                               status="initiated")
            if credit_obj:
                credit_obj[0].status = status
                credit_obj[0].updated_date = datetime.now()
                credit_obj[0].save()

    order_resp_data = order_status(razorpay_order_id)
    if order_resp_data and order_resp_data is not None:
        status = 2
        message = " Sign Verification Failed"

    payment_data = payment_status(razorpay_payment_id)

    if payment_data and payment_data is not None:
        if payment_data['method'] == 'card':
            mode = 'DC'
        elif payment_data['method'] == 'upi':
            mode = 'UPI'
        elif payment_data['method'] == 'netbanking':
            mode = 'NB'
        else:
            mode = 'RWT'
        timestamp = payment_data['created_at']
        try:
            timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')

        except Exception as e:
            print(e)
            pass

        if payment_data['status'] == 'captured':
            pay_status = 'success'
            status = '1'
            message = 'Transaction successful.'

        elif payment_data['status'] == 'authorized':
            pay_status = 'pending'

            status = '3'
            message = 'Transaction pending'

        else:
            pay_status = 'failed'
            status = '2'
            message = 'Transaction failed'

        if "wallet" in payment_data:
            wallet_type = payment_data["wallet"]

        data_dict = {
            "pg_transaction_id": razorpay_order_id,
            "transaction_id": razorpay_payment_id,
            "order_id": order_id,
            "status": pay_status,
            "mode": mode,
            "amount": payment_data["amount"],
            "timestamp": timestamp,
            "type": "app",
            "wallet_type": wallet_type
        }

    razorpay_payment_process(data_dict)

    return status


class RazorpayPaymentTransactionStatusApi(APIView):
    """
       This API shows the transaction status of the payment.
       Request Parameter :
       {
       "order_id": "order_DESlLckIVRkHWj"
       }

       The order id will search in Payment table & return status_code, message, ecom_order_id,
       paytm_order_id, transaction_id, status, date, amount.
    """

    def post(self, request, *args, **kwargs):
        import requests
        status = None
        message = None
        amount = 0.0
        date_strip = None
        try:
            order_id = request.data.get('order_id')
            if order_id:
                pay = Payment.objects.filter(order_id=order_id).last()
                if pay:
                    return Response(
                        {'status_code': 1,
                         'message': "Order fetch Successfully",
                         'ecom_order_id': order_id,
                         'paytm_order_id': order_id,
                         'transaction_id': pay.transaction_id,
                         'status': pay.status,
                         'date': pay.date,
                         'amount': pay.amount,
                         }
                    )
                else:
                    return Response(
                        {'status_code': 2,
                         'message': "Something went wrong",
                         'ecom_order_id': '',
                         'paytm_order_id': order_id,
                         'transaction_id': None,
                         'status': '',
                         'date': date_strip,
                         'amount': amount,
                         }
                    )




        except Exception as e:
            import sys
            import os
            print('-----------in exception----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)


class RazorpayEcomRepaymentApp(APIView):
    """
        Paytm Repayment Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"

         Request Parameter :
        {
        "shopping_order_id": "order_GhGAzafxmaY6J9",
        "TXN_AMOUNT": "100",
        "use_wallet": "True"
        }
        If use_wallet then used repayment_from_wallet function it will return payable amount.
        The payable amount is have to pay user. Then razorpay order will create.
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None
        ctx = {}
        resp_data = {}

        try:
            shopping_order_id = str(request.data.get('shopping_order_id'))
            TXN_AMOUNT = request.data.get('TXN_AMOUNT')
            random_number = random.sample(range(99999), 1)
            # Considering shopping order_id, CUST_ID and TXN_AMOUNT is getting.
            ORDER_ID = str(shopping_order_id) + 'e' + str(random_number[0])
            CUST_ID = ""
            user_id = request.user.id
            if user_id:
                user = User.objects.filter(id=user_id)
                if user:
                    CUST_ID = user[0].username

                # Repayment from wallet
                use_wallet = True
                if "use_wallet" in request.data:
                    use_wallet = request.data.get('use_wallet')

                if use_wallet:
                    payable_amount = repayment_from_wallet(ORDER_ID, shopping_order_id, TXN_AMOUNT, user_id)
                    if payable_amount == 0:

                        number = user[0].username
                        osms_kwrgs = {
                            'sms_type': "repayment",
                            'number': str(number)
                        }
                        order_sms(**osms_kwrgs)

                        message = "Payment Successful from wallet"
                        status = 6
                        return Response(
                            {
                                'status': status,
                                'message': message
                            }
                        )
                    else:
                        TXN_AMOUNT = payable_amount
            amount = float(TXN_AMOUNT) * 100
            data = {'amount': amount, 'currency': 'INR', 'receipt': ORDER_ID, 'payment_capture': '1'}
            client = razorpay.Client(auth=(Key_id, Key_secret))
            order = client.order.create(data)
            personal = Personal.objects.filter(user_id=request.user.id).last()
            name = None
            dec = None
            email_id = None
            mobile_no = None
            real_amount = order['amount'] / 100
            if personal:
                name = personal.name
                dec = personal.name
                email_id = personal.email_id
                mobile_no = personal.user.username
            if order:
                ctx['order'] = order
                resp_data = {
                    'order_id': order['id'],
                    'amount': str(real_amount),
                    'status': order['status'],
                    'created_at': order['created_at'],
                    'currency': order['currency'],
                    'image_url': '',
                    'key_id': Key_id,
                    'name': name,
                    'dec': dec,
                    'order_id_ck': ORDER_ID,
                    'email_id': email_id,
                    'mobile_no': mobile_no,

                }

                timestamp = order['created_at']
                try:
                    timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
                except Exception as e:
                    print(e)
                    pass

                paym = Payment.objects.create(order_id=ORDER_ID, pg_transaction_id=resp_data['order_id'], \
                                              status="initiated", amount=real_amount, type='App', \
                                              category="Repayment", product_type="Ecom", date=timestamp,
                                              return_status="rstatus", platform_type="App",
                                              pay_source="Rpay")

                if paym and paym is not None:
                    order_details = OrderDetails.objects.filter(order_text=shopping_order_id)
                    if order_details:
                        order_details[0].payment.add(paym)

                message = "Successfully Done"
                status = 1
            else:
                message = "Something Went Wrong"
                status = 0

            return Response(
                {
                    'status': status,
                    'message': message,
                    'data': resp_data
                }
            )

        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


def order_status(razorpay_order_id):
    """
    Input to this function is razorpay_order_id. It will fetch the order data for this order_id.

    Response :
     {
                'order_id': "order_GhGAzafxmaY6J9",
                'amount': "200",
                'amount_paid': "200",
                'status': "captured",
                'receipt': "ORD20713175706951e1234",
                'created_at': 2021-03-10,
                'currency': INR,
                'image_url': '',
                'key_id': Key_id
            }
    """
    resp_data = {}
    if razorpay_order_id and razorpay_order_id is not None:
        chk_order_id = razorpay_order_id
        client = razorpay.Client(auth=(Key_id, Key_secret))
        order = client.order.fetch(chk_order_id)
        if order:
            resp_data = {
                'order_id': order['id'],
                'amount': str(order['amount']),
                'amount_paid': order['amount_paid'],
                'status': order['status'],
                'receipt': order['receipt'],
                'created_at': order['created_at'],
                'currency': order['currency'],
                'image_url': '',
                'key_id': Key_id
            }

            return resp_data

    else:
        return resp_data


def check_status_of_payment(request, payment_id):
    if payment_id is not None:
        resp = payment_status(payment_id)
        if resp:
            print("response", resp)
        else:
            print("No response")


def payment_status(razorpay_payment_id):
    """
    Input parameter to this function is razorpay_payment_id. The function is used to
    fetch all payment details for this payment id.

    """
    resp_data = {}
    if razorpay_payment_id and razorpay_payment_id is not None:
        chk_payment_id = razorpay_payment_id
        client = razorpay.Client(auth=(Key_id, Key_secret))
        payment = client.payment.fetch(chk_payment_id)
        if payment:
            resp_data = {
                'payment_id': payment['id'],
                'entity': payment['entity'],
                'amount': str(payment['amount']),
                'currency': str(payment['currency']),
                'status': str(payment['status']),
                'order_id': str(payment['order_id']),
                'invoice_id': payment['invoice_id'],
                'international': payment['international'],
                'method': payment['method'],
                'amount_refunded': payment['amount_refunded'],
                'refund_status': payment['refund_status'],
                'captured': payment['captured'],
                'description': payment['description'],
                'card_id': payment['card_id'],
                'bank': payment['bank'],
                'wallet': payment['wallet'],
                'vpa': payment['vpa'],
                'email': payment['email'],
                'contact': payment['contact'],
                'notes': payment['notes'],
                'fee': payment['fee'],
                'tax': payment['tax'],
                'error_code': payment['error_code'],
                'error_description': payment['error_description'],
                'created_at': payment['created_at'],
                'key_id': Key_id
            }
            return resp_data

    else:
        return resp_data


# app code


class RazorpayEcomOrderApp(APIView):
    """
        Paytm Order Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"

    Request Parameter :
        {
        "shopping_order_id": "order_GhGAzafxmaY6J9",
        "TXN_AMOUNT": "100",
        "use_wallet": "True"
        }
        If use_wallet then used repayment_from_wallet function it will return payable amount.
        The payable amount is have to pay user. Then razorpay order will create.

        If wallet condition is 1 then it is pass to callback order razorpay with response data.
        Here order will create on razorpay.
        If wallet condition is 0, this means no extra amount paid by customer.
        If wallet condition is 2, this means this is the condition of cod.

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None
        ctx = {}
        resp_data = {}
        try:
            shopping_order_id = str(request.data.get('shopping_order_id'))
            wallet_condition = request.data.get('wallet_condition')
            random_number = random.sample(range(99999), 1)
            ORDER_ID = str(shopping_order_id) + 'e' + str(random_number[0])

            if wallet_condition == 1:
                TXN_AMOUNT = str(request.data.get('TXN_AMOUNT'))
                amount = float(TXN_AMOUNT) * 100
                order_details = OrderDetails.objects.filter(order_text=shopping_order_id)
                if order_details:
                    payment_mode = order_details[0].payment_mode
                    if payment_mode == "cash":
                        data = {'amount': amount, 'currency': 'INR', 'receipt': ORDER_ID, 'payment_capture': '1',
                                "offers": ["offer_HgHszfBUCWmqDN"]}
                    else:
                        data = {'amount': amount, 'currency': 'INR', 'receipt': ORDER_ID, 'payment_capture': '1'}
                else:
                    data = {'amount': amount, 'currency': 'INR', 'receipt': ORDER_ID, 'payment_capture': '1'}
                client = razorpay.Client(auth=(Key_id, Key_secret))
                order = client.order.create(data)
                personal = Personal.objects.filter(user_id=request.user.id).last()
                name = None
                dec = None
                email_id = None
                mobile_no = None
                real_amount = order['amount'] / 100
                if personal:
                    name = personal.name
                    dec = personal.name
                    email_id = personal.email_id
                    mobile_no = personal.user.username
                if order:
                    ctx['order'] = order
                    resp_data = {
                        'order_id': order['id'],
                        'amount': str(real_amount),
                        'status': order['status'],
                        'created_at': order['created_at'],
                        'currency': order['currency'],
                        'image_url': '',
                        'key_id': Key_id,
                        'name': name,
                        'dec': dec,
                        'order_id_ck': ORDER_ID,
                        'email_id': email_id,
                        'mobile_no': mobile_no,

                    }

                    timestamp = order['created_at']
                    try:
                        timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
                    except Exception as e:
                        print(e)
                        pass

                    paym = Payment.objects.create(order_id=ORDER_ID, pg_transaction_id=resp_data['order_id'], \
                                                  status="initiated", amount=real_amount, type='App', \
                                                  category="Order", product_type="Ecom", date=timestamp,
                                                  return_status="rstatus", platform_type="App",
                                                  pay_source="Rpay")

                    if paym and paym is not None:
                        order_details = OrderDetails.objects.filter(order_text=shopping_order_id)
                        if order_details:
                            order_details[0].payment.add(paym)

                    status = 1
                    message = 'Success'
                else:
                    message = "Something Went Wrong"
                    status = 0
                return Response(
                    {
                        'status': status,
                        'message': message,
                        'data': resp_data
                    }
                )

            if wallet_condition == 0:
                # This means no extra amount paid by customer..
                # Only updating all initiated entries to plaed for Order..
                order_details_obj = OrderDetails.objects.filter(order_text=shopping_order_id)
                if order_details_obj:
                    get_payment_obj = order_details_obj[0].payment.filter(product_type="Ecom",
                                                                          category='Order', type="Ecom_Wallet",
                                                                          mode="PPI", status="initiated")

                    if get_payment_obj:
                        get_payment_obj[0].status = "success"
                        get_payment_obj[0].update_date = datetime.now()
                        get_payment_obj[0].save()

                    # Updating Wallet object to success..
                    get_wallet_obj = UserWallet.objects.filter(order_details=order_details_obj[0], status="initiated")
                    if get_wallet_obj:
                        get_wallet_obj[0].status = "success"
                        get_wallet_obj[0].updated_date = datetime.now()
                        get_wallet_obj[0].save()

                    # Add cashback amount in user wallet..
                    payment_mode = order_details_obj[0].payment_mode
                    if payment_mode == "cash":
                        discount = 0
                        order_date = str(order_details_obj[0].created_date)[:10]
                        is_big_offer = get_big_offer_status(order_date)
                        if is_big_offer:
                            discount = 0
                        else:
                            cashback_amount = order_details_obj[0].total_amount
                            # This function is for 15% cashback..
                            discount = get_cashback_amount(cashback_amount)

                        if discount > 0:
                            # Adding cashback amount for the order in user wallet..
                            status = "cashback_amount"
                            order_obj = None
                            add_wallet = user_wallet_update(order_details_obj[0].user.id, discount,
                                                            order_details_obj[0], order_obj,
                                                            status)

                    # Updating credit entry to success
                    if order_details_obj[0].credit:
                        get_credit_obj = Credit.objects.filter(id=order_details_obj[0].credit.id,
                                                               status="initiated").order_by('id').last()
                        if get_credit_obj:
                            get_credit_obj.status = "success"
                            get_credit_obj.updated_date = datetime.now()
                            get_credit_obj.save()

                    # updating status of order to placed..
                    try:
                        update_status = change_successful_orders_status(order_details_obj[0])
                    except Exception as e:
                        print(
                            "Inside Exception of change order status ChecksumGenerateEcomPaytmOrderWebsite", e)
                    status = 2
                    message = 'Transaction successful.'

            if wallet_condition == 2:
                # This is the condition of cod..
                # updating status of order to placed..
                order_details_obj = OrderDetails.objects.filter(order_text=shopping_order_id)
                if order_details_obj:
                    try:
                        update_status = change_successful_orders_status(order_details_obj[0])
                    except Exception as e:
                        print(
                            "Inside Exception of change order status ChecksumGenerateEcomPaytmOrder", e)

                    status = 2
                    message = 'Transaction successful.'

            return Response(
                {
                    'status': status,
                    'message': message
                }
            )

        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


class Order_transaction_Status_Check_auto_app(APIView):
    """
     This function is used for order amount transaction.
     Request parameter :
    {
        "razorpay_payment_id": "pay_DESlfW9H8K9uqM",
        "razorpay_order_id": "order_GhGAzafxmaY6J9",
        "razorpay_signature": "9ef4dffbfd84f1318f6739a3ce19f9d85851857ae648f114332d8401e0949a3d",
    }

    The razorpay_order_id is passed to order_status function to check order status and razorpay_payment_id
    is passed to payment_status to check payment is recieved or not. According to payment status the
    payment entry will update.
    If payment successfully, then create entry in recovery incentive using create_recovery_incentive_entry
    function.
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        message = ''
        data_dict = None
        transaction_id = None
        mode = None
        order_text = None
        payment_order_id = None
        timestamp = None
        status = 0
        order_id = None
        pay_status = None
        razorpay_payment_id = None
        razorpay_order_id = None
        wallet_type = None

        razorpay_payment_id = str(request.data.get('razorpay_payment_id'))

        razorpay_order_id = str(request.data.get('razorpay_order_id'))
        razorpay_signature = str(request.data.get('razorpay_signature'))
        # generated_signature = hmac_sha256(razorpay_order_id + "|" + razorpay_payment_id, secret);
        client = razorpay.Client(auth=(Key_id, Key_secret))
        msg = "{}|{}".format(str(request.data.get('razorpay_order_id')), str(request.data.get('razorpay_payment_id')))

        secret = str(Key_secret)

        key = bytes(secret, 'utf-8')
        body = bytes(msg, 'utf-8')

        payment = Payment.objects.filter(pg_transaction_id=razorpay_order_id)
        if payment:
            payment_order_id = payment[0].order_id
            order_id_s = str(payment[0].order_id).split("e")
            order_id = order_id_s[0]
            data = {
                "order_id": order_id,
                "razorpay_payment_id": razorpay_payment_id,
                "razorpay_order_id": razorpay_order_id,
                "razorpay_signature": razorpay_signature,
                "pay_order_id": payment[0].order_id
            }

            order_text = data["order_id"]

        order_details_obj = OrderDetails.objects.filter(order_text=order_text)
        user_id = None
        if order_details_obj:
            user_id = order_details_obj[0].user.id
            get_wallet_obj = UserWallet.objects.filter(order_details=order_details_obj[0],
                                                       status="initiated")
            if get_wallet_obj:
                get_wallet_obj[0].status = "success"
                get_wallet_obj[0].updated_date = datetime.now()
                get_wallet_obj[0].save()

            if order_details_obj[0].credit:
                # Updating credit entries..
                get_credit_obj = Credit.objects.filter(id=order_details_obj[0].credit.id,
                                                       status="initiated")
                if get_credit_obj:
                    get_credit_obj[0].status = "success"
                    get_credit_obj[0].updated_date = datetime.now()
                    get_credit_obj[0].save()

        order_resp_data = order_status(razorpay_order_id)
        if order_resp_data and order_resp_data is not None:
            status = 2
            message = " Sign Verification Failed"

        payment_data = payment_status(razorpay_payment_id)

        if payment_data and payment_data is not None:

            if payment_data['method'] == 'card':
                mode = 'DC'
            elif payment_data['method'] == 'upi':
                mode = 'UPI'
            elif payment_data['method'] == 'netbanking':
                mode = 'NB'
            else:
                mode = 'RWT'
            timestamp = payment_data['created_at']

            try:
                timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')

            except Exception as e:
                print(e)
                pass

            if payment_data['status'] == 'captured':
                pay_status = 'success'
                status = '1'
                message = 'Transaction successful.'

            elif payment_data['status'] == 'authorized':
                pay_status = 'pending'

                status = '3'
                message = 'Transaction pending'

            else:
                pay_status = 'failed'
                status = '2'
                message = 'Transaction failed'

                # update wallet entries..

            if "wallet" in payment_data:
                wallet_type = payment_data["wallet"]

            data_dict = {
                "pg_transaction_id": razorpay_order_id,
                "transaction_id": razorpay_payment_id,
                "order_id": payment_order_id,
                "status": pay_status,
                "mode": mode,
                "amount": payment_data["amount"],
                "timestamp": timestamp,
                "type": "app",
                "wallet_type": wallet_type
            }

        razorpay_payment_process(data_dict)

        return Response(
            {
                'status': status,
                'message': message
            }
        )


class Repayment_transaction_Status_Check_auto_App(APIView):
    """
       This function is used for repayment.
        Request parameter :
       {
           "razorpay_payment_id": "pay_DESlfW9H8K9uqM",
           "razorpay_order_id": "order_GhGAzafxmaY6J9",
           "razorpay_signature": "9ef4dffbfd84f1318f6739a3ce19f9d85851857ae648f114332d8401e0949a3d",
           "repay_amount: "100"
       }

       The razorpay_order_id is passed to order_status function to check order status and razorpay_payment_id
       is passed to payment_status to check payment is recieved or not. According to payment status the
       payment entry will update.
       If payment successfully, then create entry in recovery incentive using create_recovery_incentive_entry
       function.

       """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        amount = 0.0
        mode = None
        date_strip = None
        pay_status = None
        razorpay_payment_id = str(request.data.get('razorpay_payment_id'))

        razorpay_order_id = str(request.data.get('razorpay_order_id'))
        razorpay_signature = str(request.data.get('razorpay_signature'))
        # generated_signature = hmac_sha256(razorpay_order_id + "|" + razorpay_payment_id, secret);
        client = razorpay.Client(auth=(Key_id, Key_secret))
        msg = "{}|{}".format(str(request.data.get('razorpay_order_id')), str(request.data.get('razorpay_payment_id')))

        secret = str(Key_secret)

        key = bytes(secret, 'utf-8')
        body = bytes(msg, 'utf-8')

        dig = hmac.new(key=key,
                       msg=body,
                       digestmod=hashlib.sha256)

        generated_signature = dig.hexdigest()
        result = hmac.compare_digest(generated_signature, razorpay_signature)
        payment = Payment.objects.filter(pg_transaction_id=razorpay_order_id)
        if payment:
            order_id_s = str(payment[0].order_id).split("e")
            order_id = order_id_s[0]
            amount = payment[0].amount
            data = {
                "order_id": order_id,
                "razorpay_payment_id": razorpay_payment_id,
                "razorpay_order_id": razorpay_order_id,
                "razorpay_signature": razorpay_signature,
                "pay_order_id": payment[0].order_id
            }
        order_text = data["order_id"]
        if result == True:
            pass

        order_resp_data = order_status(razorpay_order_id)
        if order_resp_data and order_resp_data is not None:
            status = 2
            message = " Sign Verification Failed"

        payment_data = payment_status(razorpay_payment_id)
        if payment_data and payment_data is not None:
            if payment_data['method'] == 'card':
                mode = 'DC'
            elif payment_data['method'] == 'upi':
                mode = 'UPI'
            elif payment_data['method'] == 'netbanking':
                mode = 'NB'
            else:
                mode = 'RWT'
            timestamp = payment_data['created_at']
            try:
                timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')

            except Exception as e:
                print(e)
                pass

            if payment_data['status'] == 'captured':
                pay_status = 'success'
                status = '1'
                message = 'Transaction successful.'

            elif payment_data['status'] == 'authorized':
                pay_status = 'pending'

                status = '3'
                message = 'Transaction pending'



            else:
                pay_status = 'failed'
                status = '2'
                message = 'Transaction failed'

        payment_obj = Payment.objects.filter(order_id=data["pay_order_id"],
                                             category='Repayment', status__in=["initiated", "pending"])
        if payment_obj:
            payment_obj[0].status = pay_status
            payment_obj[0].transaction_id = data["razorpay_payment_id"]
            payment_obj[0].mode = mode
            payment_obj[0].type = "App"
            payment_obj[0].update_date = datetime.now()
            payment_obj[0].save()

        try:
            if pay_status == 'failed':
                mobile_number = None
                order_payment = payment_obj[0].orderdetails_payment.all()
                if order_payment:
                    if order_payment[0].user:
                        mobile_number = order_payment[0].user.username

                if mobile_number:
                    failed_transaction_sms(mobile_number, payment_obj[0].amount)
        except Exception as e:
            print('-----------in exception----------')
            print(e.args)

        order_details_obj = OrderDetails.objects.filter(order_text=order_text)
        if order_details_obj:
            user_id = order_details_obj[0].user.id
            if pay_status == 'success':
                order_credit_status = check_order_details_credit_status(order_details_obj[0].id)
                # Validating
                if order_credit_status:
                    order_details_obj = OrderDetails.objects.filter(id=order_details_obj[0].id)
                    if order_details_obj:
                        order_details_obj[0].credit_status = order_credit_status
                        order_details_obj[0].updated_date = datetime.now()
                        order_details_obj[0].save()

                # check if credit exist
                credit_status = "repayment"
                import time
                sleep_time = random.random()
                time.sleep(sleep_time)
                sleep_time = random.random()
                time.sleep(sleep_time)
                get_credit_repayment = order_details_obj[0].credit_history.filter(
                    status=credit_status, repayment_add=amount,
                    created_date__startswith=str(datetime.now())[:16])
                try:
                    from Recovery.views import create_recovery_incentive_entry

                    create_recovery_incentive_entry(order_details_obj[0].id)
                except:
                    pass

                if get_credit_repayment:
                    pass

                else:
                    # No credit entry, so credit not updated
                    # Updating User credit balance..
                    update_credit = credit_balance_update(user_id, amount, credit_status)
                    if update_credit:
                        # Adding new credit entry in Credit history of Order Details Table..
                        order_details_obj[0].credit_history.add(update_credit)

                try:
                    number = order_details_obj[0].user.username
                    osms_kwrgs = {
                        'sms_type': "repayment",
                        'number': str(number)
                    }
                    order_sms(**osms_kwrgs)
                except Exception as e:
                    print("error in repayment")

                status = '1'
                message = 'Transaction successful.'

            elif pay_status == 'failed':

                status = '2'
                message = 'Transaction failed'

            elif pay_status == 'pending':

                status = '3'
                message = 'Transaction pending'

            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


def wallet_return_amount(order_details_id):
    """
    Input parameter to this function is order details id. The function is used return wallet amount.
    If order is returned, cancelled then its delivery charge, convenient_charge will have to returned
    in wallet. Using this function delivery_charge, convenient_charge will returned to wallet.
    """
    payment_mode = None
    order_details_total_amount = 0
    credit_actual_used = 0
    shipping_charge = 0
    convenient_fees = 0
    shipping_convenient_amount = 0
    pay_amount = 0
    wallet_amount = 0
    order_details_obj = OrderDetails.objects.filter(id=order_details_id)
    if order_details_obj:
        shipping_charge = get_order_details_delivery_charge(order_details_id)
        convenient_fees = get_order_details_convenient_charge(order_details_id)
        pay_amount = order_details_obj[0].pay_amount
        payment_mode = order_details_obj[0].payment_mode
        if order_details_obj[0].credit:
            credit_actual_used = float(order_details_obj[0].credit.actual_used)

        order_details_total_amount = float(order_details_obj[0].total_amount)

        extra_pay = order_details_total_amount - credit_actual_used
        if payment_mode:
            if payment_mode == "credit":
                shipping_convenient_amount = float(shipping_charge) + float(convenient_fees) + float(extra_pay)
                wallet_amount = float(pay_amount) - float(shipping_convenient_amount)

            if payment_mode == "cash":
                order_amount = order_details_obj[0].total_amount
                total_amount = float(order_amount) + float(shipping_charge) + float(convenient_fees)
                wallet_amount = float(pay_amount) - float(total_amount)

    return wallet_amount


def get_order_details_delivery_charge(order_details_id):
    """
    The function is to used return total order delivery charges. Input to the function order_details_id.
    first get all the orders include in order details exclude order status cancelled.
    """
    if order_details_id is not None:
        final_delivery_amount = 0
        get_all_orders = OrderDetails.objects.filter(id=order_details_id).values_list('order_id', flat=True)
        if get_all_orders is not None:
            all_orders = Order.objects.filter(id__in=get_all_orders).exclude(status__icontains="cancelled")
            if all_orders:
                # Get delivery charge object..
                for order in all_orders:
                    delivery_charge = 0
                    if order.delivery_charges:
                        delivery_charge = order.delivery_charges.amount
                    final_delivery_amount += delivery_charge
        return final_delivery_amount


def get_order_details_convenient_charge(order_details_id):
    """
       The function is to used return total order convenient charges. Input to the function order_details_id.
       first get all the orders include in order details exclude order status cancelled.
    """
    if order_details_id is not None:
        final_convenient_amount = 0
        get_all_orders = OrderDetails.objects.filter(id=order_details_id).values_list('order_id', flat=True)
        if get_all_orders is not None:
            all_orders = Order.objects.filter(id__in=get_all_orders).exclude(status__icontains="cancelled")
            if all_orders:
                # Get delivery charge object..
                for order in all_orders:
                    convenient_charge = 0
                    if order.convenient_fees:
                        convenient_charge = order.convenient_fees.amount
                    final_convenient_amount += convenient_charge
        return final_convenient_amount


def success_razorpay_transaction(order_details_obj, status, wallet_type=None):
    """
    This function will place the order

    parameters :
                order_details_obj : queryset of OrderDetails
                status : R'pay trans. status

    Input parameter to this function is order_details_object and status.
    If status is success call wallet_return_amoun. If wallet amount is greater than zero then
    the wallet amount add to last balance wallet amount for particular user.

    """

    if status == 'success':

        wallet_amount = wallet_return_amount(order_details_obj[0].id)

        if wallet_amount > 0:

            userwallet = UserWallet.objects.filter(
                user_id=order_details_obj[0].user_id).last()

            if userwallet:
                balance_amount = userwallet.balance_amount + wallet_amount
                UserWallet.objects.create(user_id=order_details_obj[0].user_id,
                                          amount_add=wallet_amount,
                                          balance_amount=balance_amount,
                                          status="settlement")

        if order_details_obj[0].credit:

            credit_amount = order_details_obj[0].credit.actual_used
            check_pre_credit_entry = order_details_obj[0].credit_history.filter(
                status="refund",
                repayment_add=credit_amount)

            if check_pre_credit_entry:

                credit_obj = Credit.objects.filter(user_id=order_details_obj[0].user.id,
                                                   id=check_pre_credit_entry[0].id)
                if credit_obj:
                    credit_obj[0].status = "initiated"
                    credit_obj[0].updated_date = datetime.now()
                    credit_obj[0].save()

        get_wallet_obj = UserWallet.objects.filter(order_details=order_details_obj[0],
                                                   status="success").first()

        if get_wallet_obj:

            wallet_amount = get_wallet_obj.used_amount

            user_id = order_details_obj[0].user.id

            if user_id:
                check_previous_entry = UserWallet.objects.filter(user=user_id,
                                                                 order_details=
                                                                 order_details_obj[0],
                                                                 status="refund")
                if check_previous_entry:
                    check_previous_entry[0].status = "initiated"
                    check_previous_entry[0].updated_status = datetime.now()
                    check_previous_entry[0].save()

        total_amount_paid = Payment.objects.filter(product_type="Ecom", category="Order",
                                                   order_id__startswith=order_details_obj[0].order_text,
                                                   status__icontains="success",
                                                   ).aggregate(Sum('amount'))['amount__sum']

        total_order_amount = order_details_obj[0].pay_amount

        if total_amount_paid >= total_order_amount:
            try:
                update_status = change_successful_orders_status(order_details_obj[0], wallet_type)
            except Exception as e:
                print(
                    "Inside Exception of change order status Order_transaction_Status_Check_auto",
                    e)

            try:
                order_details_obj = OrderDetails.objects.filter(id=order_details_obj[0].id)
                payment_mode = order_details_obj[0].payment_mode
                if payment_mode == "cash":
                    discount = 0
                    order_date = str(order_details_obj[0].created_date)[:10]
                    is_big_offer = get_big_offer_status(order_date)
                    is_big_offer_new = get_big_offer_status_new(order_date)
                    is_zestmony_offer = get_zest_money_offer(order_details_obj[0])

                    if is_big_offer:
                        discount = get_order_details_cashback_amount(order_details_obj[0].id)

                    elif is_big_offer_new or is_zestmony_offer:
                        discount_percentage = "fifty"
                        discount = get_order_details_cashback_amount(order_details_obj[0].id, discount_percentage)

                    else:
                        cashback_amount = order_details_obj[0].total_amount
                        # This function is for 15% cashback..
                        discount = get_cashback_amount(cashback_amount)

                    if discount > 0:
                        # Adding cashback amount for the order in user wallet..
                        ca_status = "cashback_amount"
                        order_obj = None
                        add_wallet = user_wallet_update(order_details_obj[0].user.id,
                                                        discount,
                                                        order_details_obj[0], order_obj,
                                                        ca_status)

            except Exception as e:

                print("inside Exception of cashback amount", e)


def failed_razorpay_transaction(order_details_obj, status):
    """
        This function is used to refund for the order

        parameters :
                    order_details_obj : queryset of OrderDetails
                    status : R'pay trans. status
        If Razorpay payment status is failed then we refund the amount in wallet.
        Using user_wallet_update function we will refund the amount in wallet.

        """
    if status == "failed":
        get_wallet_obj = UserWallet.objects.filter(order_details=order_details_obj[0],
                                                   status="success").first()
        if get_wallet_obj:
            wallet_amount = get_wallet_obj.used_amount
            if order_details_obj[0].user.id:
                check_previous_entry = UserWallet.objects.filter(user=order_details_obj[0].user.id,
                                                                 order_details=
                                                                 order_details_obj[0],
                                                                 status="refund")
                if not check_previous_entry:
                    status = "refund"
                    user_wallet_update(order_details_obj[0].user.id, wallet_amount, order_details_obj[0], None,
                                       status)

        # updating Credit entries..
        if order_details_obj[0].credit:
            credit_amount = order_details_obj[0].credit.actual_used
            check_pre_credit_entry = order_details_obj[0].credit_history.filter(
                status="refund",
                repayment_add=credit_amount)
            if not check_pre_credit_entry:
                credit_status = "refund"
                give_refund = credit_balance_update(order_details_obj[0].user.id, credit_amount, credit_status)
                if give_refund:
                    order_details_obj[0].credit_history.add(give_refund)


def razorpay_transaction_for_repayment(order_details_obj, status, amount):
    """
    This function handle success R'pay transaction for 'Repayment'.

    parameters :
                order_details_obj : "Queryset of OrderDetails"
                status : status of payment
                amount : amount of repayment

    First get the get_credit_repayment if not then pass order_details_id to credit_balance_update function.
    The updated new credit entry will attached to Orderdetails credit_history.

    """
    if status == "success":

        if order_details_obj:
            order_credit_status = check_order_details_credit_status(
                order_details_obj[0].id)
            # Validating
            if order_credit_status:
                order_details_obj = OrderDetails.objects.filter(id=order_details_obj[0].id)
                if order_details_obj:
                    order_details_obj[0].credit_status = order_credit_status
                    order_details_obj[0].updated_date = datetime.now()
                    order_details_obj[0].save()

            # check if credit exist
            credit_status = "repayment"

            get_credit_repayment = order_details_obj[0].credit_history.filter(
                status=credit_status, repayment_add=amount,
                created_date__startswith=str(datetime.now())[:16])
            try:
                from Recovery.views import create_recovery_incentive_entry

                create_recovery_incentive_entry(order_details_obj[0].id)
            except:
                pass

            if get_credit_repayment:
                pass

            else:
                # No credit entry, so credit not updated
                # Updating User credit balance..
                update_credit = credit_balance_update(order_details_obj[0].user.id, amount, credit_status)

                if update_credit:
                    # Adding new credit entry in Credit history of Order Details Table..
                    order_details_obj[0].credit_history.add(update_credit)

            try:
                number = order_details_obj[0].user.username
                osms_kwrgs = {
                    'sms_type': "repayment",
                    'number': str(number)
                }
                order_sms(**osms_kwrgs)
            except Exception as e:
                print("error in repayment")


class ERazorpayWebhookOrderNew(APIView):
    """
    -- Sample response:
                        {
                          "entity":"event",
                          "account_id":"acc_BFQ7uQEaa7j2z7",
                          "event":"order.paid",
                          "contains":[
                            "payment",
                            "order"
                          ],
                          "payload":{
                            "payment":{
                              "entity":{
                                "id":"pay_DESlfW9H8K9uqM",
                                "entity":"payment",
                                "amount":100,
                                "currency":"INR",
                                "status":"captured",
                                "order_id":"order_DESlLckIVRkHWj",
                                "invoice_id":null,
                                "international":false,
                                "method":"netbanking",
                                "amount_refunded":0,
                                "refund_status":null,
                                "captured":true,
                                "description":null,
                                "card_id":null,
                                "bank":"HDFC",
                                "wallet":null,
                                "vpa":null,
                                "email":"gaurav.kumar@example.com",
                                "contact":"+919876543210",
                                "notes":[],
                                "fee":2,
                                "tax":0,
                                "error_code":null,
                                "error_description":null,
                                "created_at":1567674599
                              }
                            },
                            "order":{
                              "entity":{
                                "id":"order_DESlLckIVRkHWj",
                                "entity":"order",
                                "amount":100,
                                "amount_paid":100,
                                "amount_due":0,
                                "currency":"INR",
                                "receipt":"rcptid #1",
                                "offer_id":null,
                                "status":"paid",
                                "attempts":1,
                                "notes":[],
                                "created_at":1567674581
                                }
                              }
                            },
                           "created_at":1567674606
                        }
    """

    def get(self, request, *args, **kwargs):
        request_data = {}
        message = 'Something Went Wrong'
        order_id = None
        data_dict = None
        amount = None
        status = None
        pay_order_id = None
        mode = None
        wallet_type = None
        payment_id = None
        timestamp = None
        rsp_receipt_id = None
        headers = request.META
        request_data = request.data
        client = razorpay.Client(auth=(Key_id, Key_secret))
        webhook_signature = headers['HTTP_X_RAZORPAY_SIGNATURE']
        webhook_secret = 'Mudrakwik@123'
        payload_body = json.dumps(request.data, separators=(',', ':'))
        if 'payload' in request_data:  # check for payload in response
            payload_data = request_data['payload']
            if 'payment' in payload_data:  # check for payment in response
                if 'entity' in payload_data['payment']:
                    entity_data = request_data['payload']['payment']['entity']
                    if entity_data is not None:
                        payment_data = payload_data["payment"]["entity"]
                        if payment_data and payment_data is not None:
                            if payment_data['method'] == 'card':
                                mode = 'DC'
                            elif payment_data['method'] == 'upi':
                                mode = 'UPI'
                            elif payment_data['method'] == 'netbanking':
                                mode = 'NB'
                            else:
                                mode = 'RWT'

                            if "wallet" in payment_data:
                                wallet_type = payment_data["wallet"]

                            timestamp = payment_data['created_at']
                            amount = float(payment_data["amount"]) / 100
                            try:
                                timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
                            except Exception as e:
                                print(e)
                                pass
                        res_order_id = entity_data["order_id"]  # get transcation_id from response
                        if res_order_id and res_order_id is not None:
                            order_id = res_order_id
                        res_payment_id = entity_data["id"]
                        if res_payment_id and res_payment_id is not None:
                            payment_id = res_payment_id
                        if "order" in request_data['contains'] and "payment" in request_data[
                            'contains']:  # success condition
                            # Get receipt from order
                            # Receipt means our payment order_id
                            rsp_receipt_id = request_data['payload']['order']['entity']['receipt']
                            order_text = rsp_receipt_id.split("e")
                            order_text = order_text[0]
                            order_details_obj = OrderDetails.objects.filter(order_text=order_text)
                            if order_details_obj:
                                if request_data['payload']['order']['entity']['status'] == "paid":
                                    status = "success"
                                else:
                                    status = "pending"
                                data_dict = {
                                    "pg_transaction_id": order_id,
                                    "transaction_id": payment_id,
                                    "order_id": rsp_receipt_id,
                                    "status": status,
                                    "amount": amount,
                                    "mode": mode,
                                    "wallet_type": wallet_type,
                                    "timestamp": timestamp,
                                    "type": "hook"
                                }
                        elif len(request_data['contains']) == 1 and "payment" in request_data[
                            'contains']:  # check for other conditions
                            status = ''
                            if request_data['payload']['payment']['entity']['status'] == "authorized":
                                status = "pending"
                            elif request_data['payload']['payment']['entity']['status'] == "captured":
                                status = "success"
                            elif request_data['payload']['payment']['entity']['status'] == "failed":
                                status = "failed"
                            else:
                                status = "failed"

                            try:
                                if "wallet" in request_data['payload']['payment']['entity']:
                                    wallet_type = request_data['payload']['payment']['entity']["wallet"]
                            except Exception as e:
                                print("Inside exception of Ewebhook order razorpay checking walet condition")

                            pay_order = Payment.objects.filter(pg_transaction_id=order_id).first()
                            if pay_order:
                                pay_order_id = pay_order.order_id
                            data_dict = {
                                "pg_transaction_id": order_id,
                                "transaction_id": payment_id,
                                "order_id": pay_order_id,
                                "status": status,
                                "amount": amount,
                                "mode": mode,
                                "wallet_type": wallet_type,
                                "timestamp": timestamp,
                                "type": "hook"
                            }

        razorpay_payment_process(data_dict)
        return HttpResponse("ok")


class RazorpayEcomRepaymentAll(APIView):
    """
        Paytm Repayment Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None
        ctx = {}
        TXN_AMOUNT = 0

        CUST_ID = ""
        user_id = request.user.id

        if user_id:
            user = User.objects.filter(id=user_id)
            if user:
                CUST_ID = user[0].username

        try:
            # shopping_order_id = str(request.data.get('shopping_order_id'))
            from ecom.views import get_order_details_remaining_amount
            repaymet_dict = get_order_details_remaining_amount(user_id)
            if repaymet_dict:
                for key, value in repaymet_dict.items():
                    if key == "Amount":
                        TXN_AMOUNT = value

            random_number = random.sample(range(99999), 1)
            # Considering shopping order_id, CUST_ID and TXN_AMOUNT is getting.
            date = datetime.now()
            application_id = str(date.year)[2:] + "" + str(date.month) + "" + str(date.day) + "" + str(
                date.hour) + "" + str(date.minute) + "" + str(date.second) + "" + str(random_number[0])

            ORDER_ID = 'GN' + str(application_id)

            amount = float(TXN_AMOUNT) * 100

            data = {'amount': amount, 'currency': 'INR', 'receipt': ORDER_ID, 'payment_capture': '1'}
            client = razorpay.Client(auth=(Key_id, Key_secret))
            order = client.order.create(data)
            personal = Personal.objects.filter(user_id=request.user.id).last()
            name = None
            dec = None
            email_id = None
            mobile_no = None
            real_amount = order['amount'] / 100
            if personal:
                name = personal.name
                dec = personal.name
                email_id = personal.email_id
                mobile_no = personal.user.username
            if order:
                ctx['order'] = order
                resp_data = {
                    'order_id': order['id'],
                    'amount': str(real_amount),
                    'status': order['status'],
                    'created_at': order['created_at'],
                    'currency': order['currency'],
                    'image_url': '',
                    'key_id': Key_id,
                    'name': name,
                    'dec': dec,
                    'order_id_ck': ORDER_ID,
                    'email_id': email_id,
                    'mobile_no': mobile_no,

                }

                timestamp = order['created_at']
                try:
                    timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
                except Exception as e:
                    print(e)
                    pass

                paym = Payment.objects.create(order_id=ORDER_ID, pg_transaction_id=resp_data['order_id'], \
                                              status="initiated", amount=real_amount, type='App', \
                                              category="Repayment", product_type="Ecom", date=timestamp,
                                              return_status="rstatus", platform_type="App",
                                              pay_source="Rpay")

                message = "Successfully Done"
                status = 1
            else:
                message = "Something Went Wrong"
                status = 0

            return Response(
                {
                    'status': status,
                    'message': message,
                    'data': resp_data
                }
            )

        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


class Repayment_transaction_Status_Check_auto_All(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        amount = 0.0
        data = {}
        date_strip = None
        pay_status = None
        user_id = request.user.id
        razorpay_payment_id = str(request.data.get('razorpay_payment_id'))

        razorpay_order_id = str(request.data.get('razorpay_order_id'))
        razorpay_signature = str(request.data.get('razorpay_signature'))
        client = razorpay.Client(auth=(Key_id, Key_secret))
        msg = "{}|{}".format(str(request.data.get('razorpay_order_id')), str(request.data.get('razorpay_payment_id')))

        secret = str(Key_secret)

        key = bytes(secret, 'utf-8')
        body = bytes(msg, 'utf-8')

        dig = hmac.new(key=key,
                       msg=body,
                       digestmod=hashlib.sha256)

        generated_signature = dig.hexdigest()
        result = hmac.compare_digest(generated_signature, razorpay_signature)
        payment = Payment.objects.filter(pg_transaction_id=razorpay_order_id)
        if payment:
            amount = payment[0].amount
            data = {
                "razorpay_payment_id": razorpay_payment_id,
                "razorpay_order_id": razorpay_order_id,
                "razorpay_signature": razorpay_signature,
                "pay_order_id": payment[0].order_id
            }
        if result == True:
            pass

        order_resp_data = order_status(razorpay_order_id)
        if order_resp_data and order_resp_data is not None:
            status = 2
            message = " Sign Verification Failed"

        payment_data = payment_status(razorpay_payment_id)
        if payment_data and payment_data is not None:
            if payment_data['method'] == 'card':
                mode = 'DC'
            elif payment_data['method'] == 'upi':
                mode = 'UPI'
            elif payment_data['method'] == 'netbanking':
                mode = 'NB'
            else:
                mode = 'RWT'
            timestamp = payment_data['created_at']
            try:
                timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')

            except Exception as e:
                print(e)
                pass

            if payment_data['status'] == 'captured':
                pay_status = 'success'
                status = '1'
                message = 'Transaction successful.'

            elif payment_data['status'] == 'authorized':
                pay_status = 'pending'

                status = '3'
                message = 'Transaction pending'

            else:
                pay_status = 'failed'
                status = '2'
                message = 'Transaction failed'

        Payment.objects.filter(order_id=data["pay_order_id"],
                               category='Repayment', status__in=["initiated", "pending"]).update(status=pay_status,
                                                                                                 transaction_id=data[
                                                                                                     "razorpay_payment_id"],
                                                                                                 mode=mode, type="App",
                                                                                                 update_date=datetime.now())
        if pay_status == 'success':
            from ecom.views import get_order_details_remaining_amount
            repaymet_dict = get_order_details_remaining_amount(user_id)
            if repaymet_dict:
                if "Amount" in repaymet_dict:
                    if repaymet_dict:
                        amount_val = repaymet_dict["Amount"]
                        if payment[0].amount:
                            if "Data_dict" in repaymet_dict:
                                order_value = repaymet_dict["Data_dict"]
                                if order_value:
                                    for order_text, amount in order_value.items():
                                        if amount > 0:
                                            ORDER_ID = str(order_text) + "e" + str(payment[0].order_id)

                                            paym = Payment.objects.create(order_id=ORDER_ID, status=pay_status,
                                                                          category="Repayment", amount=amount,
                                                                          type='App',
                                                                          product_type="Ecom",
                                                                          date=payment[0].date,
                                                                          return_status="rstatus", platform_type="App",
                                                                          pay_source="Rpay")

                                            if paym and paym is not None:
                                                order_details = OrderDetails.objects.filter(order_text=order_text)
                                                if order_details:
                                                    order_details[0].payment.add(paym)

                                            # check credit
                                            credit_status = "repayment"
                                            import time
                                            sleep_time = random.random()
                                            time.sleep(sleep_time)
                                            sleep_time = random.random()
                                            time.sleep(sleep_time)
                                            order_details_obj = OrderDetails.objects.filter(order_text=order_text)
                                            user_id = order_details_obj[0].user.id
                                            if order_details_obj:
                                                get_credit_repayment = order_details_obj[0].credit_history.filter(
                                                    status=credit_status, repayment_add=amount,
                                                    created_date__startswith=str(datetime.now())[:16])
                                                try:
                                                    from Recovery.views import create_recovery_incentive_entry

                                                    create_recovery_incentive_entry(order_details_obj[0].id)
                                                except:
                                                    pass

                                                if get_credit_repayment:
                                                    pass

                                                else:
                                                    # No credit entry, so credit not updated
                                                    # Updating User credit balance..
                                                    update_credit = credit_balance_update(user_id, amount,
                                                                                          credit_status)

                                                    if update_credit:
                                                        # Adding new credit entry in Credit history of Order Details Table..
                                                        order_details_obj[0].credit_history.add(update_credit)

                try:
                    number = order_details_obj[0].user.username
                    osms_kwrgs = {
                        'sms_type': "repayment",
                        'number': str(number)
                    }
                    order_sms(**osms_kwrgs)
                except Exception as e:
                    print("error in repayment")

                status = '1'
                message = 'Transaction successful.'
        elif pay_status == 'failed':

            status = '2'
            message = 'Transaction failed'

        elif pay_status == 'pending':

            status = '3'
            message = 'Transaction pending'

        return Response(
            {
                'status': status,
                'message': message
            }
        )


def ChecksumGenerateReturnDeliveryAmountPhonePe(user_id, return_id, use_wallet):
    """
        Paytm Order Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"
    """
    status = None
    message = None
    salt = None
    return_dict = {}
    try:

        get_return_order_obj = ReturnOrder.objects.filter(return_id=return_id)
        from .order import get_orders_delivery_charge
        get_delivery_amount = get_orders_delivery_charge(get_return_order_obj[0].order.id)
        random_number = random.sample(range(99999), 1)
        ORDER_ID = str(get_return_order_obj[0].return_id) + 'e' + str(random_number[0])
        shopping_order_id = get_return_order_obj[0].order.order_id
        pay_amount = 0

        callback_url = callback_base_url + "ecomapi/callback_return_delivery_razorpay/"

        CUST_ID = ""
        user = None
        if user_id:
            user = User.objects.filter(id=user_id)
            if user:
                CUST_ID = user[0].username

        if use_wallet == "True":
            from EApi.order import get_user_wallet_amount, get_wallet_payable_amount
            wallet_dict = get_user_wallet_amount(user_id)
            wallet_amount = 0
            if wallet_dict:
                wallet_amount = wallet_dict["balance_amount"]

            data_dict = get_wallet_payable_amount(wallet_amount, get_delivery_amount)
            if data_dict:
                used_wallet = data_dict['used_wallet']
                pay_amount = data_dict['pay_amount']

                if used_wallet != 0:
                    wallet_status = ""
                    payment_status = ""
                    if pay_amount > 0:
                        # This means user is using his wallet to pay delivery charges..
                        # Removing wallet balance and adding wallet entry..
                        wallet_status = "return_delivery_charge_initiated"
                        payment_status = "initiated"
                    else:
                        wallet_status = "return_delivery_charge_success"
                        payment_status = "success"

                    update_wallet = user_wallet_sub(user_id, used_wallet, None, get_return_order_obj[0].order,
                                                    wallet_status)

                    initiate_payment = Payment.objects.create(order_id=str(ORDER_ID),
                                                              amount=float(used_wallet),
                                                              date=datetime.now(),
                                                              category='Order_Return', type="Ecom_Wallet", mode="PPI",
                                                              product_type="Ecom", status=payment_status)

                    if initiate_payment and initiate_payment is not None:
                        get_order_details_obj = OrderDetails.objects.filter(order_id=get_return_order_obj[0].order.id)
                        if get_order_details_obj:
                            get_order_details_obj[0].payment.add(initiate_payment)

                    if payment_status == "success":
                        # Update order status in order table..
                        order_id = get_return_order_obj[0].order.id
                        get_order_obj = Order.objects.filter(id=order_id)
                        if get_order_obj:
                            update_order = Order.objects.filter(id=order_id).update(status="return",
                                                                                    updated_date=datetime.now())

                            update_order_status = OrderStatus.objects.create(status="return_request",
                                                                             order=get_order_obj[0])

                        # Update ReturnOrder table status for particular order..
                        update_return_order = ReturnOrder.objects.filter(id=get_return_order_obj[0].id)
                        if update_return_order:
                            update_return_order[0].verify_return_status = "return_request"
                            update_return_order[0].payment_status = "success"
                            update_return_order[0].updated_date = datetime.now()
                            update_return_order[0].save()

                        status = 1
                        return_dict = {"status": "1", "message": "Return request accepted successfully"}
                        try:
                            number = user[0].username
                            osms_kwrgs = {
                                'sms_type': "return",
                                'number': str(number)
                            }
                            order_sms(**osms_kwrgs)
                        except Exception as e:
                            print("error in order return message")

                        return return_dict

                    if payment_status == "initiated":

                        amount = float(pay_amount) * 100
                        data = None

                        tracking_url = website_base_url + "ordertracking/phonepe_order_track?order_id=" + str(
                            shopping_order_id)
                        content = {"orderContext": {
                            "trackingInfo": {"type": "HTTPS", "url": tracking_url}}}
                        content_data = json.dumps(content)

                        data = {'amount': amount, 'currency': 'INR', 'receipt': ORDER_ID, 'payment_capture': '1',
                                "phonepe_switch_context": content_data}
                        data = json.dumps(data)

                        headers = {
                            'Content-Type': 'application/json',
                        }
                        url = "https://api.razorpay.com/v1/orders"
                        response = requests.request(method="POST", url=url, headers=headers, auth=(Key_id, Key_secret),
                                                    data=data)
                        order = response.json()

                        personal = Personal.objects.filter(user_id=user_id).order_by('-id').first()
                        name = None
                        dec = None
                        email_id = None
                        mobile_no = None
                        resp_data = None
                        real_amount = order['amount'] / 100
                        if personal:
                            name = personal.name
                            dec = personal.name
                            email_id = personal.email_id
                            mobile_no = personal.user.username

                        if order:
                            resp_data = {
                                'order_id': order['id'],
                                'amount': amount,
                                'show_amount': real_amount,
                                'status': order['status'],
                                'created_at': order['created_at'],
                                'currency': order['currency'],
                                'image_url': '',
                                'key_id': Key_id,
                                'name': name,
                                'dec': dec,
                                'callbackurl': callback_url,
                                'order_id_ck': ORDER_ID,
                                'email_id': email_id,
                                'mobile_no': mobile_no,
                            }

                            timestamp = order['created_at']
                            try:
                                timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
                            except Exception as e:
                                print(e)
                                pass

                        if order:
                            payment_add = Payment.objects.create(status='initiated',
                                                                 pg_transaction_id=resp_data['order_id'],
                                                                 order_id=str(ORDER_ID), amount=float(real_amount),
                                                                 category='Order_Return',
                                                                 product_type="Ecom", date=datetime.now())

                            if initiate_payment and initiate_payment is not None:
                                get_order_details_obj = OrderDetails.objects.filter(
                                    order_id=get_return_order_obj[0].order.id)
                                if get_order_details_obj:
                                    get_order_details_obj[0].payment.add(payment_add)

                            return_dict = {"status": "2", "message": "Amount to pay", "data": resp_data}
                            return return_dict

                        else:
                            return_dict = {"status": "4", "message": "Something went wrong"}
                            return return_dict

        else:
            pay_amount = get_delivery_amount

        if pay_amount != 0:
            amount = float(pay_amount) * 100
            data = None
            tracking_url = website_base_url + "ordertracking/phonepe_order_track?order_id=" + str(
                shopping_order_id)
            content = {"orderContext": {
                "trackingInfo": {"type": "HTTPS", "url": tracking_url}}}
            content_data = json.dumps(content)

            data = {'amount': amount, 'currency': 'INR', 'receipt': ORDER_ID, 'payment_capture': '1',
                    "phonepe_switch_context": content_data}
            data = json.dumps(data)

            headers = {
                'Content-Type': 'application/json',
            }
            url = "https://api.razorpay.com/v1/orders"
            response = requests.request(method="POST", url=url, headers=headers, auth=(Key_id, Key_secret),
                                        data=data)
            order = response.json()

            if order:
                payment_add = Payment.objects.create(status='initiated',
                                                     pg_transaction_id=order['id'],
                                                     order_id=str(ORDER_ID), amount=float(pay_amount),
                                                     category='Order_Return',
                                                     product_type="Ecom", date=datetime.now())

                # After the payment objects creation, it is needed to add this payment to respected order ID's
                # entry in orders table.
                if payment_add and payment_add is not None:
                    get_order_details_obj = OrderDetails.objects.filter(order_id=get_return_order_obj[0].order.id)
                    if get_order_details_obj:
                        get_order_details_obj[0].payment.add(payment_add)

                personal = Personal.objects.filter(user_id=user_id).order_by('-id').first()
                name = None
                dec = None
                email_id = None
                mobile_no = None
                resp_data = None
                real_amount = order['amount'] / 100
                if personal:
                    name = personal.name
                    dec = personal.name
                    email_id = personal.email_id
                    mobile_no = personal.user.username

                if order:
                    resp_data = {
                        'order_id': order['id'],
                        'amount': amount,
                        'show_amount': real_amount,
                        'status': order['status'],
                        'created_at': order['created_at'],
                        'currency': order['currency'],
                        'image_url': '',
                        'key_id': Key_id,
                        'name': name,
                        'dec': dec,
                        'callbackurl': callback_url,
                        'order_id_ck': ORDER_ID,
                        'email_id': email_id,
                        'mobile_no': mobile_no,
                    }

                return_dict = {"status": "3", "message": "Amount to pay", "data": resp_data}
                return return_dict

            else:
                return_dict = {"status": "4", "message": "Something went wrong"}
                return return_dict

    except Exception as e:
        print("-----Exception----in checksum web", e)
        import sys
        import os
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        return return_dict


@method_decorator(csrf_exempt, name='dispatch')
class CallbackReturnDeliveryRazorpay(APIView):
    def post(self, request, *args, **kwargs):
        redirect_url = website_base_url + "account/process_payment?order_id="
        data = request.data
        razorpay_payment_id = None
        razorpay_order_id = None
        razorpay_signature = None
        if "razorpay_payment_id" in data:
            razorpay_payment_id = data["razorpay_payment_id"]
        if "razorpay_order_id" in data:
            razorpay_order_id = data["razorpay_order_id"]
        if "razorpay_signature" in data:
            razorpay_signature = data["razorpay_signature"]
        if None not in (razorpay_payment_id, razorpay_order_id, razorpay_signature):
            payment = Payment.objects.filter(pg_transaction_id=razorpay_order_id)
            if payment:
                order_id = str(payment[0].order_id)
                amount = payment[0].amount
                data = {
                    "order_id": order_id,
                    "razorpay_payment_id": razorpay_payment_id,
                    "razorpay_order_id": razorpay_order_id,
                    "razorpay_signature": razorpay_signature,
                    "pay_order_id": payment[0].order_id,
                    "repay_amount": amount
                }
                if order_id is not None:
                    Return_Order_Delivery_Charge_Status_Check(data)
                    redirect_with_oid = redirect_url + str(payment[0].order_id)
                    return redirect(redirect_with_oid)
        return redirect(website_base_url + "account/payment")


def Return_Order_Delivery_Charge_Status_Check(data):
    """
        description : Paytm Order Checksum API for exchange product
        :param
            {
                "order_id":"",
            }

    """
    status = None
    message = None
    amount = 0.0
    date_strip = None
    pay_status = None
    mode = None
    razorpay_payment_id = data['razorpay_payment_id']
    order_id = data["order_id"]
    razorpay_order_id = data['razorpay_order_id']
    razorpay_signature = data['razorpay_signature']
    amount = data['repay_amount']
    try:
        if order_id:
            if 'e' in order_id:
                o_id = order_id
                order_data = str(order_id).split("e")
                return_id = ''
                if order_data:
                    return_id = order_data[0]

                client = razorpay.Client(auth=(Key_id, Key_secret))
                msg = "{}|{}".format(str(data['razorpay_order_id']), str(data['razorpay_payment_id']))

                secret = str(Key_secret)

                key = bytes(secret, 'utf-8')
                body = bytes(msg, 'utf-8')

                dig = hmac.new(key=key,
                               msg=body,
                               digestmod=hashlib.sha256)

                generated_signature = dig.hexdigest()
                result = hmac.compare_digest(generated_signature, razorpay_signature)
                if result == True:
                    pass

                order_resp_data = order_status(razorpay_order_id)
                if order_resp_data and order_resp_data is not None:
                    status = 2
                    message = " Sign Verification Failed"

                payment_data = payment_status(razorpay_payment_id)
                if payment_data and payment_data is not None:
                    if payment_data['method'] == 'card':
                        mode = 'DC'
                    elif payment_data['method'] == 'upi':
                        mode = 'UPI'
                    elif payment_data['method'] == 'netbanking':
                        mode = 'NB'
                    else:
                        mode = 'RWT'
                    timestamp = payment_data['created_at']
                    try:
                        timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')

                    except Exception as e:
                        print(e)
                        pass

                    if payment_data['status'] == 'captured':
                        pay_status = 'success'
                        status = '1'
                        message = 'Transaction successful.'

                    elif payment_data['status'] == 'authorized':
                        pay_status = 'pending'

                        status = '3'
                        message = 'Transaction pending'

                    else:
                        pay_status = 'failed'
                        status = '2'
                        message = 'Transaction failed'

                    payment_obj = Payment.objects.filter(order_id=o_id, category='Order_Return').exclude(
                        type="Ecom_Wallet")

                    if payment_obj:
                        payment_obj[0].status = pay_status
                        payment_obj[0].transaction_id = data["razorpay_payment_id"]
                        payment_obj[0].mode = mode
                        payment_obj[0].type = "App"
                        payment_obj[0].update_date = datetime.now()
                        payment_obj[0].save()

                        try:
                            if pay_status == 'failed':
                                mobile_number = None
                                order_payment = payment_obj.orderdetails_payment.all()
                                if order_payment:
                                    if order_payment[0].user:
                                        mobile_number = order_payment[0].user.username

                                if mobile_number:
                                    failed_transaction_sms(mobile_number, payment_obj.amount)
                        except Exception as e:
                            print('-----------in exception----------', e.args)

                        get_return_order_obj = ReturnOrder.objects.filter(return_id=return_id)
                        if get_return_order_obj:
                            if pay_status == 'success':
                                # Here the actual return request order is created
                                update_return_order = ReturnOrder.objects.filter(return_id=return_id,
                                                                                 verify_return_status="return_request",
                                                                                 payment_status="not_paid")

                                if update_return_order:
                                    update_return_order[0].payment_status = "success"
                                    update_return_order[0].save()

                                # update status in Order table
                                order_obj = Order.objects.filter(id=get_return_order_obj[0].order.id)
                                if order_obj:
                                    order_obj[0].status = "return"
                                    order_obj[0].updated_date = datetime.now()
                                    order_obj[0].save()

                                # Update status in OrderStatus table
                                add_order_status = OrderStatus.objects.create(order=get_return_order_obj[0].order,
                                                                              status="return_request",
                                                                              date=datetime.now())

                                # Update wallet payment if available..
                                get_wallet_payment = Payment.objects.filter(order_id=o_id,
                                                                            category='Order_Return', mode="PPI",
                                                                            type="Ecom_Wallet")

                                if get_wallet_payment:
                                    get_payment_obj = Payment.objects.filter(id=get_wallet_payment[0].id)
                                    if get_payment_obj:
                                        get_payment_obj[0].status = pay_status
                                        get_payment_obj[0].update_date = datetime.now()
                                        get_payment_obj[0].save()

                                # Update status of wallet used in Return Order..
                                get_wallet_obj = UserWallet.objects.filter(order=get_return_order_obj[0].order.id,
                                                                           status="return_delivery_charge_initiated").order_by(
                                    'id').last()

                                if get_wallet_obj:
                                    get_wallet_obj.status = "return_delivery_charge_success"
                                    get_wallet_obj.updated_date = datetime.now()
                                    get_wallet_obj.save()

                                get_order_details_obj = OrderDetails.objects.filter(
                                    order_id=get_return_order_obj[0].order.id)

                                try:
                                    number = get_order_details_obj[0].user.username
                                    osms_kwrgs = {
                                        'sms_type': "return",
                                        'number': str(number)
                                    }
                                    order_sms(**osms_kwrgs)
                                except Exception as e:
                                    print("error in Order return message")

                                status = '1'
                                message = 'Transaction successful.'

                            elif pay_status == 'failed':

                                status = '2'
                                message = 'Transaction failed'

                            elif pay_status == 'pending':

                                status = '3'
                                message = 'Transaction pending'

                        return Response(
                            {
                                'status': status,
                                'message': message
                            }
                        )

    except Exception as e:
        import sys
        import os
        print('-----------in exception----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        return Response(
            {
                'status': status,
                'message': message
            }
        )
