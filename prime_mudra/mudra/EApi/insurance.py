import json
import requests
from datetime import datetime

from django.apps.registry import apps
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authentication import *
import time

Personal = apps.get_model('User', 'Personal')
UserInsurance = apps.get_model('insurance', 'UserInsurance')
Insurance = apps.get_model('insurance', 'Insurance')

# Zopper Insurance credentials..
client_id_key = "P1000002609002"
client_secret_key = "Bij4Mj5jOEMkjl5J2Nk94JE8V2nkoW6U"

# Testing credentials..

# token_generation_url = "https://foundation-uat.theblackswan.in/token-generation-api-url"
# create_user_url = "https://foundation-uat.theblackswan.in/sso-url"
# user_policy_url = "https://cms-uat.theblackswan.in/policy/business/user-policy"

# Production credentials..

token_generation_url = "https://foundation.theblackswan.in/token-generation-api-url"
create_user_url = "https://foundation.theblackswan.in/sso-url"
user_policy_url = "https://cms.theblackswan.in/policy/business/user-policy"


def auth_token():
    client_id = client_id_key
    client_secret = client_secret_key
    url = token_generation_url
    headers = {
        'Content-Type': 'application/json'
    }
    payload = {"client_id": client_id, "client_secret": client_secret}
    response = requests.request("POST", url, headers=headers, data=json.dumps(payload))
    if response.status_code == 200:
        return response.text

    else:
        return " "


class CreateUserAPIView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        """
        name = "Rohan Gupta"
            phone = "9090875467"
            email = "ra@gmail.com"
            user_id = "user_1618558742262452"
        """
        try:
            status = 0
            message = None
            url = None
            status = 0
            auth_tok = json.loads(auth_token())
            if auth_tok != None:
                Authorization = "Token " + str(auth_tok["authtoken"])
                user_id = request.user.id
                phone = None
                name = None
                email = None
                phone = request.user.username
                user_insurance_id = None
                insurance_user_id = None
                userin = UserInsurance.objects.filter(user_id=user_id).order_by('-id')
                if userin:
                    if userin[0].user_insurance_id:
                        user_insurance_id = userin[0].user_insurance_id

                    personal = Personal.objects.filter(user_id=user_id).order_by('-id')
                    if personal:
                        name = personal[0].name
                        email = personal[0].email_id
                        if None not in (name, email):
                            status = 1
                        else:
                            status = 2

                else:
                    personal = Personal.objects.filter(user_id=user_id).order_by('-id')
                    if personal:
                        name = personal[0].name
                        email = personal[0].email_id

                        if None not in (name, email):
                            create_insurance = UserInsurance.objects.create(user_id=user_id)
                            if create_insurance:
                                create_insurance.user_personal.add(personal[0])
                            status = 1

                        else:
                            status = 2

                    else:
                        status = 2

                if status == 1:
                    url = create_user_url
                    headers = {
                        'Content-Type': 'application/json'
                    }
                    if user_insurance_id:
                        payload = {"Authorization": Authorization, "businessname": "mudrakwik", "name": name,
                                   "phone": phone, "email": email, "user_id": user_insurance_id}
                    else:
                        payload = {"Authorization": Authorization, "businessname": "mudrakwik", "name": name,
                                   "phone": phone, "email": email}

                    response = requests.request("POST", url, headers=headers, data=json.dumps(payload))

                    if response.status_code == 200:
                        status = 1
                        message = "Success"

                        print("reqponse", response, response.text)
                        try:
                            json_data = json.loads(response.text)
                            if json_data:
                                if "data" in json_data:
                                    data = json_data["data"]
                                    if data:
                                        url = None
                                        if "url" in data:
                                            url = data["url"]
                                        if "user_id" in data:
                                            insurance_user_id = data["user_id"]

                                        if insurance_user_id:
                                            insurance_obj = UserInsurance.objects.filter(user_id=user_id).order_by(
                                                '-id')
                                            print("insurance_obj", insurance_obj)
                                            if not insurance_obj[0].user_insurance_id or insurance_obj[
                                                0].user_insurance_id == "":
                                                UserInsurance.objects.filter(user_id=user_id).update(
                                                    user_insurance_id=insurance_user_id,
                                                    updated_by_id=user_id,
                                                    updated_date=datetime.now())

                                    else:
                                        status = 1
                                        message = "Data is not valid"

                        except Exception as e:
                            print("-------User Personal Details API-------", e.args)
                            print(e.args)
                            import os
                            import sys
                            exc_type, exc_obj, exc_tb = sys.exc_info()
                            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                            print(exc_type, fname, exc_tb.tb_lineno)
                            status = 2
                            message = response.text

                    else:
                        status = 2
                        message = response.text

            return Response({
                'status': status,
                'message': message,
                'url': url
            })

        except Exception as e:
            print("-------Inside Exception of create User API-------", e.args)
            import os
            import sys
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': 10,
                    'message': 'Something went wrong!'
                }
            )

    def post(self, request, *args, **kwargs):
        user_id = request.user.id
        name = None
        email = None
        mobile_number = None
        if "name" in request.data:
            name = request.data.get('name')
        if "email" in request.data:
            email = request.data.get('email')
        if "phone" in request.data:
            mobile_number = request.data.get("phone")

        if None not in (name, email):
            personal = Personal.objects.create(user_id=user_id, name=name, email_id=email, mobile_no=mobile_number)
            return Response({
                'status': 1,
                'message': "successfuly ......"
            })

        else:
            return Response({
                'status': 2,
                'message': "Parameters are missing"
            })


def get_policy_data(user_id):
    auth_tok = json.loads(auth_token())
    if auth_tok != None:
        Authorization = "Token " + str(auth_tok["authtoken"])
        user_insurance_id = None
        userin = UserInsurance.objects.filter(user_id=user_id).order_by('-id')
        if userin:
            if userin[0].user_insurance_id != None:
                user_insurance_id = userin[0].user_insurance_id

        url = user_policy_url
        headers = {
            'Content-Type': 'application/json'
        }

        payload = {"Authorization": Authorization, "businessname": "mudrakwik", "user_id": user_insurance_id}
        response = requests.request("POST", url, headers=headers, data=json.dumps(payload))
        print("response", response)
        if response.status_code == 200:
            try:
                json_data = json.loads(response.text)
                print("json_data", json_data)
                if json_data["status"] == 200:
                    if "data" in json_data:
                        data = json_data["data"]
                        if data:
                            for policy in data:
                                insurance_name = ""
                                if "insurer_name" in policy:
                                    insurance_name = policy["insurer_name"]

                                policy_taken_date = policy["policy_taken_date"]
                                policy_expiry_date = policy["policy_expiry_date"]
                                try:
                                    if policy_taken_date is not None and policy_taken_date != "":
                                        conv = time.strptime(policy_taken_date, "%d/%m/%Y")
                                        policy_taken_date = time.strftime("%Y-%m-%d", conv)
                                        print("policy_taken_date", policy_taken_date)

                                    if policy_expiry_date is not None and policy_expiry_date != "":
                                        conv = time.strptime(policy_expiry_date, "%d/%m/%Y")
                                        policy_expiry_date = time.strftime("%Y-%m-%d", conv)
                                        print("policy_expiry_date", policy_expiry_date)
                                except Exception as e:
                                    print("Inside exception of time conversion..", e.args)

                                if "policy_number" in policy:
                                    insurance = Insurance.objects.filter(user_insurance=userin[0],
                                                                         policy_number=policy["policy_number"])
                                    if not insurance:
                                        Insurance.objects.create(user_insurance=userin[0], created_by_id=user_id,
                                                                 premium_expiry_date=policy["policy_expiry_date"],
                                                                 premium_taken_date=policy["policy_taken_date"],
                                                                 insurance_name=insurance_name,
                                                                 type=policy["type_of_insurance"],
                                                                 sub_type=policy["sub_type_of_insurance"],
                                                                 amount=policy["insurance_amount"],
                                                                 policy_name=policy["insurer_name"],
                                                                 policy_number=policy["policy_number"],
                                                                 pdf_url=policy["pdf_url"],
                                                                 payment_status=policy["payment_status"],
                                                                 updated_by_id=user_id, updated_date=datetime.now())

                                    else:
                                        # print("Inside Else==================")
                                        update_insurance = Insurance.objects.filter(id=insurance[0].id).update(
                                            premium_expiry_date=policy_expiry_date,
                                            premium_taken_date=policy_taken_date,
                                            type=policy["type_of_insurance"],
                                            sub_type=policy["sub_type_of_insurance"],
                                            amount=policy["insurance_amount"],
                                            policy_name=policy["insurer_name"],
                                            policy_number=policy["policy_number"],
                                            pdf_url=policy["pdf_url"],
                                            payment_status=policy["payment_status"],
                                            updated_by_id=user_id, updated_date=datetime.now())

                                        # print("update_insurance", update_insurance)
            except Exception as e:
                import sys
                import os
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                print("Inside exception of get_policy_data", e.args)
        return True


class InsuranceAPIView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        current_date = str(datetime.now())[:10]
        user_id = request.user.id
        list1 = []
        try:
            get_policy_data(user_id)
            insurance = Insurance.objects.filter(user_insurance__user_id=user_id)

            if insurance:
                for i in insurance:
                    is_active = None
                    expiry_date = None
                    start_date = None
                    if i.premium_expiry_date:
                        expiry_date = str(i.premium_expiry_date)[:10]
                    if i.premium_taken_date:
                        start_date = str(i.premium_taken_date)[:10]

                    if None not in (start_date, expiry_date):
                        if start_date <= current_date <= expiry_date:
                            is_active = True
                        else:
                            is_active = False

                    data = {'premium_expiry_date': str(i.premium_expiry_date)[:10],
                            'premium_taken_date': str(i.premium_taken_date)[:10],
                            'type': i.type,
                            'is_active': is_active,
                            'sub_type': i.sub_type,
                            'amount': i.amount,
                            'policy_name': i.policy_name,
                            'policy_number': i.policy_number,
                            'payment_status': i.payment_status,
                            'pdf_url': i.pdf_url}

                    list1.append(data)
                return Response(
                    {
                        'status': 1,
                        'message': "success",
                        'data': list1

                    }
                )
            else:
                return Response(
                    {
                        'status': 9,
                        'message': 'No Details Found',

                    }
                )

        except Exception as e:
            import sys
            import os
            print("-------User Personal Details API-------", e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': 10,
                    'message': 'Something went wrong!'
                }
            )
