from django.shortcuts import render

# Create your views here.
import datetime
import logging
import random
from ecom.views import image_thumbnail
from User.models import *
from django.apps import apps
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Sum
from django.http import HttpResponse
from django.shortcuts import render
from rest_framework import generics
from rest_framework.authentication import *
from rest_framework.permissions import *
from rest_framework.response import Response
from rest_framework.views import APIView

Category = apps.get_model('ecom', 'Category')
SubCategory = apps.get_model('ecom', 'SubCategory')
Colour = apps.get_model('ecom', 'Colour')
Size = apps.get_model('ecom', 'Size')
SGST = apps.get_model('ecom', 'SGST')
CGST = apps.get_model('ecom', 'CGST')
Product = apps.get_model('ecom', 'Product')
ProductDetails = apps.get_model('ecom', 'ProductDetails')
Seller = apps.get_model('ecom', 'Seller')
Credit = apps.get_model('ecom', 'Credit')
Stock = apps.get_model('ecom', 'Stock')
Order = apps.get_model('ecom', 'Order')
OrderDetails = apps.get_model('ecom', 'OrderDetails')
OrderDetailsOrder = apps.get_model('ecom', 'orderdetails_order_id')
Images = apps.get_model('ecom', 'Images')
EcomDocuments = apps.get_model('ecom', 'EcomDocuments')
Cart = apps.get_model('ecom', 'Cart')


class CartProductAddApi(generics.ListAPIView):
    """
        This APi is to add products to the cart for an user.

        method = post
        parameters = {   User Token,
                        'product_details_id': 12,
                        'quantity': 3
                    }
    """

    def post(self, request, *args, **kwargs):
        try:
            date = datetime.datetime.now()
            product_details_id = request.data.get('product_details_id')
            quantity = request.data.get('quantity')
            user_id = 176
            user = User.objects.filter(id=user_id)
            if user:
                if not quantity or quantity is None:
                    quantity = 1
                product = ProductDetails.objects.filter(id=product_details_id)
                if product:
                    amount = product[0].amount
                    total_amount = quantity * amount
                    # This is to add the product to the cart..
                    add_cart = Cart.objects.create(product_details=product[0], quantity=quantity,
                                                   amount=amount, total_amount=total_amount,
                                                   date=date, status="True", user=user[0])
            return "OKay"
        except Exception as e:
            print('============', e)


class CartProductRemoveApi(generics.ListAPIView):
    """
        This APi is to Remove products from the cart of a User.

        method = post
        parameters = {   User Token,
                        'cart_id' : 1
                    }
    """

    def post(self, request, *args, **kwargs):
        try:
            user = request.data.get('user')
            cart_id = request.data.get('cart_id')
            user = User.objects.filter(id=user)
            if user:
                cart_obj = Cart.objects.filter(user=user[0], id=cart_id)
                if cart_obj:
                    cart_obj.delete()
        except Exception as e:
            print('============', e)


class CartProductEditApi(generics.ListAPIView):
    """
        This APi is to Edit products to the cart for an user.

        method = post
        parameters = {   User Token,
                        'cart_id': 6
                        'product_details_id': 12,
                        'quantity': 3
                    }
    """

    def get(self, request, *args, **kwargs):
        try:
            date = datetime.datetime.now()
            user = request.data.get('user')
            product_details_id = request.data.get('product_details_id')
            quantity = request.data.get('quantity')
            cart_id = request.data.get('cart_id')
            user = User.objects.filter(id=user)
            if user:
                if not quantity or quantity is None:
                    quantity = 1
                product = ProductDetails.objects.filter(id=product_details_id)
                if product:
                    amount = product[0].amount
                    total_amount = quantity * amount
                    # This is to add the product to the cart..
                    update_cart = Cart.objects.filter(id=cart_id).update(
                                                                    product_details=product[0],
                                                                    quantity=quantity,
                                                                    amount=amount, total_amount=total_amount,
                                                                    date=date, status=True, user=user[0],
                                                                    updated_date=datetime.datetime.now())

        except Exception as e:
            print('============', e)


class UserCartDetailsApi(generics.ListAPIView):
    """
        This APi is to Show User Cart details.

        method = post
        parameters = User Token
        Response: {
                    status: 1,
                    cart_list:  [
                                {
                                    'cart_id': 1,
                                    'product_name': "Mens Watch",
                                    'category': "Electronics",
                                    'sub_category': "",
                                    'weight': "50",
                                    'height': "",
                                    'length': "",
                                    'width': "",
                                    'size': "",
                                    'colour': "Red",
                                    'amount': 2500,
                                    'total_amount': 2500,
                                    'quantity': 1,
                                    'product_image': image
                                }
                            ]
                        }
        Empty Cart Response: {
                                'status': 2,
                                cart_list: []
                            }
    """

    def get(self, request, *args, **kwargs):
        try:
            cart_list = []
            # user_id = request.data.get('user')
            user_id = 176
            user = User.objects.filter(id=user_id)
            if user:
                cart_products = Cart.objects.filter(user=user[0])
                if cart_products and cart_products is not None:
                    for cart in cart_products:
                        name = None
                        category = None
                        sub_category = None
                        size = None
                        colour = None
                        length = None
                        weight = None
                        height = None
                        width = None
                        image = None
                        if cart:
                            if cart.product_details.product_id:
                                name = cart.product_details.product_id.name
                                category = cart.product_details.product_id.category.name
                                sub_category = cart.product_details.product_id.sub_category.name
                            if cart.product_details.size_id:
                                size = cart.product_details.size_id.name
                            if cart.product_details.colour_id:
                                colour = cart.product_details.colour_id.name
                            weight = cart.product_details.weight
                            height = cart.product_details.height
                            length = cart.product_details.Length
                            width = cart.product_details.width
                            product_image = Images.objects.filter(type="main", product_details_id=
                                                                        cart.product_details_id)
                            if product_image:
                                image = image_thumbnail(product_image[0].path)
                            product_dict = {'cart_id': cart.id,
                                            'product_name': name,
                                            'category': category,
                                            'sub_category': sub_category,
                                            'weight': weight,
                                            'height': height,
                                            'length': length,
                                            'width': width,
                                            'size': size,
                                            'colour': colour,
                                            'amount': cart.amount,
                                            'total_amount': cart.total_amount,
                                            'quantity': cart.quantity,
                                            'product_image': image
                                            }
                            cart_list.append(product_dict)
                    return Response(
                        {
                            'status': 1,
                            'cart_list': cart_list
                        }
                    )
                else:

                    return Response(
                        {
                            'status': 1,
                            'cart_list': cart_list
                        }
                    )

        except Exception as e:
            print('inside exception', e)






