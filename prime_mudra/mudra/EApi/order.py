import random
from datetime import datetime, timedelta

from MApi import botofile
from User.models import *
from django.apps import apps
from django.contrib.auth.models import User
from django.db.models import Sum
from django.http import HttpResponse
from django.shortcuts import render
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from ecom.ecom_payments import check_user_defaulter_status, get_users_due_order_and_amount
from ecom.views import generate_unique_id, update_stock, initiated_stock_add
from ecom.views import image_thumbnail
from rest_framework import generics
from rest_framework.authentication import *
from rest_framework.permissions import *
from rest_framework.response import Response
from rest_framework.views import APIView

from .EChecksum_web import ChecksumGenerateReturnDeliveryAmount, get_cashback_amount, get_big_offer_status, \
    get_big_offer_status_new, \
    get_order_details_cashback_amount, ChecksumGenerateReturnDeliveryAmountWebsite, get_orders_cashback_amount
from .credit import credit_balance_update, get_user_wallet_amount
from .ecom_email import order_sms
from .product import get_product_details_price, get_product_details_stock
from .wallet import user_wallet_update, user_wallet_sub
from .razorpay import ChecksumGenerateReturnDeliveryAmountPhonePe, get_zest_money_offer

Category = apps.get_model('ecom', 'Category')
SubCategory = apps.get_model('ecom', 'SubCategory')
Colour = apps.get_model('ecom', 'Colour')
Size = apps.get_model('ecom', 'Size')
SGST = apps.get_model('ecom', 'SGST')
CGST = apps.get_model('ecom', 'CGST')
Product = apps.get_model('ecom', 'Product')
ProductDetails = apps.get_model('ecom', 'ProductDetails')
Seller = apps.get_model('ecom', 'Seller')
Credit = apps.get_model('ecom', 'Credit')
Stock = apps.get_model('ecom', 'Stock')
Order = apps.get_model('ecom', 'Order')
OrderDetails = apps.get_model('ecom', 'OrderDetails')
OrderDetailsOrder = apps.get_model('ecom', 'orderdetails_order_id')
ReturnOrderCustomerReturnImage = apps.get_model('ecom', 'returnorder_customer_return_image')
ReturnOrderSellerReturnImage = apps.get_model('ecom', 'returnorder_seller_return_image')
Images = apps.get_model('ecom', 'Images')
EcomDocuments = apps.get_model('ecom', 'EcomDocuments')
Address = apps.get_model('User', 'Address')
DeliveryCharges = apps.get_model('ecom', 'DeliveryCharges')
Personal = apps.get_model('User', 'Personal')
OrderStatus = apps.get_model('ecom', 'OrderStatus')
ReturnOrder = apps.get_model('ecom', 'ReturnOrder')
SellerAmount = apps.get_model('ecom', 'SellerAmount')
SellerAccount = apps.get_model('ecom', 'SellerAccount')
ConvenientFees = apps.get_model('ecom', 'ConvenientFees')
Payment = apps.get_model('Payments', 'Payment')
Coupon = apps.get_model('ecom', 'Coupon')


class UserOrdersApi(generics.ListAPIView):
    """
    This Api is to Users all orders

    Url: ecomapi/user_orders

    Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User toke is required..TODO
    method = get

    Success Response:
        # This response is when user have his orders
            {
                'status': 1,
                'orders':
                [
                    {
                        'order_details_id': 1,
                        'order_text': 123456789,
                        'total_quantity': 5,
                        'total_amount': 5000.0,
                        'date': 'March 14, 2020, 9:44 a.m.',
                        'address': '',
                    },
                    {
                        'order_details_id': 2,
                        'order_text': 1234567234,
                        'total_quantity': 10,
                        'total_amount': 10000.0,
                        'date': 'March 116, 2020, 9:44 a.m.',
                        'address': 'chaitanyanagar',
                    }
                ]
            }

    No orders Response:
                {
                'status': 2,
                'orders': []
                }
            )

    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        user_id = request.user.id
        order_list = []
        user_orders = OrderDetails.objects.filter(user=user_id)
        if user_orders:
            status = 1
            for order in user_orders:
                order_dict = {'order_details_id': order.id,
                              'order_text': order.order_text,
                              'total_quantity': order.total_quantity,
                              'total_amount': order.total_amount,
                              'date': order.date,
                              'address': order.address.address}

                order_list.append(order_dict)
            return Response(
                {
                    'status': status,
                    'orders': order_list
                }
            )
        else:
            status = 2
            return Response(
                {
                    'status': 2,
                    'orders': order_list
                }
            )


class UserOrderDetailsApi(generics.ListAPIView):
    """
    This Api is to Users order details in details.

    Url: ecomapi/user_order_details

    Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..TODO
    method = post
    parameter = {
                    'order_details_id': 1
                }

    Response:

            {
                "status": 1,
                "orders": [
                    {
                        "order_id": 1,
                        "product_name": "Men watch",
                        "category": "Men",
                        "sub_category": "Watches",
                        "weight": 12.0,
                        "height": "16",
                        "length": "14",
                        "width": "16",
                        "size": "Large",
                        "colour": "Reds",
                        "amount": 100.0,
                        "total_amount": 200.0,
                        "quantity": 2
                    },
                    {
                        "order_id": 2,
                        "product_name": "Men watch",
                        "category": "Men",
                        "sub_category": "Watches",
                        "weight": 0.0,
                        "height": "",
                        "length": "",
                        "width": "",
                        "size": "Large",
                        "colour": "Reds",
                        "amount": 50.0,
                        "total_amount": 150.0,
                        "quantity": 3
                    }
                ]
            }

    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        order_list = []
        order_details_id = request.data.get('order_details_id')
        order_details_obj = OrderDetails.objects.filter(id=order_details_id)
        if order_details_obj:
            all_orders = OrderDetailsOrder.objects.filter(orderdetails=order_details_obj[0])
            if all_orders:
                for order in all_orders:
                    name = None
                    category = None
                    sub_category = None
                    size = None
                    colour = None
                    length = None
                    weight = None
                    height = None
                    width = None
                    product = Order.objects.filter(id=order.order_id)
                    if product:
                        if product[0].product_details.product_id:
                            name = product[0].product_details.product_id.name
                            category = product[0].product_details.product_id.base_category.sub_category.category.name
                            sub_category = product[0].product_details.product_id.base_category.sub_category.name
                        if product[0].product_details.size_id:
                            size = product[0].product_details.size_id.name
                        if product[0].product_details.colour_id:
                            colour = product[0].product_details.colour_id.name
                        weight = product[0].product_details.weight
                        height = product[0].product_details.height
                        length = product[0].product_details.Length
                        width = product[0].product_details.width
                        product_dict = {'order_id': product[0].id,
                                        'product_name': name,
                                        'category': category,
                                        'sub_category': sub_category,
                                        'weight': weight,
                                        'height': height,
                                        'length': length,
                                        'width': width,
                                        'size': size,
                                        'colour': colour,
                                        'amount': product[0].amount,
                                        'total_amount': product[0].total_amount,
                                        'quantity': product[0].quantity,
                                        'product_image': "image"
                                        }
                        order_list.append(product_dict)
            return Response(
                {
                    'status': 1,
                    'orders': order_list
                }
            )


def get_order_details_delivery_charge(order_details_id):
    """
    The function is to used return total order delivery charges. Input to the function order_details_id.
    first get all the orders include in order details exclude order status cancelled.
    """
    if order_details_id is not None:
        final_delivery_amount = 0
        get_all_orders = OrderDetails.objects.filter(id=order_details_id).values_list('order_id', flat=True)
        if get_all_orders is not None:
            all_orders = Order.objects.filter(id__in=get_all_orders).exclude(status__icontains="cancelled")
            if all_orders:
                # Get delivery charge object..
                for order in all_orders:
                    delivery_charge = 0
                    if order.delivery_charges:
                        delivery_charge = order.delivery_charges.amount
                    final_delivery_amount += delivery_charge

        return final_delivery_amount


def get_orders_delivery_charge(order_id):
    """
    The function is used to return order delivery charge amount.
    """
    if order_id is not None:
        final_delivery_amount = 0
        # Get delivery charge object..
        order = Order.objects.filter(id=order_id)
        if order:
            if order[0].delivery_charges:
                final_delivery_amount = order[0].delivery_charges.amount
        return final_delivery_amount


class OrderPlaceApi(generics.ListAPIView):
    """
    This Api is to place orders.

    Url: ecomapi/order-place

    Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..TODO
    method = post
    parameter = {
                    'orders': [
                                {
                                    'id': 2,
                                    'quantity': 2
                                },
                                {
                                    'id': 3,
                                    'quantity': 1
                                }
                            ],
                    'Address': "Shivajinagar Pune"
                }

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    # This is post API
    def post(self, request, *args, **kwargs):
        today_date = datetime.now()
        order_data = request.data

        # generating unique Order ID here..
        user_id = request.user.id
        user = User.objects.filter(id=user_id)
        unique_id = generate_unique_id()
        order_id = "ORD" + unique_id
        orders = order_data['orders']
        address_id = order_data['address_id']
        address_obj = Address.objects.filter(id=address_id)
        detail_amount = 0
        detail_total_amount = 0
        total_quantity = 0
        pay_amount = 0
        actual_used = 0
        final_delivery_amount = 0
        order_obj_list = []
        # Checking users credit limit

        # Updating Stocks of initiated orders..
        initiated_stock_update = initiated_stock_add()

        if orders:
            for order in orders:
                amount = 0
                # creating unique order Id.
                unique_id = generate_unique_id()
                unique_order_id = "UNI" + unique_id
                product_details_id = order['id']
                quantity = order['quantity']
                total_quantity = total_quantity + quantity

                product_details_obj = ProductDetails.objects.filter(id=product_details_id,
                                                                    status="APPROVED")

                # Checking available stock..
                product_stock = 0
                stock = get_product_details_stock(product_details_obj[0].id)
                if stock:
                    product_stock = stock
                if product_stock < quantity:
                    return Response(
                        {
                            'status': 2,
                            'message': str(product_details_obj[0].product_id.name) + " is out of stock."
                        }
                    )

                if product_details_obj:
                    amount = product_details_obj[0].amount
                    product_details_amount_obj = None
                    amount_data = get_product_details_price(product_details_obj[0].id)
                    if amount_data:
                        amount = amount_data['minimum_amount']
                        product_details_amount_obj = amount_data['product_details_amount_obj']
                    detail_amount += amount
                    total_amount = quantity * amount
                    detail_total_amount += total_amount

                    # Get delivery charge object..
                    delivery_charge = None
                    get_delivery_charge = DeliveryCharges.objects.filter(
                        product=product_details_obj[0].product_id.id).order_by('id').last()
                    if get_delivery_charge:
                        delivery_charge = get_delivery_charge
                        final_delivery_amount += delivery_charge.amount
                    else:
                        get_delivery_charge = DeliveryCharges.objects.filter(product=None).order_by('id').last()
                        if get_delivery_charge:
                            delivery_charge = get_delivery_charge
                            final_delivery_amount += delivery_charge.amount

                    order_obj = Order.objects.create(order_id=order_id, product_details=product_details_obj[0],
                                                     quantity=quantity, amount=amount, total_amount=total_amount,
                                                     date=today_date, re_status="True", status="initiated",
                                                     delivery_charges=delivery_charge,
                                                     order_details_amount=product_details_amount_obj,
                                                     unique_order_id=unique_order_id)

                    order_obj_list.append(order_obj)

                    # Here we change the status of Order_status table.
                    add_order_status = OrderStatus.objects.create(order=order_obj,
                                                                  date=datetime.now(),
                                                                  status="initiated")

                    # Here we add stock of Product Details..
                    stock_status = "remove"
                    change_by = "order"
                    stock_update = update_stock(product_details_obj[0].id, quantity, stock_status, change_by)

        credit = None
        initiated_credit = None
        credit_status = "Failed"
        default_status = check_user_defaulter_status(user[0].id)
        if default_status == "False":
            credit_obj = Credit.objects.filter(user=user[0].id).exclude(status="initiated").order_by('id').last()
            if credit_obj:
                if credit_obj.balance_credit != 0:
                    credit = credit_obj
                    credit_status = "Initiated"

        if credit is not None:
            # print('here$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
            payable_amount = detail_total_amount - credit.balance_credit
            # print('payable_amount', payable_amount)
            if payable_amount > 0:
                pay_amount = payable_amount
                actual_used = credit.balance_credit
            else:
                actual_used = detail_total_amount

            # updating credit of user
            total_used_credit = credit.used_credit + actual_used
            balance_credit = credit.credit - total_used_credit
            if balance_credit < 0:
                balance_credit = 0
            initiate_credit = Credit.objects.create(credit=credit.credit, used_credit=total_used_credit,
                                                    balance_credit=balance_credit, actual_used=actual_used,
                                                    user=user[0],
                                                    status="initiated")
            if initiate_credit:
                initiated_credit = initiate_credit
        else:
            pay_amount = detail_total_amount

        # Adding convenience fees..
        convenience_obj = None
        convenience_fees = 25
        get_convenient_obj = ConvenientFees.objects.filter(status="active").order_by('id').last()
        if get_convenient_obj:
            convenience_obj = get_convenient_obj
            convenience_fees = convenience_obj.amount

        order_detail_obj = OrderDetails.objects.create(order_text=order_id, credit=initiated_credit,
                                                       total_quantity=total_quantity,
                                                       total_amount=detail_total_amount,
                                                       pay_amount=pay_amount,
                                                       convenient_fees=convenience_obj,
                                                       date=today_date, re_status="True", address=address_obj[0],
                                                       credit_status=credit_status,
                                                       status="initiated", user=user[0])
        if order_detail_obj:
            if initiated_credit is not None:
                order_detail_obj.credit_history.add(initiated_credit)

        # Adding orders in Many to many field..
        order_detail_obj.order_id.add(*order_obj_list)

        # sending order placed sms to User
        sms_message = 'Dear User,\nYour Order ' + str(

            order_id) + ' is placed Successfully.'

        sms_kwrgs = {
            'sms_message': sms_message,
            'number': str(user[0].username)

        }

        # calculating final checkout amount..
        final_checkout_amount = pay_amount + final_delivery_amount + convenience_fees
        return Response(
            {
                'status': 1,
                'order_id': order_id,
                'amount': final_checkout_amount,
                'message': "Order placed Successfully"
            }
        )


def get_wallet_payable_amount(wallet_amount, amount):
    """
    The wallet amount and amount pass in get_wallet_payable_amount function.THis function return dictionary contain used wallet and pay amount.
    If wallet amount is greater than amount then pay amount is zero and used amount is amount.If not then subtract wallet amount from amount and get pay amount.
    """
    pay_amount = 0
    used_wallet = 0
    if wallet_amount != 0:
        # User has some amount in his wallet..
        if wallet_amount >= amount:
            # This means all payable amount is used from wallet balance no need to pay any amount..
            used_wallet = amount
            pay_amount = 0  # No need to pay anything..
        else:
            # Total payable amount is remaining payable amount and delivery and convenient fees..
            pay_amount = amount - wallet_amount
            used_wallet = wallet_amount
    else:
        # This means User have 0 amount in his wallet..
        # So all amount should have to pay..
        pay_amount = amount
    data_dict = {
        "used_wallet": used_wallet,
        "pay_amount": pay_amount
    }
    return data_dict


def initiate_normal_orders(order_dict):
    # print("Inside initiate_normal_orders===================================")
    today_date = datetime.now()
    unique_id = generate_unique_id()
    order_id = "ORD" + unique_id
    order_obj_list = []
    return_dict = {}
    total_quantity = 0
    final_delivery_amount = 0
    convenience_fees = 0
    detail_total_amount = 0
    if order_dict:
        for order in order_dict:
            amount = 0
            product_details_id = order['id']
            quantity = order['quantity']
            total_quantity = total_quantity + quantity

            product_details_obj = ProductDetails.objects.filter(id=product_details_id,
                                                                status="APPROVED")

            # Checking available stock..
            product_stock = 0
            stock = get_product_details_stock(product_details_obj[0].id)
            if stock:
                product_stock = stock
            if product_stock < quantity:
                return_dict["status"] = 2
                return_dict["message"] = str(product_details_obj[0].product_id.name) + " is out of stock."
                return return_dict

            if product_details_obj:
                amount = product_details_obj[0].amount
                product_details_amount_obj = None
                amount_data = get_product_details_price(product_details_obj[0].id)
                if amount_data:
                    amount = amount_data['minimum_amount']
                    product_details_amount_obj = amount_data['product_details_amount_obj']

                total_amount = quantity * amount
                detail_total_amount += total_amount

                # Get delivery charge object..
                delivery_charge = get_delivery_charge(total_amount)
                if delivery_charge:
                    final_delivery_amount += delivery_charge.amount

                convenient_entry = None
                # Get orders convenient fees..
                order_convenience_fees = get_orders_convenient_fees(total_amount)
                if order_convenience_fees != 0:
                    convenience_fees += order_convenience_fees
                    # Create Convenient fees entry..
                    convenient_entry = ConvenientFees.objects.create(amount=order_convenience_fees,
                                                                     order_type="order")

                # creating unique order Id.
                unique_id = generate_unique_id()
                unique_order_id = "UNI" + unique_id

                order_obj = Order.objects.create(order_id=order_id, product_details=product_details_obj[0],
                                                 quantity=quantity, amount=amount, total_amount=total_amount,
                                                 date=today_date, re_status="True", status="initiated",
                                                 delivery_charges=delivery_charge,
                                                 order_type="Paid",
                                                 convenient_fees=convenient_entry,
                                                 order_details_amount=product_details_amount_obj,
                                                 unique_order_id=unique_order_id)

                order_obj_list.append(order_obj)

                # Here we change the status of Order_status table.
                add_order_status = OrderStatus.objects.create(order=order_obj,
                                                              date=datetime.now(),
                                                              status="initiated")

                # Here we add stock of Product Details..
                stock_status = "remove"
                change_by = "order"
                stock_update = update_stock(product_details_obj[0].id, quantity, stock_status, change_by)

    return_dict["status"] = 1
    return_dict["message"] = "success"
    return_dict["order_id"] = order_id
    return_dict["total_quantity"] = total_quantity
    return_dict["final_delivery_amount"] = final_delivery_amount
    return_dict["convenience_fees"] = convenience_fees
    return_dict["detail_total_amount"] = detail_total_amount
    return_dict["order_obj_list"] = order_obj_list
    return return_dict


def initiate_scheme_orders(order_dict):
    today_date = datetime.now()
    unique_id = generate_unique_id()
    order_id = "ORD" + unique_id
    order_obj_list = []
    return_dict = {}
    total_quantity = 0
    final_delivery_amount = 0
    convenience_fees = 0
    detail_total_amount = 0
    if order_dict:
        paid_count = get_paid_count(len(order_dict))
        cnt = 0
        for order in order_dict:
            order_type = None
            cnt += 1
            amount = 0
            product_details_id = order['id']
            quantity = order['quantity']
            total_quantity = total_quantity + quantity

            product_details_obj = ProductDetails.objects.filter(id=product_details_id,
                                                                status="APPROVED")

            # Checking available stock..
            product_stock = 0
            stock = get_product_details_stock(product_details_obj[0].id)
            if stock:
                product_stock = stock
            if product_stock < quantity:
                return_dict["status"] = 2
                return_dict["message"] = str(product_details_obj[0].product_id.name) + " is out of stock."
                return return_dict

            if product_details_obj:
                amount = product_details_obj[0].amount
                product_details_amount_obj = None
                amount_data = get_product_details_price(product_details_obj[0].id)
                if amount_data:
                    amount = amount_data['minimum_amount']
                    product_details_amount_obj = amount_data['product_details_amount_obj']

                total_amount = quantity * amount
                if cnt <= paid_count:
                    detail_total_amount += total_amount
                    order_type = "Paid"
                else:
                    order_type = "Free"

                    # Get delivery charge object..
                delivery_charge = get_delivery_charge(total_amount)
                if delivery_charge:
                    final_delivery_amount += delivery_charge.amount

                convenient_entry = None
                # Get orders convenient fees..
                order_convenience_fees = get_orders_convenient_fees(total_amount)
                if order_convenience_fees != 0:
                    convenience_fees += order_convenience_fees
                    # Create Convenient fees entry..
                    convenient_entry = ConvenientFees.objects.create(amount=order_convenience_fees,
                                                                     order_type="order")

                # creating unique order Id.
                unique_id = generate_unique_id()
                unique_order_id = "UNI" + unique_id

                order_obj = Order.objects.create(order_id=order_id, product_details=product_details_obj[0],
                                                 quantity=quantity, amount=amount, total_amount=total_amount,
                                                 date=today_date, re_status="True", status="initiated",
                                                 delivery_charges=delivery_charge,
                                                 order_type=order_type,
                                                 convenient_fees=convenient_entry,
                                                 order_details_amount=product_details_amount_obj,
                                                 unique_order_id=unique_order_id)

                order_obj_list.append(order_obj)

                # Here we change the status of Order_status table.
                add_order_status = OrderStatus.objects.create(order=order_obj,
                                                              date=datetime.now(),
                                                              status="initiated")

                # Here we add stock of Product Details..
                stock_status = "remove"
                change_by = "order"
                stock_update = update_stock(product_details_obj[0].id, quantity, stock_status, change_by)

    return_dict["status"] = 1
    return_dict["message"] = "success"
    return_dict["order_id"] = order_id
    return_dict["total_quantity"] = total_quantity
    return_dict["final_delivery_amount"] = final_delivery_amount
    return_dict["convenience_fees"] = convenience_fees
    return_dict["detail_total_amount"] = detail_total_amount
    return_dict["order_obj_list"] = order_obj_list
    return return_dict


def get_combine_status(orders):
    """
    This function returns on the basis of the orders contains combined orders or not..
    """
    if orders:
        grocery_count = 0
        for order in orders:
            product_details_id = order['id']
            get_product_details_obj = ProductDetails.objects.filter(id=product_details_id).first()
            if get_product_details_obj:
                if get_product_details_obj.product_id.base_category:
                    category = None
                    try:
                        category = get_product_details_obj.product_id.base_category.sub_category.category.name
                    except Exception as e:
                        print("Inside Exception of get_combine_status", e)
                    if category == "Grocery":
                        grocery_count += 1

        if grocery_count >= 2:
            return True
    return False


class OrderPlaceApiNew(generics.ListAPIView):
    """
    This Api is to place orders.

    Url: ecomapi/order-place

    Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..TODO
    method = post
    parameter = {
                    'orders': [
                                {
                                    'id': 2,
                                    'quantity': 2
                                },
                                {
                                    'id': 3,
                                    'quantity': 1
                                }
                            ],
                    'Address': "Shivajinagar Pune"
                }

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        today_date = datetime.now()
        order_data = request.data
        wallet_amount = 0
        user_id = request.user.id
        payment_mode = request.data.get("payment_mode")

        platform = False
        platform_type = None
        if "platform" in request.data:
            platform = True
            platform_type = "PhonePe"
        wallet_dict = get_user_wallet_amount(user_id, platform)
        if wallet_dict:
            wallet_amount = wallet_dict["balance_amount"]

        if payment_mode == "credit":
            # Checking if any of the previous repayments are need to pay..
            order_and_amount = get_users_due_order_and_amount(user_id)
            if order_and_amount and order_and_amount is not None:
                usable_wallet = None
                if wallet_amount and wallet_amount != 0 and wallet_amount > 0:
                    usable_wallet = wallet_amount
                return Response(
                    {
                        "status": 3,
                        "message": "To use credit limit, you have to pay one of the previous orders Repayment",
                        "order_text": order_and_amount["order_text"],
                        "repay_amount": order_and_amount["payable_amount"],
                        "wallet_amount": usable_wallet
                    }
                )

        user = User.objects.filter(id=user_id)
        unique_id = generate_unique_id()
        order_id = "ORD" + unique_id
        random_number = random.sample(range(99999), 1)
        payment_order_id = str(order_id) + "e" + str(random_number[0])
        orders = order_data['orders']
        address_id = order_data['address_id']
        coupon_code = None
        if "coupon_code" in order_data:
            coupon_code = order_data['coupon_code']
            if coupon_code:
                coupon_code = coupon_code.upper()
        address_obj = Address.objects.filter(id=address_id)
        detail_amount = 0
        detail_total_amount = 0
        showable_amount = 0
        total_quantity = 0
        pay_amount = 0
        credit = None
        actual_used = 0
        convenience_fees = 0
        delivery_charge = None
        used_wallet = 0
        final_delivery_amount = 0
        order_obj_list = []
        combine_order_obj_list = []
        # Checking users credit limit

        # Updating Stocks of initiated orders..
        initiated_stock_update = initiated_stock_add()

        if orders:
            # combine_status = get_combine_status(orders)
            for order in orders:
                combination_type = "single"
                amount = 0
                # creating unique order Id.
                unique_id = generate_unique_id()
                unique_order_id = "UNI" + unique_id
                product_details_id = order['id']
                quantity = order['quantity']
                total_quantity = total_quantity + quantity

                product_details_obj = ProductDetails.objects.filter(id=product_details_id,
                                                                    status="APPROVED")

                # Checking available stock..
                product_stock = 0
                stock = get_product_details_stock(product_details_obj[0].id)
                if stock:
                    product_stock = stock
                if product_stock < quantity:
                    return Response(
                        {
                            'status': 2,
                            'message': str(product_details_obj[0].product_id.name) + " is out of stock."
                        }
                    )

                if product_details_obj:
                    category = None
                    charge_type = "normal"
                    amount = product_details_obj[0].amount
                    product_details_amount_obj = None
                    amount_data = get_product_details_price(product_details_obj[0].id)
                    if amount_data:
                        amount = amount_data['minimum_amount']
                        product_details_amount_obj = amount_data['product_details_amount_obj']

                    try:
                        if product_details_obj[0].product_id:
                            category = product_details_obj[0].product_id.base_category.sub_category.category.name
                    except Exception as e:
                        print("inside Exception of get category in OrderPlaceApiNew", e)

                    if category == "Grocery" and product_details_obj[0].user.username == "MFPL":
                        charge_type = "Grocery"
                        combination_type = "multiple"

                    detail_amount += amount
                    total_amount = quantity * amount
                    detail_total_amount += total_amount

                    # Get delivery charge object..
                    delivery_charge = get_delivery_charge(total_amount, charge_type)
                    if delivery_charge:
                        final_delivery_amount += delivery_charge.amount

                    convenient_entry = None
                    # Get orders convenient fees..
                    order_convenience_fees = get_orders_convenient_fees(total_amount)
                    if order_convenience_fees != 0:
                        convenience_fees += order_convenience_fees
                        # Create Convenient fees entry..
                        convenient_entry = ConvenientFees.objects.create(amount=order_convenience_fees,
                                                                         order_type="order")

                    order_obj = Order.objects.create(order_id=order_id, product_details=product_details_obj[0],
                                                     quantity=quantity, amount=amount, total_amount=total_amount,
                                                     date=today_date, re_status="True", status="initiated",
                                                     combination_type=combination_type,
                                                     delivery_charges=delivery_charge,
                                                     convenient_fees=convenient_entry,
                                                     platform_type=platform_type,
                                                     order_details_amount=product_details_amount_obj,
                                                     unique_order_id=unique_order_id)

                    order_obj_list.append(order_obj)
                    if combination_type == "multiple":
                        combine_order_obj_list.append(order_obj)

                    # Here we change the status of Order_status table.
                    add_order_status = OrderStatus.objects.create(order=order_obj,
                                                                  date=datetime.now(),
                                                                  status="initiated")

                    # Here we add stock of Product Details..
                    stock_status = "remove"
                    change_by = "order"
                    stock_update = update_stock(product_details_obj[0].id, quantity, stock_status, change_by)


        # Adding convenience fees..
        convenience_obj = None


        if payment_mode == "credit":

            credit = None
            initiated_credit = None
            credit_status = "Failed"
            default_status = check_user_defaulter_status(user[0].id)
            if default_status == "False":
                # Checking Users contacts valid or not..
                from UserData.views import is_users_contacts_valid
                valid_contacts = is_users_contacts_valid(user[0].id)
                if valid_contacts:
                    credit_obj = Credit.objects.filter(user=user[0].id).exclude(
                        status__in=["initiated", "recharge_initiated"]).order_by('id').last()
                    if credit_obj:
                        if credit_obj.balance_credit != 0:
                            credit = credit_obj

            if credit is not None:
                payable_amount = detail_total_amount - credit.balance_credit
                if payable_amount > 0:
                    # This means all credit is used here..
                    used_credit = credit.balance_credit
                    # Total payable amount is remaining payable amount and delivery and convenient fees..
                    payable_amount = payable_amount + final_delivery_amount + convenience_fees

                    # Calculating payable amount, used_wallet
                    data_dict = get_wallet_payable_amount(wallet_amount, payable_amount)
                    if data_dict:
                        used_wallet = data_dict['used_wallet']
                        pay_amount = data_dict['pay_amount']

                    # Checking Users wallet to extra payment for delivery and convenient fees..
                    actual_used = credit.balance_credit
                else:
                    actual_used = detail_total_amount
                    extra_fees = final_delivery_amount + convenience_fees
                    if wallet_amount and wallet_amount != 0:
                        if wallet_amount >= extra_fees:
                            pay_amount = 0
                            used_wallet = extra_fees
                        else:
                            pay_amount = extra_fees - wallet_amount
                            used_wallet = wallet_amount
                    else:
                        pay_amount = extra_fees

                # updating credit of user
                total_used_credit = credit.used_credit + actual_used
                balance_credit = credit.credit - total_used_credit
                if balance_credit < 0:
                    balance_credit = 0

                status = "initiated"


                initiate_credit = Credit.objects.create(credit=credit.credit, used_credit=total_used_credit,
                                                        balance_credit=balance_credit, actual_used=actual_used,
                                                        user=user[0],
                                                        status=status)
                if initiate_credit:
                    credit = initiate_credit
            else:
                # Calculating payable amount, used_wallet
                extra_delivery_amount = detail_total_amount + final_delivery_amount + convenience_fees
                data_dict = get_wallet_payable_amount(wallet_amount, extra_delivery_amount)
                if data_dict:
                    used_wallet = data_dict['used_wallet']
                    pay_amount = data_dict['pay_amount']

        if payment_mode == "cash":
            extra_delivery_amount = detail_total_amount + final_delivery_amount + convenience_fees
            # Calculating payable amount, used_wallet
            data_dict = get_wallet_payable_amount(wallet_amount, extra_delivery_amount)
            if data_dict:
                used_wallet = data_dict['used_wallet']
                pay_amount = data_dict['pay_amount']

        if credit and credit != 0:
            credit_status = "Initiated"
        else:
            credit_status = "Failed"
        order_detail_obj = OrderDetails.objects.create(order_text=order_id, credit=credit,
                                                       total_quantity=total_quantity,
                                                       total_amount=detail_total_amount,
                                                       pay_amount=pay_amount,
                                                       convenient_fees=convenience_obj,
                                                       payment_mode=payment_mode,
                                                       date=today_date, re_status="True", address=address_obj[0],
                                                       credit_status=credit_status,
                                                       status="initiated", user=user[0])

        try:
            if coupon_code:
                coupon_applied = is_coupon_applied(user[0].id)
                if coupon_applied:
                    add_coupon(order_detail_obj, coupon_code)
        except Exception as e:
            print("Inside Exception of Adding coupon==============", e)

        if order_detail_obj:
            # Adding orders in Many to many field..
            order_detail_obj.order_id.add(*order_obj_list)

            if combine_order_obj_list:
                # Adding orders in combined orders..
                order_detail_obj.combine_order.add(*combine_order_obj_list)

            if credit is not None:
                order_detail_obj.credit_history.add(credit)

            # Subtracting wallet amount
            status = "initiated"
            order_obj = None
            if used_wallet > 0:


                user_wallet_sub(user_id, used_wallet, order_detail_obj, order_obj, status)

                # Creating wallet payment entry
                payment = Payment.objects.create(status=status, order_id=str(payment_order_id),
                                                 amount=float(used_wallet),
                                                 category='Order', type="Ecom_Wallet", mode="PPI",
                                                 product_type="Ecom", date=datetime.now())
                # adding fk in order details
                order_detail_obj.payment.add(payment)

        # calculating final checkout amount..
        final_checkout_amount = pay_amount

        # Checksum to be generated or not
        generate_checksum = 0
        if final_checkout_amount != 0:
            generate_checksum = 1

        if payment_mode == "cod":
            generate_checksum = 2

        return Response(
            {
                'status': 1,
                'order_id': order_id,
                'amount': final_checkout_amount,
                'generate_checksum': generate_checksum,
                'message': "Order Initiated Successfully"
            }
        )


class OrderPlaceApiNewBackup(generics.ListAPIView):
    """
    This Api is to place orders.

    Url: ecomapi/order-place

    Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..TODO
    method = post
    parameter = {
                    'orders': [
                                {
                                    'id': 2,
                                    'quantity': 2
                                },
                                {
                                    'id': 3,
                                    'quantity': 1
                                }
                            ],
                    'Address': "Shivajinagar Pune"
                }

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        today_date = datetime.now()
        order_data = request.data

        user_id = request.user.id
        payment_mode = request.data.get("payment_mode")
        user = User.objects.filter(id=user_id)
        unique_id = generate_unique_id()
        order_id = "ORD" + unique_id
        random_number = random.sample(range(99999), 1)
        payment_order_id = str(order_id) + "e" + str(random_number[0])
        orders = order_data['orders']
        address_id = order_data['address_id']
        coupon_code = None
        if "coupon_code" in order_data:
            coupon_code = order_data['coupon_code']
            if coupon_code:
                coupon_code = coupon_code.upper()
        address_obj = Address.objects.filter(id=address_id)
        detail_amount = 0
        detail_total_amount = 0
        showable_amount = 0
        total_quantity = 0
        pay_amount = 0
        credit = None
        actual_used = 0
        convenience_fees = 0
        delivery_charge = None
        used_wallet = 0
        final_delivery_amount = 0
        order_obj_list = []
        combine_order_obj_list = []
        # Checking users credit limit

        # Updating Stocks of initiated orders..
        initiated_stock_update = initiated_stock_add()

        if orders:
            for order in orders:
                combination_type = "single"
                amount = 0
                # creating unique order Id.
                unique_id = generate_unique_id()
                unique_order_id = "UNI" + unique_id
                product_details_id = order['id']
                quantity = order['quantity']
                total_quantity = total_quantity + quantity

                product_details_obj = ProductDetails.objects.filter(id=product_details_id,
                                                                    status="APPROVED")

                # Checking available stock..
                product_stock = 0
                stock = get_product_details_stock(product_details_obj[0].id)
                if stock:
                    product_stock = stock
                if product_stock < quantity:
                    return Response(
                        {
                            'status': 2,
                            'message': str(product_details_obj[0].product_id.name) + " is out of stock."
                        }
                    )

                if product_details_obj:
                    category = None
                    charge_type = "normal"
                    amount = product_details_obj[0].amount
                    product_details_amount_obj = None
                    amount_data = get_product_details_price(product_details_obj[0].id)
                    if amount_data:
                        amount = amount_data['minimum_amount']
                        product_details_amount_obj = amount_data['product_details_amount_obj']

                    try:
                        if product_details_obj[0].product_id:
                            category = product_details_obj[0].product_id.base_category.sub_category.category.name
                    except Exception as e:
                        print("inside Exception of get category in OrderPlaceApiNew", e)

                    if category == "Grocery" and product_details_obj[0].user.username == "MFPL":
                        charge_type = "Grocery"
                        combination_type = "multiple"

                    detail_amount += amount
                    # print('detail_amount', detail_amount)
                    total_amount = quantity * amount
                    detail_total_amount += total_amount

                    # Get delivery charge object..
                    delivery_charge = get_delivery_charge(total_amount, charge_type)
                    if delivery_charge:
                        final_delivery_amount += delivery_charge.amount

                    convenient_entry = None
                    # Get orders convenient fees..
                    order_convenience_fees = get_orders_convenient_fees(total_amount)
                    if order_convenience_fees != 0:
                        convenience_fees += order_convenience_fees
                        # Create Convenient fees entry..
                        convenient_entry = ConvenientFees.objects.create(amount=order_convenience_fees,
                                                                         order_type="order")

                    order_obj = Order.objects.create(order_id=order_id, product_details=product_details_obj[0],
                                                     quantity=quantity, amount=amount, total_amount=total_amount,
                                                     date=today_date, re_status="True", status="initiated",
                                                     combination_type=combination_type,
                                                     delivery_charges=delivery_charge,
                                                     convenient_fees=convenient_entry,
                                                     order_details_amount=product_details_amount_obj,
                                                     unique_order_id=unique_order_id)

                    order_obj_list.append(order_obj)
                    if combination_type == "multiple":
                        combine_order_obj_list.append(order_obj)

                    # Here we change the status of Order_status table.
                    add_order_status = OrderStatus.objects.create(order=order_obj,
                                                                  date=datetime.now(),
                                                                  status="initiated")

                    # Here we add stock of Product Details..
                    stock_status = "remove"
                    change_by = "order"
                    stock_update = update_stock(product_details_obj[0].id, quantity, stock_status, change_by)


        # Adding convenience fees..
        convenience_obj = None


        if payment_mode == "credit":

            credit = None
            initiated_credit = None
            credit_status = "Failed"
            default_status = check_user_defaulter_status(user[0].id)
            if default_status == "False":
                # Checking Users contacts valid or not..
                from UserData.views import is_users_contacts_valid
                valid_contacts = is_users_contacts_valid(user[0].id)
                if valid_contacts:
                    credit_obj = Credit.objects.filter(user=user[0].id).exclude(
                        status__in=["initiated", "recharge_initiated"]).order_by('id').last()
                    if credit_obj:
                        if credit_obj.balance_credit != 0:
                            credit = credit_obj

            if credit is not None:
                payable_amount = detail_total_amount - credit.balance_credit
                if payable_amount > 0:
                    # This means all credit is used here..
                    # Total payable amount is remaining payable amount and delivery and convenient fees..
                    payable_amount = payable_amount + final_delivery_amount + convenience_fees

                    # Calculating payable amount, used_wallet
                    data_dict = get_wallet_payable_amount(wallet_amount, payable_amount)
                    if data_dict:
                        used_wallet = data_dict['used_wallet']
                        pay_amount = data_dict['pay_amount']

                    # Checking Users wallet to extra payment for delivery and convenient fees..
                    actual_used = credit.balance_credit
                else:
                    actual_used = detail_total_amount
                    extra_fees = final_delivery_amount + convenience_fees
                    if wallet_amount and wallet_amount != 0:
                        if wallet_amount >= extra_fees:
                            pay_amount = 0
                            used_wallet = extra_fees
                        else:
                            pay_amount = extra_fees - wallet_amount
                            used_wallet = wallet_amount
                    else:
                        pay_amount = extra_fees

                # updating credit of user
                total_used_credit = credit.used_credit + actual_used
                balance_credit = credit.credit - total_used_credit
                if balance_credit < 0:
                    balance_credit = 0

                status = "initiated"

                initiate_credit = Credit.objects.create(credit=credit.credit, used_credit=total_used_credit,
                                                        balance_credit=balance_credit, actual_used=actual_used,
                                                        user=user[0],
                                                        status=status)
                if initiate_credit:
                    credit = initiate_credit
            else:
                # Calculating payable amount, used_wallet
                extra_delivery_amount = detail_total_amount + final_delivery_amount + convenience_fees
                data_dict = get_wallet_payable_amount(wallet_amount, extra_delivery_amount)
                if data_dict:
                    used_wallet = data_dict['used_wallet']
                    pay_amount = data_dict['pay_amount']

        if payment_mode == "cash":
            extra_delivery_amount = detail_total_amount + final_delivery_amount + convenience_fees
            # Calculating payable amount, used_wallet
            data_dict = get_wallet_payable_amount(wallet_amount, extra_delivery_amount)
            if data_dict:
                used_wallet = data_dict['used_wallet']
                pay_amount = data_dict['pay_amount']

        if credit and credit != 0:
            credit_status = "Initiated"
        else:
            credit_status = "Failed"
        order_detail_obj = OrderDetails.objects.create(order_text=order_id, credit=credit,
                                                       total_quantity=total_quantity,
                                                       total_amount=detail_total_amount,
                                                       pay_amount=pay_amount,
                                                       convenient_fees=convenience_obj,
                                                       payment_mode=payment_mode,
                                                       date=today_date, re_status="True", address=address_obj[0],
                                                       credit_status=credit_status,
                                                       status="initiated", user=user[0])

        try:
            if coupon_code:
                coupon_applied = is_coupon_applied(user[0].id)
                if coupon_applied:
                    add_coupon(order_detail_obj, coupon_code)
        except Exception as e:
            print("Inside Exception of Adding coupon==============", e)

        if order_detail_obj:
            order_detail_obj.order_id.add(*order_obj_list)

            if combine_order_obj_list:
                # Adding orders in combined orders..
                order_detail_obj.combine_order.add(*combine_order_obj_list)

            if credit is not None:
                order_detail_obj.credit_history.add(credit)

            # Subtracting wallet amount
            status = "initiated"
            order_obj = None
            if used_wallet > 0:


                user_wallet_sub(user_id, used_wallet, order_detail_obj, order_obj, status)

                # Creating wallet payment entry
                payment = Payment.objects.create(status=status, order_id=str(payment_order_id),
                                                 amount=float(used_wallet),
                                                 category='Order', type="Ecom_Wallet", mode="PPI",
                                                 product_type="Ecom", date=datetime.now())
                # adding fk in order details
                order_detail_obj.payment.add(payment)

        # calculating final checkout amount..
        final_checkout_amount = pay_amount

        # Checksum to be generated or not
        generate_checksum = 0
        if final_checkout_amount != 0:
            generate_checksum = 1

        if payment_mode == "cod":
            generate_checksum = 2

        return Response(
            {
                'status': 1,
                'order_id': order_id,
                'amount': final_checkout_amount,
                'generate_checksum': generate_checksum,
                'message': "Order Initiated Successfully"
            }
        )


def add_coupon(order_details_obj, coupon_code):
    """
    The function is used to create new entry in coupon with connect order details.
    """
    if None not in (order_details_obj, coupon_code):
        Coupon.objects.create(coupon=coupon_code, order_details=order_details_obj,
                              date=datetime.now())


def is_coupon_applied(user_id):
    """
    Function is used check coupon is applied on user or not...
    """
    coupon_applied = False
    if user_id is not None:
        user_obj = User.objects.filter(id=user_id)
        if user_obj:
            if user_obj[0].date_joined:
                joined_date = str(user_obj[0].date_joined)
                if joined_date[:10] >= "2020-11-03":
                    return True
                else:
                    return False

    return coupon_applied


def make_coupon_true(order_details_id):
    """
    The function is used to update coupon status True for particular order details
    """
    if order_details_id is not None:
        coupon_obj = Coupon.objects.filter(order_details=order_details_id, status=False).order_by('id').last()
        if coupon_obj:
            coupon_obj.status = True
            coupon_obj.updated_date = datetime.now()
            coupon_obj.save()
            # update_coupon = Coupon.objects.filter(id=coupon_obj.id).update(status=True, updated_date=datetime.now())


class OrderReturnApi(generics.ListAPIView):
    """
    This Api is to return the orders.

    Url: ecomapi/order-return

    Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..
    method = post
    parameter = {
                    'order_id': "13"
                }
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    # This is post API
    def post(self, request, *args, **kwargs):
        today_date = datetime.now()
        order_id = request.data.get('order_id')
        order_details_id = request.data.get('order_details_id')
        user_id = request.user.id
        # user_id = 176
        confirmation = request.data.get('confirmation')
        return_reason = request.data.get('return_reason')
        customer_image = None
        if "return_image" in request.data:
            customer_image = request.data.get('return_image')
        use_wallet = request.data.get('use_wallet')

        total_amount = 0
        total_delivery_charge = 0
        wallet_amount = 0
        if order_id is not None:
            get_order_obj = Order.objects.filter(id=order_id)
            if get_order_obj:
                order_details_obj = OrderDetails.objects.filter(order_id=get_order_obj[0].id)
                order_category = "normal"
                try:
                    order_category = order_details_obj[
                        0].product_details.product_id.base_category.sub_category.category.name
                except Exception as e:
                    print("Inside Exception of order_category name in OrderDetailsAPI")
                if order_category != "Grocery":
                    delivery_date = get_order_obj[0].updated_date
                    more_seven_days = (today_date - delivery_date).days
                    if more_seven_days < 7:
                        # TODO uncomment this after switching to Shiprocket..
                        # Checking Return Delivery available for this order or not..
                        is_return = "Yes"
                        from EApi.shiprocket import get_orders_pickup_courier_id
                        is_returnable = get_orders_pickup_courier_id(get_order_obj[0].id, is_return)
                        if is_returnable is not None:
                            if confirmation is not None:
                                if confirmation == "True":
                                    # Create entry in ReturnOrder table..
                                    unique_number = generate_unique_id()
                                    unique_return_id = "RT" + unique_number
                                    # create return Order..
                                    create_return_order = ReturnOrder.objects.create(return_id=unique_return_id,
                                                                                     return_reason=return_reason,
                                                                                     order=get_order_obj[0],
                                                                                     verify_return_status="return_request",
                                                                                     payment_status="not_paid",
                                                                                     type="return")

                                    if customer_image is not None and customer_image != "":
                                        try:
                                            # Add Customer Return Products Image..
                                            customer_image = customer_image.replace("data:image/jpeg;base64,", "")
                                            p = "media/ecom/"
                                            img_name = "RT" + str(generate_unique_id()) + ".jpeg"
                                            path = p + img_name
                                            with open(path, "wb") as fh:
                                                imgdata = base64.b64decode(customer_image)
                                                fh.write(imgdata)
                                            p = "ecom/"
                                            path = p + img_name
                                            image_add = Images.objects.create(path=path, status=True,
                                                                              type="customer_return_image")

                                            if create_return_order is not None and image_add is not None:
                                                from django.conf import settings
                                                base_path = settings.BASE_DIR + "/media/ecom/"
                                                file_path = "ecom/"
                                                new_base_path = base_path + str(img_name)
                                                new_file_path = file_path + str(img_name)

                                                from MApi.botofile import upload_image
                                                upload_image(new_base_path, new_file_path)

                                                add_customer_image = create_return_order.customer_return_image.add(
                                                    image_add)
                                        except Exception as e:
                                            print("Inside Exception of ReturnOrderApi Image upload", e)

                                    # Return request is accepted..
                                    response = ChecksumGenerateReturnDeliveryAmount(user_id,
                                                                                    create_return_order.return_id,
                                                                                    use_wallet)

                                    if response:
                                        status = response['status']
                                        if status == "1":
                                            return Response(
                                                response
                                            )

                                        else:
                                            return Response(
                                                response
                                            )

                            else:
                                # Give Delivery charges to return order..
                                get_order_obj = Order.objects.filter(id=order_id)
                                get_delivery_amount = get_orders_delivery_charge(get_order_obj[0].id)
                                wallet_data = get_user_wallet_amount(order_details_obj[0].user.id)
                                if wallet_data:
                                    wallet_amount = wallet_data['balance_amount']

                                # Want confirmation..
                                return Response(
                                    {
                                        'status': 2,
                                        'message': "Want confirmation.",
                                        'delivery_amount': get_delivery_amount,
                                        'wallet_amount': wallet_amount
                                    }
                                )

                        else:
                            # Can not return the order..
                            return Response(
                                {
                                    'status': 4,
                                    'message': "Return Delivery is not available for this order"
                                }
                            )

                    else:
                        # Can not return the order..
                        return Response(
                            {
                                'status': 3,
                                'message': "Can not return the order"
                            }
                        )
                else:
                    # Can not return the order..
                    return Response(
                        {
                            'status': 11,
                            'message': "Grocery items can not be return."
                        }
                    )

            else:
                return Response(
                    {
                        'status': 8,
                        'message': "Something went wrong"
                    }
                )

        return Response(
            {
                'status': 10,
                'message': "Something went wrong"
            }
        )


class OrderReturnApiWebsite(generics.ListAPIView):
    """
    This Api is to return the orders.

    Url: ecomapi/order-return

    Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..
    method = post
    parameter = {
                    'order_id': "13"
                }
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    # This is post API
    def post(self, request, *args, **kwargs):
        today_date = datetime.now()
        order_id = request.data.get('order_id')
        order_details_id = request.data.get('order_details_id')
        user_id = request.user.id
        # user_id = 176
        confirmation = request.data.get('confirmation')
        return_reason = request.data.get('return_reason')
        customer_image = None
        if "return_image" in request.data:
            customer_image = request.data.get('return_image')
        use_wallet = request.data.get('use_wallet')

        total_amount = 0
        total_delivery_charge = 0
        wallet_amount = 0
        if order_id is not None:
            get_order_obj = Order.objects.filter(id=order_id)
            if get_order_obj:
                order_details_obj = OrderDetails.objects.filter(order_id=get_order_obj[0].id)
                order_category = "normal"
                try:
                    order_category = order_details_obj[
                        0].product_details.product_id.base_category.sub_category.category.name
                except Exception as e:
                    print("Inside Exception of order_category name in OrderDetailsAPI")
                if order_category != "Grocery":
                    delivery_date = get_order_obj[0].updated_date
                    more_seven_days = (today_date - delivery_date).days
                    if more_seven_days < 7:
                        # TODO uncomment this after switching to Shiprocket..
                        # Checking Return Delivery available for this order or not..
                        is_return = "Yes"
                        from EApi.shiprocket import get_orders_pickup_courier_id
                        is_returnable = get_orders_pickup_courier_id(get_order_obj[0].id, is_return)
                        if is_returnable is not None:
                            if confirmation is not None:
                                if confirmation == "True":
                                    # Create entry in ReturnOrder table..
                                    unique_number = generate_unique_id()
                                    unique_return_id = "RT" + unique_number
                                    # create return Order..
                                    create_return_order = ReturnOrder.objects.create(return_id=unique_return_id,
                                                                                     return_reason=return_reason,
                                                                                     order=get_order_obj[0],
                                                                                     verify_return_status="return_request",
                                                                                     payment_status="not_paid",
                                                                                     type="return")

                                    if customer_image is not None and customer_image != "":
                                        try:
                                            # Add Customer Return Products Image..
                                            customer_image = customer_image.replace("data:image/jpeg;base64,", "")
                                            p = "media/ecom/"
                                            img_name = "RT" + str(generate_unique_id()) + ".jpeg"
                                            path = p + img_name
                                            with open(path, "wb") as fh:
                                                imgdata = base64.b64decode(customer_image)
                                                fh.write(imgdata)
                                            p = "ecom/"
                                            path = p + img_name
                                            image_add = Images.objects.create(path=path, status=True,
                                                                              type="customer_return_image")

                                            if create_return_order is not None and image_add is not None:
                                                from django.conf import settings
                                                base_path = settings.BASE_DIR + "/media/ecom/"
                                                file_path = "ecom/"
                                                new_base_path = base_path + str(img_name)
                                                new_file_path = file_path + str(img_name)

                                                from MApi.botofile import upload_image
                                                upload_image(new_base_path, new_file_path)

                                                add_customer_image = create_return_order.customer_return_image.add(
                                                    image_add)
                                        except Exception as e:
                                            print("Inside Exception of ReturnOrderApi Image upload", e)

                                    # Return request is accepted..
                                    response = ChecksumGenerateReturnDeliveryAmountWebsite(user_id,
                                                                                           create_return_order.return_id,
                                                                                           use_wallet)

                                    if response:
                                        status = response['status']
                                        if status == "1":
                                            return Response(
                                                response
                                            )

                                        else:
                                            return Response(
                                                response
                                            )

                            else:
                                # Give Delivery charges to return order..
                                get_order_obj = Order.objects.filter(id=order_id)
                                get_delivery_amount = get_orders_delivery_charge(get_order_obj[0].id)
                                wallet_data = get_user_wallet_amount(order_details_obj[0].user.id)
                                if wallet_data:
                                    wallet_amount = wallet_data['balance_amount']

                                # Want confirmation..
                                return Response(
                                    {
                                        'status': 2,
                                        'message': "Want confirmation.",
                                        'delivery_amount': get_delivery_amount,
                                        'wallet_amount': wallet_amount
                                    }
                                )

                        else:
                            # Can not return the order..
                            return Response(
                                {
                                    'status': 4,
                                    'message': "Return Delivery is not available for this order"
                                }
                            )
                    else:
                        # Can not return the order..
                        return Response(
                            {
                                'status': 3,
                                'message': "Can not return the order"
                            }
                        )
                else:
                    # Can not return the order..
                    return Response(
                        {
                            'status': 11,
                            'message': "Grocery items can not be return."
                        }
                    )
            else:
                return Response(
                    {
                        'status': 8,
                        'message': "Something went wrong"
                    }
                )

        return Response(
            {
                'status': 10,
                'message': "Something went wrong"
            }
        )


class OrderReturnApiWebsitePhonePe(generics.ListAPIView):
    """
    This Api is to return the orders.

    Url: ecomapi/order-return

    Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..
    method = post
    parameter = {
                    'order_id': "13"
                }
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    # This is post API
    def post(self, request, *args, **kwargs):
        today_date = datetime.now()
        order_id = request.data.get('order_id')
        order_details_id = request.data.get('order_details_id')
        user_id = request.user.id
        confirmation = request.data.get('confirmation')
        return_reason = request.data.get('return_reason')
        customer_image = None
        platform = True
        if "return_image" in request.data:
            customer_image = request.data.get('return_image')
        use_wallet = request.data.get('use_wallet')

        total_amount = 0
        total_delivery_charge = 0
        wallet_amount = 0
        if order_id is not None:
            get_order_obj = Order.objects.filter(id=order_id)
            if get_order_obj:
                order_details_obj = OrderDetails.objects.filter(order_id=get_order_obj[0].id)
                order_category = "normal"
                try:
                    order_category = order_details_obj[
                        0].product_details.product_id.base_category.sub_category.category.name
                except Exception as e:
                    print("Inside Exception of order_category name in OrderDetailsAPI")
                if order_category != "Grocery":
                    delivery_date = get_order_obj[0].updated_date
                    more_seven_days = (today_date - delivery_date).days
                    if more_seven_days < 7:
                        # TODO uncomment this after switching to Shiprocket..
                        # Checking Return Delivery available for this order or not..
                        is_return = "Yes"
                        from EApi.shiprocket import get_orders_pickup_courier_id
                        is_returnable = get_orders_pickup_courier_id(get_order_obj[0].id, is_return)
                        if is_returnable is not None:
                            if confirmation is not None:
                                if confirmation == "True":
                                    # Create entry in ReturnOrder table..
                                    unique_number = generate_unique_id()
                                    unique_return_id = "RT" + unique_number
                                    # create return Order..
                                    create_return_order = ReturnOrder.objects.create(return_id=unique_return_id,
                                                                                     return_reason=return_reason,
                                                                                     order=get_order_obj[0],
                                                                                     verify_return_status="return_request",
                                                                                     payment_status="not_paid",
                                                                                     type="return")

                                    if customer_image is not None and customer_image != "":
                                        try:
                                            # Add Customer Return Products Image..
                                            customer_image = customer_image.replace("data:image/jpeg;base64,", "")
                                            p = "media/ecom/"
                                            img_name = "RT" + str(generate_unique_id()) + ".jpeg"
                                            path = p + img_name
                                            with open(path, "wb") as fh:
                                                imgdata = base64.b64decode(customer_image)
                                                fh.write(imgdata)
                                            p = "ecom/"
                                            path = p + img_name
                                            image_add = Images.objects.create(path=path, status=True,
                                                                              type="customer_return_image")

                                            if create_return_order is not None and image_add is not None:
                                                from django.conf import settings
                                                base_path = settings.BASE_DIR + "/media/ecom/"
                                                file_path = "ecom/"
                                                new_base_path = base_path + str(img_name)
                                                new_file_path = file_path + str(img_name)

                                                from MApi.botofile import upload_image
                                                upload_image(new_base_path, new_file_path)

                                                add_customer_image = create_return_order.customer_return_image.add(
                                                    image_add)
                                        except Exception as e:
                                            print("Inside Exception of ReturnOrderApi Image upload", e)

                                    # Return request is accepted..
                                    response = ChecksumGenerateReturnDeliveryAmountPhonePe(user_id,
                                                                                           create_return_order.return_id,
                                                                                           use_wallet)

                                    if response:
                                        status = response['status']
                                        if status == "1":
                                            return Response(
                                                response
                                            )

                                        else:
                                            return Response(
                                                response
                                            )

                            else:
                                # Give Delivery charges to return order..
                                get_order_obj = Order.objects.filter(id=order_id)
                                get_delivery_amount = get_orders_delivery_charge(get_order_obj[0].id)
                                wallet_data = get_user_wallet_amount(order_details_obj[0].user.id, platform)
                                if wallet_data:
                                    wallet_amount = wallet_data['balance_amount']

                                # Want confirmation..
                                return Response(
                                    {
                                        'status': 2,
                                        'message': "Want confirmation.",
                                        'delivery_amount': get_delivery_amount,
                                        'wallet_amount': wallet_amount
                                    }
                                )

                        else:
                            # Can not return the order..
                            return Response(
                                {
                                    'status': 4,
                                    'message': "Return Delivery is not available for this order"
                                }
                            )
                    else:
                        # Can not return the order..
                        return Response(
                            {
                                'status': 3,
                                'message': "Can not return the order"
                            }
                        )
                else:
                    # Can not return the order..
                    return Response(
                        {
                            'status': 11,
                            'message': "Grocery items can not be return."
                        }
                    )
            else:
                return Response(
                    {
                        'status': 8,
                        'message': "Something went wrong"
                    }
                )

        return Response(
            {
                'status': 10,
                'message': "Something went wrong"
            }
        )



def order_return_payment_wallet_credit_entry(order_id):
    """
    This function is used to return wallet and credit used for the order. Input to this function is order_id.
    The order_id will search in OrderDetails & get payment mode of used for this order.
    If payment mode is cod then call get_order_details_return_cancel_amount function.
    If credit used for this order then call credit_balance_update. If wallet used for this order
    then call user_wallet_update function.
    If payment mode is cash then call get_orders_cashback_amount function.
    """
    if order_id is not None:
        # Here we get total order amount
        total_amount = 0
        user_id = None
        payment_mode = None
        get_order_obj = Order.objects.filter(id=order_id)
        if get_order_obj:
            total_amount = get_order_obj[0].total_amount

        # Here we add order amount in to users credit.
        get_order_details_obj = OrderDetails.objects.filter(order_id=order_id)
        if get_order_details_obj:
            user_id = get_order_details_obj[0].user.id
            payment_mode = get_order_details_obj[0].payment_mode

        if payment_mode != "cod":
            credit_used = 0
            extra_amount_paid = 0
            if get_order_details_obj[0].credit:
                credit_used = get_order_details_obj[0].credit.actual_used
                from EApi.credit import get_order_details_return_cancel_amount
                # get order cancel and return total amount..
                total_cancel_return_amount = get_order_details_return_cancel_amount(get_order_details_obj[0].id)
                if total_cancel_return_amount:
                    credit_used = credit_used - total_cancel_return_amount

            # Here we are checking amount to be added in credit and amount to be added in User wallet..
            credit_add_amount = None
            wallet_amount = None
            if credit_used != 0:
                # This means User has used credit for this order..
                if credit_used < total_amount:
                    credit_add_amount = credit_used
                    wallet_amount = total_amount - credit_used
                else:
                    credit_add_amount = total_amount
            else:
                # This means User did not used credit for this order..
                wallet_amount = total_amount

            if credit_add_amount is not None:
                status = "return"
                add_credit = credit_balance_update(user_id, credit_add_amount, status)
                if add_credit:
                    # Adding new credit entry in Credit history of Order Details Table..
                    get_order_details_obj[0].credit_history.add(add_credit)

            delivery_charge = None
            if get_order_obj[0].delivery_charges:
                delivery_charge = get_order_obj[0].delivery_charges.amount

            if wallet_amount is not None:
                status = "order_return"
                add_wallet = user_wallet_update(user_id, wallet_amount, get_order_details_obj[0], get_order_obj[0],
                                                status)

            if payment_mode == "cash":
                disc_amount = 0
                order_date = str(get_order_details_obj[0].created_date)[:10]
                is_big_offer = get_big_offer_status(order_date)
                is_big_offer_new = get_big_offer_status_new(order_date)
                is_zestmony_offer = get_zest_money_offer(get_order_details_obj[0])

                if is_big_offer:
                    disc_amount = get_orders_cashback_amount(get_order_obj[0].id)

                elif is_big_offer_new or is_zestmony_offer:
                    discount_percentage = "fifty"
                    disc_amount = get_orders_cashback_amount(get_order_obj[0].id, discount_percentage)

                else:
                    total_amount = get_order_obj[0].total_amount
                    # This function is for 15% cashback..
                    disc_amount = get_cashback_amount(total_amount)

                if disc_amount > 0:
                    # Subtracting Discount amount for the order from user wallet
                    status = "order_return_discount"
                    user_wallet_sub(user_id, disc_amount, get_order_details_obj[0], get_order_obj, status)

        return True


def return_wallet_amount_cancel_order(request, unique_order_id):
    """
    The function is used to cancel order. It contains the function 'cancel_order' which actually cnacel the order
    with return credit and wallet amount.
    """
    if unique_order_id is not None:
        get_order_obj = Order.objects.filter(unique_order_id=unique_order_id)
        if get_order_obj:
            order_cancel = cancel_order(get_order_obj[0].id)
            if order_cancel:
                return HttpResponse("Order Cancelled")
            else:
                return HttpResponse("Not Okay")
        else:
            return HttpResponse("Order id not found")
    else:
        return HttpResponse("Order id should not None")


def return_wallet_amount_undelivered_order(request, unique_order_id):
    """
    The function is used to return wallet amount of undelivered amount using function
    'handel_undelivered_order'. The function return undelivered order wallet amount return
    or not..
    """
    if unique_order_id is not None:
        get_order_obj = Order.objects.filter(unique_order_id=unique_order_id)
        if get_order_obj:
            order_cancel = handel_undelivered_order(get_order_obj[0].id)
            if order_cancel:
                return HttpResponse("Order Undelivered")
            else:
                return HttpResponse("Something went Wrong")
        else:
            return HttpResponse("Order id not found")
    else:
        return HttpResponse("Order id should not None")


def cancel_order(order_id):
    """
    This function is used for cancel order.
    First create entry of cancel order in OrderStatus.
    Using 'update_stock' cancel order stock will update.
    If payment mode is cod then credit and wallet will not be return.
    To update credit 'credit_balance_update' is used.
    To update wallet 'user_wallet_update' is used.
    The delivery charge, convenient_fees and cashback_amount is return in wallet.
    """
    if order_id is not None:
        user_id = None
        total_amount = 0
        today_date = datetime.now()
        get_order_obj = Order.objects.filter(id=order_id)
        if get_order_obj:
            # order total amount
            total_amount = get_order_obj[0].total_amount
            # Here we change the status of the order..
            get_order_obj[0].status = "cancelled"
            get_order_obj[0].updated_date = today_date
            get_order_obj[0].save()

            # Here we change the status of Order_status table.
            add_order_status = OrderStatus.objects.create(order=get_order_obj[0],
                                                          date=datetime.now(),
                                                          status="cancelled")
            # Here we add stock of Product Details..
            stock_status = "add"
            change_by = "order"
            quantity = get_order_obj[0].quantity
            product_details_id = get_order_obj[0].product_details.id
            from ecom.views import update_stock
            stock_update = update_stock(product_details_id, quantity, stock_status, change_by)
            # Here we add order amount in to users credit..

            if get_order_obj[0].platform_type:
                platform = get_order_obj[0].platform_type
                if platform == "PhonePe":
                    return True

            payment_mode = None
            get_order_details_obj = OrderDetails.objects.filter(order_id=order_id)
            if get_order_details_obj:
                user_id = get_order_details_obj[0].user.id
                payment_mode = get_order_details_obj[0].payment_mode

            if payment_mode == "cod":
                return True

            credit_used = 0
            if get_order_details_obj[0].credit:
                credit_used = get_order_details_obj[0].credit.actual_used
                from EApi.credit import get_order_details_return_cancel_amount
                # get order cancel and return total amount..
                total_cancel_return_amount = get_order_details_return_cancel_amount(get_order_details_obj[0].id)
                if total_cancel_return_amount:
                    credit_used = credit_used - total_cancel_return_amount

            # Here we are checking amount to be added in credit and amount to be added in User wallet..
            credit_add_amount = None
            wallet_amount = None
            cancel_cashback_amount = None
            if credit_used != 0:
                # This means User has used credit for this order..
                if credit_used < total_amount:
                    credit_add_amount = credit_used
                    wallet_amount = total_amount - credit_used
                else:
                    credit_add_amount = total_amount
            else:
                # This means User did not used credit for this order..
                if payment_mode == "cash":
                    discount = 0
                    order_date = str(get_order_details_obj[0].created_date)[:10]
                    is_big_offer = get_big_offer_status(order_date)
                    is_big_offer_new = get_big_offer_status_new(order_date)
                    is_zestmony_offer = get_zest_money_offer(get_order_details_obj[0])

                    if is_big_offer:
                        discount = get_orders_cashback_amount(get_order_obj[0].id)

                    elif is_big_offer_new or is_zestmony_offer:
                        discount_percentage = "fifty"
                        discount = get_orders_cashback_amount(get_order_obj[0].id, discount_percentage)

                    else:
                        cashback_amount = get_order_obj[0].total_amount
                        # This function is for 15% cashback..
                        discount = get_cashback_amount(cashback_amount)

                    if discount > 0:
                        # Deduct extra 15% amount from total return amount before giving to the wallet..
                        cancel_cashback_amount = discount
                    wallet_amount = total_amount
                else:
                    wallet_amount = total_amount
            delivery_charge = None
            if get_order_obj[0].delivery_charges:
                delivery_charge = get_order_obj[0].delivery_charges.amount

            convenient_charge = None
            if get_order_obj[0].convenient_fees:
                convenient_charge = get_order_obj[0].convenient_fees.amount

            if credit_add_amount is not None:
                status = "cancel"
                from EApi.credit import credit_balance_update
                add_credit = credit_balance_update(user_id, credit_add_amount, status)
                if add_credit:
                    # Adding new credit entry in Credit history of Order Details Table..
                    get_order_details_obj[0].credit_history.add(add_credit)

            from EApi.wallet import user_wallet_update
            if wallet_amount is not None:
                status = "order_cancel"

                add_wallet = user_wallet_update(user_id, wallet_amount, get_order_details_obj[0], get_order_obj[0],
                                                status)
            if delivery_charge:
                status = "delivery_charge"
                add_wallet = user_wallet_update(user_id, delivery_charge, get_order_details_obj[0], get_order_obj[0],
                                                status)

            if convenient_charge:
                status = "convenience_charge"
                add_wallet = user_wallet_update(user_id, convenient_charge, get_order_details_obj[0], get_order_obj[0],
                                                status)

            from EApi.wallet import user_wallet_sub
            if cancel_cashback_amount:
                status = "cancel_cashback"
                add_wallet = user_wallet_sub(user_id, cancel_cashback_amount, get_order_details_obj[0],
                                             get_order_obj[0], status)

            get_combined_order_obj = OrderDetails.objects.filter(combine_order=order_id)
            if get_combined_order_obj:
                remove_object = get_combined_order_obj[0].combine_order.remove(get_order_obj[0])

            return True

        else:
            return False


class OrderCancellationApi(generics.ListAPIView):
    """
    This Api is to Cancel the orders.
    Url: ecomapi/order-cancel
    Authentication token required
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..
    method = post
    parameter = {
                    'order_id': "12"
                }
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    # This is post API
    def post(self, request, *args, **kwargs):
        today_date = datetime.now()
        confirmation = None
        user_id = None
        total_amount = 0
        total_delivery_charge = 0
        order_id = request.data.get('order_id')
        platform = False
        if "platform" in request.data:
            platform = True
        if order_id is not None:
            confirmation = request.data.get('confirmation')
            get_order_obj = Order.objects.filter(id=order_id)
            if get_order_obj:
                status = get_order_obj[0].status
                if status == "placed":
                    if confirmation == "True":
                        # Here calling order cancellation API..
                        call_cancel_order = cancel_order(get_order_obj[0].id)
                        if call_cancel_order:
                            get_order_details_obj = OrderDetails.objects.filter(order_id=get_order_obj[0].id)
                            number = get_order_details_obj[0].user.username
                            sms_type = "cancelled"
                            if platform:
                                sms_type = "phonepe_cancelled"

                            osms_kwrgs = {
                                'sms_type': sms_type,
                                'number': str(number)
                            }
                            order_sms(**osms_kwrgs)
                            return Response(
                                {
                                    'status': 3,
                                    'message': 'Order cancelled Successfully'
                                }
                            )
                    else:
                        return Response(
                            {
                                'status': 2,
                                'message': 'want confirmation'
                            }
                        )
                else:
                    return Response(
                        {
                            'status': 1,
                            'message': 'order can not be cancelled'
                        }
                    )


# This is a @ exempt functions called by javascript of sellers page.. This needs to change.
@csrf_exempt
def SellerOrderReturnVerify(request):
    """
    Input parameter to this function is seller_status, order_id, seller_reason, product_image and verify_return_status.
    The order_id will check in ReturnOrder table, if its payment_status is success then go for next procedure.
    Verify return status are Return_Confirm or Return_Rejected. If Return_Confirm then order is return to confirmed
    and if Return_Rejected then order can't be returned. According to return_confirm and return_reject the
    status of seller_status will updated in ReturnOrder table.
    """
    if request.method == "POST":
        print('=========here============')
        today_date = datetime.now()
        user_id = request.user.id
        # user_id = 176
        user = User.objects.filter(id=user_id)
        seller_status = request.POST['seller_status']
        order_id = request.POST['order_id']
        seller_reason = request.POST['seller_reason']
        image = request.POST['product_image']  # This should include multiple images of the product.

        if seller_status is not None:
            get_order_obj = Order.objects.filter(id=order_id)
            if get_order_obj:

                pass
            get_return_order_obj = ReturnOrder.objects.filter(order=order_id, verify_return_status="return_confirm",
                                                              payment_status="success")
            if get_return_order_obj:
                status = ""
                if seller_status == "Return_Confirm":
                    status = "seller_return_confirm"
                if seller_status == "Return_Rejected":
                    status = "seller_return_rejected"

                return_obj = ReturnOrder.objects.filter(order=get_order_obj[0].id, payment_status="success",
                                                        verify_return_status="return_confirm")
                if return_obj:
                    return_obj[0].seller_status = status
                    return_obj[0].seller_reason = seller_reason
                    return_obj[0].product_verify_date = today_date
                    return_obj[0].seller_verify_by = user[0]
                    return_obj[0].updated_date = today_date
                    return_obj[0].save()

                # Update Order status entry..
                add_order_status = OrderStatus.objects.create(order=get_order_obj[0], status=status)

                # Here is to add the product images verified by the seller.
                if image and image is not None:
                    add_image = Images.objects.create(path=image, status=True, type="seller_return_image")
                    if add_image:
                        get_return_order_obj[0].seller_return_image.add(add_image)

            if seller_status == "Return_Confirm":
                # This means whole return process is Done.
                # Returning Credit and Wallet amount..
                try:
                    amount_calculations = order_return_payment_wallet_credit_entry(order_id)
                except Exception as e:
                    print('inside exception of amount calculations in return order')

                # completing return process..
                return_order_obj = ReturnOrder.objects.filter(return_id=get_return_order_obj[0].return_id,
                                                              verify_return_status="return_confirm",
                                                              seller_status="seller_return_confirm")
                if return_order_obj:
                    return_order_obj[0].verify_return_status = "return_complete"
                    return_order_obj[0].updated_date = datetime.now()
                    return_order_obj[0].save()

        return HttpResponse("ok")


class ReturnOrderDetailsList(generic.ListView):
    """
    The class is used to render order_details_obj, total_delivery_charge and return order object.
    """
    template_name = "Ecom/orders/return_order_details_list.html"

    def get(self, request, *args, **kwargs):
        ctx = {}
        data_dict = {}
        order_details = OrderDetails.objects.filter(status="return_request")
        for order_obj in order_details:
            order_detail = OrderDetails.objects.filter(id=order_obj.id).values_list("order_id", flat="True")
            returns = ReturnOrder.objects.filter(order_details=order_obj)
            delivery_charge = 0
            if order_details:
                orders = Order.objects.filter(id__in=order_detail)
                if orders:
                    for order in orders:
                        delivery_charges = DeliveryCharges.objects.filter(
                            product_id=order.product_details.product_id.id)
                        if delivery_charges:
                            delivery_charge = delivery_charges[0].amount + delivery_charge
                data_dict.update({order_obj: [delivery_charge, returns[0]]})
        ctx['order'] = data_dict
        return render(request, self.template_name, ctx)


class ReturnOrderList(generic.ListView):
    """
        This class render return order list, order details obj, delivery charges and customer image.
    """
    template_name = "Ecom/orders/return_order_list.html"

    def get(self, request, seller, *args, **kwargs):
        ctx = {}
        data_dict = {}
        return_obj = ReturnOrder.objects.filter(verify_return_status="return_request", payment_status='success',
                                                order__product_details__user__username=seller)
        for return_order in return_obj:
            customer_image = ""
            if return_order:
                path = return_order.customer_return_image.filter(type='customer_return_image')
                if path:
                    customer_image = botofile.get_url(path[0].path)
            else:
                return_order = 0
            order_detail = OrderDetailsOrder.objects.filter(order=return_order.order).values_list("orderdetails",
                                                                                                  flat="True")
            order_details = OrderDetails.objects.filter(id__in=order_detail)
            delivery_charge = 0
            if order_details:
                delivery_charges = DeliveryCharges.objects.filter(
                    product_id=return_order.order.product_details.product_id.id)
                if delivery_charges:
                    delivery_charge = delivery_charges[0].amount + delivery_charge
                data_dict.update({return_order: [order_details[0], delivery_charge, customer_image]})
        ctx['data'] = data_dict
        return render(request, self.template_name, ctx)


class ReturnSellerOrderList(generic.ListView):
    """
        This class render return order list, order details obj, delivery charges and customer image for seller.
    """
    template_name = "Ecom/orders/return_seller_order_list.html"

    def get(self, request, *args, **kwargs):
        ctx = {}
        data_dict = {}
        user_id = request.user.id
        return_obj = ReturnOrder.objects.filter(verify_return_status="return_confirm", payment_status='success',
                                                order__product_details__user=user_id)
        for return_order in return_obj:
            path = return_order.customer_return_image.filter(type='customer_return_image')
            if path:
                customer_image = botofile.get_url(path[0].path)
            else:
                customer_image = 0
            order_detail = OrderDetailsOrder.objects.filter(order=return_order.order.id).values_list("orderdetails",
                                                                                                     flat="True")
            order_details = OrderDetails.objects.filter(id__in=order_detail)
            delivery_charge = 0
            if order_details:
                delivery_charges = DeliveryCharges.objects.filter(
                    product_id=return_order.order.product_details.product_id.id)
                if delivery_charges:
                    delivery_charge = delivery_charges[0].amount + delivery_charge
                data_dict.update({return_order: [order_details[0], delivery_charge, customer_image]})
            ctx['data'] = data_dict
        return render(request, self.template_name, ctx)


@csrf_exempt
def AdminOrderDetailsVerify(request):
    """
    Input parameter to this function is order_type, order_id and verify_return_status. Order type should be order.
    The order_id will check in ReturnOrder table, if its payment_status is success then go for next procedure.
    Verify return status are Return_Confirm or Return_Rejected. If Return_Confirm then order is return to confirmed
    and if Return_Rejected then order can't be returned. According to return_confirm and return_reject the
    status of verify_return_status will updated in ReturnOrder table.
    """
    if request.method == "POST":
        today_date = datetime.now()
        user = None
        user_id = request.user.id
        if user_id:
            user = User.objects.filter(id=user_id).first()
        order_type = request.POST.get('order_type')
        if order_type == "order":
            return_status = ""
            status = ""
            verify_return_status = request.POST.get('verify_return_status')
            if verify_return_status == "Return_Confirm":
                return_status = "return_confirm"
                status = "admin_return_confirm"
            if verify_return_status == "Return_Rejected":
                return_status = "return_rejected"
                status = "admin_return_reject"
            unique_order_id = request.POST.get('order_id')
            reason = request.POST.get('reason')
            get_order_obj = Order.objects.filter(unique_order_id=unique_order_id)
            if get_order_obj:
                # updating return status on order status table..
                OrderStatus.objects.create(order=get_order_obj[0], date=datetime.now(), status=status)

            # updating status in Return Order table
            get_return_order_obj = ReturnOrder.objects.filter(order=get_order_obj[0].id,
                                                              payment_status="success").order_by('id').last()

            if verify_return_status == "Return_Rejected":
                # Giving delivery charges return to customer wallet..
                get_paid_amount = Payment.objects.filter(order_id__startswith=get_return_order_obj.return_id,
                                                         category="Order_Return", status="success").aggregate(
                    Sum('amount'))['amount__sum']

                if get_paid_amount:
                    get_order_details_obj = OrderDetails.objects.filter(order_id=get_return_order_obj.order.id)
                    # return this amount to users wallet..
                    status = "return_reject_delivery_charge"
                    update_users_wallet = user_wallet_update(user_id, get_paid_amount, get_order_details_obj[0],
                                                             get_order_obj[0], status)

            if verify_return_status == "Return_Confirm":
                # create Logistics Order..
                # Checking Return Delivery available for this order or not..
                order_mode = "Reverse"
                is_return = "Yes"
                from EApi.shiprocket import get_orders_pickup_courier_id
                is_returnable = get_orders_pickup_courier_id(get_order_obj[0].id, is_return)
                if is_returnable is not None:
                    from EApi.shiprocket import generate_return_order
                    try:
                        create_shiprocket_return_order = generate_return_order(get_order_obj[0].id)
                        if create_shiprocket_return_order == "1":
                            if get_return_order_obj:
                                get_return_order_obj.verify_return_status = return_status
                                get_return_order_obj.admin_verified_date = today_date
                                get_return_order_obj.admin_verified_date = today_date
                                get_return_order_obj.verified_by = user
                                get_return_order_obj.save()

                            else:
                                pass

                    except Exception as e:
                        print('inside exception of create_shiprocket_rekturn_order', e)
                        print('-----------in exception----------')
                        print(e.args)
                        import os
                        import sys
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                        print(exc_type, fname, exc_tb.tb_lineno)

        return HttpResponse("ok")


class SellerReturnOrderHistory(generic.ListView):
    """
     This Function is to show count of return order for ecomadmin and seller with seller_status
     seller_return_confirm and seller_return_rejected....
    """
    template_name = "Ecom/orders/seller_return_order_history.html"

    def get(self, request, *args, **kwargs):
        ctx = {}
        data_dict = {}
        user = User.objects.filter(id=request.user.id)
        seller_group = user[0].groups.filter(name='Seller').exists()
        if seller_group:
            return_confirm_count = ReturnOrder.objects.filter(order__product_details__user=user[0],
                                                              seller_status="seller_return_confirm").count()
            return_rejected_count = ReturnOrder.objects.filter(order__product_details__user=user[0],
                                                               seller_status="seller_return_rejected").count()
            data_dict.update({user[0]: [return_confirm_count, return_rejected_count]})
            ctx['seller_return_data'] = data_dict

        ecom_admin_group = user[0].groups.filter(name__in=['EcomAdmin', 'EcomManager']).exists()
        if ecom_admin_group:
            sellers_list = ReturnOrder.objects.filter(type="return", payment_status="success").values_list(
                'order__product_details__user__username', flat=True).distinct()
            if sellers_list:
                for seller in sellers_list:
                    return_confirm_count = ReturnOrder.objects.filter(payment_status="success",
                                                                      order__product_details__user__username=seller,
                                                                      seller_status="seller_return_confirm").count()
                    return_rejected_count = ReturnOrder.objects.filter(payment_status="success",
                                                                       order__product_details__user__username=seller,
                                                                       seller_status="seller_return_rejected").count()
                    verify_return_confirm_count = ReturnOrder.objects.filter(payment_status="success",
                                                                             order__product_details__user__username=seller,
                                                                             verify_return_status="return_confirm").count()
                    verify_return_rejected_count = ReturnOrder.objects.filter(payment_status="success",
                                                                              order__product_details__user__username=seller,
                                                                              verify_return_status="return_rejected").count()
                    data_dict.update({seller: [return_confirm_count, return_rejected_count, verify_return_confirm_count,
                                               verify_return_rejected_count]})
                    ctx['admin_return_data'] = data_dict
        return render(request, self.template_name, ctx)


class SellerReturnOrderdata(generic.ListView):
    """
    This class is to show return order details for particular seller, particular status and
    particular type.
    """
    template_name = "Ecom/orders/seller_return_order_data.html"

    def get(self, request, seller, status, type, *args, **kwargs):
        ctx = {}
        if type == "seller_status":
            return_obj = ReturnOrder.objects.filter(payment_status="success",
                                                    order__product_details__user__username=seller, seller_status=status)
            if return_obj:
                ctx['return_order'] = return_obj
        if type == "verify_return_status":
            return_obj = ReturnOrder.objects.filter(payment_status="success",
                                                    order__product_details__user__username=seller,
                                                    verify_return_status=status)
            if return_obj:
                ctx['return_order'] = return_obj
        return render(request, self.template_name, ctx)


class SellerExchangeOrderHistory(generic.ListView):
    """
         This Function is to show count of exchange order for ecomadmin and seller with seller_status
         seller_exchange_confirm and seller_exchange_rejected....
    """
    template_name = "Ecom/orders/seller_exchange_order_history.html"

    def get(self, request, *args, **kwargs):
        ctx = {}
        data_dict = {}
        user = User.objects.filter(id=request.user.id)
        seller_group = user[0].groups.filter(name='Seller').exists()
        if seller_group:
            exchange_confirm_count = ReturnOrder.objects.filter(order__product_details__user=user[0],
                                                                seller_status="seller_exchange_confirm").count()
            exchange_rejected_count = ReturnOrder.objects.filter(order__product_details__user=user[0],
                                                                 seller_status="seller_exchange_rejected").count()
            data_dict.update({user[0]: [exchange_confirm_count, exchange_rejected_count]})
            ctx['seller_exchange_data'] = data_dict
        ecom_admin_group = user[0].groups.filter(name='EcomAdmin').exists()
        if ecom_admin_group:
            sellers_list = ReturnOrder.objects.filter(seller_status__in=["seller_exchange_confirm",
                                                                         "seller_exchange_rejected"]).values_list(
                'order__product_details__user__username', flat=True).distinct()
            if sellers_list:
                for seller in sellers_list:
                    exchange_confirm_count = ReturnOrder.objects.filter(order__product_details__user__username=seller,
                                                                        seller_status="seller_exchange_confirm").count()
                    exchange_rejected_count = ReturnOrder.objects.filter(order__product_details__user__username=seller,
                                                                         seller_status="seller_exchange_rejected").count()
                    data_dict.update({seller: [exchange_confirm_count, exchange_rejected_count]})
                    ctx['admin_exchange_data'] = data_dict
        return render(request, self.template_name, ctx)


class SellerExchangeOrderdata(generic.ListView):
    """
    This Function is to show exchange order details for particular seller and particular status..
    """
    template_name = "Ecom/orders/seller_exchange_order_data.html"

    def get(self, request, seller, status, *args, **kwargs):
        ctx = {}
        exchange_obj = ReturnOrder.objects.filter(order__product_details__user__username=seller, seller_status=status)
        if exchange_obj:
            ctx['exchange_order'] = exchange_obj
        return render(request, self.template_name, ctx)


def seller_amount_calculations(request):
    """
    The function is used to calculate seller amount. The particular seller orders are add to SellerAmount orders,
    and the return order, exchange order also attached to SellerAmount return_orders, exchange_orders respectively.
    """
    yesterday = datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d')
    # Counting for Dispatched and Delivered orders..
    all_sellers = Order.objects.filter(updated_date__startswith=yesterday,
                                       status__in=["dispatched", "delivered"]).values_list(
        'product_details__user', flat=True).distinct()
    if all_sellers:
        get_sellers_user_id = User.objects.filter(id__in=all_sellers)
        if get_sellers_user_id:
            for seller in get_sellers_user_id:
                seller_amount_objects_list = []
                exchange_order_list = []
                return_order_list = []
                half_dispatch_amount = 0
                half_delivered_amount = 0
                return_order_amount = 0
                exchange_order_amount = 0
                total_dispatch_order_amount = 0
                total_delivered_order_amount = 0
                # here we calculate each orders amount..
                # get all dispatch orders for this seller..
                get_all_dispatched_orders = Order.objects.filter(updated_date__startswith=yesterday,
                                                                 status="dispatched",
                                                                 product_details__user=seller.id)
                if get_all_dispatched_orders:
                    dispatch_orders_list = []
                    for order in get_all_dispatched_orders:
                        seller_amount = order.order_details_amount.seller_amount
                        total_dispatch_order_amount += seller_amount
                        if total_dispatch_order_amount != 0:
                            half_dispatch_amount = total_dispatch_order_amount / 2
                        dispatch_orders_list.append(order)
                    # creating object in Seller Amount table for dispatch amount..
                    add_seller_dispatch_amount_obj = SellerAmount.objects.create(type="dispatched",
                                                                                 total_amount=half_dispatch_amount,
                                                                                 )
                    seller_amount_objects_list.append(add_seller_dispatch_amount_obj)
                    # add orders in M2M fields..
                    if add_seller_dispatch_amount_obj:
                        add_seller_dispatch_amount_obj.order.add(*dispatch_orders_list)

                # get all delivered orders for this seller..
                get_all_delivered_orders = Order.objects.filter(updated_date__startswith=yesterday,
                                                                status="delivered",
                                                                product_details__user=seller.id)
                if get_all_delivered_orders:
                    delivered_orders_list = []
                    for order in get_all_delivered_orders:
                        seller_amount = order.order_details_amount.seller_amount
                        total_delivered_order_amount += seller_amount
                        if total_delivered_order_amount != 0:
                            half_delivered_amount = total_delivered_order_amount / 2
                        delivered_orders_list.append(order)
                    # creating object in Seller Amount table for delivered amount..
                    add_seller_delivered_amount_obj = SellerAmount.objects.create(type="delivered",
                                                                                  total_amount=half_delivered_amount,
                                                                                  )
                    seller_amount_objects_list.append(add_seller_delivered_amount_obj)
                    # add orders in M2M fields..
                    if add_seller_delivered_amount_obj:
                        add_seller_delivered_amount_obj.order.add(*delivered_orders_list)

                # Calculating Return Orders from ReturnOrder Table..
                get_return_orders = ReturnOrder.objects.filter(seller_verify_date__startswith=yesterday,
                                                               seller_verify_by=seller.id,
                                                               status="return_confirm")
                if get_return_orders:
                    for return_order_obj in get_return_orders:
                        get_order_obj = Order.objects.filter(id=return_order_obj.order)
                        if get_order_obj:
                            return_order_amount += get_order_obj.order_details_amount.seller_amount
                            return_order_list.append(return_order_obj)

                # Calculating Exchange Orders from ReturnOrder Table..
                get_exchange_orders = ReturnOrder.objects.filter(seller_verify_date__startswith=yesterday,
                                                                 seller_verify_by=seller.id,
                                                                 status="exchange_confirm")
                if get_exchange_orders:
                    for return_order_obj in get_exchange_orders:
                        get_order_obj = Order.objects.filter(id=return_order_obj.order)
                        if get_order_obj:
                            exchange_order_amount += get_order_obj.order_details_amount.seller_amount
                            exchange_order_list.append(return_order_obj)

                # Adding Sellers Account entry in Sellers Account Table..
                total_amount = half_dispatch_amount + half_delivered_amount
                amount_to_pay = total_amount - (exchange_order_amount + return_order_amount)
                add_seller_account_obj = SellerAccount.objects.create(total_amount=total_amount,
                                                                      return_amount=return_order_amount,
                                                                      exchange_amount=exchange_order_amount,
                                                                      amount_to_pay=amount_to_pay,
                                                                      seller=seller.id)
                if add_seller_account_obj:
                    # Adding SellerAmount many to many field..
                    add_seller_account_obj.seller_amount.add(*seller_amount_objects_list)
                    if return_order_list is not None:
                        # Adding return_orders many to many field..
                        add_seller_account_obj.return_orders.add(*return_order_list)
                    if exchange_order_list is not None:
                        # Adding exchange_orders many to many field..
                        add_seller_account_obj.exchange_orders.add(*exchange_order_list)


class Invoices(generics.ListAPIView):
    """
    This API gives all order details information of a user like order_details_id, invoiceId,
    title of product, created date, order amount, and order details status.
     Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
     Response:
        data = [
                {
                    "id": "1",
                    "invoiceId": "500884010",
                    "title": "Marshall Kilburn Portable Wireless Speaker",
                    "dateCreate": "20-1-2020",
                    "amount": "42.99",
                    "status": "Successful delivery",
                },
                {
                    "id": "2",
                    "invoiceId": "593347935",
                    "title": "Herschel Leather Duffle Bag In Brown Color",
                    "dateCreate": "20-1-2020",
                    "amount": "299.99",
                    "status": "Cancel",
                },
                {
                    "id": "3",
                    "invoiceId": "593347935",
                    "title": "-----------Xbox One Wireless Controller Black Color",
                    "dateCreate": "20-1-2020",
                    "amount": "19.99",
                    "status": "Cancel",
                },
                {
                    "id": "4",
                    "invoiceId": "615397400",
                    "title": "+++++++++++++++==Indoor Of Show Jumping Novel",
                    "dateCreate": "20-1-2020",
                    "amount": "141.00",
                    "status": "Cancel",
                }

            ]

    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):

        invoices_list = []
        user = User.objects.filter(id=request.user.id)
        if user:
            order_details_objects = OrderDetails.objects.filter(user=user[0].id).exclude(status="initiated")
            if order_details_objects is not None:
                for order_details in order_details_objects:
                    order_dict = {}
                    order_dict = {
                        "id": order_details.id,
                        "invoiceId": str(order_details.order_text),
                        "title": str(order_details.order_text),
                        "dateCreate": order_details.created_date,
                        "amount": order_details.total_amount,
                        "status": order_details.status,
                    }

                    invoices_list.append(order_dict)

        return Response(
            {
                "data": invoices_list
            }
        )


class CheckOutApi(generics.ListAPIView):
    """
    This Api is to place orders.

    Url: ecomapi/order-place

    Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..
    method = post
    parameter = {
                    'orders': [
                                {
                                    'product_details_id': 2,
                                    'quantity': 2
                                },
                                {
                                    'product_details_id': 3,
                                    'quantity': 1
                                }
                            ],
                    'Address': "Shivajinagar Pune"
                }

    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    # This is post API
    def get(self, request, *args, **kwargs):
        order_data = {
            'orders': [
                {
                    'product_details_id': 44,
                    'quantity': 2
                },
                {
                    'product_details_id': 45,
                    'quantity': 1
                }
            ],
            'address': '2015 South Street, Midland, Texas',
            'credit_amount': '1000',
            'subtotal': '500',
            'shipping_amount': '200',
        }

        return Response(
            {
                'status': 1,
                'message': "Order fetch Successfully",
                'data': order_data
            }
        )


class InvoiceDetails(generics.ListAPIView):
    """
    This API gives all order details information of particular order..
    """

    def post(self, request, *args, **kwargs):
        """
       {
    "data": [
        {
            "id": "6",
            "thumbnail": "/static/img/products/shop/5.jpg",
            "title": "Grand Slam Indoor Of Show Jumping Novel",
            "vendor": "Roberts Store",
            "sale": "True",
            "price": "32.99",
            "salePrice": "41.00",
            "rating": "True",
            "ratingCount": "4",
            "badge": [
                {
                    "type": "sale",
                    "value": "-37%"
                }
            ],
            "status": "placed"
        },
        {
            "id": "7",
            "thumbnail": "/static/img/products/shop/6.jpg",
            "title": "Sound Intone I65 Earphone White Version",
            "vendor": "Youngshop",
            "sale": "True",
            "price": "100.99",
            "salePrice": "106.00",
            "rating": "True",
            "status": "dispatched",
            "ratingCount": "5",
            "badge": [
                {
                    "type": "sale",
                    "value": "-5%"
                }
            ]
        }
    ],
    "shipping_address": {},
    "credit": ""
}
        """
        invoices = {}
        order_details_id = request.data.get('order_details_id')
        output = {}
        data = []
        if order_details_id is not None:
            order_details_obj = OrderDetails.objects.filter(id=order_details_id)
            if order_details_obj:
                credit = 0
                if order_details_obj[0].credit:
                    credit = order_details_obj[0].credit.used_credit
                order_objects = OrderDetails.objects.filter(id=order_details_id).values_list("order_id", flat="True")
                if order_objects:
                    orders = Order.objects.filter(id__in=order_objects)
                    for order in orders:
                        product_amount = 0
                        amount_data = get_product_details_price(order.product_details.id)
                        if amount_data:
                            product_amount = amount_data['minimum_amount']
                        image = ""
                        product_details_image = Images.objects.filter(type="other",
                                                                      product_details=order.product_details.id).order_by(
                            'id').last()

                        if product_details_image:
                            image = image_thumbnail(product_details_image.path)

                        invoices = {
                            "id": order.id,
                            "thumbnail": str(image),
                            "title": order.product_details.product_id.name,
                            "vendor": order.product_details.user.username,
                            "status": order.status,
                            "sale": True,
                            "quantity": order_details_obj[0].total_quantity,
                            "price": product_amount,
                            "salePrice": product_amount,
                            "rating": True,
                            "ratingCount": '5',
                            "badge": [
                                {
                                    "type": 'sale',
                                    "value": '-5%',
                                },
                            ]
                        }
                        shipping_charges = get_order_details_delivery_charge(order_details_obj[0].id)
                        data.append(invoices)
                        output = {'data': data,
                                  "shipping_address": order_details_obj[0].address.address,
                                  "shipping_charges": shipping_charges,
                                  "credit": str(credit),
                                  }
        return Response(
            {
                "data": output
            }
        )


class OrderPaymentDeliveryChargesDetail(generic.ListView):
    """
    This class is used to render order details object , repayment, payment and delivery charges of respective order details.
    """
    template_name = "Ecom/orders/order_payment_delivery_charges_detail.html"

    def get(self, request, *args, **kwargs):
        ctx = {}
        data_dict = {}
        order_details_obj = OrderDetails.objects.all()
        if order_details_obj:
            for order_details in order_details_obj:
                rep_amount = order_details.payment.filter(status='success', product_type="Ecom",
                                                          category="Repayment").aggregate(Sum('amount'))['amount__sum']
                order_amount = order_details.payment.filter(status='success', product_type="Ecom",
                                                            category="Order").aggregate(Sum('amount'))['amount__sum']
                delivery_charges = get_order_details_delivery_charge(order_details.id)
                data_dict.update({order_details: [delivery_charges, rep_amount, order_amount]})
                ctx['data'] = data_dict
        return render(request, self.template_name, ctx)


class UserOrderHistoryApi(generics.ListAPIView):
    """
    This Api is to Users all order details with all order...
    Url: ecomapi/user_order_details_and_order_api
    Authentication token required
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
    User token is required..TODO
    method = get
    Response:
            {
            "status": "1",
            "data": [
                {
                    "order_details_id": 2,
                    "status": "placed",
                    "credit used": 0,
                    "date": "2020-03-28",
                    "total_amount": 6000.0,
                    "orders": []
                },
                {
                    "order_details_id": 1,
                    "status": "cancelled",
                    "credit used": 2000.0,
                    "date": "2020-03-14",
                    "total_amount": 2000.0,
                    "orders": [
                        {
                            "order_id": "UNI14187",
                            "status": "cancelled",
                            "title": "Rado Watch",
                            "thumbnail": "https://mudrakwik-storage.s3.amazonaws.com/ecom/watch_1.jpeg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA3AOPBD3KG46TBT62%2F20200626%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20200626T070137Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=7a5580ad53f39cd3d35d7f9c5c25cc81d762afabbe5a7edc6d65e9c4e8b96f73",
                            "total_amount": 1000.0
                        },
                        {
                            "order_id": "UNI20688",
                            "status": "cancelled",
                            "title": "Fast-track watch 001",
                            "thumbnail": "https://mudrakwik-storage.s3.amazonaws.com/ecom/watch_1.jpeg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA3AOPBD3KG46TBT62%2F20200626%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20200626T070138Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=59456368b03762928ec90e4408beae441ae8311b20bc225d80981a02f2331c15",
                            "total_amount": 1000.0
                        }
                    ]
                }
            ]
        }
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        user_id = request.user.id
        order_details_list = []
        today_date = datetime.now()
        order_details_obj = OrderDetails.objects.filter(user=user_id).exclude(
            status="initiated").order_by("-id")
        if order_details_obj:
            for order_details in order_details_obj:
                credit = 0
                if order_details.credit:
                    credit = order_details.credit.actual_used
                    from EApi.credit import get_order_details_return_cancel_amount
                    # get order cancel and return total amount..
                    total_cancel_return_amount = get_order_details_return_cancel_amount(
                        order_details_obj[0].id)
                    if total_cancel_return_amount:
                        credit = credit - total_cancel_return_amount
                order_list = []
                orders = order_details.order_id.all()
                for order in orders:
                    exchange_status = False
                    return_status = False
                    status = None
                    product_image = Images.objects.filter(type="main",
                                                          product=order.product_details.product_id).last()
                    image = None
                    if product_image:
                        image = image_thumbnail(product_image.path)
                    order_status = Order.objects.filter(id=order.id)
                    if order_status:
                        order_category = "normal"
                        status = order_status[0].status
                        try:
                            order_category = order_status[
                                0].product_details.product_id.base_category.sub_category.category.name
                        except Exception as e:
                            print("Inside Exception of order category in UserOrderHistoryApi")
                        if order_status[0].status == "s_dispatched":
                            status = "placed"
                        if today_date < order_status[0].updated_date + timedelta(days=7):
                            if order_status[0].status == "delivered":
                                if order_category != "Grocery":
                                    return_status = True
                                exchange_status = True

                    order_dict = {
                        "order_id": order.id,
                        "unique_order_id": order.unique_order_id,
                        "status": status,
                        "exchange_status": exchange_status,
                        "return_status": return_status,
                        "title": order.product_details.product_id.name,
                        "thumbnail": str(image),
                        "total_amount": order.total_amount
                    }
                    order_list.append(order_dict)

                order_details_dict = {
                    "order_details_id": order_details.id,
                    "order_text": order_details.order_text,
                    "status": order_details.status,
                    "credit used": credit,
                    "date": str(order_details.date)[:10],
                    "total_amount": order_details.total_amount,
                    "orders": order_list
                }

                order_details_list.append(order_details_dict)

            return Response(
                {
                    "status": "1",
                    "data": order_details_list
                }
            )
        return Response(
            {
                "status": "2",
                "message": "No orders",
                "data": order_details_list
            }
        )


class SellerReturnRequestOrder(generic.ListView):
    """
    This class is used to render seller and its Return Order object.
    """
    template_name = "Ecom/orders/seller_return_request_order.html"

    def get(self, request, *args, **kwargs):
        ctx = {}
        data_dict = {}
        sellers_list = ReturnOrder.objects.filter(verify_return_status="return_request",
                                                  payment_status="success").values_list(
            'order__product_details__user__username',
            flat=True).distinct()
        if sellers_list:
            for seller in sellers_list:
                sellers_orders = ReturnOrder.objects.filter(order__product_details__user__username=seller,
                                                            verify_return_status="return_request",
                                                            payment_status="success").count()
                data_dict.update({seller: sellers_orders})
                ctx['sellers'] = data_dict
        return render(request, self.template_name, ctx)


class OrderVariantApi(generics.ListAPIView):
    """
    Pre exchange api
    This API takes order id and give products variants related to the product ordered in that order.
    """

    def post(self, request, *args, **kwargs):
        """
        :param request:
            {
                "order_id": 1185,
            }
        :param args:
        :param kwargs:
        :return:
        {
            "status": 1,
            "data": {
                "product_id": 36,
                "title": "G-CORD Headphones",
                "thumbnail": "https://mudrakwik-storage.s3.amazonaws.com/ecom/whatsapp-image-2020-04-29-at-5.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA3AOPBD3KG46TBT62%2F20200629%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20200629T100128Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=afa9ba4e31205ab66a0bd7b60e641cef096d66bd468a3614d74ebffd647d29a1"
            },
            "message": "details found"
        }
        """
        if "order_id" in request.data:
            order_id = request.data['order_id']
            order_dict = {}
            if order_id is not None:
                order_obj = Order.objects.filter(id=order_id, status="delivered")
                if order_obj:
                    image = None
                    product_id = order_obj[0].product_details.product_id.id
                    product_name = order_obj[0].product_details.product_id.name
                    product_image = Images.objects.filter(type="main",
                                                          product=order_obj[0].product_details.product_id.id).order_by(
                        'id').last()
                    if product_image:
                        image = image_thumbnail(product_image.path)
                    order_dict = {
                        "product_id": product_id,
                        "title": product_name,
                        "thumbnail": str(image)
                    }
                    return Response(
                        {
                            'status': 1,
                            'data': order_dict,
                            'message': "details found",
                        }
                    )
                else:
                    return Response(
                        {
                            'status': 2,
                            'data': order_dict,
                            'message': "Order could not be found",
                        }
                    )
            return Response(
                {
                    'status': 3,
                    'message': "Something went wrong",
                }
            )
        else:
            return Response(
                {
                    'status': 4,
                    'message': "order_data is Mandatory",
                }
            )


def get_delivery_charges(product_details_id):
    """
    The function is used to get delivery charge amount of particular product details.
    """
    # Get delivery charge object from product_details_id
    delivery_charge = None
    if product_details_id:
        product_details_obj = ProductDetails.objects.filter(id=product_details_id)
        if product_details_obj:
            get_delivery_charge = DeliveryCharges.objects.filter(
                product=product_details_obj[0].product_id.id).order_by('id').last()
            if get_delivery_charge:
                delivery_charge = get_delivery_charge.amount
            else:
                get_delivery_charge = DeliveryCharges.objects.filter(product=None).order_by('id').last()
                if get_delivery_charge:
                    delivery_charge = get_delivery_charge.amount
    return delivery_charge


def handel_exchange_order_amount_difference_2(previous_order, new_order_id, product_details_id):
    """
    for exchange: gets difference of previous_order, product_details_id and calculates amount to be paid
    :param new_order_id:
    :param previous_order:
    :param product_details_id:
    {
        "previous_order_id": "ORD205291630356501",
        "new_order_id": "ORD205291641183985"
    }
    :return:
    {
        "message": "Amount to be paid: 275.0",
        "status": 2,
        "amount_data": {
            "amount": 275.0,
            "delivery_charges": 75.0
        }
    }

    Or

    {
        "message": "200.0 can be added in your wallet",
        "status": 1,
        "amount_data": {
            "amount": 200.0,
            "delivery_charges": 0
        }
    }
    """
    payment_data = {}
    message = 'Something went wrong'
    status = 5
    final_amount = 0
    delivery_charges = 0

    get_order = Order.objects.filter(id=previous_order)
    if get_order and get_order is not None:
        get_exc_order = Order.objects.filter(id=new_order_id)
        if get_exc_order and get_exc_order is not None:
            amount_data = get_product_details_price(product_details_id)
            if amount_data and amount_data is not None:
                pre_order_amt = get_order[0].total_amount
                exc_order_amt = amount_data['minimum_amount']

                # Get Difference of new and prev order
                amount_difference = float(pre_order_amt) - float(exc_order_amt)

                if amount_difference > 0:
                    # New order amount is less than prev order amt
                    final_amount = amount_difference
                    """    adds amount in wallet for exchanges    """
                    get_order_details_obj = OrderDetails.objects.filter(order_id=get_exc_order[0].order_id)
                    if get_order_details_obj and get_order_details_obj is not None:
                        # Adding the extra amount in the user wallet
                        user_wallet_update(get_order_details_obj[0].user_id, amount_difference,
                                           get_order_details_obj[0], get_order[0], "")

                    message = str(final_amount) + 'rs added in your wallet'
                    status = 1
                else:
                    # prev order amount is less than New order amt
                    # user needs to pay this amt

                    delivery_charges = get_delivery_charges(product_details_id)

                    exchange_amount_to_be_paid = abs(amount_difference) + delivery_charges

                    final_amount = abs(exchange_amount_to_be_paid)
                    message = 'Amount to be paid: ' + str(final_amount)
                    status = 2
    amount_data = {
        "amount": final_amount,
        "delivery_charges": delivery_charges
    }

    payment_data = {
        "message": message,
        "status": status,
        "amount_data": amount_data,
        "product_details_id": product_details_id
    }

    return payment_data


class CalculateExchangeAmountAPIView(APIView):
    """
    Get calculation for exchange order
    :parameter:
        Parameter
        {
            "previous_order_id": "CharField",
            "color": "CharField",
            "size": "CharField",
            "product_id": "CharField",
        }
        Response
        {
            "status": "CharField",                      # 1
            "message": "CharField", 1                   # Valid pan card
        }
        Note
        Authentication token required
        [
            {
                "key":"Authorization",
                "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
            }
        ]
        status 1 : Valid pan card
        status 2 : Invalid pan card ( already used by anothoer user)
        status 10 : Something went wrong (Django Error)
        status 11 : Enter valid data
    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    def post(self, request, *args, **kwargs):
        return_data = None
        previous_order_id = request.data['previous_order_id']
        color = request.data['color']
        size = request.data['size']
        product_id = request.data['product_id']
        get_product_details = ProductDetails.objects.filter(product_id_id=product_id, colour_id__name=color,
                                                            size_id__name=size)
        if get_product_details:
            return_data = calculate_exchange_order_amount_difference_2(previous_order_id, get_product_details[0].id)
        return Response(
            return_data
        )


def calculate_exchange_order_amount_difference_2(previous_order, product_details_id):
    """
    for exchange: gets difference of previous_order, product_details_id and calculates amount to be paid
    :param previous_order:
    :param product_details_id:
    {
        "previous_order_id": "ORD205291630356501",
        "new_order_id": "ORD205291641183985"
    }
    :return:
    {
        "message": "Amount to be paid: 275.0",
        "status": 2,
        "amount_data": {
            "amount": 275.0,
            "delivery_charges": 75.0
        }
    }
    Or
    {
        "message": "200.0 can be added in your wallet",
        "status": 1,
        "amount_data": {
            "amount": 200.0,
            "delivery_charges": 0
        }
    }
    """
    payment_data = {}
    message = 'Something went wrong'
    status = 5
    final_amount = 0
    delivery_charges = 0
    get_order = Order.objects.filter(id=previous_order)
    if get_order and get_order is not None:
        amount_data = get_product_details_price(product_details_id)
        if amount_data and amount_data is not None:
            pre_order_amt = get_order[0].total_amount
            exc_order_amt = amount_data['minimum_amount']
            # Get Difference of new and prev order
            amount_difference = float(pre_order_amt) - float(exc_order_amt)
            # get delivery charges
            delivery_charges = get_delivery_charges(product_details_id)
            if amount_difference > 0:
                # New order amount is less than prev order amt
                exchange_amount_to_be_paid = abs(amount_difference) + delivery_charges
                final_amount = abs(exchange_amount_to_be_paid)
                message = str(final_amount) + ' can be added in your wallet'
                status = 1
            elif amount_difference == 0:
                # New order amount is less than prev order amt
                final_amount = delivery_charges
                message = 'No need to pay any extra amount for product, just pay delivery charges'
                status = 1
            else:
                # prev order amount is less than New order amt
                # user needs to pay this amt
                exchange_amount_to_be_paid = abs(amount_difference) + delivery_charges
                # TODO accept user payment
                final_amount = abs(exchange_amount_to_be_paid)
                message = 'Amount to be paid: ' + str(final_amount)
                status = 2
    amount_data = {
        "amount": final_amount,
        "delivery_charges": delivery_charges
    }
    payment_data = {
        "message": message,
        "status": status,
        "amount_data": amount_data,
        "product_details_id": product_details_id
    }
    return payment_data


class OrderDetailsApi(APIView):
    """
            {
            "status": "1",
            "data": [
                {
                    "order_text": "44146",
                    "image": "",
                    "status": "true",
                    "total_amount": 6000.0,
                    "credit_amount": 3400.0,
                    "credit_paid": 0,
                    "remaming_amount": 3400,
                    "credit_status": null
                },
                {
                    "order_text": "123456789",
                    "image": "ecom/watch_1.jpeg",
                    "status": "placed",
                    "total_amount": 7000.0,
                    "credit_amount": 700.0,
                    "credit_paid": 500.0,
                    "remaming_amount": 200.0,
                    "credit_status": null
                }
            ]
        }
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        order_details_list = []
        product_list = []
        user = request.user.id
        order_details_obj = OrderDetails.objects.filter(user=user).exclude(status="initiated")
        if order_details_obj:
            for order_details in order_details_obj:
                order_list = order_details.order_id.all()
                product_id_list = Order.objects.filter(id__in=order_list).values_list('product_details__product_id',
                                                                                      flat=True)
                image = ""
                if product_id_list:
                    product_id = random.choice(product_id_list)
                    image = Images.objects.filter(product=product_id, type='main').last()
                    if image:
                        try:
                            image = botofile.get_url(image.path)
                        except Exception as e:
                            print("======e", e)
                            image = None
                pay_amount = order_details.payment.filter(product_type="Ecom", status='success',
                                                          category="Repayment").aggregate(Sum('amount'))['amount__sum']
                if pay_amount:
                    pay_amount = pay_amount
                else:
                    pay_amount = 0

                used_credit = 0
                if order_details.credit and order_details.credit is not None:
                    used_credit = order_details.credit.actual_used
                    from EApi.credit import get_order_details_return_cancel_amount
                    # get order cancel and return total amount..
                    total_cancel_return_amount = get_order_details_return_cancel_amount(
                        order_details.id)
                    if total_cancel_return_amount:
                        used_credit = used_credit - total_cancel_return_amount

                remaning_amount = 0
                remaning_amount = int(used_credit) - pay_amount

                if used_credit != 0:
                    order_details_dict = {
                        'order_text': order_details.order_text,
                        'image': image,
                        'status': order_details.status,
                        'total_amount': order_details.total_amount,
                        'credit_amount': used_credit,
                        'credit_paid': pay_amount,
                        'remaming_amount': remaning_amount,
                        'credit_status': order_details.credit_status
                    }
                    order_details_list.append(order_details_dict)
            return Response(
                {
                    'status': "1",
                    'data': order_details_list
                }
            )
        return Response(
            {
                'status': "2",
                'data': order_details_list
            }
        )


def get_delivery_charge(amount, charge_type):
    """
        gives delivery amount as per order amount
        0 - 100 = 25
        101 - 500 = 50
        501 - 1000 = 100
        1000 - 2000 = 150
        > 2001 = 250
    :param amount: orders total amount
    :return: delivery charge amount
    """
    delivery_charge_obj = None
    total_amount = round(amount)
    if charge_type == "normal":
        if total_amount or total_amount == 0:
            if 0 <= total_amount <= 100:
                delivery_charge_obj = DeliveryCharges.objects.filter(amount=25, re_status=True)
            elif 101 <= total_amount <= 500:
                delivery_charge_obj = DeliveryCharges.objects.filter(amount=50, re_status=True)
            elif 501 <= total_amount <= 1000:
                delivery_charge_obj = DeliveryCharges.objects.filter(amount=100, re_status=True)
            elif 1001 <= total_amount <= 2000:
                delivery_charge_obj = DeliveryCharges.objects.filter(amount=150, re_status=True)
            elif total_amount > 2001:
                delivery_charge_obj = DeliveryCharges.objects.filter(amount=250, re_status=True)
            else:
                delivery_charge_obj = DeliveryCharges.objects.filter(amount=75, re_status=True)

    if charge_type == "Grocery":
        if total_amount is not None and total_amount != 0:
            delivery_charge = ceil((total_amount * 15) / 100)
            if delivery_charge:
                delivery_charge_obj = DeliveryCharges.objects.create(amount=delivery_charge, re_status=True,
                                                                     status="APPROVED")
                return delivery_charge_obj

    if delivery_charge_obj:
        return delivery_charge_obj[0]
    else:
        return delivery_charge_obj


from math import ceil


def get_orders_convenient_fees(amount):
    """
    The function is used to return convenient fees.
    """
    convenient_fees = 0
    if amount is not None and amount != 0:
        convenient_fees = ceil((amount * 2.5) / 100)
    return convenient_fees


def handel_undelivered_order(order_id):
    """
    This function calculates all amounts and returns it to the customer in the case of undelivered..
    """
    if order_id is not None:
        previous_undelivered = OrderStatus.objects.filter(order=order_id,
                                                          status="inv_undelivered")
        if not previous_undelivered:
            user_id = None
            total_amount = 0
            today_date = datetime.now()
            get_order_obj = Order.objects.filter(id=order_id)
            if get_order_obj:
                # order total amount
                total_amount = get_order_obj[0].total_amount
                # Here we change the status of the order..
                order_obj = Order.objects.filter(id=order_id)
                if order_obj:
                    order_obj[0].status = "undelivered"
                    order_obj[0].updated_date = today_date
                    order_obj[0].save()

                # Here we change the status of Order_status table.
                add_order_status = OrderStatus.objects.create(order=get_order_obj[0],
                                                              date=datetime.now(),
                                                              status="inv_undelivered")
                # Here we add stock of Product Details..
                stock_status = "add"
                change_by = "undelivered"
                quantity = get_order_obj[0].quantity
                product_details_id = get_order_obj[0].product_details.id
                from ecom.views import update_stock
                stock_update = update_stock(product_details_id, quantity, stock_status, change_by)
                # Here we add order amount in to users credit.
                get_order_details_obj = OrderDetails.objects.filter(order_id=order_id)
                if get_order_details_obj:
                    user_id = get_order_details_obj[0].user.id

                credit_used = 0
                if get_order_details_obj[0].credit:
                    credit_used = get_order_details_obj[0].credit.actual_used
                    from EApi.credit import get_order_details_return_cancel_amount
                    # get order cancel and return total amount..
                    total_cancel_return_amount = get_order_details_return_cancel_amount(get_order_details_obj[0].id)
                    if total_cancel_return_amount:
                        credit_used = credit_used - total_cancel_return_amount

                # Here we are checking amount to be added in credit and amount to be added in User wallet..
                credit_add_amount = None
                wallet_amount = None
                cancel_cashback_amount = None
                payment_mode = get_order_details_obj[0].payment_mode
                if credit_used != 0:
                    # This means User has used credit for this order..
                    if credit_used < total_amount:
                        credit_add_amount = credit_used
                        wallet_amount = total_amount - credit_used
                    else:
                        credit_add_amount = total_amount
                else:
                    # This means User did not used credit for this order..
                    if payment_mode == "cash":
                        discount = 0
                        order_date = str(get_order_details_obj[0].created_date)[:10]
                        is_big_offer = get_big_offer_status(order_date)
                        is_big_offer_new = get_big_offer_status_new(order_date)
                        is_zestmony_offer = get_zest_money_offer(get_order_details_obj[0])

                        if is_big_offer:
                            discount = get_orders_cashback_amount(get_order_obj[0].id)

                        elif is_big_offer_new or is_zestmony_offer:
                            discount_percentage = "fifty"
                            discount = get_orders_cashback_amount(get_order_obj[0].id, discount_percentage)

                        else:
                            cashback_amount = get_order_obj[0].total_amount
                            # This function is for 15% cashback..
                            discount = get_cashback_amount(cashback_amount)

                        if discount > 0:
                            # Deduct extra 15% amount from total return amount before giving to the wallet..
                            cancel_cashback_amount = discount
                        wallet_amount = total_amount
                    else:
                        wallet_amount = total_amount
                delivery_charge = None
                if get_order_obj[0].delivery_charges:
                    delivery_charge = get_order_obj[0].delivery_charges.amount
                if credit_add_amount is not None:
                    if payment_mode != "cod":
                        status = "undelivered"
                        from EApi.credit import credit_balance_update
                        add_credit = credit_balance_update(user_id, credit_add_amount, status)
                        if add_credit:
                            # Adding new credit entry in Credit history of Order Details Table..
                            get_order_details_obj[0].credit_history.add(add_credit)

                from EApi.wallet import user_wallet_update
                if wallet_amount is not None:
                    if payment_mode != "cod":
                        status = "order_undelivered"
                        add_wallet = user_wallet_update(user_id, wallet_amount, get_order_details_obj[0],
                                                        get_order_obj[0],
                                                        status)

                from EApi.wallet import user_wallet_sub
                if delivery_charge:
                    status = "undelivered_delivery_charge"
                    add_wallet = user_wallet_sub(user_id, delivery_charge, get_order_details_obj[0], get_order_obj[0],
                                                 status)

                if cancel_cashback_amount:
                    if payment_mode != "cod":
                        status = "cancel_cashback"
                        add_wallet = user_wallet_sub(user_id, cancel_cashback_amount, get_order_details_obj[0],
                                                     get_order_obj[0], status)

                return True

            else:
                return False


def is_placed_pending(order_details_obj, order_quantity):
    """
    This function returns is order is in condition of placed pending or not. Input parameter to this function is order details object and order quantity.

    """
    placed_pending = False
    if None not in (order_details_obj, order_quantity):
        if order_details_obj.payment_mode == "credit":
            user_id = order_details_obj.user.id
            credit = 0
            credit_used = 0
            total_used_till = 0
            credit_obj = Credit.objects.filter(user=user_id).exclude(status="initiated").order_by('id').last()
            if credit_obj:
                credit = credit_obj.credit
                total_used_till = credit_obj.used_credit

            try:
                if credit > 2000:
                    used_percentage = (100 * total_used_till) / credit
                    if used_percentage >= 60 or order_quantity > 2:
                        placed_pending = True
                        return placed_pending
            except Exception as e:
                print("Inside Exception of is_placed_pending function", e)
                return placed_pending
        else:
            return placed_pending

    return placed_pending


class UniqueIdStatus(generics.ListAPIView):
    """
    This API return unique_order_id, status and order_id(primary_key) of order_text

    name : UniqueIdStatus

    method : post

    authentication : required   (token b4683a8182d85dfb5106182c381aceb36d836f2a)

    parameters :

            "order_text" : 	"ORD2010211530187527567" (string)

    response :

            success :

                    {
                        "status": 1,
                        "message": "success",
                        "data": [
                            {
                                "unique_order_id": "UNI2010211530189625761",
                                "status": "initiated",
                                "order_id": 15475
                            },
                            {
                                "unique_order_id": "UNI2010211530181982774",
                                "status": "initiated",
                                "order_id": 15476
                            },
                            {
                                "unique_order_id": "UNI2010211530185468894",
                                "status": "initiated",
                                "order_id": 15477
                            }
                        ]
                    }

            error :

                    {
                        "status": 2,
                        "message": "No data found",
                        "data": []
                    }
    """
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        data_list = []
        status = 2
        message = ""
        if "order_text" in request.data:
            order_text = request.data.get("order_text")
            if order_text and order_text is not None:
                order_details_obj = OrderDetails.objects.filter(order_text=order_text).last()
                if order_details_obj:
                    res = order_details_obj.order_id.values_list("status", "unique_order_id", "id")
                    if res:
                        for data in res:
                            data_dict = {
                                "unique_order_id": data[1],
                                "status": data[0],
                                "order_id": data[2]
                            }

                            if data_dict:
                                data_list.append(data_dict)

                        message = "success"
                        status = 1

                else:
                    message = "Order text not found"

            else:
                message = "order Text could not be None"

        else:
            message = "Provide Order Text"

        return Response(
            {
                "status": status,
                "message": message,
                "data": data_list
            }
        )


def return_wallet_amount_undelivered_order(request, unique_order_id):
    """
    The function is used to return wallet amount of undelivered order of particular order id.
    """
    if unique_order_id is not None:
        get_order_obj = Order.objects.filter(unique_order_id=unique_order_id)
        if get_order_obj:
            order_undelivered = handel_undelivered_order(get_order_obj[0].id)
            if order_undelivered:
                return HttpResponse("Order Undelivered")
            else:
                return HttpResponse("Not Okay")
        else:
            return HttpResponse("Order id not found")
    else:
        return HttpResponse("Order id should not None")
