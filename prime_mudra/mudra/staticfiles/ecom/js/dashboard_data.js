	$(document).ready(function () {
		// put Ajax here.
			loan_data_1();
			loan_data_2();
			loan_data_3();
			loan_data_4();
			loan_data_5();
			loan_data_6();
			loan_data_7();
			loan_data_8();
			load_data_9();
            loan_data_10();
            loan_data_11();
            loan_data_12();
            loan_data_13();
            loan_data_14();
            loan_data_15();
	});



	function loan_data_1() {

		$.ajax({
			url: "/ecom/get_dashboard_data_1/",
			type: 'GET',
			dataType: 'json', // added data type
			success: function (res, status) {
				process_data(res)
			},
			error: function (data) {
				console.log("===========", data)
			}
		});
	}

// process data and display
function process_data(data) {

    var element1 = document.getElementById("total_orders");
	if (element1) {
		document.getElementById("total_orders").innerText = data.total_orders;
		        $("#total_orders_loader").remove();

	}

	var element2 = document.getElementById("placed_orders");
	if (element2) {
		document.getElementById("placed_orders").innerText = data.placed_orders;
				        $("#placed_orders_loader").remove();

	}

	var element3 = document.getElementById("delivered_orders");
	if (element3) {
		document.getElementById("delivered_orders").innerText = data.delivered_orders;
						        $("#delivered_orders_loader").remove();

	}


	var element4 = document.getElementById("p_p");
	if (element4) {
		document.getElementById("p_p").innerText = data.pending_orders;
								        $("#p_p_loader").remove();

	}

	var elements = document.getElementById("dispatch_orders");
    if (elements) {
    document.getElementById("dispatch_orders").innerText = data.dispatch_orders;
    								        $("#dispatch_orders_loader").remove();

    }

    var undelivered_orders = document.getElementById("undelivered_orders");
	if (undelivered_orders) {
		document.getElementById("undelivered_orders").innerText = data.undelivered_orders;
		           								        $("#undelivered_orders_loader").remove();

	}

	var cancel_elementExists = document.getElementById("cancelled_order");
	if (cancel_elementExists) {
		document.getElementById("cancelled_order").innerText = data.cancelled_order;
		       				$("#cancelled_order_loader").remove();

	}

	var initiated_orders_elementExists = document.getElementById("initiated_orders");
	if (initiated_orders_elementExists) {
		document.getElementById("initiated_orders").innerText = data.initiated_orders;
		$("#initiated_orders_loader").remove();
	}

	var orders_count = document.getElementById("orders_count");
    if (orders_count) {
        document.getElementById("orders_count").innerText = data.orders_count;
                				        $("#orders_count_loader").remove();

        }

}









	function loan_data_2() {
		$.ajax({
			url: "/ecom/get_dashboard_data_2/",
			type: 'GET',
			dataType: 'json', // added data type
			success: function (res, status) {
				process_data2(res)
			},
			error: function (data) {
				console.log("===========", data)
			}
		});
	}

// process data and display
function process_data2(data) {
    var elementExists1 = document.getElementById("return_orders");
	if (elementExists1) {
		document.getElementById("return_orders").innerText = data.return_orders;
				           								        $("#return_orders_loader").remove();

	}


	var elementExists3 = document.getElementById("logistics_orders");
	if (elementExists3) {
		document.getElementById("logistics_orders").innerText = data.logistics_orders;
						           								        $("#logistics_orders_loader").remove();

	}

	var elementExists4 = document.getElementById("all_products");
	if (elementExists4) {
		document.getElementById("all_products").innerText = data.all_products;
		$("#all_products_loader").remove();

	}
	var elementExists5 = document.getElementById("profit");
	if (elementExists5) {
		document.getElementById("profit").innerText = data.profit;
				$("#profit_loader").remove();

	}

	var elementExists6 = document.getElementById("packed_logistics_orders");
    if (elementExists6) {
       document.getElementById("packed_logistics_orders").innerText = data.packed_logistics_orders;
       				$("#packed_logistics_orders_loader").remove();

    }



}



	function loan_data_3() {
		$.ajax({
			url: "/ecom/get_dashboard_data_3/",
			type: 'GET',
			dataType: 'json', // added data type
			success: function (res, status) {
				process_data3(res);
			},
			error: function (data) {
				console.log("====process_data3=======", data)
			}
		});
	}

// process data and display
function process_data3(data) {

    var seler_amt_elementExists = document.getElementById("over_dues_total");
	if (seler_amt_elementExists) {
		document.getElementById("over_dues_total").innerText = data.over_dues_total;
		$("#over_dues_total_loader").remove();

	}


	var elementExists = document.getElementById("total_product_details1");
	if (elementExists) {
		document.getElementById("total_product_details1").innerText = data.total_product_details;
				       				$("#total_product_details1_loader").remove();

	}


	var seller_elementExists = document.getElementById("return_confirm");
	if (seller_elementExists) {
		document.getElementById("return_confirm").innerText = data.return_confirm;
								       				$("#return_confirm_loader").remove();

	}




	var seler_amt_elementExists = document.getElementById("seller_amount");
	if (seler_amt_elementExists) {
		document.getElementById("seller_amount").innerText = data.seller_amount;
										       				$("#seller_amount_loader").remove();

	}






}









	function loan_data_4() {
		$.ajax({
			url: "/ecom/get_dashboard_data_4/",
			type: 'GET',
			dataType: 'json', // added data type
			success: function (res, status) {
				process_data4(res)
			},
			error: function (data) {
				console.log("process_data4===========", data)
			}
		});
	}

// process data and display
function process_data4(data) {


	var sub_prod_elementExists = document.getElementById("submitted_products");
	if (sub_prod_elementExists) {
		document.getElementById("submitted_products").innerText = data.submitted_products;
				$("#submitted_products_loader").remove();

	}

	var total_users_elementExists = document.getElementById("total_users");
	if (total_users_elementExists) {
		document.getElementById("total_users").innerText = data.total_users;
						$("#total_users_loader").remove();

	}

	var total_buyer_elementExists = document.getElementById("total_buyer");
	if (total_buyer_elementExists) {
		document.getElementById("total_buyer").innerText = data.total_buyer;
								$("#total_buyer_loader").remove();

	}

	var order_amount = document.getElementById("order_amount");
        if (order_amount) {
    document.getElementById("order_amount").innerText = data.order_amount;
    								$("#order_amount_loader").remove();

    }

    var final_seller_amount = document.getElementById("final_seller_amount");
    if (final_seller_amount) {
        document.getElementById("final_seller_amount").innerText = data.final_seller_amount;
            								$("#final_seller_amount_loader").remove();

    }

    var convinient_fees = document.getElementById("Convinient");
    if (convinient_fees) {
        document.getElementById("Convinient").innerText = data.convinient_fees;
        $("#Convinient_loader").remove();

    }



}










	function loan_data_5() {
		$.ajax({
			url: "/ecom/get_dashboard_data_5/",
			type: 'GET',
			dataType: 'json', // added data type
			success: function (res, status) {
				process_data5(res)
			},
			error: function (data) {
				console.log("process_data4===========", data)
			}
		});
	}

// process data and display
function process_data5(data) {


	var rep_amt_elementExists = document.getElementById("total_repayment_amount");
	if (rep_amt_elementExists) {
		document.getElementById("total_repayment_amount").innerText = data.total_repayment_amount;
		        $("#total_repayment_amount_loader").remove();

	}


	var return_request_elementExists = document.getElementById("return_request");
	if (return_request_elementExists) {
		document.getElementById("return_request").innerText = data.return_request;
				        $("#return_request_loader").remove();

	}


    var return_request_elementExists = document.getElementById("return_request");
    if (return_request_elementExists) {
        document.getElementById("return_request").innerText = data.return_request;
        				        $("#return_request_loader").remove();

        }



    var credit_elementExists = document.getElementById("total_credit");
        if (credit_elementExists) {
                document.getElementById("total_credit").innerText = data.total_credit;
                                				        $("#total_credit_loader").remove();

        }

    var shipping_fees = document.getElementById("shipping_fees");
    if (return_request_elementExists) {
       document.getElementById("shipping_fees").innerText = data.shipping_fees;
       $("#shipping_fees_loader").remove();

    }

    var net_receivables = document.getElementById("net_receivables");
    if (net_receivables) {
        document.getElementById("net_receivables").innerText = data.net_receivables;
               $("#net_receivables_loader").remove();

    }

}










	function loan_data_6() {
		$.ajax({
			url: "/ecom/get_dashboard_data_6/",
			type: 'GET',
			dataType: 'json', // added data type
			success: function (res, status) {
				process_data6(res)
			},
			error: function (data) {
				console.log("process_data6===========", data)
			}
		});
	}

// process data and display
function process_data6(data) {

        var wallet_amount = document.getElementById("wallet_amount");
        if (wallet_amount) {
                document.getElementById("wallet_amount").innerText = data.wallet_amount;
                        $("#wallet_amount_loader").remove();

        }


        var net_profit = document.getElementById("net_profit");
        if (net_profit) {
            document.getElementById("net_profit").innerText = data.total;
                           $("#net_profit_loader").remove();

        }

        var net_pay = document.getElementById("net_pay");
        if (net_pay) {
                document.getElementById("net_pay").innerText = data.net_pay;
                                        $("#net_pay_loader").remove();

        }

        var in_transit_logistics_orders = document.getElementById("in_transit_logistics_orders");
        if (in_transit_logistics_orders) {
           document.getElementById("in_transit_logistics_orders").innerText = data.in_transit_logistics_orders;
                                                   $("#in_transit_logistics_orders_loader").remove();

        }


}

function loan_data_7() {
		$.ajax({
			url: "/ecom/get_dashboard_data_7/",
			type: 'GET',
			dataType: 'json', // added data type
			success: function (res, status) {
				process_data7(res)
			},
			error: function (data) {
				console.log("process_data7===========", data)
			}
		});
	}


// process data and display
function process_data7(data) {

        var seller_elementExists = document.getElementById("sellers_count");
	    if (seller_elementExists) {
		document.getElementById("sellers_count").innerText = data.sellers_count;
						       				$("#sellers_count_loader").remove();

	    }

        var bill_amount = document.getElementById("bill_amount");
        if (bill_amount) {
                document.getElementById("bill_amount").innerText = data.bill_amount;
                                                                   $("#bill_amount_loader").remove();

        }


        var total_payment_received = document.getElementById("total_payment_received");
        if (total_payment_received) {
            document.getElementById("total_payment_received").innerText = data.total_payment_received;
            $("#total_payment_received_loader").remove();

        }

        var net_due = document.getElementById("net_due");
        if (net_due) {
                document.getElementById("net_due").innerText = data.net_due;
                            $("#net_due_loader").remove();

        }




}



function loan_data_8() {
		$.ajax({
			url: "/ecom/get_dashboard_data_8/",
			type: 'GET',
			dataType: 'json', // added data type
			success: function (res, status) {
				process_data8(res)
			},
			error: function (data) {
				console.log("process_data6===========", data)
			}
		});
	}

function process_data8(data) {

        var total_penalty = document.getElementById("total_penalty");
        if (total_penalty) {
                document.getElementById("total_penalty").innerText = data.total_penalty;
                                            $("#total_penalty_loader").remove();

        }
        var total_unused_credit = document.getElementById("total_unused_credit");
        if (total_unused_credit) {
                document.getElementById("total_unused_credit").innerText = data.total_unused_credit;
                                                            $("#total_unused_credit_loader").remove();

        }

}


function load_data_9() {
		$.ajax({
			url: "/ecom/get_dashboard_data_9/",
			type: 'GET',
			dataType: 'json', // added data type
			success: function (res, status) {
				process_data_9(res)
			},
			error: function (data) {
				console.log("process_data6===========", data)
			}
		});
	}

function process_data_9(data) {


        var get_cod_order_count = document.getElementById("cod");
        if (get_cod_order_count) {
                document.getElementById("cod").innerText = data.get_cod_order_count;
                                            $("#cod_loader").remove();
        }



        console.log("===============", data.payment_amount);
        var payment_amount = document.getElementById("payment_amount");
        if (payment_amount) {
                document.getElementById("payment_amount").innerText = data.payment_amount;
                $("#payment_amount_loader").remove();

        }

        var get_cod_order_amount = document.getElementById("cod_amount");
        if (get_cod_order_amount) {
                document.getElementById("cod_amount").innerText = data.cod_order_amount;
                                            $("#cod_amount_loader").remove();
        }


        }


function loan_data_10() {
		$.ajax({
			url: "/ecom/get_dashboard_data_10/",
			type: 'GET',
			dataType: 'json', // added data type
			success: function (res, status) {
				process_data_10(res)
			},
			error: function (data) {
				console.log("===========", data)
			}
		});
	}

// process data and display
function process_data_10(data) {

    var elementExists1 = document.getElementById("packed_mfpl_logistics_order_details");
    if (elementExists1) {
       document.getElementById("packed_mfpl_logistics_order_details").innerText = data.packed_mfpl_logistics_order_details;
       				$("#packed_mfpl_logistics_order_details_loader").remove();

    }

    var elementExists2 = document.getElementById("mfpl_placed_order_details");
    if (elementExists2) {
       document.getElementById("mfpl_placed_order_details").innerText = data.mfpl_placed_order;
       				$("#mfpl_placed_order_details_loader").remove();

    }

	}

function loan_data_11() {
		$.ajax({
			url: "/ecom/get_dashboard_data_11/",
			type: 'GET',
			dataType: 'json', // added data type
			success: function (res, status) {
				process_data_11(res)
			},
			error: function (data) {
				console.log("===========", data)
			}
		});
	}

// process data and display
function process_data_11(data) {

    var elementExists1 = document.getElementById("defaulter_count");
    if (elementExists1) {
       document.getElementById("defaulter_count").innerText = data.defaulter_count;
       				$("#defaulter_count_loader").remove();

    }

    var elementExists2 = document.getElementById("recovery_count");
    if (elementExists2) {
       document.getElementById("recovery_count").innerText = data.recovery_count;
       				$("#recovery_count_loader").remove();

    }

    var elementExists3 = document.getElementById("defaulter_amount");
    if (elementExists3) {
       document.getElementById("defaulter_amount").innerText = data.defaulter_amount;
       				$("#defaulter_amount_loader").remove();

    }

    var elementExists4 = document.getElementById("insentive_amount");
    if (elementExists4) {
       document.getElementById("insentive_amount").innerText = data.insentive_amount;
       				$("#insentive_amount_loader").remove();

    }

    var elementExists5 = document.getElementById("insentive_topper");
    var elementExists6 = document.getElementById("insentive_topper_name");
    if (elementExists5 && elementExists6) {
       document.getElementById("insentive_topper").innerText = data.insentive_amount_top;
       document.getElementById("insentive_topper_name").innerText = data.insentive_amount_name;
       $("#insentive_topper_loader").remove();

    }

    var elementExists7 = document.getElementById("call_topper");
    var elementExists8 = document.getElementById("call_topper_name");
    if (elementExists7 && elementExists8) {
       document.getElementById("call_topper").innerText = data.call_topper_count;
       document.getElementById("call_topper_name").innerText = data.call_topper_name;
       $("#call_topper_loader").remove();

    }

    var elementExists9 = document.getElementById("call_count");
    if (elementExists9) {
       document.getElementById("call_count").innerText = data.call_count;
       				$("#call_count_loader").remove();

    }

    var elementExists10 = document.getElementById("recovery_amount");
    if (elementExists10) {
       document.getElementById("recovery_amount").innerText = data.recovery_amount;
       				$("#recovery_amount_loader").remove();

    }

	}







function loan_data_12() {
		$.ajax({
			url: "/ecom/get_dashboard_data_12/",
			type: 'GET',
			dataType: 'json', // added data type
			success: function (res, status) {
				process_data_12(res)
			},
			error: function (data) {
				console.log("===========", data)
			}
		});
	}

// process data and display
function process_data_12(data) {

    var elementExists1 = document.getElementById("rejected_loan_application");
    if (elementExists1) {
       document.getElementById("rejected_loan_application").innerText = data.rejected_loan_application;
       				$("#rejected_loan_application_loader").remove();

    }

    var elementExists2 = document.getElementById("approved_loan_application");
    if (elementExists2) {
       document.getElementById("approved_loan_application").innerText = data.approved_loan_application;
       				$("#approved_loan_application_loader").remove();

    }
  var elementExists3 = document.getElementById("all_loan_application");
    if (elementExists3) {
       document.getElementById("all_loan_application").innerText = data.all_loan_application;
       				$("#all_loan_application_loader").remove();

    }
  var elementExists4 = document.getElementById("new_loan_submitted");
    if (elementExists4) {
       document.getElementById("new_loan_submitted").innerText = data.new_loan_submitted;
       				$("#new_loan_submitted_loader").remove();

    }

	}






function loan_data_13() {
		$.ajax({
			url: "/ecom/get_dashboard_data_13/",
			type: 'GET',
			dataType: 'json', // added data type
			success: function (res, status) {
				process_data_13(res)
			},
			error: function (data) {
				console.log("===========", data)
			}
		});
	}

// process data and display
function process_data_13(data) {

    var elementExists1 = document.getElementById("due_amount");
    if (elementExists1) {
       document.getElementById("due_amount").innerText = data.due_amount;
       				$("#due_amount_loader").remove();

    }

    var elementExists2 = document.getElementById("disbursed_amount");
    if (elementExists2) {
       document.getElementById("disbursed_amount").innerText = data.disbursed_amount;
       				$("#disbursed_amount_loader").remove();

    }
  var elementExists3 = document.getElementById("completed_loan_amount");
    if (elementExists3) {
       document.getElementById("completed_loan_amount").innerText = data.completed_loan_amount;
       				$("#completed_loan_amount_loader").remove();

    }
  var elementExists4 = document.getElementById("installment");
    if (elementExists4) {
       document.getElementById("installment").innerText = data.installment;
       				$("#installment_loader").remove();

    }
    var elementExists5 = document.getElementById("completed_loan_profit");
    if (elementExists5) {
       document.getElementById("completed_loan_profit").innerText = data.completed_loan_profit;
       				$("#completed_loan_profit_loader").remove();

    }
    var elementExists6 = document.getElementById("completed_loan_count");
    if (elementExists6) {
       document.getElementById("completed_loan_count").innerText = data.completed_loan_count;
       				$("#completed_loan_count_loader").remove();

    }

	}










function loan_data_14() {
		$.ajax({
			url: "/ecom/get_dashboard_data_14/",
			type: 'GET',
			dataType: 'json', // added data type
			success: function (res, status) {
				process_data_14(res)
			},
			error: function (data) {
				console.log("===========", data)
			}
		});
	}

// process data and display
function process_data_14(data) {

    var elementExists1 = document.getElementById("overdue_amount");
    if (elementExists1) {
       document.getElementById("overdue_amount").innerText = data.overdue_amount;
       				$("#overdue_amount_loader").remove();

    }

    var elementExists2 = document.getElementById("overdue_count");
    if (elementExists2) {
       document.getElementById("overdue_count").innerText = data.overdue_count;
       				$("#overdue_count_loader").remove();

    }
  var elementExists3 = document.getElementById("delinquency");
    if (elementExists3) {
       document.getElementById("delinquency").innerText = data.delinquency;
       				$("#delinquency_loader").remove();

    }
  var elementExists4 = document.getElementById("net_profit_1");
    if (elementExists4) {
       document.getElementById("net_profit_1").innerText = data.net_profit;
       				$("#net_profit_loader_1").remove();

    }

	}


function loan_data_15() {
		$.ajax({
			url: "/ecom/get_dashboard_data_15/",
			type: 'GET',
			dataType: 'json', // added data type
			success: function (res, status) {
				process_data_15(res)
			},
			error: function (data) {
				console.log("===========", data)
			}
		});
	}

// process data and display
function process_data_15(data) {

    var elementExists1 = document.getElementById("phone_pe");
    if (elementExists1) {
       document.getElementById("phone_pe").innerText = data.phone_pe;
       				$("#phone_pe_cancel_order_loader").remove();

    }


	}







