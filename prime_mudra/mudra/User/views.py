import csv

import pandas as pd
from django.apps import apps
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render, HttpResponse
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import generic
from django_pandas.io import read_frame

from .models import *

LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
Payment = apps.get_model('Payments', 'Payment')
AutoApproved = apps.get_model('LoanAppData', 'AutoApproved')
PanCardMaster = apps.get_model('UserData', 'PanCardMaster')


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class SalaryWise_Report(generic.ListView):
    template_name = "admin/UserAccounts/salary_wise_by_user.html"
    title = 'Daily Disbursment'

    def get(self, request, *args, **kwargs):
        ctx = {}
        try:
            userloantransaction = Personal.objects.all()
            df = read_frame(userloantransaction)
            data_df = df.groupby(df['monthly_income'])['mobile_no'].count().sort_index(axis=0)
            create_dated = data_df.index
            count = data_df.values
            ctx['users'] = zip(create_dated, count)
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class SalaryWise_Report_All_User(generic.ListView):
    template_name = "admin/UserAccounts/salary_wise_by_user_all.html"
    title = 'Daily Disbursment'

    def get(self, request, income, *args, **kwargs):
        ctx = {}
        try:

            userloantransaction = Personal.objects.filter(monthly_income=float(income))
            ctx['users'] = userloantransaction
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class NotSubscribedUsers(generic.ListView):
    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="Not_subscribed_users_2019.csv"'
        writer = csv.writer(response)
        writer.writerow(['Username', 'Date Joined'])
        sub_users = Subscription.objects.filter(payment_status="success")
        non_sub_users = User.objects.exclude(id__in=sub_users)
        if non_sub_users:
            for user in non_sub_users:
                writer.writerow([user.username, user.date_joined])
        return response


def payment_page(request):
    return render(request, "admin/UserAccounts/payment.html")


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class pinget(generic.ListView):
    '''
    Function to get mobile number and operation
​
    '''

    template_name = "admin/UserAccounts/get_pincode_data.html"
    title = 'Daily Disbursment'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, )


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class getdata(generic.ListView):
    '''
    function to get data as per dropdown
​
    '''
    template_name = "admin/UserAccounts/pindata.html"
    template_name1 = "admin/UserAccounts/get_subdata.html"

    def post(self, request, *args, **kwargs):
        ctx = {}

        if request.method == "POST":
            number = request.POST['number']
            status = request.POST['id_status']
            user = User.objects.filter(username=number)
            if user:

                if status == 'pin':
                    pindata = AuthenticationPin.objects.filter(user_id=user[0].id)
                    ctx['pindata'] = pindata[0].pin
                    ctx['number'] = number
                    return render(request, self.template_name, ctx)

                if status == 'Subscription':
                    sub = Subscription.objects.filter(user_id=user[0].id, )
                    ctx['sub'] = sub
                    return render(request, self.template_name1, ctx)

                if status == "autoapproved":
                    auto_app = AutoApproved.objects.filter(user_id=user[0].id)
                    ctx['auto'] = auto_app
                    return render(request, self.template_name1, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class get_status(generic.ListView):
    '''
    Redirecting to subscription status change page
​
    '''

    template_name = "admin/UserAccounts/sub_status_update.html"

    def get(self, request, order_id, *args, **kwargs):
        ctx = {}
        try:
            ctx['order_id'] = order_id

        except:
            pass
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class update_status(generic.ListView):
    '''
​
​
    Update subscription status
​
    '''

    template_name = "admin/UserAccounts/get_subdata.html"

    def post(self, request, *args, **kwargs):
        ctx = {}

        if request.method == "POST":
            oid = request.POST['order_id']
            status = request.POST['id_status']
            pay = Payment.objects.filter(order_id=oid)
            if pay:
                Subscription.objects.filter(payment_id_id=pay[0].id).update(payment_status=status)
                sub = Subscription.objects.filter(payment_id_id=pay[0].id)
                sub1 = Subscription.objects.filter(user_id=sub[0].user_id)
                ctx['sub'] = sub1
                return render(request, self.template_name, ctx)

            return HttpResponse("Wrong Order Id")



def subscriptions_check(request):
    """
        Reference https://www.pythoncircle.com/post/30/how-to-upload-and-process-the-csv-file-in-django/
    :param request:
    :return:
    """
    data = {}
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="user_subscriptions.csv"'
    writer = csv.writer(response)
    writer.writerow(
        ['Mobile no', 'Sub start date', 'Sub start Time', 'Sub end date', 'Sub end time', 'status', 'Amount'])

    if "GET" == request.method:
        return render(request, "admin/UserAccounts/subscriptions_check.html", data)
    # if not GET, then proceed
    mobile_list = []
    message = ''
    csv_file = request.FILES["csv_file"]
    if not csv_file.name.endswith('.csv'):
        messages.error(request, 'File is not CSV type')
        return HttpResponseRedirect(reverse("subscriptions_check"))
    # if file is too large, return
    if csv_file.multiple_chunks():
        messages.error(request, "Uploaded file is too big (%.2f MB)." % (csv_file.size / (1000 * 10000),))
        return HttpResponseRedirect(reverse("subscriptions_check"))

    file_data = csv_file.read().decode("utf-8")
    lines = file_data.split("\n")
    if lines:
        for line in lines:
            if line:
                fields = line.split(",")
                if fields:
                    if fields and fields[0] != 'Mobile No':
                        mobile_list.append(fields[0])
    if mobile_list is not None:
        for mob_no in mobile_list:
            subscriptions = Subscription.objects.filter(user__username=mob_no,
                                                        payment_status__icontains="success")
            if subscriptions:
                for sub in subscriptions:
                    #start_date = (sub.start_date).strftime("%Y-%m-%d")
                    writer.writerow(
                        [sub.user.username, str(sub.start_date)[10:], str(sub.start_date)[10:],
                         str(sub.expiry_date)[:10],
                         str(sub.expiry_date)[10:], sub.payment_status,
                         sub.payment_id.amount])
    return response
