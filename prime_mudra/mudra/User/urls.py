from django.urls import path

from .views import *

urlpatterns = [

    path('salary_wise_report/', SalaryWise_Report.as_view(), name='salary_wise_report'),
    path('not-sub-users/', NotSubscribedUsers.as_view(), name='not_sub_users'),
    path('pindata/', getdata.as_view(), name='pindata'),
    path('select_operation/', pinget.as_view(), name='getpin'),
    path('get_status/<order_id>/', get_status.as_view(), name='update_status'),
    path('update_status/', update_status.as_view(), name='update_status'),

    # Razorpay trial
    path('payment/', payment_page, name='payment_page'),
    # sub check csv upload
    path('subscriptions_check_csv/', subscriptions_check, name='subscriptions_check_csv'),

]
