# api/tests.py

# Add these imports at the top
from django.test import TestCase
from django.urls import reverse

from rest_framework.test import APIClient
from rest_framework import status


# Define this after the ModelTestCase
class ViewTestCase(TestCase):
    """Test suite for the api views."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.client = APIClient()
        self.banklist_data = {'bank_name': 'Go to Ibiza', 'account_number': 'test', 'ifsc_code': 'test'}
        self.response = self.client.post(
            reverse('create'),
            self.banklist_data,
            format="json")

    def test_api_can_create_a_bucketlist(self):
        """Test the api has bucket creation capability."""
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)
