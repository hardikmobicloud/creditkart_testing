from django.conf import settings
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.auth.models import Group

from .models import *


# Register your models here.


class AuthenticationPinAdmin(admin.ModelAdmin):
    list_display = ('user', 'pin',)
    search_fields = ('user__username',)


class OtpAdmin(admin.ModelAdmin):
    list_display = ('user', 'otp', 'is_expired', 'created_date')
    search_fields = ('user__username',)


class DocumentAdmin(admin.ModelAdmin):
    list_display = ('user', 'type', 'path', 'created_date')
    list_filter = ('user', 'type')
    search_fields = ('user__username',)


class DocumentAdmin(admin.ModelAdmin):
    list_display = ('user', 'type', 'path', 'created_date')
    list_filter = ['type']
    search_fields = ('user__username',)


class PersonalDetailsAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'birthdate', 'gender', 'marital_status', 'occupation', 'mobile_no', 'alt_mobile_no', 'monthly_income',
        'pan_number', 'aadhar_number', 'type', 'invitations', 'email_id', 'itr', 'mobile_linked_aadhaar', 'residence_type',
        'industry_type',
        'educational')
    exclude = ('updated_date', 'is_approved', 'is_submitted', 'loan', 'is_deleted',)
    search_fields = ('user__username', 'name', 'alt_mobile_no', 'pan_number', 'aadhar_number')
    list_filter = ['occupation', 'type']

    # def has_add_permission(self, request):
    #
    #     if request.user.email in settings.SUPER_ADMIN_EMAIL:
    #
    #         return True
    #     else:
    #
    #         return False
    #
    # def has_delete_permission(self, request, obj=None):
    #
    #     if request.user.email in settings.SUPER_ADMIN_EMAIL:
    #
    #         return True
    #     else:
    #
    #         return False
    #
    # def has_change_permission(self, request, obj=None):
    #
    #     if request.user.email in settings.SUPER_ADMIN_EMAIL:
    #
    #         return True
    #     else:
    #
    #         return False


class ProfessionalAdmin(admin.ModelAdmin):
    list_display = (
        'user', 'company_name', 'company_email_id', 'company_address', 'company_landline_no', 'company_designation',
        'experience', 'join_date')
    exclude = ('updated_date', 'created_date')
    search_fields = ('user__username',)

    # def has_add_permission(self, request):
    #
    #     if request.user.email in settings.SUPER_ADMIN_EMAIL:
    #
    #         return True
    #     else:
    #
    #         return False
    #
    # def has_delete_permission(self, request, obj=None):
    #
    #     if request.user.email in settings.SUPER_ADMIN_EMAIL:
    #
    #         return True
    #     else:
    #
    #         return False
    #
    # def has_change_permission(self, request, obj=None):
    #
    #     if request.user.email in settings.SUPER_ADMIN_EMAIL:
    #
    #         return True
    #     else:
    #
    #         return False


class ReferenceAdmin(admin.ModelAdmin):
    list_display = (
        'user', 'colleague_name', 'colleague_mobile_no', 'colleague_email_id', 'relationships', 'relative_name',
        'relative_mobile_no',)
    exclude = (
        'updated_date', 'created_date',
    )
    search_fields = ('user__username',)

    def has_add_permission(self, request):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return True
        else:

            return False

    def has_delete_permission(self, request, obj=None):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return True
        else:

            return False

    def has_change_permission(self, request, obj=None):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return True
        else:

            return False


class DocumentsAdmin(admin.ModelAdmin):
    list_display = ('user', 'residence_address_proof', 'residence_address_type',)
    exclude = (
        'updated_date', 'is_approved', 'is_submitted',
        'is_deleted', 'loan',
        'shop_establishment_certification',
        'bank_statement', 'adhar_card', 'address_proof_aahar', 'address_proof_credit_card_bill',
        'address_proof_electricity_bill', 'address_proof_telephone_bill', 'address_proof_voter_id',
        'address_proof_passport',
    )
    search_fields = ('user__username',)

    def has_add_permission(self, request):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return True
        else:

            return False

    def has_delete_permission(self, request, obj=None):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return True
        else:

            return False

    def has_change_permission(self, request, obj=None):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return True
        else:

            return False


class BankAdmin(admin.ModelAdmin):
    list_display = ('user', 'bank_name', 'account_number', 'ifsc_code',)
    exclude = ('updated_date', 'created_date',)
    search_fields = ('user__username', 'bank_name',)

    def has_add_permission(self, request):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return True
        else:

            return False

    def has_delete_permission(self, request, obj=None):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return True
        else:

            return False

    def has_change_permission(self, request, obj=None):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return True
        else:

            return False


class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ('user', 'start_date', 'expiry_date', 'payment_status', 'payment_id',)
    exclude = ('created_date',)
    search_fields = ('user__username', 'start_date', 'expiry_date',)
    list_filter = ['payment_status']

    def has_add_permission(self, request):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return True
        else:

            return False

    def has_delete_permission(self, request, obj=None):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return True
        else:

            return False

    def has_change_permission(self, request, obj=None):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return True
        else:

            return False


class AddressAdmin(admin.ModelAdmin):
    list_display = ('user', 'address', 'city', 'state', 'pin_code', 'type',)
    exclude = ('created_date',)
    search_fields = ('user__username', 'city', 'state', 'pin_code', 'type',)
    list_filter = ['city', 'state', 'pin_code', 'type',]

    # def has_add_permission(self, request):
    #
    #     if request.user.email in settings.SUPER_ADMIN_EMAIL:
    #
    #         return True
    #     else:
    #
    #         return False
    #
    # def has_delete_permission(self, request, obj=None):
    #
    #     if request.user.email in settings.SUPER_ADMIN_EMAIL:
    #
    #         return True
    #     else:
    #
    #         return False
    #
    # def has_change_permission(self, request, obj=None):
    #
    #     if request.user.email in settings.SUPER_ADMIN_EMAIL:
    #
    #         return True
    #     else:
    #
    #         return False


from rest_framework.authtoken.models import Token


class TokenAdmin(admin.ModelAdmin):
    list_display = ('user', 'key', 'created',)
    search_fields = ('user__username',)

    def has_add_permission(self, request):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return False
        else:

            return False

    def has_delete_permission(self, request, obj=None):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return False
        else:

            return False

    def has_change_permission(self, request, obj=None):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return False
        else:

            return False


admin.site.unregister(Token)
admin.site.register(Token, TokenAdmin)


class LocationAdmin(admin.ModelAdmin):
    list_display = (
        'user', 'date_created', 'type', 'mocked', 'timestamp', 'speed', 'heading', 'accuracy', 'altitude', 'location',
        'latitude', 'longitude')
    exclude = ('date_updated',)
    search_fields = ('user__username', 'type',)
    list_filter = ('type',)


admin.site.register(Location, LocationAdmin)
admin.site.register(AuthenticationPin, AuthenticationPinAdmin)
admin.site.register(Personal, PersonalDetailsAdmin)
admin.site.register(Professional, ProfessionalAdmin)
admin.site.register(Bank, BankAdmin)
admin.site.register(Reference, ReferenceAdmin)
admin.site.register(Subscription, SubscriptionAdmin)
admin.site.register(Address, AddressAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(Documents, DocumentAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(Otp, OtpAdmin)
