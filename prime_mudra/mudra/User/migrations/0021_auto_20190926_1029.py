# Generated by Django 2.2.4 on 2019-09-26 10:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('User', '0020_auto_20190923_1045'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bank',
            name='account_number',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='bank',
            name='bank_name',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='bank',
            name='ifsc_code',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='documents',
            name='path',
            field=models.ImageField(blank=True, max_length=256, null=True, upload_to='images'),
        ),
        migrations.AlterField(
            model_name='personal',
            name='name',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='personal',
            name='pan_number',
            field=models.CharField(blank=True, max_length=15, null=True),
        ),
    ]
