# Generated by Django 2.2.4 on 2019-09-19 13:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('User', '0018_auto_20190919_1136'),
    ]

    operations = [
        migrations.AlterField(
            model_name='address',
            name='address',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='city',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='pin_code',
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='state',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='personal',
            name='birthdate',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='personal',
            name='monthly_income',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='professional',
            name='company_address',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='professional',
            name='company_designation',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='professional',
            name='company_email_id',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='professional',
            name='company_landline_no',
            field=models.CharField(blank=True, max_length=15, null=True),
        ),
        migrations.AlterField(
            model_name='professional',
            name='company_name',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='professional',
            name='experience',
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
        migrations.AlterField(
            model_name='reference',
            name='relationships',
            field=models.CharField(blank=True, choices=[('Mother', 'MO'), ('Father', 'FA'), ('Brother', 'BR'), ('Sister', 'SI'), ('Spouse', 'SP'), ('Son', 'SN'), ('Daughter', 'DT'), ('None', 'None')], max_length=20, null=True),
        ),
    ]
