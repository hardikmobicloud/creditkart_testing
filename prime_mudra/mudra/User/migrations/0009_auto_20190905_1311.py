# Generated by Django 2.2.4 on 2019-09-05 13:11

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('User', '0008_auto_20190905_0905'),
    ]

    operations = [
        migrations.AlterField(
            model_name='professional',
            name='experience',
            field=models.CharField(max_length=10),
        ),
    ]
