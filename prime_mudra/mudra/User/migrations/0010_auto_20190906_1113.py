# Generated by Django 2.2.4 on 2019-09-06 11:13

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ('User', '0009_auto_20190905_1311'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documents',
            name='verify_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE,
                                    related_name='verify_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='documents',
            name='verify_date',
            field=models.DateField(blank=True, max_length=256, null=True),
        ),
    ]
