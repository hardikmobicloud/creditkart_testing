# Generated by Django 2.2.4 on 2019-09-10 14:58

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('User', '0010_auto_20190906_1113'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documents',
            name='type',
            field=models.CharField(blank=True, choices=[('Aadhar', 'AA'), ('Pan', 'PN'), ('Bank_Statement', 'BS'),
                                                        ('Salary_Slip', 'SS'), ('Company_Id', 'CI'),
                                                        ('Self_Video', 'SV'), ('Current_Address', 'CA'),
                                                        ('Permanent_Address', 'PA')], max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='professional',
            name='experience',
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
        migrations.AlterField(
            model_name='professional',
            name='join_date',
            field=models.DateField(auto_now=True, null=True),
        ),
    ]
