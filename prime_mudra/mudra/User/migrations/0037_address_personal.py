# Generated by Django 2.2.4 on 2020-09-02 12:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('User', '0036_personal_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='address',
            name='personal',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='personal_address', to='User.Personal'),
        ),
    ]
