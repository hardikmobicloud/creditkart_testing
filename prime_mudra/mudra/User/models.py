from Payments.models import Payment
from django.contrib.auth.models import User
from django.db import models

from mudra import constants


class CompleteModel(models.Model):

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     self._meta = None

    def is_completed(self):
        """
        check weather the data is complete or not
        :return:
        """
        # all_fields = self._meta.fields
        all_fields = [f.name for f in self._meta.get_fields()]
        all_fields = [x for x in all_fields if '_ptr' not in x]

        for i in all_fields:
            field_value = getattr(self, i)
            if not field_value:
                return False
        return True

    class Meta:
        abstract = True


class Personal(CompleteModel):
    name = models.CharField(db_index=True, max_length=256, blank=True, null=True)
    fathers_name = models.CharField(max_length=256, blank=True, null=True)
    birthdate = models.DateField(blank=True, null=True)
    gender = models.CharField(db_index=True, choices=constants.GENDER, max_length=10, blank=True, null=True)
    marital_status = models.CharField(db_index=True, blank=True, null=True, max_length=15,
                                      choices=constants.MARITAL_STATUS)
    occupation = models.CharField(blank=True, null=True, max_length=15, choices=constants.OCCUPATION)
    mobile_no = models.CharField(db_index=True, blank=True, null=True, max_length=15)
    alt_mobile_no = models.CharField(blank=True, null=True, max_length=15)
    email_id = models.CharField(max_length=100, null=False, blank=True)
    monthly_income = models.FloatField(db_index=True, blank=True, null=True)
    pan_number = models.CharField(db_index=True, max_length=15, null=True, blank=True)
    aadhar_number = models.CharField(max_length=15, null=True, blank=True)
    type = models.CharField(db_index=True, max_length=15, null=True, blank=True,
                            choices=constants.PERSONAL_ADDRESS_TYPE)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_personal")
    itr = models.CharField(choices=constants.COM, max_length=10, blank=True, null=True)
    mobile_linked_aadhaar = models.CharField(choices=constants.COM, max_length=10, blank=True, null=True)
    residence_type = models.CharField(choices=constants.RESIDENCE_TYPE, max_length=100, blank=True, null=True)
    industry_type = models.CharField(choices=constants.INDUSTRY, max_length=256, blank=True, null=True)
    educational = models.CharField(choices=constants.EDUCATIONAL, max_length=256, blank=True, null=True)
    invitations = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return '%s %s' % (self.name, self.user)


class Documents(CompleteModel):
    type = models.CharField(db_index=True, blank=True, null=True, max_length=30, choices=constants.DOCUMENT_TYPE)
    path = models.ImageField(upload_to='images', max_length=256, null=True, blank=True)
    verify_date = models.DateField(max_length=256, null=True, blank=True)
    verify_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="verify_by", null=True, blank=True)
    is_verified = models.BooleanField(default=False, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_documents")


class Professional(CompleteModel):
    company_name = models.CharField(db_index=True, max_length=256, null=True, blank=True)
    company_email_id = models.CharField(max_length=100, null=True, blank=True)
    company_address = models.CharField(max_length=256, null=True, blank=True)
    company_landline_no = models.CharField(max_length=15, null=True, blank=True)
    company_designation = models.CharField(max_length=100, null=True, blank=True)
    experience = models.CharField(db_index=True, max_length=10, null=True, blank=True)
    join_date = models.DateField(auto_now=True, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_professional")


class Reference(CompleteModel):
    colleague_name = models.CharField(db_index=True, max_length=256, blank=True, null=True)
    colleague_mobile_no = models.CharField(db_index=True, max_length=20, blank=True, null=True)
    colleague_email_id = models.CharField(max_length=100, blank=True, null=True)
    relationships = models.CharField(db_index=True, max_length=20, choices=constants.RELATION, blank=True, null=True)
    relative_name = models.CharField(db_index=True, max_length=256, blank=True, null=True)
    relative_mobile_no = models.CharField(db_index=True, max_length=20, blank=True, null=True)
    relative_email_id = models.CharField(max_length=100, blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_reference")


class Bank(CompleteModel):
    bank_name = models.CharField(db_index=True, max_length=256, null=True, blank=True)
    account_number = models.CharField(db_index=True, max_length=50, null=True, blank=True)
    ifsc_code = models.CharField(max_length=50, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_bank")
    account_type = models.CharField(db_index=True,default="savings", blank=True, null=True, max_length=10, choices=constants.ACCOUNT_TYPE)
    type = models.CharField(db_index=True, blank=True, null=True, max_length=10, default='loan',
                            choices=constants.BANK_TYPE)


class Otp(CompleteModel):
    otp = models.CharField(max_length=10)
    is_expired = models.BooleanField()
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_otp")

    # print(i, field_value)


class Address(CompleteModel):
    address = models.CharField(max_length=256, null=True, blank=True)
    city = models.CharField(db_index=True, max_length=100, null=True, blank=True)
    state = models.CharField(db_index=True, max_length=100, null=True, blank=True)
    pin_code = models.CharField(db_index=True, max_length=10, null=True, blank=True)
    personal = models.ForeignKey(Personal, on_delete=models.CASCADE, null=True, blank=True,
                                 related_name="personal_address")
    type = models.CharField(db_index=True, blank=True, null=True, max_length=10, choices=constants.ADDRESS_TYPE)
    created_date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, related_name="user_address")
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)


class AuthenticationPin(CompleteModel):
    """
    To store user authentication pin
    """

    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_authentication_pin_user')
    pin = models.CharField(blank=True, max_length=256)
    type = models.CharField(max_length=256, null=True, blank=True, choices=constants.TYPE)

    class Meta:
        ordering = ['-id']


class Subscription(CompleteModel):
    """
        To store INCOME & EXPENSE subscription
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_subscription", blank=True, null=False)
    start_date = models.DateTimeField(db_index=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    expiry_date = models.DateTimeField(null=True)
    payment_status = models.CharField(db_index=True, max_length=50, null=True)
    payment_id = models.ForeignKey(Payment, on_delete=models.CASCADE, related_name="subscription_payment", null=True)
    is_loanmart = models.BooleanField(default=False)

    class Meta:
        ordering = ['-id']


class Location(CompleteModel):
    """
    To store user office/Home location
    """

    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, related_name='location')
    type = models.CharField(max_length=20, blank=True, null=True, choices=constants.LOCATION_TYPE, default='H')
    mocked = models.BooleanField(default=False)
    timestamp = models.DateTimeField(null=True, blank=True)
    speed = models.CharField(max_length=200, blank=True, null=True)
    heading = models.CharField(max_length=200, blank=True, null=True)
    accuracy = models.CharField(max_length=200, blank=True, null=True)
    altitude = models.CharField(max_length=200, blank=True, null=True)
    location = models.CharField(max_length=256, blank=True, null=True)
    latitude = models.CharField(max_length=256, blank=True, null=True)
    longitude = models.CharField(max_length=256, blank=True, null=True)

#
# from User.models import *
# obj = Personal()
# obj.is_completed()
