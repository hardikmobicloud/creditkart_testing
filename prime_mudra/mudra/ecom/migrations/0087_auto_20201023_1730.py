# Generated by Django 2.2.4 on 2020-10-23 17:30

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('ecom', '0086_auto_20201023_1122'),
    ]

    operations = [
        migrations.CreateModel(
            name='LoanDefaulter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mobile_number', models.CharField(blank=True, max_length=25, null=True)),
                ('is_defaulter', models.BooleanField(blank=True, default=False, null=True)),
                ('status', models.BooleanField(blank=True, default=True, null=True)),
                ('type', models.CharField(blank=True, choices=[('mudrakwik', 'mudrakwik'), ('rupeekwik', 'rupeekwik')], max_length=50, null=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
            ],
        ),
        migrations.AlterField(
            model_name='userwallet',
            name='status',
            field=models.CharField(blank=True, choices=[('account', 'account'), ('wallet', 'wallet'), ('order', 'order'), ('delivery_charge', 'delivery_charge'), ('default', 'default'), ('repayment', 'repayment'), ('initiated', 'initiated'), ('success', 'success'), ('order_return', 'order_return'), ('order_cancel', 'order_cancel'), ('order_exchange', 'order_exchange'), ('cashback_amount', 'cashback_amount'), ('cancel_cashback', 'cancel_cashback'), ('repayment_cashback', 'repayment_cashback'), ('undelivered_delivery_charge', 'undelivered_delivery_charge'), ('order_undelivered', 'order_undelivered'), ('repayment_cancel_cashback', 'repayment_cancel_cashback'), ('order_return_discount', 'order_return_discount'), ('return_reject_delivery_charge', 'return_reject_delivery_charge'), ('return_delivery_charge_initiated', 'return_delivery_charge_initiated'), ('return_delivery_charge_success', 'return_delivery_charge_success'), ('return_cancel_delivery_charge', 'return_cancel_delivery_charge')], max_length=50, null=True),
        ),
        migrations.CreateModel(
            name='OfferBanner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('path', models.ImageField(blank=True, db_index=True, max_length=256, null=True, upload_to='offerbanner')),
                ('start_date', models.DateTimeField(blank=True, null=True)),
                ('end_date', models.DateTimeField(blank=True, null=True)),
                ('status', models.BooleanField(default=True)),
                ('type', models.CharField(blank=True, choices=[('App', 'App'), ('Web', 'Web'), ('Both', 'Both')], max_length=25, null=True)),
                ('priority', models.IntegerField(blank=True, default=None, null=True)),
                ('description', models.CharField(blank=True, max_length=256, null=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='offer_banner_created_by', to=settings.AUTH_USER_MODEL)),
                ('updated_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='offer_banner_updated_by', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
