# Generated by Django 2.2.4 on 2020-12-02 15:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecom', '0096_auto_20201202_1414'),
    ]

    operations = [
        migrations.AddField(
            model_name='userwallet',
            name='auto_adjust',
            field=models.BooleanField(default=False),
        ),

        migrations.AlterField(
            model_name='orderdetails',
            name='payment_mode',
            field=models.CharField(blank=True, choices=[('cash', 'cash'), ('credit', 'credit'), ('cod', 'cod')],
                                   max_length=20, null=True),
        ),
    ]
