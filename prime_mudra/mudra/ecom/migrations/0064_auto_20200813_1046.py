# Generated by Django 2.2.4 on 2020-08-13 10:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecom', '0063_auto_20200812_1513'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderlocation',
            name='remark',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='orderlocation',
            name='address',
            field=models.TextField(blank=True, null=True),
        ),
    ]
