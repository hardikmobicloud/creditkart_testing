# Generated by Django 2.2.4 on 2020-07-01 17:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecom', '0045_auto_20200630_1702'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderdetailsstatus',
            name='status',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='returnorder',
            name='logistic_status',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='returnorder',
            name='payment_status',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='returnorder',
            name='seller_status',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='returnorder',
            name='type',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='returnorder',
            name='verify_return_status',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
