# Generated by Django 2.2.4 on 2020-08-04 12:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecom', '0055_selleramount_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='seller',
            name='address',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='seller',
            name='main_address',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
    ]
