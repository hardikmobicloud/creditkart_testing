# Generated by Django 2.2.4 on 2020-04-13 12:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecom', '0012_auto_20200411_1031'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrderLocation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('altitude', models.CharField(blank=True, max_length=200, null=True)),
                ('latitude', models.CharField(blank=True, max_length=256, null=True)),
                ('longitude', models.CharField(blank=True, max_length=256, null=True)),
                ('address', models.TextField()),
                ('city', models.CharField(blank=True, max_length=256, null=True)),
                ('taluka', models.CharField(blank=True, max_length=256, null=True)),
                ('state', models.CharField(blank=True, max_length=256, null=True)),
                ('pin_code', models.CharField(blank=True, max_length=20, null=True)),
                ('date', models.DateTimeField(blank=True, null=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
            ],
        ),
    ]
