# Generated by Django 2.2.4 on 2020-07-16 14:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecom', '0050_logisticsdeliverycharges'),
    ]

    operations = [
        migrations.AddField(
            model_name='productdetails',
            name='mrp',
            field=models.FloatField(blank=True, default=0, null=True),
        ),
        migrations.AddField(
            model_name='productdetails',
            name='sku',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
        migrations.AddField(
            model_name='productdetailsamount',
            name='mrp',
            field=models.FloatField(blank=True, default=0, null=True),
        ),
    ]
