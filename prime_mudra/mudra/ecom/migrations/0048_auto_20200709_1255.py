# Generated by Django 2.2.4 on 2020-07-09 12:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecom', '0047_orderdetails_credit_history'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='dashboard_status',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='credit',
            name='status',
            field=models.CharField(blank=True, choices=[('fetched', 'fetched'), ('repayment', 'repayment'), ('initiated', 'initiated'), ('success', 'success'), ('failed', 'failed'), ('order_return', 'order_return'), ('order_cancel', 'order_cancel'), ('order_exchange', 'order_exchange'), ('order_exchange', 'order_exchange')], max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='logisticorder',
            name='order_data_text',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(blank=True, choices=[('initiated', 'initiated'), ('placed', 'placed'), ('accepted', 'accepted'), ('packing', 'packing'), ('cancelled', 'cancelled'), ('pending', 'pending'), ('shipped', 'shipped'), ('delivered', 'delivered'), ('return', 'return'), ('exchange', 'exchange')], max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='orderdetails',
            name='status',
            field=models.CharField(blank=True, choices=[('initiated', 'initiated'), ('placed', 'placed'), ('accepted', 'accepted'), ('packing', 'packing'), ('cancelled', 'cancelled'), ('pending', 'pending'), ('shipped', 'shipped'), ('delivered', 'delivered'), ('return', 'return'), ('exchange', 'exchange')], max_length=256, null=True),
        ),
    ]
