# Generated by Django 2.2.4 on 2020-08-11 10:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecom', '0060_auto_20200810_1248'),
    ]

    operations = [
        migrations.AlterField(
            model_name='basecategory',
            name='base_name',
            field=models.CharField(blank=True, db_index=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='cart',
            name='amount',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='cart',
            name='quantity',
            field=models.IntegerField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='cart',
            name='total_amount',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='category',
            name='name',
            field=models.CharField(blank=True, db_index=True, max_length=250, null=True),
        ),
        migrations.AlterField(
            model_name='cgst',
            name='percentage',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='colour',
            name='name',
            field=models.CharField(blank=True, db_index=True, max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='convenientfees',
            name='amount',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='convenientfees',
            name='re_status',
            field=models.BooleanField(db_index=True, default=False),
        ),
        migrations.AlterField(
            model_name='credit',
            name='actual_used',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='credit',
            name='balance_credit',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='credit',
            name='credit',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='credit',
            name='repayment_add',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='credit',
            name='used_credit',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='deliverycharges',
            name='amount',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='ecomdocuments',
            name='path',
            field=models.ImageField(blank=True, db_index=True, max_length=256, null=True, upload_to='ecom_doc'),
        ),
        migrations.AlterField(
            model_name='images',
            name='path',
            field=models.ImageField(blank=True, db_index=True, max_length=256, null=True, upload_to='ecom'),
        ),
        migrations.AlterField(
            model_name='images',
            name='status',
            field=models.BooleanField(db_index=True, default=False),
        ),
        migrations.AlterField(
            model_name='logisticorder',
            name='logistics',
            field=models.CharField(blank=True, db_index=True, max_length=40, null=True),
        ),
        migrations.AlterField(
            model_name='logisticorder',
            name='refnum',
            field=models.CharField(blank=True, db_index=True, max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='logisticorder',
            name='waybill_no',
            field=models.CharField(blank=True, db_index=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='logisticsdeliverycharges',
            name='amount',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='logisticsdeliverycharges',
            name='gram',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='logisticsdeliverycharges',
            name='percentage',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='amount',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='quantity',
            field=models.IntegerField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='total_amount',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='unique_order_id',
            field=models.CharField(blank=True, db_index=True, max_length=250, null=True),
        ),
        migrations.AlterField(
            model_name='orderdetails',
            name='credit_status',
            field=models.CharField(blank=True, choices=[('Initiated', 'Initiated'), ('Pay', 'Pay'), ('Paid', 'Paid'), ('Failed', 'Failed')], db_index=True, max_length=25, null=True),
        ),
        migrations.AlterField(
            model_name='orderdetails',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True),
        ),
        migrations.AlterField(
            model_name='orderdetails',
            name='pay_amount',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='orderdetails',
            name='re_status',
            field=models.BooleanField(db_index=True, default=False),
        ),
        migrations.AlterField(
            model_name='orderdetails',
            name='status',
            field=models.CharField(blank=True, choices=[('initiated', 'initiated'), ('placed', 'placed'), ('accepted', 'accepted'), ('packing', 'packing'), ('cancelled', 'cancelled'), ('pending', 'pending'), ('shipped', 'shipped'), ('packed', 'packed'), ('dispatched', 'dispatched'), ('delivered', 'delivered'), ('return', 'return'), ('exchange', 'exchange')], db_index=True, max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='orderdetails',
            name='total_amount',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='orderdetails',
            name='total_quantity',
            field=models.IntegerField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='orderdetailsstatus',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True),
        ),
        migrations.AlterField(
            model_name='orderdetailsstatus',
            name='status',
            field=models.CharField(blank=True, db_index=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='orderstatus',
            name='status',
            field=models.CharField(blank=True, db_index=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='name',
            field=models.CharField(blank=True, db_index=True, max_length=250, null=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_id',
            field=models.CharField(blank=True, db_index=True, max_length=250, null=True),
        ),
        migrations.AlterField(
            model_name='productdetails',
            name='amount',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='productdetails',
            name='seller_amount',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='productdetailsamount',
            name='admin_amount',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='productdetailsamount',
            name='seller_amount',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='returnorder',
            name='return_id',
            field=models.CharField(blank=True, db_index=True, max_length=250, null=True),
        ),
        migrations.AlterField(
            model_name='seller',
            name='mobile_no',
            field=models.CharField(blank=True, db_index=True, max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='seller',
            name='name',
            field=models.CharField(blank=True, db_index=True, max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='selleramount',
            name='invoice_id',
            field=models.CharField(blank=True, db_index=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='selleramount',
            name='type',
            field=models.CharField(choices=[('dispatched', 'dispatched'), ('delivered', 'delivered')], db_index=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='sellerwarehouse',
            name='approval_status',
            field=models.CharField(blank=True, db_index=True, max_length=40, null=True),
        ),
        migrations.AlterField(
            model_name='sellerwarehouse',
            name='company_name',
            field=models.CharField(blank=True, db_index=True, max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='sellerwarehouse',
            name='warehouse_id',
            field=models.CharField(blank=True, db_index=True, max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='sgst',
            name='percentage',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='size',
            name='name',
            field=models.CharField(blank=True, db_index=True, max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='stock',
            name='status',
            field=models.BooleanField(db_index=True, default=False),
        ),
        migrations.AlterField(
            model_name='stock',
            name='stock_add',
            field=models.IntegerField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='stock',
            name='stock_balance',
            field=models.IntegerField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='stock',
            name='stock_sub',
            field=models.IntegerField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='subcategory',
            name='name',
            field=models.CharField(blank=True, db_index=True, max_length=250, null=True),
        ),
        migrations.AlterField(
            model_name='userwallet',
            name='amount_add',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='userwallet',
            name='balance_amount',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='userwallet',
            name='used_amount',
            field=models.FloatField(blank=True, db_index=True, default=0, null=True),
        ),
    ]
