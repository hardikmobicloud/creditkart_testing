# Generated by Django 2.2.4 on 2020-10-22 11:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecom', '0084_auto_20201017_1626'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(blank=True, choices=[('initiated', 'initiated'), ('placed', 'placed'), ('accepted', 'accepted'), ('packing', 'packing'), ('packed', 'packed'), ('s_dispatched', 's_dispatched'), ('cancelled', 'cancelled'), ('pending', 'pending'), ('shipped', 'shipped'), ('in_transit', 'in_transit'), ('dispatched', 'dispatched'), ('delivered', 'delivered'), ('return', 'return'), ('exchange', 'exchange'), ('undelivered', 'undelivered'), ('lost', 'lost'), ('return_cancelled', 'return_cancelled')], max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='orderdetails',
            name='status',
            field=models.CharField(blank=True, choices=[('initiated', 'initiated'), ('placed', 'placed'), ('accepted', 'accepted'), ('packing', 'packing'), ('packed', 'packed'), ('s_dispatched', 's_dispatched'), ('cancelled', 'cancelled'), ('pending', 'pending'), ('shipped', 'shipped'), ('in_transit', 'in_transit'), ('dispatched', 'dispatched'), ('delivered', 'delivered'), ('return', 'return'), ('exchange', 'exchange'), ('undelivered', 'undelivered'), ('lost', 'lost'), ('return_cancelled', 'return_cancelled')], db_index=True, max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='sellerwarehouse',
            name='type',
            field=models.CharField(blank=True, choices=[('ithink', 'ithink'), ('shiprocket', 'shiprocket')], default='ithink', max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='userwallet',
            name='status',
            field=models.CharField(blank=True, choices=[('account', 'account'), ('wallet', 'wallet'), ('order', 'order'), ('delivery_charge', 'delivery_charge'), ('default', 'default'), ('repayment', 'repayment'), ('initiated', 'initiated'), ('success', 'success'), ('order_return', 'order_return'), ('order_cancel', 'order_cancel'), ('order_exchange', 'order_exchange'), ('cashback_amount', 'cashback_amount'), ('cancel_cashback', 'cancel_cashback'), ('repayment_cashback', 'repayment_cashback'), ('undelivered_delivery_charge', 'undelivered_delivery_charge'), ('order_undelivered', 'order_undelivered'), ('repayment_cancel_cashback', 'repayment_cancel_cashback'), ('order_return_discount', 'order_return_discount'), ('return_reject_delivery_charge', 'return_reject_delivery_charge'), ('return_delivery_charge_initiated', 'return_delivery_charge_initiated'), ('return_delivery_charge_success', 'return_delivery_charge_success'), ('return_cancel_delivery_charge', 'return_cancel_delivery_charge')], max_length=50, null=True),
        ),
    ]
