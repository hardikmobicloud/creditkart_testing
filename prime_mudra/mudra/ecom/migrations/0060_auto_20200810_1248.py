# Generated by Django 2.2.4 on 2020-08-10 12:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ecom', '0059_category_priority'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='priority',
            field=models.IntegerField(blank=True, default=None, null=True),
        ),
        migrations.AlterField(
            model_name='credit',
            name='status',
            field=models.CharField(blank=True, choices=[('fetched', 'fetched'), ('default', 'default'), ('repayment', 'repayment'), ('initiated', 'initiated'), ('success', 'success'), ('failed', 'failed'), ('order_return', 'order_return'), ('order_cancel', 'order_cancel'), ('order_exchange', 'order_exchange'), ('order_exchange', 'order_exchange')], max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='priority',
            field=models.IntegerField(blank=True, default=None, null=True),
        ),
        migrations.CreateModel(
            name='ProductMinMaxAMount',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('min_amount', models.FloatField(blank=True, default=0, null=True)),
                ('max_amount', models.FloatField(blank=True, default=0, null=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
                ('product', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='product_min_max_amount', to='ecom.Product')),
            ],
        ),
    ]
