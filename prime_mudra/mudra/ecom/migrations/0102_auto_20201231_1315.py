from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ('ecom', '0101_auto_20201224_1135'),
    ]

    operations = [
        migrations.AddField(
            model_name='logisticorder',
            name='order_details',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE,
                                    related_name='logistic_order_details_id', to='ecom.OrderDetails'),
        ),
        migrations.AddField(
            model_name='order',
            name='combination_type',
            field=models.CharField(blank=True, choices=[('single', 'single'), ('multiple', 'multiple')], max_length=25,
                                   null=True),
        ),
        migrations.AddField(
            model_name='orderdetails',
            name='combine_order',
            field=models.ManyToManyField(blank=True, null=True, related_name='combine_order', to='ecom.Order'),
        ),
        migrations.AddField(
            model_name='shiprocketapiinfo',
            name='order_details',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE,
                                    to='ecom.OrderDetails'),
        ),
        migrations.AddField(
            model_name='shiprocketorder',
            name='order_details',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE,
                                    related_name='shiprocket_order_details', to='ecom.OrderDetails'),
        ),
        migrations.AlterField(
            model_name='shiprocketorder',
            name='order',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE,
                                    related_name='order_create_order', to='ecom.Order'),
        ),
    ]
