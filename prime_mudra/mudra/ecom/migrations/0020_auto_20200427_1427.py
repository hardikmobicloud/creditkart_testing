# Generated by Django 2.2.4 on 2020-04-27 14:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ecom', '0019_returnorder_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='delivery_charges',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='product_delivery_charge', to='ecom.DeliveryCharges'),
        ),
        migrations.AddField(
            model_name='returnorder',
            name='order_in_exchange',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='order_in_exchange', to='ecom.Order'),
        ),
    ]
