# Generated by Django 2.2.4 on 2020-12-07 16:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ecom', '0097_userwallet_auto_adjust'),
    ]

    operations = [
        migrations.AddField(
            model_name='convenientfees',
            name='order_type',
            field=models.CharField(blank=True, choices=[('order', 'order'), ('order_details', 'order_details')], max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='order',
            name='convenient_fees',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='orders_convenient_fees', to='ecom.ConvenientFees'),
        ),
        migrations.AddField(
            model_name='productdetails',
            name='HSN',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
    ]
