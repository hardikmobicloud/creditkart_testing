# Generated by Django 2.2.4 on 2020-08-26 13:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecom', '0067_orderdetails_payment_mode'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sellerpayment',
            name='type',
            field=models.CharField(blank=True, choices=[('Cash', 'Cash'), ('Account', 'Account'), ('App', 'App'), ('On_Us', 'On_Us'), ('Pt-Invoice', 'Pt-Invoice')], db_index=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='userwallet',
            name='status',
            field=models.CharField(blank=True, choices=[('account', 'account'), ('wallet', 'wallet'), ('order', 'order'), ('delivery_charge', 'delivery_charge'), ('default', 'default'), ('repayment', 'repayment'), ('initiated', 'initiated'), ('success', 'success'), ('order_return', 'order_return'), ('order_cancel', 'order_cancel'), ('order_exchange', 'order_exchange'), ('cashback_amount', 'cashback_amount'), ('cancel_cashback', 'cancel_cashback'), ('order_return_discount', 'order_return_discount')], max_length=50, null=True),
        ),
    ]
