import datetime

import pandas as pd
from django.apps.registry import apps
from django.contrib.auth.models import User
from django.db.models import Sum
from django.shortcuts import render, redirect
from django.views import generic
from django_pandas.io import read_frame
from rest_framework import generics
from rest_framework.authentication import *
from rest_framework.permissions import *
from rest_framework.response import Response
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import HttpResponse
from rest_framework.utils import json
from .fusioncharts import FusionCharts
from collections import OrderedDict
from collections import Counter


Payments = apps.get_model('Payments', 'Payment')
OrderDetails = apps.get_model('ecom', 'OrderDetails')
DeliveryCharges = apps.get_model('ecom', 'DeliveryCharges')
Order = apps.get_model('ecom', 'Order')
SellerAmount = apps.get_model('ecom', 'SellerAmount')
SellerAccount = apps.get_model('ecom', 'SellerAccount')
ReturnOrder = apps.get_model('ecom', 'ReturnOrder')
Credit = apps.get_model('ecom', 'Credit')
SellerWallet = apps.get_model('ecom', 'SellerWallet')
SellerInvoiceClearAmount = apps.get_model('ecom', 'SellerInvoiceClearAmount')
UserPersonal = apps.get_model('User', 'Personal')
SellerPayment = apps.get_model('ecom', 'SellerPayment')
Seller = apps.get_model('ecom', 'Seller')


class OrderPaymentMonthwise(generic.ListView):
    """
    This class is used to show order repayment monthwise. In this class using pandas month and
    its total repayment amount will find out.
    """
    template_name = "Ecom/ecom_payments/order_payment_monthwise.html"
    title = ' order payment Monthwise'

    def get(self, request, *args, **kwargs):
        MainDict = {}
        month_wise_repayment_amount_dict = {}
        ctx = {}
        try:
            repayment = Payments.objects.filter(product_type="Ecom", status__in=['Success', 'success'],
                                                category="Repayment").order_by('-date')
            repayment_dataframe = read_frame(repayment)
            repayment_dataframe['YearMonth'] = pd.to_datetime(repayment_dataframe['date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            month_wise_repayment_amount_dict = \
                repayment_dataframe.groupby(repayment_dataframe['YearMonth'])['amount'].sum().sort_index(axis=0)
            month_wise_repayment_amount_dict = dict(month_wise_repayment_amount_dict)
            BothDicts = [month_wise_repayment_amount_dict]
            MainDict = {}
            for k in month_wise_repayment_amount_dict.keys():
                MainDict[k] = list(MainDict[k] for MainDict in BothDicts)
            ctx['MainData'] = MainDict
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


class OrderPaymentDatewise(generic.ListView):
    """
       This class is used to show order repayment datewise. In this class using pandas date and
       its total repayment amount will find out.
    """
    template_name = "Ecom/ecom_payments/order_payment_datewise.html"
    title = 'order payment Datewise'

    def get(self, request, on_month, *args, **kwargs):
        ctx = {}
        new_date = ""
        try:
            date_list = on_month.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
                new_date = '{}-{}'.format(y, m)
            else:
                m = '0' + str(date_list[1])
                new_date = '{}-{}'.format(y, m)
            payments = Payments.objects.filter(product_type="Ecom", category="Repayment",
                                               date__startswith=new_date, status__in=['Success', 'success']).order_by(
                'id')
            if payments:
                payments_dataframe = read_frame(payments)
                payments_dataframe['YearMonthDay'] = pd.to_datetime(payments_dataframe['date']).apply(
                    lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
                month_wise_repayment_amount_dict = \
                    payments_dataframe.groupby(payments_dataframe['YearMonthDay'])['amount'].sum().sort_index(axis=0)
                month_wise_repayment_amount_dict = dict(month_wise_repayment_amount_dict)
                ctx['MainData'] = month_wise_repayment_amount_dict
            return render(request, self.template_name, ctx)
        except Exception as e:
            return render(request, self.template_name, ctx)


class OrderPaymentDetails(generic.ListView):
    """
       This class is used to show repayment details on particular date. The order id which is present in
       payment is split by 'e', the first split part is nothing but order_text in OrderDetails.
       The Payment and OrderDetails object are pass to the template.
    """
    template_name = "Ecom/ecom_payments/order_payment_details.html"
    title = 'order payment Datewise'

    def get(self, request, on_date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        y = ''
        date_list = on_date.split('-')
        y = date_list[0]
        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}-{}'.format(y, m, d)
        if len(date_list[2]) == 2:
            d = date_list[2]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)
        details = Payments.objects.filter(product_type="Ecom", date__startswith=new_date,
                                          category="Repayment", status__icontains="success").order_by('-date')
        details_list = []
        if details:
            number_list = []
            for i in details:
                number = None
                if i.order_id:
                    oid = i.order_id.split('e')
                    if len(oid) >= 1:
                        order_details_obj = OrderDetails.objects.filter(order_text=oid[0])
                        if order_details_obj:
                            number = order_details_obj[0].user.username
                number_list.append(number)
                details_list.append(i)
            ctx['details'] = zip(details_list, number_list)
        return render(request, self.template_name, ctx)


class OrderDetailsDeliveryCharges(generic.ListView):
    """
    This class is used to show order and its delivery charges. The order id list is given from OrderDetails and exclude
    credit status failed in it. The order object and delivery charge amount is passed to the template.
    """
    template_name = "Ecom/ecom_payments/order_details_delivery_charges.html"

    def get(self, request, *args, **kwargs):
        ctx = {}
        data_dict = {}

        order_info = OrderDetails.objects.exclude(credit_status="Failed")
        for order_obj in order_info:
            order_details = OrderDetails.objects.exclude(credit_status="Failed").filter(id=order_obj.id).values_list(
                "order_id", flat="True")
            if order_details:
                orders = Order.objects.filter(id__in=order_details)
                if orders:
                    for order in orders:
                        delivery_charge = 0
                        get_delivery_charge = DeliveryCharges.objects.filter(
                            product=order.product_details.product_id.id).order_by('id').last()
                        if get_delivery_charge:
                            delivery_charge = get_delivery_charge.amount
                        else:
                            get_delivery_charge = DeliveryCharges.objects.filter(product=None).order_by('id').last()
                            if get_delivery_charge:
                                delivery_charge = get_delivery_charge.amount
                        data_dict.update({order_obj: delivery_charge})
        ctx['order'] = data_dict

        return render(request, self.template_name, ctx)


class OrderDetailsPayment(generic.ListView):
    """
    This class is used to show payments details of particular order details id.
    """
    template_name = "Ecom/ecom_payments/order_details_payment.html"

    def get(self, request, order_details_id, *args, **kwargs):
        ctx = {}
        order_details = OrderDetails.objects.filter(id=order_details_id).exclude(credit_status="Failed").values_list(
            'payment', flat="True")

        payment = Payments.objects.filter(id__in=order_details)

        ctx['payment'] = payment
        return render(request, self.template_name, ctx)


# NEW SELLER AMOUNT REPORT
class SellersDatewiseTotalAmount(generic.ListView):
    """
    This Function is to show datewise total amount, exchange amount , return amount using pandas.
    """
    template_name = "Ecom/ecom_payments/sellers_datewise_total_amount.html"

    def get(self, request, *args, **kwargs):
        ctx = {}
        user = User.objects.filter(id=request.user.id)
        seller_group = user[0].groups.filter(name='Seller').exists()
        if seller_group:
            total_amount = SellerAccount.objects.filter(seller=user[0])
            total_amount_dataframe = read_frame(total_amount)
            total_amount_dataframe['YearMonthDay'] = pd.to_datetime(total_amount_dataframe['verify_date']).apply(
                lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
            datewise_total_amount_dict = \
                total_amount_dataframe.groupby(total_amount_dataframe['YearMonthDay'])['total_amount'].sum().sort_index(
                    axis=0)
            datewise_total_amount_dict = dict(datewise_total_amount_dict)
            datewise_return_amount_dict = \
                total_amount_dataframe.groupby(total_amount_dataframe['YearMonthDay'])[
                    'return_amount'].sum().sort_index(
                    axis=0)
            datewise_return_amount_dict = dict(datewise_return_amount_dict)
            datewise_exchange_amount_dict = \
                total_amount_dataframe.groupby(total_amount_dataframe['YearMonthDay'])[
                    'exchange_amount'].sum().sort_index(
                    axis=0)
            datewise_exchange_amount_dict = dict(datewise_exchange_amount_dict)
            BothDicts = [datewise_total_amount_dict, datewise_return_amount_dict, datewise_exchange_amount_dict]
            MainDict = {}
            for k in datewise_total_amount_dict.keys():
                MainDict[k] = list(MainDict[k] for MainDict in BothDicts)
            ctx['MainData'] = MainDict
            return render(request, self.template_name, ctx)
        ecom_admin_group = user[0].groups.filter(name='EcomAdmin').exists()
        if ecom_admin_group:
            total_amount = SellerAccount.objects.all()
            total_amount_dataframe = read_frame(total_amount)
            total_amount_dataframe['YearMonthDay'] = pd.to_datetime(total_amount_dataframe['verify_date']).apply(
                lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
            datewise_total_amount_dict = \
                total_amount_dataframe.groupby(total_amount_dataframe['YearMonthDay'])['total_amount'].sum().sort_index(
                    axis=0)
            datewise_total_amount_dict = dict(datewise_total_amount_dict)
            datewise_return_amount_dict = \
                total_amount_dataframe.groupby(total_amount_dataframe['YearMonthDay'])[
                    'return_amount'].sum().sort_index(
                    axis=0)
            datewise_return_amount_dict = dict(datewise_return_amount_dict)
            datewise_exchange_amount_dict = \
                total_amount_dataframe.groupby(total_amount_dataframe['YearMonthDay'])[
                    'exchange_amount'].sum().sort_index(
                    axis=0)
            datewise_exchange_amount_dict = dict(datewise_exchange_amount_dict)
            BothDicts = [datewise_total_amount_dict, datewise_return_amount_dict, datewise_exchange_amount_dict]
            MainDict = {}
            for k in datewise_total_amount_dict.keys():
                MainDict[k] = list(MainDict[k] for MainDict in BothDicts)
            ctx['MainData'] = MainDict
            return render(request, self.template_name, ctx)


class SellersAmount(generic.ListView):
    """
    This Function is to show sellers wise dispatch, delivered amount. It is used for both ecomadmin and seller login.

    """
    template_name = "Ecom/ecom_payments/seller_amount.html"

    def get(self, request, date, *args, **kwargs):
        ctx = {}
        data_dict = {}
        new_date = ""
        date_list = date.split('-')
        y = date_list[0]
        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}'.format(y, m)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}'.format(y, m)
        if len(date_list[2]) > 1:
            d = date_list[2]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)
        user = User.objects.filter(id=request.user.id)
        seller_group = user[0].groups.filter(name='Seller').exists()
        if seller_group:
            dispatch_amount = 0
            dispatch_type = SellerAmount.objects.filter(order__product_details__user=user[0],
                                                        created_date__startswith=new_date, type="Dispatch", )
            if dispatch_type:
                dispatch_amount = dispatch_type[0].total_amount
            delivered_amount = 0
            delivered_type = SellerAmount.objects.filter(order__product_details__user=user[0],
                                                         created_date__startswith=new_date, type="Delivered")
            if delivered_type:
                delivered_amount = delivered_type[0].total_amount
            data_dict.update({user[0]: [dispatch_amount, delivered_amount, new_date]})
            ctx['seller'] = data_dict
        ecom_admin_group = user[0].groups.filter(name='EcomAdmin').exists()
        if ecom_admin_group:
            order = SellerAccount.objects.filter(verify_date__startswith=new_date).values_list(
                "seller", flat="True")
            order_set = set(order)
            for seller in order_set:
                username = User.objects.filter(id=seller)
                dispatch_amount = 0
                dispatch_type = SellerAmount.objects.filter(order__product_details__user=seller,
                                                            created_date__startswith=new_date, type="Dispatch")
                if dispatch_type:
                    dispatch_amount = dispatch_type[0].total_amount
                delivered_amount = 0
                delivered_type = SellerAmount.objects.filter(order__product_details__user=seller,
                                                             created_date__startswith=new_date, type="Delivered")
                if delivered_type:
                    delivered_amount = delivered_type[0].total_amount
                data_dict.update({username[0]: [dispatch_amount, delivered_amount, new_date]})
                ctx['sellers'] = data_dict
        return render(request, self.template_name, ctx)


class SellersDispatchDetails(generic.ListView):
    """
    This Function is to show sellers dispatch, delivered orders on specific date and specific seller.
    """
    template_name = "Ecom/ecom_payments/sellers_dispatch_details.html"

    def get(self, request, date, seller, type, *args, **kwargs):
        ctx = {}
        orders = SellerAmount.objects.filter(created_date__startswith=date, type=type,
                                             order__product_details__user__username=seller).values_list("order",
                                                                                                        flat="True")
        if orders:
            order = Order.objects.filter(id__in=orders)
            ctx["orders"] = order
        return render(request, self.template_name, ctx)


class SellerReturnOrderCount(generic.ListView):
    """
    This class is to show monthwise sellers return, exchange order count. The data is get from SellerAccount table. The get
    return order count the order ids are filtered in ReturnOrder.
    """
    template_name = "Ecom/ecom_payments/seller_return_order_count.html"

    def get(self, request, date, order, *args, **kwargs):
        ctx = {}
        return_dict = {}
        new_date = ""
        date_list = date.split('-')
        y = date_list[0]
        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}'.format(y, m)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}'.format(y, m)
        if len(date_list[2]) > 1:
            d = date_list[2]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)
        user = User.objects.filter(id=request.user.id)
        seller_group = user[0].groups.filter(name='Seller').exists()
        if seller_group:
            return_order_count = 0
            return_order = SellerAccount.objects.filter(seller=user[0],
                                                        verify_date__startswith=new_date).values_list(order,
                                                                                                      flat="True")
            if return_order:
                return_order_count = ReturnOrder.objects.filter(id__in=return_order).count()
                if return_order_count:
                    return_order_count = return_order_count
            return_dict.update({user[0]: [return_order_count, date, order]})
            ctx["order"] = return_dict
        ecom_admin_group = user[0].groups.filter(name='EcomAdmin').exists()
        if ecom_admin_group:
            sellers = SellerAccount.objects.filter(verify_date__startswith=new_date).values_list("seller", flat="True")
            if sellers:
                for seller in sellers:
                    username = User.objects.filter(id=seller)
                    return_order_count = 0
                    return_order = SellerAccount.objects.filter(seller=seller,
                                                                verify_date__startswith=new_date).values_list(order,
                                                                                                              flat="True")
                    if return_order:
                        return_order_count = ReturnOrder.objects.filter(id__in=return_order).count()
                    return_dict.update({username[0]: [return_order_count, date, order]})
                    ctx["orders"] = return_dict
        return render(request, self.template_name, ctx)


class SellerReturnOrder(generic.ListView):
    """
    This class is to show datewise sellers return, exchange order count. The data is get from SellerAccount table. The get
    return order count the order ids are filtered in ReturnOrder.
    """
    template_name = "Ecom/ecom_payments/seller_return_order.html"

    def get(self, request, date, seller, order, *args, **kwargs):
        ctx = {}
        new_date = ""
        date_list = date.split('-')
        y = date_list[0]
        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}'.format(y, m)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}'.format(y, m)
        if len(date_list[2]) > 1:
            d = date_list[2]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)
        orders_return = SellerAccount.objects.filter(verify_date__startswith=new_date,
                                                     seller__username=seller).values_list(order, flat="True")
        if orders_return:
            return_order = ReturnOrder.objects.filter(id__in=orders_return)
            ctx["return_orders"] = return_order
        return render(request, self.template_name, ctx)


def decide_slabs_of_credit(order_details_id):
    """
    This function takes order_details_id and gives slabs of installments according
    to credit used for this order..
    """
    slabs_dict = {}
    if order_details_id:
        order_details_obj = OrderDetails.objects.filter(id=order_details_id)
        if order_details_obj:
            order_date = order_details_obj[0].created_date
            first_installment_date = order_date + datetime.timedelta(days=30)
            second_installment_date = order_date + datetime.timedelta(days=60)
            third_installment_date = order_date + datetime.timedelta(days=90)
            dates = [first_installment_date, second_installment_date, third_installment_date]
            credit = 0
            if order_details_obj[0].credit:
                credit = order_details_obj[0].credit.actual_used
                from EApi.credit import get_order_details_return_cancel_amount
                # get order cancel and return total amount..
                total_cancel_return_amount = get_order_details_return_cancel_amount(order_details_obj[0].id)
                if total_cancel_return_amount:
                    credit = credit - total_cancel_return_amount

            if credit > 0:
                parts = 3
                equal_installments = [credit // parts + (1 if x < credit % parts else 0) for x in range(parts)]
                slabs_dict = dict(zip(dates, equal_installments))
                return slabs_dict
            else:
                return slabs_dict


# date__range=(start_date, end_date),
def get_amount_between_dates(start_date, end_date, order_details):
    """
    This function is used to  get repayment amount between two dates. It will return total repayment amount
    between the start date and end date for particular order details.
    """
    rep_amount = 0

    if order_details is not None:
        rep_amount = order_details.payment.filter(date__range=(str(start_date)[:19], str(end_date)[:19]),
                                                  status='success', product_type="Ecom",
                                                  category="Repayment").aggregate(Sum('amount'))['amount__sum']
    return rep_amount


def get_amount_paid_for_order_details(order_details):
    """
    This function is used to total amount paid for specific order details. It will return repayment amount and
    last repayment date.
    """
    rep_amount = 0
    if order_details.payment:
        rep_amount = order_details.payment.filter(product_type="Ecom", status='success',
                                                  category="Repayment").aggregate(Sum('amount'))['amount__sum']

        if rep_amount is not None:
            get_last_date = order_details.payment.filter(product_type="Ecom", status="success",
                                                         category="Repayment").order_by('id').last()
            if get_last_date:
                last_date = get_last_date.date
                last_date = last_date
                data = {
                    "rep_amount": rep_amount,
                    "last_date": last_date
                }
                return data

        else:
            data = {
                "rep_amount": 0,
                "last_date": None
            }
            return data


def check_user_defaulter_status(user_id):
    """
    This function is used to check user is defaulter or not. First check in defaulter status, if defaulter then return True.
    Then check in npci defaulter. If user is not defaulter in defaulter status and npci defaulter then check according to
    credit slabs.
    """
    # user_id = 200
    print('check user defaulter check called 518 ecom.ecom_payments')
    is_defaulter = "False"
    user = User.objects.filter(id=user_id)
    # Calling function which returns defaulter status from Loan users..
    from EApi.credit import is_defaulter_status
    defaulter_status = is_defaulter_status(user[0].username)
    if defaulter_status:
        return "True"
    from npci.views import user_npci_defaulter
    npci_defaulter = user_npci_defaulter(user[0].id)
    if npci_defaulter == "True":
        return "True"
    else:
        today_date = datetime.datetime.now()
        if user_id is not None:
            get_order_details = OrderDetails.objects.filter(user=user_id).exclude(status="initiated").exclude(
                credit_status="Paid")
            if get_order_details:
                # checking for each orders..
                for order_details in get_order_details:
                    if order_details.credit:
                        order_date = order_details.created_date
                        # This function gives slabs of installments for this order.
                        slabs = decide_slabs_of_credit(order_details.id)
                        if slabs:
                            new_dict = {}
                            new_amount = 0
                            for date, amount in slabs.items():
                                new_amount += amount
                                new_dict.update({date: new_amount})

                            for end_date, amount in new_dict.items():
                                data = get_amount_paid_for_order_details(order_details)
                                if data:
                                    paid_amount = data['rep_amount']
                                    last_pay_date = data['last_date']
                                    if paid_amount != 0 and last_pay_date is not None:
                                        # if paid_amount < amount and today_date < end_date:
                                        #     is_defaulter = "False"
                                        if paid_amount < amount and today_date > end_date:
                                            is_defaulter = "True"
                                            return is_defaulter
                                        else:
                                            is_defaulter = "False"
                                            return is_defaulter
                                    else:
                                        if today_date > end_date:
                                            is_defaulter = "True"
                                            return is_defaulter
        return is_defaulter


def get_order_details_total_due_amount(order_details_id):
    """
    This function is used to get total due amount on specific order details id. According to credit slab if user not pay
    the amount till date then the remaining amount will be due amount if it crosses the last pay date.
    """
    # user_id = 200
    is_defaulter = "False"
    today_date = datetime.datetime.now()
    order_details_obj = OrderDetails.objects.filter(id=order_details_id).first()
    if order_details_obj:
        total_due_amount = 0
        # checking for each orders..
        if order_details_obj.credit:
            # This function gives slabs of installments for this order.
            slabs = decide_slabs_of_credit(order_details_obj.id)
            if slabs:
                new_dict = {}
                new_amount = 0
                for date, amount in slabs.items():
                    new_amount += amount
                    new_dict.update({date: new_amount})

                for end_date, amount in new_dict.items():
                    data = get_amount_paid_for_order_details(order_details_obj)
                    if data:
                        paid_amount = data['rep_amount']
                        last_pay_date = data['last_date']
                        if paid_amount != 0 and last_pay_date is not None:
                            # if paid_amount < amount and today_date < end_date:
                            #     is_defaulter = "False"
                            if paid_amount < amount and today_date > end_date:
                                # is_defaulter = "True"
                                # total_due_amount += amount - paid_amount
                                total_due_amount = amount - paid_amount
                        else:
                            if today_date > end_date:
                                # is_defaulter = "True"
                                total_due_amount = amount

        return total_due_amount


class CreditRepaymentInfo(generics.ListAPIView):
    """
    This API is to give Credit repayments information to the user for particular order. It will return date, time,
    credit amount, remaining_amount, pay status and is_overdue or not. The post method returns same information
    but for specific order details.
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        user_id = request.user.id
        # user_id = 176
        try:
            if user_id is not None:
                data_list = []
                user_instance = User.objects.filter(id=user_id)
                if user_instance:
                    order_details_objects = OrderDetails.objects.filter(user=user_instance[0].id).exclude(
                        status="initiated").order_by('id')
                    if order_details_objects:
                        for order_details in order_details_objects:
                            repay_list = []
                            # total_amount_paid = 0
                            credit = 0
                            order_details_obj = OrderDetails.objects.filter(id=order_details.id)
                            if order_details_obj:
                                if order_details_obj[0].credit:  # Checks for Credit used for this order or Not..
                                    total_amount_paid = 0

                                    credit = order_details_obj[0].credit.actual_used
                                    from EApi.credit import get_order_details_return_cancel_amount
                                    # get order cancel and return total amount..
                                    total_cancel_return_amount = get_order_details_return_cancel_amount(
                                        order_details_obj[0].id)
                                    if total_cancel_return_amount:
                                        credit = credit - total_cancel_return_amount

                                    if credit != 0:
                                        order_date = order_details_obj[0].created_date
                                        credit_slabs = decide_slabs_of_credit(order_details_obj[0].id)
                                        if credit_slabs:
                                            new_dict_list = []
                                            new_dict = {}
                                            new_amount = 0
                                            for date, amount in credit_slabs.items():
                                                new_amount += amount
                                                new_dict = {'date': date, 'credit_amount': amount,
                                                            'new_amount': new_amount}
                                                new_dict_list.append(new_dict)

                                            for slab in new_dict_list:
                                                repay_dict = {}
                                                pay_status = ""
                                                is_overdue = ""
                                                end_date = slab['date']
                                                credit_amount = slab['credit_amount']
                                                amount = slab['new_amount']
                                                today_date = datetime.datetime.now()
                                                paid_amount = 0
                                                data = get_amount_paid_for_order_details(order_details_obj[0])
                                                if data:
                                                    paid_amount = data['rep_amount']
                                                    last_pay_date = data['last_date']
                                                    remaining_amount = 0
                                                    if paid_amount != 0 and last_pay_date is not None:
                                                        total_amount_paid = paid_amount
                                                        remaining_amount = amount - total_amount_paid
                                                        if remaining_amount < 0:
                                                            remaining_amount = 0
                                                        elif remaining_amount > credit_amount:
                                                            remaining_amount = credit_amount
                                                        if total_amount_paid < amount and today_date < end_date:
                                                            pay_status = "unpaid"
                                                            is_overdue = "No"
                                                        elif total_amount_paid < amount and today_date > end_date:
                                                            pay_status = "unpaid"
                                                            is_overdue = "Yes"
                                                        elif total_amount_paid >= amount:
                                                            pay_status = "paid"
                                                            is_overdue = "No"
                                                        # elif total_amount_paid >= amount and today_date > end_date:
                                                        #     pay_status = "unpaid"
                                                        #     is_overdue = "No"

                                                        repay_dict = {'date': str(end_date)[:10],
                                                                      'time': str(end_date)[11:19],
                                                                      'credit_amt': credit_amount,
                                                                      'remaining_amount': remaining_amount,
                                                                      'pay_status': pay_status,
                                                                      'is_overdue': is_overdue}
                                                        repay_list.append(repay_dict)

                                                    else:
                                                        if today_date < end_date:
                                                            pay_status = "unpaid"
                                                            is_overdue = "No"
                                                        elif today_date > end_date:
                                                            pay_status = "unpaid"
                                                            is_overdue = "Yes"

                                                        repay_dict = {'date': str(end_date)[:10],
                                                                      'time': str(end_date)[11:19],
                                                                      'credit_amt': credit_amount,
                                                                      'remaining_amount': credit_amount,
                                                                      'pay_status': pay_status,
                                                                      'is_overdue': is_overdue}
                                                        repay_list.append(repay_dict)

                                        remaining_payment = credit - total_amount_paid
                                        order_dict = {"login_id": str(order_details_obj[0].user.id),
                                                      "order_id": order_details_obj[0].order_text,
                                                      "order_date": str(order_details_obj[0].created_date)[:19],
                                                      "credit_used": credit,
                                                      "remaining_payment": remaining_payment,
                                                      "repay": repay_list
                                                      }

                                        data_list.append(order_dict)
                    return Response(
                        {
                            "status": 1,
                            "data": data_list
                        }
                    )
                else:
                    return Response(
                        {
                            "status": 2,
                            "data": data_list
                        }
                    )
        except Exception as e:
            print("------USer all details exc", e)
            print('-----------in exception----------')
            print(e.args)
            import os
            import sys
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)

    def post(self, request, *args, **kwargs):
        user_id = request.user.id
        order_text = request.data.get('order_text')
        wallet_amount = 0
        # user_id = 400
        try:
            if user_id is not None:
                data_list = []
                user_instance = User.objects.filter(id=user_id)
                if user_instance:
                    repay_list = []
                    # total_amount_paid = 0
                    credit = 0
                    order_details_obj = OrderDetails.objects.filter(order_text=order_text)
                    if order_details_obj:
                        if order_details_obj[0].credit:  # Checks for Credit used for this order or Not..
                            total_amount_paid = 0
                            credit = order_details_obj[0].credit.actual_used
                            from EApi.credit import get_order_details_return_cancel_amount
                            # get order cancel and return total amount..
                            total_cancel_return_amount = get_order_details_return_cancel_amount(
                                order_details_obj[0].id)
                            if total_cancel_return_amount:
                                credit = credit - total_cancel_return_amount

                            if credit != 0:
                                order_date = order_details_obj[0].created_date
                                credit_slabs = decide_slabs_of_credit(order_details_obj[0].id)
                                if credit_slabs:
                                    new_dict_list = []
                                    new_dict = {}
                                    new_amount = 0
                                    for date, amount in credit_slabs.items():
                                        new_amount += amount
                                        new_dict = {'date': date, 'credit_amount': amount,
                                                    'new_amount': new_amount}
                                        new_dict_list.append(new_dict)

                                    for slab in new_dict_list:
                                        repay_dict = {}
                                        pay_status = ""
                                        is_overdue = ""
                                        end_date = slab['date']
                                        credit_amount = slab['credit_amount']
                                        amount = slab['new_amount']
                                        today_date = datetime.datetime.now()
                                        paid_amount = 0
                                        data = get_amount_paid_for_order_details(order_details_obj[0])
                                        if data:
                                            paid_amount = data['rep_amount']
                                            last_pay_date = data['last_date']
                                            remaining_amount = 0
                                            if paid_amount != 0 and last_pay_date is not None:
                                                total_amount_paid = paid_amount
                                                remaining_amount = amount - total_amount_paid
                                                if remaining_amount < 0:
                                                    remaining_amount = 0
                                                elif remaining_amount > credit_amount:
                                                    remaining_amount = credit_amount
                                                if total_amount_paid < amount and today_date < end_date:
                                                    pay_status = "unpaid"
                                                    is_overdue = "No"
                                                elif total_amount_paid < amount and today_date > end_date:
                                                    pay_status = "unpaid"
                                                    is_overdue = "Yes"
                                                elif total_amount_paid >= amount:
                                                    pay_status = "paid"
                                                    is_overdue = "No"
                                                # elif total_amount_paid >= amount and today_date > end_date:
                                                #     pay_status = "unpaid"
                                                #     is_overdue = "No"

                                                repay_dict = {'date': str(end_date)[:10],
                                                              'time': str(end_date)[11:19],
                                                              'credit_amt': credit_amount,
                                                              'remaining_amount': remaining_amount,
                                                              'pay_status': pay_status,
                                                              'is_overdue': is_overdue}
                                                repay_list.append(repay_dict)

                                            else:
                                                if today_date < end_date:
                                                    pay_status = "unpaid"
                                                    is_overdue = "No"
                                                elif today_date > end_date:
                                                    pay_status = "unpaid"
                                                    is_overdue = "Yes"

                                                repay_dict = {'date': str(end_date)[:10],
                                                              'time': str(end_date)[11:19],
                                                              'credit_amt': credit_amount,
                                                              'remaining_amount': credit_amount,
                                                              'pay_status': pay_status,
                                                              'is_overdue': is_overdue}
                                                repay_list.append(repay_dict)

                                remaining_payment = credit - total_amount_paid
                                from EApi.credit import get_user_wallet_amount
                                wallet_dict = get_user_wallet_amount(user_instance[0].id)
                                if wallet_dict:
                                    wallet_amount = wallet_dict["balance_amount"]
                                order_dict = {"login_id": str(order_details_obj[0].user.id),
                                              "order_id": order_details_obj[0].order_text,
                                              "order_date": str(order_details_obj[0].created_date)[:19],
                                              "credit_used": credit,
                                              "remaining_payment": remaining_payment,
                                              "repay": repay_list,
                                              "available_wallet_amount": wallet_amount
                                              }

                                data_list.append(order_dict)
                    return Response(
                        {
                            "status": 1,
                            "data": data_list
                        }
                    )
                else:
                    return Response(
                        {
                            "status": 2,
                            "data": data_list
                        }
                    )

        except Exception as e:
            print("------USer all details exc", e)
            print('-----------in exception----------')
            print(e.args)
            import os
            import sys
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)


class CreditRepaymentDetails(generic.ListView):
    template_name = "Ecom/ecom_payments/credit_repayment_details.html"
    def get(self, request, *args, **kwargs):
        ctx = {}
        data_dict = {}
        credit_given = 0
        outstanding = 0
        get_repayment_amount = 0
        total_used = 0
        df = pd.DataFrame(
            list(Credit.objects.exclude(status="initiated").order_by('created_date').values_list("user", "credit")),
            columns=['user', 'credit'])
        total_credit = df.groupby('user').tail(1)
        credit_given = total_credit['credit'].sum()
        df = pd.DataFrame(
            list(
                Credit.objects.exclude(status="initiated").order_by('created_date').values_list("user", "used_credit")),
            columns=['user', 'used_credit'])
        total_credit = df.groupby('user').tail(1)
        outstanding = total_credit['used_credit'].sum()
        repayment_amount = Payments.objects.filter(product_type="Ecom", status='success',
                                                   category="Repayment").aggregate(Sum('amount'))['amount__sum']
        if repayment_amount:
            get_repayment_amount = repayment_amount
        if credit_given:
            credit_given = credit_given
        if outstanding:
            outstanding = outstanding
        total_used = outstanding + get_repayment_amount
        data_dict.update({total_used: [get_repayment_amount, outstanding, credit_given]})
        ctx['credits'] = data_dict
        return render(request, self.template_name, ctx)


class UserWiseCreditRepaymentDetails(generic.ListView):
    """
    This class is used to show userwise credit repayment details. It will return users credit given, used credit,
    total repayment and total remaining.
    """
    template_name = "Ecom/ecom_payments/sellerwise_credit_repayment_details.html"

    def get(self, request, *args, **kwargs):
        ctx = {}
        data_dict = {}
        credit_given = 0
        users = Credit.objects.filter(status__in=["success", "repayment"]).values_list('user__username',
                                                                                       flat='True').distinct()
        for user in users:
            credit_obj = Credit.objects.filter(user__username=user, status='success').last()
            if credit_obj:
                credit_given = credit_obj.credit
                used_credit = credit_obj.used_credit
            else:
                credit_given = 0
                used_credit = 0
            total_repayment = \
                Credit.objects.filter(user__username=user, status='repayment').aggregate(Sum('repayment_add'))[
                    'repayment_add__sum']
            if total_repayment:
                total_repayment = total_repayment
            else:
                total_repayment = 0
            total_remaining = used_credit - total_repayment
            data_dict.update({user: [credit_given, used_credit, total_repayment, total_remaining]})
            ctx['credits'] = data_dict
        return render(request, self.template_name, ctx)


class UserWiseOrderCreditDetails(generic.ListView):
    """
    This class is used to show users order details for specific credit. It will return order details object.
    """
    template_name = "Ecom/ecom_payments/sellerwise_order_credit_details.html"

    def get(self, request, user, *args, **kwargs):
        ctx = {}
        orders = []
        credit = Credit.objects.filter(user__username=user, status='success')
        if credit:
            orders = OrderDetails.objects.filter(credit__in=credit)
            if orders:
                ctx['orders'] = orders
        return render(request, self.template_name, ctx)


class UserWiseOrderPaymentDetails(generic.ListView):
    """
    This class is used to show userwise orders repayment details. It will return order details objects its paid amount and
    remaining amount. The amount paid for order details is get from payment attached to specific order details.
    """
    template_name = "Ecom/ecom_payments/sellerwise_order_payment_details.html"

    def get(self, request, user, *args, **kwargs):
        ctx = {}
        data_dict = {}
        payment = Payments.objects.filter(product_type='Ecom', category='Repayment', status='success')
        if payment:
            order_details_obj = OrderDetails.objects.filter(user__username=user, payment__in=payment)
            if order_details_obj:
                order_details_objects = set(order_details_obj)
                for order_details in order_details_objects:
                    if order_details.credit:
                        paid_amount = 0
                        payment_amount = order_details.payment.filter(product_type="Ecom", category='Repayment',
                                                                      status='success').aggregate(Sum('amount'))[
                            'amount__sum']
                        if payment_amount:
                            paid_amount = payment_amount
                        remaining_amount = order_details.credit.actual_used - paid_amount
                        data_dict.update({order_details: [paid_amount, remaining_amount]})
                        ctx['orders'] = data_dict
        return render(request, self.template_name, ctx)


class RepaymentOrderDetails(generic.ListView):
    """
    This class is used to show order details and repayment amount. It will return order details objects its paid amount and
    remaining amount. The amount paid for order details is get from payment attached to specific order details.
    """
    template_name = "Ecom/ecom_payments/sellerwise_order_payment_details.html"

    def get(self, request, *args, **kwargs):
        ctx = {}
        data_dict = {}
        payment = Payments.objects.filter(product_type='Ecom', category='Repayment', status='success')
        if payment:
            order_details_obj = OrderDetails.objects.filter(payment__in=payment)
            if order_details_obj:
                order_details_objects = set(order_details_obj)
                for order_details in order_details_objects:
                    if order_details.credit:
                        paid_amount = 0
                        payment_amount = order_details.payment.filter(product_type="Ecom", category='Repayment',
                                                                      status='success').aggregate(Sum('amount'))[
                            'amount__sum']
                        if payment_amount:
                            paid_amount = payment_amount
                        remaining_amount = order_details.credit.actual_used - paid_amount
                        data_dict.update({order_details: [paid_amount, remaining_amount]})
                        ctx['orders'] = data_dict
        return render(request, self.template_name, ctx)


class UserwiseCreditGot(generic.ListView):
    """
    This class is used to show total credit used to user. It will return user its credit object and total used credit till date.
    The total used credit is given from subtraction of actual used sum and credit_used get from get_order_details_used_credit function.
    """
    template_name = "Ecom/ecom_payments/userwise_credit_got.html"

    def get(self, request, *args, **kwargs):
        ctx = {}
        data_dict = {}
        total_used = 0
        final_total_used = 0
        user_name = ""
        credit_got = Credit.objects.filter(status__in=["fetched", "default", "success"]).values_list("user__username",
                                                                                                     flat=True).distinct()
        if credit_got:
            for user in credit_got:
                final_total_used = 0
                actual_used = 0
                credit_objects = Credit.objects.filter(user__username=user).exclude(status='initiated').last()
                if credit_objects:
                    user_name = None
                    name = UserPersonal.objects.filter(user=credit_objects.user).values_list("name", flat=True)
                    if name:
                        user_name = name[0]
                    total_used = 0
                    actual_used = Credit.objects.filter(user__username=user).exclude(status='initiated').aggregate(
                        Sum('actual_used'))['actual_used__sum']
                    if actual_used:
                        actual_used = actual_used
                    else:
                        actual_used = 0
                    order_details_obj = OrderDetails.objects.filter(user__username=user)
                    if order_details_obj:
                        for order_details in order_details_obj:
                            used = get_order_details_used_credit(order_details.id)
                            if used:
                                total_used += used
                            else:
                                total_used = 0
                    final_total_used = actual_used - total_used
                data_dict.update({user: [credit_objects, user_name, final_total_used]})
            ctx['credit_objects'] = data_dict
        return render(request, self.template_name, ctx)


class NetReceivables(generic.ListView):
    """
    This class is used to get users total credit, its actual used credit and its remaining credit.
    """
    template_name = "Ecom/ecom_payments/net_receivables.html"
    def get(self, request, *args, **kwargs):
        ctx = {}
        data_dict = {}
        total_credit = 0
        total_repayment = 0
        df = pd.DataFrame(
            list(Credit.objects.exclude(status="initiated").order_by('created_date').values_list("user", "credit")),
            columns=['user', 'credit'])
        total_credit = df.groupby('user').tail(1)
        total_credit = total_credit['credit'].sum()
        actual_used = Credit.objects.aggregate(Sum('actual_used'))['actual_used__sum']
        total_remaining = total_credit - actual_used
        data_dict.update({total_credit: [actual_used, total_remaining]})
        ctx["credits"] = data_dict
        return render(request, self.template_name, ctx)


def get_sellers_last_balance_amount(seller_id):
    """
    This function returns sellers last available balance..
    It should be plus or minus or 0.
    """
    balance_dict = {}
    if seller_id is not None:
        seller_wallet_obj = SellerWallet.objects.filter(seller=seller_id,
                                                        is_wallet_consumed=False).order_by('id').last()

        if seller_wallet_obj:
            balance_dict["balance_amount"] = seller_wallet_obj.balance_amount
            balance_dict["last_wallet_obj"] = seller_wallet_obj

    return balance_dict


def update_sellers_wallet_balance(seller_obj, invoice_obj, amount_add, amount_sub):
    """
    This function gets Sellers object, Invoice object, amount to add, amount to sub
    and calculate for new entry of Seller Wallet..
    """
    if None not in (seller_obj, amount_add, amount_sub):
        last_available_balance = 0
        last_wallet_obj = None
        is_full_amount_paid = False
        full_amount_paid_date = None
        # Getting sellers last balance remained to pay..
        actual_invoice_amount = amount_add - amount_sub
        get_last_balance = get_sellers_last_balance_amount(seller_obj.id)
        if get_last_balance:
            last_available_balance = get_last_balance["balance_amount"]
            last_wallet_obj = get_last_balance["last_wallet_obj"]
        forwarded_amount = amount_add + last_available_balance
        new_balance_amount = forwarded_amount - amount_sub

        if actual_invoice_amount == 0:
            # This is the case where all the previous invoices should clear..
            is_full_amount_paid = True
            full_amount_paid_date = datetime.datetime.now()
        if actual_invoice_amount < 0:
            # This is the case where all the previous invoices should clear and add the \
            # amount in Sellers clear amount table..
            is_full_amount_paid = True
            full_amount_paid_date = datetime.datetime.now()
            # Updating sellers new clear amount..
            clear_amount = abs(actual_invoice_amount)
            update_clear_amount = update_sellers_clear_amount(seller_obj, clear_amount)

        add_seller_wallet = SellerWallet.objects.create(amount_add=amount_add, forwarded_amount=forwarded_amount,
                                                        amount_sub=amount_sub,
                                                        is_full_amount_paid=is_full_amount_paid,
                                                        full_amount_paid_date=full_amount_paid_date,
                                                        actual_invoice_amount=actual_invoice_amount,
                                                        balance_amount=new_balance_amount, seller=seller_obj,
                                                        invoice=invoice_obj)

        # Consuming last wallet entry in Sellers Wallet..
        if add_seller_wallet:
            if last_wallet_obj:
                carry_fwd_last_obj = SellerWallet.objects.filter(id=last_wallet_obj.id).update(is_wallet_consumed=True,
                                                                                               updated_date=datetime.datetime.now())

            # Clear the invoices by getting clear amount
            clear_amount = get_sellers_invoice_clear_amount(seller_obj.id)
            if clear_amount:
                clear_invoices = clear_sellers_invoices(seller_obj, clear_amount)

            return True
    return False


def update_sellers_clear_amount(seller_obj, amount):
    """
    This function is used to update sellers clear amount in SellerInvoiceClearAmount. The clear amount is get from
    get_sellers_invoice_clear_amount function. If entry is not present in SellerInvoiceClearAmount table for specific
    user then create new entry in SellerInvoiceClearAmount for that user.
    """
    # getting sellers previous clear amount..
    clear_amount = get_sellers_invoice_clear_amount(seller_obj.id)
    # Updating entry in SellerInvoiceClearAmount table..
    new_clear_amount = clear_amount + amount
    clear_obj = SellerInvoiceClearAmount.objects.filter(seller=seller_obj.id)
    if clear_obj:
        SellerInvoiceClearAmount.objects.filter(id=clear_obj[0].id).update(amount=new_clear_amount,
                                                                           updated_date=datetime.datetime.now())
    else:
        SellerInvoiceClearAmount.objects.create(seller=seller_obj, amount=new_clear_amount)

    return True


def clear_sellers_invoices(seller_obj, amount):
    """
    This function get the seller id and amount and clears the invoices by comparing it in actual_invoice_amount.
    If remaining_amount is greater than zero then update the remaning amount in SellerInvoiceClearAmount, otherwise
    create new entry if entry is not present for specific user.
    """
    unpaid_invoices = SellerWallet.objects.filter(seller=seller_obj.id, is_full_amount_paid=False,
                                                  payment_status=False,
                                                  actual_invoice_amount__gt=0).order_by('id')
    remaining_amount = amount
    if unpaid_invoices:
        for invoice in unpaid_invoices:
            if remaining_amount >= invoice.actual_invoice_amount:
                update_wallet = SellerWallet.objects.filter(id=invoice.id).update(
                    is_full_amount_paid=True, full_amount_paid_date=datetime.datetime.now())

                if invoice.invoice:
                    update_invoice = SellerAmount.objects.filter(id=invoice.invoice.id).update(
                        is_full_amount_paid=True, full_amount_paid_date=datetime.datetime.now(),
                        updated_date=datetime.datetime.now())

                remaining_amount = remaining_amount - invoice.actual_invoice_amount
            else:
                break
    if remaining_amount >= 0:
        # Update Sellers clear amount..
        seller_invoice_obj = SellerInvoiceClearAmount.objects.filter(seller=seller_obj.id).first()
        if seller_invoice_obj:
            SellerInvoiceClearAmount.objects.filter(id=seller_invoice_obj.id).update(amount=remaining_amount,
                                                                                     updated_date=datetime.datetime.now())
        else:
            SellerInvoiceClearAmount.objects.create(seller=seller_obj, amount=remaining_amount)

    return True


def recharge_sellers_wallet(seller_obj, seller_payment_obj):
    """
    This function creates payment entry in SellersWallet and clears previous invoices with recharge amount..
    """
    if None not in (seller_obj, seller_payment_obj):
        paid_amount = int(seller_payment_obj.amount)
        last_amount = 0
        last_obj = None
        last_balance = get_sellers_last_balance_amount(seller_obj.id)
        if last_balance:
            last_amount = last_balance["balance_amount"]
            last_obj = last_balance["last_wallet_obj"]
        new_balance_amount = last_amount - paid_amount
        add_wallet = SellerWallet.objects.create(seller=seller_obj, actual_paid=paid_amount,
                                                 payment_status=True,
                                                 balance_amount=new_balance_amount,
                                                 seller_payment=seller_payment_obj)

        if add_wallet:
            if last_obj:
                update_entry = SellerWallet.objects.filter(id=last_obj.id).update(is_wallet_consumed=True,
                                                                                  updated_date=datetime.datetime.now())

            if new_balance_amount < 0:
                new_balance_amt = abs(new_balance_amount)
                update_clear_amount = update_sellers_clear_amount(seller_obj, int(new_balance_amt))

            # Update Sellers clear amount..
            update_clear_amount = update_sellers_clear_amount(seller_obj, int(paid_amount))
            if update_clear_amount:
                clear_amount = get_sellers_invoice_clear_amount(seller_obj.id)
                if clear_amount >= 0:
                    # clear previous invoices..
                    clear_sellers_invoices(seller_obj, clear_amount)
                else:
                    pass

        return True


def get_sellers_invoice_clear_amount(seller_id):
    """
    This function takes seller_id and returns clear amount that is present in SellerInvoiceClearAmount table..
    """
    clear_amount = 0
    if seller_id is not None:
        clear_invoice_obj = SellerInvoiceClearAmount.objects.filter(seller=seller_id).order_by('id').last()
        if clear_invoice_obj:
            clear_amount = clear_invoice_obj.amount

    return clear_amount


@login_required(login_url="/ecom/login/")
def seller_pending_amount_invoice(request):
    """
    This function is used to calculate sellers pending invoice amount. It will return seller name, balance amount and
    last wallet object.
    """
    data_dict = {}
    ctx = {}
    user_id = request.user.id
    user = User.objects.filter(id=user_id)
    seller_group = user[0].groups.filter(name='Seller').exists()
    if seller_group:
        url = "/ecom/seller_wallet_data/" + str(user_id) + "/"
        return redirect(url)

    last_wallet_obj = None
    pending_amount = 0
    balance_amount = 0
    invoice_id = 0
    seller_list = SellerWallet.objects.values_list("seller", flat=True).distinct()
    if seller_list:
        for seller in seller_list:
            pending_amount = get_sellers_last_balance_amount(seller)
            seller_name = User.objects.filter(id=seller)
            if pending_amount:
                balance_amount = pending_amount['balance_amount']
                last_wallet_obj = pending_amount['last_wallet_obj']
                if last_wallet_obj:
                    last_wallet_obj = last_wallet_obj
                else:
                    last_wallet_obj = None
            else:
                balance_amount = 0
            data_dict.update({seller_name[0]: [balance_amount, last_wallet_obj, seller]})
        ctx['pending_amount'] = data_dict
    return render(request, "Ecom/seller_pending_amount_invoice.html", ctx)


@login_required(login_url="/ecom/login/")
def seller_wallet_data(request, seller):
    """
    This function is used to pass seller to the template.
    """
    ctx = {}
    ctx['seller'] = seller
    return render(request, "Ecom/seller_wallet_data.html", ctx)


from math import ceil


@csrf_exempt
@login_required(login_url="/ecom/login/")
def seller_wallet_ajax_data(request):
    """
    This function is used to get seller wallet data. This function is called by ajax and pagination is used in it.
    It will show SellerWallet data for specific seller.
    """
    ctx = {}
    count = 0
    data_dict = {}
    wallet_list = []
    if request.method == "POST":
        page_no = int(request.POST.get("page_no"))
        ppp = int(request.POST.get("ppp"))
        seller = request.POST.get("seller")

        if page_no:
            points = get_start_end_ponit(page_no, ppp)
        if seller:
            wallet_obj = SellerWallet.objects.filter(seller=seller).order_by('-id')[points[0]:points[1]]
            count = SellerWallet.objects.filter(seller=seller).order_by('-id').count()
            total_page_count = ceil(int(count) / (ppp))
            page_status_info = page_status(page_no, total_page_count)
            next = page_status_info[1]
            previous = page_status_info[0]
            if wallet_obj:
                for obj in wallet_obj:
                    seller_name = Seller.objects.filter(user__id=seller)
                    date = str(obj.created_date)[:10]
                    data_dict = {
                        "seller_name": seller_name[0].name,
                        "amount_add": obj.amount_add,
                        "forwarded_amount": obj.forwarded_amount,
                        "amount_sub": obj.amount_sub,
                        "balance_amount": obj.balance_amount,
                        "actual_paid": obj.actual_paid,
                        "date": date
                    }
                    wallet_list.append(data_dict)

    ctx["data_dict"] = wallet_list
    ctx["length"] = len(wallet_list)
    ctx['page_count'] = total_page_count
    ctx['current_page'] = page_no
    ctx['previous'] = page_status_info[0]
    ctx['next'] = page_status_info[1]

    return HttpResponse(json.dumps(ctx))


def get_start_end_ponit(current_page, product_per_page):
    """
       This function is used for pagination. The input to the function is current page and product per page.
       It will return the start point and end point of page.
    """
    start_point = 0
    end_point = 0
    if not current_page:
        current_page = 0
    if current_page is None or not current_page or current_page == 0:
        start_point = 0
        end_point = start_point + product_per_page
    else:
        end_point = current_page * product_per_page
        start_point = end_point - product_per_page

    return (start_point, end_point)


def page_status(current_page, total_page_count):
    """
       The function is used for pagination. To show next and previous button it will get true and false status.
       If true then show respective previous and next button.
    """
    previous = "False"
    next = "False"
    if current_page <= 1:
        next = "True"
        previous = "False"
    if current_page == total_page_count:
        previous = "True"
        next = "False"
    if current_page <= 1 and current_page == total_page_count:
        previous = "False"
        next = "False"
    if 1 < current_page < total_page_count:
        previous = "True"
        next = "True"
    return (previous, next)


def get_order_details_used_credit(order_details_id):
    """
    This function is used to get used credit of specific order details id. The return and cancel order amount
    is deducted from actual used credit. Then remaining credit is return this function.
    """
    credit = 0
    order_details_obj = OrderDetails.objects.filter(id=order_details_id)
    if order_details_obj[0].credit:
        credit = order_details_obj[0].credit.actual_used
        from EApi.credit import get_order_details_return_cancel_amount
        # get order cancel and return total amount..
        total_cancel_return_amount = get_order_details_return_cancel_amount(order_details_obj[0].id)
        if total_cancel_return_amount:
            credit = credit - total_cancel_return_amount

    return credit


@login_required(login_url="/ecom/login/")
def default_chart(request):
    """
    This function is use to show defaulter data of each state of India.
    """
    dataSource = OrderedDict()
    mapConfig = OrderedDict()
    mapConfig["caption"] = "Default Information"
    mapConfig["includevalueinlabels"] = "1"
    mapConfig["labelsepchar"] = ":"
    mapConfig["entityFillHoverColor"] = "#808080"
    mapConfig["theme"] = "fusion"
    colorDataObj = {"minvalue": "0", "code": "#b7e4eb", "gradient": "1",
                    "color": [
                        {"minValue": "0.0", "maxValue": "1.0", "code": "#a3dde6"},
                        {"minValue": "1.0", "maxValue": "10.0", "code": "#8bdce8"},
                        {"minValue": "10.0", "maxValue": "50.0", "code": "#67c1cf"},
                        {"minValue": "50.0", "maxValue": "100.0", "code": "#7aced65"},
                        {"minValue": "100.0", "maxValue": "200.0", "code": "#6bc8d1"},
                        {"minValue": "200.0", "maxValue": "300.0", "code": "#6bc8d1"},
                        {"minValue": "300.0", "maxValue": "400.0", "code": "#36cee6"},
                        {"minValue": "400.0", "maxValue": "500.0", "code": "#1fc8e3"},
                        {"minValue": "500.0", "maxValue": "600.0", "code": "#1ab6cf"},
                        {"minValue": "600.0", "maxValue": "700.0", "code": "#17a2b8"},
                        {"minValue": "700.0", "maxValue": "800.0", "code": "#148ea1"},
                        {"minValue": "800.0", "maxValue": "900.0", "code": "#117a8b"},
                        {"minValue": "900.0", "maxValue": "1000.0", "code": "#0f6674"},
                        {"minValue": "1000.0", "maxValue": "2000.0", "code": "#0f6674"},
                    ]
                    }
    data = []
    dict = {
        "001": "Andaman and Nicobar Islands", "002": "andhra pradesh", "003": "Arunachal Pradesh", "004": "assam",
        "005": "bihar", "006": "chandigarh", "007": "chattisgarh", "008": "dadra & nagar haveli", "009": "daman & diu",
        "010": "delhi", "011": "goa", "012": "gujarat", "013": "haryana", "014": "himachal pradesh",
        "015": "jammu & kashmir", "016": "jharkhand", "017": "karnataka", "018": "kerala", "019": "Lakshadweep",
        "020": "madhya pradesh", "021": "maharashtra", "022": "manipur", "023": "meghalaya", "024": "mizoram",
        "025": "nagaland", "026": "odisha", "027": "pondicherry", "028": "punjab", "029": "rajasthan", "030": "Sikkim",
        "031": "tamil nadu", "032": "tripura", "033": "uttar pradesh", "034": "uttarakhand", "035": "west bengal",
        "036": "telangana"
    }
    order_dict = {}
    final_order_dict = {}

    for index, state in dict.items():
        count_list = []
        order_count = 0
        check_state = OrderDetails.objects.filter(address__state__icontains=state).exclude(status="initiated")
        if check_state:
            user = OrderDetails.objects.filter(address__state__icontains=state).exclude(
                status="initiated").values_list("user",
                                                flat=True).distinct()
            if user:
                for i in user:
                    default_user = check_user_defaulter_status(i)

                    if default_user == 'True':

                        count = default_user.count('True')
                        count_list.append(count)
                    else:
                        pass

        count_value = len(count_list)

        data_dict = {
            "id": index,
            "value": count_value,
        }
        data.append(data_dict)
        order_dict.update({
            state: count_value
        })
    k = Counter(order_dict)
    high = k.most_common(10)
    for i in high:
        final_order_dict.update({
            i[0]: i[1]
        })

    dataSource["chart"] = mapConfig
    dataSource["colorrange"] = colorDataObj
    dataSource["data"] = data
    fusionMap = FusionCharts("maps/india", "ex1", "400", "600", "chart-1", "json", dataSource)
    return render(request, 'Ecom/default_map.html', {'output': fusionMap.render(), 'data': final_order_dict})


@login_required(login_url="/ecom/login/")
def user_due_amount(request):
    """
    This function is used to show users and its due amount. The function get_order_details_total_due_amount is return
    due amount.
    """
    ctx = {}
    data_dict = {}
    check_state = OrderDetails.objects.exclude(status="initiated")
    if check_state:
        user_data = OrderDetails.objects.exclude(status="initiated").values_list("user__username", "id")
        if user_data:
            for i in user_data:
                due_amount = get_order_details_total_due_amount(i[1])
                data_dict.update({i[0]: due_amount})
    ctx['data'] = data_dict
    return render(request, 'Ecom/user_due_amount.html', ctx)


@login_required(login_url="/ecom/login/")
def user_due_amount_current(request):
    """
    This function is used to show current defaulters due amount. To get current defaulters check_user_defaulter_status
    function used and to get its due amount get_order_details_total_due_amount function used.
    """
    ctx = {}
    data_dict = {}

    user_data = OrderDetails.objects.exclude(status="initiated").values_list("user", "id", "user__username",
                                                                             "order_text", "created_date",
                                                                             "order_id")
    if user_data:
        for i in user_data:
            user_overdue = check_user_defaulter_status(i[0])

            if user_overdue == 'True':

                value = get_order_details_total_due_amount(i[1])

                if value != 0:

                    order = i[3]
                    date = i[4]

                    data_dict.update({i[2]: [value, order, date, i[5]]})

                else:
                    pass

    ctx['data'] = data_dict
    return render(request, 'Ecom/user_due_amount_1.html', ctx)


def get_order_details_required_payment(order_details_id):
    """
    This function gives order details credit repayment amount required to do the new
    Order according to the repayment amount and Credit used.
    """
    if order_details_id is not None:
        payable_amount = None
        today_date = datetime.datetime.now()
        get_order_obj = OrderDetails.objects.filter(id=order_details_id).first()
        if get_order_obj:
            credit = None
            if get_order_obj.credit:
                credit = get_order_obj.credit.actual_used
            if credit:
                # This function gives slabs of installments for this order.
                slabs = decide_slabs_of_credit(order_details_id)
                if slabs:
                    new_dict = {}
                    new_amount = 0
                    installment_list = []
                    full_installment_list = []
                    for date, amount in slabs.items():
                        new_amount += amount
                        installment_list.append(amount)
                        full_installment_list.append(new_amount)
                        new_dict.update({date: new_amount})
                    for end_date, amount in new_dict.items():
                        data = get_amount_paid_for_order_details(get_order_obj)
                        if data:
                            paid_amount = data['rep_amount']
                            last_pay_date = data['last_date']
                            if paid_amount != 0 and last_pay_date is not None:
                                if paid_amount < amount:
                                    remaining_amount = amount - paid_amount
                                    paid_amount_percentage = (paid_amount / amount) * 100
                                    if paid_amount_percentage <= 60:
                                        payable_amount = remaining_amount
                                    if paid_amount_percentage > 60:
                                        amount_index = full_installment_list.index(amount)
                                        next_index = amount_index + 1
                                        if next_index > 2:
                                            payable_amount = remaining_amount
                                        else:
                                            next_installment = installment_list[next_index]
                                            payable_amount = remaining_amount + next_installment
                                    if payable_amount and payable_amount >= 0:
                                        return payable_amount
                                if paid_amount == amount:
                                    amount_index = full_installment_list.index(amount)
                                    next_index = amount_index + 1
                                    if next_index < 2:
                                        next_installment = installment_list[next_index]
                                        payable_amount = next_installment
                                        if payable_amount and payable_amount >= 0:
                                            return payable_amount

                            else:
                                payable_amount = amount
                                return payable_amount
        return payable_amount


def get_users_due_order_and_amount(user_id):
    """
    This function returns Order details and its remaining payable amount.
    """
    return_dict = None
    if user_id is not None:
        is_valid = is_last_repayment_valid(user_id)
        if is_valid:
            return return_dict
        else:
            # Getting all orders used by credit..
            get_unpaid_orders = OrderDetails.objects.filter(user=user_id).exclude(
                status__in=["initiated", "cancelled"]).exclude(
                credit_status__in=["Failed", "Paid"]).order_by('created_date')
            if get_unpaid_orders:
                for order_details in get_unpaid_orders:
                    # Getting order details remaining payment..
                    payable_amount = get_order_details_required_payment(order_details.id)
                    if payable_amount:
                        return_dict = {
                            "payable_amount": payable_amount,
                            "order_text": order_details.order_text
                        }
                        return return_dict
    return return_dict


def get_last_repayment(user_id):
    """
    This function returns the last repayment done by the user for any of his orders..
    """
    repaid_dict = None
    time_now = datetime.datetime.now()
    time_before_six_hours = time_now - datetime.timedelta(hours=6)
    if user_id is not None:
        order_payments = OrderDetails.objects.filter(user=user_id, payment__category="Repayment",
                                                     payment__status="success").values_list('payment__id')
        if order_payments:
            order_text = None
            repaid_amount = None
            repayment = Payments.objects.filter(id__in=order_payments).order_by('id').last()
            if repayment:
                repaid_amount = repayment.amount
                order_id = repayment.order_id
                if order_id:
                    order_id_list = order_id.split('e')
                    if order_id_list:
                        order_text = order_id_list[0]
                repay_date = repayment.create_date
                if repay_date >= time_before_six_hours:
                    if None not in (order_text, repaid_amount):
                        repaid_dict = {
                            "repaid_amount": repaid_amount,
                            "order_text": order_text,
                            "repay_date": repay_date
                        }

    return repaid_dict


def is_last_repayment_valid(user_id):
    """
    This function is used to check valid repayment or not. The repayment amount, order_rext and repayment date is
    get from get_last_repayment. Then get the actual used for this order details and to calculate installment calculate
    33 % of actual used credit then calculate 80% of installment.
    """
    if user_id:
        repaid_dict = get_last_repayment(user_id)
        if repaid_dict:
            amount = repaid_dict["repaid_amount"]
            order_text = repaid_dict["order_text"]
            repay_date = repaid_dict["repay_date"]
            if order_text:
                get_order_details_obj = OrderDetails.objects.filter(order_text=order_text).first()
                if get_order_details_obj:
                    if get_order_details_obj.credit:
                        # This function gives slabs of installments for this order.
                        credit = get_order_details_obj.credit.actual_used
                        if credit and credit > 0:
                            installment = (credit * 33) / 100
                            eighty_installment = (installment * 80) / 100
                            if amount >= eighty_installment:
                                # Checking if any orders placed after this??
                                get_orders = OrderDetails.objects.filter(user=user_id,
                                                                         created_date__gt=repay_date).exclude(
                                    status="initiated")
                                if not get_orders:
                                    return True
    return False


import csv


def sell_report(request):
    """
    This function is used render to the template.
    """
    return render(request, "Ecom/ecom_payments/sell_report.html")


def sell_csv(request):
    """
    This function is used to return sell csv. It conatins all details of user presonal details, Order details,
    shipping charge, convenient fees and taxes.
    """
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="product_upload_status.csv"'
    writer = csv.writer(response)
    writer.writerow(
        ['Mobile No', 'PAN No', 'Payment Type', 'Order Id', 'Unique Order Id', 'Product Name', 'Status', 'Category',
         'Subcategory', 'BaseCategory',
         'SGST', 'CGST', 'Amount', 'TAX Amount', 'Shipping Charge', 'ConvenientFees', 'Total Amount', 'State', 'Date'])
    order_id = ''
    unique_order_id = ''
    status = ''
    product_name = ''
    category = ''
    subcategory = ''
    basecategory = ''
    SGST = ''
    CGST = ''
    TAX_Amount = ''
    Total_Amount = ''
    convenient_fees = 0
    user_amount = 0
    sgst = 0
    cgst = 0
    seller_amount = 0
    Amount = 0
    total_amount = 0
    shipping_charge = 0
    mobile_no = ''
    pan_no = ''
    payment_type = 'mix'
    state = ''
    date = ''
    month = None
    if "month" in request.POST:
        month = request.POST["month"]
        date1 = month.split("-")
        month = "-".join([date1[1], date1[0]])

        users = OrderDetails.objects.filter(created_date__startswith=month).values_list('user__username',
                                                                                        flat=True).distinct()

    elif "from_date" in request.POST:
        from_date = request.POST['from_date']
        to_date = request.POST['to_date']
        users = OrderDetails.objects.filter(created_date__gte=from_date, created_date__lte=to_date).values_list(
            'user__username',
            flat=True).distinct()
    else:
        month = None

    for user in users:
        if month:
            order_details = OrderDetails.objects.filter(created_date__startswith=month,
                                                        user__username=user).exclude(status='initiated').values_list(
                "order_id")
        else:
            order_details = OrderDetails.objects.filter(created_date__range=[from_date, to_date],
                                                        user__username=user).exclude(status='initiated').values_list(
                "order_id")

        order_obj = Order.objects.filter(id__in=order_details)
        if order_obj:
            for order in order_obj:
                convenient_fees = 0
                shipping_charge = 0
                sgst = 0
                cgst = 0
                seller_amount = 0
                amount = 0
                user_amount = 0
                order_id = order.order_id
                unique_order_id = order.unique_order_id
                product_name = order.product_details.product_id.name
                status = order.status
                category = order.product_details.product_id.base_category.sub_category.category.name
                subcategory = order.product_details.product_id.base_category.sub_category.name
                basecategory = order.product_details.product_id.base_category.base_name
                date = str(order.created_date)[:11]
                from .views import order_price_details
                get_data = order_price_details(order.id)
                if get_data:
                    mudra_amount = get_data['mudra_amount']
                    seller_amount = get_data['seller_amount']
                    sgst = get_data['sgst']
                    cgst = get_data['cgst']
                # total sgst and cgst
                total_tax = sgst + cgst
                if total_tax != 0:
                    # quantity and seller_amount
                    amount = order.quantity * (float(mudra_amount))
                    # tax in rupees
                    user_amount = (float(amount) * total_tax) / 100
                    # calulate tax and amount
                    total_amount = ceil(float(amount) + user_amount)
                SGST = sgst
                CGST = cgst
                Amount = amount * 100 / (100 + total_tax)
                user_amount = (float(Amount) * total_tax) / 100
                TAX_Amount = user_amount
                # Total_Amount  = total_amount
                if order.delivery_charges:
                    shipping_charge = order.delivery_charges.amount
                    if shipping_charge:
                        shipping_charge = shipping_charge
                else:
                    shipping_charge = 0
                if order.convenient_fees:
                    conv = order.convenient_fees.amount
                    if conv:
                        convenient_fees = conv
                else:
                    convenient_fees = 0
                from EApi.order import get_order_details_delivery_charge
                order_details_obj = OrderDetails.objects.filter(order_id=order.id)
                if order_details_obj:
                    payment_type = order_details_obj[0].payment_mode
                    if order_details_obj[0].convenient_fees:
                        pass
                        # convenient_fees = order_details_obj[0].convenient_fees.amount
                    if order_details_obj[0].address:
                        state = order_details_obj[0].address.state
                user_personal_obj = UserPersonal.objects.filter(user__username=user)
                if user_personal_obj:
                    for user_personal in user_personal_obj:
                        pan_number = user_personal.pan_number
                        if pan_number and pan_number is not None:
                            pan_no = pan_number
                Total_Amount = amount + shipping_charge + convenient_fees
                writer.writerow(
                    [user, pan_no, payment_type, order_id, unique_order_id, product_name, status, category, subcategory,
                     basecategory,
                     SGST, CGST, Amount, TAX_Amount, shipping_charge, convenient_fees, Total_Amount, state, date])

    return response
