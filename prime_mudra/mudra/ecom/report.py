import datetime
from django.apps.registry import apps
from django.core.management.base import BaseCommand
from django.shortcuts import render
from django.contrib.auth.models import User
from math import ceil
from django.db.models import Sum
from ecom.views import order_price_details
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required


Payments = apps.get_model('Payments', 'Payment')
Order = apps.get_model('ecom', 'Order')
ProductDetails = apps.get_model('ecom', 'ProductDetails')
StoreCSVData = apps.get_model('ecom', 'StoreCSVData')
StoreInvoiceData = apps.get_model('ecom', 'StoreInvoiceData')
Seller = apps.get_model('ecom', 'Seller')
SellerAccount = apps.get_model('ecom', 'SellerAccount')
SellerAmount = apps.get_model('ecom', 'SellerAmount')
ReturnOrder = apps.get_model('ecom', 'ReturnOrder')
SellerOrderCancellation = apps.get_model('ecom', 'SellerOrderCancellation')


@login_required(login_url="/ecom/login/")
def get_reports_page(request):
    ctx = {}
    csv_list = []
    payment_status = ['initiated', 'success', 'failed', 'cancelled', 'cancelled_m', 'pending', 'all']
    ctx['payment_status'] = payment_status

    order_status = ['initiated', 'placed', 'placed_pending', 'accepted', 'packing', 'packed', 's_dispatched',
                    'cancelled',
                    'pending', 'shipped', 'in_transit', 'dispatched', 'delivered', 'return', 'exchange', 'undelivered',
                    'lost', 'return_cancelled', 'all']
    # csv_obj = StoreCSVData.objects.all()
    ctx['order_status'] = order_status

    product_details_status = ['SUBMITTED', 'APPROVED', 'REJECTED', 'DELETED', 'all']
    ctx['product_details_status'] = product_details_status

    csv_obj = StoreCSVData.objects.filter(model_name="Payments")
    if csv_obj:
        ctx['csv_list'] = csv_obj
    order_csv_obj = StoreCSVData.objects.filter(model_name="Order")
    if order_csv_obj:
        ctx['order_csv_list'] = order_csv_obj

    product_details_obj = StoreCSVData.objects.filter(model_name="ProductDetails")
    if product_details_obj:
        ctx['product_details_csv_list'] = product_details_obj
    return render(request, 'Ecom/reports/get_report_csv.html', ctx)


import pandas as pd


class GetCSV(BaseCommand):
    """
    https://django-postgres-copy.readthedocs.io/en/latest/
    """
    start_date = None
    end_date = None
    status = None

    def __init__(self, start_date, end_date, status, obj_id):
        self.start_date = start_date
        self.end_date = end_date
        self.status = status
        self.obj_id = obj_id

    def payment_csv(self, *args, **kwargs):
        file_name = self.status + "*" + self.start_date + " to " + self.end_date + ".csv"
        file_path = "media/csv/" + file_name

        if self.status == "all":
            generate_csv = Payments.objects.filter(create_date__range=(self.start_date, self.end_date)).to_csv(
                file_path)
        else:
            generate_csv = Payments.objects.filter(create_date__range=(self.start_date, self.end_date),
                                                   status=self.status).to_csv(
                file_path)
        if self.obj_id is None:
            create_data = {'start_date': self.start_date,
                           'end_date': self.end_date,
                           'status': self.status,
                           'file_path': file_name,
                           'model_name': "Payments"}
            store_data = StoreCSVData
            store_data.store_data_entry(self, create_data)
            # create_csv_obj = StoreCSVData.objects.create(start_date=self.start_date, end_date=self.end_date,
            #                                              status=self.status, file_path=file_name, model_name="Payments")
        else:
            actual_data = {'created_date': datetime.datetime.now()}
            store_data = StoreCSVData
            store_data.store_data_update(self, actual_data)
            # update_csv_obj = StoreCSVData.objects.filter(id=self.obj_id).update(created_date=datetime.datetime.now())
        return file_path

    def order_csv(self, *args, **kwargs):
        file_name = self.status + "*" + self.start_date + " to " + self.end_date + ".csv"
        file_path = "media/csv/" + file_name

        if self.status == "all":
            generate_csv = pd.DataFrame(list(Order.objects.filter(created_date__range=(self.start_date, self.end_date),
                                                                  status=self.status).values('order_id',
                                                                                             'product_details__product_id__name',
                                                                                             'delivery_charges__amount',
                                                                                             'unique_order_id',
                                                                                             'quantity',
                                                                                             'order_details_amount__seller_amount',
                                                                                             'convenient_fees__amount',
                                                                                             'platform_type',
                                                                                             'is_refunded', 'amount',
                                                                                             'total_amount',
                                                                                             'date', 're_status',
                                                                                             'combination_type',
                                                                                             'is_stock_return',
                                                                                             'status', 'created_date',
                                                                                             'updated_date',
                                                                                             'created_by_new',
                                                                                             'updated_by_new'))).to_csv(
                file_path)


        else:
            generate_csv = pd.DataFrame(list(Order.objects.filter(created_date__range=(self.start_date, self.end_date),
                                                                  status=self.status).values('order_id',
                                                                                             'product_details__product_id__name',
                                                                                             'delivery_charges__amount',
                                                                                             'unique_order_id',
                                                                                             'quantity',
                                                                                             'order_details_amount__seller_amount',
                                                                                             'convenient_fees__amount',
                                                                                             'platform_type',
                                                                                             'is_refunded', 'amount',
                                                                                             'total_amount',
                                                                                             'date', 're_status',
                                                                                             'combination_type',
                                                                                             'is_stock_return',
                                                                                             'status', 'created_date',
                                                                                             'updated_date',
                                                                                             'created_by_new',
                                                                                             'updated_by_new'))).to_csv(
                file_path)

        if self.obj_id is None:
            create_data = {'start_date': self.start_date,
                           'end_date': self.end_date,
                           'status': self.status,
                           'file_path': file_name,
                           'model_name': "Order"}
            store_data = StoreCSVData
            store_data.store_data_entry(self, create_data)
            # create_csv_obj = StoreCSVData.objects.create(start_date=self.start_date, end_date=self.end_date,
            # status=self.status, file_path=file_name, model_name="Payments")
        else:
            actual_data = {'created_date': datetime.datetime.now()}
            store_data = StoreCSVData
            store_data.store_data_update(self, actual_data)
            # update_csv_obj = StoreCSVData.objects.filter(id=self.obj_id).update(created_date=datetime.datetime.now())
        return file_path

    def product_details_csv(self, *args, **kwargs):
        print("heetttte")
        file_name = self.status + "*" + self.start_date + " to " + self.end_date + ".csv"
        file_path = "media/csv/" + file_name

        if self.status == "all":
            generate_csv = pd.DataFrame(
                list(ProductDetails.objects.filter(created_date__range=(self.start_date, self.end_date),
                                                   status=self.status).values('product_id',
                                                                              'unique_product_details_id',
                                                                              'seller_amount',
                                                                              'amount', 'mrp',
                                                                              'discount',
                                                                              'sku', 'HSN',
                                                                              'size_id__name', 'colour_id__name',
                                                                              'unit_count',
                                                                              'unit', 'description', 'specification',
                                                                              'status', 'height', 'Length',
                                                                              'width', 'weight', 'sgst__percentage',
                                                                              'cgst__percentage', 'date', 're_status',
                                                                              'reason', 'status', 'user__username',
                                                                              'created_date', 'updated_date',
                                                                              'created_by_new',
                                                                              'updated_by_new'))).to_csv(file_path)
        else:
            generate_csv = pd.DataFrame(
                list(ProductDetails.objects.filter(created_date__range=(self.start_date, self.end_date),
                                                   status=self.status).values('product_id',
                                                                              'unique_product_details_id',
                                                                              'seller_amount',
                                                                              'amount', 'mrp',
                                                                              'discount',
                                                                              'sku', 'HSN',
                                                                              'size_id__name', 'colour_id__name',
                                                                              'unit_count',
                                                                              'unit', 'description', 'specification',
                                                                              'status', 'height', 'Length',
                                                                              'width', 'weight', 'sgst__percentage',
                                                                              'cgst__percentage', 'date', 're_status',
                                                                              'reason', 'status', 'user__username',
                                                                              'created_date', 'updated_date',
                                                                              'created_by_new',
                                                                              'updated_by_new'))).to_csv(file_path)
        if self.obj_id is None:
            create_data = {'start_date': self.start_date,
                           'end_date': self.end_date,
                           'status': self.status,
                           'file_path': file_name,
                           'model_name': "ProductDetails"}
            store_data = StoreCSVData
            store_data.store_data_entry(self, create_data)
            # create_csv_obj = StoreCSVData.objects.create(start_date=self.start_date, end_date=self.end_date,
            #                                              status=self.status, file_path=file_name, model_name="Payments")
        else:
            actual_data = {'created_date': datetime.datetime.now()}
            store_data = StoreCSVData
            store_data.store_data_update(self, actual_data)
            # update_csv_obj = StoreCSVData.objects.filter(id=self.obj_id).update(created_date=datetime.datetime.now())
        return file_path


@csrf_exempt
def get_csv(request):
    """
    This function is used to get start date, end_date and status which will select by user.
    """
    s = e = None
    obj_id = None
    pay_obj = Payments.objects.first()
    s = str(pay_obj.create_date)[:10]
    e = str(datetime.datetime.now())[:10]
    if "start_date" in request.POST:
        if "start_date" in request.POST:
            start_date = request.POST["start_date"]
            if start_date:
                s = str(datetime.datetime.strptime(start_date, "%Y-%m-%d").date())
        if "end_date" in request.POST:
            end_date = request.POST["end_date"]
            if end_date:
                e = str(datetime.datetime.strptime(end_date, "%Y-%m-%d").date())

        status = "all"
        if "status" in request.POST:
            if request.POST["status"] and request.POST["status"] is not None:
                status = request.POST["status"]

        if "obj_id" in request.POST:
            obj_id = request.POST["obj_id"]
        obj = GetCSV(s, e, status, obj_id)
        get_file_name = obj.payment_csv()
        content = open(get_file_name).read()
        # print("content",content)
        response = HttpResponse(content, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="' + get_file_name + '"'
        return response


@csrf_exempt
def get_order_csv(request):
    """
    This function is used to get start date, end_date and status which will select by user.
    """
    s = e = None
    obj_id = None
    pay_obj = Order.objects.first()
    s = str(pay_obj.created_date)[:10]
    e = str(datetime.datetime.now())[:10]
    if "start_date" in request.POST:
        if "start_date" in request.POST:
            start_date = request.POST["start_date"]
            if start_date:
                s = str(datetime.datetime.strptime(start_date, "%Y-%m-%d").date())
        if "end_date" in request.POST:
            end_date = request.POST["end_date"]
            if end_date:
                e = str(datetime.datetime.strptime(end_date, "%Y-%m-%d").date())

        status = "all"
        if "status" in request.POST:
            if request.POST["status"] and request.POST["status"] is not None:
                status = request.POST["status"]

        if "obj_id" in request.POST:
            obj_id = request.POST["obj_id"]
        obj = GetCSV(s, e, status, obj_id)
        get_file_name = obj.order_csv()
        content = open(get_file_name).read()
        response = HttpResponse(content, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="' + get_file_name + '"'
        return response


@csrf_exempt
def get_product_details_csv(request):
    """
    This function is used to get start date, end_date and status which will select by user.
    """
    s = e = None
    obj_id = None
    pay_obj = Order.objects.first()
    s = str(pay_obj.created_date)[:10]
    e = str(datetime.datetime.now())[:10]
    if "start_date" in request.POST:
        if "start_date" in request.POST:
            start_date = request.POST["start_date"]
            if start_date:
                s = str(datetime.datetime.strptime(start_date, "%Y-%m-%d").date())
        if "end_date" in request.POST:
            end_date = request.POST["end_date"]
            if end_date:
                e = str(datetime.datetime.strptime(end_date, "%Y-%m-%d").date())

        status = "all"
        if "status" in request.POST:
            if request.POST["status"] and request.POST["status"] is not None:
                status = request.POST["status"]

        if "obj_id" in request.POST:
            obj_id = request.POST["obj_id"]
        obj = GetCSV(s, e, status, obj_id)
        get_file_name = obj.product_details_csv()
        content = open(get_file_name).read()
        response = HttpResponse(content, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="' + get_file_name + '"'
        return response


@login_required(login_url="/ecom/login/")
def get_invoice_page(request):
    ctx = {}

    seller_obj = User.objects.filter(groups__name="Seller")
    if seller_obj:
        ctx['sellers'] = seller_obj

    invoice_obj = StoreInvoiceData.objects.filter(model_name="SellerInvoice")

    if invoice_obj:
        ctx['invoice_list'] = invoice_obj
    return render(request, 'Ecom/reports/get_seller_invoice.html', ctx)


class GetInvoice(BaseCommand):
    """
    https://django-postgres-copy.readthedocs.io/en/latest/
    """
    start_date = None
    end_date = None
    status = None

    def __init__(self, start_date, end_date, seller, obj_id, pdf_name):
        self.start_date = start_date
        self.end_date = end_date
        self.seller = seller
        self.obj_id = obj_id
        self.pdf_name = pdf_name

    def seller_amount_invoice(self, *args, **kwargs):
        if self.obj_id is None:
            create_data = {'start_date': self.start_date,
                           'end_date': self.end_date,
                           'seller': self.seller,
                           'file_path': self.pdf_name,
                           'model_name': "SellerInvoice"}
            store_data = StoreInvoiceData
            store_data.store_data_entry(self, create_data)
        else:
            actual_data = {'created_date': datetime.datetime.now()}
            store_data = StoreInvoiceData
            store_data.store_data_update(self, actual_data)
        return self.pdf_name


@csrf_exempt
def seller_amount_invoice_new(request):
    template_name = "Ecom/reports/seller_pdf_invoice.html"
    s = e = None
    obj_id = None
    total_amount_list = []
    amount_to_pay_list = []
    seller_amount_obj = SellerAmount.objects.first()
    s = str(seller_amount_obj.created_date)[:10]
    e = str(datetime.datetime.now())[:10]
    if "start_date" in request.POST:
        if "start_date" in request.POST:
            start_date = request.POST["start_date"]
            if start_date:
                s = str(datetime.datetime.strptime(start_date, "%Y-%m-%d").date())
        if "end_date" in request.POST:
            end_date = request.POST["end_date"]
            if end_date:
                e = str(datetime.datetime.strptime(end_date, "%Y-%m-%d").date())
        seller = "all"
        if "Seller" in request.POST:
            if request.POST["Seller"] and request.POST["Seller"] is not None:
                seller = request.POST["Seller"]

        if "obj_id" in request.POST:
            obj_id = request.POST["obj_id"]

        ctx = {}

        total_amount = 0

        amount_to_pay = 0
        cancel_final_amount = 0
        return_final_amount = 0
        undelivered_final_amount = 0
        amount_to_pay_dict = {}
        payment={}
        payments_details_dict={}

        seller_data = Seller.objects.filter(user__username=seller)

        if seller_data:
            ctx['seller_name'] = seller_data[0].name
            ctx['gst_no'] = seller_data[0].gst_no
            ctx['address'] = seller_data[0].main_address
        # check seller amount entry for particular seller and date with is_full_amount_paid
        seller_amount_obj = SellerAmount.objects.filter(seller__username=seller,
                                                        created_date__range=(start_date, end_date)).distinct()
        if seller_amount_obj:
            ctx['seller_amount'] = seller_amount_obj
            invoice_dict = {}
            final_return_data_dict ={}
            final_amount_dict = {}
            final_return_dict = {}
            final_cancel_data_dict = {}
            final_cancel_dict = {}
            final_undelivered_data_dict ={}
            final_undelivered_dict ={}
            for seller_amount_obj in seller_amount_obj:
                # get all orders attahced to seller_amount
                order_list = seller_amount_obj.order.all()
                final_amount = 0
                data_dict = {}
                return_data_dict ={}
                cancel_data_dict = {}
                undelivered_data_dict = {}
                if order_list:
                    # get all orders connected to seller amount
                    orders = Order.objects.filter(id__in=order_list)

                    if orders:
                        for order in orders:
                            # get seller_amount and quantity
                            sgst = 0
                            cgst = 0
                            seller_amount = 0
                            amount = 0
                            user_amount = 0
                            # return seller amount, sgst, cgst
                            get_data = order_price_details(order.id)
                            # print("get_data")
                            if get_data:
                                seller_amount = get_data['seller_amount']
                                sgst = get_data['sgst']
                                cgst = get_data['cgst']
                            # total sgst and cgst
                            total_tax = sgst + cgst
                            if total_tax != 0:
                                # quantity and seller_amount
                                amount = order.quantity * (float(seller_amount))
                                # tax in rupees
                                user_amount = (float(amount) * total_tax) / 100
                                # calulate tax and amount
                                total_amount = ceil(float(amount) + user_amount)
                            else:
                                total_amount += order.quantity * seller_amount
                            # calculate final amount
                            final_amount += total_amount
                            data_dict.update(
                                {order: [order.quantity, seller_amount, amount, user_amount, total_amount]})
                        invoice_dict.update({seller_amount_obj.invoice_id: data_dict})
                        final_amount_dict.update({seller_amount_obj.invoice_id: float(final_amount)})
                ctx['invoice'] = invoice_dict
                ctx['final_amount'] = final_amount_dict

                # return_orders
                return_orders = seller_amount_obj.return_orders.all()
                if return_orders:
                    return_orders_obj = ReturnOrder.objects.filter(id__in=return_orders).values_list('order', flat=True)
                    # get all orders connected to seller amount
                    orders = Order.objects.filter(id__in=return_orders_obj)
                    if orders:
                        for order in orders:
                            # get seller_amount and quantity
                            return_sgst = 0
                            return_cgst = 0
                            return_seller_amount = 0
                            return_amount = 0
                            return_user_amount = 0
                            # return seller amount, sgst, cgst````
                            get_data = order_price_details(order.id)
                            if get_data:
                                return_seller_amount = get_data['seller_amount']
                                return_sgst = get_data['sgst']
                                return_cgst = get_data['cgst']
                            # total sgst and cgst
                            return_total_tax = return_sgst + return_cgst
                            if return_total_tax != 0:
                                # quantity and seller_amount
                                return_amount = order.quantity * (float(return_seller_amount))
                                # tax in rupees
                                return_user_amount = (float(return_amount) * return_total_tax) / 100
                                # calulate tax and amount
                                return_total_amount = ceil(float(return_amount) + return_user_amount)
                            else:
                                return_total_amount += order.quantity * return_seller_amount
                            # calculate final amount
                            return_final_amount += return_total_amount
                            return_data_dict.update({order: [order.quantity, return_seller_amount, return_amount,
                                                             return_user_amount, return_total_amount]})
                        final_return_data_dict.update({seller_amount_obj.invoice_id:return_data_dict})
                        final_return_dict.update({seller_amount_obj.invoice_id: float(final_amount)})
                ctx['return_invoice'] = final_return_data_dict
                ctx['return_final_amount'] = final_return_dict

                # cancelled_orders
                cancelled_orders = seller_amount_obj.cancelled_orders.all()
                if cancelled_orders:
                    seller_order_cancellation = SellerOrderCancellation.objects.filter(
                        id__in=cancelled_orders).values_list('order', flat=True)
                    # get all orders connected to seller amount
                    orders = Order.objects.filter(id__in=seller_order_cancellation)
                    if orders:
                        for order in orders:
                            # get seller_amount and quantity
                            cancel_sgst = 0
                            cancel_cgst = 0
                            cancel_seller_amount = 0
                            cancel_amount = 0
                            cancel_user_amount = 0
                            # return seller amount, sgst, cgst
                            get_data = order_price_details(order.id)
                            if get_data:
                                cancel_seller_amount = get_data['seller_amount']
                                cancel_sgst = get_data['sgst']
                                cancel_cgst = get_data['cgst']
                            # total sgst and cgst
                            cancel_total_tax = cancel_sgst + cancel_cgst
                            if cancel_total_tax != 0:
                                # quantity and seller_amount
                                cancel_amount = order.quantity * (float(cancel_seller_amount))
                                # tax in rupees
                                cancel_user_amount = (float(cancel_amount) * cancel_total_tax) / 100
                                # calulate tax and amount
                                cancel_total_amount = ceil(float(cancel_amount) + cancel_user_amount)
                            else:
                                cancel_total_amount += order.quantity * cancel_seller_amount
                            # calculate final amount
                            cancel_final_amount += cancel_total_amount
                            cancel_data_dict.update({order: [order.quantity, cancel_seller_amount, cancel_amount,
                                                             cancel_user_amount, cancel_total_amount]})
                        final_cancel_data_dict.update({seller_amount_obj.invoice_id: cancel_data_dict})
                        final_cancel_dict.update({seller_amount_obj.invoice_id: float(cancel_final_amount)})
                    ctx['cancel_invoice'] = final_cancel_data_dict
                    ctx['cancel_final_amount'] = final_cancel_dict

                    # ctx['cancel_invoice'] = cancel_data_dict
                    #     ctx['cancel_final_amount'] = float(cancel_final_amount)

                # undelivered_orders
                undelivered_orders = seller_amount_obj.undelivered_orders.all()
                if undelivered_orders:
                    # get all orders connected to seller amount
                    orders = Order.objects.filter(id__in=undelivered_orders)
                    if orders:
                        for order in orders:

                            # get seller_amount and quantity
                            undelivered_sgst = 0
                            undelivered_cgst = 0
                            undelivered_seller_amount = 0
                            undelivered_amount = 0
                            undelivered_user_amount = 0
                            # return seller amount, sgst, cgst
                            get_data = order_price_details(order.id)
                            if get_data:
                                undelivered_seller_amount = get_data['seller_amount']
                                undelivered_sgst = get_data['sgst']
                                undelivered_cgst = get_data['cgst']
                            # total sgst and cgst
                            undelivered_total_tax = undelivered_sgst + undelivered_sgst
                            if undelivered_total_tax != 0:
                                # quantity and seller_amount
                                undelivered_amount = order.quantity * (float(undelivered_seller_amount))
                                # tax in rupees
                                undelivered_user_amount = (float(undelivered_amount) * undelivered_total_tax) / 100
                                # calulate tax and amount
                                undelivered_total_amount = ceil(float(undelivered_amount) + undelivered_user_amount)
                            else:
                                undelivered_total_amount += order.quantity * undelivered_seller_amount
                            # calculate final amount
                            undelivered_final_amount += undelivered_total_amount
                            undelivered_data_dict.update(
                                {order: [order.quantity, undelivered_seller_amount, undelivered_amount,
                                         undelivered_user_amount, undelivered_total_amount]})
                        final_undelivered_data_dict.update({seller_amount_obj.invoice_id: undelivered_data_dict})
                        final_undelivered_dict.update({seller_amount_obj.invoice_id: float(undelivered_final_amount)})
                    ctx['undelivered_invoice'] = final_undelivered_data_dict
                    ctx['undelivered_final_amount'] = final_undelivered_dict
                        # ctx['undelivered_invoice'] = undelivered_data_dict
                        # ctx['undelivered_final_amount'] = float(undelivered_final_amount)

                # get paid data from seller_payment
                # get all paid payment sum
                payments = 0
                payments_details = None
                total_paid = seller_amount_obj.seller_payment.filter(status='success').aggregate(Sum('amount'))[
                    'amount__sum']

                if total_paid:
                    payments = total_paid
                    payments_details = seller_amount_obj.seller_payment.filter(status='success')
                else:
                    payments = 0
                    payments_details = None
                payment.update({seller_amount_obj.invoice_id: payments})
                payments_details_dict.update({seller_amount_obj.invoice_id: payments_details})
                # ctx['payment'] = payments
                # ctx['payment_details'] = payments_details
                amount_to_pay = 0
                # get amount paid till date
                amount_to_pay = float(final_amount) - float(payments)
                # amount_to_pay_list.append(amount_to_pay)
                # amount_to_pay.update({})

                # ctx['amount_to_pay'] = amount_to_pay
                amount_to_pay_dict.update({seller_amount_obj.invoice_id: amount_to_pay})
            ctx['payment'] = payment
            ctx['payment_details'] = payments_details_dict
            ctx['amount_to_pay'] = amount_to_pay_dict

            from django.template import loader
            html = loader.render_to_string(template_name, ctx)
            import os
            path = "media/invoice"
            isdir = os.path.isdir(path)
            if isdir == True:
                pass
            if isdir == False:
                os.makedirs(path)
            pdf_name = seller + "*" + start_date + " to " + end_date + ".pdf"
            import pdfkit
            from django.template import loader
            template = loader.render_to_string(template_name, ctx)
            # from django.template.loader import get_template
            # template = get_template(template_name).render(ctx)
            base = "media/invoice" + "/" + str(pdf_name)
            pdfkit.from_string(template, base)
            obj = GetInvoice(s, e, seller, obj_id, pdf_name)
            get_file_name = obj.seller_amount_invoice()
            test_file = open(base, 'rb')

            response = HttpResponse(content=test_file)
            response['Content-Type'] = 'application/pdf'
            response['Content-Disposition'] = 'attachment; filename=' + base
            month = datetime.datetime.now().month
            year = datetime.datetime.now().year
            # upload files on s3
            date_path = str(year) + "/" + str(month) + "/" + pdf_name
            upload_to_path = "ecom/seller_invoice/" + date_path

            from MApi.botofile import upload_image
            upload_image(path, upload_to_path)
            return response
        else:
            return HttpResponse("No Data found..")