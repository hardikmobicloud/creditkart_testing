import os
import sys

# Initializing The scheduler
# Note: Do Not Remove this
from EApi.logistics import fetch_logistics_status, fetch_logistics_status_2
from apscheduler.schedulers.background import BackgroundScheduler
from ecom.views import calculate_seller_orders, due_sms, create_dashboard_data
from ecom.dashboard import save_overdue_on_date
from LoanAppData.loan_funcations import loan_amount_deduct_from_wallet_scheduler


# try:
#     fetch_log_status = scheduler.add_job(fetch_logistics_status, 'interval', hours=3)
#     print("fetch_log_status", fetch_log_status)
# except Exception as e:
#     print('---------Scheduler Exception in fetch_log_status----------')
#     print(e.args)
#     exc_type, exc_obj, exc_tb = sys.exc_info()
#     fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
#     print(exc_type, fname, exc_tb.tb_lineno)
#     pass

# try:
#     fetch_log_status2 = scheduler.add_job(fetch_logistics_status_2, 'cron', day='1-31', hour='02', minute='30')
#     print("fetch_log_status2", fetch_log_status2)
# except Exception as e:
#     print('---------Scheduler Exception in fetch_log_status2----------')
#     print(e.args)
#     exc_type, exc_obj, exc_tb = sys.exc_info()
#     fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
#     print(exc_type, fname, exc_tb.tb_lineno)
#     pass

'''
scheduler = BackgroundScheduler()


try:
    calculate_seller_orders_sch = scheduler.add_job(calculate_seller_orders, 'cron', day='1-31', hour='03', minute='00')
    print("calculate_seller_orders_sch", calculate_seller_orders_sch)
except Exception as e:
    print('---------Scheduler Exception in calculate_seller_orders----------')
    print(e.args)
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print(exc_type, fname, exc_tb.tb_lineno)
    pass

try:
    due_sms_sch = scheduler.add_job(due_sms, 'interval', hours=1)
    print("due_sms_sch", due_sms_sch)
except Exception as e:
    print('---------Scheduler Exception in due_sms_sch----------')
    print(e.args)
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print(exc_type, fname, exc_tb.tb_lineno)
    pass


try:
    due_sms_sch = scheduler.add_job(loan_amount_deduct_from_wallet_scheduler, 'interval', hours=6)
    print("loan_amount_deduct_from_wallet_scheduler", loan_amount_deduct_from_wallet_scheduler)
except Exception as e:
    print('---------Scheduler Exception in loan_amount_deduct_from_wallet_scheduler----------')
    print(e.args)
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print(exc_type, fname, exc_tb.tb_lineno)
    pass


try:
    create_dashboard_data_sch = scheduler.add_job(create_dashboard_data, 'cron', day='1-31', hour='0', minute='00')
    create_dashboard_data_sch2 = scheduler.add_job(create_dashboard_data, 'cron', day='1-31', hour='12', minute='01')
    print("create_dashboard_data_sch", create_dashboard_data_sch)
    print("create_dashboard_data_sch2", create_dashboard_data_sch2)
except Exception as e:
    print('---------Scheduler Exception in create_dashboard_data_sch----------')
    print(e.args)
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print(exc_type, fname, exc_tb.tb_lineno)
    pass
    
    
try:
    save_overdue_on_date_sch = scheduler.add_job(save_overdue_on_date, 'cron', day='1-31', hour='03', minute='30')
    print("save_overdue_on_date_sch", save_overdue_on_date_sch)
except Exception as e:
    print('---------Scheduler Exception in save_overdue_on_date----------')
    print(e.args)
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print(exc_type, fname, exc_tb.tb_lineno)
    pass

# Note: Do Not Remove this
scheduler.start()

'''

def eschedular_test(request):
    from django.http import HttpResponse
    return HttpResponse("working")
