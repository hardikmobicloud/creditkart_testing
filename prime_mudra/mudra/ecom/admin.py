from django.apps import apps
from django.contrib import admin

from .models import *

OrderDetails_order_id = apps.get_model('ecom', 'OrderDetails_order_id')
OrderDetails_combine_order = apps.get_model('ecom', 'OrderDetails_combine_order')
Images_product_details = apps.get_model('ecom', 'Images_product_details')


class CategoryAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'description', 'status', 'priority', 'created_date', 'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ('status', 'priority')
    search_fields = ['name']


admin.site.register(Category, CategoryAdmin)


class SubCategoryAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'category', 'description', 'status', 'created_date', 'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['status', 'category']
    search_fields = ['name', 'status', 'category__name']


admin.site.register(SubCategory, SubCategoryAdmin)


class BaseCategoryAdmin(admin.ModelAdmin):
    list_display = ('base_name', 'sub_category', 'created_date', 'updated_date', 'created_by_new', 'updated_by_new')
    search_fields = ['base_name', 'sub_category__name']


admin.site.register(BaseCategory, BaseCategoryAdmin)


class CGSTAdmin(admin.ModelAdmin):
    list_display = (
        'percentage', 'description', 'status', 'created_date', 'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['status']
    search_fields = ['percentage']


admin.site.register(CGST, CGSTAdmin)


class SGSTAdmin(admin.ModelAdmin):
    list_display = (
        'percentage', 'description', 'status', 'created_date', 'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['status', 'percentage', ]
    search_fields = ['percentage']


admin.site.register(SGST, SGSTAdmin)


class SizeAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'status', 'created_date', 'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['status']
    search_fields = ['name']


admin.site.register(Size, SizeAdmin)


class ColourAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'status', 'created_date', 'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['status']
    search_fields = ['name']


admin.site.register(Colour, ColourAdmin)


class ProductAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'product_id', 'base_category', 're_status', 'date', 'description', 'priority', 'dashboard_status',
        'seller', 'created_date', 'created_by_new', 'updated_by_new')

    list_filter = ['dashboard_status', 'base_category', 're_status', 'priority', 'seller', ]
    search_fields = ['name', 'product_id', 'seller__username']


admin.site.register(Product, ProductAdmin)


class ProductDetailsAdmin(admin.ModelAdmin):
    list_display = (
        'product_id', 'seller_amount', 'amount', 'mrp', 'discount', 'sku', 'HSN', 'unique_product_details_id',
        'size_id',
        'colour_id', 'description', 'unit_count', 'unit',
        'height', 'Length', 'width', 'weight', 'sgst', 'cgst', 'date', 're_status',
        'reason', 'status', 'user', 'created_date', 'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['status', 'size_id', 'colour_id', 're_status']
    search_fields = ['product_id__name', 'user__username', 'unique_product_details_id']


admin.site.register(ProductDetails, ProductDetailsAdmin)


class StockAdmin(admin.ModelAdmin):
    list_display = (
        'product_details', 'stock_add', 'stock_sub', 'stock_balance', 'status', 'created_date', 'updated_date')
    list_filter = ['status', 'product_details', 'stock_balance', 'created_by_new', 'updated_by_new']
    search_fields = ['product_details__product_id__name']


admin.site.register(Stock, StockAdmin)


class CartAdmin(admin.ModelAdmin):
    list_display = (
        'product_details', 'quantity', 'amount', 'total_amount', 'date', 'status', 'user', 'created_date',
        'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['status', 'user', ]
    search_fields = ['product_details__product_id__name']


admin.site.register(Cart, CartAdmin)


class CreditAdmin(admin.ModelAdmin):
    list_display = (
        'credit', 'used_credit', 'balance_credit', 'actual_used', 'repayment_add', 'status', 'user', 'created_date',
        'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['status']
    search_fields = ['user__username']


admin.site.register(Credit, CreditAdmin)


class DeliveryChargesAdmin(admin.ModelAdmin):
    list_display = (
        'product', 'amount', 're_status', 'status', 'created_date', 'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['status', 're_status', 'product', ]
    search_fields = ['product__name']


admin.site.register(DeliveryCharges, DeliveryChargesAdmin)


class ProductDetailsAmountAdmin(admin.ModelAdmin):
    list_display = (
        'product_details', 'seller_amount', 'admin_amount', 'mrp', 'discount', 'date', 'sgst', 'cgst', 'created_date',
        'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['sgst', 'cgst', 'product_details', ]
    search_fields = ['product_details__product_id__name']


admin.site.register(ProductDetailsAmount, ProductDetailsAmountAdmin)


class OrderAdmin(admin.ModelAdmin):
    list_display = (
        'order_id', 'product_details', 'unique_order_id', 'seller', 'customer', 'delivery_charges', 'convenient_fees',
        'quantity', 'amount', 'total_amount', 'date', 'combination_type', 'platform_type', 'is_refunded',
        're_status', 'status', 'created_date', 'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['status', 're_status', ]
    search_fields = ['order_id', 'unique_order_id', 'product_details__user__username',
                     'product_details__product_id__name']

    def seller(self, obj):
        return obj.product_details.user

    def customer(self, obj):
        username = None
        if obj is not None:
            order_details_obj = OrderDetails.objects.filter(order_id=obj)
            if order_details_obj:
                username = order_details_obj[0].user.username
        return username


admin.site.register(Order, OrderAdmin)


class OrderDetailsAdmin(admin.ModelAdmin):
    list_display = ('order_text', 'credit', 'credit_status', 'pay_amount', 'total_quantity',
                    'total_amount', 'wallet_type', 're_status', 'date', 'convenient_fees',
                    'status', 'user', 'address', 'created_date', 'updated_date')
    list_filter = ('status', 'credit_status', 'wallet_type')

    search_fields = ['order_text', 'user__username', 'order_id__unique_order_id']


admin.site.register(OrderDetails, OrderDetailsAdmin)


class SellerAdmin(admin.ModelAdmin):
    list_display = ('name', 'main_address', 'address', 'mobile_no', 'landline_no', 'type',
                    'description', 'gst_no', 'pan_no', 'shop_act_licence', 'pan_image',
                    'licence_image', 'shop_image', 'user', 'created_date', 'updated_date', 'created_by_new',
                    'updated_by_new')
    list_filter = ['type', 'user', 'name', ]
    search_fields = ['name', 'user__username', 'mobile_no']


admin.site.register(Seller, SellerAdmin)


class ImagesAdmin(admin.ModelAdmin):
    list_display = (
        'product', 'path', 'status', 'date', 'type', 'created_date', 'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['type', 'status', ]
    search_fields = ['product_id__name']


admin.site.register(Images, ImagesAdmin)


class EcomDocumentsAdmin(admin.ModelAdmin):
    list_display = (
        'type', 'path', 'verify_date', 'verify_by', 'is_verified', 'created_date', 'updated_date', 'user',
        'created_by_new',
        'updated_by_new')
    list_filter = ['type', 'user']
    search_fields = ['user__username']


admin.site.register(EcomDocuments, EcomDocumentsAdmin)


class OrderLocationAdmin(admin.ModelAdmin):
    list_display = ('address', 'city', 'taluka', 'state', 'pin_code', 'remark', 'date', 'created_date', 'updated_date',
                    'created_by_new', 'updated_by_new')
    list_filter = ('state', 'taluka', 'pin_code', 'city',)
    search_fields = ['address', 'city', 'pin_code']


admin.site.register(OrderLocation, OrderLocationAdmin)


class OrderStatusAdmin(admin.ModelAdmin):
    list_display = ('order', 'status', 'date', 'created_date', 'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['status']
    search_fields = ['order__order_id', 'order__unique_order_id']


admin.site.register(OrderStatus, OrderStatusAdmin)


class OrderDetailsStatusAdmin(admin.ModelAdmin):
    list_display = (
        'order_details', 'status', 'date', 'created_date', 'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['status']
    search_fields = ['order_details__order_text']


admin.site.register(OrderDetailsStatus, OrderDetailsStatusAdmin)


class ReturnOrderAdmin(admin.ModelAdmin):
    list_display = (
        'return_id', 'order', 'order_in_exchange', 'order_details', 'return_reason', 'verify_return_status',
        'admin_verified_date', 'verified_by', 'pickup_date', 'logistic_status', 'logistic_reason',
        'logistic_verify_by', 'product_verify_date', 'seller_verify_by',
        'seller_status', 'seller_reason', 'payment_status', 'type',
        'created_date', 'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['verify_return_status', 'seller_status', 'logistic_status']
    search_fields = ['return_id', 'order__order_id', 'order_details__order_text', 'order__unique_order_id']


admin.site.register(ReturnOrder, ReturnOrderAdmin)


class SellerAmountAdmin(admin.ModelAdmin):
    list_display = (
        'seller', 'invoice_id', 'total_amount', 'return_amount', 'cancel_amount', 'manually_cancelled_amount',
        'undelivered_amount', 'type', 'is_full_amount_paid', 'date', 'created_date', 'updated_date', 'created_by_new',
        'updated_by_new')
    list_filter = ('type', 'is_full_amount_paid')
    search_fields = ['invoice_id', 'order__order_id', 'seller__username', 'order__unique_order_id']


admin.site.register(SellerAmount, SellerAmountAdmin)


class SellerAccountAdmin(admin.ModelAdmin):
    list_display = ('total_amount', 'return_amount', 'exchange_amount', 'amount_to_pay', 'amount_paid',
                    'verify_date', 'seller', 'verify_by', 'seller_payment',
                    'created_date', 'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['verify_by', ]
    search_fields = ['seller__username', ]


admin.site.register(SellerAccount, SellerAccountAdmin)


class LogisticOrderAdmin(admin.ModelAdmin):
    list_display = ('order', 'waybill_no', 'refnum', 'remark', 'logistics', 'status',
                    'order_data_text', 'created_date', 'updated_date', 'order_mode', 'type', 'created_by_new',
                    'updated_by_new')
    list_filter = ['order_mode', 'status', 'logistics', 'type']
    search_fields = ['order__order_id', 'order__unique_order_id', 'order_mode', 'waybill_no']


admin.site.register(LogisticOrder, LogisticOrderAdmin)


class SellerWarehouseAdmin(admin.ModelAdmin):
    list_display = ('seller', 'company_name', 'primary_address', 'secondary_address',
                    'mobile_no', 'pin_code', 'city_id', 'state_id', 'country_id', 'warehouse_id',
                    'approval_status', 'type', 'created_date', 'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['approval_status', 'type']
    search_fields = ['seller__username', 'mobile_no', 'pin_code', 'city_id', 'warehouse_id']


admin.site.register(SellerWarehouse, SellerWarehouseAdmin)


class ConvenientFeesAdmin(admin.ModelAdmin):
    list_display = (
        'amount', 'status', 'order_type', 're_status', 'created_date', 'updated_date', 'created_by_new',
        'updated_by_new')
    list_filter = ['status', 'order_type']
    search_fields = ['amount', 'order_type']


admin.site.register(ConvenientFees, ConvenientFeesAdmin)


class UserWalletAdmin(admin.ModelAdmin):
    list_display = (
        'amount_add', 'used_amount', 'balance_amount', 'payment', 'order_details', 'user', 'status', 'auto_adjust',
        'created_date', 'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['status']
    search_fields = ['user__username']


admin.site.register(UserWallet, UserWalletAdmin)


class LogisticsDeliveryChargesAdmin(admin.ModelAdmin):
    list_display = (
        'gram', 'amount', 'percentage', 'percentage_amount', 'tax', 'delivery_amount', 'created_by_new',
        'updated_by_new')
    search_fields = ['tax', 'delivery_amount']


admin.site.register(LogisticsDeliveryCharges, LogisticsDeliveryChargesAdmin)


class SellerPaymentAdmin(admin.ModelAdmin):
    list_display = ('order_id', 'transaction_id', 'pg_transaction_id', 'status', 'category', 'mode', 'type',
                    'response', 'amount', 'pay_source', 'return_status', 'response_code', 'product_type', 'create_date',
                    'update_date', 'created_by_new', 'updated_by_new')
    list_filter = ('status', 'category', 'mode', 'type', 'pay_source', 'product_type')
    search_fields = ['order_id', 'transaction_id', 'pg_transaction_id', 'return_status']


admin.site.register(SellerPayment, SellerPaymentAdmin)


class ProductMinMaxAMountAdmin(admin.ModelAdmin):
    list_display = ('product', 'min_amount', 'max_amount', 're_status', 'created_by_new', 'updated_by_new')
    list_filter = ['re_status']
    search_fields = ['product__name', 're_status']


admin.site.register(ProductMinMaxAMount, ProductMinMaxAMountAdmin)


class ProductMinMaxDiscountAdmin(admin.ModelAdmin):
    list_display = ('product', 'min_discount', 'max_discount', 're_status', 'created_by_new', 'updated_by_new')
    list_filter = ['re_status']
    search_fields = ['product__name', 're_status']


admin.site.register(ProductMinMaxDiscount, ProductMinMaxDiscountAdmin)


class DefaultWalletAmountAdmin(admin.ModelAdmin):
    list_display = ('mobile_no', 'user', 'amount', 'type', 'created_by_new', 'updated_by_new')
    list_filter = ['type']
    search_fields = ['mobile_no', 'amount']


admin.site.register(DefaultWalletAmount, DefaultWalletAmountAdmin)


class SellerOrderCancellationAdmin(admin.ModelAdmin):
    list_display = ('order', 'amount', 'cancel_by', 'status', 'created_by_new', 'updated_by_new')
    list_filter = ['cancel_by', 'status', ]
    search_fields = ['order__unique_order_id']


admin.site.register(SellerOrderCancellation, SellerOrderCancellationAdmin)


class ShiprocketTokenAdmin(admin.ModelAdmin):
    list_display = (
        'token', 'first_name', 'status', 'date', 'created_date', 'expiry_date', 'created_by_new', 'updated_by_new')
    list_filter = ['status']
    search_fields = ['first_name']


admin.site.register(ShiprocketToken, ShiprocketTokenAdmin)


class ShiprocketOrderAdmin(admin.ModelAdmin):
    list_display = ['shipment_id', 'shiprocket_order_id', 'order', 'order_details', 'awb_code', 'courier_name',
                    'sr_status', 'created_date', 'created_by_new', 'updated_by_new']
    list_filter = ['status', 'sr_status']
    search_fields = ['order__unique_order_id', 'order__order_id', 'order_details__order_text', 'shipment_id',
                     'shiprocket_order_id']


class ShiprocketShipmentPickupAdmin(admin.ModelAdmin):
    list_display = ['pickup_token_number', 'sr_status', 'pickup_status', 'scheduled_date', 'sr_order', 'created_date',
                    'created_by_new', 'updated_by_new']
    list_filter = ['status', 'sr_status']
    search_fields = ['pickup_token_number', ]


admin.site.register(ShiprocketOrder, ShiprocketOrderAdmin)
admin.site.register(ShiprocketShipmentPickup, ShiprocketShipmentPickupAdmin)


class SellerInvoiceClearAmountAdmin(admin.ModelAdmin):
    list_display = ('seller', 'amount', 'created_date', 'updated_date', 'status', 'created_by_new', 'updated_by_new')
    list_filter = ['seller', 'status', ]
    search_fields = ['seller__username', 'status']


admin.site.register(SellerInvoiceClearAmount, SellerInvoiceClearAmountAdmin)


class SellerWalletAdmin(admin.ModelAdmin):
    list_display = (
        'seller', 'amount_add', 'amount_sub', 'forwarded_amount', 'balance_amount', 'actual_invoice_amount',
        'actual_paid',
        'is_full_amount_paid', 'full_amount_paid_date', 'is_wallet_consumed', 'payment_status', 'seller',
        'created_by_new', 'updated_by_new')
    list_filter = ('is_full_amount_paid', 'is_wallet_consumed', 'payment_status',)
    search_fields = ['seller__username']


admin.site.register(SellerWallet, SellerWalletAdmin)


class ProductPriorityAdmin(admin.ModelAdmin):
    list_display = (
        'product', 'dashboard_status', 'priority', 'view_type', 'type', 'text_view', 'created_date', 'created_by_new',
        'updated_by_new')
    list_filter = ('type', 'dashboard_status', 'priority', 'view_type')
    search_fields = ['product', 'view_type', 'type']


admin.site.register(ProductPriority, ProductPriorityAdmin)


class BannerAdmin(admin.ModelAdmin):
    list_display = (
        'product', 'path', 'priority', 'status', 'type', 'text_view', 'date', 'created_date', 'created_by_new',
        'updated_by_new')
    list_filter = ('type', 'status', 'priority')
    search_fields = ['product']


admin.site.register(Banner, BannerAdmin)


class ShiprocketManifestAdmin(admin.ModelAdmin):
    list_display = (
        'sr_order', 'manifest_url', 'response', 'status', 'created_date', 'created_by_new', 'updated_by_new')
    list_filter = ('status',)
    search_fields = ['sr_order__shipment_id', ]


admin.site.register(ShiprocketManifest, ShiprocketManifestAdmin)


class ShiprocketLabelAdmin(admin.ModelAdmin):
    list_display = ('sr_order', 'label_url', 'response', 'status', 'created_date', 'created_by_new', 'updated_by_new')
    list_filter = ('status',)
    search_fields = ['sr_order__shipment_id', ]


admin.site.register(ShiprocketLabel, ShiprocketLabelAdmin)


class ShiprocketApiInfoAdmin(admin.ModelAdmin):
    list_display = ('order', 'order_details', 'create_order', 'invoice_generate', 'awb_generate', 'manifest_generate',
                    'pickup_schedule',
                    'label_generate', 'type', 'response', 'status', 'created_date', 'created_by_new', 'updated_by_new')
    list_filter = ('create_order', 'invoice_generate', 'awb_generate', 'manifest_generate', 'pickup_schedule',
                   'label_generate', 'type')
    search_fields = ['order__unique_order_id', 'order_details__order_text']


admin.site.register(ShiprocketApiInfo, ShiprocketApiInfoAdmin)


class OfferBannerAdmin(admin.ModelAdmin):
    list_display = (
        'path', 'start_date', 'end_date', 'type', 'priority', 'status', 'created_date', 'created_by_new',
        'updated_by_new')
    list_filter = ('type', 'status', 'priority')


admin.site.register(OfferBanner, OfferBannerAdmin)


class LoanDefaulterAdmin(admin.ModelAdmin):
    list_display = ('mobile_number', 'is_defaulter', 'type', 'status', 'created_date', 'updated_date', 'created_by_new',
                    'updated_by_new')
    list_filter = ('mobile_number', 'type', 'is_defaulter', 'status', 'created_date',)
    search_fields = ['mobile_number', ]


admin.site.register(LoanDefaulter, LoanDefaulterAdmin)


class OfferAdmin(admin.ModelAdmin):
    list_display = ('title', 'offer_id', 'status', 'path', 'type', 'priority', 'created_by_new', 'updated_by_new')
    list_filter = ('status', 'type')


admin.site.register(Offer, OfferAdmin)


class UserOfferAdmin(admin.ModelAdmin):
    list_display = ('user', 'offer', 'is_read', 'status', 'created_by_new', 'updated_by_new')
    list_filter = ('is_read', 'status')
    search_fields = ['user__username', 'offer__offer_id']


admin.site.register(UserOffer, UserOfferAdmin)


class CouponAdmin(admin.ModelAdmin):
    list_display = ('order_details', 'coupon', 'status', 'created_by_new', 'updated_by_new')
    list_filter = ['status']
    search_fields = ['order_details__order_text', 'coupon']


admin.site.register(Coupon, CouponAdmin)


class NewsLetterAdmin(admin.ModelAdmin):
    list_display = ('email_id', 'created_date', 'created_by_new', 'updated_by_new')
    search_fields = ['email_id']


admin.site.register(NewsLetter, NewsLetterAdmin)


class ProductRatingAdmin(admin.ModelAdmin):
    list_display = ('product', 'rating', 'created_by_new', 'updated_by_new')
    search_fields = ['product']


admin.site.register(ProductRating, ProductRatingAdmin)


class WalletRechargeAdmin(admin.ModelAdmin):
    list_display = ('amount', 'date', 'created_by_new', 'updated_by_new')
    search_fields = ['date']


admin.site.register(WalletRecharge, WalletRechargeAdmin)


class UnitAdmin(admin.ModelAdmin):
    list_display = ('name', 're_status', 'created_date', 'created_by_new', 'updated_by_new')
    list_filter = ['re_status']
    search_fields = ['name']


admin.site.register(Unit, UnitAdmin)


class OverDueDataAdmin(admin.ModelAdmin):
    list_display = ('amount', 'date', 'created_date', 'updated_date', 'created_by_new', 'updated_by_new')
    list_filter = ['re_status']


admin.site.register(OverDueData, OverDueDataAdmin)


class Images_product_detailsAdmin(admin.ModelAdmin):
    list_display = ['images', 'productdetails']
    search_fields = ['productdetails__product_id__name']


admin.site.register(Images_product_details, Images_product_detailsAdmin)


class OrderDetails_order_idAdmin(admin.ModelAdmin):
    list_display = ['orderdetails', 'order_id']
    search_fields = ['order_id__unique_order_id', 'orderdetails__order_text']


admin.site.register(OrderDetails_order_id, OrderDetails_order_idAdmin)


class OrderDetails_combine_orderAdmin(admin.ModelAdmin):
    list_display = ['orderdetails', 'order_id']
    search_fields = ['order_id__unique_order_id', 'orderdetails__order_text']


admin.site.register(OrderDetails_combine_order, OrderDetails_combine_orderAdmin)


class DashboardDataAdmin(admin.ModelAdmin):
    list_display = ['status', 'type', 'dashboard_data', 'created_date']


admin.site.register(DashboardData, DashboardDataAdmin)


class LogHistoryAdmin(admin.ModelAdmin):
    list_display = ['model_name', 'model_fk', 'action', 'action_by', 'created_by', 'updated_by', 'previous_data',
                    'current_data',
                    'created_date', 'updated_date']
    list_filter = ['model_name']
    search_fields = ['model_name', 'action_by__username', 'created_by__username', 'updated_by__username', 'model_fk']


admin.site.register(LogHistory, LogHistoryAdmin)

from django.contrib.admin.models import LogEntry, DELETION
from django.utils.html import escape
from django.urls import reverse
from django.utils.safestring import mark_safe


@admin.register(LogEntry)
class LogEntryAdmin(admin.ModelAdmin):
    date_hierarchy = 'action_time'
    list_filter = [
        'user',
        'content_type',
        'action_flag'
    ]
    search_fields = [
        'object_repr',
        'change_message'
    ]
    list_display = [
        'action_time',
        'user',
        'content_type',
        'object_link',
        'action_flag',
    ]

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_view_permission(self, request, obj=None):
        return request.user.is_superuser

    def object_link(self, obj):
        if obj.action_flag == DELETION:
            link = escape(obj.object_repr)
        else:
            ct = obj.content_type
            link = '<a href="%s">%s</a>' % (
                reverse('admin:%s_%s_change' % (ct.app_label, ct.model), args=[obj.object_id]),
                escape(obj.object_repr),
            )
        return mark_safe(link)

    object_link.admin_order_field = "object_repr"
    object_link.short_description = "object"


admin.site.register(LogEntry, LogEntryAdmin)


class StoreCSVDataAdmin(admin.ModelAdmin):
    list_display = ['start_date', 'end_date', 'status', 'file_path', 'model_name', 'created_date']
    list_filter = ['model_name', 'status']
    search_fields = ['status', 'model_name']


admin.site.register(StoreCSVData, StoreCSVDataAdmin)


class StoreInvoiceDataAdmin(admin.ModelAdmin):
    list_display = ['start_date', 'end_date', 'seller', 'file_path', 'model_name', 'created_date']
    list_filter = ['model_name', 'seller']
    search_fields = ['seller', 'model_name']


admin.site.register(StoreInvoiceData, StoreInvoiceDataAdmin)
