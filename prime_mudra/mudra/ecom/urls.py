from django.urls import path

from .ecom_payments import *
from .escheduler import eschedular_test
from .views import *
from .report import *
from .dashboard import calculate_overdue_data

urlpatterns = [
     
     path('credit_details/', GetNewUserCreditDetails.as_view(), name="get_new_user_credit_details"),
    path('login/', user_login, name="user_login"),
    path('logout/', user_logout, name="user_logout"),
    path('change_password/', ChangePassword.as_view(), name="change_password"),
    path('reset_password/', ResetPassword.as_view()),

    path('product_add', ProductAdd.as_view(), name='product_add'),
    path('product_add/<msg>/', ProductAdd.as_view(), name='product_add'),
    path('product_details_all_images/<unique_product_details_id>/', product_details_all_images),
    path('product_details/<product_id>/', ProductDetailsView.as_view(), name='product_details'),
    path('total_users/', TotalUsers.as_view(), name='total_users'),
    path('total_buyer/', TotalBuyer.as_view(), name='total_users'),

    path('product_details_add/<product_id>/', ProductDetailsAdd.as_view(), name='product_details_add'),
    path('product_details_add/', ProductDetailsAdd.as_view(), name='product_details_add'),
    path('product_details_add/<product_id>/<msg>/', ProductDetailsAdd.as_view(), name='product_details_add'),

    # path('product_list', ProductList.as_view(), name='product_list'),
    path('product_list_response/', ProductList, name='product_list_response'),
    path('product_list/', product_list, name='product_list'),
    path('submittedproducts/', submitted_products, name='submitted_products'),
    path('submitted_products/', SubmittedProducts, name='submitted_products'),
    # path('submitted_products', SubmittedProducts.as_view(), name='submitted_products'),
    path('submitted_product_details/<product_id>/', SubmittedProductDetailsView.as_view(),
         name='submitted_product_details'),
    path('search_for/<status>/', Search_for.as_view(), name="search_for"),
    path('search_for_product_details/<product_id>/<status>/', Search_for_productdetails.as_view()),

    # Dashboard functions using ajax
    path('get_dashboard_data_1/', get_dashboard_data_1, name="get_dashboard_data_1"),
    path('get_dashboard_data_2/', get_dashboard_data_2, name="get_dashboard_data_2"),
    path('get_dashboard_data_3/', get_dashboard_data_3, name="get_dashboard_data_3"),
    path('get_dashboard_data_4/', get_dashboard_data_4, name="get_dashboard_data_4"),
    path('get_dashboard_data_5/', get_dashboard_data_5, name="get_dashboard_data_5"),
    path('get_dashboard_data_6/', get_dashboard_data_6, name="get_dashboard_data_6"),
    path('get_dashboard_data_7/', get_dashboard_data_7, name="get_dashboard_data_7"),
    path('get_dashboard_data_8/', get_dashboard_data_8, name="get_dashboard_data_8"),
    path('get_dashboard_data_9/', get_dashboard_data_9, name="get_dashboard_data_9"),
    path('get_dashboard_data_10/', get_dashboard_data_10, name="get_dashboard_data_10"),
    path('get_dashboard_data_11/', get_dashboard_data_11, name="get_dashboard_data_11"),
    path('get_dashboard_data_12/', get_dashboard_data_12, name="get_dashboard_data_12"),
    path('get_dashboard_data_13/', get_dashboard_data_13, name="get_dashboard_data_13"),
    path('get_dashboard_data_14/', get_dashboard_data_14, name="get_dashboard_data_14"),
    path('get_dashboard_data_15/', get_dashboard_data_15, name="get_dashboard_data_15"),

    path('monthwise_order_amount/', MonthwiseOrderAmount.as_view()),
    path('datewise_order_amount/<date>/', DatewiseOrderAmount.as_view()),
    path('order_amount_data/<date>/', OrderAmountData.as_view()),

    path('monthwise_seller_amount/', MonthwiseSellerAmount.as_view()),
    path('datewise_seller_amount/<date>/', DatewiseSellerAmount.as_view()),
    path('seller_amount_data/<date>/', SellerAmountData.as_view()),

    path('month_wise_shipping_fees/', MonthWiseShippingFees),
    path('date_wise_shipping_fees/<date>/', DateWiseShippingFees),
    path('shipping_order_details/<date>/', ShippingOrderDetails),

    # path('month_wise_conv_fees/', MonthWiseConvinientFees),
    # path('date_wise_conv_fees/<date>/', DateWiseConvenientFees),
    # path('conv_order_details/<date>/', ConvenientOrderDetails),

    path('category_subcategory_basecategory/', CategorySubaCategoryBasecategory.as_view()),
    path('category_subcategory_basecategory/<msg>/', CategorySubaCategoryBasecategory.as_view()),
    path('update/', Update),
    path('category_priority/', category_priority),
    path('product_priority/<category>/', product_priority),
    path('get_data/', get_data),

    path('product_csv/', product_csv),
    path('category_product_csv/', category_product_csv),
    path('product_list_csv/', ProductListCsv.as_view()),

    path('category_product/', CategoryProduct.as_view()),
    path('product_data/<category_name>/', ProductData.as_view()),
    path('edit_product_priority/<id>/', EditProductPriority.as_view()),
    path('set_priority', set_priority),
    path('change/<product>/<category>/', change_product_restatus),

    path('product_details_edit/<product_id>/', ProductDetailsEdit.as_view()),
    path('product_details_edit/<product_id>/<msg>/', ProductDetailsEdit.as_view()),

    # path('product_edit/<product_id>/', ProductEdit.as_view(), name='product_edit'),
    # path('product_edit/<msg>/', ProductEdit.as_view(), name='product_edit'),

    path('product-edit-1/<product_id>/', ProductEdit_1.as_view(), name='product_edit_1'),
    path('product-edit-1/<msg>/', ProductEdit_1.as_view(), name='product_edit_1'),

    path('add_data/(?P<status>)/(?P<msg>)/', AddData.as_view(), name="add_data"),
    path('add_data/<status>/', AddData.as_view(), name="add_data"),
    path('add_new_data/', AddDataFunction.as_view(), name="add_new_data"),

    path('add_partner/<status>/', Add_partner.as_view(), name='add_partner'),
    path('add_new_partner/', Add_new_partner.as_view(), name="add_new_partner"),
    path('add_partner/(?P<status>)/(?P<msg>)/', Add_partner.as_view(), name='add_partner'),

    path('credit_add_static/', CreditAddStatic.as_view(), name="credit_add_static"),
    path('credit_add_static/<id>/', CreditAddStatic.as_view(), name="credit_add_static"),

    path('add_stock/', AddStock.as_view(), name="add_stock"),
    path('add_new_stock/', add_new_stock, name="add_new_stock"),
    path('new_stock_update/', new_stock_update, name="new_stock_update"),
    path('add_stock_for_product/<id>/', AddStockForProduct.as_view(), name="add_stock_for_product"),

    path('sellers-total-orders/<seller>/', SellersOrders.as_view(), name="sellers_orders"),
    path('sellers-total-orders/', SellersOrders.as_view(), name="sellers_orders"),

    path('sellers-pending-orders/<seller>/', SellersPendingOrders.as_view(), name="sellers_pending_orders"),
    path('sellers-pending-orders/', SellersPendingOrders.as_view(), name="sellers_pending_orders"),

    path('sellers-initiated-orders/<seller>/', SellersInitiatedOrders.as_view()),
    path('sellers-initiated-orders/', SellersInitiatedOrders.as_view()),

    path('sellers-placed-orders/<seller>/', SellersPlacedOrders.as_view(), name="sellers_placed_orders"),
    path('sellers-placed-orders/', SellersPlacedOrders.as_view(), name="sellers_placed_orders"),
    path('placed_pending_orders/', placed_pending_orders),

    path('sellers-accepted-orders/<seller>/', SellersAcceptedOrders.as_view(), name="sellers_accepted_orders"),
    path('sellers-accepted-orders/', SellersAcceptedOrders.as_view(), name="sellers_accepted_orders"),

    path('user-cancelled-orders/<seller>/', UserCancelledOrders.as_view(), name="user-cancelled_orders"),
    path('user-cancelled-orders/', UserCancelledOrders.as_view(), name="user-cancelled_orders"),

    path('sellers-rejected-orders/<seller>/', SellersRejectedOrders.as_view(), name="sellers_rejected_orders"),
    path('sellers-rejected-orders/', SellersRejectedOrders.as_view(), name="sellers_rejected_orders"),

    path('sellers-delivered-orders/<seller>/', SellersDeliveredOrders.as_view(), name="sellers_placed_orders"),
    path('sellers-delivered-orders/', SellersDeliveredOrders.as_view(), name="sellers_placed_orders"),
    path('order-all-details/<order_id>/', OrderAllDetails.as_view(), name="order_all_details"),
    path('order-all-details/<order_id>/<msg>/', OrderAllDetails.as_view(), name="order_all_details"),
    path('change-order-status/', ChangeOrderStatus.as_view(), name="change_order_status"),
    path('packed_order_details/', packed_order_details),
    path('mfpl_cancel_order/', mfpl_cancel_order),

    # Sellers Dashboard new functions monthwise and daywise..
    path('sellers-all-orders/<status>/', SellersAllOrders.as_view()),
    path("total_seller/<status>/", total_seller),
    path("page_data/", page_data),
    path("monthwise_seller/<seller_id>/<status>/", monthwise_seller),
    path('monthwise_seller_count/', monthwise_seller_count),
    # path('monthwise_seller_count/<seller_id>/<status>/', monthwise_seller_count),
    path("datewise_seller/<seller_id>/<month>/<status>/", datewise_seller),
    path('datewise_seller_count/', datewise_seller_count),
    # path('datewise_seller_count/<seller_id>/<month>/<status>/', datewise_seller_count),
    path('seller_total_order/<seller_id>/<month>/<status>/', seller_total_order),
    path('sellers_total_orders_data/', sellers_total_orders_data),
    # path('sellers-total-orders-data/<seller_id>/<month>/<status>/', sellers_total_orders_data),

    path('update_parameters/', UpdateParameters, name="update_parameters"),
    path('update_partners/', UpdatePartners, name="update_partners"),

    path('add_subcategory/(?P<status>)/(?P<msg>)/', AddSubcategory.as_view(), name="add_subcategory"),
    path('add_subcategory/', AddSubcategory.as_view(), name="add_subcategory"),
    path('add_subcategory_data/', AddSubCategoryFunction.as_view(), name="add_subcategory_data"),
    path('add_base_category/(?P<msg>)/', AddBaseCategory.as_view(), name="add_base_category"),
    path('add_base_category/', AddBaseCategory.as_view(), name="add_base_category"),
    path('add_base_category_data/', AddBaseCategoryFunction.as_view(), name="add_base_category_data"),
    path('update_base_category', UpdateBaseCategory, name="update_base_category"),

    path('all_orders/', AllOrders.as_view(), name="all_orders"),
    path('order_details/<order_text>/', OrdersDetail.as_view(), name="order_details"),

    path('users-cart/', UserCartDetails.as_view()),
    path('users-cart/<user>/', UserCartDetails.as_view()),

    path('sellers-dashboard/', SellersDashboard.as_view(), name="sellers_dashboard"),
    path('dashboard/<dashboard>/', dashboard),

    # path('sellers_orders/<seller>/', SellersOrdersNew.as_view(), name="sellers_orders"),
    # path('sellers_orders/', SellersOrdersNew.as_view(), name="sellers_orders"),

    path('total-sellers/', TotalSellers.as_view()),
    path('product_status_count/<seller>/', ProductStatusCount.as_view()),
    path('sellers_product_details/<seller>/<status>/', SellersProductDetails.as_view()),
    path('sellers-product_status_count/', SellersProductStatusCount.as_view()),
    path('total-logistics/', TotalLogistics.as_view()),

    path('update_product_details/', update_Product_details, name="update_Product_details"),
    path('delivery-charges-details/', DeliveryChargesDetails.as_view(), name='delivery_charges_details'),
    path('search-order-status/', SearchOrderStatus.as_view(), name='search_order'),
    path('check_pin_code/', check_pin_code),

    path('user-profile/<user_id>/', EcomUserProfile.as_view(), name="ecom-user-profile"),
    path('ecom-order-details/<order_details_id>/', EcomOrderAllDetails.as_view()),

    # Urls of ecom_payments =================================================
    path('order_payment_monthwise/', OrderPaymentMonthwise.as_view(), name='order_payment_monthwise'),
    path('order_payment_datewise/<on_month>/', OrderPaymentDatewise.as_view(), name='order_payment_datewise'),
    path('order_payment_details/<on_date>/', OrderPaymentDetails.as_view(), name='order_payment_details'),
    path('order_details_delivery_charges/', OrderDetailsDeliveryCharges.as_view(),
         name='order_details_delivery_charges'),
    path('order_details_payment/<order_details_id>/', OrderDetailsPayment.as_view(), name='order_details_payment'),
    path('credit_repayment_info/', CreditRepaymentInfo.as_view(), name='credit_repayment_info'),

    path('credit_repayment_details/', CreditRepaymentDetails.as_view(), name="credit_repayment_details"),
    path('userwise_credit_got/', UserwiseCreditGot.as_view(), name="userwise_credit_got"),
    path('sellerwise_credit_repayment_details/', UserWiseCreditRepaymentDetails.as_view(),
         name="sellerwise_credit_repayment_details"),
    path('sellerwise_order_credit_details/<user>/', UserWiseOrderCreditDetails.as_view(),
         name="sellerwise_order_credit_details"),
    path('sellerwise_order_payment_details/<user>/', UserWiseOrderPaymentDetails.as_view(),
         name="sellerwise_order_payment_details"),
    path('repayment_order_details/', RepaymentOrderDetails.as_view(), name="repayment_order_details"),

    path('seller_amount/<date>/', SellersAmount.as_view(), name="seller_amount"),

    path('seller_dispatch_details/', SellersDispatchDetails.as_view(), name="seller_dispatch_details"),
    path('seller_return_order/<date>/<seller>/', SellerReturnOrder.as_view(), name="seller_return_order"),
    path('accounts_report/', AccountsReport.as_view()),
    path('invoice/', Invoice.as_view()),

    path('sellers_datewise_total_amount/', SellersDatewiseTotalAmount.as_view()),
    path('seller_amount/<date>/', SellersAmount.as_view(), name="seller_amount"),
    path('seller_dispatch_details/<date>/<seller>/<type>/', SellersDispatchDetails.as_view(),
         name="seller_dispatch_details"),
    path('seller_return_order_count/<date>/<order>/', SellerReturnOrderCount.as_view(),
         name="seller_return_order_count"),
    path('seller_return_order/<date>/<seller>/<order>/', SellerReturnOrder.as_view(), name="seller_return_order"),

    path('seller_details/<seller>/', SellerDetails.as_view()),
    path('logistics_details/<logistic>/', LogisticsDetails.as_view()),
    path('seller_bill/<date>/', OnDateSellerBill.as_view(), name="OnDateSellerBill"),

    # Accounts
    path('print_seller_invoice/', print_seller_invoice),

    path('bulk_product_add/', bulk_product_add_csv_file, name="bulk_product_add"),
    # path('bulk_product_upload/', bulk_product_add_csv_file, name="bulk_product_upload"),

    path('download_bulk_upload_sample/<file_type>/', download_bulk_upload_sample),
    path('privacy_policy', privacy_policy),
    path('shipping_policy', shipping_policy),
    path('about_us_web', about_us_web),
    path('credit_policy', credit_policy),
    path('return_policy', return_policy),
    path('terms_and_conditions', terms_and_conditions),
    path('faq', frequently_asked_questions),
    path('tax_invoice/<order_id>/', generate_tax_invoice),
    path('generate_order_details_tax_invoice/<order_details_id>/', generate_order_details_tax_invoice),
    path('search_order_invoice/', search_order_invoice),
    path('invoice_data/', invoice_data),

    # scheduler function
    path('calculate_seller_orders', calculate_seller_orders_request),
    path('calculate_overdue_data/', calculate_overdue_data),
    path('category_pdfdownload/', category_pdfdownload),
    path('contact_us/', contact_us),

    path('admin_amount_update/', adminamount_update),

    # seller dispatch and delivered order

    path('dispatch_delivered_box/', DispatchDeliveredBox.as_view()),
    path('monthwise_report_for_accounts/<status>/', MonthwiseReportForAccounts.as_view()),
    path('order_report_for_accounts/<date>/<status>/', OrderReportForAccounts.as_view()),
    path('sellerwise_order_report/<date>/<status>/', SellerwiseOrderReport.as_view()),
    path('dispatch_delivered_order/<date>/<status>/<seller>/', DispatchDeliveredOrder.as_view()),

    # seller amount

    path('sellerwise_invoice_report/', SellerwiseInvoiceReport.as_view()),
    path('sellerwise_details/<name>/', SellerwiseInvoiceDetails.as_view()),
    path('seller_datewise_orders/<username>/<date>/', SellerDatewiseOrderDetail.as_view()),
    path('seller_datewise_orderdelivered/<username>/<date>/', SellerDatewiseOrderDelivered.as_view()),
    path('seller_bill/<date>/<seller_id>/', OnDateSellerBill.as_view(), name="OnDateSellerBill"),
    path('invoice_form/<invoice_id>/', invoice_form.as_view()),
    path('invoice_data/<invoice_id>/', Invoice_data.as_view()),
    path('invoice_form_new/<seller_id>/', invoice_form_new.as_view()),
    path('invoice_data_new/<seller_id>/', Invoice_data_new.as_view()),

    # Product wise total sells count
    path('sells_count/', sells_count),
    path('sells_count_data/', sells_count_data),
    path('sells_order/<date>/<category>/', sells_order),
    path('month_wise_sells_count/<category>/', month_wise_sellers_count),
    path('date_wise_sells_count/<date>/<category>/', date_wise_sells_count),

    # Reports
    # Product wise total sells count
    path('sells_count/', sells_count),
    path('sells_order/<date>/<category>/', sells_order),
    path('month_wise_sells_count/<category>/', month_wise_sellers_count),
    path('date_wise_sells_count/<date>/<category>/', date_wise_sells_count),
    path('monthwise_order/', MonthwiseOrder.as_view()),
    path('datewise_order/<date>/', DatewiseOrder.as_view()),
    path('order_data/<date>/', OrderData.as_view()),
    path('monthwise_cashback/', MonthwiseCashback.as_view()),
    path('datewise_cashback/<date>/', DatewiseCashback.as_view()),
    path('cashback_data/<date>/<status>/', CashbackData.as_view()),

    # profit amount
    path('total_profit_amount/', total_profit_amount),
    path('month_wise_profit/', month_wise_profit),
    path('date_wise_profit/<date>/', date_wise_profit),
    path('profit_order/<date>/', profit_order),
    path('month_wise_logistic_profit/', month_wise_logistic_profit),
    path('date_wise_logistic_profit/<date>/', date_wise_logistic_profit),

    # payment wise amount reports
    # path('payment_wise_amount/', payment_wise_amount),
    # path('date_wise_payment_wise_amount/<date>/', date_wise_payment_wise_amount),
    # path('order_details/<date>/<status>/', order_details),

    # calculate total overdue amount
    path('get_total_over_dues_amount/', get_total_over_dues_amount),
    path('date_wise_over_dues_amount/<date>/', date_wise_over_dues_amount),

    # search order
    path('search_order/<status>/', SearchOrder.as_view()),
    path('search_duplicates/', SearchDupliactes.as_view()),
    path('order_related_data/<order_id>/', order_related_data),
    path('order_details_related_data/<order_details_id>/', order_details_related_data),
    path('return_orders_csv/<order_id>/', return_orders_csv),
    path("seller_order_csv/<seller_id>/", seller_order_csv),
    path('change_status/', change_status),
    path('change_status1/', change_status1),
    path('cancel_order/', cancel_order_new),
    path('user_due_amount_current/', user_due_amount_current),
    path("credit_wallet_logistics_data/<order_id>/<status>/", CreditWalletLogisticsData.as_view()),
    path('repayment_details/', repayment_details),
    path('order_details_related_data1/', order_details_related_data1),
    path('get_orders_details_payments/', get_orders_details_payments),

    # convenient fees report
    path('month_wise_conv_fees/', month_wise_convenient_fees),
    path('date_wise_conv_fees/<date>/', date_wise_convenient_fees),
    path('conv_order_details/<date>/', convenient_order_details),

    # # shipping reports
    # path('month_wise_shipping_fees/', month_wise_shipping_fees),
    # path('date_wise_shipping_fees/<date>/', date_wise_shipping_fees),
    # path('shipping_order_details/<date>/', shipping_order_details),

    path('monthwise_seller_amt/', month_wise_seller_amount),
    path('seller_wise_sellers_count/<date>/', seller_wise_sellers_count),
    path('seller_date_wise_sellers_count/<date>/<seller>/', seller_date_wise_sellers_count),

    # Order reports for all status
    path('monthwise_orders/<status>/', MonthwiseOrders.as_view()),
    path('datewise_orders/<status>/<date>/', DatewiseOrders.as_view()),
    path('order_details_all/<status>/<date>/', OrderDetailsAll.as_view()),

    # Sellers wise place packed orders
    path('placed_packed_order/', PlacedPackedOrder.as_view()),
    path('monthwise_placed_packed_order/<seller>/<status>/', MonthwisePlacedPackedOrder.as_view()),
    path('datewise_placed_packed_order/<date>/<seller>/<status>/', DatewisePlacedPackedOrder.as_view()),
    path('placed_packed_order_data/<date>/<seller>/<status>/', PlacedPackedOrderData.as_view()),

    # sellers orders
    path("sellers_orders/", sellers_orders),
    path("sellers_all_orders/<id>/", sellers_all_orders),

    # search seller
    path("search_seller/", search_seller),
    path("seller_data/", seller_data),

    # missing logistic orders
    path("missing_logistic_orders/", missing_logistic_orders),
    path("missing_logistic_orders_csv/<object>/", missing_logistic_orders_csv),

    # search logistics order
    path('search_logistics_order/', SearchLogisticsOrder.as_view()),
    path('search_logistics_orders/', search_logistics_order),
    # Reports Ends

    # stock add, subtract and out of stock urls
    path('stock_details/', StockDetails.as_view()),
    path('new_stock_update/', new_stock_update, name="new_stock_update"),

    # search seller and show its dispatch delivered order
    path('search_seller_dispatched_delivered_order/', SearchSellerDispatchedDeliveredOrder.as_view()),
    path('get_seller/', get_seller),
    path('show_dispatch_delivered_details/<seller>/<status>/', show_dispatch_delivered_details),

    # Accounts invoice Urls

    # path('sellerwise_invoice_report/', SellerwiseInvoiceReport.as_view()),
    path('sellerwise_paid_pending_invoice/', SellerwisePaidPendingInvoice.as_view()),
    path('monthwise_paid_pending_invoice/<seller>/', MonthwisePaidPendingInvoice.as_view()),
    path('datewise_paid_pending_invoice/<seller>/<month>/<status>/', DatewisePaidPendingInvoice.as_view()),

    # date wise seller invoices
    path('monthwise_invoice/', MonthwiseInvoice.as_view()),
    path('datewise_invoice/<month>/', DatewiseInvoice.as_view()),
    path('seller_list_invoice/<date>/', seller_list_invoice),
    path('seller_bill/<date>/<seller_id>/<status>/', OnDateSellerBill.as_view(), name="OnDateSellerBill"),
    # path('invoice_form/<invoice_id>/', invoice_form.as_view()),
    # path('invoice_data/<invoice_id>/', Invoice_data.as_view()),
    # Accounts invoice Urls

    # seller order cancellation reports
    path("seller_order_cancel/", seller_order_cancel),
    path("seller_wise_cancel_order/<date>/", seller_wise_cancel_order),
    path("date_wise_seller_order_cancel/<date>/", date_wise_seller_order_cancel),
    path("cancel_order_data/<seller>/<date>/", cancel_order_data),
    path("cancel_order_csv/<id_list>/", cancel_order_csv),

    # logistics packed order
    path("logistic_delivery_reverse_packed_count/", LogisticsDeliveryReversePackedCount.as_view()),
    path("logistic_orders/<seller>/<status>/", LogisticOrderDetails.as_view()),

    # logistics packed order details
    path("logistics_order_details/", LogisticsOrderDetails.as_view()),
    path("logistics_order_details_data/<status>/", LogisticsOrderDetailsData.as_view()),
    path("logistics_order_details_order_data/<order_details_id>/", logistics_order_details_order_data),

    # logistics in_transit order
    path("logistic_delivery_reverse_in_transit_count/", LogisticsDeliveryReverseInTransitCount.as_view()),
    path("logistic_in_transit_orders/<seller>/<status>/", LogisticInTransitOrderDetails.as_view()),

    # logistics error order
    path("logistic_error_order_count/", LogisticsErrorOrderCount.as_view()),
    path("logistic_error_orders/<seller>/<status>/", LogisticErrorOrderDetails.as_view()),

    # check placed order
    path('check_placed_order/', check_placed_order),

    # bulk packed report
    path("bulk_packed/", bulk_packed),
    path("bulk_change_to_packed/", bulk_change_to_packed),

    # Test
    path('eschedular_test/', eschedular_test),
    path('update_min_max_amount/', update_min_max_amount),
    path("seller_order_cancelled/", seller_order_cancelled),

    # order wise orders
    path("order_wise_month_wise_orders/", order_wise_month_wise_orders),
    path("order_wise_date_wise_orders/<date>/", order_wise_date_wise_orders),
    path("order_wise_orders/<date>/<status>/", order_wise_orders),

    # Seller wise total, paid, remaining Amount
    path("sellerwise_amount_invoice/", SellerwiseAmountInvoice.as_view()),
    path("monthwise_amount_invoice/<user>/", MonthwiseAmountInvoice.as_view()),
    path("datewise_amount_invoice/<user>/<date>/", DatewiseAmountInvoice.as_view()),
    path("seller_amount_invoice/<seller_id>/<date>/", SellerAmountInvoice.as_view()),

    # Net pay
    path("net_pay/", NetPay.as_view()),
    path("month_wise_net_pay/<user>/", MonthwiseNetPay.as_view()),
    path("date_wise_net_pay/<user>/<date>/", DatewiseNetPay.as_view()),

    # NetReceivables
    path("net_receivables/", NetReceivables.as_view()),

    # show product details data
    path("show_product_details_data/<product_id>/", show_product_details_data),

    # NetProfit
    path("return_product_csv/", return_product_csv),
    path("return_product_csv_file/", return_product_csv_file),
    path("net_profit/", NetProfit.as_view()),

    # customer csv data
    path("date_wise_data/", date_wise_data),
    path("return_customer_csv/", return_customer_csv),

    # Bulk Stock Add
    path("bulk_stock_add/", bulk_stock_add),
    path('download_stock_bulk_upload_sample/', download_stock_bulk_upload_sample),

    # due sms
    path('due_sms/', due_sms_request),

    path('get_users_csv/', get_users_csv),

    # set product priority
    path('category_product_priority/', CategoryProductPriority.as_view()),
    path('product_data_priority/<seller_name>/', ProductDataPriority.as_view()),
    path('set_product_priority/', set_product_priority),

    # create banner
    path('sellerwise_banner_product_list/', sellerwise_banner_product_list),
    path('banner_product_list/<seller>/', banner_product_list),
    path('edit_banner_report/<banner_id>/', EditBannerReport.as_view()),
    path('create_banner/<product_id>/', CreateBanner.as_view()),

    # banner report
    path('sellerwise_banner_report/', sellerwise_banner_report),
    path('product_banner_report/<seller>/', product_banner_report),
    path('product_priority/', product_priority),
    path('get_banner_data/', get_banner_data),

    # product_status_change
    path("product_status_change/", product_change_status),
    path("download_product_sample/", download_product_sample),

    # order map
    path('order_chart/', order_chart),
    path('user_chart/', user_chart),
    path('product_chart/', product_chart),
    path('default_chart/', default_chart),
    path('product_chart_for_seller/', product_chart_for_seller),

    # seller pending amount
    path('seller_pending_amount_invoice/', seller_pending_amount_invoice),
    path('seller_wallet_data/<seller>/', seller_wallet_data),
    path("seller_wallet_ajax_data/", seller_wallet_ajax_data),

    # seller invoice data
    path('monthwise_seller_amount_data/', MonthwiseSellerAmountData.as_view()),
    path('datewise_seller_amount_data/<date>/', DatewiseSellerAmountData.as_view()),
    path('sellerwise_seller_amount_data/<date>/', SellerwiseSellerAmountData.as_view()),
    path('show_seller_amount_data/<date>/<seller>/<order_type>/', ShowSellerAmountData.as_view()),

    # order chart
    path('order_graph_for_ecomadmin/', order_graph_for_ecomadmin),
    path('order_graph_for_ecomadmin1/', order_graph_for_ecomadmin1),
    path('order_graph_for_seller/', order_graph_for_seller),
    path('order_graph_for_seller1/', order_graph_for_seller1),

    path('product_graph_for_ecomadmin/', product_graph_for_ecomadmin),
    path('product_graph_for_ecomadmin1/', product_graph_for_ecomadmin1),
    path('product_graph_for_seller/', product_graph_for_seller),
    path('product_graph_for_seller1/', product_graph_for_seller1),

    # Account maps
    path("net_profit_graph/", net_profit_graph),
    path("net_profit_graph_data/", net_profit_graph_data),

    # notification urls
    path("payment_reminder/", payment_reminder),
    path("credit_limit_reminder/", credit_limit_reminder),

    # unused credit amount report
    path("unsused_credit/", unsused_credit),
    path("unsused_credit_report/", unsused_credit_report),
    # path("unsused_credit_report/", unsused_credit_report),

    # penalty reports
    path('penalty_amount/', penalty_amount),
    path('month_wise_sellers_penalty/<seller>/', month_wise_sellers_penalty),
    path('date_wise_sellers_penalty/<seller>/<date>/', date_wise_sellers_penalty),
    path('show_sellers_penalty_data/<seller>/<date>/', show_sellers_penalty_data),

    # users monthwise purchasing
    path("month_wise_total_orders/", month_wise_total_orders),
    path("users_month_wise_total_orders_csv/<month>/", users_month_wise_total_orders),

    # create offer banner
    path('offer_banner_product_list/', offer_banner_product_list),
    path('create_offer_banner/', CreateOfferBanner.as_view()),
    path('edit_offer_banner/<id>/', EditOfferBanner.as_view()),
    path('offer_banner_priority/', offer_banner_priority),
    path('get_offer_bannner_data/', get_offer_bannner_data),

    # create user offer
    path('user_offer_list/', user_offer_list),
    path('create_user_offer/', CreateUserOffer.as_view()),
    path('edit_user_offer/<id>/', EditUserOffer.as_view()),
    path('edit_user_offer_data/<user>/<offer_id>/', EditUserOfferData.as_view()),
    path('offer_priority/', offer_priority),
    path('get_offer_data/', get_offer_data),

    # bulk user offer add
    path('bulk_user_offer_add/', bulk_user_offer_add),
    path('user_offer_bulk_upload_sample/', user_offer_bulk_upload_sample),

    # user offer report
    path('user_offer_report/', user_offer_report),
    path('user_offer_data/<username>/', user_offer_data),
    path('offer_for_user/<offer_id>/', offer_for_user),

    # Defaulter
    path('defaulter/', defaulter),
    path('cancel_order_test/', cancel_order_test),

    # coupon data
    path("coupon_data/", coupon_data),
    path("coupon_csv/", coupon_csv),

    # pending OrderDetails
    path("pending_order_details/<id>/", pending_order_details),
    path("all_pending_order_details/", all_pending_order_details),

    # search contact
    path("search_contact/", search_contact),
    path("contact_data/", contact_data),
    path("contact_data_csv/<username>/", contact_data_csv),

    # seller invoice csv
    path("seller_invoice_csv/", seller_invoice_csv),
    path("seller_invoice_csv_file/", seller_invoice_csv_file),

    path('wallet_recharge/', wallet_recharge),
    path('walletrecharge_data/', walletrecharge_data),
    path('updatewallet/', updatewallet),

    path('product_rating/', product_rating),
    path('user_product_rating_data/<product_name>/', user_product_rating_data),

    # dashboard data functions
    path("create_dashboard_data/", create_dashboard_data_request),
    path("dashboard_data/", dashboard_data),
    path("date_wise_dashboard_data/<date>/", date_wise_dashboard_data),
    path("daily_dashboard_data/<date>/", daily_dashboard_data),
    path("dashboard_data_csv/", DashboardDataCsv.as_view()),
    path("get_dashboard_csv/<date>/", get_dashboard_csv),

    # pure cash payment report
    path('user_wise_amount/', user_wise_amount),
    path('user_wise_month_wise_amount/<year>/', user_wise_month_wise_amount),
    path('user_wise_date_wise_amount/<month>/', user_wise_date_wise_amount),

    path('user_match_alternate_no/', user_match_alternate_no),

    # monthwise, datewise cash on delivery report
    path('monthwise_cod/', monthwise_cod),
    path('datewise_cod/<created_date>/', datewise_cod),
    path('cod_data/<created_date>/', cod_data),
    path('cod_order_data/<order_details_id>/', cod_order_data),
    path('cod_payment/', cod_payment),
    path('create_cod_payment/', create_cod_payment),

    # bulk_hsn_add
    path("bulk_HSN_add/", bulk_HSN_add),
    path("download_bulk_hsn_add_sample/", download_bulk_hsn_add_sample),

    # search_orders_repayment
    path("search_orders_repayment/", search_orders_repayment),
    path("orders_repayment_data/", orders_repayment_data),

    path('orders_total_credit_repayment_data/<order_text>/<category>/', orders_total_credit_data),
    path('convenient_fees_update/', convenient_fees_update),

    # delete_product_details_image
    path('delete_product_details_image/', delete_product_details_image),
    path('get_product_details_images/', get_product_details_images),

    # mfpl placed order_details
    path('monthwise_placed_mfpl_order/', monthwise_placed_mfpl_order),
    path('datewise_placed_mfpl_order/<date>/', datewise_placed_mfpl_order),
    path('placed_mfpl_order_data/<date>/', placed_mfpl_order_data),
    path('packed_order_details_data/<order_details_id>/', packed_order_details_data),

    # month wise credit sell
    path('monthwise_placed_mfpl_order/', monthwise_placed_mfpl_order),
    path("order_details_order_data/<order_details_id>/", order_details_order_data),

    #sell report

    path("sell_report/",sell_report),
    path("sell_csv/",sell_csv),

    # Orders Repayment Entry
    path("create_repayment_entry/", create_repayment_entry),
    path("add_repayment_entry/", add_repayment_entry),

    #Search Cod
    path("cod_order_search/", cod_order_search),
    path("show_cod_repayment_data/", show_cod_repayment_data),



    #Cod order amount
    path('monthwise_cod_order_amount/', monthwise_cod_order_amount),
    path('show_cod_order_amount/<date>/', show_cod_order_amount),

    # Bulk Mobile Block Add
    path("bulk_mobile_block/", bulk_mobile_block),
    path("download_bulk_mobile_block_sample/", download_bulk_mobile_block_sample),

    # update credit balance
    path('update_credit_balance/', update_credit_balance),

    # phonepe_cancel_order
    path('phonepe_order/', phonepe_order),

    path('phonepe_cancel_order/<is_refunded>/', PhoenpeCancelOrder.as_view(), name='phonepe_cancel_order'),
    path('update_is_refund/', update_is_refund),


   #product_details_sku_csv
    path("product_details_sku_change/", product_details_sku_change),
    path("download_product_details_sample/", download_product_details_sample),

    path('seller_data/<status>/', Seller_data.as_view(), name="seller_data"),
    path('return_seller_orders_csv/<order_id>/', return_seller_orders_csv),

    #zest_money_order
    path('zest_money_order/', zest_money_order),
    path('zest_money_order_data/<order_id>/', zest_money_order_data),

    # Reports.py urls..
    path("get_reports_page/", get_reports_page),
    path("get_csv/", get_csv),
    path("get_order_csv/", get_order_csv),
    path("get_product_details_csv/", get_product_details_csv),
    path("get_invoice_page/", get_invoice_page),
    path("seller_amount_invoice_new/", seller_amount_invoice_new),

]
