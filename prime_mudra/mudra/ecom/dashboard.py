from django.apps.registry import apps
from django.contrib.auth.models import User
from django.db.models import Sum
from django_pandas.io import read_frame
from rest_framework.utils import json
# from django.db import connection, reset_queries
import pandas as pd
import datetime
from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse

LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
Product = apps.get_model('ecom', 'Product')
ProductDetails = apps.get_model('ecom', 'ProductDetails')
Order = apps.get_model('ecom', 'Order')
OrderDetails = apps.get_model('ecom', 'OrderDetails')
ReturnOrder = apps.get_model('ecom', 'ReturnOrder')
LogisticOrder = apps.get_model('ecom', 'LogisticOrder')
UserWallet = apps.get_model('ecom', 'UserWallet')
SellerAccount = apps.get_model('ecom', 'SellerAccount')
SellerAmount = apps.get_model('ecom', 'SellerAmount')
Payment = apps.get_model('Payments', 'Payment')
SellerWallet = apps.get_model('ecom', 'SellerWallet')
WalletRecharge = apps.get_model('ecom', 'WalletRecharge')
Credit = apps.get_model('ecom', 'Credit')
Recovery = apps.get_model('Recovery', 'Recovery')
RecoveryInsentive = apps.get_model('Recovery', 'RecoveryInsentive')
RecoveryCallHistory = apps.get_model('Recovery', 'RecoveryCallHistory')
OverDueData = apps.get_model('ecom', 'OverDueData')

'''
def query_debugger(func):
    @functools.wraps(func)
    def inner_func(*args, **kwargs):
        reset_queries()

        start_queries = len(connection.queries)

        start = time.perf_counter()
        result = func(*args, **kwargs)
        end = time.perf_counter()

        end_queries = len(connection.queries)

        print(f"Function : {func.__name__}")
        print(f"Number of Queries : {end_queries - start_queries}")
        print(f"Finished in : {(end - start):.2f}s")
        return result

    return inner_func
'''


# @query_debugger
def data_1(user):
    """
    Total orders count , cancelled, delivered, dispatched, initiated, placed, packed, pending, undelivered,
    s_dispatched & in_transit orders count
    """

    ctx = {}

    orders_count = 0
    get_cancelled_orders = 0
    get_initiated_orders = 0

    # EcomAdmin & EcomTeam & EcomManager Group
    ecom_admin_group = user.groups.filter(name__in=['EcomAdmin', 'EcomTeam', 'EcomManager']).exists()
    if ecom_admin_group:
        df = pd.DataFrame(Order.objects.values_list("status", flat=True), columns=["status"])
        data_df = df.groupby(df['status'])['status'].count().sort_index(axis=0)
        result = data_df.to_json(orient="split")

        parsed = json.loads(result)
        status = parsed["index"]
        counts = parsed["data"]
        count_list = []
        status_list = ['cancelled', 'delivered', 'dispatched', 'initiated', 'placed', 'packed', 'pending',
                       'undelivered',
                       's_dispatched', 'in_transit']
        for i in status_list:
            if i in status:
                index = status.index(i)
                count_list.append(counts[index])
            else:
                count_list.append(0)

        orders_count = count_list[4] + count_list[5] + count_list[2] + count_list[8] + count_list[9] + count_list[1]

        get_total_orders = sum(counts)
        get_pending_orders = count_list[6]
        get_placed_orders = count_list[4]
        get_delivered_orders = count_list[1]
        get_dispatch_orders = count_list[2]
        get_undelivered_orders = count_list[7]
        get_initiated_orders = count_list[3]
        get_cancelled_orders = count_list[0]

    # Sellers Group
    else:
        df = pd.DataFrame(Order.objects.filter(product_details__user=user).values_list("status", flat=True),
                          columns=["status"])
        data_df = df.groupby(df['status'])['status'].count().sort_index(axis=0)
        result = data_df.to_json(orient="split")

        parsed = json.loads(result)
        status = parsed["index"]
        counts = parsed["data"]
        count_list = []
        status_list = ['cancelled', 'delivered', 'dispatched', 'initiated', 'placed', 'packed', 'pending',
                       'undelivered',
                       's_dispatched', 'in_transit']
        for i in status_list:
            if i in status:
                index = status.index(i)
                count_list.append(counts[index])
            else:
                count_list.append(0)

        get_total_orders = sum(counts) - count_list[3]
        get_placed_orders = count_list[4]
        get_delivered_orders = count_list[1]
        get_pending_orders = count_list[6]
        get_dispatch_orders = count_list[2]
        get_undelivered_orders = count_list[7]
        get_cancelled_orders = count_list[0]

    ctx['total_orders'] = get_total_orders
    ctx['pending_orders'] = get_pending_orders
    ctx['placed_orders'] = get_placed_orders
    ctx['delivered_orders'] = get_delivered_orders
    ctx['dispatch_orders'] = get_dispatch_orders
    ctx['undelivered_orders'] = get_undelivered_orders
    ctx['initiated_orders'] = get_initiated_orders
    ctx['cancelled_orders'] = get_cancelled_orders
    ctx['orders_count'] = orders_count

    return ctx


# @query_debugger
def data_2(user):
    """
      Return Orders count, Total logistics Order, Total Products Count, Total Profit , Total Packed logistics orders
    """
    ctx = {}
    get_return_orders = 0
    get_all_products = 0
    get_logistics_orders = 0
    logistic_amount = 0
    final_profit = 0
    customer_amount = 0
    get_packed_logistics_orders = 0
    convenient_fees = 0

    # EcomAdmin & EcomTeam & EcomManager Group
    ecom_admin_group = user.groups.filter(name__in=['EcomAdmin', 'EcomTeam', 'EcomManager']).exists()

    if ecom_admin_group:
        total_profit_amount = 0
        customer_obj = \
            Order.objects.exclude(status__in=["initiated", "cancelled"]).aggregate(Sum('delivery_charges__amount'))[
                'delivery_charges__amount__sum']
        if customer_obj:
            customer_amount = customer_obj
        wallet_recharge_obj = WalletRecharge.objects.aggregate(Sum('amount'))['amount__sum']

        if wallet_recharge_obj:
            logistic_amount = wallet_recharge_obj
        logistic_profit = customer_amount - logistic_amount

        order_obj = Order.objects.select_related("product_details__sgst", "product_details__cgst",
                                                 "order_details_amount",
                                                 "order_details_amount__cgst", "order_details_amount__sgst").exclude(
            status__in=["cancelled", "initiated", "return", "undelivered"])
        if order_obj:
            from .views import return_profit_amount
            for order in order_obj:
                if order.order_details_amount.cgst and order.order_details_amount.sgst:
                    profit_amount = return_profit_amount(order.order_details_amount.admin_amount,
                                                         order.order_details_amount.seller_amount,
                                                         (order.order_details_amount.cgst.percentage +
                                                          order.order_details_amount.sgst.percentage))
                    total_profit_amount += profit_amount
        else:
            total_profit_amount = 0

        conv_charges_order_details = \
            OrderDetails.objects.exclude(status="initiated").exclude(convenient_fees=None).aggregate(
                Sum('convenient_fees__amount'))[
                'convenient_fees__amount__sum']
        if conv_charges_order_details:
            convenient_fees += conv_charges_order_details

        conv_charges_order = \
            Order.objects.exclude(status="initiated").exclude(convenient_fees=None).aggregate(
                Sum('convenient_fees__amount'))[
                'convenient_fees__amount__sum']
        if conv_charges_order:
            convenient_fees += conv_charges_order

        final_profit = logistic_profit + convenient_fees + total_profit_amount
        final_profit = "{:12.2f}".format(final_profit)

        return_orders = ReturnOrder.objects.filter(type="return", payment_status="success").count()
        if return_orders and return_orders is not None:
            get_return_orders = return_orders

        logistics_orders = LogisticOrder.objects.count()
        if logistics_orders:
            get_logistics_orders = logistics_orders

        all_products = Product.objects.filter(re_status=True).count()
        if all_products and all_products is not None:
            get_all_products = all_products

        packed_logistics_orders = LogisticOrder.objects.filter(order__status='packed', status='success',
                                                               order_mode='Delivery').count()
        if packed_logistics_orders:
            get_packed_logistics_orders = packed_logistics_orders


    # sellers Group
    else:
        return_orders = ReturnOrder.objects.filter(
            seller_status__in=["seller_return_confirm", "seller_return_rejected"],
            order__product_details__user=user).count()
        if return_orders and return_orders is not None:
            get_return_orders = return_orders

        logistics_orders = LogisticOrder.objects.filter(order__product_details__user=user).count()
        if logistics_orders:
            get_logistics_orders = logistics_orders

        all_products = Product.objects.filter(re_status=True, seller=user).count()
        if all_products:
            get_all_products = all_products

        packed_logistics_orders = LogisticOrder.objects.filter(order__product_details__user=user, status='success',
                                                               order__status='packed', order_mode='Delivery').count()
        if packed_logistics_orders:
            get_packed_logistics_orders = packed_logistics_orders

    ctx['return_orders'] = get_return_orders
    ctx['logistics_orders'] = get_logistics_orders
    ctx['all_products'] = get_all_products
    ctx['profit'] = final_profit
    ctx['packed_logistics_orders'] = get_packed_logistics_orders

    return ctx


@login_required(login_url="/ecom/login/")
def calculate_overdue_data(request):
    save_overdue_on_date()
    return HttpResponse("ok")


def save_overdue_on_date():
    todays_date = datetime.datetime.now()
    get_over_dues_total = 0
    get_order_details = OrderDetails.objects.select_related("credit").exclude(status="initiated").exclude(
        credit_status__in=["Failed", "Paid"])
    if get_order_details:
        from .views import get_order_details_return_cancel_amount
        from .views import get_order_details_total_due_amount_duplicate
        for obj in get_order_details:
            credit = 0
            if obj.credit:
                credit = obj.credit.actual_used
                # get order cancel and return total amount..
                total_cancel_return_amount = get_order_details_return_cancel_amount(obj.id)
                if total_cancel_return_amount:
                    credit = credit - total_cancel_return_amount
            if credit != 0:
                overdue_amount = get_order_details_total_due_amount_duplicate(obj.id)
                if overdue_amount:
                    get_over_dues_total += overdue_amount

        create_entry = OverDueData.objects.create(amount=get_over_dues_total, date=todays_date)
        # print("create_entry", create_entry)
    return True


# @query_debugger
def data_3(user):
    """
    Seller amount for particular seller, Total Product Details, Return confirm count, Total Over Due
    """
    ctx = {}
    get_seller_amount = 0
    get_total_product_details = 0
    get_return_confirm = 0
    get_over_dues_total = 0

    ecom_admin_group = user.groups.filter(name__in=['EcomAdmin', 'EcomTeam', 'EcomManager']).exists()
    if ecom_admin_group:
        todays_date = datetime.datetime.now()
        day_count = 3
        for i in (todays_date - datetime.timedelta(n) for n in range(day_count)):
            date_to_check = str(i)[:10]
            overdue_obj = OverDueData.objects.filter(date__startswith=date_to_check, re_status=True).order_by('id').last()
            if overdue_obj:
                get_over_dues_total = overdue_obj.amount
                break

    # Sellers Group
    else:
        seller_amount = SellerAccount.objects.filter(seller=user).aggregate(Sum('total_amount'))[
            'total_amount__sum']
        if seller_amount and seller_amount is not None:
            get_seller_amount = seller_amount

        return_confirm = ReturnOrder.objects.filter(verify_return_status="return_confirm",
                                                    payment_status="success",
                                                    order__product_details__user=user).count()
        if return_confirm and return_confirm is not None:
            get_return_confirm = return_confirm

        total_product_details = ProductDetails.objects.filter(user=user, re_status=True,
                                                              status__in=["APPROVED", "SUBMITTED", "REJECTED"]).count()
        if total_product_details and total_product_details is not None:
            get_total_product_details = total_product_details

    ctx['seller_amount'] = "{:12.2f}".format(get_seller_amount)
    ctx['total_product_details'] = get_total_product_details
    ctx['return_confirm'] = get_return_confirm
    ctx['over_dues_total'] = "{:12.2f}".format(get_over_dues_total)
    return ctx


# @query_debugger
def data_4(user):
    """
    No of product category, Submitted product count, Total users, Total buyer, Total convenient fees, Total Order Amount
    & Total Seller Amount
    """
    ctx = {}
    submitted_products_count = 0
    product_category = 0
    total_users = 0
    total_buyer = 0
    order_amount = 0
    final_seller_amount = 0
    convenient_fees = 0

    # EcomAdmin & EcomTeam & EcomManager Group
    ecom_admin_group = user.groups.filter(name__in=['EcomAdmin', 'EcomTeam', 'EcomManager']).exists()
    if ecom_admin_group:
        product_category = Product.objects.values_list('base_category__sub_category__category').distinct().count()
        if product_category and product_category is not None:
            product_category = product_category
        submitted_products = set(
            ProductDetails.objects.filter(status="SUBMITTED", re_status=True, ).values_list('product_id', flat=True))
        submitted_products_count = len(submitted_products)

        total_users = User.objects.exclude(groups__name="Seller").exclude(is_staff="True").exclude(
            is_superuser="True").count()
        if total_users:
            total_users = total_users

        total_buyer = OrderDetails.objects.exclude(status='initiated').values_list("user",
                                                                                   flat=True).distinct().count()
        if total_buyer:
            total_buyer = total_buyer

        df = pd.DataFrame(
            SellerAmount.objects.values_list("total_amount", "return_amount", "cancel_amount",
                                             "manually_cancelled_amount",
                                             "undelivered_amount"),
            columns=["total_amount", "return_amount", "cancel_amount", "manually_cancelled_amount",
                     "undelivered_amount"])
        seller_amount = df["total_amount"].sum()
        return_amount = df["return_amount"].sum()
        cancel_amount = df["cancel_amount"].sum()
        undelivered_amount = df["undelivered_amount"].sum()
        manually_cancelled_amount = df["manually_cancelled_amount"].sum()

        final_seller_amount = seller_amount - (
                return_amount + cancel_amount + manually_cancelled_amount + undelivered_amount)
        convenient_fees = 0

        conv_charges_order_details = \
            OrderDetails.objects.exclude(status="initiated").exclude(convenient_fees=None).aggregate(
                Sum('convenient_fees__amount'))[
                'convenient_fees__amount__sum']
        if conv_charges_order_details:
            convenient_fees += conv_charges_order_details

        conv_charges_order = \
            Order.objects.exclude(status="initiated").exclude(convenient_fees=None).aggregate(
                Sum('convenient_fees__amount'))[
                'convenient_fees__amount__sum']
        if conv_charges_order:
            convenient_fees += conv_charges_order

        order_amount = Order.objects.exclude(status__in=["initiated", "cancelled", "return", "undelivered"]).aggregate(
            Sum('total_amount'))[
            'total_amount__sum']

    # Sellers Group
    else:
        seller_amount = SellerAmount.objects.filter(seller=user).aggregate(Sum('total_amount'))[
            'total_amount__sum']
        if seller_amount:
            final_seller_amount = seller_amount

    ctx['product_category'] = product_category
    ctx['submitted_products'] = submitted_products_count
    ctx['total_users'] = total_users
    ctx['total_buyer'] = total_buyer
    ctx['convenient_fees'] = "{:12.2f}".format(convenient_fees)
    ctx['order_amount'] = order_amount
    ctx['final_seller_amount'] = "{:12.2f}".format(final_seller_amount)

    return ctx


# @query_debugger
def data_5(user):
    """
    Total repayment amount, Total Credit, Total return request, Total net receivables, Total shipping fees
    """
    ctx = {}
    get_repayment_amount = 0
    get_return_request = 0
    get_total_credit = 0
    shipping_fees = 0
    net_receviables = 0

    # EcomAdmin & EcomTeam & EcomManager Group
    ecom_admin_group = user.groups.filter(name__in=['EcomAdmin', 'EcomTeam', 'EcomManager']).exists()
    if ecom_admin_group:
        df = pd.DataFrame(list(
            Credit.objects.exclude(status="initiated").order_by('created_date').values_list("user", "used_credit")),
                          columns=["user", "used_credit"])
        total_credit = df.groupby('user').tail(1)
        net_receviables = total_credit['used_credit'].sum()

        repayment_amount = Payment.objects.filter(product_type="Ecom", status='success',
                                                  category="Repayment").aggregate(Sum('amount'))['amount__sum']
        if repayment_amount:
            get_repayment_amount = repayment_amount
        customer_obj = \
            Order.objects.exclude(status__in=["initiated", "cancelled"]).aggregate(Sum('delivery_charges__amount'))[
                'delivery_charges__amount__sum']
        if customer_obj:
            shipping_fees = customer_obj

        return_request = ReturnOrder.objects.filter(verify_return_status="return_request",
                                                    payment_status="success").count()
        if return_request and return_request is not None:
            get_return_request = return_request

    total_credit = net_receviables + get_repayment_amount

    ctx['total_repayment_amount'] = "{:12.2f}".format(get_repayment_amount)
    ctx['total_credit'] = "{:12.2f}".format(total_credit)
    ctx['return_request'] = get_return_request
    ctx['net_receivables'] = "{:12.2f}".format(net_receviables)
    ctx['shipping_fees'] = "{:12.2f}".format(shipping_fees)
    return ctx


# @query_debugger
def data_6(user):
    """
    Total Net Pay, Total Net Profit,  Total Wallet Amount, Total in_transit Logistics Orders
    """

    ctx = {}
    total = 0
    net_pay = 0
    get_wallet_amount = 0
    get_in_transit_logistics_orders = 0

    # EcomAdmin & EcomTeam & EcomManager Group
    ecom_admin_group = user.groups.filter(name__in=['EcomAdmin', 'EcomTeam', 'EcomManager']).exists()

    if ecom_admin_group:

        df = read_frame(
            UserWallet.objects.exclude(status__in=["initiated", "return_delivery_charge_initiated"]).order_by(
                'created_date'), fieldnames=['user__username', 'balance_amount'])
        a = df.groupby('user__username').tail(1)
        get_wallet_amount = a['balance_amount'].sum()

        from .views import net_profit_amount

        total = net_profit_amount()

        try:
            seller_wallet_obj = SellerWallet.objects.filter(is_wallet_consumed=False).order_by("seller",
                                                                                               "-id").distinct(
                "seller")
            if seller_wallet_obj:
                for obj in seller_wallet_obj:
                    if obj.balance_amount:
                        net_pay += obj.balance_amount

        except Exception as e:
            print("Inside Exception of data_6", e)

        in_transit_logistics_orders = LogisticOrder.objects.filter(order__status='in_transit', status='success',
                                                                   order_mode='Delivery').count()
        if in_transit_logistics_orders:
            get_in_transit_logistics_orders = in_transit_logistics_orders

    # Sellers Group
    else:
        in_transit_logistics_orders = LogisticOrder.objects.filter(order__product_details__user=user, status='success',
                                                                   order__status='in_transit',
                                                                   order_mode='Delivery').count()
        if in_transit_logistics_orders:
            get_in_transit_logistics_orders = in_transit_logistics_orders

    ctx['net_pay'] = "{:12.2f}".format(net_pay)
    ctx["total"] = "{:12.2f}".format(total)
    ctx['net_profit'] = "{:12.2f}".format(total)
    ctx['wallet_amount'] = "{:12.2f}".format(get_wallet_amount)
    ctx['in_transit_logistics_orders'] = get_in_transit_logistics_orders
    return ctx


# @query_debugger
def data_7(user):
    """
    Total sellers count, Total bill amount, Total payment received, Total pending amount
    """
    ctx = {}
    get_sellers_count = 0
    total_amount = 0
    total_paid_amount = 0
    total_pending_amount = 0

    # Sellers Group
    ecom_seller_group = user.groups.filter(name='Seller').exists()
    ecom_admin_group = user.groups.filter(name__in=['EcomAdmin', 'EcomTeam', 'EcomManager']).exists()

    if ecom_seller_group:
        actual_invoice_amount = \
            SellerWallet.objects.filter(seller__username=user).aggregate(Sum('actual_invoice_amount'))[
                'actual_invoice_amount__sum']
        if actual_invoice_amount:
            total_amount = actual_invoice_amount

        payment_amount = \
            SellerWallet.objects.filter(seller__username=user, payment_status=True).aggregate(Sum('actual_paid'))[
                'actual_paid__sum']
        if payment_amount:
            total_paid_amount = payment_amount

        total_pending_amount = total_amount - total_paid_amount

    # EcomAdmin & EcomTeam & EcomManager Group
    if ecom_admin_group:
        sellers_count = Product.objects.distinct("seller").count()
        if sellers_count and sellers_count is not None:
            get_sellers_count = sellers_count

    ctx['sellers_count'] = get_sellers_count
    ctx['bill_amount'] = "{:12.2f}".format(total_amount)
    ctx['total_payment_received'] = "{:12.2f}".format(total_paid_amount)
    ctx['net_due'] = "{:12.2f}".format(total_pending_amount)
    return ctx


# @query_debugger
def data_8(user):
    """
    Total Penalty, Total unused credit
    """
    ctx = {}
    total_penalty = 0
    total_unused_credit = 0
    ecom_admin_group = user.groups.filter(name__in=['EcomAdmin', 'EcomTeam', 'EcomManager']).exists()
    ecom_seller_group = user.groups.filter(name__in=['Seller']).exists()
    from .views import get_seller_total_penalty_amount
    from .views import return_lats_unused_credit_sellers

    # Sellers Group
    if ecom_seller_group:
        user_id = user.id
        penalty = get_seller_total_penalty_amount(user_id)
        if penalty:
            penalty_amount = penalty
            if penalty_amount:
                total_penalty = penalty_amount

    # EcomAdmin & EcomTeam & EcomManager Group
    if ecom_admin_group:
        seller_id = ProductDetails.objects.values_list("user__id", flat=True).distinct()
        if seller_id:
            for user_id in seller_id:
                penalty = get_seller_total_penalty_amount(user_id)
                if penalty:
                    penalty_amount = penalty
                    total_penalty += penalty_amount

        data = return_lats_unused_credit_sellers()
        if data:
            # total_unused_credit = sum(data.values())
            total_unused_credit = sum(data['credit'])
    ctx["total_penalty"] = "{:12.2f}".format(total_penalty)
    ctx["total_unused_credit"] = "{:12.2f}".format(total_unused_credit)
    return ctx


# @query_debugger
def data_9(user):
    """
    Total Placed & pending Orders Count, Total COD Count, Total Payment Amount
    """
    placed_pending_count = 0
    payment_amount = 0
    get_cod_order_count = 0
    get_cod_order_amount = 0
    ctx = {}

    # EcomAdmin & EcomTeam & EcomManager Group
    ecom_admin_group = user.groups.filter(name__in=['EcomAdmin', 'EcomTeam', 'EcomManager']).exists()
    if ecom_admin_group:
        status_list = ['placed', 'placed_pending', 'accepted', 'packing', 'packed', 's_dispatched', 'pending',
                       'shipped', 'in_transit', 'dispatched', 'delivered', 'return', 'exchange', 'lost',
                       'return_cancelled']

        placed_pending_orders = OrderDetails.objects.filter(status="pending").count()
        if placed_pending_orders:
            placed_pending_count = placed_pending_orders

        order_count = OrderDetails.objects.filter(order_id__status__in=status_list, payment_mode="cod").exclude(
            status='initiated').distinct().count()
        if order_count:
            get_cod_order_count = order_count

        payment_amount_qs = Payment.objects.filter(status="success", product_type="Ecom",
                                                   type__in=["App", "Account"]).aggregate(Sum('amount'))['amount__sum']
        if payment_amount_qs is not None:
            payment_amount = payment_amount_qs

        cod_order_details_obj = OrderDetails.objects.filter(order_id__status__in=status_list,
                                                            payment_mode="cod").exclude(status='initiated').distinct()
        if cod_order_details_obj:
            total_delivery_charges = 0
            total_convenient_fees = 0
            cod_order_amount = 0
            for cod_order in cod_order_details_obj:
                cod_order_amount += cod_order.total_amount
                charges_data = cod_pay_amount(cod_order.id)
                total_delivery_charges += charges_data['delivery_charges']
                total_convenient_fees += charges_data['convenient_fees']
            cod_total_amount = cod_order_amount + total_delivery_charges + total_convenient_fees
            if cod_total_amount:
                get_cod_order_amount = cod_total_amount

    ctx["placed_pending_orders"] = placed_pending_count
    ctx['get_cod_order_count'] = get_cod_order_count
    ctx['payment_amount'] = "{:12.2f}".format(payment_amount)
    ctx['cod_order_amount'] = "{:12.2f}".format(get_cod_order_amount)

    return ctx


# for MFPL
def data_10(user):
    ctx = {}
    get_mfpl_packed_logistics_order_details = 0
    get_mfpl_placed_order = 0

    # EcomAdmin & EcomTeam & EcomManager Group
    ecom_admin_group = user.groups.filter(name__in=['EcomAdmin', 'EcomTeam', 'EcomManager']).exists()

    if ecom_admin_group:
        mfpl_packed_logistics_order_details = LogisticOrder.objects.filter(order_details__status='packed',
                                                                           status='success',
                                                                           order_mode='Delivery').count()
        if mfpl_packed_logistics_order_details:
            get_mfpl_packed_logistics_order_details = mfpl_packed_logistics_order_details

        mfpl_placed_order = OrderDetails.objects.filter(combine_order__status='placed').distinct().count()
        if mfpl_placed_order:
            get_mfpl_placed_order = mfpl_placed_order

    # Seller Group
    else:
        if user.username == "MFPL":
            mfpl_packed_logistics_order_details = LogisticOrder.objects.filter(status='success',
                                                                               order_details__status='packed',
                                                                               order_mode='Delivery').count()
            if mfpl_packed_logistics_order_details:
                get_mfpl_packed_logistics_order_details = mfpl_packed_logistics_order_details

            mfpl_placed_order = OrderDetails.objects.filter(combine_order__status='placed').distinct().count()
            if mfpl_placed_order:
                get_mfpl_placed_order = mfpl_placed_order
    ctx['packed_mfpl_logistics_order_details'] = get_mfpl_packed_logistics_order_details
    ctx['mfpl_placed_order'] = get_mfpl_placed_order
    return ctx


# for Recovery
def data_11(user):
    ctx = {}
    get_defaulter_count = 0
    get_recovery_count = 0
    get_defaulter_amount = 0
    get_recovery_amount = 0
    get_insentive_amount = 0
    get_insentive_amount_top = 0
    get_insentive_amount_name = None
    get_call_topper_count = 0
    get_call_topper_name = None
    get_call_count = 0
    # get todays month
    today = datetime.datetime.now()
    newdate = str(today)
    # for Recovery Team & Recovery Head
    recovery_team = user.groups.filter(name__in=['Recovery Team', 'Recovery Head']).exists()
    if recovery_team:
        # Defaulter Count
        defaulter_list = Recovery.objects.filter(recovery_status="No", user=user).count()
        if defaulter_list:
            get_defaulter_count = defaulter_list
        # Recovery Count
        recovery_list = Recovery.objects.filter(recovery_status="Yes", user=user,
                                                created_date__startswith=newdate[:7]).count()
        if recovery_list:
            get_recovery_count = recovery_list
        # Defaulter Amount
        defaulter_amount = \
        Recovery.objects.filter(recovery_status="No", user=user).aggregate(Sum('pending_installment_amount'))[
            'pending_installment_amount__sum']
        if defaulter_amount:
            get_defaulter_amount = defaulter_amount
        # Recovery Amount
        recovery_data1 = RecoveryInsentive.objects.filter(recovery__recovery_status="Yes", recovery__user=user,
                                                          created_date__startswith=newdate[:7])
        if recovery_data1:
            recovery_df = read_frame(recovery_data1,
                                     fieldnames=['insentive_amount', 'recovery_commission__commission_percent'])
            recovery_df['recovery_commission__commission_percent'] = recovery_df[
                'recovery_commission__commission_percent'].astype(float)
            recovery_df["recovery_amount"] = (recovery_df["insentive_amount"]) * 100 / (
                recovery_df['recovery_commission__commission_percent'])
            get_recovery_amount = recovery_df["recovery_amount"].sum()
        # Incentive Amount
        insentive_amount = \
        RecoveryInsentive.objects.filter(recovery__user=user, created_date__startswith=newdate[:7]).aggregate(
            Sum('insentive_amount'))['insentive_amount__sum']
        if insentive_amount:
            get_insentive_amount = insentive_amount
        # Call Count
        call_count = RecoveryCallHistory.objects.filter(calling_status='Yes', recovery__user=user,
                                                        created_date__startswith=newdate[:7]).count()
        if call_count:
            get_call_count = call_count
    # For Recovery Supervisor  & EcomAdmin
    recovery_supervisor = user.groups.filter(name__in=['Recovery Supervisor', 'EcomAdmin']).exists()
    if recovery_supervisor:
        # Defaulter Count
        defaulter_list = Recovery.objects.filter(recovery_status="No").count()
        if defaulter_list:
            get_defaulter_count = defaulter_list
        # Recovery Count
        recovery_list = Recovery.objects.filter(recovery_status="Yes", created_date__startswith=newdate[:7]).count()
        if recovery_list:
            get_recovery_count = recovery_list
        # Defaulter Amount
        defaulter_amount = Recovery.objects.filter(recovery_status="No").aggregate(Sum('pending_installment_amount'))[
            'pending_installment_amount__sum']
        if defaulter_amount:
            get_defaulter_amount = defaulter_amount
        # Recovery Amount
        recovery_data1 = RecoveryInsentive.objects.filter(recovery__recovery_status="Yes",
                                                          created_date__startswith=newdate[:7])
        if recovery_data1:
            recovery_df = read_frame(recovery_data1,
                                     fieldnames=['insentive_amount', 'recovery_commission__commission_percent'])
            recovery_df['recovery_commission__commission_percent'] = recovery_df[
                'recovery_commission__commission_percent'].astype(float)
            recovery_df["recovery_amount"] = (recovery_df["insentive_amount"]) * 100 / (
                recovery_df['recovery_commission__commission_percent'])
            get_recovery_amount = recovery_df["recovery_amount"].sum()
        # Incentive Amount
        insentive_amount = \
        RecoveryInsentive.objects.filter(created_date__startswith=newdate[:7]).aggregate(Sum('insentive_amount'))[
            'insentive_amount__sum']
        if insentive_amount:
            get_insentive_amount = insentive_amount
        # Call Count
        call_count = RecoveryCallHistory.objects.filter(calling_status='Yes',
                                                        created_date__startswith=newdate[:7]).count()
        if call_count:
            get_call_count = call_count
    # for Recovery Supervisor, EcomAdmin, Recovery Team & Recovery Head
    recovery_group = user.groups.filter(
        name__in=['Recovery Supervisor', 'EcomAdmin', 'Recovery Team', 'Recovery Head']).exists()
    if recovery_group:
        recovery_data2 = RecoveryInsentive.objects.filter(created_date__startswith=newdate[:7])
        if recovery_data2:
            recovery_info_df = read_frame(recovery_data2, fieldnames=['recovery__user__username', 'insentive_amount',
                                                                      'recovery_commission__commission_percent',
                                                                      'created_date'])
            recovery_info_df['recovery_commission__commission_percent'] = recovery_info_df[
                'recovery_commission__commission_percent'].astype(float)
            recovery_info_df["insentive_amount"] = (recovery_info_df["insentive_amount"]) * 100 / (
                recovery_info_df['recovery_commission__commission_percent'])
            insentive_amount = recovery_info_df.groupby(recovery_info_df['recovery__user__username'])[
                'insentive_amount'].sum().sort_index(axis=0)
            recovery_info_df["insentive_amount"] = recovery_info_df["insentive_amount"].round(1)
            data = insentive_amount.sort_values(ascending=False)
            data_dict = dict(data)
            name_insentive_top = pd.Series(data_dict, index=data_dict.keys())
            name_insentive_top_reciover = name_insentive_top.sort_values(ascending=False)
            name_insentive_top_reco = list(name_insentive_top_reciover.keys())[0]
            insentive_amount_top = data_dict.get(name_insentive_top_reco)
            get_insentive_amount_top = insentive_amount_top
            get_insentive_amount_name = name_insentive_top_reco
        user_id_list = User.objects.filter(groups__name="Recovery Team", is_active='t')
        calling_top = {}
        if user_id_list:
            for i in user_id_list:
                call_counts = RecoveryCallHistory.objects.filter(calling_status='Yes', recovery__user__username=i,
                                                                 created_date__startswith=newdate[:7]).count()
                if call_counts:
                    calling_top.update({i.username: call_counts})
                    calling_top_pd = pd.Series(calling_top, index=calling_top.keys())
                    user_calling_top = calling_top_pd.sort_values(ascending=False)
                    name_user = list(user_calling_top.keys())[0]
                    get_call_topper_count = user_calling_top.get(name_user)
                    get_call_topper_name = name_user
    ctx['defaulter_count'] = get_defaulter_count
    ctx['recovery_count'] = get_recovery_count
    ctx['defaulter_amount'] = "{:12.2f}".format(get_defaulter_amount)
    ctx['recovery_amount'] = "{:12.2f}".format(get_recovery_amount)
    ctx['insentive_amount'] = "{:12.2f}".format(get_insentive_amount)
    ctx["insentive_amount_top"] = "{:12.2f}".format(get_insentive_amount_top)
    ctx["insentive_amount_name"] = get_insentive_amount_name
    ctx["call_topper_count"] = get_call_topper_count
    ctx["call_topper_name"] = get_call_topper_name
    ctx["call_count"] = get_call_count
    return ctx


def cod_pay_amount(order_details_id):
    data_dict = {}
    total_delivery_charges = 0
    total_convenient_fees = 0
    orders = OrderDetails.objects.filter(id=order_details_id).last().order_id.exclude(
        status__in=["cancelled", "undelivered", "initiated"]).values_list("id", flat=True)
    if orders:
        total_delivery_charges = \
            Order.objects.filter(id__in=orders).exclude(delivery_charges=None).aggregate(
                Sum('delivery_charges__amount'))[
                'delivery_charges__amount__sum']
        if total_delivery_charges:
            total_delivery_charges = total_delivery_charges
        else:
            total_delivery_charges = 0
        total_convenient_fees = \
            Order.objects.filter(id__in=orders).exclude(convenient_fees=None).aggregate(Sum('convenient_fees__amount'))[
                'convenient_fees__amount__sum']
        if total_convenient_fees:
            total_convenient_fees = total_convenient_fees
        else:
            total_convenient_fees = 0
        data_dict = {
            "delivery_charges": total_delivery_charges,
            "convenient_fees": total_convenient_fees
        }
    else:
        data_dict = {
            "delivery_charges": 0,
            "convenient_fees": 0
        }
    return data_dict



def data_12(user):

    try:
        ecom_admin = user.groups.filter(name__in=['EcomAdmin']).exists()
        ctx = {}
        reject_count = 0
        approved_count = 0
        total_count = 0
        loan_submitted_count = 0
        if ecom_admin:
            reject_count = LoanApplication.objects.values_list('status', 'id').filter(
                status="REJECTED").count()
            approved_count = LoanApplication.objects.values_list('status', 'id').filter(
                status__in=["COMPLETED", "APPROVED"]).count()

            total_count = LoanApplication.objects.values_list('id').count()
            loan_submitted_count = LoanApplication.objects.values_list('status', 'id').filter(
                status__in=["SUBMITTED", "PROCESS"]).count()

        ctx['rejected_loan_application'] = reject_count
        ctx['approved_loan_application'] = approved_count
        ctx['all_loan_application'] = total_count
        ctx['new_loan_submitted'] = loan_submitted_count
        return ctx
    except:
        pass






def data_13(user):

    try:
        ecom_admin = user.groups.filter(name__in=['EcomAdmin']).exists()
        ctx = {}
        due_amount = 0
        disbursed_amount = 0
        completed_loan_amount = 0
        completed_loan_profit = 0
        completed_loan_count = 0
        installment = 0
        if ecom_admin:
            completed_loan_count = LoanApplication.objects.filter(status="COMPLETED", user_status="Accept").count()
            if completed_loan_count:
                due_amount = LoanApplication.objects.filter(
                    status="APPROVED",disbursed_amount_wallet__status="loan_disbursed").aggregate(Sum('approved_amount'))[
                    'approved_amount__sum']
                disbursed = LoanApplication.objects.filter(status__in=["COMPLETED", "APPROVED"],disbursed_amount_wallet__status="loan_disbursed").aggregate(Sum('approved_amount'))[
                    'approved_amount__sum']
                if disbursed > 0:
                    disbursed_amount = disbursed * 90 / 100

                completed_loan_amount_1 =  LoanApplication.objects.filter(status="COMPLETED").aggregate(Sum('approved_amount'))[
                    'approved_amount__sum']
                install = LoanApplication.objects.filter(status="APPROVED")
                if install:
                    for i in install:
                        amount = i.repayment_amount.filter(status="success").aggregate(Sum('amount'))[
                        'amount__sum']
                        if amount != None and  amount:
                            installment += amount

                    if completed_loan_amount_1 != None and completed_loan_amount_1:
                        completed_loan_amount = completed_loan_amount_1
                        completed_loan_profit = completed_loan_amount * 10 / 100




        ctx['due_amount'] = due_amount
        ctx['disbursed_amount'] = disbursed_amount
        ctx['completed_loan_amount'] = completed_loan_amount
        ctx['installment'] = installment
        ctx['completed_loan_profit'] = completed_loan_profit
        ctx['completed_loan_count'] = completed_loan_count
        return ctx
    except:
        pass




def data_14(user):

    try:
        ecom_admin = user.groups.filter(name__in=['EcomAdmin']).exists()
        ctx = {}
        overdue_amount = 0
        net_profit = 0
        delinquency = 0
        overdue_count = 0
        completed_loan_profit = 0
        disbursed_amount = 0

        if ecom_admin:
            loan_overdue=get_loan_details_total_due_amount()
            if loan_overdue:
                overdue_amount = loan_overdue["overdue_amount"]
                overdue_count = loan_overdue["overdue_count"]
                completed_loan_amount_1 = \
                LoanApplication.objects.filter(status="COMPLETED").aggregate(Sum('approved_amount'))[
                    'approved_amount__sum']
                if completed_loan_amount_1 != None and completed_loan_amount_1:
                    completed_loan_profit = completed_loan_amount_1 * 10 / 100
                net_profit = completed_loan_profit - overdue_amount

                disbursed = \
                LoanApplication.objects.filter(status__in=["COMPLETED", "APPROVED"]).aggregate(Sum('approved_amount'))[
                    'approved_amount__sum']
                if disbursed is not None:
                    if disbursed > 0:
                        disbursed_amount = disbursed * 90 / 100

                    delinquency = overdue_amount * 100 / disbursed_amount

        ctx['overdue_amount'] = overdue_amount
        ctx['net_profit'] = net_profit
        ctx['delinquency'] = round(delinquency,2)
        ctx['overdue_count'] = overdue_count
        return ctx
    except:
        pass


def get_loan_details_total_due_amount():
    total_overdue_count = 0
    total_overdue_amount = 0
    ctx = {}
    loan_obj = LoanApplication.objects.filter(status="APPROVED", user_status="Accept")
    if loan_obj:
        for loan in loan_obj:
            overdue_count = 0
            overdue_amount = 0
            overdue_status = 0
            from LoanAppData.loan_funcations import loan_amount_slab
            slabs = loan_amount_slab(loan.id)
            if slabs:
                for dat in slabs:
                    if dat["pay_status"] == "unpaid" and dat["is_overdue"] == "Yes":
                        overdue_status = 1
                        overdue_amount = dat["remaining_amount"]
                if overdue_status == 1:
                    total_overdue_count += 1
                    total_overdue_amount += overdue_amount
    ctx["overdue_count"] = total_overdue_count
    ctx["overdue_amount"] = total_overdue_amount
    return ctx


def data_15(user):
    order_count = 0
    try:
        ecom_admin = user.groups.filter(name__in=['EcomAdmin']).exists()
        ctx = {}
        if ecom_admin:

            order_phonepe = Order.objects.filter(platform_type="PhonePe", status="cancelled").values_list('id').count()
            if order_phonepe:
                order_count = order_phonepe


        ctx['phone_pe'] = order_count

        return ctx
    except:
        pass





