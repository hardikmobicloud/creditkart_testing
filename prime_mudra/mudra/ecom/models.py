from Payments.models import Payment
from User.models import Address
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.db import models
from mudra import ecom_constants
from django.db.models import Sum
from django_currentuser.db.models import CurrentUserField
from django.contrib.postgres.fields import JSONField
from postgres_copy import CopyManager


class Category(models.Model):
    """
    this model is use to save category of product.
    """
    name = models.CharField(max_length=250, null=True, blank=True, db_index=True)
    description = models.TextField(blank=True, null=True)
    status = models.BooleanField(default=False)
    re_status = models.BooleanField(default=True)
    priority = models.IntegerField(default=None, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="category_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="category_updated_by_new", null=True, blank=True,
                                      editable=False)

    def __str__(self):
        return '%s %s' % (self.name, self.status)


class SubCategory(models.Model):
    """
    this table is use to save sub category of product.
    """
    name = models.CharField(max_length=250, null=True, blank=True, db_index=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="products_category", null=True,
                                 blank=True)
    description = models.TextField(blank=True, null=True)
    status = models.BooleanField(default=False)
    re_status = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="sub_category_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="sub_category_updated_by_new", null=True, blank=True,
                                      editable=False)

    def __str__(self):
        return '%s %s' % (self.name, self.category)


class BaseCategory(models.Model):
    """
    This table is use to save base category of product.
    """
    base_name = models.CharField(max_length=100, blank=True, null=True, db_index=True)
    sub_category = models.ForeignKey(SubCategory, on_delete=models.CASCADE, related_name="products_sub_category_fk",
                                     null=True, blank=True)
    re_status = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="base_category_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="base_category_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)

    def __str__(self):
        return '%s %s' % (self.base_name, self.sub_category)


class CGST(models.Model):
    """
    this model is use to save cgst on product
    """
    percentage = models.FloatField(default=0, null=True, blank=True, db_index=True)
    description = models.TextField(blank=True, null=True)
    status = models.BooleanField(default=False)
    re_status = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="cgst_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="cgst_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)

    def __str__(self):
        return '%s %s' % (self.percentage, self.status)


class SGST(models.Model):
    """
    this model is use to save sgst on product
    """
    percentage = models.FloatField(default=0, null=True, blank=True, db_index=True)
    description = models.TextField(blank=True, null=True)
    status = models.BooleanField(default=False)
    re_status = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="sgst_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="sgst_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)

    def __str__(self):
        return '%s %s' % (self.percentage, self.status)


class Size(models.Model):
    """
    this table is use to save size of product.
    """
    name = models.CharField(max_length=256, null=True, blank=True, db_index=True)
    description = models.TextField(blank=True, null=True)
    date = models.DateTimeField(null=True, blank=True)
    status = models.BooleanField(default=False)
    re_status = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="size_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="size_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)

    def __str__(self):
        return '%s %s' % (self.name, self.status)


class Colour(models.Model):
    """
    this table is use to save color of product.
    """
    name = models.CharField(max_length=256, null=True, blank=True, db_index=True)
    description = models.TextField(blank=True, null=True)
    date = models.DateTimeField(null=True, blank=True)
    status = models.BooleanField(default=False)
    re_status = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="colour_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="colour_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)

    def __str__(self):
        return '%s %s' % (self.name, self.status)


class Product(models.Model):
    """
    this model is use to save product with unique product id.
    """
    name = models.CharField(max_length=250, null=True, blank=True, db_index=True)
    product_id = models.CharField(max_length=250, null=True, blank=True, db_index=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="product_category", null=True,
                                 blank=True)
    sub_category = models.ForeignKey(SubCategory, on_delete=models.CASCADE, related_name="product_sub_category",
                                     null=True, blank=True)
    base_category = models.ForeignKey(BaseCategory, on_delete=models.CASCADE, related_name="base_category", null=True,
                                      blank=True)
    re_status = models.BooleanField(default=True)
    priority = models.IntegerField(default=None, null=True, blank=True)
    dashboard_status = models.BooleanField(default=False)
    seller = models.ForeignKey(User, on_delete=models.CASCADE, related_name="product_seller", blank=True, null=True)
    date = models.DateTimeField(null=True, blank=True)
    description = models.TextField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="product_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="product_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)

    def __str__(self):
        return '%s %s' % (self.name, self.base_category)


class Unit(models.Model):
    """
    This model is use to save unit of entity.
    """
    name = models.CharField(max_length=256, null=True, blank=True, db_index=True)
    description = models.TextField(blank=True, null=True)
    date = models.DateTimeField(null=True, blank=True)
    re_status = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="unit_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="unit_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)

    def __str__(self):
        return '%s %s' % (self.name, self.re_status)


class ProductDetails(models.Model):
    '''
    To store details of product of ecom and include unit_combination function.
    '''
    product_id = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="product_details", null=True,
                                   blank=True)
    unique_product_details_id = models.CharField(max_length=50, null=True, blank=True)
    seller_amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    mrp = models.FloatField(default=0, null=True, blank=True)
    discount = models.FloatField(default=0, null=True, blank=True)
    sku = models.CharField(max_length=256, null=True, blank=True)
    HSN = models.CharField(max_length=256, null=True, blank=True)
    size_id = models.ForeignKey(Size, on_delete=models.CASCADE, blank=True, null=True, related_name="product_size")
    colour_id = models.ForeignKey(Colour, on_delete=models.CASCADE, blank=True, null=True,
                                  related_name="product_colour")
    unit_count = models.CharField(max_length=50, default=0, null=True, blank=True, db_index=True)
    unit = models.ForeignKey(Unit, null=True, on_delete=models.CASCADE, blank=True, db_index=True)
    description = models.TextField(blank=True, null=True)
    specification = JSONField(null=True, blank=True)
    height = models.CharField(max_length=250, null=True, blank=True)
    Length = models.CharField(max_length=250, null=True, blank=True)
    width = models.CharField(max_length=250, null=True, blank=True)
    weight = models.FloatField(default=0, null=True, blank=True)
    sgst = models.ForeignKey(SGST, on_delete=models.CASCADE, related_name="product_SGST", null=True, blank=True)
    cgst = models.ForeignKey(CGST, on_delete=models.CASCADE, related_name="product_CGST", null=True, blank=True)
    date = models.DateTimeField(null=True, blank=True)
    re_status = models.BooleanField(default=True)
    reason = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=256, null=True, blank=True, choices=ecom_constants.APPROVAL_STATUS)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="seller_user", blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    verify_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE,
                                  related_name="product_details_verify_by")
    created_by_new = CurrentUserField(related_name="product_details_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="product_details_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)

    objects = CopyManager()

    def __str__(self):
        return '%s %s' % (self.product_id, self.status)

    def unit_combination(product_details_obj):
        """
        This function returns unit combination
        """
        print("inside this function")
        unit_combination = ""
        if product_details_obj:
            print("product_details_obj", product_details_obj)
            unit_count = ""
            unit = ""
            if product_details_obj.unit_count:
                unit_count = str(product_details_obj.unit_count)
            if product_details_obj.unit:
                unit = str(product_details_obj.unit.name)
            print("unit_count", unit_count)
            print("unit", unit)
            if None not in (unit_count, unit):
                unit_combination = unit_count + " " + unit
            print('unit combo---------------------', unit_combination)
        return unit_combination


class Stock(models.Model):
    """
    This model is used to save stock data of product.
    """
    product_details = models.ForeignKey(ProductDetails, on_delete=models.CASCADE, related_name="product_details_stock",
                                        null=True, blank=True)
    stock_add = models.IntegerField(default=0, null=True, blank=True, db_index=True)
    stock_sub = models.IntegerField(default=0, null=True, blank=True, db_index=True)
    stock_balance = models.IntegerField(default=0, null=True, blank=True, db_index=True)
    status = models.BooleanField(default=False, db_index=True)
    re_status = models.BooleanField(default=True)
    change_by = models.CharField(max_length=50, null=True, blank=True, choices=ecom_constants.STOCK_CHANGE_BY)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="stock_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="stock_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)


class Cart(models.Model):
    """
    This model is use to product details all information in cart.
    """
    product_details = models.ForeignKey(ProductDetails, on_delete=models.CASCADE, related_name="product_details_cart",
                                        null=True, blank=True)
    quantity = models.IntegerField(default=0, null=True, blank=True, db_index=True)
    amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    total_amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    date = models.DateTimeField(null=True, blank=True)
    status = models.BooleanField(default=False)
    re_status = models.BooleanField(default=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="cart_user", blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="cart_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="cart_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)


class Credit(models.Model):
    """
    this table is use to save credit information of user.
    """
    credit = models.FloatField(default=0, null=True, blank=True, db_index=True)
    used_credit = models.FloatField(default=0, null=True, blank=True, db_index=True)
    balance_credit = models.FloatField(default=0, null=True, blank=True, db_index=True)
    actual_used = models.FloatField(default=0, null=True, blank=True, db_index=True)
    repayment_add = models.FloatField(default=0, null=True, blank=True, db_index=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="credit_user", blank=True, null=True)
    status = models.CharField(max_length=50, null=True, blank=True, choices=ecom_constants.CREDIT_DATA_STATUS)
    type = models.CharField(max_length=30, null=True, blank=True, choices=ecom_constants.CREDIT_TYPE)
    created_date = models.DateTimeField(auto_now_add=True)
    re_status = models.BooleanField(default=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="credit_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="credit_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)

    def __str__(self):
        return '%s %s' % (self.actual_used, self.user)


class DeliveryCharges(models.Model):
    """
    this model is use to save delivery charge of product.
    """
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="product_delivery_charge",
                                null=True, blank=True)
    amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    re_status = models.BooleanField(default=True)
    status = models.CharField(max_length=256, null=True, blank=True, choices=ecom_constants.APPROVAL_STATUS)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    created_by_new = CurrentUserField(related_name="delivery_charges_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="delivery_charges_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)

    def __str__(self):
        return '%s %s' % (self.product, self.amount)


class ProductDetailsAmount(models.Model):
    """
        this model is product detail amount with all amount data.

    """
    product_details = models.ForeignKey(ProductDetails, on_delete=models.CASCADE,
                                        related_name="product_details_amount",
                                        null=True, blank=True)
    seller_amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    admin_amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    mrp = models.FloatField(default=0, null=True, blank=True)
    discount = models.FloatField(default=0, null=True, blank=True)
    sgst = models.ForeignKey(SGST, on_delete=models.CASCADE, related_name="product_details_SGST",
                             null=True, blank=True)
    cgst = models.ForeignKey(CGST, on_delete=models.CASCADE, related_name="product_details_CGST",
                             null=True, blank=True)
    date = models.DateTimeField(null=True, blank=True)
    re_status = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    created_by_new = CurrentUserField(related_name="product_details_amount_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="product_details_amount_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)

    def __str__(self):
        return '%s %s' % (self.product_details, self.admin_amount)


class ProductMinMaxAMount(models.Model):
    """
    this model is  use to product min max amount of product of order.
    """
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="product_min_max_amount",
                                null=True, blank=True)
    min_amount = models.FloatField(default=0, null=True, blank=True)
    max_amount = models.FloatField(default=0, null=True, blank=True)
    min_product_details = models.ForeignKey(ProductDetails, on_delete=models.CASCADE,
                                            related_name="min_product_details",
                                            null=True, blank=True)
    max_product_details = models.ForeignKey(ProductDetails, on_delete=models.CASCADE,
                                            related_name="max_product_details",
                                            null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    re_status = models.BooleanField(default=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="product_min_max_amount_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="product_min_max_amount_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)


class ProductMinMaxDiscount(models.Model):
    """
    This model is use save product min max discount of product.
    """
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="product_min_max_discount",
                                null=True, blank=True)
    min_discount = models.FloatField(default=0, null=True, blank=True)
    max_discount = models.FloatField(default=0, null=True, blank=True)
    min_product_details = models.ForeignKey(ProductDetails, on_delete=models.CASCADE,
                                            related_name="min_discount_product_details",
                                            null=True, blank=True)
    max_product_details = models.ForeignKey(ProductDetails, on_delete=models.CASCADE,
                                            related_name="max_discount_product_details",
                                            null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    re_status = models.BooleanField(default=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="product_min_max_discount_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="product_min_max_discount_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)


class ConvenientFees(models.Model):
    """
    this model is use to save convenient fees of the order.
    """
    amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    re_status = models.BooleanField(default=True, db_index=True)
    order_type = models.CharField(max_length=20, null=True, blank=True,
                                  choices=ecom_constants.CONVENIENT_FEES_ORDER_TYPE)
    status = models.CharField(max_length=20, null=True, blank=True, choices=ecom_constants.CONVENIENT_FEE_STATUS)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    created_by_new = CurrentUserField(related_name="convenient_fee_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="convenient_fee_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)

    def __str__(self):
        return '%s %s' % (self.amount, self.status)


class Order(models.Model):
    """
    This table is used to save order data
    """
    order_id = models.CharField(max_length=250, null=True, blank=True)
    product_details = models.ForeignKey(ProductDetails, on_delete=models.CASCADE, related_name="product_details_order",
                                        null=True, blank=True)
    delivery_charges = models.ForeignKey(DeliveryCharges, on_delete=models.CASCADE,
                                         related_name="product_delivery_charge",
                                         null=True, blank=True)
    unique_order_id = models.CharField(max_length=250, null=True, blank=True, db_index=True)
    quantity = models.IntegerField(default=0, null=True, blank=True, db_index=True)
    order_details_amount = models.ForeignKey(ProductDetailsAmount, on_delete=models.CASCADE,
                                             related_name="order_details_price",
                                             null=True, blank=True)
    convenient_fees = models.ForeignKey(ConvenientFees, on_delete=models.CASCADE,
                                        related_name="orders_convenient_fees",
                                        blank=True, null=True)
    platform_type = models.CharField(max_length=25, null=True, blank=True, choices=ecom_constants.PLATFORM_TYPE)
    is_refunded = models.BooleanField(default=False)
    amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    total_amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    date = models.DateTimeField(null=True, blank=True)
    re_status = models.BooleanField(default=True)
    combination_type = models.CharField(max_length=25, null=True, blank=True, choices=ecom_constants.COMBINATION_TYPE)
    is_stock_return = models.BooleanField(default=False, null=True, blank=True)
    status = models.CharField(max_length=256, null=True, blank=True, choices=ecom_constants.ORDER_STATUS)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="order_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="order_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)

    objects = CopyManager()

    def __str__(self):
        return '%s %s' % (self.order_id, self.unique_order_id)


class OrderDetails(models.Model):
    """
    This table is used to save order details data in it.It include actual_used,wallet_used_amount,return_cancelled_amount,
    repayment_amount,actual_paid,is_placed_combine,used_wallet and cash_payment function.

    """
    order_text = models.CharField(max_length=100, null=True, blank=True)
    credit = models.ForeignKey(Credit, on_delete=models.CASCADE, related_name="user_credit", null=True, blank=True)
    credit_history = models.ManyToManyField(Credit, related_name="order_details_credit_history", null=True, blank=True)
    pay_amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    total_quantity = models.IntegerField(default=0, null=True, blank=True, db_index=True)
    total_amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    date = models.DateTimeField(null=True, blank=True, db_index=True)
    re_status = models.BooleanField(default=True, db_index=True)
    status = models.CharField(max_length=256, null=True, blank=True, choices=ecom_constants.ORDER_STATUS, db_index=True)
    credit_status = models.CharField(max_length=25, null=True, blank=True, choices=ecom_constants.CREDIT_STATUS,
                                     db_index=True)
    payment = models.ManyToManyField(Payment, related_name="orderdetails_payment", null=True,
                                     blank=True)
    order_id = models.ManyToManyField(Order, related_name="oderdetails_order", null=True,
                                      blank=True)
    combine_order = models.ManyToManyField(Order, related_name="combine_order", null=True,
                                           blank=True)
    convenient_fees = models.ForeignKey(ConvenientFees, on_delete=models.CASCADE,
                                        related_name="order_Details_convenient_fees",
                                        blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="order_user", blank=True, null=True)

    address = models.ForeignKey(Address, on_delete=models.CASCADE, related_name="orderdertails_addr", null=True,
                                blank=True)
    payment_mode = models.CharField(max_length=20, null=True, blank=True,
                                    choices=ecom_constants.ORDER_DETAILS_PAYMENT_MODE)
    created_date = models.DateTimeField(auto_now_add=True)
    wallet_type = models.CharField(max_length=20, null=True, blank=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="order_details_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="order_details_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)

    def __str__(self):
        return '%s %s' % (self.order_text, self.status)

    @property
    def actual_used(self):
        """
        This function return actual_used amount from orderdetails.
        """
        credit = 0
        total_cancel_return_amount = 0
        order_detail_obj = OrderDetails.objects.filter(id=self.id).select_related("credit")
        if order_detail_obj[0].credit:
            credit = order_detail_obj[0].credit.actual_used
            total_amount = order_detail_obj[0].credit_history.filter(
                status__in=["order_cancel", "order_return"]).aggregate(
                Sum('repayment_add'))['repayment_add__sum']
            if total_amount:
                total_cancel_return_amount = total_amount
        return credit - total_cancel_return_amount

    @property
    def wallet_used_amount(self):
        """
        this function is return wallet amount from Userwallet user status success used amount.
        """
        wallet_amount = 0
        order_detail_obj = UserWallet.objects.filter(order_details=self.id, status="success")
        if order_detail_obj:
            wallet_amount = order_detail_obj[0].used_amount
        return wallet_amount

    @property
    def return_cancelled_amount(self):
        """
        this function return return_cancelled_amount of OrderDetails by calculate amount.
        """
        total_cancel_return_amount = 0
        get_order_details_obj = OrderDetails.objects.filter(id=self.id)
        if get_order_details_obj[0].credit_history:
            total_amount = get_order_details_obj[0].credit_history.filter(
                status__in=["order_cancel", "order_return"]).aggregate(
                Sum('repayment_add'))['repayment_add__sum']
            if total_amount:
                total_cancel_return_amount = total_amount
        return total_cancel_return_amount

    @property
    def repayment_amount(self):
        """
        This function return repayment_amount  by calculate order details payment amount.
        """
        repayment_amount = 0
        amount = 0
        get_order_details_obj = OrderDetails.objects.filter(id=self.id).prefetch_related("payment")
        if get_order_details_obj:
            for obj in get_order_details_obj:
                payment_obj = obj.payment.filter(product_type="Ecom", status='success',
                                                 category="Repayment").aggregate(Sum('amount'))['amount__sum']
                if payment_obj:
                    amount += payment_obj
            if amount:
                repayment_amount = amount
        return repayment_amount

    @property
    def actual_paid(self):
        """
        this function return actual_paid amount from order details payment amount
        """
        actual_paid = 0
        amount = 0
        get_order_details_obj = OrderDetails.objects.filter(id=self.id).prefetch_related("payment")
        if get_order_details_obj:
            for obj in get_order_details_obj:
                payment_obj = obj.payment.filter(product_type="Ecom", status='success',
                                                 category="Order").aggregate(Sum('amount'))['amount__sum']
                if payment_obj:
                    amount += payment_obj
            if amount:
                actual_paid = amount
        return actual_paid

    @property
    def is_placed_combine(self):
        """
        this function check is combine order or not by checking order count.
        """
        get_order_details_obj = OrderDetails.objects.filter(id=self.id)
        if get_order_details_obj:
            count = get_order_details_obj[0].combine_order.filter(status="placed").count()
            if count >= 1:
                return True
            else:
                return False
        return False

    @property
    def used_wallet(self):
        """
        this function is used to calculate wallet amount of order details.
        """
        wallet_amount = 0
        get_wallet_amount = \
            OrderDetails.objects.filter(id=self.id).last().payment.filter(status="success", type="Ecom_Wallet",
                                                                          category="Order").aggregate(
                Sum('amount'))['amount__sum']
        if get_wallet_amount:
            wallet_amount = get_wallet_amount
        else:
            wallet_amount = 0
        return wallet_amount

    @property
    def cash_payment(self):
        """
        this function return cash amount of OrderDetails.y calculated is payment amount.
        """

        cash_amount = 0
        get_cash_amount = \
            OrderDetails.objects.filter(id=self.id).last().payment.filter(status="success", category="Order").exclude(
                type="Ecom_Wallet").aggregate(
                Sum('amount'))['amount__sum']
        if get_cash_amount:
            cash_amount = get_cash_amount
        else:
            cash_amount = 0
        return cash_amount


class Seller(models.Model):
    """
    this table is use to save seller information.
    """
    name = models.CharField(max_length=256, null=True, blank=True, db_index=True)
    main_address = models.CharField(max_length=256, null=True, blank=True)
    address = models.CharField(max_length=256, null=True, blank=True)
    mobile_no = models.CharField(max_length=20, null=True, blank=True, db_index=True)
    email_id = models.CharField(max_length=50, null=True, blank=True)
    landline_no = models.CharField(max_length=50, null=True, blank=True)
    type = models.CharField(max_length=50, null=True, blank=True, choices=ecom_constants.SELLER_TYPE)
    description = models.TextField(blank=True, null=True)
    gst_no = models.CharField(max_length=50, null=True, blank=True)
    pan_no = models.CharField(max_length=50, null=True, blank=True)
    shop_act_licence = models.CharField(max_length=50, null=True, blank=True)
    pan_image = models.ImageField(upload_to='images', max_length=256, null=True, blank=True)
    licence_image = models.ImageField(upload_to='images', max_length=256, null=True, blank=True)
    shop_image = models.ImageField(upload_to='images', max_length=256, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="seller_id_user", blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    re_status = models.BooleanField(default=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="seller_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="seller_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)

    def __str__(self):
        return '%s %s' % (self.name, self.user)


class Images(models.Model):
    """
     this table is use to save information of Images of product
    """
    path = models.ImageField(upload_to='ecom', max_length=256, null=True, blank=True, db_index=True)
    product_details = models.ManyToManyField(ProductDetails,
                                             related_name="product_details_images",
                                             null=True, blank=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="product_images",
                                null=True, blank=True)
    status = models.BooleanField(default=False, db_index=True)
    date = models.DateTimeField(null=True, blank=True)
    re_status = models.BooleanField(default=True)
    type = models.CharField(max_length=256, null=True, blank=True, choices=ecom_constants.PRODUCT_IMAGE_TYPE)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="images_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="images_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)

    # def save(self, *args, **kwargs):
    #     from PIL import Image as Img
    #     imageTemproary = Image.open(self.path)
    #     outputIoStream = BytesIO()
    #     # imageTemproaryResized = imageTemproary
    #     if self.type is not None:
    #         if self.type == "main":
    #             imageTemproaryResized = imageTemproary.resize((200, 250), Img.ANTIALIAS)
    #             imageTemproaryResized.save(outputIoStream, format='JPEG', quality=85)
    #         else:
    #             imageTemproaryResized = imageTemproary.resize((300, 300), Img.ANTIALIAS)
    #             imageTemproaryResized.save(outputIoStream, format='JPEG', quality=85)
    #     outputIoStream.seek(0)
    #     self.path = InMemoryUploadedFile(outputIoStream, 'ImageField',
    #                                      "%s.jpg" % self.path.name.split('.')[0], 'image/jpeg',
    #                                      sys.getsizeof(outputIoStream), None)
    #     super(Images, self).save(*args, **kwargs)
    # from PIL import Image
    # def save(self):
    #     # a =super().save()  # saving image first
    #     # print("------------------",a)
    #     img = Image.open(self.path)  # Open image using self
    #     new_img = (300, 300)
    #     img.thumbnail(new_img)
    #     img.save(self.path)

    # def save(self, *args, **kwargs):
    #     super(Images, self).save(*args, **kwargs)
    #
    #     img = Image.open(self.path.path)
    #     output_size = (300, 300)
    #     img.thumbnail(output_size)
    #     img.save(self.path.path)
    #     print('===========================================================', img)


class EcomDocuments(models.Model):
    """
    This model is to save all sellers and logistics documents details images.
    """
    type = models.CharField(db_index=True, blank=True, null=True, max_length=30,
                            choices=ecom_constants.ECOM_DOCUMENT_TYPE)
    path = models.ImageField(upload_to='ecom_doc', max_length=256, null=True, blank=True, db_index=True)
    verify_date = models.DateField(max_length=256, null=True, blank=True)
    verify_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="doc_verify_by", null=True, blank=True)
    is_verified = models.BooleanField(default=False, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    re_status = models.BooleanField(default=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="ecom_documents")
    created_by_new = CurrentUserField(related_name="ecom_documents_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="ecom_documents_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)


class OrderLocation(models.Model):
    """
    To store Order location
    """
    altitude = models.CharField(max_length=200, blank=True, null=True)
    latitude = models.CharField(max_length=256, blank=True, null=True)
    longitude = models.CharField(max_length=256, blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    city = models.CharField(max_length=256, blank=True, null=True)
    taluka = models.CharField(max_length=256, blank=True, null=True)
    state = models.CharField(max_length=256, blank=True, null=True)
    pin_code = models.CharField(max_length=20, blank=True, null=True)
    re_status = models.BooleanField(default=True)
    date = models.DateTimeField(null=True, blank=True)
    remark = models.CharField(max_length=256, blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="order_location_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="order_location_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)


class OrderStatus(models.Model):
    """
    this table is used to save order status information.
    """
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="order_status")
    status = models.CharField(null=True, blank=True, max_length=200, db_index=True)
    date = models.DateTimeField(null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    re_status = models.BooleanField(default=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    Location = models.ManyToManyField(OrderLocation, null=True, blank=True, max_length=256,
                                      related_name="order_location")
    created_by_new = CurrentUserField(related_name="order_status_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="order_status_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)


class OrderDetailsStatus(models.Model):
    """
    this table is use to save orderdetail status information.
    """
    order_details = models.ForeignKey(OrderDetails, on_delete=models.CASCADE,
                                      related_name="order_details_status")
    status = models.CharField(null=True, blank=True, max_length=50, db_index=True)
    re_status = models.BooleanField(default=True)
    date = models.DateTimeField(null=True, blank=True, db_index=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    Location = models.ManyToManyField(OrderLocation, null=True, blank=True, max_length=256,
                                      related_name="order_details_location")
    created_by_new = CurrentUserField(related_name="order_details_status_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="order_details_status_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)


class ReturnOrder(models.Model):
    """
    This model is used for saving all records related to return order
    """
    # Order return part
    return_id = models.CharField(max_length=250, null=True, blank=True, db_index=True)
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="order_return",
                              null=True, blank=True)
    re_status = models.BooleanField(default=True)
    customer_return_image = models.ManyToManyField(Images, null=True, blank=True, max_length=256,
                                                   related_name="customer_return_image")
    order_in_exchange = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="order_in_exchange",
                                          null=True, blank=True)
    order_details = models.ForeignKey(OrderDetails, on_delete=models.CASCADE,
                                      related_name="order_details_return", null=True, blank=True)
    return_reason = models.CharField(max_length=256, null=True, blank=True, choices=ecom_constants.RETURN_REASON)

    # Admin verification part
    verify_return_status = models.CharField(null=True, blank=True, max_length=50)
    admin_verified_date = models.DateTimeField(null=True, blank=True)
    verified_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="admin_user", blank=True,
                                    null=True)

    # Logistics verification part
    pickup_date = models.DateTimeField(null=True, blank=True)
    logistic_status = models.CharField(null=True, blank=True, max_length=50)
    logistic_reason = models.CharField(max_length=256, null=True, blank=True)
    logistic_verify_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="verify_logistic_user",
                                           blank=True, null=True)

    # Seller verification part
    seller_return_image = models.ManyToManyField(Images, null=True, blank=True, max_length=256,
                                                 related_name="seller_return_image")
    product_verify_date = models.DateTimeField(null=True, blank=True)
    seller_verify_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="verify_seller_user",
                                         blank=True,
                                         null=True)
    seller_status = models.CharField(null=True, blank=True, max_length=50)
    seller_reason = models.CharField(max_length=256, null=True, blank=True)

    payment_status = models.CharField(null=True, blank=True, max_length=50)
    type = models.CharField(null=True, blank=True, max_length=50)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    created_by_new = CurrentUserField(related_name="return_order_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="return_order_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)


class LogisticOrder(models.Model):
    """
    this function is use to save logistic order information.
    """
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="logistic_order_id",
                              null=True, blank=True)
    order_details = models.ForeignKey(OrderDetails, on_delete=models.CASCADE, related_name="logistic_order_details_id",
                                      null=True, blank=True)
    waybill_no = models.CharField(null=True, blank=True, max_length=50, db_index=True)
    refnum = models.CharField(null=True, blank=True, max_length=256, db_index=True)
    remark = models.TextField(null=True, blank=True)
    logistics = models.CharField(null=True, blank=True, max_length=40, db_index=True)
    status = models.CharField(null=True, blank=True, max_length=40)
    re_status = models.BooleanField(default=True)
    order_data_text = models.TextField(null=True, blank=True)
    order_mode = models.CharField(null=True, blank=True, max_length=40, choices=ecom_constants.LOGISTICS_ORDER_MODE)
    created_date = models.DateTimeField(auto_now_add=True)
    type = models.CharField(null=True, blank=True, max_length=30, choices=ecom_constants.LOGISTIC_TYPE)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="logistic_order_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="logistic_order_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)


class SellerWarehouse(models.Model):
    """
    this function is use to save seller warehouse data.
    """
    seller = models.ForeignKey(User, on_delete=models.CASCADE, related_name="warehouse_seller",
                               null=True, blank=True)
    re_status = models.BooleanField(default=True)
    company_name = models.CharField(null=True, blank=True, max_length=256, db_index=True)
    nickname = models.CharField(null=True, blank=True, max_length=50)
    type = models.CharField(default="ithink", null=True, blank=True, max_length=30,
                            choices=ecom_constants.LOGISTIC_TYPE)
    primary_address = models.CharField(null=True, blank=True, max_length=256)
    secondary_address = models.CharField(null=True, blank=True, max_length=256)
    mobile_no = models.CharField(null=True, blank=True, max_length=30)
    pin_code = models.CharField(null=True, blank=True, max_length=30)
    city_id = models.CharField(null=True, blank=True, max_length=30)
    state_id = models.CharField(null=True, blank=True, max_length=30)
    country_id = models.CharField(null=True, blank=True, max_length=30)
    warehouse_id = models.CharField(null=True, blank=True, max_length=30, db_index=True)
    approval_status = models.CharField(null=True, blank=True, max_length=40, db_index=True)
    adding_status = models.BooleanField(default=False)
    response = models.TextField(null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="seller_warehouse_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="seller_warehouse_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)


class Wallet(models.Model):
    """
    this table is use to stores wallet calculations
    """
    balance = models.FloatField(default=0, null=True, blank=True, db_index=True)
    reward_amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    reward_amount_used = models.FloatField(default=0, null=True, blank=True, db_index=True)
    amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    amount_used = models.FloatField(default=0, null=True, blank=True, db_index=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_wallet_calc", blank=True, null=True)
    status = models.CharField(max_length=50, null=True, blank=True, choices=ecom_constants.CREDIT_DATA_STATUS)
    re_status = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="wallet_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="wallet_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)

    def __str__(self):
        return '%s %s' % (self.balance, self.user)


class UserWallet(models.Model):
    """
    this table is use to save userwallet data.
    """
    amount_add = models.FloatField(default=0, null=True, blank=True, db_index=True)
    used_amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    balance_amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    payment = models.ForeignKey(Payment, on_delete=models.CASCADE, related_name="wallet_payment", null=True,
                                blank=True)
    order_details = models.ForeignKey(OrderDetails, on_delete=models.CASCADE, related_name="wallet_order_details",
                                      null=True, blank=True)
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="wallet_order",
                              null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="wallet_user", blank=True, null=True)
    wallet = models.ForeignKey(Wallet, on_delete=models.CASCADE, related_name="wallet", blank=True, null=True)
    status = models.CharField(max_length=50, null=True, blank=True, choices=ecom_constants.WALLET_DATA_STATUS)
    re_status = models.BooleanField(default=True, null=True, blank=True)
    auto_adjust = models.BooleanField(default=False, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="user_wallet_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="user_wallet_updated_by_new", null=True,
                                      blank=True,
                                      editable=False)

    def __str__(self):
        return '%s %s' % (self.used_amount, self.status)


class LogisticsDeliveryCharges(models.Model):
    """
    this table is used to save logisticsdeiverycharge in it.
    """
    gram = models.FloatField(default=0, null=True, blank=True, db_index=True)
    amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    percentage = models.FloatField(default=0, null=True, blank=True, db_index=True)
    percentage_amount = models.FloatField(default=0, null=True, blank=True)
    tax = models.FloatField(default=0, null=True, blank=True)
    re_status = models.BooleanField(default=True)
    delivery_amount = models.FloatField(default=0, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="logistics_delivery_charges_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="logistics_delivery_charges_updated_by_new",
                                      null=True,
                                      blank=True,
                                      editable=False)


class SellerPayment(models.Model):
    """
    this table is used to save seller payment data.
    """
    order_id = models.CharField(db_index=True, max_length=200, null=True, blank=True)
    transaction_id = models.CharField(db_index=True, max_length=200, null=True, blank=True)
    pg_transaction_id = models.CharField(max_length=50, null=True, blank=True)
    status = models.CharField(db_index=True, max_length=50, null=True, blank=True,
                              choices=ecom_constants.STATUS_TYPE)
    category = models.CharField(db_index=True, max_length=50, null=True, blank=True,
                                choices=ecom_constants.SELLER_AMOUNT_STATUS)
    mode = models.CharField(db_index=True, max_length=50, null=True, blank=True,
                            choices=ecom_constants.PAYMENT_MODE)
    type = models.CharField(db_index=True, max_length=50, null=True, blank=True,
                            choices=ecom_constants.PAYMENT_Type)
    re_status = models.BooleanField(default=True)
    response = models.TextField(blank=True, null=True)
    amount = models.FloatField(db_index=True, max_length=50, null=True, blank=True)
    description = models.CharField(max_length=256, blank=True, null=True)
    date = models.DateTimeField(db_index=True, null=True, blank=True)
    pay_source = models.CharField(db_index=True, null=True, blank=True, max_length=50,
                                  choices=ecom_constants.PaymentGateway)
    return_status = models.CharField(db_index=True, null=True, blank=True, max_length=50,
                                     choices=ecom_constants.ReturnStatus)
    response_code = models.CharField(db_index=True, null=True, blank=True, max_length=50)
    product_type = models.CharField(default="Ecom", max_length=256, blank=True, null=True,
                                    choices=ecom_constants.PRODUCT_TYPE)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="seller_payment_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="seller_payment_updated_by_new",
                                      null=True,
                                      blank=True,
                                      editable=False)


class SellerOrderCancellation(models.Model):
    """
    This model is to store the record of orders cancellation from Seller..
    """
    order = models.ForeignKey(Order, on_delete=models.CASCADE, db_index=True, null=True, blank=True)
    amount = models.FloatField(null=True, blank=True)
    cancel_by = models.CharField(max_length=20, null=True, blank=True, choices=ecom_constants.SELLER_ORDER_CANCEL_BY)
    status = models.CharField(max_length=20, null=True, blank=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="seller_order_cancellation_created_by",
                                   null=True, blank=True)
    updated_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="seller_order_cancellation_updated_by",
                                   null=True, blank=True)
    request_parameters = models.TextField(null=True, blank=True)
    is_penalty_paid = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="seller_order_cancellation_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="seller_order_cancellation_updated_by_new",
                                      null=True,
                                      blank=True,
                                      editable=False)


class SellerAmount(models.Model):
    """
    this function used to save seller amount data.
    """
    type = models.CharField(max_length=50, choices=ecom_constants.SELLER_AMOUNT_STATUS, db_index=True)
    total_amount = models.FloatField(default=0)
    invoice_id = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    is_full_amount_paid = models.BooleanField(default=False)
    full_amount_paid_date = models.DateTimeField(max_length=256, null=True, blank=True)
    re_status = models.BooleanField(default=True)
    order = models.ManyToManyField(Order, null=True, blank=True, max_length=256,
                                   related_name="seller_amount_order")
    return_amount = models.FloatField(default=0, null=True, blank=True)
    return_orders = models.ManyToManyField(ReturnOrder, null=True, blank=True, max_length=256,
                                           related_name="return_orders_from_return_order")
    cancel_amount = models.FloatField(default=0, null=True, blank=True)
    cancelled_orders = models.ManyToManyField(SellerOrderCancellation, null=True, blank=True, max_length=256,
                                              related_name="cancelled_orders_from_seller_order_cancellation")
    manually_cancelled_amount = models.FloatField(default=0, null=True, blank=True)
    manually_cancelled = models.ManyToManyField(Order, null=True, blank=True, max_length=256,
                                                related_name="manually_cancelled_orders")
    undelivered_amount = models.FloatField(default=0, null=True, blank=True)
    undelivered_orders = models.ManyToManyField(Order, null=True, blank=True, max_length=256,
                                                related_name="undelivered_orders")
    date = models.DateTimeField(db_index=True, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    seller = models.ForeignKey(User, on_delete=models.CASCADE, related_name="amount_for_seller",
                               null=True, blank=True)
    seller_payment = models.ManyToManyField(SellerPayment, null=True, blank=True,
                                            related_name="seller_amount_payment")
    created_by_new = CurrentUserField(related_name="seller_amount_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="seller_amount_updated_by_new",
                                      null=True, blank=True, editable=False)


class SellerAccount(models.Model):
    """
    this function use to save seller account all that in it.
    """
    total_amount = models.FloatField(default=0, null=True, blank=True)
    return_amount = models.FloatField(default=0, null=True, blank=True)
    return_orders = models.ManyToManyField(ReturnOrder, null=True, blank=True, max_length=256,
                                           related_name="return_orders_in_return_order")
    exchange_amount = models.FloatField(default=0, null=True, blank=True)
    exchange_orders = models.ManyToManyField(ReturnOrder, null=True, blank=True, max_length=256,
                                             related_name="exchange_orders_in_return_order")
    amount_to_pay = models.FloatField(default=0, null=True, blank=True)
    paid_amount = models.FloatField(default=0, null=True, blank=True)
    amount_paid = models.BooleanField(default=False)
    re_status = models.BooleanField(default=True)
    verify_date = models.DateField(max_length=256, null=True, blank=True)
    seller = models.ForeignKey(User, on_delete=models.CASCADE, related_name="account_for_seller",
                               null=True, blank=True)
    verify_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="seller_doc_verify_by",
                                  null=True, blank=True)
    seller_amount = models.ManyToManyField(SellerAmount, null=True, blank=True, max_length=256,
                                           related_name="seller_amount")
    seller_payment = models.ForeignKey(Payment, null=True, blank=True, on_delete=models.CASCADE,
                                       related_name="seller_payment")
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="seller_account_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="seller_account_updated_by_new",
                                      null=True, blank=True, editable=False)


class LogHistory(models.Model):
    """
    This model is used to maintain logs of each and every action in models change..
    """
    model_name = models.CharField(max_length=100, null=True, blank=True)
    model_fk = models.CharField(max_length=10, null=True, blank=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="log_history_created_by", null=True,
                                   blank=True)
    updated_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="log_history_updated_by", null=True,
                                   blank=True)
    action_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="log_history_action_by", null=True,
                                  blank=True)
    action = models.CharField(max_length=20, null=True, blank=True, choices=ecom_constants.LOG_ACTION)
    re_status = models.BooleanField(default=True)
    previous_data = models.TextField(null=True, blank=True)
    current_data = models.TextField(null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)


class DefaultWalletAmount(models.Model):
    """
    This table is to store user default wallet amount store..
    """
    mobile_no = models.CharField(max_length=15, null=True, blank=True, db_index=True)
    amount = models.FloatField(null=True, blank=True, db_index=True)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE, db_index=True)
    type = models.CharField(max_length=25, null=True, blank=True, choices=ecom_constants.DEFAULT_WALLET_AMOUNT_TYPE)
    re_status = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="default_wallet_amount_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="default_wallet_amount_updated_by_new",
                                      null=True, blank=True, editable=False)

    def __str__(self):
        return '%s %s' % (self.mobile_no, self.amount)


class ProductPriority(models.Model):
    """
        This table is to set priority of product..
    """
    product = models.ForeignKey(Product, null=True, blank=True, on_delete=models.CASCADE, db_index=True)
    priority = models.IntegerField(default=None, null=True, blank=True)
    text_view = models.CharField(max_length=25, null=True, blank=True, choices=ecom_constants.TEXT_VIEW)
    dashboard_status = models.BooleanField(default=False)
    type = models.CharField(max_length=25, null=True, blank=True, choices=ecom_constants.PRODUCT_PRIORITY_TYPE)
    view_type = models.CharField(max_length=25, null=True, blank=True,
                                 choices=ecom_constants.PRODUCT_PRIORITY_VIEW_TYPE)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="product_priority_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="product_priority_updated_by_new",
                                      null=True, blank=True, editable=False)


class SellerWallet(models.Model):
    """
    this function used ti save seller data in sellerwallet.
    """
    seller = models.ForeignKey(User, default=True, on_delete=models.CASCADE, null=True, blank=True, db_index=True)
    amount_add = models.FloatField(default=0, null=True, blank=True)
    forwarded_amount = models.FloatField(default=0, null=True, blank=True)
    amount_sub = models.FloatField(default=0, null=True, blank=True)
    balance_amount = models.FloatField(default=0, null=True, blank=True)
    actual_paid = models.FloatField(default=0, null=True, blank=True)
    actual_invoice_amount = models.FloatField(default=0, null=True, blank=True)
    is_full_amount_paid = models.BooleanField(default=False, null=True, blank=True)
    full_amount_paid_date = models.DateTimeField(max_length=256, null=True, blank=True)
    is_wallet_consumed = models.BooleanField(default=False, null=True, blank=True)
    payment_status = models.BooleanField(default=False, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    date = models.DateTimeField(db_index=True, null=True, blank=True)
    seller_payment = models.ForeignKey(SellerPayment, on_delete=models.CASCADE, null=True, blank=True,
                                       related_name="seller_wallet_amount_payment", db_index=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    invoice = models.ForeignKey(SellerAmount, on_delete=models.CASCADE, null=True, blank=True, db_index=True)
    created_by_new = CurrentUserField(related_name="seller_wallet_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="seller_wallet_updated_by_new",
                                      null=True, blank=True, editable=False)


class Banner(models.Model):
    """
        This table is for banner.which save banner data.
    """
    product = models.ForeignKey(Product, null=True, blank=True, on_delete=models.CASCADE, db_index=True)
    path = models.ImageField(upload_to='banner_images', max_length=256, null=True, blank=True, db_index=True)
    date = models.DateTimeField(null=True, blank=True)
    status = models.BooleanField(default=True)
    type = models.CharField(max_length=25, null=True, blank=True, choices=ecom_constants.BANNER_TYPE)
    priority = models.IntegerField(default=None, null=True, blank=True)
    text_view = models.CharField(max_length=25, null=True, blank=True, choices=ecom_constants.BANNER_TEXT_VIEW)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="banner_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="banner_updated_by_new",
                                      null=True, blank=True, editable=False)


class SellerInvoiceClearAmount(models.Model):
    """
    This model is used to save extra wallet amount that we pay to the seller..
    """
    seller = models.ForeignKey(User, default=True, on_delete=models.CASCADE, null=True, blank=True, db_index=True)
    amount = models.FloatField(default=0, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=False, null=True, blank=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="invoice_clear_amount_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="invoice_clear_amount_updated_by_new",
                                      null=True, blank=True, editable=False)


class ProductNotification(models.Model):
    """
    This model is for product related notifications

    """
    sms = models.TextField(blank=True, null=True)
    discount_percentage = models.FloatField(default=0, null=True, blank=True, db_index=True)
    discount_amount = models.FloatField(default=0, null=True, blank=True, db_index=True)
    discount_description = models.TextField(blank=True, null=True)
    product = models.ForeignKey(Product, null=True, blank=True, on_delete=models.CASCADE, db_index=True)
    type = models.CharField(max_length=25, null=True, blank=True, choices=ecom_constants.DISCOUNT_TYPE)
    date = models.DateTimeField(null=True, blank=True)
    status = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="product_notification_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="product_notification_updated_by_new",
                                      null=True, blank=True, editable=False)


# Shiprocket Models
class ShiprocketToken(models.Model):
    """
    this model save Shiprocket Token.
    """
    token = models.TextField(null=True, blank=True)
    first_name = models.CharField(max_length=256, null=True, blank=True)
    last_name = models.CharField(max_length=256, null=True, blank=True)
    email_id = models.CharField(max_length=50, null=True, blank=True)
    company_id = models.CharField(max_length=50, null=True, blank=True)
    status = models.BooleanField(default=False)
    date = models.DateTimeField(blank=True, null=True)

    created_date = models.DateTimeField(auto_now_add=True)
    expiry_date = models.DateTimeField(null=True, blank=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="shiprocket_token_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="shiprocket_token_updated_by_new",
                                      null=True, blank=True, editable=False)

    def __str__(self):
        return '%s %s' % (self.first_name, self.status)


class ShiprocketOrder(models.Model):
    """
        this model save Shipment Order data
    """
    shipment_id = models.CharField(max_length=30, null=True, blank=True)
    shiprocket_order_id = models.CharField(max_length=30, null=True, blank=True)
    status_code = models.CharField(max_length=30, null=True, blank=True)
    onboarding_completed_now = models.CharField(max_length=15, null=True, blank=True)
    awb_code = models.CharField(max_length=50, null=True, blank=True)
    courier_company_id = models.CharField(max_length=30, null=True, blank=True)
    courier_name = models.CharField(max_length=60, null=True, blank=True)
    sr_status = models.CharField(max_length=60, blank=True, null=True)
    re_status = models.BooleanField(default=False)
    status = models.CharField(max_length=60, null=True, blank=True)
    order = models.ForeignKey(Order, on_delete=models.CASCADE,
                              related_name="order_create_order", null=True, blank=True)
    order_details = models.ForeignKey(OrderDetails, on_delete=models.CASCADE, related_name="shiprocket_order_details",
                                      null=True, blank=True)
    order_mode = models.CharField(max_length=30, null=True, blank=True, choices=ecom_constants.LOGISTICS_ORDER_MODE)
    response = models.TextField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="shiprocket_order_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="shiprocket_order_updated_by_new",
                                      null=True, blank=True, editable=False)

    def __str__(self):
        return '%s %s' % (self.shipment_id, self.shiprocket_order_id)


class ShiprocketShipmentPickup(models.Model):
    """
    this model save  Shipment Pickup data
    """
    pickup_token_number = models.CharField(max_length=256, null=True, blank=True)
    data = models.CharField(max_length=100, null=True, blank=True)
    pickup_status = models.CharField(max_length=20, null=True, blank=True)
    sr_status = models.CharField(max_length=20, null=True, blank=True)
    status = models.CharField(max_length=50, null=True, blank=True)
    re_status = models.BooleanField(default=True)
    scheduled_date = models.DateTimeField(blank=True, null=True)

    response = models.TextField(blank=True, null=True)
    sr_order = models.ForeignKey(ShiprocketOrder, on_delete=models.CASCADE,
                                 related_name="sr_order")
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="shiprocket_shipment_pickup_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="shiprocket_shipment_pickup_updated_by_new",
                                      null=True, blank=True, editable=False)

    def __str__(self):
        return '%s' % self.scheduled_date


class ShiprocketLabel(models.Model):
    """
    this model save  Shipment label data
    """
    label_url = models.CharField(max_length=256, null=True, blank=True)
    status = models.CharField(max_length=256, null=True, blank=True)
    response = models.TextField(blank=True, null=True)
    sr_order = models.ForeignKey(ShiprocketOrder, on_delete=models.CASCADE,
                                 related_name="label_sr_order")
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="shiprocket_label_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="shiprocket_label_updated_by_new",
                                      null=True, blank=True, editable=False)

    def __str__(self):
        return '%s' % self.label_url


class ShiprocketManifest(models.Model):
    """
    this model save  Shipment Manifest data
    """
    manifest_url = models.CharField(max_length=256, null=True, blank=True)
    status = models.CharField(max_length=256, null=True, blank=True)
    response = models.TextField(blank=True, null=True)
    sr_order = models.ForeignKey(ShiprocketOrder, on_delete=models.CASCADE,
                                 related_name="manifest_sr_order")
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="shiprocket_manifest_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="shiprocket_manifest_updated_by_new",
                                      null=True, blank=True, editable=False)

    def __str__(self):
        return '%s' % self.manifest_url


class ShiprocketApiInfo(models.Model):
    """
    This table is use for save shiprocket on api information.
    """
    order = models.ForeignKey(Order, on_delete=models.CASCADE, null=True, blank=True)
    order_details = models.ForeignKey(OrderDetails, on_delete=models.CASCADE, null=True, blank=True)
    create_order = models.BooleanField(default=False, null=True, blank=True)
    invoice_generate = models.BooleanField(default=False, null=True, blank=True)
    awb_generate = models.BooleanField(default=False, null=True, blank=True)
    manifest_generate = models.BooleanField(default=False, null=True, blank=True)
    pickup_schedule = models.BooleanField(default=False, null=True, blank=True)
    label_generate = models.BooleanField(default=False, null=True, blank=True)
    response = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=50, null=True, blank=True)
    type = models.CharField(max_length=50, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    created_by_new = CurrentUserField(related_name="shiprocket_api_info_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="shiprocket_api_info_updated_by_new",
                                      null=True, blank=True, editable=False)


class LoanDefaulter(models.Model):
    """
    This table is used to save loan defaulter data.
    """
    mobile_number = models.CharField(max_length=25, null=True, blank=True)
    is_defaulter = models.BooleanField(default=False, null=True, blank=True)

    status = models.BooleanField(default=True, null=True, blank=True)
    type = models.CharField(max_length=50, null=True, blank=True, choices=ecom_constants.DEFAULTER_TYPE)

    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    created_by_new = CurrentUserField(related_name="loan_defaulter_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="loan_defaulter_updated_by_new",
                                      null=True, blank=True, editable=False)


class OfferBanner(models.Model):
    """
        This table is for OfferBanner.which save offerbanner data.
    """
    path = models.ImageField(upload_to='offerbanner', max_length=256, null=True, blank=True, db_index=True)
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    status = models.BooleanField(default=True)
    type = models.CharField(max_length=25, null=True, blank=True, choices=ecom_constants.OFFER_BANNER_TYPE)
    priority = models.IntegerField(default=None, null=True, blank=True)
    description = models.CharField(max_length=256, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="offer_banner_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="offer_banner_updated_by_new",
                                      null=True, blank=True, editable=False)


class Offer(models.Model):
    """
        This table is for Offer.which save offer data.
    """
    title = models.CharField(max_length=250, null=True, blank=True)
    offer_id = models.CharField(max_length=50, null=True, blank=True)
    path = models.ImageField(upload_to='user_offer', max_length=256, null=True, blank=True, db_index=True)
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    status = models.BooleanField(default=True)
    type = models.CharField(max_length=25, null=True, blank=True, choices=ecom_constants.USER_OFFER_TYPE)
    priority = models.IntegerField(default=None, null=True, blank=True)
    description = models.CharField(max_length=256, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="offer_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="offer_updated_by_new",
                                      null=True, blank=True, editable=False)

    def __str__(self):
        return '%s %s' % (self.offer_id, self.title)


class UserOffer(models.Model):
    """
        This table is for UserOffer..
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_offer", blank=True, null=True)
    is_read = models.BooleanField(default=False)
    status = models.BooleanField(default=True)
    offer = models.ForeignKey(Offer, on_delete=models.CASCADE, related_name="user_offer", blank=True, null=True)
    created_by_new = CurrentUserField(related_name="user_offer_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="user_offer_updated_by_new",
                                      null=True, blank=True, editable=False)


class Coupon(models.Model):
    """
    This table is used to save coupons for order details..
    """
    order_details = models.ForeignKey(OrderDetails, on_delete=models.CASCADE, null=True, blank=True, db_index=True)
    coupon = models.CharField(max_length=100, null=True, blank=True)
    date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    status = models.BooleanField(default=False, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="coupon_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="coupon_updated_by_new",
                                      null=True, blank=True, editable=False)

    def __str__(self):
        return '%s %s' % (self.order_details, self.coupon)


class NewsLetter(models.Model):
    """
    This table is used to save email details..
    """
    email_id = models.CharField(max_length=50, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    created_by_new = CurrentUserField(related_name="newsletter_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="newsletter_updated_by_new",
                                      null=True, blank=True, editable=False)


class ProductRating(models.Model):
    """
    This table is used to save ProductRating. In this model there is function which
    calculate no_of_rating,no_of_reviews and avg_rating.
    """
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, blank=True)
    rating = models.IntegerField(null=True, blank=True, choices=ecom_constants.RATING_CHOICES)
    review = models.TextField(null=True, blank=True)
    path = models.ImageField(upload_to='review_images', max_length=256, null=True, blank=True, db_index=True)
    re_status = models.BooleanField(default=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_rating", blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    created_by_new = CurrentUserField(related_name="product_rating_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="product_rating_updated_by_new",
                                      null=True, blank=True, editable=False)

    def no_of_rating(self):
        rating = ProductRating.objects.filter(product=self.product, re_status=True)
        return len(rating)

    def no_of_reviews(self):
        reviews = ProductRating.objects.filter(product=self.product, re_status=True).exclude(review=None)
        return len(reviews)

    def avg_rating(self):
        # print("inside this=================================================")
        sum = 0
        ratings = ProductRating.objects.filter(product=self.product, re_status=True)
        if ratings:
            for rating in ratings:
                new_rating = 0
                if rating.rating:
                    new_rating = rating.rating
                else:
                    new_rating = 0
                sum += new_rating
            if (len(ratings) > 0):
                return round(sum / len(ratings), 1)
            else:
                return 0
        else:
            return 0

    def __str__(self):
        return str(self.product) + "---" + str(self.user)


class WalletRecharge(models.Model):
    """
    this table is used to save WalletRecharge
    """
    order_id = models.CharField(max_length=250, null=True, blank=True)
    transaction_id = models.CharField(max_length=250, null=True, blank=True)
    amount = models.FloatField(db_index=True, max_length=50, null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    re_status = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="wallet_recharge_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="wallet_recharge_updated_by_new",
                                      null=True, blank=True, editable=False)


class OverDueData(models.Model):
    amount = models.IntegerField(null=True, blank=True)
    re_status = models.BooleanField(default=True)
    date = models.DateTimeField(db_index=True, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="overdue_data_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="overdue_data_updated_by_new",
                                      null=True, blank=True, editable=False)

    def __str__(self):
        return '%s %s' % (self.amount, self.date)


class DashboardData(models.Model):
    """
    this table is used to save dashboard data
    """
    status = models.BooleanField(default=False)
    type = models.CharField(max_length=50, null=True, blank=True, choices=ecom_constants.DASHBOARD_DATA_TIME_TYPE)
    dashboard_data = JSONField()
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)


from django.forms.models import model_to_dict
from django.db.models.signals import pre_save, post_save, post_delete
from django.dispatch import receiver, Signal
import os
import sys

list_of_models = (
    "Category", "SubCategory", "BaseCategory", "CGST", "SGST", "Size", "Colour", "Product", "Unit", "ProductDetails",
    "Stock", "Cart", "Credit", "DeliveryCharges", "ProductDetailsAmount", "ProductMinMaxAMount",
    "ProductMinMaxDiscount", "ConvenientFees", "Order", "OrderDetails", "Seller", "Images", "EcomDocuments",
    "OrderLocation", "OrderStatus", "OrderDetailsStatus", "ReturnOrder", "LogisticOrder", "SellerWarehouse", "Wallet",
    "UserWallet", "LogisticsDeliveryCharges", "OverDueData",
    "SellerPayment", "SellerOrderCancellation", "SellerAmount", "SellerAccount", "DefaultWalletAmount",
    "ProductPriority", "SellerWallet", "Banner", "SellerInvoiceClearAmount", "ProductNotification", "ShiprocketToken",
    "ShiprocketOrder", "ShiprocketShipmentPickup", "ShiprocketLabel", "ShiprocketManifest", "ShiprocketApiInfo",
    "LoanDefaulter", "OfferBanner", "Offer", "UserOffer", "Coupon", "NewsLetter", "ProductRating", "WalletRecharge",
    "Payment", "Recovery", "RecoveryCallHistory", "User", "DistributionLimitAmount", "Remark", "RecoveryCommission",
    "UserInsurance"
)


@receiver(pre_save)
def log_before_save(sender, instance, **kwargs):
    # print("inside pre save", sender.__name__)
    if sender.__name__ in list_of_models:
        # print("-----here--------", pre_save)
        from .middleware import RequestMiddleware

        try:
            instance._pre_save_instance = sender.objects.get(pk=instance.pk)
        except sender.DoesNotExist:
            instance._pre_save_instance = instance

        instance = instance._pre_save_instance
        # print("=========================", instance)
        action = "before_update"
        # print("---------------------------------------------", action)
        model_name = str(sender.__name__)
        # print("model_name============", model_name)
        request = RequestMiddleware(get_response=None)
        try:
            # print("Inside Try")
            if request.thread_local.current_request:
                request = request.thread_local.current_request
                action_by = request.user
                # print("action_by", action_by)
                create_log = create_log_entry(model_name, action, action_by, instance)

        except Exception as e:
            print("Inside Exception of pre_save", e)
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)


@receiver(post_save)
def log_after_save(sender, instance, created, **kwargs):
    # list_of_models = ('Credit', 'Order')
    # print("inside post save", sender.__name__)

    if sender.__name__ in list_of_models:
        from .middleware import RequestMiddleware
        model_name = str(sender.__name__)

        request = RequestMiddleware(get_response=None)
        try:
            if request.thread_local.current_request:
                request = request.thread_local.current_request
                action_by = request.user
                action = None
                if created:
                    action = "added"
                else:
                    action = "after_update"

                # print("action in log_after_save", action)
                create_log = create_log_entry(model_name, action, action_by, instance)

        except Exception as e:
            print("Inside Exception of post_save", e)
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)


@receiver(post_delete)
def delete_logs(sender, instance, **kwargs):
    # print("after delete function===========")
    action = "deleted"
    if sender.__name__ in list_of_models:
        from .middleware import RequestMiddleware
        model_name = str(sender.__name__)

        request = RequestMiddleware(get_response=None)
        try:
            if request.thread_local.current_request:
                request = request.thread_local.current_request
                action_by = request.user
                # print("user=======", action_by)

                create_log = create_log_entry(model_name, action, action_by, instance)

        except Exception as e:
            print("Inside Exception of post_delete", e)
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)


def create_log_entry(model_name, action, action_by=None, instance=None):
    if None not in (model_name, action):
        previous_data = {}
        current_data = {}
        created_by = None
        if action_by:
            is_instance = isinstance(action_by, User)
            if not is_instance:
                action_by = None

        updated_by = None
        instance_id = None
        if instance:
            instance_id = instance.id

            try:
                if instance.created_by_new:
                    created_by = instance.created_by_new

                if instance.updated_by_new:
                    updated_by = instance.updated_by_new

                if action in ["deleted", "before_update"]:
                    previous_data = model_to_dict(instance)
                else:
                    current_data = model_to_dict(instance)

            except Exception as e:
                print("Inside Exception of created_by_new field not found error", e)

        if None not in (action, instance_id):
            create_log = LogHistory.objects.create(model_name=model_name, model_fk=instance_id, created_by=created_by,
                                                   updated_by=updated_by, action_by=action_by, action=action,
                                                   previous_data=previous_data, current_data=current_data)


class StoreCSVData(models.Model):
    """
    This model is used to store csv report data
    """
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    status = models.CharField(null=True, blank=True, max_length=50)
    file_path = models.CharField(blank=True, null=True, max_length=250)
    model_name = models.CharField(null=True, blank=True, max_length=50)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'Loan Application id {} {}'.format(self.start_date, self.end_date)

    @property
    def check_file_exist(self):
        file_status = False
        path = self.file_path
        file_path = "media/csv/" + path
        check_file = os.path.exists(file_path)
        if check_file:
            file_status = True
        return file_status

    def store_data_entry(self, create_data):
        create_csv_obj = StoreCSVData.objects.create(**create_data)

    def store_data_update(self, actual_data):
        update_csv_obj = StoreCSVData.objects.filter(id=self.obj_id).update(**actual_data)


class StoreInvoiceData(models.Model):
    """
    This model is used to store invoice pdf report data
    """
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    seller = models.CharField(null=True, blank=True, max_length=50)
    file_path = models.CharField(blank=True, null=True, max_length=250)
    model_name = models.CharField(null=True, blank=True, max_length=50)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'Loan Application id {} {}'.format(self.start_date, self.end_date)

    @property
    def check_file_exist(self):
        file_status = False
        path = self.file_path
        file_path = "media/invoice/" + path
        check_file = os.path.exists(file_path)
        if check_file:
            file_status = True
        return file_status

    def store_data_entry(self, create_data):
        create_csv_obj = StoreInvoiceData.objects.create(**create_data)

    def store_data_update(self, actual_data):
        update_csv_obj = StoreInvoiceData.objects.filter(id=self.obj_id).update(**actual_data)
