from .models import *
from django.conf import settings
import datetime
from decimal import Decimal


def create_crif_master(dataa):
    print(dataa, 'data')
    first_name = None
    last_name = None
    middle_name = None
    score_type = None
    score_version = None
    try:
        data = {
            'first_name': dataa['FirstName'],
            'middle_name': dataa['MiddleName'],
            'last_name': dataa['LastName'],
            'score_type': dataa['score_type'],
            'score_version': dataa['score_version'],
            'status': '1',
            'user_id':dataa['user_id']

        }
        equi_instance = CrifIndvMaster.objects.create(**data)
        return equi_instance
    except:
        return False

def create_score(instance, credit_score_type, credit_score_version): 
    score_type = None
    score_version = None

    try:
        data = {
            'equifax': instance,
            'score_type': score_type,
            'score_version': score_version
            }

        crifIndvInstance = CrifIndvScore.objects.create(**data)
        print(crifIndvInstance, 'indv instance')
    except:
        pass

    return True


def create_score_details(instance,score, score_element, credit_score_type, credit_score_version):
    score_type = None
    score_version = None
    score_name = None
    score_value = None
    scoring_element = None

    try:
        score_type = credit_score_type
        score_version = credit_score_version
        score_value = score
        scoring_element = score_element

        data = {
            'equifax': instance,
            'score_type': score_type,
            'score_version': score_version,
            'score_value': score_value,
            'scoring_element': scoring_element
        }

        CrifScoreDetails.objects.create(**data)

    except Exception as e:
        print(e.args)

    return True