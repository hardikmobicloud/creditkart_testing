import datetime
from random import randint
from django.shortcuts import render, HttpResponse
import requests
from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import render
import os, sys
from django.apps.registry import apps

from Equfix.views import state_code_status
from .models import CrifIndvMaster, CrifScoreDetails
from .utils import create_crif_master, create_score, create_score_details

LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')

# Test credentials for individual..
# ANANDFIN_USERNAME_IND = "anand_fin_indv_test"
# ANANDFIN_PASSWORD_IND = "038F2F24E74C238685DE9D750C897A6449D4E8F5"

# REQ_MBR_IND = "NBF0003092"
# SUB_MBR_ID_IND = "ANAND PROPERTY FINANCE LIMITED"
# TEST_FLG_IND = "Y"
# LOS_NAME_IND ="ANANDFIN"
# LOS_VENDER_IND ="HO"


# Production credentials for individual..
ANANDFIN_USERNAME_IND = "anand_fin_1_indv_prod"
ANANDFIN_PASSWORD_IND = "722281D01C88151066630B1F555C834B70F42946"

REQ_MBR_IND = "NBF0002236"
SUB_MBR_ID_IND = "ANAND PROPERTY FIN LTD"
TEST_FLG_IND = "N"
LOS_NAME_IND ="ANANDFIN"
LOS_VENDER_IND ="HO"

input_headers_ind = {"userId": ANANDFIN_USERNAME_IND, "password": ANANDFIN_PASSWORD_IND, 'Content-type': 'text/xml'}


# Testing urls..
# base_sync_url = "https://test.crifhighmark.com/Inquiry/Inquiry/CPUSingleAction.action"

# Production urls..
base_sync_url = "https://hub.crifhighmark.com/Inquiry/Inquiry/CPUSingleAction.action"

CRED_ERROR = 'INVALID_CREDENTIALS'
CRED_ERROR_DESC = 'Username or Password is incorrect.'
HEAD_ERROR = 'EMPTY_HEADERS'
HEAD_ERROR_DESC = 'Username, Password or XML data can not be empty.'

from rest_framework import generics
from rest_framework.response import Response

from django.apps.registry import apps

# CreditPartner = apps.get_model('crif', 'CreditPartner')
# CrifResponse = apps.get_model('crif', 'CrifResponse') 
CrifScoreResponse = apps.get_model('crif', 'CrifScoreResponse')


def add_user_data(*args):
    print(args[4], 'argss')
    from datetime import datetime
    from xml.etree import ElementTree as et
    now = datetime.now()
    import random
    DT_TM = now.strftime("%d-%m-%Y %H:%M:%S")
    UNI_REF_NO = now.strftime("%d%m%Y") + "UNIQUE_REF_ID" + str(random.randint(0, 1000))
    ADDRESS = args[0]
    CITY = args[1]
    PINCODE = args[2]
    PAN = args[3]
    DOB = args[4].strftime("%d-%m-%Y")
    print(DOB, 'dob')
    STATE_CODE = args[5]
    APPLICANT_NAME = args[6]
    MOBILE = args[7]
    EMAIL = args[8]
    GENDER = args[9]
    LOAN_AMNT = args[10]
    BANK_NAME = args[11]
    KENDRA_ID = args[12]
    LOS_APP_ID = now.strftime("%d%m%Y") + "LOS_APP_ID" + str(random.randint(0, 1000))

    temp_name = "templates/Crif/base_sync_request_data.xml"
    tree = et.parse(temp_name)
    tree.find('REQUEST-REQUEST-FILE/HEADER-SEGMENT/REQ-MBR').text = REQ_MBR_IND
    tree.find('REQUEST-REQUEST-FILE/HEADER-SEGMENT/SUB-MBR-ID').text = SUB_MBR_ID_IND
    tree.find('REQUEST-REQUEST-FILE/HEADER-SEGMENT/USER-ID').text = ANANDFIN_USERNAME_IND
    tree.find('REQUEST-REQUEST-FILE/HEADER-SEGMENT/PWD').text = ANANDFIN_PASSWORD_IND
    tree.find('REQUEST-REQUEST-FILE/HEADER-SEGMENT/INQ-DT-TM').text = DT_TM


    tree.find('REQUEST-REQUEST-FILE/INQUIRY/APPLICANT-SEGMENT/APPLICANT-NAME/NAME1').text = APPLICANT_NAME
    tree.find('REQUEST-REQUEST-FILE/INQUIRY/APPLICANT-SEGMENT/DOB/DOB-DATE').text = DOB
    tree.find('REQUEST-REQUEST-FILE/INQUIRY/APPLICANT-SEGMENT/IDS/ID/VALUE').text = PAN
    
    tree.find('REQUEST-REQUEST-FILE/INQUIRY/APPLICANT-SEGMENT/PHONES/PHONE/TELE-NO').text = MOBILE
    
    tree.find('REQUEST-REQUEST-FILE/INQUIRY/APPLICANT-SEGMENT/GENDER').text = GENDER
    
    tree.find('REQUEST-REQUEST-FILE/INQUIRY/APPLICANT-SEGMENT/EMAILS/EMAIL').text = EMAIL
    
    tree.find('REQUEST-REQUEST-FILE/INQUIRY/ADDRESS-SEGMENT/ADDRESS/ADDRESS-1').text = ADDRESS

    tree.find('REQUEST-REQUEST-FILE/INQUIRY/ADDRESS-SEGMENT/ADDRESS/CITY').text = CITY

    tree.find('REQUEST-REQUEST-FILE/INQUIRY/ADDRESS-SEGMENT/ADDRESS/STATE').text = STATE_CODE

    tree.find('REQUEST-REQUEST-FILE/INQUIRY/ADDRESS-SEGMENT/ADDRESS/PIN').text = PINCODE

    tree.find('REQUEST-REQUEST-FILE/INQUIRY/APPLICATION-SEGMENT/INQUIRY-UNIQUE-REF-NO').text = UNI_REF_NO

    tree.find('REQUEST-REQUEST-FILE/INQUIRY/APPLICATION-SEGMENT/MBR-ID').text = REQ_MBR_IND

    tree.find('REQUEST-REQUEST-FILE/INQUIRY/APPLICATION-SEGMENT/KENDRA-ID').text = KENDRA_ID

    tree.find('REQUEST-REQUEST-FILE/INQUIRY/APPLICATION-SEGMENT/BRANCH-ID').text = BANK_NAME
    
    tree.find('REQUEST-REQUEST-FILE/INQUIRY/APPLICATION-SEGMENT/LOAN-AMOUNT').text = LOAN_AMNT

    tree.write(temp_name)

    with open(temp_name, 'r') as f:
        data = f.read()

    return data


def IndvidualScoreRequestAPI(request):
    print('called from template')
        # print("--------------------", request.META)

    loan_application_id = request.GET.get('loan_application_id', None)
    if loan_application_id is not None: 
        print('not none')
        print(loan_application_id, 'loan id')
    loan_obj = LoanApplication.objects.select_related('user', 'user_professional', 'user_personal','current_address').filter(loan_application_id=loan_application_id)
    # print(loan_application_id,loan_obj, 'loan object')
    user_id = loan_obj[0].user_id
    name = (loan_obj[0].user_personal.name).upper()
    fathers_name = (loan_obj[0].user_personal.fathers_name).upper()
    mobile = loan_obj[0].user_personal.mobile_no
    email = (loan_obj[0].user_personal.email_id).upper()
    gender = loan_obj[0].user_personal.gender
    print(gender, 'gender')
    if gender == 'Male':
        gender = 'G01'
    if gender == 'Female':
        gender = 'G02'
    if gender == 'Other':
        gender = 'G03'
    loan_amount = loan_obj[0].loan_scheme_id
    bank_name = (loan_obj[0].bank_id.bank_name).upper()
    kendra_id = loan_obj[0].bank_id.ifsc_code

    address = (loan_obj[0].current_address.address).upper()
    city = (loan_obj[0].current_address.city).upper()
    pincode = loan_obj[0].current_address.pin_code
    pan = loan_obj[0].user_personal.pan_number
    # date_time_str = str(str(loan_obj[0].user_personal.birthdate))
    # dob = datetime.datetime.strptime(date_time_str[:10], '%Y-%m-%d')

    dob = loan_obj[0].user_personal.birthdate

    state_code = state_code_status(loan_application_id)
    if state_code == '':
        state_code = 'MH'

    print(address, city, pincode, pan, dob, state_code, name, mobile, email, gender, loan_amount, bank_name, kendra_id, 'user details')
    path = "templates/Crif/base_sync_request_data.xml"
    exchange_credentials = add_user_data(address, city, pincode, pan, dob, state_code, name, mobile, email, gender, loan_amount, bank_name, kendra_id)

    with open('templates/Crif/base_sync_request_data.xml', 'r') as f:
        inner_xml_data = f.read()[10:-11]
        f.close()

    input_headers_ind["userId"] = 'creditkart'
    input_headers_ind["password"] = 'Creditkart@123!@#'

    input_headers_ind["inquiryXML"] = inner_xml_data
    

    # print(input_headers_ind, 'individual header')
    base_sync_url = "https://staging.anandfin.in/credit/base_sync_api/"
    response_data = requests.post(url=base_sync_url, headers=input_headers_ind)

    if response_data:
        
        response_data = (response_data.text).replace("\n", "")
        response_data = (response_data).replace("\t", "")
        response_data = (response_data).replace("\r", "")
        print(response_data, 'response data')
        with open('templates/Crif/base_sync_response_data.xml', 'w') as f:
            f.write("<metadata>")
            f.write(response_data)
            f.write("</metadata>")

        from xml.etree import ElementTree as et
        temp_name = "templates/Crif/base_sync_response_data.xml"
        tree = et.parse(temp_name)
        credit_score = tree.find('BASE-PLUS-REPORT-FILE/BASE-PLUS-REPORTS/BASE-PLUS-REPORT/SCORES/SCORE/SCORE-VALUE').text
        credit_score_type = tree.find('BASE-PLUS-REPORT-FILE/BASE-PLUS-REPORTS/BASE-PLUS-REPORT/SCORES/SCORE/SCORE-TYPE').text
        credit_score_version = tree.find('BASE-PLUS-REPORT-FILE/BASE-PLUS-REPORTS/BASE-PLUS-REPORT/SCORES/SCORE/SCORE-FACTORS').text
        data = {
            'FirstName': name,
            'MiddleName': fathers_name,
            'LastName': name,
            'score_type': credit_score_type,
            'score_version': credit_score_version,
            'status': '1',
            'user_id':user_id
        }
        cr_master = create_crif_master(data)
        print(cr_master,'cr master')
        if cr_master:
            create_score(cr_master, credit_score_type, credit_score_version)
            create_score_details(cr_master, credit_score, pan, credit_score_type, credit_score_version)

        user_value = CrifIndvMaster.objects.filter(user_id=loan_obj[0].user_id).order_by('-id')
        if user_value:
            score_value = CrifScoreDetails.objects.filter(equifax_id=user_value[0].id).order_by('-id')
            if score_value:
                equfix_score = score_value[0].score_value
        update_data = CrifScoreResponse.objects.create(loan_applicant=loan_obj[0].user, request_data=inner_xml_data, status="SUCCESS", response_data=response_data, created_date=datetime.datetime.now(), updated_date=datetime.datetime.now())


        # if cr_master:
        #     create_score(cr_master, credit_score_type, credit_score_version)
        #     create_score_details(cr_master, credit_score, pan, credit_score_type, credit_score_version)

        # user_value = CrifIndvMaster.objects.filter(user_id=loan_obj[0].user_id).order_by('-id')
        # if user_value:
        #     score_value = CrifScoreDetails.objects.filter(equifax_id=user_value[0].id).order_by('-id')
        #     if score_value:
        #         equfix_score = score_value[0].score_value
        # update_data = CrifScoreResponse.objects.create(loan_applicant=loan_obj[0].user, request_data=inner_xml_data, status="SUCCESS", response_data=response_data.text, created_date=datetime.datetime.now(), updated_date=datetime.datetime.now())


    # print(inner_xml_data, 'inner xml data')

        # response_data = requests.post(url=base_sync_url, headers=input_headers_ind)
        # headers = request.headers
        # username = None
        # password = None
        # inquiryxml_data = None
        # # print("headers----------------", headers)
        # if "userId" in headers:
        #     username = headers["userId"]

        # if "password" in headers:
        #     password = headers["password"]

        # if "inquiryXML" in headers:
        #     inquiryxml_data = headers["inquiryXML"]

        # print("inquiryxml_data", inquiryxml_data)
        # if None not in (username, password, inquiryxml_data):
        #     print('first if None not in (username, password, inquiryxml_data)')
        #     get_partner = CreditPartner.objects.filter(username=username, password=password, status=True).order_by(
        #         'id').last()
        #     print(get_partner,'get partner')
        #     if get_partner:
        #         print(get_partner, 'got partner base sync')
        #         # This is a valid customer..
        #         # Save input request to the CrifResponse Table..
        #         head_data = {}
        #         head_data["userId"] = username
        #         head_data["password"] = password
        #         head_data["xml_data"] = inquiryxml_data
        #         add_request_data = CrifScoreResponse.objects.create(credit_partner=get_partner, request_data=head_data)

        #         import requests
        #         inner_xml_data = None
        #         try:
        #             with open('templates/Crif/base_sync_request_data.xml', 'w') as f:
        #                 f.write("<metadata>")
        #                 f.write(inquiryxml_data)
        #                 f.write("</metadata>")

        #             path = "templates/Crif/base_sync_request_data.xml"
        #             exchange_credentials = add_username_password_base_sync(path)

        #         except Exception as e:
        #             print("Inside Exception of changing Username and Password from XML..")

        #         with open('templates/Crif/base_sync_request_data.xml', 'r') as f:
        #             inner_xml_data = f.read()[10:-11]
        #             f.close()

        #         input_headers_ind["inquiryXML"] = inner_xml_data
        #         # print(input_headers_ind, 'input headers for 2nd request')
        #         print(base_sync_url, 'base sync url')
        #         response_data = requests.post(url=base_sync_url, headers=input_headers_ind)
        #         if response_data:
        #             # Updating the entry with response data..
        #             update_data = CrifScoreResponse.objects.filter(id=add_request_data.id).update(
        #                 response_data=response_data.text, updated_date=datetime.datetime.now())

        #             response_data = (response_data.text).replace("\n", "")
        #             response_data = (response_data).replace("\t", "")
        #             response_data = (response_data).replace("\r", "")
        #             # response_data = (response_data).replace("\\", "")

        #         print("response_data", response_data)

        #         # resp_data = None
        #         # with open('templates/response_text.xml', 'r') as f:
        #         #     resp_data = f.read()

        #         return Response(
        #             response_data
        #         )

        #     else:
        #         resp_data = get_return_data(CRED_ERROR, CRED_ERROR_DESC)
        #         return Response(
        #             resp_data
        #         )

        # else:
        #     print("here22222222222222222222222")
        #     resp_data = get_return_data(HEAD_ERROR, HEAD_ERROR_DESC)
        #     return Response(
        #         resp_data
        #     )
    data = {
        'is_taken': credit_score
    }

    return JsonResponse(data)

def get_return_data(error_code, error_description):
    """
    This function takes the error_code and error_description and returns the required response data..
    """
    data = None
    from xml.etree import ElementTree as et
    temp_name = "templates/Crif/internal_response.xml"
    tree = et.parse(temp_name)
    tree.find('REPORT-FILE/INQUIRY-STATUS/INQUIRY/ERRORS/ERROR/CODE').text = error_code
    tree.find('REPORT-FILE/INQUIRY-STATUS/INQUIRY/ERRORS/ERROR/DESCRIPTION').text = error_description
    tree.write(temp_name)

    with open(temp_name, 'r') as f:
        data = f.read()

    return data