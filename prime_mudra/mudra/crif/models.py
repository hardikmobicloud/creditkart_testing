from django.db import models

from django.conf import settings

from django.contrib.auth.models import User
# Create your models here.


# class CreditPartner(models.Model):
#     name = models.CharField(max_length=250, null=True, blank=True)
#     username = models.CharField(max_length=250, null=True, blank=True)
#     password = models.CharField(max_length=100, null=True, blank=True)
#     status = models.BooleanField(default=True, null=True, blank=True)
#     created_date = models.DateTimeField(auto_now_add=True)
#     updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)

#     def __str__(self):
#         return '%s' % (self.name)


# class CrifResponse(models.Model):
#     credit_partner = models.ForeignKey(CreditPartner, on_delete=models.CASCADE, null=True, blank=True, db_index=True)
#     request_data = models.TextField(null=True, blank=True)
#     status = models.CharField(max_length=50, null=True, blank=True)
#     response_data = models.TextField(null=True, blank=True)
#     created_date = models.DateTimeField(auto_now_add=True)
#     updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)

#     def __str__(self):
#         return '%s' % (self.credit_partner)


class CrifScoreResponse(models.Model):
    loan_applicant = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True, db_index=True)
    request_data = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=50, null=True, blank=True)
    response_data = models.TextField(null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return '%s' % (self.loan_applicant)

class CrifIndvMaster(models.Model):

    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE,blank=True,null=True)

    first_name = models.CharField(max_length=255, blank=True, null=True)
    middle_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)

    score_type = models.CharField(max_length=255, blank=True, null=True)
    score_version = models.CharField(max_length=255, blank=True, null=True)

    status = models.CharField(max_length=255, blank=True, null=True)


class CrifIndvScore(models.Model):

    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    crif_indv_master = models.ForeignKey(CrifIndvMaster, on_delete=models.CASCADE)

    score_type = models.CharField(max_length=100, blank=True, null=True)
    score_version = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):

        return self.score_type

class CrifScoreDetails(models.Model):

    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(CrifIndvMaster, on_delete=models.CASCADE)

    score_type = models.CharField(max_length=255, blank=True, null=True)
    score_version = models.CharField(max_length=255, blank=True, null=True)
    score_name = models.CharField(max_length=255, blank=True, null=True)
    score_value = models.IntegerField(db_index=True,blank=True, null=True)
    scoring_element = models.TextField(blank=True, null=True)

    # def __str__(self):
    #
    #     return "{}".format(self.id)
