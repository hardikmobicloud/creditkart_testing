from django.contrib import admin

# Register your models here.
from .models import *

class CrifIndvMasterAdmin(admin.ModelAdmin):

    list_display = ('first_name', 'last_name', 'score_type', 'score_version', 'status',)

class CrifIndvScoreAdmin(admin.ModelAdmin):

    list_display = ('crif_indv_master', 'score_type', 'score_version', 'date_created', 'date_update',)

admin.site.register(CrifIndvMaster, CrifIndvMasterAdmin)

admin.site.register(CrifIndvScore, CrifIndvScoreAdmin)
# class CreditPartnerAdmin(admin.ModelAdmin):
#     list_display = ["name", "username", "password", "status", "created_date", "updated_date"]
#     list_filter = ["status"]
#     search_fields = ["name"]


class CrifScoreResponseAdmin(admin.ModelAdmin):
    list_display = ["loan_applicant", "request_data", "status", "response_data", "created_date","updated_date"]
    list_filter = ["status"]
    search_fields = ["loan_applicant"]


# admin.site.register(CreditPartner, CreditPartnerAdmin)
admin.site.register(CrifScoreResponse, CrifScoreResponseAdmin)
