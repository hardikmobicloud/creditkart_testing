from django.contrib import admin
from .models import *


class AutomationAdmin(admin.ModelAdmin):
    list_display = ('reason', 'status','date','loan_application_id','loan_id','automation_status','create_date','response',)
    list_filter = ('status', 'loan_application_id','automation_status',)
    exclude = ('update_date',)
    search_fields = ('loan_application_id',)





class FaceMatchAdmin(admin.ModelAdmin):
    list_display = ('automation', 'pan_eid_score','pan_error','pan_selfie_score','selfie_error','eid_selfie_score','eid_error','create_date',)





class TextMatchAdmin(admin.ModelAdmin):

    list_display = ('automation', 'company_name', 'company_score','company_error','pan_name','pan_score','pan_error','pan_eid_name',
            'pan_eid_score','pan_eid_error','pan_salary_name','pan_salary_score','pan_salary_error','salary_month','salary_amount','salary_error','create_date',)





admin.site.register(TextMatch, TextMatchAdmin)
admin.site.register(FaceMatch, FaceMatchAdmin)
admin.site.register(Automation, AutomationAdmin)
