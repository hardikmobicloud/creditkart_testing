from django.db import models
from django.conf import settings
from LoanAppData.models import LoanApplication



class Automation(models.Model):
    reason = models.CharField(null=True, blank=True, max_length=256)
    response = models.CharField(null=True,blank=True,max_length=256)
    status = models.CharField(db_index=True, max_length=50, choices=settings.LOAN_APPLICATION_STATUS, blank=True,
                              null=True,
                              default='SUBMITTED')
    date = models.DateTimeField(db_index=True, null=True, blank=True)
    loan_application_id = models.CharField(max_length=256,null=True,blank=True)
    loan_id = models.ForeignKey(LoanApplication, on_delete=models.CASCADE, null=True, blank=True, related_name="loan_id_automation")
    automation_status=models.BooleanField(null=True,blank=True,default=False,max_length=10)
    hold_day=models.IntegerField(null=True,blank=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, null=True, blank=True)



class FaceMatch(models.Model):
    automation = models.ForeignKey(Automation, on_delete=models.CASCADE, null=True, blank=True, related_name="loan_id_facematch")
    pan_eid_score = models.FloatField(null=True,blank=True)
    pan_error = models.CharField(null=True,blank=True,max_length=256)
    pan_selfie_score = models.FloatField(null=True,blank=True)
    selfie_error = models.CharField(null=True,blank=True,max_length=256)
    eid_selfie_score = models.FloatField(null=True,blank=True)
    eid_error = models.CharField(null=True,blank=True,max_length=256)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, null=True, blank=True)





class TextMatch(models.Model):
    automation = models.ForeignKey(Automation, on_delete=models.CASCADE, null=True, blank=True,
                                   related_name="loan_id_textmatch")
    company_name = models.CharField(null=True, blank=True, max_length=256)
    company_score = models.FloatField(null=True, blank=True)
    company_error = models.CharField(null=True, blank=True, max_length=256)
    pan_name = models.CharField(null=True, blank=True, max_length=256)
    pan_score = models.FloatField(null=True, blank=True)
    pan_error = models.CharField(null=True, blank=True, max_length=256)
    pan_eid_name = models.CharField(null=True, blank=True, max_length=256)
    pan_eid_score = models.FloatField(null=True, blank=True)
    pan_eid_error = models.CharField(null=True, blank=True, max_length=256)
    pan_salary_name = models.CharField(null=True, blank=True, max_length=256)
    pan_salary_score = models.FloatField(null=True, blank=True)
    pan_salary_error = models.CharField(null=True, blank=True, max_length=256)
    salary_month = models.CharField(null=True, blank=True, max_length=256)
    salary_amount = models.FloatField(null=True, blank=True)
    salary_error = models.CharField(null=True, blank=True, max_length=256)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, null=True, blank=True)



