import json
import os
import sys
import logging

from django.shortcuts import render, HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.views import generic
from django.apps.registry import apps
from django.contrib.auth.models import User
from django.db.models import Sum
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.sites.models import Site
from django.db.models import Q
from crif.models import CrifIndvMaster, CrifScoreDetails
from MApi import botofile
import datetime
from MApi.email import sms
from LoanAppData.tasks import *

EquiFaxMaster = apps.get_model('Equfix', 'EquiFaxMaster')
EquiFaxScoreDetails = apps.get_model('Equfix', 'EquiFaxScoreDetails')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
subscription = apps.get_model('User', 'subscription')
RBLTransaction = apps.get_model('Payments', 'RBLTransaction')
Personal = apps.get_model('User', 'Personal')
AuthenticationPin = apps.get_model('User', 'AuthenticationPin')
Automation = apps.get_model('automation', 'Automation')
FaceMatch = apps.get_model('automation', 'FaceMatch')
TextMatch = apps.get_model('automation', 'TextMatch')
InstalledApps = apps.get_model('UserData', 'InstalledApps')
UserWallet = apps.get_model('ecom', 'UserWallet')


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class Dashboard(generic.ListView):
    template_name = "CustomerExecutive/dashboard.html"
    title = 'User Details By Mobile no'

    def get(self, request, *args, **kwargs):
        ctx = {}
        if 'message' in kwargs:
            ctx['msg'] = kwargs['message']
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class UserLoanDetailsByMobile(generic.ListView):
    template_name = "CustomerExecutive/userloan_details_by_mobile.html"
    title = 'User Details By Mobile no'

    def post(self, request, *args, **kwargs):
        ctx = {}
        disbursed_amount_list = []
        repayment_amount_list = []
        try:
            if request.method == "POST":
                mobile = request.POST['mobile']
                if "user_details" in request.POST:
                    # print('inside ')
                    try:
                        user = User.objects.get(username=mobile)
                        if user:
                            user_loans = LoanApplication.objects.filter(user=user)
                            if user_loans:
                                # print('inside if----------')
                                for loan in user_loans:
                                    if loan.disbursed_amount:
                                        disbursed_amount = loan.disbursed_amount.amount
                                        disbursed_amount_list.append(disbursed_amount)
                                    else:
                                        disbursed_amount_list.append(0.0)
                                    if loan.repayment_amount:
                                        rep_amount = loan.repayment_amount.filter(
                                            status__icontains='success').aggregate(Sum('amount'))['amount__sum']
                                        repayment_amount_list.append(rep_amount)
                                    else:
                                        repayment_amount_list.append(0.0)
                    except:

                        user_loans = LoanApplication.objects.filter(loan_application_id=mobile)
                        if user_loans:
                            for loan in user_loans:
                                if loan.disbursed_amount:
                                    disbursed_amount = loan.disbursed_amount.amount
                                    disbursed_amount_list.append(disbursed_amount)
                                else:
                                    disbursed_amount_list.append(0.0)
                                if loan.repayment_amount:
                                    rep_amount = loan.repayment_amount.filter(
                                        status__icontains='success').aggregate(Sum('amount'))['amount__sum']
                                    repayment_amount_list.append(rep_amount)
                                else:
                                    repayment_amount_list.append(0.0)

                    ctx['user_loan'] = zip(user_loans, disbursed_amount_list, repayment_amount_list)
                    return render(request, self.template_name, ctx)
                elif "send_sms" in request.POST:
                    sms_message = "Dear valued customer,your profile please re apply after"
                    sms_kwrgs = {
                        'sms_message': sms_message,
                        'number': str(mobile)
                    }
                    sms(**sms_kwrgs)
                    return HttpResponseRedirect(reverse('Customer_Dashboard', \
                                                        kwargs={'message': "Message is successfully sent.."}))
        except Exception as e:
            print("----exception --", e)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class LoanDetails(generic.ListView):
    template_name = "CustomerExecutive/loan_details.html"
    title = 'loan Details'

    def get(self, request, loan_id, *args, **kwargs):
        ctx = {}
        try:
            user_loan = LoanApplication.objects.filter(loan_application_id=loan_id)
            if user_loan:
                payment = user_loan[0].repayment_amount.all()
                ctx['payment'] = payment
                Rbl_payments = RBLTransaction.objects.filter(transaction_id_text=user_loan[0].loan_application_id)
                if Rbl_payments:
                    ctx['rbl_payment'] = Rbl_payments[0]
                    ctx['loan_id'] = loan_id
            return render(request, self.template_name, ctx)
        except Exception as e:
            print("---- loan cancelled err", e)
            print("-------rbl save data-------", e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


def PancardMatch(pan_number):
    try:
        user_count = Personal.objects.values('mobile_no').filter(
            pan_number=pan_number).distinct().count()
        if user_count > 1:
            return user_count
        else:
            user_count = 0
            return user_count
    except:
        return 0


def get_reference_details(loan_application_id):
    """
    returns user reference details
    :param user_id:
    :return:
    """
    today = datetime.datetime.now()
    ctx = {}
    NOT_FOUND = "Referance is not a user"

    user = LoanApplication.objects.get(loan_application_id=loan_application_id)
    try:
        ref1 = user.reference_id.colleague_mobile_no
        ref2 = user.reference_id.relative_mobile_no
        loan_users = LoanApplication.objects.filter(status='APPROVED')
        users = []
        for user in loan_users:
            if user.user.username == ref1 or user.user.username == ref2:
                if (today > user.loan_end_date):
                    NOT_FOUND = "Defaulters found in referance"
                else:
                    NOT_FOUND = "No defaulters found in referance"
    except:
        pass
    return NOT_FOUND


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class UserAllDetails(generic.ListView):
    """
        Template : customer_user_all_details.html
        shows user loan application
    """
    login_required = True
    template_name = "CustomerExecutive/customer_user_all_details.html"
    title = 'User Details'
    pancount = 0

    def get(self, request, id, *args, **kwargs):
        print(request.user.id, 'user id')
        ctx = {}
        try:
            # print('id', id)
            loan_users = LoanApplication.objects.select_related('verified_by', 'user', 'user_professional',
                                                                'user_personal', 'aadhar_id', 'bank_sts_id',
                                                                'salary_slip_id', 'company_id', 'self_video',
                                                                'current_address', 'permanent_address', 'reference_id',
                                                                'bank_id', 'cibil_id', 'device_data_id').get(id=id)
            # print('loan_users', loan_users)
            from MApi.botofile import file_exists

            try:
                if loan_users.company_id:
                    ci = get_old_loan_documents_path(loan_users.company_id.path, 'company_id')
                    file_ex = file_exists(ci)
                    if file_ex:
                        # Old Document
                        company_id = ci
                        company_id = botofile.get_url(company_id)
                    else:
                        company_id = loan_users.company_id.path
                        company_id = botofile.get_url(company_id)
                else:
                    company_id = None
            except:
                company_id = None

            try:
                if loan_users.pancard_id:
                    pan = get_old_loan_documents_path(loan_users.pancard_id.path, 'pan')
                    file_ex = file_exists(pan)
                    if file_ex:
                        pan = pan
                        pan = botofile.get_url(pan)
                    else:
                        pan = loan_users.pancard_id.path
                        pan = botofile.get_url(pan)
                else:
                    pan = None
            except:
                pan = None

            try:
                if loan_users.salary_slip_id:
                    ss = get_old_loan_documents_path(loan_users.salary_slip_id.path, 'salary_slip')
                    file_ex = file_exists(ss)
                    print("------file_ex", file_ex)
                    if file_ex:
                        # Old Document
                        salary_slip = ss
                        salary_slip = botofile.get_url(salary_slip)
                    else:
                        # New document
                        salary_slip = loan_users.salary_slip_id.path
                        salary_slip = botofile.get_url(salary_slip)
                else:
                    salary_slip = None
            except:
                salary_slip = None

            try:
                if loan_users.self_video:
                    sv = get_old_loan_documents_path(loan_users.self_video.path, 'self_video')
                    file_ex = file_exists(sv)
                    print("------file_ex", file_ex)
                    if file_ex:
                        profile_images = sv
                        profile_images = botofile.get_url(profile_images)
                    else:
                        profile_images = loan_users.self_video.path
                        profile_images = botofile.get_url(profile_images)
                else:
                    profile_images = None
            except:
                profile_images = None

            equfix_score = None
            # user_value = EquiFaxMaster.objects.values_list('id').filter(user_id=loan_users.user_id).order_by('-id')
            # if user_value:
            #     score_value = EquiFaxScoreDetails.objects.values_list('score_value').filter(
            #         equifax_id=user_value[0][0]).order_by('-id')
            #     if score_value:
            #         equfix_score = score_value[0][0]

            user_value = CrifIndvMaster.objects.values_list('id').filter(user_id=loan_users.user_id).order_by('-id')
            if user_value:
                score_value = CrifScoreDetails.objects.values_list('score_value').filter(
                    equifax_id=user_value[0][0]).order_by('-id')
                if score_value:
                    equfix_score = score_value[0][0]

            loan_count = 0
            repayment_txc_count = 0
            loan_ids = LoanApplication.objects.values_list('id', 'loan_application_id', 'repayment_amount').order_by(
                'loan_application_id').distinct('loan_application_id').filter(user_id=loan_users.user_id,
                                                                              status__in=['APPROVED', 'COMPLETED'])
            loan_count = len(loan_ids)
            try:
                for id in loan_ids:
                    if id[2] is not None:
                        loan_payment_count = id[2].repayment_amount.filter(
                            status__icontains='success').count()
                        repayment_txc_count += loan_payment_count
                    else:
                        repayment_txc_count = 0
                        pass
            except:
                pass
            if loan_users.overdue:
                overdue = "This profile is under Overdue with Count :  " + str(loan_users.overdue)
            else:
                overdue = ""

            common_app = []
            try:
                gambling_app = settings.GAMBLING_APPS
                installed_obj = InstalledApps.objects.filter(user_id=loan_users.user_id).values_list('name')
                installed_app_list = list(int(i) for i in installed_obj)
                common_app = list(set(installed_app_list).intersection(gambling_app))
            except Exception as e:
                print("Inside exception of gambling_app", e.args)

            # Seraches pancard for this number
            pancount = PancardMatch(loan_users.user_personal.pan_number)

            current_address = loan_users.current_address

            # if current_address is not None:
            #     pincode = get_pincodewise_analysis(current_address.pin_code)
            #     try:
            #         if pincode:
            #             ctx['user_count'] = pincode[0]
            #             ctx['overdue'] = pincode[1]
            #         else:
            #             ctx['user_count'] = None
            #             ctx['overdue'] = None
            #     except:
            #         ctx['user_count'] = None
            #         ctx['overdue'] = None

            ctx['pan'] = pan
            ctx['company_id'] = company_id
            ctx['salary_slip'] = salary_slip

            ctx['pancount'] = pancount

            ctx['users'] = loan_users
            ctx['users_images'] = profile_images
            ctx['overdue_count'] = overdue

            ctx['smscount'] = loan_users.total_sms
            ctx['user_dpd'] = loan_users.dpd
            ctx['equfix_score'] = equfix_score
            user_ref_list = []
            default_users = []
            # Generating user dpd list
            dpd_list = []
            loan_userss = LoanApplication.objects.values_list('loan_application_id', 'dpd', 'loan_scheme_id',
                                                              'status').filter(user=loan_users.user_id,
                                                                               status__in=['APPROVED', 'COMPLETED'])
            for i in loan_userss:
                dpd_list.append([i[0], i[1], i[2], i[3]])
            if dpd_list is None:
                dpd_list = None
            from LoanAppData.loan_funcations import loan_amount_slab
            slabs_data = loan_amount_slab(loan_users.id)
            print(slabs_data, 'slabs data')
            if slabs_data:
                ctx['payment_slab'] = slabs_data
            print(loan_users.disbursed_amount_wallet, 'disbursed amouont')
            user_wallet_obj = UserWallet.objects.filter(user=loan_users.user_id).exclude(status="initiated").order_by('-id')
            print(user_wallet_obj, 'user wallet object')
            if user_wallet_obj:
                for user_wallet_data in user_wallet_obj:
                    # print(user_wallet_data.balance_amount, 'balance')
                    # print(user_wallet_data.amount_add, 'balance add')
                    balance_wallet = user_wallet_data.balance_amount
            ctx['user_loancount_list'] = loan_count
            ctx['user_transaction_list'] = repayment_txc_count
            ctx['balance'] = loan_users.balance
            ctx['disbursed_wallet_amount'] = loan_users.disbursed_amount_wallet
            ctx['dpd_list'] = dpd_list
            ctx['gambling_app'] = len(common_app)
            ctx['emi'] = round((loan_users.approved_amount) / 3) 
            print(loan_users.approved_amount, 'emi amount')
            ctx['approved_amount'] = loan_users.approved_amount
            ctx['NOT_FOUND'] = get_reference_details(str(loan_users.loan_application_id))

            return render(request, self.template_name, ctx)

        except Exception as e:
            print("-----Exception----", e)
            import sys
            import os
            print('-----------in exception Cusomer ExecUserAllDetails----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class Subscription(generic.ListView):
    template_name = "CustomerExecutive/subscriptions.html"
    title = 'subscription'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class UserSubscription(generic.ListView):
    template_name = "CustomerExecutive/user_subscriptions.html"
    title = 'user subscription'

    def post(self, request, *args, **kwargs):
        ctx = {}
        data = {}
        subs = {}
        sub_list = []
        try:
            if request.method == "POST":
                mobile = request.POST['mobile']
                user = User.objects.get(username=mobile)
                if user:
                    personal = Personal.objects.filter(mobile_no=user.username).first()
                    subscriptions = subscription.objects.filter(user=user.id)
                    if subscriptions:
                        for sub in subscriptions:
                            sub_list.append(sub)
                    ctx['subs'] = sub_list
                    ctx['name'] = personal
                    ctx['sub_user'] = user
                return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class RepaymentDetails(generic.ListView):
    def get(self, request, *args, **kwargs):
        if request.method == "GET":
            loan_id = request.GET.get('loan_id')
            pay_list = []
            data_dict = {}
            user_loan = LoanApplication.objects.filter(loan_application_id=loan_id).first()
            if user_loan:
                # Getting data from Many to many Field
                payment = user_loan.repayment_amount.all()
                if payment:
                    for i in payment:
                        data_dict = {
                            'order_id': i.order_id,
                            'type': i.type,
                            'category': i.category,
                            'mode': i.mode,
                            'amount': i.amount,
                            'date': str(i.date),
                            'transaction_status': i.status
                        }
                        pay_list.append(data_dict)

        return HttpResponse(json.dumps(pay_list))


def get_repayment_data(request):
    lid = request.GET.get('loan_id')
    data = {"loan_id": lid, "test": "test"}
    return HttpResponse(data)
    return HttpResponse(json.dumps(data))


def pancardsearch(request):
    return render(request, "CustomerExecutive/pan_card_search.html")


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class PanCardUserLoans(generic.ListView):
    template_name = "CustomerExecutive/pancard_user_loans.html"
    title = ' Pancard User Loans '

    def post(self, request, *args, **kwargs):
        ctx = {}
        if request.method == "POST":
            pan_no = request.POST['pan_no']
            if pan_no and pan_no is not None:
                users = Personal.objects.filter(pan_number=pan_no)
                if users:
                    # print('user_id', user[0].id)
                    loans = LoanApplication.objects.filter(user_personal_id__in=users)
                    # print('loans', loans)
                    ctx['loans'] = loans
                return render(request, self.template_name, ctx)
            else:
                return render(request, "CustomerExecutive/pan_card_search.html")


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class PanCardUserPersonals(generic.ListView):
    template_name = "CustomerExecutive/pancard_user_personal.html"
    title = ' Pancard User Loans '

    def post(self, request, *args, **kwargs):
        ctx = {}
        if request.method == "POST":
            pan_no = request.POST['pan_no']
            if pan_no and pan_no is not None:
                users = Personal.objects.filter(pan_number=pan_no)
                if users:
                    ctx['loans'] = users
                    # print(ctx)
                return render(request, self.template_name, ctx)
            else:
                return render(request, "CustomerExecutive/pan_card_search.html")


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class Pin(generic.ListView):
    template_name = "CustomerExecutive/pin.html"
    title = 'subscription'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class UserPin(generic.ListView):
    template_name = "CustomerExecutive/user_pin.html"
    title = 'user pin'

    def post(self, request, *args, **kwargs):
        ctx = {
            'pin_data': None
        }
        try:
            if request.method == "POST":
                username = request.POST['username']
                if username:
                    pin_data = AuthenticationPin.objects.filter(user__username=username).first()
                    if not pin_data or pin_data is None:
                        pin_data = None
                    ctx['pin_data'] = pin_data
                return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


def get_old_loan_documents_path(path, type):
    """
        company id , pan card, salary_slip_id , self_video
    """
    image_path = None
    if path is not None:
        # print("split ==", str(path).split('/'))
        get_image_name = str(path).split('/')
        if len(get_image_name) == 3:
            image_name = get_image_name[2]
        elif len(get_image_name) == 1:
            image_name = get_image_name[0]
        elif len(get_image_name) == 2:
            image_name = get_image_name[1]
        else:
            image_name = False

        pre_salary_slip = 'user_salary_slip/'
        pre_balanced_sheet = 'user_balanced_sheet/'
        pre_residence_address_proof = 'user_residence_address_proof/'
        company_id = 'user_work_identity_images/'
        pre_pan_card = 'user_pan_card/'
        profile_image = 'profile_images/'

        if image_name:
            if type == 'pan':
                image_path = pre_pan_card + image_name
            elif type == 'salary_slip':
                image_path = pre_salary_slip + image_name
            elif type == 'company_id':
                image_path = company_id + image_name
            elif type == 'self_video':
                image_path = profile_image + image_name
    return image_path
