from django.urls import path
from .views import *

urlpatterns = [
    path(r'customer-dashboard/(?P<message>)/', Dashboard.as_view(), name='Customer_Dashboard'),
    path(r'customer-dashboard/', Dashboard.as_view(), name='Customer_Dashboard'),
    path(r'user-details-mobile/', UserLoanDetailsByMobile.as_view(), name='userloan_details_mobile'),
    path(r'user-all-details/<int:id>/', UserAllDetails.as_view(), name='user_all_details'),
    path(r'loan-details/<int:loan_id>/', LoanDetails.as_view(), name='userloan_details_mobile'),

    path(r'subscription-check/', Subscription.as_view(), name='subscription_check'),
    path(r'user-subscriptions/', UserSubscription.as_view(), name='user_subscription'),
    path(r'repayment-details/', RepaymentDetails.as_view(), name='repayment_details'),
    path(r'get_repayment_data/', get_repayment_data, name='get_repayment_data'),

    # Dashboard
    path(r'pin/', Pin.as_view(), name='pin'),
    path(r'user-pin/', UserPin.as_view(), name='user_pin'),

    # Pan card search loan details
    path('pancard-search/', pancardsearch, name='pancard_search'),
    path('pancard-user-loans/', PanCardUserLoans.as_view(), name='pancard_user_loans'),
    path('pancard-user-personal/', PanCardUserPersonals.as_view(), name='pancard_user_personal'),

]