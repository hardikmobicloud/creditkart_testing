import datetime
import random
from django.apps.registry import apps
from django.db.models import Sum
from django.contrib.auth.models import User

Credit = apps.get_model('ecom', 'Credit')
UserWallet = apps.get_model('ecom', 'UserWallet')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
Payment = apps.get_model('Payments', 'Payment')
RazorpayOrder = apps.get_model('LoanAppData', 'RazorpayOrder')


def get_approved_amount_list():
    """
    Approved Amount List
    """
    approved_amount_list = [1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 15000, 20000, 25000,
                            30000, 35000, 40000, 45000, 50000, 55000, 60000, 65000, 70000, 75000, 80000, 85000, 90000,
                            95000, 100000]
    return approved_amount_list


def loan_paid_amount(loan_id):
    """
    This function will return total repayment amount  for that loan..
    """
    total_return_amount = 0
    if loan_id is not None:
        get_loan_obj = LoanApplication.objects.filter(id=loan_id)
        if get_loan_obj:
            total_amount = \
                get_loan_obj[0].repayment_amount.filter(status="success", category='Loan_Repayment').aggregate(
                    Sum('amount'))[
                    'amount__sum']
            if total_amount:
                total_return_amount = total_amount
    return total_return_amount


def loan_slabs(loan_id):
    """
    create slab for loan
    """
    slabs_dict = {}
    loan_app_obj = LoanApplication.objects.filter(id=loan_id)
    if loan_app_obj:
        order_date = loan_app_obj[0].loan_start_date
        first_installment_date = order_date + datetime.timedelta(days=30)
        second_installment_date = order_date + datetime.timedelta(days=60)
        third_installment_date = order_date + datetime.timedelta(days=90)
        dates = [first_installment_date, second_installment_date, third_installment_date]
        approved_amount = 0
        if loan_app_obj[0].approved_amount:
            approved_amount = loan_app_obj[0].approved_amount

            total_return_amount = loan_paid_amount(loan_app_obj[0].id)
            if total_return_amount:
                approved_amount = approved_amount
            if approved_amount > 0:
                parts = 3
                equal_installments = [approved_amount // parts + (1 if x < approved_amount % parts else 0) for x in
                                      range(parts)]
                slabs_dict = dict(zip(dates, equal_installments))
                return slabs_dict
        else:
            return slabs_dict


def get_amount_paid_for_loan(loan_obj):
    """
    get amount paid for that loan
    """
    rep_amount = 0
    if loan_obj.repayment_amount:
        rep_amount = \
        loan_obj.repayment_amount.filter(status='success', category='Loan_Repayment').aggregate(Sum('amount'))[
            'amount__sum']
        if rep_amount is not None:
            get_last_date = loan_obj.repayment_amount.filter(status="success", category='Loan_Repayment').order_by(
                'id').last()
            if get_last_date:
                last_date = get_last_date.date
                last_date = last_date
                data = {
                    "rep_amount": rep_amount,
                    "last_date": last_date
                }
                return data
        else:
            data = {
                "rep_amount": 0,
                "last_date": None
            }
            return data


def loan_amount_slab(loan_id):
    """
    get all details of loan slab, date, overdue and paid, pending amount
    """
    print('in slabs func')
    repay_list = []
    loan_obj = LoanApplication.objects.filter(id=loan_id)
    if loan_obj:
        if loan_obj[0].approved_amount:
            approved = loan_obj[0].approved_amount
            total_return_amount = loan_paid_amount(loan_obj[0].id)
            if total_return_amount:
                approved = approved - total_return_amount
            if approved != 0:
                loans_slabs = loan_slabs(loan_obj[0].id)
                if loans_slabs:
                    new_dict_list = []
                    new_dict = {}
                    new_amount = 0
                    for date, amount in loans_slabs.items():
                        new_amount += amount
                        new_dict = {'date': date, 'approved_amount': amount,
                                    'new_amount': new_amount}
                        new_dict_list.append(new_dict)

                    for slab in new_dict_list:
                        repay_dict = {}
                        pay_status = ""
                        is_overdue = ""
                        end_date = slab['date']
                        approved_amount = slab['approved_amount']
                        amount = slab['new_amount']
                        today_date = datetime.datetime.now()
                        paid_amount = 0

                        data = get_amount_paid_for_loan(loan_obj[0])
                        if data:
                            paid_amount = data['rep_amount']
                            last_pay_date = data['last_date']
                            remaining_amount = 0
                            if paid_amount != 0 and last_pay_date is not None:
                                total_amount_paid = paid_amount
                                remaining_amount = amount - total_amount_paid
                                if remaining_amount < 0:
                                    remaining_amount = 0
                                elif remaining_amount > approved_amount:
                                    remaining_amount = approved_amount
                                if total_amount_paid < amount and today_date < end_date:
                                    pay_status = "unpaid"
                                    is_overdue = "No"
                                elif total_amount_paid < amount and today_date > end_date:
                                    pay_status = "unpaid"
                                    is_overdue = "Yes"
                                elif total_amount_paid >= amount:
                                    pay_status = "paid"
                                    is_overdue = "No"

                                repay_dict = {'date': str(end_date)[:10],
                                              'time': str(end_date)[11:19],
                                              'approved_amount': approved_amount,
                                              'remaining_amount': remaining_amount,
                                              'pay_status': pay_status,
                                              'is_overdue': is_overdue}
                                repay_list.append(repay_dict)

                            else:
                                if today_date < end_date:
                                    pay_status = "unpaid"
                                    is_overdue = "No"
                                elif today_date > end_date:
                                    pay_status = "unpaid"
                                    is_overdue = "Yes"

                                repay_dict = {'date': str(end_date)[:10],
                                              'time': str(end_date)[11:19],
                                              'approved_amount': approved_amount,
                                              'remaining_amount': approved_amount,
                                              'pay_status': pay_status,
                                              'is_overdue': is_overdue}
                                repay_list.append(repay_dict)
    return repay_list


def get_user_wallet_amount(user_id):
    """
    This function returns users last available wallet balance..
    """
    wallet_dict = {}
    balance_amount = 0
    wallet_obj = None
    if user_id is not None:
        get_wallet_obj = UserWallet.objects.filter(user=user_id).exclude(
            status__in=["initiated", "return_delivery_charge_initiated", "recharge_initiated"]).order_by(
            'id').last()
        if get_wallet_obj:
            balance_amount = get_wallet_obj.balance_amount
            wallet_obj = get_wallet_obj
    wallet_dict["balance_amount"] = balance_amount
    wallet_dict["wallet_obj"] = wallet_obj
    return wallet_dict


def user_wallet_sub(user_id, wallet_amount, status, auto_adjust=False):
    if user_id and wallet_amount is not None:
        user = User.objects.filter(id=user_id)
        get_wallet_obj = UserWallet.objects.filter(user=user_id).exclude(
            status__in=["initiated", "return_delivery_charge_initiated", "recharge_initiated"]).order_by(
            'id').last()
        last_balance_amount = 0
        if get_wallet_obj:
            last_balance_amount = get_wallet_obj.balance_amount
        else:
            last_balance_amount = 0

        new_balance_amount = last_balance_amount - wallet_amount
        sub_user_wallet = UserWallet.objects.create(used_amount=wallet_amount, balance_amount=new_balance_amount,
                                                    user=user[0], status=status, auto_adjust=auto_adjust)
        if sub_user_wallet:
            return sub_user_wallet


def get_loan_total_due_amount(loan_id):
    is_defaulter = "False"
    today_date = datetime.datetime.now()
    loan_obj = LoanApplication.objects.filter(id=loan_id).first()
    if loan_obj:
        total_due_amount = 0
        # checking for each orders..
        if loan_obj:
            # This function gives slabs of installments for this order.
            slabs = loan_slabs(loan_obj.id)
            if slabs:
                new_dict = {}
                new_amount = 0
                for date, amount in slabs.items():
                    new_amount += amount
                    new_dict.update({date: new_amount})
                for end_date, amount in new_dict.items():
                    data = get_amount_paid_for_loan(loan_obj)
                    if data:
                        paid_amount = data['rep_amount']
                        last_pay_date = data['last_date']
                        if paid_amount != 0 and last_pay_date is not None:

                            if paid_amount < amount and today_date > end_date:
                                total_due_amount = amount - paid_amount

                        else:
                            if today_date > end_date:
                                # is_defaulter = "True"
                                total_due_amount = amount
        return total_due_amount


def check_loan_default():
    """
    get default loan till date
    """
    is_defaulter = "False"
    start_date = "2020-08-28"
    today_date = datetime.datetime.now()
    default_loan_list = []
    default_list = []
    # recovery_last_date = Recovery.objects.order_by('id').last()
    # if recovery_last_date:
    #     start_date = recovery_last_date.created_date
    get_loan_obj = LoanApplication.objects.filter(created_date__range=(start_date, today_date), status="APPROVED",
                                                  user_status="Accept")
    if get_loan_obj:
        # checking for each orders..
        for loan in get_loan_obj:
            if loan:
                loan_date = loan.created_date
                # This function gives slabs of installments for this order.
                slabs = loan_slabs(loan.id)
                if slabs:
                    new_dict = {}
                    new_amount = 0
                    for date, amount in slabs.items():
                        new_amount += amount
                        new_dict.update({date: new_amount})
                    for end_date, amount in new_dict.items():
                        data = get_amount_paid_for_loan(loan)
                        if data:
                            paid_amount = data['rep_amount']
                            last_pay_date = data['last_date']
                            if paid_amount != 0 and last_pay_date is not None:
                                if paid_amount < amount and today_date > end_date:
                                    is_defaulter = "True"
                                    default_loan_list.append(loan.id)
                                else:
                                    is_defaulter = "False"
                            else:
                                if today_date > end_date:
                                    is_defaulter = "True"
                                    default_loan_list.append(loan.id)
    default_list = set(default_loan_list)
    return default_list


def loan_amount_deduct_from_wallet_scheduler():
    print("Inside loan_amount_deduct_from_wallet_scheduler function")
    one_month_ago = datetime.datetime.now() - datetime.timedelta(days=30)
    two_month_ago = datetime.datetime.now() - datetime.timedelta(days=60)
    three_month_ago = datetime.datetime.now() - datetime.timedelta(days=90)
    date_list = [three_month_ago, two_month_ago, one_month_ago]
    if date_list:
        for date in date_list:
            start_date = date - datetime.timedelta(hours=6)
            end_date = date
            loan_obj = LoanApplication.objects.filter(loan_start_date__range=(start_date, end_date), status="APPROVED",
                                                      user_status="Accept")
            if loan_obj:
                for loan in loan_obj:
                    overdue_amount = get_loan_total_due_amount(loan.id)
                    wallet_amount = 0
                    if overdue_amount > 0:
                        wallet_amount = 0
                        wallet_dict = get_user_wallet_amount(loan.user.id)
                        if wallet_dict:
                            wallet_amount = wallet_dict["balance_amount"]
                        if overdue_amount > 0 and wallet_amount > 0:
                            deduct_amount = 0
                            if wallet_amount >= overdue_amount:
                                deduct_amount = overdue_amount
                                # Total Overdue Recovered
                            if wallet_amount < overdue_amount:
                                remaning_overdue_amount = overdue_amount - wallet_amount
                                try:
                                    from MApi.email import sms
                                    number = loan.user.username
                                    sms_kwrgs = {
                                        'sms_message': "Hi, your EMI of Rs." + str(
                                            remaning_overdue_amount) + " is due today. Pay it now for more shopping. Pay : https://bit.ly/3lt1xgQ",
                                        'number': str(number)
                                    }
                                    sms(**sms_kwrgs)
                                except Exception as e:
                                    print("error in user order msg send")
                                deduct_amount = wallet_amount

                            if deduct_amount > 0:
                                wallet_status = "loan_repayment_adjust"
                                auto_adjust = True
                                update_wallet = user_wallet_sub(loan.user.id, deduct_amount, wallet_status, auto_adjust)
                                random_number = random.sample(range(99999), 1)
                                ORDER_ID = str(loan.loan_application_id) + 'l' + str(random_number[0])
                                success_payment = Payment.objects.create(order_id=str(ORDER_ID),
                                                                         amount=float(deduct_amount),
                                                                         date=datetime.datetime.now(),
                                                                         category='Loan_Repayment', type="Ecom_Wallet",
                                                                         mode="PPI", status="success")

                                if success_payment and success_payment is not None:
                                    get_loan_obj = LoanApplication.objects.filter(id=loan.id)
                                    if get_loan_obj:
                                        get_loan_obj[0].repayment_amount.add(success_payment)

                        else:
                            try:
                                from MApi.email import sms
                                number = loan.user.username
                                sms_kwrgs = {
                                    'sms_message': "Hi, your EMI of Rs." + str(
                                        overdue_amount) + " is due today. Pay it now for more shopping. Pay : https://bit.ly/3lt1xgQ",
                                    'number': str(number),
                                    'tempid': None
                                }
                                sms(**sms_kwrgs)
                            except Exception as e:
                                print("error in user order msg send")
    return True


# This function hit by scheduler & check todays.... last date to pay amount according to loan slab
def recurring_amount(request):
    today = datetime.datetime.now()
    today_date = str(today)[:11]
    # today_date = "2021-02-07"
    loan_obj = LoanApplication.objects.filter(status="APPROVED", user_status="Accept")
    if loan_obj:
        for loan in loan_obj:
            slab_data = loan_amount_slab(loan.id)
            pending_installment_amount = 0
            if slab_data:
                for slab in slab_data:
                    if slab['pay_status'] == 'unpaid' and today_date == slab['date']:
                        pending_installment_amount = slab['remaining_amount']

            if pending_installment_amount != 0:
                # Create Loan Payment Initiated Entry
                random_number = random.sample(range(99999), 1)
                ORDER_ID = str(loan.loan_application_id) + 'l' + str(random_number[0])

                # Check_alredy_exist
                already_exist_payment = loan.repayment_amount.filter(create_date__startswith=today_date)
                if already_exist_payment:
                    pass

                else:
                    # Create repayment entry
                    create_payment = Payment.objects.create(order_id=str(ORDER_ID),
                                                            date=datetime.datetime.now(),
                                                            category='Loan_Repayment', type="App",
                                                            status='initiated')

                    # Attach Repayment Entry in loan
                    loan.repayment_amount.add(create_payment)

                    from MApi.enach_razorpay import order_to_charge_customer_api
                    rsp = order_to_charge_customer_api(loan.loan_application_id, pending_installment_amount,
                                                       create_payment)
                    if "msg" in rsp:
                        pass

                    elif "id" in rsp:
                        pg_transaction_id = rsp['id']
                        order_id = rsp['receipt']
                        amount = rsp['amount']
                        paid_amount = rsp['amount_paid']
                        loan_application_id = rsp['notes']['notes_key_1']
                        payment_order_id = rsp['notes']['notes_key_2']

                        if rsp['status'] == 'captured':
                            pay_status = 'success'

                        elif rsp['status'] == 'authorized':
                            pay_status = 'pending'

                        else:
                            pay_status = 'failed'

                        new_paid_amount = float(paid_amount / 100)

                        # Check LoanApplication
                        get_loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id).first()
                        if get_loan_obj:
                            # Check RazorpayOrder
                            razorpay_obj = RazorpayOrder.objects.filter(order_id=order_id).last()
                            if razorpay_obj:
                                # Update Razorpay Entry
                                update_entry = RazorpayOrder.objects.filter(order_id=order_id).update(
                                    pg_transaction_id=pg_transaction_id,
                                    amount=new_paid_amount,
                                    response=rsp,
                                    type='SU_ORDER',
                                    status=pay_status)

                                # Update repayment entry
                                success_payment = Payment.objects.filter(order_id=payment_order_id).update(
                                    pg_transaction_id=pg_transaction_id,
                                    amount=new_paid_amount,
                                    date=datetime.datetime.now(),
                                    category='Loan_Repayment', type="App",
                                    status=pay_status, mode=razorpay_obj.auth_type)

                    else:
                        pass

    return None
