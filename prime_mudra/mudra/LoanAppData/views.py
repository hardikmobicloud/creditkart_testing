# Create your views here.
import datetime
import errno
import os
import re
import sys
from datetime import datetime
from datetime import timedelta
from rest_framework.authentication import *
from rest_framework.permissions import IsAuthenticated
# from MApi.paytm_invoice import *
from LoanAppData.tasks import *
from MApi import botofile
import requests
# from IcomeExpense.models import SmsStringMaster
from MApi.email import email, sms
from MApi.botofile import upload_image
from MApi.user_sharedata import share_approveddata
from Payments.tasks import rbl_worker
from User.models import Personal
from apscheduler.schedulers.background import BackgroundScheduler
from django.apps.registry import apps
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.db.models import Count
from django.db.models import Sum
from django.http import HttpResponseRedirect
from django.shortcuts import render, HttpResponse, redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.generic import ListView
from mudra.constants import scheme
from difflib import SequenceMatcher
from django.views.decorators.csrf import csrf_exempt
from rest_framework.utils import json
from django.http import JsonResponse

from .models import AutoApproved, OverdueReport
from .tasks import test_cahnge

from django.views import generic
from django_pandas.io import read_frame

RBLTransaction = apps.get_model('Payments', 'RBLTransaction')
Payments = apps.get_model('Payments', 'Payment')
Bank = apps.get_model('User', 'Bank')
Address = apps.get_model('User', 'Address')
Reference = apps.get_model('User', 'Reference')
Document = apps.get_model('User', 'Documents')
Professional = apps.get_model('User', 'Professional')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
LoanBanner = apps.get_model('LoanAppData', 'LoanBanner')
LoanAcknowledgement = apps.get_model('LoanAppData', 'LoanAcknowledgement')
PincodeMaster = apps.get_model('StaticData', 'PincodeMaster')
EquiFaxMaster = apps.get_model('Equfix', 'EquiFaxMaster')
EquiFaxScoreDetails = apps.get_model('Equfix', 'EquiFaxScoreDetails')
Subscription = apps.get_model("User", "Subscription")
Recovery = apps.get_model('Recovery', 'Recovery')
SmsStringMaster = apps.get_model('IcomeExpense', 'SmsStringMaster')
LoanPayment = apps.get_model('LoanAppData', 'loanapplication_repayment_amount')
RazorpayOrder = apps.get_model('LoanAppData', 'RazorpayOrder')
ApprovedLoans = apps.get_model('LoanAppData', 'ApprovedLoans')
OrderDetails = apps.get_model('ecom', 'OrderDetails')

Credit = apps.get_model('ecom', 'Credit')
UserWallet = apps.get_model('ecom', 'UserWallet')

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class LoanApplicationView(generic.ListView):
    '''
    Template : userloan_applications_list.html
    shows user loan applications in details
    '''
    template_name = "admin/UserAccounts/userloan_applications_list_new.html"
    title = ' User Loan Application'

    def get(self, request, date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        date_list = date.split('-')
        y = date_list[0]

        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}-{}'.format(y, m, d)
        if len(date_list[2]) == 2:
            d = date_list[2]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)

        loan_users = LoanApplication.objects.filter(status="SUBMITTED").filter(
            created_date__startswith=new_date).order_by('-id')
        ctx['users'] = loan_users
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class LoanApplicationProcessView(generic.ListView):
    '''
    Template : userloan_applications_list.html
    shows user loan applications in details
    '''
    template_name = "admin/UserAccounts/userloan_applications_list_process_new.html"
    title = ' User Loan Application'

    def get(self, request, date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        date_list = date.split('-')
        y = date_list[0]

        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}-{}'.format(y, m, d)
        if len(date_list[2]) == 2:
            d = date_list[2]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)

        loan_users = LoanApplication.objects.filter(status="PROCESS").filter(
            created_date__startswith=new_date).order_by('-id')
        ctx['users'] = loan_users
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class LoanApplicationPreApprovedView(generic.ListView):
    '''
    Template : userloan_applications_list.html
    shows user loan applications in details
    '''
    template_name = "admin/UserAccounts/userloan_applications_list_pre_approved_new.html"
    title = ' User Loan Application'

    def get(self, request, date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        date_list = date.split('-')
        y = date_list[0]

        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}-{}'.format(y, m, d)
        if len(date_list[2]) == 2:
            d = date_list[2]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)
        loan_users = LoanApplication.objects.filter(status="PRE_APPROVED").filter(
            created_date__startswith=new_date).order_by('-id')
        ctx['users'] = loan_users

        return render(request, self.template_name, ctx)


from django_pandas.io import read_frame
import pandas as pd


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class LoanApplicationPreApprovedViewNew(generic.ListView):
    '''
    Template : userloan_applications_list.html
    shows user loan applications by date
    '''
    template_name = "admin/UserAccounts/userloan_application_list_new_pre_approved_new.html"
    title = ' User Loan Application new'

    def get(self, request, UpdateMessage="", *args, **kwargs):
        day_wise_application_count_dict = {}
        try:
            ctx = {}
            loan_users = LoanApplication.objects.filter(status="PRE_APPROVED")
            dataframe = read_frame(loan_users)
            dataframe['YearMonthDay'] = pd.to_datetime(dataframe['created_date']).apply(
                lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
            day_wise_application_count = \
                dataframe.groupby(dataframe['YearMonthDay'])[
                    'loan_application_id'].count().sort_index(axis=0)
            day_wise_application_count_dict = dict(day_wise_application_count)
        except:
            pass
        ctx['application_details'] = day_wise_application_count_dict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class LoanApplicationProcessViewNew(generic.ListView):
    '''
    Template : userloan_applications_list.html
    shows user loan applications by date
    '''
    template_name = "admin/UserAccounts/userloan_application_list_new_process_new.html"
    title = ' User Loan Application new'

    def get(self, request, UpdateMessage="", *args, **kwargs):
        day_wise_application_count_dict = {}
        try:
            ctx = {}
            loan_users = LoanApplication.objects.filter(status="PROCESS")
            dataframe = read_frame(loan_users)
            dataframe['YearMonthDay'] = pd.to_datetime(dataframe['created_date']).apply(
                lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
            day_wise_application_count = \
                dataframe.groupby(dataframe['YearMonthDay'])[
                    'loan_application_id'].count().sort_index(axis=0)
            day_wise_application_count_dict = dict(day_wise_application_count)
        except:
            pass
        ctx['application_details'] = day_wise_application_count_dict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class LoanApplicationViewNew(generic.ListView):
    '''
    Template : userloan_applications_list.html
    shows user loan applications by date
    '''
    template_name = "admin/UserAccounts/userloan_application_list_new1.html"
    title = ' User Loan Application new'

    def get(self, request, UpdateMessage="", *args, **kwargs):
        day_wise_application_count_dict = {}
        try:
            ctx = {}
            loan_users = LoanApplication.objects.filter(status="SUBMITTED")
            dataframe = read_frame(loan_users)
            dataframe['YearMonthDay'] = pd.to_datetime(dataframe['created_date']).apply(
                lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
            day_wise_application_count = \
                dataframe.groupby(dataframe['YearMonthDay'])[
                    'loan_application_id'].count().sort_index(axis=0)
            day_wise_application_count_dict = dict(day_wise_application_count)
        except:
            pass
        ctx['application_details'] = day_wise_application_count_dict
        return render(request, self.template_name, ctx)


def loan_application_dpd(user_id):
    Today_date = datetime.datetime.now()
    int_dbd_total = 0
    try:
        user = LoanApplication.objects.filter(user_id=user_id, status__in=['APPROVED', 'COMPLETED'])
        for users in user:
            payment_return = users.repayment_amount.filter(status='success').order_by('-id').first()

            if users.status == 'APPROVED':
                defaulted_by_date = Today_date - users.loan_end_date
                int_dbd = defaulted_by_date.days
                if int_dbd > 0:
                    int_dbd_total += int_dbd
            else:
                defaulted_by_date = payment_return.date - users.loan_end_date
                int_dbd = defaulted_by_date.days
                if int_dbd > 0:
                    int_dbd_total += int_dbd
    except:
        pass
    return int_dbd_total


def per_pan_name_match(user_name, pancard_name):
    personal_name = str(user_name).split()
    pan_name = str(pancard_name).split()
    pan_len = len(pan_name)
    personal_len = len(personal_name)
    count = 0
    per = 0
    # if personal_len == pan_len:
    for i in range(0, pan_len):
        cou = 0
        for pe in personal_name:
            if pe.upper() == pan_name[i].upper():
                cou = cou + 1
        if cou > 0:
            count = count + 1

    if count > 0:
        per = count * 100 / pan_len

    return per


def check_order_details_defaulter_one(id):
    """
    parameter : order_details_id
    This function return order overdue amount  by using get_amount_paid_for_order_details_1 function
    """
    rep = []
    slb = []
    dpd_count = 0
    get_order_details = OrderDetails.objects.filter(id=id).exclude(status="initiated").exclude(
        credit_status__in=["Failed"])
    if get_order_details:
        if get_order_details[0].credit:
            order_date = get_order_details[0].created_date
            # This function gives slabs of installments for this order.
            from ecom.views import decide_slabs_of_credit_1
            slabs = decide_slabs_of_credit_1(get_order_details[0].id)
            repayment = get_order_details[0].payment.filter(product_type="Ecom", status='success',
                                                            category="Repayment")
            re_amount = 0
            # re_list = []
            for re in repayment:
                re_list = []
                re_amount += re.amount
                re_list.append(re.date)
                re_list.append(re_amount)
                rep.append(re_list)
            if slabs:
                new_dict = {}
                new_amount = 0

                for date, amount in slabs.items():
                    s = []
                    new_amount += amount
                    new_dict.update({date: new_amount})
                    s.append(date)
                    s.append(new_amount)
                    slb.append(s)
                # if rep:

                for d in slb:
                    date = d[0]
                    amount = d[1]
                    count = 0
                    i = 0
                    if rep and rep[0]:
                        for re in rep:
                            if date < re[0]:
                                if amount <= re[1]:
                                    defaulted_by_date = re[0] - date
                                    count = defaulted_by_date.days
                                    if count == 0:
                                        # payment done on the same day
                                        dpd_in_seconds = defaulted_by_date.total_seconds()

                                        if dpd_in_seconds > 0:
                                            dpd_count += 1
                                    if count > 0:
                                        dpd_count += count
                                    if amount == re[1]:
                                        rep.remove(re)
                                    break
                                elif amount >= re[1]:
                                    dat_1 = datetime.datetime.now()
                                    defaulted_by_date = dat_1 - date
                                    count = defaulted_by_date.days
                                    if count == 0:
                                        # payment done on the same day
                                        dpd_in_seconds = defaulted_by_date.total_seconds()

                                        if dpd_in_seconds > 0:
                                            dpd_count += 1
                                    if count > 0:
                                        dpd_count += count
                                    rep.remove(re)
                                    break


                            elif date > re[0]:
                                if amount > re[1]:
                                    dat_1 = datetime.datetime.now()
                                    defaulted_by_date = dat_1 - date
                                    count = defaulted_by_date.days
                                    if count == 0:
                                        # payment done on the same day
                                        dpd_in_seconds = defaulted_by_date.total_seconds()

                                        if dpd_in_seconds > 0:
                                            dpd_count += 1
                                    if count > 0:
                                        dpd_count += count
                                    rep.remove(re)
                                elif amount <= re[1]:
                                    dat_1 = datetime.datetime.now()
                                    defaulted_by_date = re[0] - date
                                    count = defaulted_by_date.days
                                    if count == 0:
                                        # payment done on the same day
                                        dpd_in_seconds = defaulted_by_date.total_seconds()

                                        if dpd_in_seconds > 0:
                                            dpd_count += 1
                                    if count > 0:
                                        dpd_count += count
                                    if amount == re[1]:
                                        rep.remove(re)
                                # break
                    else:
                        dat = datetime.datetime.now()
                        if date < dat and amount > 0:
                            defaulted_by_date = dat - date
                            count = defaulted_by_date.days
                            if count == 0:
                                dpd_in_seconds = defaulted_by_date.total_seconds()

                                if dpd_in_seconds > 0:
                                    dpd_count += 1
                            if count > 0:
                                dpd_count += count

    return dpd_count


def delay_days(request):
    user_id = request.GET.get('userid', None)
    print("user_id", user_id)
    ctx = {}
    total_order = 0
    cash_order = 0
    credit_order = 0
    cod_order = 0
    delay_days = 0
    orderdetails = OrderDetails.objects.filter(user_id=user_id).exclude(status="initiated")
    if orderdetails:
        for order in orderdetails:
            total_order += 1
            if order.payment_mode == "cash":
                cash_order += 1
            elif order.payment_mode == "cod":
                cod_order += 1
            elif order.payment_mode == "credit":
                credit_order += 1
                dpd = check_order_details_defaulter_one(order.id)
                delay_days += dpd

    ctx["total_order"] = total_order
    ctx["cash_order"] = cash_order
    ctx["credit_order"] = credit_order
    ctx["cod_order"] = cod_order
    ctx["delay_days"] = delay_days

    return JsonResponse(ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class UserAllDetails(generic.ListView):
    """
        Template : user_all_details.html
        shows user loan application
    """
    login_required = True
    template_name = "admin/UserAccounts/user_all_details_new.html"
    title = 'User Details'
    pancount = 0

    def get(self, request, id):
        ctx = {}
        try:
            import json
            loan_pdf = LoanAcknowledgement.objects.filter(loan_transaction_id=id).last()
            if loan_pdf:
                ctx["file"] = loan_pdf.acknowledgement_pdf

            loan_users = LoanApplication.objects.select_related('verified_by', 'user', 'user_professional',
                                                                'user_personal', 'aadhar_id', 'bank_sts_id',
                                                                'salary_slip_id', 'company_id', 'self_video',
                                                                'current_address', 'permanent_address', 'reference_id',
                                                                'bank_id', 'cibil_id', 'device_data_id').get(id=id)

            pan_card_number = loan_users.user_personal.pan_number
            dict_param = {
                'pan_card': pan_card_number,
                'username': "creditkartpan",
                'password': "credit@123",
            }
            json_body = json.dumps(dict_param)
            prod_url = 'https://mudrakwik.anandfin.com/api/pancard-validate-creditkart/'
            headers = {
                'content-type': "application/json",
                'cache-control': "no-cache",
            }
            response = requests.post(prod_url, data=json_body, headers=headers, verify=False)
            responce_js = response.json()
            pancard_name = responce_js["name"]
            user_name = loan_users.user_personal.name
            per = per_pan_name_match(user_name, pancard_name)

            pan_card_no = pan_no_check(loan_users.user.id, pan_card_number)

            ctx["pan_card_no_per"] = pan_card_no["number_count"]
            ctx["pan_card_no"] = pan_card_no["pan_number"]

            pincodeno = pincode_number(loan_users.user.id, loan_users.current_address.pin_code)
            ctx["pincode_number_count"] = pincodeno["number_count"]
            ctx["pincode_no"] = pincodeno["pincode_no"]

            monthincome = month_income(loan_users.user.id, loan_users.user_personal.monthly_income)
            ctx["mi_number_count"] = monthincome["number_count"]
            ctx["monthly_income"] = monthincome["month_income"]

            if loan_users:
                approved_amount = loan_users.approved_amount
                ctx['approved_amount'] = approved_amount
                balance_credit = 0
                used_credit = 0
                credit_got = 0
                get_credit = Credit.objects.filter(user=loan_users.user).exclude(status="initiated").order_by(
                    'id')
                if get_credit:
                    last_credit = get_credit.last()
                    balance_credit = last_credit.balance_credit
                    used_credit = last_credit.used_credit
                    credit_got = last_credit.credit
                    ctx['balance_credit'] = balance_credit
                    ctx['used_credit'] = used_credit
                    ctx['credit_got'] = credit_got
                    ctx['user_credits'] = get_credit
                from .loan_funcations import get_approved_amount_list
                approved_amount_list = get_approved_amount_list()
                ctx['approved_amount_list'] = approved_amount_list

                disbursed_amount_details = loan_users.disbursed_amount.all()
                if disbursed_amount_details:
                    ctx['disbursed_amount_details'] = disbursed_amount_details

                repayment_amount_details = loan_users.repayment_amount.all()
                if repayment_amount_details:
                    ctx['repayment_amount_details'] = repayment_amount_details

                balance_amount = 0
                wallet_obj = UserWallet.objects.filter(user=loan_users.user).exclude(status="initiated").order_by(
                    '-id')
                if wallet_obj:
                    last_wallet = wallet_obj.first()
                    balance_amount = last_wallet.balance_amount
                    ctx['balance_amount'] = balance_amount
                    ctx['wallet'] = wallet_obj

            try:
                company_id = loan_users.company_id.path
                company_id = botofile.get_url(company_id)
            except:
                company_id = None

            try:
                pan = loan_users.pancard_id.path
                pan = botofile.get_url(pan)
            except:
                pan = None

            try:
                salary_slip = loan_users.salary_slip_id.path
                salary_slip = botofile.get_url(salary_slip)
            except:
                salary_slip = None

            try:
                profile_images = loan_users.self_video.path
                profile_images = botofile.get_url(profile_images)
            except:
                profile_images = None

            equfix_score = 0
            user_value = EquiFaxMaster.objects.values_list('id').filter(user_id=loan_users.user_id).order_by('-id')
            if user_value:
                score_value = EquiFaxScoreDetails.objects.values_list('score_value').filter(
                    equifax_id=user_value[0][0]).order_by('-id')
                if score_value:
                    equfix_score = score_value[0][0]

            loan_count = 0
            repayment_txc_count = 0
            loan_ids = LoanApplication.objects.values_list('id', 'loan_application_id', 'repayment_amount').order_by(
                'loan_application_id').distinct('loan_application_id').filter(user_id=loan_users.user_id,
                                                                              status__in=['APPROVED', 'COMPLETED'])
            loan_count = len(loan_ids)
            try:
                for id in loan_ids:
                    if id[2] is not None:
                        loan_rep = LoanApplication.objects.filter(loan_application_id=id[1])
                        loan_payment_count = loan_rep[0].repayment_amount.filter(
                            status='success').count()
                        repayment_txc_count += loan_payment_count
                    else:
                        repayment_txc_count = 0
                        # TODO Raise Error
                        pass
            except:
                pass
            if loan_users.overdue:
                overdue = "This profile is under Overdue with Count :  " + str(loan_users.overdue)
            else:
                overdue = ""

            # Seraches pancard for this number
            pancount = PancardMatch(loan_users.user_personal.pan_number)

            current_address = loan_users.current_address

            from .loan_funcations import loan_amount_slab
            slabs_data = loan_amount_slab(loan_users.id)
            if slabs_data:
                ctx['payment_slab'] = slabs_data

            ctx['pan'] = pan
            ctx['pancard_name'] = pancard_name
            ctx['pancard_matching_percentage'] = round(per, 2)
            ctx['company_id'] = company_id
            ctx['salary_slip'] = salary_slip

            ctx['pancount'] = pancount

            ctx['users'] = loan_users
            ctx['users_images'] = profile_images
            ctx['overdue_count'] = overdue

            ctx['smscount'] = loan_users.total_sms
            ctx['user_dpd'] = loan_users.dpd
            ctx['equfix_score'] = equfix_score
            user_ref_list = []
            default_users = []
            # Generating user dpd list
            dpd_list = []
            loan_userss = LoanApplication.objects.values_list('loan_application_id', 'dpd', 'loan_scheme_id',
                                                              'status', 'approved_amount').filter(
                user=loan_users.user_id,
                status__in=['APPROVED', 'COMPLETED'])
            for i in loan_userss:
                dpd_list.append([i[0], i[1], i[2], i[3], i[4]])
            if dpd_list is None:
                dpd_list = None
            ctx['user_loancount_list'] = loan_count
            ctx['user_transaction_list'] = repayment_txc_count
            ctx['balance'] = 0
            ctx['dpd_list'] = dpd_list
            ctx['NOT_FOUND'] = get_reference_details(str(loan_users.loan_application_id))
            return render(request, self.template_name, ctx)

        except Exception as e:
            print("------USer all details exc", e)
            print('-----------in exception----------')
            print(e.args)
            import os
            import sys
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class UserAllDetailsProcess(generic.ListView):
    """
        Template : user_all_details.html
        shows user loan application
    """
    login_required = True
    template_name = "admin/UserAccounts/user_all_details_process_new.html"
    title = 'User Details'
    pancount = 0

    def get(self, request, id):
        ctx = {}
        try:
            import json
            loan_pdf = LoanAcknowledgement.objects.filter(loan_transaction_id=id).last()
            if loan_pdf:
                ctx["file"] = loan_pdf.acknowledgement_pdf
            loan_users = LoanApplication.objects.select_related('verified_by', 'user', 'user_professional',
                                                                'user_personal', 'aadhar_id', 'bank_sts_id',
                                                                'salary_slip_id', 'company_id', 'self_video',
                                                                'current_address', 'permanent_address', 'reference_id',
                                                                'bank_id', 'cibil_id', 'device_data_id').get(id=id)
            pan_card_number = loan_users.user_personal.pan_number
            dict_param = {
                'pan_card': pan_card_number,
                'username': "creditkartpan",
                'password': "credit@123",
            }
            json_body = json.dumps(dict_param)
            prod_url = 'https://mudrakwik.anandfin.com/api/pancard-validate-creditkart/'
            headers = {
                'content-type': "application/json",
                'cache-control': "no-cache",
            }
            response = requests.post(prod_url, data=json_body, headers=headers, verify=False)
            responce_js = response.json()
            pancard_name = responce_js["name"]
            user_name = loan_users.user_personal.name
            per = per_pan_name_match(user_name, pancard_name)

            pan_card_no = pan_no_check(loan_users.user.id, pan_card_number)

            ctx["pan_card_no_per"] = pan_card_no["number_count"]
            ctx["pan_card_no"] = pan_card_no["pan_number"]

            pincodeno = pincode_number(loan_users.user.id, loan_users.current_address.pin_code)
            ctx["pincode_number_count"] = pincodeno["number_count"]
            ctx["pincode_no"] = pincodeno["pincode_no"]

            monthincome = month_income(loan_users.user.id, loan_users.user_personal.monthly_income)
            ctx["mi_number_count"] = monthincome["number_count"]
            ctx["monthly_income"] = monthincome["month_income"]

            if loan_users:
                approved_amount = loan_users.approved_amount
                ctx['approved_amount'] = approved_amount
                balance_credit = 0
                used_credit = 0
                credit_got = 0
                get_credit = Credit.objects.filter(user=loan_users.user).exclude(status="initiated").order_by(
                    'id')
                if get_credit:
                    last_credit = get_credit.last()
                    balance_credit = last_credit.balance_credit
                    used_credit = last_credit.used_credit
                    credit_got = last_credit.credit
                    ctx['balance_credit'] = balance_credit
                    ctx['used_credit'] = used_credit
                    ctx['credit_got'] = credit_got
                    ctx['user_credits'] = get_credit
                from .loan_funcations import get_approved_amount_list
                approved_amount_list = get_approved_amount_list()
                ctx['approved_amount_list'] = approved_amount_list

                disbursed_amount_details = loan_users.disbursed_amount.all()
                if disbursed_amount_details:
                    ctx['disbursed_amount_details'] = disbursed_amount_details

                repayment_amount_details = loan_users.repayment_amount.all()
                if repayment_amount_details:
                    ctx['repayment_amount_details'] = repayment_amount_details

                balance_amount = 0
                wallet_obj = UserWallet.objects.filter(user=loan_users.user).exclude(status="initiated").order_by(
                    '-id')
                if wallet_obj:
                    last_wallet = wallet_obj.first()
                    balance_amount = last_wallet.balance_amount
                    ctx['balance_amount'] = balance_amount
                    ctx['wallet'] = wallet_obj

            try:
                company_id = loan_users.company_id.path
                company_id = botofile.get_url(company_id)
            except:
                company_id = None

            try:
                pan = loan_users.pancard_id.path
                pan = botofile.get_url(pan)
            except:
                pan = None

            try:
                salary_slip = loan_users.salary_slip_id.path
                salary_slip = botofile.get_url(salary_slip)
            except:
                salary_slip = None

            try:
                profile_images = loan_users.self_video.path
                profile_images = botofile.get_url(profile_images)
            except:
                profile_images = None

            equfix_score = 0
            user_value = EquiFaxMaster.objects.values_list('id').filter(user_id=loan_users.user_id).order_by('-id')
            if user_value:
                score_value = EquiFaxScoreDetails.objects.values_list('score_value').filter(
                    equifax_id=user_value[0][0]).order_by('-id')
                if score_value:
                    equfix_score = score_value[0][0]

            loan_count = 0
            repayment_txc_count = 0
            loan_ids = LoanApplication.objects.values_list('id', 'loan_application_id', 'repayment_amount').order_by(
                'loan_application_id').distinct('loan_application_id').filter(user_id=loan_users.user_id,
                                                                              status__in=['APPROVED', 'COMPLETED'])
            loan_count = len(loan_ids)
            try:
                for id in loan_ids:
                    if id[2] is not None:
                        loan_rep = LoanApplication.objects.filter(loan_application_id=id[1])
                        loan_payment_count = loan_rep[0].repayment_amount.filter(
                            status='success').count()
                        repayment_txc_count += loan_payment_count
                    else:
                        repayment_txc_count = 0
                        # TODO Raise Error
                        pass
            except:
                pass
            if loan_users.overdue:
                overdue = "This profile is under Overdue with Count :  " + str(loan_users.overdue)
            else:
                overdue = ""

            # Seraches pancard for this number
            pancount = PancardMatch(loan_users.user_personal.pan_number)

            current_address = loan_users.current_address

            from .loan_funcations import loan_amount_slab
            slabs_data = loan_amount_slab(loan_users.id)
            if slabs_data:
                ctx['payment_slab'] = slabs_data

            ctx['pan'] = pan
            ctx['pancard_name'] = pancard_name
            ctx['pancard_matching_percentage'] = round(per, 2)
            ctx['company_id'] = company_id
            ctx['salary_slip'] = salary_slip

            ctx['pancount'] = pancount

            ctx['users'] = loan_users
            ctx['users_images'] = profile_images
            ctx['overdue_count'] = overdue

            ctx['smscount'] = loan_users.total_sms
            ctx['user_dpd'] = loan_users.dpd
            ctx['equfix_score'] = equfix_score
            user_ref_list = []
            default_users = []
            # Generating user dpd list
            dpd_list = []
            loan_userss = LoanApplication.objects.values_list('loan_application_id', 'dpd', 'loan_scheme_id',
                                                              'status', 'approved_amount').filter(
                user=loan_users.user_id,
                status__in=['APPROVED', 'COMPLETED'])
            for i in loan_userss:
                dpd_list.append([i[0], i[1], i[2], i[3], i[4]])
            if dpd_list is None:
                dpd_list = None
            ctx['user_loancount_list'] = loan_count
            ctx['user_transaction_list'] = repayment_txc_count
            ctx['balance'] = 0
            ctx['dpd_list'] = dpd_list
            ctx['NOT_FOUND'] = get_reference_details(str(loan_users.loan_application_id))
            return render(request, self.template_name, ctx)

        except Exception as e:
            print(e.args)
            import os
            import sys
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class UserAllDetailsPreApproved(generic.ListView):
    """
        Template : user_all_details.html
        shows user loan application
    """
    login_required = True
    template_name = "admin/UserAccounts/user_all_details_pre_approved_new.html"
    title = 'User Details'
    pancount = 0

    def get(self, request, id):
        ctx = {}
        try:
            import json
            loan_pdf = LoanAcknowledgement.objects.filter(loan_transaction_id=id).last()
            if loan_pdf:
                ctx["file"] = loan_pdf.acknowledgement_pdf
            loan_users = LoanApplication.objects.select_related('verified_by', 'user', 'user_professional',
                                                                'user_personal', 'aadhar_id', 'bank_sts_id',
                                                                'salary_slip_id', 'company_id', 'self_video',
                                                                'current_address', 'permanent_address', 'reference_id',
                                                                'bank_id', 'cibil_id', 'device_data_id').get(id=id)
            pan_card_number = loan_users.user_personal.pan_number
            dict_param = {
                'pan_card': pan_card_number,
                'username': "creditkartpan",
                'password': "credit@123",
            }
            json_body = json.dumps(dict_param)
            prod_url = 'https://mudrakwik.anandfin.com/api/pancard-validate-creditkart/'
            headers = {
                'content-type': "application/json",
                'cache-control': "no-cache",
            }
            response = requests.post(prod_url, data=json_body, headers=headers, verify=False)
            responce_js = response.json()
            pancard_name = responce_js["name"]

            user_name = loan_users.user_personal.name
            per = per_pan_name_match(user_name, pancard_name)

            pan_card_no = pan_no_check(loan_users.user.id, pan_card_number)

            ctx["pan_card_no_per"] = pan_card_no["number_count"]
            ctx["pan_card_no"] = pan_card_no["pan_number"]

            pincodeno = pincode_number(loan_users.user.id, loan_users.current_address.pin_code)
            ctx["pincode_number_count"] = pincodeno["number_count"]
            ctx["pincode_no"] = pincodeno["pincode_no"]

            monthincome = month_income(loan_users.user.id, loan_users.user_personal.monthly_income)
            ctx["mi_number_count"] = monthincome["number_count"]
            ctx["monthly_income"] = monthincome["month_income"]

            if loan_users:
                approved_amount = loan_users.approved_amount
                ctx['approved_amount'] = approved_amount
                balance_credit = 0
                used_credit = 0
                credit_got = 0
                get_credit = Credit.objects.filter(user=loan_users.user).exclude(status="initiated").order_by(
                    'id')
                if get_credit:
                    last_credit = get_credit.last()
                    balance_credit = last_credit.balance_credit
                    used_credit = last_credit.used_credit
                    credit_got = last_credit.credit
                    ctx['balance_credit'] = balance_credit
                    ctx['used_credit'] = used_credit
                    ctx['credit_got'] = credit_got
                    ctx['user_credits'] = get_credit

                from .loan_funcations import get_approved_amount_list
                approved_amount_list = get_approved_amount_list()
                ctx['approved_amount_list'] = approved_amount_list

                disbursed_amount_details = loan_users.disbursed_amount.all()
                if disbursed_amount_details:
                    ctx['disbursed_amount_details'] = disbursed_amount_details

                repayment_amount_details = loan_users.repayment_amount.all()
                if repayment_amount_details:
                    ctx['repayment_amount_details'] = repayment_amount_details

                balance_amount = 0
                wallet_obj = UserWallet.objects.filter(user=loan_users.user).exclude(status="initiated").order_by(
                    '-id')
                if wallet_obj:
                    last_wallet = wallet_obj.first()
                    balance_amount = last_wallet.balance_amount
                    ctx['balance_amount'] = balance_amount
                    ctx['wallet'] = wallet_obj

            try:
                company_id = loan_users.company_id.path
                company_id = botofile.get_url(company_id)
            except:
                company_id = None

            try:
                pan = loan_users.pancard_id.path
                pan = botofile.get_url(pan)
            except:
                pan = None

            try:
                salary_slip = loan_users.salary_slip_id.path
                salary_slip = botofile.get_url(salary_slip)
            except:
                salary_slip = None

            try:
                profile_images = loan_users.self_video.path
                profile_images = botofile.get_url(profile_images)
            except:
                profile_images = None

            equfix_score = 0
            user_value = EquiFaxMaster.objects.values_list('id').filter(user_id=loan_users.user_id).order_by('-id')
            if user_value:
                score_value = EquiFaxScoreDetails.objects.values_list('score_value').filter(
                    equifax_id=user_value[0][0]).order_by('-id')
                if score_value:
                    equfix_score = score_value[0][0]

            loan_count = 0
            repayment_txc_count = 0
            loan_ids = LoanApplication.objects.values_list('id', 'loan_application_id', 'repayment_amount').order_by(
                'loan_application_id').distinct('loan_application_id').filter(user_id=loan_users.user_id,
                                                                              status__in=['APPROVED', 'COMPLETED'])
            loan_count = len(loan_ids)
            try:
                for id in loan_ids:
                    if id[2] is not None:
                        loan_rep = LoanApplication.objects.filter(loan_application_id=id[1])
                        loan_payment_count = loan_rep[0].repayment_amount.filter(
                            status='success').count()
                        repayment_txc_count += loan_payment_count
                    else:
                        repayment_txc_count = 0
                        # TODO Raise Error
                        pass
            except:
                pass
            if loan_users.overdue:
                overdue = "This profile is under Overdue with Count :  " + str(loan_users.overdue)
            else:
                overdue = ""

            # Seraches pancard for this number
            pancount = PancardMatch(loan_users.user_personal.pan_number)

            current_address = loan_users.current_address

            from .loan_funcations import loan_amount_slab
            slabs_data = loan_amount_slab(loan_users.id)
            if slabs_data:
                ctx['payment_slab'] = slabs_data

            ctx['pan'] = pan
            ctx['pancard_name'] = pancard_name
            ctx['pancard_matching_percentage'] = round(per, 2)
            ctx['company_id'] = company_id
            ctx['salary_slip'] = salary_slip

            ctx['pancount'] = pancount

            ctx['users'] = loan_users
            ctx['users_images'] = profile_images
            ctx['overdue_count'] = overdue

            ctx['smscount'] = loan_users.total_sms
            ctx['user_dpd'] = loan_users.dpd
            ctx['equfix_score'] = equfix_score
            user_ref_list = []
            default_users = []
            # Generating user dpd list
            dpd_list = []
            loan_userss = LoanApplication.objects.values_list('loan_application_id', 'dpd', 'loan_scheme_id',
                                                              'status', 'approved_amount').filter(
                user=loan_users.user_id,
                status__in=['APPROVED', 'COMPLETED'])
            for i in loan_userss:
                dpd_list.append([i[0], i[1], i[2], i[3], i[4]])
            if dpd_list is None:
                dpd_list = None
            ctx['user_loancount_list'] = loan_count
            ctx['user_transaction_list'] = repayment_txc_count
            ctx['balance'] = 0
            ctx['dpd_list'] = dpd_list
            ctx['NOT_FOUND'] = get_reference_details(str(loan_users.loan_application_id))

            return render(request, self.template_name, ctx)

        except Exception as e:
            print(e.args)
            import os
            import sys
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


import logging

logger = logging.getLogger(__name__)


def rbl_save_data(request):
    '''
    Change Status
        if Status = Approved:
            create RBLTransaction object
            call rbl_worker()
            update LoanApplication object
            update auto approve
    :param request:
    :return:
    '''
    logger.info("Rbl save data called")
    UpdateMessage = ""
    # try:
    loan_application_id = request.POST['user_loan_application_id']
    status = request.POST['id_status']
    # calcelReason = request.POST['calcelReason']
    calcelReason = None
    reason = request.POST['Reason']
    approved_amount = None
    approved_amount_1 = request.POST['approved_amount']
    if approved_amount_1 != "None":
        approved_amount = approved_amount_1
    reason_text = request.POST['ReasonText']
    verified_id = request.user.id
    save_reason = None
    if (reason and reason is not None and reason != "None") and (reason_text and reason_text is not None):
        save_reason = reason + ":" + reason_text
    elif (reason and reason is not None and reason != "None"):
        save_reason = reason
    elif (reason_text and reason_text is not None):
        save_reason = reason_text
    onhold_time = request.POST['onhold_time']
    updatedate = datetime.datetime.now()
    obj = LoanApplication.objects.select_related('user', 'user_professional', 'user_personal', 'aadhar_id',
                                                 'bank_sts_id', 'salary_slip_id', 'company_id', 'self_video').get(
        loan_application_id=loan_application_id)
    date = str(obj.created_date)[:10]

    email_id = None
    UpdateMessage = ""
    if obj.status == status:
        UpdateMessage = "Already changed ... !"
        pass

    else:
        if status == 'APPROVED':
            date_1 = datetime.datetime.now()
            # loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
            #                                                                                           approved_amount=approved_amount,
            #                                                                                           verified_by_id=verified_id,
            #                                                                                           verified_date=date_1)
            loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
            if loan_obj:
                loan_obj[0].status = status
                loan_obj[0].approved_amount = approved_amount
                loan_obj[0].verified_by_id = verified_id
                loan_obj[0].verified_date = date_1
                loan_obj[0].save()

            try:

                loan_approved_entry(loan_obj[0].id, status)


            except Exception as e:
                # print("-----------------exc", e, e.args)
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                pass

            sms_message = "Dear valued customer,Congratulations! Your loan of Rs. " + str(
                approved_amount) + " has been approved. Please visit the Creditkart app."

            sms_kwrgs = {

                'sms_message': sms_message,

                # 'number': '7875583679'
                'number': str(obj.user.username)

            }

            sms(**sms_kwrgs)

            email_id_1 = 'test@thecreditkart.com'
            email_kwargs_1 = {

                'subject': 'CreditKart - User Approved',

                'to_email': email_id_1,

                'email_template': 'emails/loan/approved_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User Approved',

                },

            }

            # # email(**email_kwargs_1)

        elif status == 'PRE_APPROVED':
            rejection_date = datetime.datetime.now()

            UpdateMessage = "LOAN PRE APPROVED"
            # fcm_parameter_notification(**fmp_kwargs)

            email_id_1 = 'test@thecreditkart.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'CreditKart - User PRE APPROVED In',

                'to_email': email_id_1,

                'email_template': 'emails/loan/rejected_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User Rejected In',

                },

            }

            # email(**email_kwargs_1)

            # LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
            #                                                                                updated_date=updatedate,
            #                                                                                loan_start_date=updatedate,
            #                                                                                reason=save_reason,
            #                                                                                verified_date=updatedate,
            #                                                                                verified_by_id=verified_id)
            loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
            if loan_obj:
                loan_obj[0].status = status
                loan_obj[0].updated_date = updatedate
                loan_obj[0].loan_start_date = updatedate
                loan_obj[0].reason = save_reason
                loan_obj[0].verified_date = updatedate
                loan_obj[0].verified_by_id = verified_id
                loan_obj[0].save()




        elif status == 'CANCELLED':

            try:

                if calcelReason == "Company I'd":

                    reason = "Company i'd is missing/not clear"
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled because of your " + reason + ". Please upload the clear company I'd and re-apply."



                elif calcelReason == 'Salary slip':

                    reason = "Salary slip is Missing/Unclear/Not latest"
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled because of your " + reason + ". Please upload clear and Latest Salary slip and then reapply."


                elif calcelReason == 'Insufficient_credit_history':

                    # reason = "Insufficient Credit History"
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled because of your registered mobile doesn’t have enough banking transaction history. Please keep on using creditkart for shopping for next 15 days and then reapply."



                elif calcelReason == 'Selfie':

                    reason = "Selfie is missing/unclear"
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled because of your " + reason + ". Please upload the clear Selfie and reapply."



                else:
                    reason = ''
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled."

                cancelled_date = datetime.datetime.now()

                sms_kwrgs = {

                    'sms_message': sms_message,

                    # 'number': '7875583679'
                    'number': str(obj.user.username)

                }

                sms(**sms_kwrgs)

                email_kwargs = {

                    'subject': 'CreditKart - Loan Cancelled',

                    'to_email': email_id,

                    'email_template': 'emails/loan/cancelled_loan.html',

                    'data': {

                        'domain': Site.objects.get_current(),

                        'loan_instance': obj,

                        'cancelled_date': cancelled_date,

                        'reason': sms_message,

                    },

                }

                email(**email_kwargs)

                fmp_kwargs = {

                    'user_id': obj.user.id,

                    'title': 'Loan Cancelled',

                    'message': sms_message,

                    'type': 'loan_cancelled',

                    'to_store': True,

                    'notification_id': obj.id,

                    'notification_key': 'loan_id',

                }

                UpdateMessage = "LOAN CANCELLED"

                email_id_1 = 'test@thecreditkart.com'
                date_1 = datetime.datetime.now()
                email_kwargs_1 = {

                    'subject': 'CreditKart - User Cancelled',

                    'to_email': email_id_1,

                    'email_template': 'emails/loan/cancelled_loan.html',

                    'data': {

                        'domain': Site.objects.get_current(),

                        'loan_instance': obj,

                        'cancelled_date': date_1,

                        'reason': 'User Cancelled',

                    },

                }
                # LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
                #                                                                                updated_date=updatedate,
                #                                                                                loan_start_date=updatedate,
                #                                                                                reason=save_reason,
                #                                                                                verified_date=updatedate,
                #                                                                                verified_by_id=verified_id)
                loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
                if loan_obj:
                    loan_obj[0].status = status
                    loan_obj[0].updated_date = updatedate
                    loan_obj[0].loan_start_date = updatedate
                    loan_obj[0].reason = save_reason
                    loan_obj[0].verified_date = updatedate
                    loan_obj[0].verified_by_id = verified_id
                    loan_obj[0].save()
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                pass

        elif status == 'ON_HOLD':
            updatedate = datetime.datetime.now()

            email_kwargs = {
                'subject': 'CreditKart - Loan ON_HOLD',
                'to_email': email_id,
                'email_template': 'emails/loan/pending_loan.html',
                'data': {
                    'domain': Site.objects.get_current(),
                    'loan_instance': obj,
                    'rejection_date': updatedate,
                },
            }

            email(**email_kwargs)
            sms_message = "Dear customer,your profile doesn't match with our eligibility criteria, please re apply after " + onhold_time + " days.You may continue using other features."

            sms_kwrgs = {
                'sms_message': sms_message,
                'number': str(obj.user.username)
            }

            sms(**sms_kwrgs)

            fmp_kwargs = {
                'user_id': obj.user.id,
                'title': 'Loan ON_HOLD',
                'message': sms_message,
                'type': 'ON_HOLD',
                'to_store': True,
                'notification_id': obj.id,
                'notification_key': 'loan_id',
            }
            UpdateMessage = "LOAN On Hold"
            # fcm_parameter_notification(**fmp_kwargs)

            email_id_1 = 'test@thecreditkart.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'CreditKart - User ON_HOLD In',

                'to_email': email_id_1,

                'email_template': 'emails/loan/pending_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User ON_HOLD In',

                },

            }

            if onhold_time is None:
                # LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
                #                                                                                updated_date=updatedate,
                #                                                                                hold_days=0,
                #                                                                                loan_start_date=updatedate,
                #                                                                                reason=save_reason,
                #                                                                                verified_date=updatedate,
                #                                                                                verified_by_id=verified_id)
                loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
                if loan_obj:
                    loan_obj[0].status = status
                    loan_obj[0].updated_date = updatedate
                    loan_obj[0].hold_days = 0
                    loan_obj[0].loan_start_date = updatedate
                    loan_obj[0].reason = save_reason
                    loan_obj[0].verified_date = updatedate
                    loan_obj[0].verified_by_id = verified_id
                    loan_obj[0].save()
            else:
                # LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
                #                                                                                updated_date=updatedate,
                #                                                                                hold_days=onhold_time,
                #                                                                                loan_start_date=updatedate,
                #                                                                                reason=save_reason,
                #                                                                                verified_date=updatedate,
                #                                                                                verified_by_id=verified_id)
                loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)

                if loan_obj:
                    loan_obj[0].status = status
                    loan_obj[0].updated_date = updatedate
                    loan_obj[0].hold_days = onhold_time
                    loan_obj[0].loan_start_date = updatedate
                    loan_obj[0].reason = save_reason
                    loan_obj[0].verified_date = updatedate
                    loan_obj[0].verified_by_id = verified_id
                    loan_obj[0].save()

        elif status == 'REJECTED':
            rejection_date = datetime.datetime.now()

            email_kwargs = {
                'subject': 'CreditKart - Loan Rejected',
                'to_email': email_id,
                'email_template': 'emails/loan/rejected_loan.html',
                'data': {
                    'domain': Site.objects.get_current(),
                    'loan_instance': obj,
                    'rejection_date': rejection_date,
                },
            }

            email(**email_kwargs)
            sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been rejected."

            sms_kwrgs = {
                'sms_message': sms_message,
                'number': str(obj.user.username)
            }

            # sms(**sms_kwrgs)
            fmp_kwargs = {
                'user_id': obj.user.id,
                'title': 'Loan Rejected',
                'message': sms_message,
                'type': 'loan_rejected',
                'to_store': True,
                'notification_id': obj.id,
                'notification_key': 'loan_id',
            }
            UpdateMessage = "LOAN REJECTED"
            # fcm_parameter_notification(**fmp_kwargs)

            email_id_1 = 'test@thecreditkart.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'CreditKart - User Rejected In',

                'to_email': email_id_1,

                'email_template': 'emails/loan/rejected_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User Rejected In',

                },

            }

            # email(**email_kwargs_1)

            # LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
            #                                                                                updated_date=updatedate,
            #                                                                                loan_start_date=updatedate,
            #                                                                                reason=save_reason,
            #                                                                                verified_date=updatedate,
            #                                                                                verified_by_id=verified_id)
            loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
            if loan_obj:
                loan_obj[0].status = status
                loan_obj[0].updated_date = updatedate
                loan_obj[0].loan_start_date = updatedate
                loan_obj[0].reason = save_reason
                loan_obj[0].verified_date = updatedate
                loan_obj[0].verified_by_id = verified_id
                loan_obj[0].save()


        elif status == 'PENDING':

            email_kwargs = {
                'subject': 'CreditKart - Loan Pending',
                'to_email': email_id,
                'email_template': 'emails/loan/pending_loan.html',
                'data': {
                    'domain': Site.objects.get_current(),
                    'loan_instance': obj,
                },
            }

            email(**email_kwargs)

            sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + " is pending."

            sms_kwrgs = {
                'sms_message': sms_message,
                'number': str(obj.user.username)
            }

            sms(**sms_kwrgs)

            fmp_kwargs = {
                'user_id': obj.user.id,
                'title': 'Loan Pending',
                'message': sms_message,
                'type': 'loan_pending',
                'to_store': True,
                'notification_id': obj.id,
                'notification_key': 'loan_id',
            }
            UpdateMessage = "LOAN PENDING"

            email_id_1 = 'test@thecreditkart.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'CreditKart - User Pending',

                'to_email': email_id_1,

                'email_template': 'emails/loan/pending_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User Pending',

                },

            }

            # email(**email_kwargs_1)

            # fcm_parameter_notification(**fmp_kwargs)
            # LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
            #                                                                                updated_date=updatedate,
            #                                                                                loan_start_date=updatedate,
            #                                                                                reason=save_reason,
            #                                                                                verified_date=updatedate,
            #                                                                                verified_by_id=verified_id)
            loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
            if loan_obj:
                loan_obj[0].status = status
                loan_obj[0].updated_date = updatedate
                loan_obj[0].loan_start_date = updatedate
                loan_obj[0].reason = save_reason
                loan_obj[0].verified_date = updatedate
                loan_obj[0].verified_by_id = verified_id
                loan_obj[0].save()

        elif status == 'COMPLETED':

            sms_message = "We are happy to inform you that your loan application number " + obj.loan_application_id + " is completed."

            fmp_kwargs = {
                'user_id': obj.user.id,
                'title': 'Loan Completed',
                'message': sms_message,
                'type': 'loan_completed',
                'to_store': True,
                'notification_id': obj.id,
                'notification_key': 'loan_id',
            }
            UpdateMessage = "LOAN COMPLETED"
            # fcm_parameter_notification(**fmp_kwargs)
            email_id_1 = 'test@thecreditkart.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'CreditKart - User COMPLETED',

                'to_email': email_id_1,

                'email_template': 'emails/loan/pending_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User COMPLETED',

                },

            }

            # email(**email_kwargs_1)

            # loan_complete_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id).update(
            #     status=status,
            #     updated_date=updatedate,
            #     reason=save_reason,
            #     verified_date=updatedate,
            #     verified_by_id=verified_id)
            loan_complete_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
            if loan_complete_obj:
                loan_complete_obj[0].status = status
                loan_complete_obj[0].updated_date = updatedate
                loan_complete_obj[0].reason = save_reason
                loan_complete_obj[0].verified_date = updatedate
                loan_complete_obj[0].verified_by_id = verified_id
                loan_complete_obj[0].save()
            try:

                loan_approved_entry(loan_complete_obj[0].id, status)


            except Exception as e:
                # print("-----------------exc", e, e.args)
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                pass

    return HttpResponseRedirect(reverse('userloan_application_list1', kwargs={'date': date}))


def rbl_save_data_process(request):
    '''
    Change Status
        if Status = Approved:
            create RBLTransaction object
            call rbl_worker()
            update LoanApplication object
            update auto approve
    :param request:
    :return:
    '''
    logger.info("Rbl save data called")
    UpdateMessage = ""
    # try:
    loan_application_id = request.POST['user_loan_application_id']
    status = request.POST['id_status']
    # calcelReason = request.POST['calcelReason']
    calcelReason = None
    reason = request.POST['Reason']
    reason_text = request.POST['ReasonText']
    approved_amount = None
    approved_amount_1 = request.POST['approved_amount']
    if approved_amount_1 != "None":
        approved_amount = approved_amount_1
    verified_id = request.user.id
    save_reason = None
    if (reason and reason is not None and reason != "None") and (reason_text and reason_text is not None):
        save_reason = reason + ":" + reason_text
    elif (reason and reason is not None and reason != "None"):
        save_reason = reason
    elif (reason_text and reason_text is not None):
        save_reason = reason_text
    onhold_time = request.POST['onhold_time']
    updatedate = datetime.datetime.now()
    obj = LoanApplication.objects.select_related('user', 'user_professional', 'user_personal', 'aadhar_id',
                                                 'bank_sts_id', 'salary_slip_id', 'company_id', 'self_video').get(
        loan_application_id=loan_application_id)
    date = str(obj.created_date)[:10]

    email_id = None
    UpdateMessage = ""
    if obj.status == status:
        UpdateMessage = "Already changed ... !"
        pass

    else:
        if status == 'APPROVED':
            pass


        elif status == 'PRE_APPROVED':

            UpdateMessage = "LOAN PRE APPROVED"
            # fcm_parameter_notification(**fmp_kwargs)

            email_id_1 = 'test@thecreditkart.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'CreditKart - User PRE APPROVED In',

                'to_email': email_id_1,

                'email_template': 'emails/loan/rejected_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User Rejected In',

                },

            }

            # email(**email_kwargs_1)

            # LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
            #                                                                                updated_date=updatedate,
            #                                                                                loan_start_date=updatedate,
            #                                                                                reason=save_reason,
            #                                                                                verified_date=updatedate,
            #                                                                                verified_by_id=verified_id)
            loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
            if loan_obj:
                loan_obj[0].status = status
                loan_obj[0].updated_date = updatedate
                loan_obj[0].loan_start_date = updatedate
                loan_obj[0].reason = save_reason
                loan_obj[0].verified_date = updatedate
                loan_obj[0].verified_by_id = verified_id
                loan_obj[0].save()

        elif status == 'CANCELLED':

            try:

                if calcelReason == "Company I'd":

                    reason = "Company i'd is missing/not clear"
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled because of your " + reason + ". Please upload the clear company I'd and re-apply."



                elif calcelReason == 'Salary slip':

                    reason = "Salary slip is Missing/Unclear/Not latest"
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled because of your " + reason + ". Please upload clear and Latest Salary slip and then reapply."


                elif calcelReason == 'Insufficient_credit_history':

                    # reason = "Insufficient Credit History"
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled because of your registered mobile doesn’t have enough banking transaction history. Please keep on using creditkart for shopping for next 15 days and then reapply."


                elif calcelReason == 'Selfie':

                    reason = "Selfie is missing/unclear"
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled because of your " + reason + ". Please upload the clear Selfie and reapply."



                else:
                    reason = ''
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled."

                cancelled_date = datetime.datetime.now()

                sms_kwrgs = {

                    'sms_message': sms_message,
                    'number': str(obj.user.username)

                }

                sms(**sms_kwrgs)

                email_kwargs = {

                    'subject': 'CreditKart - Loan Cancelled',

                    'to_email': email_id,

                    'email_template': 'emails/loan/cancelled_loan.html',

                    'data': {

                        'domain': Site.objects.get_current(),

                        'loan_instance': obj,

                        'cancelled_date': cancelled_date,

                        'reason': sms_message,

                    },

                }

                email(**email_kwargs)

                fmp_kwargs = {

                    'user_id': obj.user.id,

                    'title': 'Loan Cancelled',

                    'message': sms_message,

                    'type': 'loan_cancelled',

                    'to_store': True,

                    'notification_id': obj.id,

                    'notification_key': 'loan_id',

                }

                UpdateMessage = "LOAN CANCELLED"

                email_id_1 = 'test@thecreditkart.com'
                date_1 = datetime.datetime.now()
                email_kwargs_1 = {

                    'subject': 'CreditKart - User Cancelled',

                    'to_email': email_id_1,

                    'email_template': 'emails/loan/cancelled_loan.html',

                    'data': {

                        'domain': Site.objects.get_current(),

                        'loan_instance': obj,

                        'cancelled_date': date_1,

                        'reason': 'User Cancelled',

                    },

                }
                # LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
                #                                                                                updated_date=updatedate,
                #                                                                                loan_start_date=updatedate,
                #                                                                                reason=save_reason,
                #                                                                                verified_date=updatedate,
                #                                                                                verified_by_id=verified_id)
                loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
                if loan_obj:
                    loan_obj[0].status = status
                    loan_obj[0].updated_date = updatedate
                    loan_obj[0].loan_start_date = updatedate
                    loan_obj[0].reason = save_reason
                    loan_obj[0].verified_date = updatedate
                    loan_obj[0].verified_by_id = verified_id
                    loan_obj[0].save()
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                pass

        elif status == 'ON_HOLD':
            updatedate = datetime.datetime.now()

            email_kwargs = {
                'subject': 'CreditKart - Loan ON_HOLD',
                'to_email': email_id,
                'email_template': 'emails/loan/pending_loan.html',
                'data': {
                    'domain': Site.objects.get_current(),
                    'loan_instance': obj,
                    'rejection_date': updatedate,
                },
            }

            email(**email_kwargs)
            sms_message = "Dear valued customer,your profile doesn't match with our eligibility criteria, please re apply after " + onhold_time + " days.You may continue using other features."

            sms_kwrgs = {
                'sms_message': sms_message,
                'number': str(obj.user.username)
            }

            sms(**sms_kwrgs)

            fmp_kwargs = {
                'user_id': obj.user.id,
                'title': 'Loan ON_HOLD',
                'message': sms_message,
                'type': 'ON_HOLD',
                'to_store': True,
                'notification_id': obj.id,
                'notification_key': 'loan_id',
            }
            UpdateMessage = "LOAN On Hold"
            # fcm_parameter_notification(**fmp_kwargs)

            email_id_1 = 'test@thecreditkart.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'CreditKart - User ON_HOLD In',

                'to_email': email_id_1,

                'email_template': 'emails/loan/pending_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User ON_HOLD In',

                },

            }

            if onhold_time is None:
                # LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
                #                                                                                updated_date=updatedate,
                #                                                                                hold_days=0,
                #                                                                                loan_start_date=updatedate,
                #                                                                                reason=save_reason,
                #                                                                                verified_date=updatedate,
                #                                                                                verified_by_id=verified_id)
                loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
                if loan_obj:
                    loan_obj[0].status = status
                    loan_obj[0].updated_date = updatedate
                    loan_obj[0].hold_days = 0
                    loan_obj[0].loan_start_date = updatedate
                    loan_obj[0].reason = save_reason
                    loan_obj[0].verified_date = updatedate
                    loan_obj[0].verified_by_id = verified_id
                    loan_obj[0].save()

            else:
                # LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
                #                                                                                updated_date=updatedate,
                #                                                                                hold_days=onhold_time,
                #                                                                                loan_start_date=updatedate,
                #                                                                                reason=save_reason,
                #                                                                                verified_date=updatedate,
                #                                                                                verified_by_id=verified_id)

                loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
                if loan_obj:
                    loan_obj[0].status = status
                    loan_obj[0].updated_date = updatedate
                    loan_obj[0].hold_days = onhold_time
                    loan_obj[0].loan_start_date = updatedate
                    loan_obj[0].reason = save_reason
                    loan_obj[0].verified_date = updatedate
                    loan_obj[0].verified_by_id = verified_id
                    loan_obj[0].save()

        elif status == 'REJECTED':
            rejection_date = datetime.datetime.now()

            email_kwargs = {
                'subject': 'CreditKart - Loan Rejected',
                'to_email': email_id,
                'email_template': 'emails/loan/rejected_loan.html',
                'data': {
                    'domain': Site.objects.get_current(),
                    'loan_instance': obj,
                    'rejection_date': rejection_date,
                },
            }

            email(**email_kwargs)
            sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been rejected."

            sms_kwrgs = {
                'sms_message': sms_message,
                'number': str(obj.user.username)
            }

            sms(**sms_kwrgs)
            fmp_kwargs = {
                'user_id': obj.user.id,
                'title': 'Loan Rejected',
                'message': sms_message,
                'type': 'loan_rejected',
                'to_store': True,
                'notification_id': obj.id,
                'notification_key': 'loan_id',
            }
            UpdateMessage = "LOAN REJECTED"
            # fcm_parameter_notification(**fmp_kwargs)

            email_id_1 = 'test@thecreditkart.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'CreditKart - User Rejected In',

                'to_email': email_id_1,

                'email_template': 'emails/loan/rejected_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User Rejected In',

                },

            }

            # LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
            #                                                                                updated_date=updatedate,
            #                                                                                loan_start_date=updatedate,
            #                                                                                reason=save_reason,
            #                                                                                verified_date=updatedate,
            #                                                                                verified_by_id=verified_id)
            loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
            if loan_obj:
                loan_obj[0].status = status
                loan_obj[0].updated_date = updatedate
                loan_obj[0].loan_start_date = updatedate
                loan_obj[0].reason = save_reason
                loan_obj[0].verified_date = updatedate
                loan_obj[0].verified_by_id = verified_id
                loan_obj[0].save()


        elif status == 'PENDING':

            email_kwargs = {
                'subject': 'CreditKart - Loan Pending',
                'to_email': email_id,
                'email_template': 'emails/loan/pending_loan.html',
                'data': {
                    'domain': Site.objects.get_current(),
                    'loan_instance': obj,
                },
            }

            email(**email_kwargs)

            sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + " is pending."

            sms_kwrgs = {
                'sms_message': sms_message,
                'number': str(obj.user.username)
            }

            sms(**sms_kwrgs)

            fmp_kwargs = {
                'user_id': obj.user.id,
                'title': 'Loan Pending',
                'message': sms_message,
                'type': 'loan_pending',
                'to_store': True,
                'notification_id': obj.id,
                'notification_key': 'loan_id',
            }
            UpdateMessage = "LOAN PENDING"

            email_id_1 = 'test@thecreditkart.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'CreditKart - User Pending',

                'to_email': email_id_1,

                'email_template': 'emails/loan/pending_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User Pending',

                },

            }

            # fcm_parameter_notification(**fmp_kwargs)
            # LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
            #                                                                                updated_date=updatedate,
            #                                                                                loan_start_date=updatedate,
            #                                                                                reason=save_reason,
            #                                                                                verified_date=updatedate,
            #                                                                                verified_by_id=verified_id)
            loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
            if loan_obj:
                loan_obj[0].status = status
                loan_obj[0].updated_date = updatedate
                loan_obj[0].loan_start_date = updatedate
                loan_obj[0].reason = save_reason
                loan_obj[0].verified_date = updatedate
                loan_obj[0].verified_by_id = verified_id
                loan_obj[0].save()

        elif status == 'COMPLETED':

            sms_message = "We are happy to inform you that your loan application number " + obj.loan_application_id + " is completed."

            fmp_kwargs = {
                'user_id': obj.user.id,
                'title': 'Loan Completed',
                'message': sms_message,
                'type': 'loan_completed',
                'to_store': True,
                'notification_id': obj.id,
                'notification_key': 'loan_id',
            }
            UpdateMessage = "LOAN COMPLETED"
            # fcm_parameter_notification(**fmp_kwargs)
            email_id_1 = 'test@thecreditkart.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'CreditKart - User COMPLETED',

                'to_email': email_id_1,

                'email_template': 'emails/loan/pending_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User COMPLETED',

                },

            }

            # email(**email_kwargs_1)

            # loan_complete_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id).update(
            #     status=status,
            #     updated_date=updatedate,
            #     reason=save_reason,
            #     verified_date=updatedate,
            #     verified_by_id=verified_id)
            loan_complete_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
            if loan_complete_obj:
                loan_complete_obj[0].status = status
                loan_complete_obj[0].updated_date = updatedate
                loan_complete_obj[0].reason = save_reason
                loan_complete_obj[0].verified_date = updatedate
                loan_complete_obj[0].verified_by_id = verified_id
                loan_complete_obj[0].save()

            try:

                loan_approved_entry(loan_complete_obj[0].id, status)


            except Exception as e:
                # print("-----------------exc", e, e.args)
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                pass

    return HttpResponseRedirect(reverse('userloan_application_list_process', kwargs={'date': date}))


def rbl_save_data_pre_approved(request):
    '''
    Change Status
        if Status = Approved:
            create RBLTransaction object
            call rbl_worker()
            update LoanApplication object
            update auto approve
    :param request:
    :return:
    '''
    logger.info("Rbl save data called")
    UpdateMessage = ""
    # try:
    loan_id = request.POST['loan_id']
    loan_application_id = request.POST['user_loan_application_id']
    status = request.POST['id_status']
    # calcelReason = request.POST['calcelReason']
    calcelReason = None
    reason = request.POST['Reason']
    reason_text = request.POST['ReasonText']
    approved_amount = None
    approved_amount_1 = request.POST['approved_amount']
    if approved_amount_1 != "None":
        approved_amount = approved_amount_1
    verified_id = request.user.id
    save_reason = None
    if (reason and reason is not None and reason != "None") and (reason_text and reason_text is not None):
        save_reason = reason + ":" + reason_text
    elif (reason and reason is not None and reason != "None"):
        save_reason = reason
    elif (reason_text and reason_text is not None):
        save_reason = reason_text
    onhold_time = request.POST['onhold_time']
    updatedate = datetime.datetime.now()
    obj = LoanApplication.objects.select_related('user', 'user_professional', 'user_personal', 'aadhar_id',
                                                 'bank_sts_id', 'salary_slip_id', 'company_id', 'self_video').get(
        id=loan_id,
        loan_application_id=loan_application_id)
    date = str(obj.created_date)[:10]

    email_id = None
    UpdateMessage = ""
    if obj.status == status:
        UpdateMessage = "Already changed ... !"
        pass

    else:
        if status == 'APPROVED':
            date_1 = datetime.datetime.now()
            # loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
            #                                                                                           approved_amount=approved_amount,
            #                                                                                           verified_by_id=verified_id,
            #                                                                                           verified_date=date_1)
            loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
            if loan_obj:
                loan_obj[0].status = status
                loan_obj[0].approved_amount = approved_amount
                loan_obj[0].verified_by_id = verified_id
                loan_obj[0].verified_date = date_1
                loan_obj[0].save()
            try:

                loan_approved_entry(loan_obj[0].id, status)


            except Exception as e:
                # print("-----------------exc", e, e.args)
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                pass

            sms_message = "Dear valued customer,Congratulations! Your loan of Rs." + str(
                approved_amount) + " has been approved. Please visit the Creditkart app."
            sms_kwrgs = {

                'sms_message': sms_message,
                'number': str(obj.user.username)

            }

            email_id_1 = 'test@thecreditkart.com'
            email_kwargs_1 = {

                'subject': 'CreditKart - User Approved',

                'to_email': email_id_1,

                'email_template': 'emails/loan/approved_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User Approved',

                },

            }

        elif status == 'PRE_APPROVED':

            rejection_date = datetime.datetime.now()

            UpdateMessage = "LOAN PRE APPROVED"
            # fcm_parameter_notification(**fmp_kwargs)

            email_id_1 = 'test@thecreditkart.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'CreditKart - User PRE APPROVED In',

                'to_email': email_id_1,

                'email_template': 'emails/loan/rejected_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User Rejected In',

                },

            }

            # LoanApplication.objects.filter(id=loan_id, loan_application_id=loan_application_id).update(status=status,
            #                                                                                            updated_date=updatedate,
            #                                                                                            loan_start_date=updatedate,
            #                                                                                            reason=save_reason,
            #                                                                                            verified_date=updatedate,
            #                                                                                            verified_by_id=verified_id,
            #                                                                                            approved_amount=approved_amount)
            loan_obj = LoanApplication.objects.filter(id=loan_id, loan_application_id=loan_application_id)
            if loan_obj:
                loan_obj[0].status = status
                loan_obj[0].updated_date = updatedate
                loan_obj[0].loan_start_date = updatedate
                loan_obj[0].reason = save_reason
                loan_obj[0].verified_date = updatedate
                loan_obj[0].verified_by_id = verified_id
                loan_obj[0].approved_amount = approved_amount
                loan_obj[0].save()

        elif status == 'CANCELLED':

            try:

                if calcelReason == "Company I'd":

                    reason = "Company i'd is missing/not clear"
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled because of your " + reason + ". Please upload the clear company I'd and re-apply."



                elif calcelReason == 'Salary slip':

                    reason = "Salary slip is Missing/Unclear/Not latest"
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled because of your " + reason + ". Please upload clear and Latest Salary slip and then reapply."


                elif calcelReason == 'Insufficient_credit_history':

                    # reason = "Insufficient Credit History"
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled because of your registered mobile doesn’t have enough banking transaction history. Please keep on using creditkart for shopping for next 15 days and then reapply."


                elif calcelReason == 'Selfie':

                    reason = "Selfie is missing/unclear"
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled because of your " + reason + ". Please upload the clear Selfie and reapply."



                else:
                    reason = ''
                    sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been cancelled."

                cancelled_date = datetime.datetime.now()

                sms_kwrgs = {

                    'sms_message': sms_message,
                    'number': str(obj.user.username)
                }

                sms(**sms_kwrgs)

                email_kwargs = {

                    'subject': 'CreditKart - Loan Cancelled',

                    'to_email': email_id,

                    'email_template': 'emails/loan/cancelled_loan.html',

                    'data': {

                        'domain': Site.objects.get_current(),

                        'loan_instance': obj,

                        'cancelled_date': cancelled_date,

                        'reason': sms_message,

                    },

                }

                email(**email_kwargs)

                fmp_kwargs = {

                    'user_id': obj.user.id,

                    'title': 'Loan Cancelled',

                    'message': sms_message,

                    'type': 'loan_cancelled',

                    'to_store': True,

                    'notification_id': obj.id,

                    'notification_key': 'loan_id',

                }

                UpdateMessage = "LOAN CANCELLED"

                email_id_1 = 'test@thecreditkart.com'
                date_1 = datetime.datetime.now()
                email_kwargs_1 = {

                    'subject': 'CreditKart - User Cancelled',

                    'to_email': email_id_1,

                    'email_template': 'emails/loan/cancelled_loan.html',

                    'data': {

                        'domain': Site.objects.get_current(),

                        'loan_instance': obj,

                        'cancelled_date': date_1,

                        'reason': 'User Cancelled',

                    },

                }
                # LoanApplication.objects.filter(id=loan_id, loan_application_id=loan_application_id).update(
                #     status=status,
                #     updated_date=updatedate,
                #     loan_start_date=updatedate,
                #     reason=save_reason,
                #     verified_date=updatedate,
                #     verified_by_id=verified_id, approved_amount=approved_amount)
                loan_obj = LoanApplication.objects.filter(id=loan_id, loan_application_id=loan_application_id)
                if loan_obj:
                    loan_obj[0].status = status
                    loan_obj[0].updated_date = updatedate
                    loan_obj[0].loan_start_date = updatedate
                    loan_obj[0].reason = save_reason
                    loan_obj[0].verified_date = updatedate
                    loan_obj[0].verified_by_id = verified_id
                    loan_obj[0].approved_amount = approved_amount
                    loan_obj[0].save()
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                pass

        elif status == 'ON_HOLD':
            updatedate = datetime.datetime.now()

            email_kwargs = {
                'subject': 'CreditKart - Loan ON_HOLD',
                'to_email': email_id,
                'email_template': 'emails/loan/pending_loan.html',
                'data': {
                    'domain': Site.objects.get_current(),
                    'loan_instance': obj,
                    'rejection_date': updatedate,
                },
            }

            email(**email_kwargs)
            sms_message = "Dear valued customer,your profile doesn't match with our eligibility criteria, please re apply after " + onhold_time + " days.You may continue using other features."

            sms_kwrgs = {
                'sms_message': sms_message,
                'number': str(obj.user.username)
            }

            sms(**sms_kwrgs)

            fmp_kwargs = {
                'user_id': obj.user.id,
                'title': 'Loan ON_HOLD',
                'message': sms_message,
                'type': 'ON_HOLD',
                'to_store': True,
                'notification_id': obj.id,
                'notification_key': 'loan_id',
            }
            UpdateMessage = "LOAN On Hold"
            # fcm_parameter_notification(**fmp_kwargs)

            email_id_1 = 'test@thecreditkart.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'CreditKart - User ON_HOLD In',

                'to_email': email_id_1,

                'email_template': 'emails/loan/pending_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User ON_HOLD In',

                },

            }

            if onhold_time is None:
                # LoanApplication.objects.filter(id=loan_id, loan_application_id=loan_application_id).update(
                #     status=status,
                #     updated_date=updatedate,
                #     hold_days=0,
                #     loan_start_date=updatedate,
                #     reason=save_reason,
                #     verified_date=updatedate,
                #     verified_by_id=verified_id,
                #     approved_amount=approved_amount)

                loan_obj = LoanApplication.objects.filter(id=loan_id, loan_application_id=loan_application_id)
                if loan_obj:
                    loan_obj[0].status = status
                    loan_obj[0].updated_date = updatedate
                    loan_obj[0].hold_days = 0
                    loan_obj[0].loan_start_date = updatedate
                    loan_obj[0].reason = save_reason
                    loan_obj[0].verified_date = updatedate
                    loan_obj[0].verified_by_id = verified_id
                    loan_obj[0].approved_amount = approved_amount
                    loan_obj[0].save()
            else:
                # LoanApplication.objects.filter(id=loan_id, loan_application_id=loan_application_id).update(
                #     status=status,
                #     updated_date=updatedate,
                #     hold_days=onhold_time,
                #     loan_start_date=updatedate,
                #     reason=save_reason,
                #     verified_date=updatedate,
                #     verified_by_id=verified_id,
                #     approved_amount=approved_amount)

                loan_obj = LoanApplication.objects.filter(id=loan_id, loan_application_id=loan_application_id)
                if loan_obj:
                    loan_obj[0].status = status
                    loan_obj[0].updated_date = updatedate
                    loan_obj[0].hold_days = onhold_time
                    loan_obj[0].loan_start_date = updatedate
                    loan_obj[0].reason = save_reason
                    loan_obj[0].verified_date = updatedate
                    loan_obj[0].verified_by_id = verified_id
                    loan_obj[0].approved_amount = approved_amount
                    loan_obj[0].save()

        elif status == 'REJECTED':
            rejection_date = datetime.datetime.now()

            email_kwargs = {
                'subject': 'CreditKart - Loan Rejected',
                'to_email': email_id,
                'email_template': 'emails/loan/rejected_loan.html',
                'data': {
                    'domain': Site.objects.get_current(),
                    'loan_instance': obj,
                    'rejection_date': rejection_date,
                },
            }

            email(**email_kwargs)
            sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been rejected."

            sms_kwrgs = {
                'sms_message': sms_message,
                'number': str(obj.user.username)
            }

            sms(**sms_kwrgs)
            fmp_kwargs = {
                'user_id': obj.user.id,
                'title': 'Loan Rejected',
                'message': sms_message,
                'type': 'loan_rejected',
                'to_store': True,
                'notification_id': obj.id,
                'notification_key': 'loan_id',
            }
            UpdateMessage = "LOAN REJECTED"
            # fcm_parameter_notification(**fmp_kwargs)

            email_id_1 = 'test@thecreditkart.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'CreditKart - User Rejected In',

                'to_email': email_id_1,

                'email_template': 'emails/loan/rejected_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User Rejected In',

                },

            }

            # email(**email_kwargs_1)

            # LoanApplication.objects.filter(id=loan_id, loan_application_id=loan_application_id).update(status=status,
            #                                                                                            updated_date=updatedate,
            #                                                                                            loan_start_date=updatedate,
            #                                                                                            reason=save_reason,
            #                                                                                            verified_date=updatedate,
            #                                                                                            verified_by_id=verified_id,
            #                                                                                            approved_amount=approved_amount)

            loan_obj = LoanApplication.objects.filter(id=loan_id, loan_application_id=loan_application_id)
            if loan_obj:
                loan_obj[0].status = status
                loan_obj[0].updated_date = updatedate
                loan_obj[0].loan_start_date = updatedate
                loan_obj[0].reason = save_reason
                loan_obj[0].verified_date = updatedate
                loan_obj[0].verified_by_id = verified_id
                loan_obj[0].approved_amount = approved_amount
                loan_obj[0].save()


        elif status == 'PENDING':

            email_kwargs = {
                'subject': 'CreditKart - Loan Pending',
                'to_email': email_id,
                'email_template': 'emails/loan/pending_loan.html',
                'data': {
                    'domain': Site.objects.get_current(),
                    'loan_instance': obj,
                },
            }

            email(**email_kwargs)

            sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + " is pending."

            sms_kwrgs = {
                'sms_message': sms_message,
                'number': str(obj.user.username)
            }

            sms(**sms_kwrgs)

            fmp_kwargs = {
                'user_id': obj.user.id,
                'title': 'Loan Pending',
                'message': sms_message,
                'type': 'loan_pending',
                'to_store': True,
                'notification_id': obj.id,
                'notification_key': 'loan_id',
            }
            UpdateMessage = "LOAN PENDING"

            email_id_1 = 'test@thecreditkart.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'CreditKart - User Pending',

                'to_email': email_id_1,

                'email_template': 'emails/loan/pending_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User Pending',

                },

            }

            # email(**email_kwargs_1)

            # fcm_parameter_notification(**fmp_kwargs)
            # LoanApplication.objects.filter(id=loan_id, loan_application_id=loan_application_id).update(status=status,
            #                                                                                            updated_date=updatedate,
            #                                                                                            loan_start_date=updatedate,
            #                                                                                            reason=save_reason,
            #                                                                                            verified_date=updatedate,
            #                                                                                            verified_by_id=verified_id,
            #                                                                                            approved_amount=approved_amount)

            loan_obj = LoanApplication.objects.filter(id=loan_id, loan_application_id=loan_application_id)
            if loan_obj:
                loan_obj[0].status = status
                loan_obj[0].updated_date = updatedate
                loan_obj[0].loan_start_date = updatedate
                loan_obj[0].reason = save_reason
                loan_obj[0].verified_date = updatedate
                loan_obj[0].verified_by_id = verified_id
                loan_obj[0].approved_amount = approved_amount
                loan_obj[0].save()


        elif status == 'COMPLETED':

            sms_message = "We are happy to inform you that your loan application number " + obj.loan_application_id + " is completed."

            fmp_kwargs = {
                'user_id': obj.user.id,
                'title': 'Loan Completed',
                'message': sms_message,
                'type': 'loan_completed',
                'to_store': True,
                'notification_id': obj.id,
                'notification_key': 'loan_id',
            }
            UpdateMessage = "LOAN COMPLETED"
            # fcm_parameter_notification(**fmp_kwargs)
            email_id_1 = 'test@thecreditkart.com'
            date_1 = datetime.datetime.now()
            email_kwargs_1 = {

                'subject': 'CreditKart - User COMPLETED',

                'to_email': email_id_1,

                'email_template': 'emails/loan/pending_loan.html',

                'data': {

                    'domain': Site.objects.get_current(),

                    'loan_instance': obj,

                    'cancelled_date': date_1,

                    'reason': 'User COMPLETED',

                },

            }

            # email(**email_kwargs_1)

            # loan_complete_obj = LoanApplication.objects.filter(id=loan_id,
            #                                                    loan_application_id=loan_application_id).update(
            #     status=status,
            #     updated_date=updatedate,
            #     reason=save_reason,
            #     verified_date=updatedate,
            #     verified_by_id=verified_id,
            #     approved_amount=approved_amount)

            loan_complete_obj = LoanApplication.objects.filter(id=loan_id, loan_application_id=loan_application_id)
            if loan_complete_obj:
                loan_complete_obj[0].status = status
                loan_complete_obj[0].updated_date = updatedate
                loan_complete_obj[0].reason = save_reason
                loan_complete_obj[0].verified_date = updatedate
                loan_complete_obj[0].verified_by_id = verified_id
                loan_complete_obj[0].approved_amount = approved_amount
                loan_complete_obj[0].save()

            try:

                loan_approved_entry(loan_complete_obj[0].id, status)


            except Exception as e:
                # print("-----------------exc", e, e.args)
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                pass

    return HttpResponseRedirect(reverse('userloan_application_list_pre_approved', kwargs={'date': date}))


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class UserLoanApplicationView(generic.ListView):
    template_name = "admin/UserAccounts/userloan_applications_list_new.html"
    title = ' User Loan Application'

    def get(self, request, UpdateMessage="", *args, **kwargs):
        userreject = []
        loan_users = []
        ctx = {}

        try:

            loan_users = LoanApplication.objects.filter(status="SUBMITTED")


        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            pass
        ctx['users'] = loan_users
        ctx['mess'] = UpdateMessage
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class getDefaulterList(ListView):
    template_name = 'admin/dashboard/defaulter_list_new.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        users = []
        total_repayment = []
        try:
            today = datetime.datetime.now()

            loan_users = LoanApplication.objects.filter(status='APPROVED')
            if loan_users:
                ctx['users'] = loan_users
            # zip(users, total_repayment)
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


def get_defaulters():
    ctx = {}
    users = []
    total_repayment = []
    today = datetime.datetime.now()

    loan_users = LoanApplication.objects.filter(status='APPROVED')
    for user in loan_users:
        for i in scheme:
            if i == user.loan_scheme_id:
                if user.loan_end_date:
                    return_date = user.loan_end_date + timedelta(int(scheme[i]['ms_tenure']))
                    if today > return_date:
                        rep_amount = user.repayment_amount.filter(
                            status='success').aggregate(Sum('amount'))['amount__sum']
                        users.append(user)
                        total_repayment.append(rep_amount)
    ctx['users'] = zip(users, total_repayment)
    return ctx


def get_div_by_zero(x, y):
    try:
        return (x / y) * 100
    except:
        return 0.0


def get_pincodewise_analysis(pin_code):
    today = datetime.datetime.now()
    user_counter = 0
    overdue_counter = 0
    users = []
    user = None

    loan_users = LoanApplication.objects.filter(
        status__in=['APPROVED', 'COMPLETED'])

    for user in loan_users:
        if user is not None:
            if user.current_address is not None:
                if pin_code == str(user.current_address.pin_code):
                    user_counter += 1
                    for i in scheme:
                        if i == user.loan_scheme_id:
                            if user.loan_end_date:
                                if today > user.loan_end_date:
                                    overdue_counter += 1
            else:
                pass

    overdue_counter_list = get_div_by_zero(overdue_counter, user_counter)
    users.append(user_counter)
    users.append(overdue_counter_list)

    return users


def get_pincodewise_analysis_overdue_defaulter(request):
    try:
        template_name = "admin/dashboard/pin_code_overdue_list.html"
        title = 'Loan Application'
        ctx = {}
        unique_pincode_3_digit = []
        today = datetime.datetime.now()
        pincode_list = []
        overdue_counter_list = []
        user_counter_list = []
        unique_pincode_3_digit = get_all_pincode()
        for pin_code in unique_pincode_3_digit:
            user_counter = 0
            overdue_counter = 0
            default_counter = 0
            loan_users = LoanApplication.objects.filter(
                status__in=['APPROVED', 'COMPLETED'])
            for user in loan_users:
                if user.current_address:
                    if pin_code == str(user.current_address.pin_code)[:3]:
                        user_counter += 1
                        if user.status != 'COMPLETED':
                            for i in scheme:
                                if i == user.loan_scheme_id:
                                    if user.loan_end_date:
                                        return_date = user.loan_end_date + timedelta(int(scheme[i]['ms_tenure']))
                                        if (today > user.loan_end_date) & (today <= return_date):
                                            overdue_counter += 1
                                        elif today > return_date:
                                            default_counter += 1
            user_counter_list.append(user_counter)
            pincode_list.append(pin_code)
            overdue_counter_list.append(
                round((get_div_by_zero(overdue_counter, user_counter) + get_div_by_zero(default_counter, user_counter)),
                      2))
        ctx['users'] = zip(pincode_list, user_counter_list, overdue_counter_list)
        return render(request, template_name, ctx)

    except:

        return render(request, template_name, ctx)


def get_all_pincode():
    unique_pincode_first_3_letter_list = []
    try:
        pin_code_set = set()

        pin_codes_qset = LoanApplication.objects.filter(
            status__in=['APPROVED', 'COMPLETED']).distinct()

        if pin_codes_qset:
            for pin in pin_codes_qset:
                if pin.current_address:
                    first_three_digit = pin.current_address.pin_code[0:3]
                    pin_code_set.add(first_three_digit)
                unique_pincode_first_3_letter_list = list(pin_code_set)
        return unique_pincode_first_3_letter_list
    except:
        return unique_pincode_first_3_letter_list


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class getOverDueList(ListView):
    template_name = 'admin/dashboard/overdue_list_new.html'

    def get(self, request, *args, **kwargs):

        ctx = {}
        try:

            users = []
            loan_obj = LoanApplication.objects.filter(status="APPROVED", user_status="Accept")
            if loan_obj:
                for loan in loan_obj:
                    overdue_staus = 0
                    total = []
                    amount = 0
                    from LoanAppData.loan_funcations import loan_amount_slab
                    slabs = loan_amount_slab(loan.id)
                    if slabs:
                        for dat in slabs:
                            if dat["pay_status"] == "unpaid" and dat["is_overdue"] == "Yes":
                                overdue_staus = 1
                                from LoanAppData.loan_funcations import get_amount_paid_for_loan
                                data = get_amount_paid_for_loan(loan)
                                if data:
                                    if data['rep_amount'] > 0:
                                        amount = data['rep_amount']

                        if overdue_staus == 1:
                            total.append(loan)
                            total.append(amount)
                            users.append(total)

            ctx['users'] = users
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


def get_over_due_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="overdue_users_csv.csv"'
    writer = csv.writer(response)
    writer.writerow(
        ['User Name', 'Mobile number', 'LoanApplication Id', 'Disbursed Amount', 'Apply Amount', 'Approved Amount',
         'Received Amount', 'Balance Amount', 'Start date', 'End date', 'Status'])

    try:

        users = []
        loan_obj = LoanApplication.objects.filter(status="APPROVED", user_status="Accept")
        if loan_obj:
            for loan in loan_obj:
                overdue_staus = 0
                total = []
                amount = 0
                from LoanAppData.loan_funcations import loan_amount_slab
                slabs = loan_amount_slab(loan.id)
                if slabs:
                    for dat in slabs:
                        if dat["pay_status"] == "unpaid" and dat["is_overdue"] == "Yes":
                            overdue_staus = 1
                            from LoanAppData.loan_funcations import get_amount_paid_for_loan
                            data = get_amount_paid_for_loan(loan)
                            if data:
                                if data['rep_amount'] > 0:
                                    amount = data['rep_amount']

                    balance_amount = int(loan.approved_amount) - amount

                    if overdue_staus == 1:
                        writer.writerow([loan.user_personal.name, loan.user.username, loan.loan_application_id,
                                         loan.approved_amount,
                                         loan.loan_scheme_id, loan.approved_amount, amount, balance_amount,
                                         loan.loan_start_date, loan.loan_end_date, loan.status])
        return response

    except Exception as e:
        print("Inside Exception=============", e.args)
        return response


def get_total_due_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="overdue_users_csv.csv"'
    writer = csv.writer(response)
    writer.writerow(
        ['User Name', 'Mobile number', 'LoanApplication Id', 'Disbursed Amount', 'Apply Amount', 'Approved Amount',
         'Received Amount', 'Balance Amount', 'Start date', 'End date', 'Status'])

    try:
        loan_obj = LoanApplication.objects.filter(status="APPROVED", user_status="Accept")
        if loan_obj:
            for loan in loan_obj:
                amount = 0

                from LoanAppData.loan_funcations import get_amount_paid_for_loan
                data = get_amount_paid_for_loan(loan)
                if data:
                    if data['rep_amount'] > 0:
                        amount = data['rep_amount']

                balance_amount = int(loan.approved_amount) - amount

                writer.writerow([loan.user_personal.name, loan.user.username, loan.loan_application_id,
                                 loan.approved_amount,
                                 loan.loan_scheme_id, loan.approved_amount, amount, balance_amount,
                                 loan.loan_start_date, loan.loan_end_date, loan.status])
        return response

    except Exception as e:
        print("Inside Exception=============", e.args)
        return response



@method_decorator([login_required(login_url="/login/")], name='dispatch')
class getActualOverDueList(ListView):
    template_name = 'admin/dashboard/overdue_list_actual_new.html'

    def get(self, request, *args, **kwargs):

        ctx = {}
        try:
            total_amount = 0
            users = []
            loan_obj = LoanApplication.objects.filter(status="APPROVED", user_status="Accept")
            if loan_obj:
                for loan in loan_obj:
                    first_slab_amount = 0
                    second_slab_amount = 0
                    third_slab_amount = 0
                    overdue_status = 0
                    total = []
                    amount = 0
                    from LoanAppData.loan_funcations import loan_amount_slab
                    slabs = loan_amount_slab(loan.id)
                    if slabs:
                        cnt = 0
                        for dat in slabs:
                            cnt += 1
                            slab_amount = 0
                            if dat["pay_status"] == "unpaid" and dat["is_overdue"] == "Yes":
                                overdue_status = 1
                                if dat['remaining_amount'] > 0:
                                    slab_amount = dat['remaining_amount']
                                    total_amount += slab_amount
                                    if cnt == 1:
                                        first_slab_amount = slab_amount
                                    elif cnt == 2:
                                        second_slab_amount = slab_amount
                                    elif cnt == 3:
                                        third_slab_amount = slab_amount

                    if overdue_status == 1:
                        loan_dict = {
                            "loan": loan,
                            "first_slab_amount": first_slab_amount,
                            "second_slab_amount": second_slab_amount,
                            "third_slab_amount": third_slab_amount,
                        }
                        users.append(loan_dict)

            ctx['users'] = users
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class UserReferanceView(generic.ListView):
    """
    Gets user reference Details
    """
    template_name = "admin/UserAccounts/user_referance_details_new.html"
    title = 'Loan Application'

    def get(self, request, loan_application_id, *args, **kwargs):
        ctx = {}
        try:
            user = LoanApplication.objects.filter(loan_application_id=loan_application_id)
            ctx['reference'] = user
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


def PancardMatch(pan_number):
    try:
        user_count = Personal.objects.values('mobile_no').filter(
            pan_number=pan_number).distinct().count()
        if user_count > 1:
            return user_count
        else:
            user_count = 0
            return user_count
    except:
        return 0


def loan_on_hold_report(request):
    """
        Shows list of users woth status ONHOLD
    :param request:
    :return:
    """
    template_name = "admin/UserAccounts/onhold_user_list.html"
    title = 'ONHOLD User'

    ctx = {}
    try:
        loan_users = LoanApplication.objects.order_by('user').all().filter(
            status__in=['ON_HOLD'])

        ctx['active_users'] = loan_users
        return render(request, template_name, ctx)
    except:
        return render(request, template_name, ctx)


def loan_pending_report(request):
    """
        Shows list of users woth status ONHOLD
    :param request:
    :return:
    """
    template_name = "admin/UserAccounts/pending_user_list.html"
    title = 'pending User'

    ctx = {}
    try:
        loan_users = LoanApplication.objects.order_by('user').all().filter(
            status__in=['PENDING'])

        ctx['active_users'] = loan_users
        return render(request, template_name, ctx)
    except:
        return render(request, template_name, ctx)


def loan_application_reject(loan_application_id):
    status = "REJECTED"
    updatedate = datetime.datetime.now()
    obj = LoanApplication.objects.get(loan_application_id=loan_application_id)
    user_instance = User.objects.get(id=obj.user.id)

    if obj.user_professional.company_email_id:
        email_id = obj.user_professional.company_email_id

    UpdateMessage = ""
    if obj.status == status:
        UpdateMessage = "Already changed ... !"
        pass

    else:
        if status == 'REJECTED':
            email_kwargs = {
                'subject': 'CreditKart - Loan Rejected',
                'to_email': email_id,
                'email_template': 'emails/loan/rejected_loan.html',
                'data': {
                    'domain': Site.objects.get_current(),
                    'loan_instance': obj,
                    'rejection_date': updatedate,
                },
            }

            email(**email_kwargs)
            sms_message = "We regret to inform you that your loan application number " + obj.loan_application_id + "  has been rejected."

            sms_kwrgs = {
                'sms_message': sms_message,
                'number': str(user_instance)
            }

            # sms(**sms_kwrgs)

            fmp_kwargs = {
                'user_id': obj.user.id,
                'title': 'Loan Rejected',
                'message': sms_message,
                'type': 'loan_rejected',
                'to_store': True,
                'notification_id': obj.id,
                'notification_key': 'loan_id',
            }
            UpdateMessage = "LOAN REJECTED"
            # fcm_parameter_notification(**fmp_kwargs)
            # LoanApplication.objects.filter(loan_application_id=loan_application_id).update(status=status,
            #                                                                                updated_date=updatedate,
            #                                                                                loan_start_date=updatedate)
            loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
            if loan_obj:
                loan_obj[0].status = status
                loan_obj[0].updated_date = updatedate
                loan_obj[0].loan_start_date = updatedate
                loan_obj[0].save()
    return UpdateMessage


def reject_loan_application(request, loan_application_id):
    UpdateMessage = loan_application_reject(loan_application_id)
    return HttpResponseRedirect(reverse('userloan_application_list1', kwargs={'UpdateMessage': UpdateMessage}))


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class UserWiseLoanDetailsView(generic.ListView):
    template_name = "admin/UserAccounts/user_loan_details_list_new.html"
    title = 'Active User'

    def get(self, request, username, *args, **kwargs):
        ctx = {}
        try:
            user = User.objects.get(username=username)
            loan_users = LoanApplication.objects.filter(user=user).filter(status__in=['APPROVED', 'COMPLETED'])

            ctx['user_loan'] = loan_users

            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class UserParticularLoanWiseDetailsView(generic.ListView):
    template_name = "admin/UserAccounts/user_particular_loan_details_new.html"
    title = 'Active User'

    def get(self, request, loan_application_id, *args, **kwargs):
        ctx = {}

        try:
            rep_amount = 0
            user_loan_details = LoanApplication.objects.filter(loan_application_id=loan_application_id)

            loan_payment = []
            for user in user_loan_details:
                if user.repayment_amount:
                    rep_amount = user.repayment_amount.filter(status='success')
                    loan_payment.append(rep_amount)

            ctx['user_loan_details'] = rep_amount
            ctx['loan_application_id'] = loan_application_id
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


def get_reference_details(loan_application_id):
    """
    returns user reference details
    :param user_id:
    :return:
    """
    today = datetime.datetime.now()
    ctx = {}
    NOT_FOUND = "Referance is not a user"

    user = LoanApplication.objects.get(loan_application_id=loan_application_id)
    try:
        ref1 = user.reference_id.colleague_mobile_no
        ref2 = user.reference_id.relative_mobile_no
        loan_users = LoanApplication.objects.filter(status='APPROVED')
        users = []
        for user in loan_users:
            if user.user.username == ref1 or user.user.username == ref2:
                if (today > user.loan_end_date):
                    NOT_FOUND = "Defaulters found in referance"
                else:
                    NOT_FOUND = "No defaulters found in referance"
    except:
        pass
    return NOT_FOUND


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class Total_Clients(generic.ListView):
    template_name = "admin/UserAccounts/total_clients.html"
    title = 'Cibil Report'

    def get(self, request, *args, **kwargs):
        ctx = {}
        user_data = LoanApplication.objects.all().order_by('user').distinct('user')
        ctx['users'] = user_data
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class AllActiveUserView(generic.ListView):
    template_name = "admin/UserAccounts/active_user_list.html"
    title = 'Active User'

    def get(self, request, date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        date_list = date.split('-')
        y = date_list[0]
        if len(date_list[1]) > 1:
            m = date_list[1]
        else:
            m = '0' + str(date_list[1])
            # new_date = '{}-{}-{}'.format(y, m, d)
        if len(date_list[2]) > 1:
            d = date_list[2]
        else:
            d = '0' + str(date_list[2])
            # new_date = '{}-{}-{}'.format(y, m, d)
        new_date = '{}-{}-{}'.format(y, m, d)
        try:
            loan_users = LoanApplication.objects.order_by('user').all().filter(
                status__in=['APPROVED', 'COMPLETED'], loan_start_date__startswith=new_date).distinct('user')
            full_name_list = []
            username_list = []
            for user in loan_users:
                username_list.append(user.user.username)
                full_name_list.append(user.user_personal.name)
            loan_amount_of_all = get_total_loan_of_user(self, username_list)
            ctx['active_users'] = zip(full_name_list, username_list, loan_amount_of_all)
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class All_Active_Monthwise(generic.ListView):
    '''
        Template : yelouser_application_list_new.html
        shows yelo user applications by date
        '''
    template_name = "admin/UserAccounts/active_user_monthwise.html"
    title = ' yelo UserApplication new'

    def get(self, request, *args, **kwargs):
        ctx = {}

        try:
            active_users = loan_users = LoanApplication.objects.order_by('user').all().filter(
                status__in=['APPROVED', 'COMPLETED']).distinct('user')
            dataframe = read_frame(active_users)
            dataframe['Month'] = pd.to_datetime(dataframe['loan_start_date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            month_wise_active_count = \
                dataframe.groupby(dataframe['Month'])['id'].count().sort_index(axis=0)
            month_wise_active_count_dict = dict(month_wise_active_count)
        except:
            pass

        ctx['month_wise_active'] = month_wise_active_count_dict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class All_Active_Datewise(generic.ListView):
    template_name = "admin/UserAccounts/active_user_monthwise.html"

    def get(self, request, month, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        date_list = month.split('-')
        y = date_list[0]
        try:
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
            new_date = '{}-{}'.format(y, m, )
            active_users = loan_users = LoanApplication.objects.order_by('user').all().filter(
                status__in=['APPROVED', 'COMPLETED'], loan_start_date__startswith=new_date).distinct('user')
            dataframe = read_frame(active_users)
            dataframe['Date'] = pd.to_datetime(dataframe['loan_start_date']).apply(
                lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
            date_wise_active_count = \
                dataframe.groupby(dataframe['Date'])['id'].count().sort_index(axis=0)
            date_wise_active_count_dict = dict(date_wise_active_count)
        except:
            pass

        ctx['date_wise_active'] = date_wise_active_count_dict
        return render(request, self.template_name, ctx)


def get_total_loan_of_user(self, user_list):
    for username in user_list:
        user = User.objects.get(username=username)
        loans_of_user = LoanApplication.objects.values_list('loan_scheme_id').filter(user=user).filter(
            status__in=['APPROVED', 'COMPLETED'])
        total_loan_amount = 0
        for loan in loans_of_user:
            total_loan_amount += int(loan[0])
        yield total_loan_amount


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class Pincode_Report_City(generic.ListView):
    template_name = "admin/UserAccounts/pincode_report_city.html"
    title = 'Cibil Report'

    def get(self, request, *args, **kwargs):
        ctx = {}

        pin_code_master = PincodeMaster.objects.values_list('pincode', 'taluka', 'city', 'state').order_by(
            'city').distinct('city')

        ctx['pincode_info'] = pin_code_master
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class Pincode_Report(generic.ListView):
    template_name = "admin/UserAccounts/pincode_report.html"
    title = 'Cibil Report'

    def get(self, request, *args, **kwargs):
        ctx = {}
        cibil_data = Address.objects.all().order_by(
            'user_id').distinct('user_id')
        df = read_frame(cibil_data)
        cibil_data_df = df.groupby(df['pin_code'])['pin_code'].count()
        cibil_score = cibil_data_df.index
        count = cibil_data_df.values
        ctx['pincode_info'] = zip(cibil_score, count)
        return render(request, self.template_name, ctx)


def defaulter_messages(request):
    """Sending user messages as their payment date"""
    Today_date = datetime.datetime.now()

    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="Defaulter_message.csv"'
    writer = csv.writer(response)
    writer.writerow(
        ['User id', 'Mobile number', 'name', 'User Application', 'Start date', 'Repayment date', 'tenure',
         'Repayment',
         'short mess',
         'Message', 'repayment_date', 'DPD'])

    try:
        loan_users = LoanApplication.objects.filter(status='APPROVED')
        amount = []
        for i in scheme:
            amount.append(i)

        for user in loan_users:
            if user.loan_start_date != None and user.loan_end_date != None:
                try:
                    loan_amount = int(user.loan_scheme_id)
                except ValueError:
                    loan_amount = int(float(user.loan_scheme_id))
                p = amount.index(str(loan_amount))

                m = p + 1
                rep_amount = user.repayment_amount.filter(
                    status='success').aggregate(Sum('amount'))['amount__sum']
                if rep_amount is None and not rep_amount:
                    rep_amount = 0.0
                total_amount_pending = loan_amount - float(rep_amount)
                next_loan_amount = int(amount[m])

                tommory = Today_date + timedelta(int(1))
                end_over_date = Today_date - timedelta(int(6))
                last_over_date = Today_date - timedelta(int(7))

                if user.loan_end_date.date() == Today_date.date():
                    sms_message = ''
                    sms_message = "Dear " + user.user_personal.name + ", your loan repayment of RS." + str(
                        total_amount_pending) + " is due today.\nPay it today & get instant loan of RS." + str(
                        next_loan_amount)

                    writer.writerow(
                        [user.user_id, user.user.username, user.user_personal.name, user.loan_application_id,
                         user.loan_start_date.date(),
                         user.loan_end_date.date(),
                         '', '', "Today", sms_message, '', 0])
                    sentSms(sms_message, user.user.username)
                    sentFcm(user, sms_message)


                elif user.loan_end_date.date() == tommory.date():
                    sms_message = ''
                    sms_message = "Dear " + user.user_personal.name + ", your loan repayment of RS." + str(
                        total_amount_pending) + " is due tomorrow.\nPay it today & get instant loan of RS." + str(
                        next_loan_amount)

                    writer.writerow(
                        [user.user_id, user.user.username, user.user_personal.name, user.loan_application_id,
                         user.loan_start_date.date(),
                         user.loan_end_date.date(),
                         '', '', "Tomm", sms_message, '', 1])
                    sentSms(sms_message, user.user.username)
                    sentFcm(user, sms_message)

                elif user.loan_end_date.date() < Today_date.date() and user.loan_end_date.date() > end_over_date.date():
                    day = user.loan_end_date.date() - Today_date.date()
                    sms_message = ''
                    sms_message = "Dear " + user.user_personal.name + ", your loan repayment of RS." + str(
                        total_amount_pending) + " is overdue by " + str(
                        -(day.days)) + " days. \nPay it today to avoid penalty & get instant loan of RS." + str(
                        next_loan_amount)
                    writer.writerow(
                        [user.user_id, user.user.username, user.user_personal.name, user.loan_application_id,
                         user.loan_start_date.date(),
                         user.loan_end_date.date(),
                         '', '', 'defaulter', sms_message, '', day.days])
                    sentSms(sms_message, user.user.username)
                    sentFcm(user, sms_message)
                elif user.loan_end_date.date() == end_over_date.date():
                    sms_message = ''
                    sms_message = "Final reminder: Dear " + user.user_personal.name + "" + ", pay your overdue loan today to avoid penalty and legal action."
                    writer.writerow(
                        [user.user_id, user.user.username, user.user_personal.name, user.loan_application_id,
                         user.loan_start_date.date(),
                         user.loan_end_date.date(),
                         '', '', "Final Reminder", sms_message, '', 7])
                    sentSms(sms_message, user.user.username)
                    sentFcm(user, sms_message)
                elif user.loan_end_date.date() == last_over_date.date():
                    sms_message = ''
                    sms_message = "Dear " + user.user_personal.name + ", Despite many reminders, you have not responded. We shall report to " \
                                                                      "CIBIL and start legal action against you if not paid today. "
                    writer.writerow(
                        [user.user_id, user.user.username, user.user_personal.name, user.loan_application_id,
                         user.loan_start_date.date(),
                         user.loan_end_date.date(),
                         '', '', "Last Day", sms_message, '', 8])
                    sentSms(sms_message, user.user.username)
                    sentFcm(user, sms_message)

        return response
    except Exception as e:
        return response


def sentSms(sms_message, number):
    sms_kwrgs = {
        'sms_message': sms_message,
        'number': str(number)
    }

    sms(**sms_kwrgs)


def sentFcm(user, sms_message):
    fmp_kwargs = {
        'user_id': user.id,
        'title': 'Payment Reminder',
        'message': 'sms_message',
        'type': 'loan_incomplete',
        'to_store': False,
        'notification_id': 'user.id',
        'notification_key': 'user.loan_id',

    }
    # fcm_parameter_notification(**fmp_kwargs)


from .tasks import loan_status


def loan_test(resuest):
    total_sms = 555555
    balance = 5454
    loan = loan_status.delay(total_sms)
    return HttpResponse(loan)


def get_repayment_amount(user):
    """
        get user loan repayment amount
    """
    total_paid_amount = user.repayment_amount.filter(
        status='success').aggregate(Sum('amount'))['amount__sum']
    if total_paid_amount:
        total_amount_pending = float(user.loan_scheme_id) - float(total_paid_amount)
    else:
        total_amount_pending = float(user.loan_scheme_id)
    return total_amount_pending


def paytm_send_link(request):
    """
    {
        "link_name":"UATTest",
        "link_description": "Create link UAT Test ",
        "link_type": "FIXED",
        "amount":"50",
        "customer_name":"test",
        "customer_email":"keval.thakkar@mudrakwik.com",
        "customer_number":"7875583679",
        "send_sms":"true",
        "send_email":"false",
        "is_active":"true",
        "expiry_date":"10/04/2020"
    }
    :param request:
    :return:
    """
    # user_loan =
    list = get_defaulters()
    ctx = {}
    ctx_list = []
    for users, total_repayment in list['users']:
        link_description = "Creditkart Emi for loan: " + users.loan_application_id
        cust_email = 'keval.thakkar@mudrakwik.com'
        cust_number = '7875583679'
        ctx = {
            "link_name": "UATTest",
            "link_description": link_description,
            "link_type": "FIXED",
            "amount": get_repayment_amount(users),
            "customer_name": users.user_personal.name,
            "customer_email": cust_email,
            "customer_number": cust_number,
            "send_sms": "true",
            "send_email": "false",
            "is_active": "true",
            "expiry_date": "10/04/2020"
        }
        paytm_create_link_api.delay(ctx)

    return HttpResponse("ok")


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class SearchLoanApplication(generic.ListView):
    # login_required=True
    template_name = "admin/UserAccounts/search_loan.html"
    title = ''

    def post(self, request, *args, **kwargs):
        ctx = {}
        loan_id = request.POST['loan_id']

        try:

            loan_application = LoanApplication.objects.get(loan_application_id=loan_id)
            if loan_application:
                template = 'user-all-details' + str(loan_application.id) + '/'
                return HttpResponseRedirect(
                    reverse('user_all_details', kwargs={'id': loan_application.id}))

            return render(request, self.template_name, ctx)
        except Exception as e:
            return render(request, self.template_name, ctx)


def search_lid(request):
    return render(request, "admin/UserAccounts/search_loan.html")


def test(request):
    test_cahnge.delay()

    return HttpResponse("done")


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class UndisbursedLoanMontheDetails(generic.ListView):
    '''
    Template : userloan_applications_list.html
    shows user loan applications by date
    '''

    template_name = "admin/UserAccounts/un_disbursed_loan_monthe_detail.html"
    title = ' User Loan Application new'

    def get(self, request, UpdateMessage="", *args, **kwargs):
        day_wise_application_count_dict = {}
        ctx = {}
        try:
            loan_list = []
            get_approved_loans = LoanApplication.objects.filter(status="APPROVED")
            if get_approved_loans:
                for loan in get_approved_loans:
                    if loan.disbursed_amount_id or loan.disbursed_amount:
                        if loan.disbursed_amount.status != "Success" and loan.disbursed_amount.status != "success":
                            loan_list.append(loan.loan_start_date)
                dataframe = pd.DataFrame(loan_list)
                dataframe['YearMonthDay'] = pd.to_datetime(dataframe[0]).apply(
                    lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
                day_wise_application_count = \
                    dataframe.groupby(dataframe['YearMonthDay'])[
                        0].count().sort_index(axis=0)
                day_wise_application_count_dict = dict(day_wise_application_count)
        except:
            pass
        ctx['application_details'] = day_wise_application_count_dict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class UndisbursedLoanDetails(generic.ListView):
    template_name = "admin/UserAccounts/Undisburse_loan_list.html"
    title = 'Undisbursed Loan Details'

    def get(self, request, date='', *args, **kwargs):
        ctx = {}
        disb_status = []
        loan_list = []
        type = []
        new_date = ''
        m = ''
        d = ''
        date_list = date.split('-')
        y = date_list[0]

        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}-{}'.format(y, m, d)

        if len(date_list[2]) == 2:
            d = date_list[2]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)

        get_approved_loans = LoanApplication.objects.filter(loan_start_date__startswith=new_date, status="APPROVED")
        if get_approved_loans:
            for loan in get_approved_loans:
                if loan.disbursed_amount_id or loan.disbursed_amount:
                    if loan.disbursed_amount.status != "Success" and loan.disbursed_amount.status != "success":
                        loan_list.append(loan)
                        type.append("not success")
                        disb_status.append(loan.disbursed_amount.status)
                else:
                    loan_list.append(loan)
                    type.append("no entry")
                    disb_status.append(None)
            ctx['loan'] = zip(loan_list, type, disb_status)
        return render(request, self.template_name, ctx)


def mobile_search(request):
    return render(request, "admin/UserAccounts/user_details_mobile_search.html")


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class UserLoanDetailsByMobile(generic.ListView):
    template_name = "admin/UserAccounts/userloan_details_by_mobile.html"
    title = 'User Details By Mobile no'

    def post(self, request, *args, **kwargs):
        ctx = {}
        disbursed_amount_list = []
        repayment_amount_list = []
        try:
            if request.method == "POST":
                mobile = request.POST['mobile']
                user = User.objects.get(username=mobile)
                user_loans = LoanApplication.objects.filter(user=user)
                if user_loans:
                    for loan in user_loans:
                        if loan.disbursed_amount:
                            disbursed_amount = loan.disbursed_amount.amount
                            disbursed_amount_list.append(disbursed_amount)
                        else:
                            disbursed_amount_list.append(0.0)
                        if loan.repayment_amount:
                            rep_amount = loan.repayment_amount.filter(
                                status='success').aggregate(Sum('amount'))['amount__sum']
                            repayment_amount_list.append(rep_amount)
                        else:
                            repayment_amount_list.append(0.0)
                ctx['user_loan'] = zip(user_loans, disbursed_amount_list, repayment_amount_list)
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class LoanDetails(generic.ListView):
    template_name = "admin/UserAccounts/loan_details.html"
    title = 'loan Details'

    def get(self, request, loan_id, *args, **kwargs):
        ctx = {}
        try:
            user_loan = LoanApplication.objects.filter(loan_application_id=loan_id)
            if user_loan:
                payment = user_loan[0].repayment_amount.all()
                ctx['payment'] = payment
            return render(request, self.template_name, ctx)
        except Exception as e:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class DatewiseDisbursmentView(generic.ListView):
    template_name = "admin/UserAccounts/datewise_disbursment_details.html"
    title = 'Datewise Disbursment'

    def get(self, request, on_date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        amount_list = []
        try:
            date_list = on_date.split('-')
            y = date_list[0]

            if len(date_list[1]) > 1:
                m = date_list[1]
                new_date = '{}-{}-{}'.format(y, m, d)
            else:
                m = '0' + str(date_list[1])
                new_date = '{}-{}-{}'.format(y, m, d)

            if len(date_list[2]) == 2:
                d = date_list[2]
                new_date = '{}-{}-{}'.format(y, m, d)
            else:
                d = '0' + str(date_list[2])
                new_date = '{}-{}-{}'.format(y, m, d)

            users = Payments.objects.filter(date__startswith=new_date)
            if users:
                for user in users:
                    amount = LoanApplication.objects.filter(disbursed_amount_id=user.id)
                    rrn = RBLTransaction.objects.filter(payment_id=user.id)
                    if amount:
                        amount_dict = {}
                        amount_dict['User'] = amount[0].user.username
                        amount_dict['created_date'] = rrn[0].created_date
                        amount_dict['transaction_id_text'] = rrn[0].transaction_id_text
                        amount_dict['corp_id'] = rrn[0].corp_id
                        amount_dict['ref_number'] = rrn[0].ref_number
                        amount_dict['channel_part_ref_number'] = rrn[0].channel_part_ref_number
                        amount_dict['amount'] = rrn[0].amount
                        amount_dict['ben_acc_no'] = rrn[0].ben_acc_no
                        amount_list.append(amount_dict)
            ctx['users'] = amount_list
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class DatewiseRepaymentView(generic.ListView):
    template_name = "admin/UserAccounts/datewise_repayment_details.html"
    title = 'Datewise Repayment'

    def get(self, request, on_date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        amount_list = []
        try:
            date_list = on_date.split('-')
            y = date_list[0]

            if len(date_list[1]) > 1:
                m = date_list[1]
                new_date = '{}-{}-{}'.format(y, m, d)
            else:
                m = '0' + str(date_list[1])
                new_date = '{}-{}-{}'.format(y, m, d)

            if len(date_list[2]) == 2:
                d = date_list[2]
                new_date = '{}-{}-{}'.format(y, m, d)
            else:
                d = '0' + str(date_list[2])
                new_date = '{}-{}-{}'.format(y, m, d)

            users = Payments.objects.filter(date__startswith=new_date, status__icontains="Success")
            if users:
                for user in users:
                    amounts = LoanApplication.objects.filter(repayment_amount=user.id)
                    if amounts:
                        for amount in amounts:
                            amount_dict = {}
                            amount_dict['Amount'] = user.amount
                            amount_dict['transaction_date'] = user.date
                            amount_dict['LoanApplicationId'] = amount.loan_application_id
                            amount_dict['type'] = user.type
                            amount_dict['status'] = user.status
                            amount_dict['Order_id'] = user.order_id
                            amount_dict['Username'] = amount.user.username
                            amount_list.append(amount_dict)
            ctx['users'] = amount_list
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class LoanDetailsMonthwise(generic.ListView):
    template_name = "admin/UserAccounts/loan_details_monthwise.html"
    title = ' Loan Details Monthwise'

    def get(self, request, *args, **kwargs):
        MainDict = {}
        monthwise_submitted_loans_dict = {}
        month_wise_approved_loans_dict = {}
        completed_loans_dict = {}
        all_dict = {}
        ctx = {}
        try:
            # For Counting Submitted loans monthwise
            submitted_loans = LoanApplication.objects.all().order_by('-created_date')
            submitted_dataframe = read_frame(submitted_loans)
            submitted_dataframe['YearMonth'] = pd.to_datetime(submitted_dataframe['created_date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            monthwise_submitted_loans_dict = \
                submitted_dataframe.groupby(submitted_dataframe['YearMonth'])['loan_application_id'].count().sort_index(
                    axis=0)
            monthwise_submitted_loans_dict = dict(monthwise_submitted_loans_dict)

            # For Counting Approved loans monthwise
            approved_loans = Payments.objects.filter(category='Rbl', status='Success').order_by('-create_date')
            approved_dataframe = read_frame(approved_loans)
            approved_dataframe['YearMonth'] = pd.to_datetime(approved_dataframe['create_date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            month_wise_approved_loans_dict = \
                approved_dataframe.groupby(approved_dataframe['YearMonth'])['transaction_id'].count().sort_index(axis=0)
            month_wise_approved_loans_dict = dict(month_wise_approved_loans_dict)

            # For Counting Completed loans monthwise
            created_date = ""
            created_date_list = []
            users = LoanApplication.objects.filter(status="COMPLETED")
            for user in users:
                amounts = user.repayment_amount.filter(status='success').order_by('-create_date').first()
                if amounts:
                    p = datetime.datetime.strftime(amounts.create_date, "%Y-%m-%d")
                    created_date_list.append(p)

            dataframe = pd.DataFrame(created_date_list)
            dataframe = pd.DataFrame(created_date_list, columns=['Dates'])
            dataframe['YearMonth'] = pd.to_datetime(dataframe['Dates']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            completed_loans = dataframe.groupby(dataframe['YearMonth'])['Dates'].count().sort_index(axis=0)
            completed_loans_dict = dict(completed_loans)
            all_dict = [monthwise_submitted_loans_dict, month_wise_approved_loans_dict, completed_loans_dict]
            allkey = reduce(set.union, map(set, map(dict.keys, all_dict)))
            for i in allkey:
                if i in month_wise_approved_loans_dict:
                    pass
                else:
                    month_wise_approved_loans_dict[i] = None
                if i in completed_loans_dict:
                    pass
                else:
                    completed_loans_dict[i] = None
                if i in monthwise_submitted_loans_dict:
                    pass
                else:
                    monthwise_submitted_loans_dict[i] = None
            All_Dicts = [monthwise_submitted_loans_dict, month_wise_approved_loans_dict, completed_loans_dict]
            Main_Dict = {}
            for k in monthwise_submitted_loans_dict.keys():
                Main_Dict[k] = list(Main_Dict[k] for Main_Dict in All_Dicts)
            ctx['MainData'] = Main_Dict

            return render(request, self.template_name, ctx)
        except Exception as e:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class LoanDetailsDatewise(generic.ListView):
    template_name = "admin/UserAccounts/loan_details_datewise.html"
    title = ' Loan Details Datewise'

    def get(self, request, on_month, *args, **kwargs):
        Main_Dict = {}
        datewise_submitted_loans_dict = {}
        date_wise_approved_loans_dict = {}
        completed_loans_dict = {}
        all_dict = {}
        ctx = {}
        new_date = ""
        try:
            date_list = on_month.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
                new_date = '{}-{}'.format(y, m)
            else:
                m = '0' + str(date_list[1])
                new_date = '{}-{}'.format(y, m)

            # For Counting Submitted loans Datewise
            submitted_loans = LoanApplication.objects.filter(created_date__startswith=new_date).order_by(
                '-created_date')
            submitted_dataframe = read_frame(submitted_loans)
            submitted_dataframe['MonthDay'] = pd.to_datetime(submitted_dataframe['created_date']).apply(
                lambda x: '{month}-{day}'.format(month=x.month, day=x.day))
            datewise_submitted_loans_dict = \
                submitted_dataframe.groupby(submitted_dataframe['MonthDay'])['loan_application_id'].count().sort_index(
                    axis=0)
            datewise_submitted_loans_dict = dict(datewise_submitted_loans_dict)

            # For Counting Approved loans datewise
            approved_loans = Payments.objects.filter(create_date__startswith=new_date, category='Rbl',
                                                     status='Success').order_by('-create_date')
            approved_dataframe = read_frame(approved_loans)
            approved_dataframe['MonthDay'] = pd.to_datetime(approved_dataframe['create_date']).apply(
                lambda x: '{month}-{day}'.format(month=x.month, day=x.day))
            date_wise_approved_loans_dict = \
                approved_dataframe.groupby(approved_dataframe['MonthDay'])['transaction_id'].count().sort_index(axis=0)
            date_wise_approved_loans_dict = dict(date_wise_approved_loans_dict)

            # For Counting Completed loans datewise
            created_date = ""
            created_date_list = []
            users = LoanApplication.objects.filter(status="COMPLETED")
            for user in users:
                amounts = user.repayment_amount.filter(create_date__startswith=new_date, status='success').order_by(
                    '-create_date').first()
                if amounts:
                    p = datetime.datetime.strftime(amounts.create_date, "%Y-%m-%d")
                    created_date_list.append(p)

            dataframe = pd.DataFrame(created_date_list)
            dataframe = pd.DataFrame(created_date_list, columns=['Dates'])
            dataframe['MonthDay'] = pd.to_datetime(dataframe['Dates']).apply(
                lambda x: '{month}-{day}'.format(month=x.month, day=x.day))
            completed_loans = dataframe.groupby(dataframe['MonthDay'])['Dates'].count().sort_index(axis=0)
            completed_loans_dict = dict(completed_loans)
            all_dict = [datewise_submitted_loans_dict, date_wise_approved_loans_dict, completed_loans_dict]
            allkey = reduce(set.union, map(set, map(dict.keys, all_dict)))
            for i in allkey:
                if i in date_wise_approved_loans_dict:
                    pass
                else:
                    date_wise_approved_loans_dict[i] = None
                if i in completed_loans_dict:
                    pass
                else:
                    completed_loans_dict[i] = None
                if i in datewise_submitted_loans_dict:
                    pass
                else:
                    datewise_submitted_loans_dict[i] = None
            All_Dicts = [datewise_submitted_loans_dict, date_wise_approved_loans_dict, completed_loans_dict]
            for k in datewise_submitted_loans_dict.keys():
                Main_Dict[k] = list(Main_Dict[k] for Main_Dict in All_Dicts)
            ctx['MainData'] = Main_Dict
            ctx['year'] = y
            return render(request, self.template_name, ctx)
        except Exception as e:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class LoanDisbursedRepaymentMonthwise(generic.ListView):
    template_name = "admin/UserAccounts/disbursed_repayment_monthwise.html"
    title = ' Disbursed Repayment Monthwise'

    def get(self, request, *args, **kwargs):
        MainDict = {}
        month_wise_disbursed_amount_dict = {}
        month_wise_repayment_amount_dict = {}
        ctx = {}
        try:
            disbursed = Payments.objects.filter(category='Rbl', status__in=['Success', 'success']).order_by('-date')
            disbursed_dataframe = read_frame(disbursed)
            disbursed_dataframe['YearMonth'] = pd.to_datetime(disbursed_dataframe['date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            month_wise_disbursed_amount_dict = \
                disbursed_dataframe.groupby(disbursed_dataframe['YearMonth'])['amount'].sum().sort_index(axis=0)
            month_wise_disbursed_amount_dict = dict(month_wise_disbursed_amount_dict)
            repayment = Payments.objects.filter(category='Loan', status__in=['Success', 'success']).order_by('-date')
            repayment_dataframe = read_frame(repayment)
            repayment_dataframe['YearMonth'] = pd.to_datetime(repayment_dataframe['date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            month_wise_repayment_amount_dict = \
                repayment_dataframe.groupby(repayment_dataframe['YearMonth'])['amount'].sum().sort_index(axis=0)
            month_wise_repayment_amount_dict = dict(month_wise_repayment_amount_dict)
            BothDicts = [month_wise_disbursed_amount_dict, month_wise_repayment_amount_dict]
            MainDict = {}
            for k in month_wise_disbursed_amount_dict.keys():
                MainDict[k] = list(MainDict[k] for MainDict in BothDicts)
            ctx['MainData'] = MainDict
            return render(request, self.template_name, ctx)
        except:
            pass
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class LoanDisbursedRepaymentDatewise(generic.ListView):
    template_name = "admin/UserAccounts/disbursed_repayment_datewise.html"
    title = 'Disbursed Repayment Datewise'

    def get(self, request, on_month, *args, **kwargs):
        ctx = {}
        new_date = ""
        try:
            date_list = on_month.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
                new_date = '{}-{}'.format(y, m)
            else:
                m = '0' + str(date_list[1])
                new_date = '{}-{}'.format(y, m)
            date_set = set()
            disbursed = Payments.objects.filter(date__startswith=new_date).order_by('-date')
            for user in disbursed:
                dt = str(user.date)
                date_set.add(dt[0:10])
            create_month_1 = []
            disbursed_amount = []
            for dt in date_set:
                cost_sum = Payments.objects.filter(date__startswith=dt, category='Rbl',
                                                   status__in=['Success', 'success']).aggregate(
                    Sum('amount'))['amount__sum']
                create_month_1.append(dt)
                disbursed_amount.append(cost_sum)
            create_month_2 = []
            repayment_amount = []
            for dt in date_set:
                cost_sum = Payments.objects.filter(date__startswith=dt, category='Loan',
                                                   status__in=['Success', 'success']).aggregate(
                    Sum('amount'))['amount__sum']
                create_month_2.append(dt)
                repayment_amount.append(cost_sum)
            ctx['amount_info'] = zip((create_month_1), disbursed_amount, repayment_amount)
            return render(request, self.template_name, ctx)
        except Exception as e:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class DateReport(generic.ListView):
    def get(self, request, *args, **kwargs):
        template_name = 'admin/UserAccounts/report.html'

        return render(request, template_name)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class ReportBetweenDates(generic.ListView):
    def post(self, request, *args, **kwargs):
        template_name = 'admin/UserAccounts/new_dashboard.html'
        amount_dict = {}
        data = {}
        ctx = {}
        amount_list = []
        try:

            if request.method == 'POST':
                start_date = request.POST['start_date']
                end_date = request.POST['end_date']
                payment_type = request.POST['type']
                data['start_date'] = start_date
                data['end_date'] = end_date
                data['payment_type'] = payment_type
                ctx['data'] = data
                s = datetime.datetime.strptime(start_date, "%Y-%m-%d").date()
                e = datetime.datetime.strptime(end_date, "%Y-%m-%d").date() + timedelta(days=1)
                if payment_type == "Disbursed":
                    users = Payments.objects.filter(date__range=(s, e))
                    if users:
                        for user in users:
                            amount = LoanApplication.objects.filter(disbursed_amount_id=user.id)
                            rrn = RBLTransaction.objects.filter(payment_id=user.id)
                            if amount:
                                amount_dict = {}
                                amount_dict['Name'] = amount[0].user_personal.name
                                amount_dict['Mobile_number'] = amount[0].user.username
                                amount_dict['LoanApplicationId'] = amount[0].loan_application_id
                                amount_dict['Loan_Amount'] = amount[0].loan_scheme_id
                                amount_dict['Amount'] = user.amount
                                amount_dict['date'] = user.date
                                amount_dict['bank_acc_no'] = amount[0].bank_id.account_number
                                amount_dict['stauts'] = user.status
                                amount_dict['RRN'] = rrn[0].rrn
                                amount_list.append(amount_dict)
                else:
                    users = Payments.objects.filter(date__range=(s, e), status__icontains="Success")
                    if users:
                        for user in users:
                            amounts = LoanApplication.objects.filter(repayment_amount=user.id)
                            if amounts:
                                for amount in amounts:
                                    amount_dict = {}
                                    amount_dict['Name'] = amount.user_personal.name
                                    amount_dict['Mobile_number'] = amount.user.username
                                    amount_dict['LoanApplicationId'] = amount.loan_application_id
                                    amount_dict['Loan_Amount'] = amount.loan_scheme_id
                                    amount_dict['Amount'] = user.amount
                                    amount_dict['date'] = user.date
                                    amount_dict['bank_acc_no'] = amount.bank_id.account_number
                                    amount_dict['stauts'] = user.status
                                    rrn = RBLTransaction.objects.filter(payment_id=user.id)
                                    if rrn:
                                        amount_dict['RRN'] = rrn[0].rrn
                                    amount_list.append(amount_dict)
            ctx['user'] = amount_list
            return render(request, template_name, ctx)
        except:
            pass
            return render(request, template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class CsvDownload(generic.ListView):
    def post(self, request, *args, **kwargs):
        # Create the HttpResponse object with the appropriate CSV header.
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="Loan_details_2019.csv"'
        writer = csv.writer(response)
        writer.writerow(
            ['Name', 'Mobile number', 'LoanApplicationId', 'Loan Amount', 'Amount', 'RRN', 'date', 'Bank Acc.No.',
             'Status'])
        if request.method == 'POST':
            start_date = request.POST['start_date']
            end_date = request.POST['end_date']
            payment_type = request.POST['payment_type']
            s = datetime.datetime.strptime(start_date, "%Y-%m-%d").date()
            e = datetime.datetime.strptime(end_date, "%Y-%m-%d").date() + timedelta(days=1)
            try:
                if payment_type == "Disbursed":
                    users = Payments.objects.filter(date__range=(s, e))
                    if users:
                        for user in users:
                            amount = LoanApplication.objects.filter(disbursed_amount_id=user.id)
                            rrn = RBLTransaction.objects.filter(payment_id=user.id)
                            if amount:
                                writer.writerow([amount[0].user_personal.name,
                                                 amount[0].user.username,
                                                 amount[0].loan_application_id,
                                                 amount[0].loan_scheme_id,
                                                 user.amount,
                                                 rrn[0].rrn,
                                                 user.date,
                                                 amount[0].bank_id.account_number,
                                                 user.status])
                else:
                    users = Payments.objects.filter(date__range=(s, e), status__icontains="Success")
                    if users:
                        for user in users:
                            amount = LoanApplication.objects.filter(repayment_amount=user.id)
                            if amount:
                                writer.writerow([amount[0].user_personal.name,
                                                 amount[0].user.username,
                                                 amount[0].loan_application_id,
                                                 amount[0].loan_scheme_id,
                                                 user.amount,
                                                 "",
                                                 user.date,
                                                 amount[0].bank_id.account_number,
                                                 user.status])

            except:
                pass
            return response


from functools import reduce


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class LoanDetailsMonthwise(generic.ListView):
    template_name = "admin/UserAccounts/loan_details_monthwise.html"
    title = ' Loan Details Monthwise'

    def get(self, request, *args, **kwargs):
        MainDict = {}
        monthwise_submitted_loans_dict = {}
        month_wise_approved_loans_dict = {}
        completed_loans_dict = {}
        all_dict = {}
        ctx = {}
        try:
            # For Counting Submitted loans monthwise
            submitted_loans = LoanApplication.objects.all()
            submitted_dataframe = read_frame(submitted_loans)
            submitted_dataframe['YearMonth'] = pd.to_datetime(submitted_dataframe['created_date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            monthwise_submitted_loans_dict = \
                submitted_dataframe.groupby(submitted_dataframe['YearMonth'])['loan_application_id'].count().sort_index(
                    axis=0)
            monthwise_submitted_loans_dict = dict(monthwise_submitted_loans_dict)

            # For Counting Approved loans monthwise
            approved_loans = Payments.objects.filter(category='Rbl', status='Success').order_by('-create_date')
            approved_dataframe = read_frame(approved_loans)
            approved_dataframe['YearMonth'] = pd.to_datetime(approved_dataframe['create_date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            month_wise_approved_loans_dict = \
                approved_dataframe.groupby(approved_dataframe['YearMonth'])['transaction_id'].count().sort_index(axis=0)
            month_wise_approved_loans_dict = dict(month_wise_approved_loans_dict)

            # For Counting Completed loans monthwise
            created_date = ""
            created_date_list = []
            users = LoanApplication.objects.filter(status="COMPLETED")
            for user in users:
                amounts = user.repayment_amount.filter(status='success').order_by('-create_date').first()
                if amounts:
                    p = datetime.datetime.strftime(amounts.create_date, "%Y-%m-%d")
                    created_date_list.append(p)

            dataframe = pd.DataFrame(created_date_list)
            dataframe = pd.DataFrame(created_date_list, columns=['Dates'])
            dataframe['YearMonth'] = pd.to_datetime(dataframe['Dates']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            completed_loans = dataframe.groupby(dataframe['YearMonth'])['Dates'].count().sort_index(axis=0)
            completed_loans_dict = dict(completed_loans)
            all_dict = [monthwise_submitted_loans_dict, month_wise_approved_loans_dict, completed_loans_dict]
            allkey = reduce(set.union, map(set, map(dict.keys, all_dict)))
            for i in allkey:
                if i in month_wise_approved_loans_dict:
                    pass
                else:
                    month_wise_approved_loans_dict[i] = None
                if i in completed_loans_dict:
                    pass
                else:
                    completed_loans_dict[i] = None
                if i in monthwise_submitted_loans_dict:
                    pass
                else:
                    monthwise_submitted_loans_dict[i] = None
            All_Dicts = [monthwise_submitted_loans_dict, month_wise_approved_loans_dict, completed_loans_dict]
            Main_Dict = {}
            for k in monthwise_submitted_loans_dict.keys():
                Main_Dict[k] = list(Main_Dict[k] for Main_Dict in All_Dicts)
            ctx['MainData'] = Main_Dict

            return render(request, self.template_name, ctx)
        except Exception as e:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class LoanDetailsDatewise(generic.ListView):
    template_name = "admin/UserAccounts/loan_details_datewise.html"
    title = ' Loan Details Datewise'

    def get(self, request, on_month, *args, **kwargs):
        Main_Dict = {}
        datewise_submitted_loans_dict = {}
        date_wise_approved_loans_dict = {}
        completed_loans_dict = {}
        all_dict = {}
        ctx = {}
        new_date = ""
        try:
            date_list = on_month.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
                new_date = '{}-{}'.format(y, m)
            else:
                m = '0' + str(date_list[1])
                new_date = '{}-{}'.format(y, m)

            # For Counting Submitted loans Datewise
            submitted_loans = LoanApplication.objects.filter(created_date__startswith=new_date).order_by(
                '-created_date')
            submitted_dataframe = read_frame(submitted_loans)
            submitted_dataframe['MonthDay'] = pd.to_datetime(submitted_dataframe['created_date']).apply(
                lambda x: '{month}-{day}'.format(month=x.month, day=x.day))
            datewise_submitted_loans_dict = \
                submitted_dataframe.groupby(submitted_dataframe['MonthDay'])['loan_application_id'].count().sort_index(
                    axis=0)
            datewise_submitted_loans_dict = dict(datewise_submitted_loans_dict)

            # For Counting Approved loans datewise
            approved_loans = Payments.objects.filter(create_date__startswith=new_date, category='Rbl',
                                                     status='Success').order_by('-create_date')
            approved_dataframe = read_frame(approved_loans)
            approved_dataframe['MonthDay'] = pd.to_datetime(approved_dataframe['create_date']).apply(
                lambda x: '{month}-{day}'.format(month=x.month, day=x.day))
            date_wise_approved_loans_dict = \
                approved_dataframe.groupby(approved_dataframe['MonthDay'])['transaction_id'].count().sort_index(axis=0)
            date_wise_approved_loans_dict = dict(date_wise_approved_loans_dict)

            # For Counting Completed loans datewise
            created_date = ""
            created_date_list = []
            users = LoanApplication.objects.filter(status="COMPLETED")
            for user in users:
                amounts = user.repayment_amount.filter(create_date__startswith=new_date, status='success').order_by(
                    '-create_date').first()
                if amounts:
                    p = datetime.datetime.strftime(amounts.create_date, "%Y-%m-%d")
                    created_date_list.append(p)

            dataframe = pd.DataFrame(created_date_list)
            dataframe = pd.DataFrame(created_date_list, columns=['Dates'])
            dataframe['MonthDay'] = pd.to_datetime(dataframe['Dates']).apply(
                lambda x: '{month}-{day}'.format(month=x.month, day=x.day))
            completed_loans = dataframe.groupby(dataframe['MonthDay'])['Dates'].count().sort_index(axis=0)
            completed_loans_dict = dict(completed_loans)
            all_dict = [datewise_submitted_loans_dict, date_wise_approved_loans_dict, completed_loans_dict]
            allkey = reduce(set.union, map(set, map(dict.keys, all_dict)))
            for i in allkey:
                if i in date_wise_approved_loans_dict:
                    pass
                else:
                    date_wise_approved_loans_dict[i] = None
                if i in completed_loans_dict:
                    pass
                else:
                    completed_loans_dict[i] = None
                if i in datewise_submitted_loans_dict:
                    pass
                else:
                    datewise_submitted_loans_dict[i] = None
            All_Dicts = [datewise_submitted_loans_dict, date_wise_approved_loans_dict, completed_loans_dict]
            for k in datewise_submitted_loans_dict.keys():
                Main_Dict[k] = list(Main_Dict[k] for Main_Dict in All_Dicts)
            ctx['MainData'] = Main_Dict
            ctx['year'] = y
            return render(request, self.template_name, ctx)
        except Exception as e:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class DatewiseDisbursmentView(generic.ListView):
    template_name = "admin/UserAccounts/datewise_disbursment_details.html"
    title = 'Datewise Disbursment'

    def get(self, request, on_date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        amount_list = []
        try:
            date_list = on_date.split('-')
            y = date_list[0]

            if len(date_list[1]) > 1:
                m = date_list[1]
                new_date = '{}-{}-{}'.format(y, m, d)
            else:
                m = '0' + str(date_list[1])
                new_date = '{}-{}-{}'.format(y, m, d)

            if len(date_list[2]) == 2:
                d = date_list[2]
                new_date = '{}-{}-{}'.format(y, m, d)
            else:
                d = '0' + str(date_list[2])
                new_date = '{}-{}-{}'.format(y, m, d)

            users = Payments.objects.filter(date__startswith=new_date)
            if users:
                for user in users:
                    amount = LoanApplication.objects.filter(disbursed_amount_id=user.id)
                    rrn = RBLTransaction.objects.filter(payment_id=user.id)
                    if amount and rrn:
                        amount_dict = {}
                        amount_dict['User'] = amount[0].user.username
                        amount_dict['created_date'] = rrn[0].created_date
                        amount_dict['transaction_id_text'] = rrn[0].transaction_id_text
                        amount_dict['corp_id'] = rrn[0].corp_id
                        amount_dict['ref_number'] = rrn[0].ref_number
                        amount_dict['channel_part_ref_number'] = rrn[0].channel_part_ref_number
                        amount_dict['amount'] = rrn[0].amount
                        amount_dict['ben_acc_no'] = rrn[0].ben_acc_no
                        amount_list.append(amount_dict)
            ctx['users'] = amount_list
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class DatewiseRepaymentView(generic.ListView):
    template_name = "admin/UserAccounts/datewise_repayment_details.html"
    title = 'Datewise Repayment'

    def get(self, request, on_date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        amount_list = []
        try:
            date_list = on_date.split('-')
            y = date_list[0]

            if len(date_list[1]) > 1:
                m = date_list[1]
                new_date = '{}-{}-{}'.format(y, m, d)
            else:
                m = '0' + str(date_list[1])
                new_date = '{}-{}-{}'.format(y, m, d)

            if len(date_list[2]) == 2:
                d = date_list[2]
                new_date = '{}-{}-{}'.format(y, m, d)
            else:
                d = '0' + str(date_list[2])
                new_date = '{}-{}-{}'.format(y, m, d)

            users = Payments.objects.filter(date__startswith=new_date, status__icontains="Success")
            if users:
                for user in users:
                    amounts = LoanApplication.objects.filter(repayment_amount=user.id)
                    if amounts:
                        for amount in amounts:
                            amount_dict = {}
                            amount_dict['Amount'] = user.amount
                            amount_dict['transaction_date'] = user.date
                            amount_dict['LoanApplicationId'] = amount.loan_application_id
                            amount_dict['type'] = user.type
                            amount_dict['status'] = user.status
                            amount_dict['Order_id'] = user.order_id
                            amount_dict['Username'] = amount.user.username
                            amount_list.append(amount_dict)
            ctx['users'] = amount_list
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


def OverdueView(request):
    today = datetime.datetime.now()
    day_before_yesterday = (datetime.datetime.now() - timedelta(2)).strftime('%Y-%m-%d')
    yesterday = datetime.datetime.now() - timedelta(days=1)
    yesterday_1 = (datetime.datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d')
    day_before_yesterday_1 = datetime.datetime.now() - timedelta(days=2)
    overdue_counter = 0
    recovery_overdue_counter = 0
    default_counter = 0
    recovery_default_counter = 0
    loan_users = LoanApplication.objects.filter(status="APPROVED", loan_end_date__startswith=day_before_yesterday)

    for user in loan_users:
        if user.status != 'COMPLETED':
            if yesterday > user.loan_end_date:
                overdue_counter += 1

    users = LoanApplication.objects.filter(status="APPROVED")
    for user in users:
        if user.status != 'COMPLETED':
            for i in scheme:
                if i == user.loan_scheme_id:
                    if user.loan_end_date:
                        return_date = user.loan_end_date + timedelta(int(scheme[i]['ms_tenure']))
                        if (yesterday - return_date).days == 1:
                            default_counter += 1

    recovery_users = Recovery.objects.filter(completed_date__startswith=yesterday_1, recovery_status="Yes")
    if recovery_users:
        for user in recovery_users:
            if user.loan_transaction:
                if user.loan_transaction.status:
                    if user.loan_transaction.status == "COMPLETED":
                        for i in scheme:
                            if user.loan_transaction.loan_scheme_id:
                                if i == user.loan_transaction.loan_scheme_id:
                                    if user.loan_transaction.loan_end_date:
                                        return_date = user.loan_transaction.loan_end_date + timedelta(
                                            int(scheme[i]['ms_tenure']))
                                        if user.completed_date:
                                            if user.completed_date > return_date:
                                                recovery_default_counter += 1
                                            elif user.completed_date > user.loan_transaction.loan_end_date:
                                                recovery_overdue_counter += 1
    date = OverdueReport.objects.filter(date__startswith=yesterday_1)
    if date:
        return HttpResponse('Yesterdays data is already saved..')
    else:
        OverdueReport.objects.create(date=yesterday, overdue_count=overdue_counter,
                                     defaulter_count=default_counter,
                                     recovery_overdue_count=recovery_overdue_counter,
                                     recovery_defaulter_count=recovery_default_counter)
        return HttpResponse('Data saved Successfully')


def OverdueView_sch():
    today = datetime.datetime.now()
    day_before_yesterday = (datetime.datetime.now() - timedelta(2)).strftime('%Y-%m-%d')
    yesterday = datetime.datetime.now() - timedelta(days=1)
    yesterday_1 = (datetime.datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d')
    day_before_yesterday_1 = datetime.datetime.now() - timedelta(days=2)
    overdue_counter = 0
    recovery_overdue_counter = 0
    default_counter = 0
    recovery_default_counter = 0
    loan_users = LoanApplication.objects.filter(status="APPROVED", loan_end_date__startswith=day_before_yesterday)

    for user in loan_users:
        if user.status != 'COMPLETED':
            if yesterday > user.loan_end_date:
                overdue_counter += 1

    users = LoanApplication.objects.filter(status="APPROVED")
    for user in users:
        if user.status != 'COMPLETED':
            for i in scheme:
                if i == user.loan_scheme_id:
                    if user.loan_end_date:
                        return_date = user.loan_end_date + timedelta(int(scheme[i]['ms_tenure']))
                        if (yesterday - return_date).days == 1:
                            default_counter += 1

    recovery_users = Recovery.objects.filter(completed_date__startswith=yesterday_1, recovery_status="Yes")
    if recovery_users:
        for user in recovery_users:
            if user.loan_transaction:
                if user.loan_transaction.status:
                    if user.loan_transaction.status == "COMPLETED":
                        for i in scheme:
                            if user.loan_transaction.loan_scheme_id:
                                if i == user.loan_transaction.loan_scheme_id:
                                    if user.loan_transaction.loan_end_date:
                                        return_date = user.loan_transaction.loan_end_date + timedelta(
                                            int(scheme[i]['ms_tenure']))
                                        if user.completed_date:
                                            if user.completed_date > return_date:
                                                recovery_default_counter += 1
                                            elif user.completed_date > user.loan_transaction.loan_end_date:
                                                recovery_overdue_counter += 1
    date = OverdueReport.objects.filter(date__startswith=yesterday_1)
    if date:
        return HttpResponse('Yesterdays data is already saved..')
    else:
        OverdueReport.objects.create(date=yesterday, overdue_count=overdue_counter,
                                     defaulter_count=default_counter,
                                     recovery_overdue_count=recovery_overdue_counter,
                                     recovery_defaulter_count=recovery_default_counter)
        return HttpResponse('Data saved Successfully')


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class MonthwiseOverdueData(generic.ListView):
    template_name = "admin/UserAccounts/monthwise_overdue_report.html"
    title = 'Monthwise Overdue Report'

    def get(self, request, *args, **kwargs):
        MainDict = {}
        monthwise_overdue_dict = {}
        monthwise_defaulter_dict = {}
        monthwise_recovery_overdue_dict = {}
        monthwise_recovery_defaulter_dict = {}
        all_dict = {}
        ctx = {}
        try:
            # For Counting overdue monthwise
            overdue_count = OverdueReport.objects.all()
            overdue_dataframe = read_frame(overdue_count)
            overdue_dataframe['YearMonth'] = pd.to_datetime(overdue_dataframe['date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            monthwise_overdue_dict = \
                overdue_dataframe.groupby(overdue_dataframe['YearMonth'])['overdue_count'].sum().sort_index(axis=0)
            monthwise_overdue_dict = dict(monthwise_overdue_dict)

            # For Counting defaulters monthwise
            defaulter_count = OverdueReport.objects.all()
            defaulter_dataframe = read_frame(defaulter_count)
            defaulter_dataframe['YearMonth'] = pd.to_datetime(defaulter_dataframe['date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            monthwise_defaulter_dict = \
                overdue_dataframe.groupby(defaulter_dataframe['YearMonth'])['defaulter_count'].sum().sort_index(axis=0)
            monthwise_defaulter_dict = dict(monthwise_defaulter_dict)

            # For Counting recovery overdue monthwise
            recovery_overdue_count = OverdueReport.objects.all()
            recovery_overdue_dataframe = read_frame(recovery_overdue_count)
            recovery_overdue_dataframe['YearMonth'] = pd.to_datetime(recovery_overdue_dataframe['date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            monthwise_recovery_overdue_dict = \
                recovery_overdue_dataframe.groupby(overdue_dataframe['YearMonth'])[
                    'recovery_overdue_count'].sum().sort_index(axis=0)
            monthwise_recovery_overdue_dict = dict(monthwise_recovery_overdue_dict)

            # For Counting recovery defaulters monthwise
            recovery_defaulter_count = OverdueReport.objects.all()
            recovery_defaulter_dataframe = read_frame(recovery_defaulter_count)
            recovery_defaulter_dataframe['YearMonth'] = pd.to_datetime(recovery_defaulter_dataframe['date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            monthwise_recovery_defaulter_dict = \
                recovery_defaulter_dataframe.groupby(recovery_defaulter_dataframe['YearMonth']) \
                    ['recovery_defaulter_count'].sum().sort_index(axis=0)
            monthwise_recovery_defaulter_dict = dict(monthwise_recovery_defaulter_dict)

            All_Dicts = [monthwise_overdue_dict, monthwise_defaulter_dict, \
                         monthwise_recovery_overdue_dict, monthwise_recovery_defaulter_dict]
            Main_Dict = {}
            for k in monthwise_overdue_dict.keys():
                Main_Dict[k] = list(Main_Dict[k] for Main_Dict in All_Dicts)
            ctx['MainData'] = Main_Dict

            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class DatewiseOverdueData(generic.ListView):
    template_name = "admin/UserAccounts/datewise_overdue_report.html"
    title = 'datewise Overdue Report'

    def get(self, request, on_month, *args, **kwargs):

        MainDict = {}
        datewise_overdue_dict = {}
        datewise_defaulter_dict = {}
        datewise_recovery_overdue_dict = {}
        datewise_recovery_defaulter_dict = {}
        all_dict = {}
        new_date = ""
        ctx = {}
        date_list = on_month.split('-')
        y = date_list[0]
        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}'.format(y, m)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}'.format(y, m)

        # For Counting overdue datewise
        overdue_count = OverdueReport.objects.filter(date__startswith=new_date)
        overdue_dataframe = read_frame(overdue_count)
        overdue_dataframe['MonthDay'] = pd.to_datetime(overdue_dataframe['date']).apply(
            lambda x: '{month}-{day}'.format(month=x.month, day=x.day))
        datewise_overdue_dict = \
            overdue_dataframe.groupby(overdue_dataframe['MonthDay'])['overdue_count'].sum().sort_index(axis=0)
        datewise_overdue_dict = dict(datewise_overdue_dict)

        # For Counting defaulters datewise
        defaulter_count = OverdueReport.objects.filter(date__startswith=new_date)
        defaulter_dataframe = read_frame(defaulter_count)
        defaulter_dataframe['MonthDay'] = pd.to_datetime(defaulter_dataframe['date']).apply(
            lambda x: '{month}-{day}'.format(month=x.month, day=x.day))
        datewise_defaulter_dict = \
            defaulter_dataframe.groupby(defaulter_dataframe['MonthDay'])['defaulter_count'].sum().sort_index(axis=0)
        datewise_defaulter_dict = dict(datewise_defaulter_dict)

        # For Counting recovery overdue datewise
        recovery_overdue_count = OverdueReport.objects.filter(date__startswith=new_date)
        recovery_overdue_dataframe = read_frame(recovery_overdue_count)
        recovery_overdue_dataframe['MonthDay'] = pd.to_datetime(recovery_overdue_dataframe['date']).apply(
            lambda x: '{month}-{day}'.format(month=x.month, day=x.day))
        datewise_recovery_overdue_dict = \
            recovery_overdue_dataframe.groupby(recovery_overdue_dataframe['MonthDay']) \
                ['recovery_overdue_count'].sum().sort_index(axis=0)
        datewise_recovery_overdue_dict = dict(datewise_recovery_overdue_dict)

        # For Counting recovery overdue datewise
        recovery_defaulter_count = OverdueReport.objects.filter(date__startswith=new_date)
        recovery_defaulter_dataframe = read_frame(recovery_defaulter_count)
        recovery_defaulter_dataframe['MonthDay'] = pd.to_datetime(recovery_defaulter_dataframe['date']).apply(
            lambda x: '{month}-{day}'.format(month=x.month, day=x.day))
        datewise_recovery_defaulter_dict = \
            recovery_defaulter_dataframe.groupby(recovery_defaulter_dataframe['MonthDay']) \
                ['recovery_defaulter_count'].sum().sort_index(axis=0)
        datewise_recovery_defaulter_dict = dict(datewise_recovery_defaulter_dict)

        All_Dicts = [datewise_overdue_dict, datewise_defaulter_dict, \
                     datewise_recovery_overdue_dict, datewise_recovery_defaulter_dict]
        Main_Dict = {}
        for k in datewise_overdue_dict.keys():
            Main_Dict[k] = list(Main_Dict[k] for Main_Dict in All_Dicts)
        ctx['MainData'] = Main_Dict
        ctx['year'] = y
        return render(request, self.template_name, ctx)


class MonthlyAnalysysByPaymentStrategy(generic.ListView):
    template_name = "admin/UserAccounts/Monthly_payment_strategy_analysys.html"
    title = ' Monthly Repayment Monthwise'

    def get(self, request, *args, **kwargs):
        MainDict = {}
        month_wise_disbursed_amount_dict = {}
        month_wise_repayment_amount_dict = {}
        ctx = {}
        try:
            loan_users = LoanApplication.objects.all().filter \
                (status__in=['APPROVED', 'COMPLETED']).values_list('user', flat=True)
            unique_loan_users = set(loan_users)
            users = User.objects.filter(id__in=unique_loan_users)
            users_dataframe = read_frame(users)
            users_dataframe['YearMonth'] = pd.to_datetime(users_dataframe['date_joined']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            monthly_users_dict = \
                users_dataframe.groupby(users_dataframe['YearMonth'])['id'].count().sort_index(axis=0)
            monthly_users_dict = dict(monthly_users_dict)
            ctx['users'] = monthly_users_dict
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


class DaywiseAnalysysByPaymentStrategy(generic.ListView):
    template_name = "admin/UserAccounts/daywise_payment_strategy_analysys.html"
    title = ' daily payment strategy '

    def get(self, request, on_month, *args, **kwargs):
        new_date = ""
        ctx = {}
        date_list = on_month.split('-')
        y = date_list[0]
        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}'.format(y, m)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}'.format(y, m)

        loan_users = LoanApplication.objects.all().filter \
            (status__in=['APPROVED', 'COMPLETED']).values_list('user', flat=True)
        unique_loan_users = set(loan_users)
        users = User.objects.filter(id__in=unique_loan_users).filter(date_joined__startswith=new_date)
        users_dataframe = read_frame(users)
        users_dataframe['YearMonthDay'] = pd.to_datetime(users_dataframe['date_joined']).apply(
            lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
        datewise_users_dict = \
            users_dataframe.groupby(users_dataframe['YearMonthDay'])['id'].count().sort_index(axis=0)
        datewise_users_dict = dict(datewise_users_dict)
        ctx['users'] = datewise_users_dict
        ctx['year'] = y
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class AnalysysByPaymentStrategy(generic.ListView):
    template_name = "admin/UserAccounts/payment_strategy_analysys.html"
    title = 'Daily Repayments'

    def get(self, request, on_date, *args, **kwargs):
        ctx = {}
        user_list = []
        user_id_list = []
        user_loancount_list = []
        user_transaction_list = []
        new_date = ''
        m = ''
        d = ''
        try:
            date_list = on_date.split('-')
            y = date_list[0]

            if len(date_list[1]) > 1:
                m = date_list[1]
                new_date = '{}-{}-{}'.format(y, m, d)
            else:
                m = '0' + str(date_list[1])
                new_date = '{}-{}-{}'.format(y, m, d)
            if len(date_list[2]) == 2:
                d = date_list[2]
                new_date = '{}-{}-{}'.format(y, m, d)
            else:
                d = '0' + str(date_list[2])
                new_date = '{}-{}-{}'.format(y, m, d)

            strategy_users = LoanApplication.objects.all().filter \
                (status__in=['APPROVED', 'COMPLETED']).values_list('user', flat=True)
            unique_strategy_users = set(strategy_users)
            users = User.objects.filter(id__in=unique_strategy_users).filter \
                (date_joined__startswith=new_date)

            for user in users:
                loan_count = 0
                repayment_txc_count = 0
                loan_ids = LoanApplication.objects.all().order_by(
                    'loan_application_id').distinct('loan_application_id').filter(user=user.id,
                                                                                  status__in=['APPROVED', 'COMPLETED'])
                loan_count = len(loan_ids)
                for id in loan_ids:
                    rep_amount = id.repayment_amount.filter(
                        status='success').aggregate(Count('amount'))['amount__count']
                    repayment_txc_count += rep_amount
                user_list.append(user)
                user_id_list.append(user.id)
                user_loancount_list.append(loan_count)
                user_transaction_list.append(repayment_txc_count)

            ctx['users'] = zip(user_list, user_loancount_list, user_transaction_list, user_id_list)

            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class LoanStatusDetails(generic.ListView):
    template_name = "admin/UserAccounts/monthwise_loan_status_details.html"
    title = ' Monthwise Loan Status Details '

    def get(self, request, *args, **kwargs):
        ctx = {}
        Main_Dict = {}
        monthly_users_dict = {}
        approved_users_dict = {}
        onhold_users_dict = {}
        submitted_users_dict = {}
        total_completed = {}
        total_cancelled = {}

        total_users = LoanApplication.objects.filter(status__in= \
                                                         ['APPROVED', 'COMPLETED', 'ON_HOLD', 'CANCELLED', 'SUBMITTED'])
        users_dataframe = read_frame(total_users)
        users_dataframe['YearMonth'] = pd.to_datetime(users_dataframe['loan_start_date']).apply(
            lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
        monthly_users_dict = \
            users_dataframe.groupby(users_dataframe['YearMonth'])['id'].count().sort_index(axis=0)
        monthly_users_dict = dict(monthly_users_dict)

        total_submited = LoanApplication.objects.filter(status="SUBMITTED")
        submitted_dataframe = read_frame(total_submited)
        submitted_dataframe['YearMonth'] = pd.to_datetime(submitted_dataframe['loan_start_date']).apply(
            lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
        submitted_users_dict = \
            submitted_dataframe.groupby(submitted_dataframe['YearMonth'])['id'].count().sort_index(axis=0)
        submitted_users_dict = dict(submitted_users_dict)

        total_Approved = LoanApplication.objects.filter(status="APPROVED")
        approved_dataframe = read_frame(total_Approved)
        approved_dataframe['YearMonth'] = pd.to_datetime(approved_dataframe['loan_start_date']).apply(
            lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
        approved_users_dict = \
            approved_dataframe.groupby(approved_dataframe['YearMonth'])['id'].count().sort_index(axis=0)
        approved_users_dict = dict(approved_users_dict)

        total_onhold = LoanApplication.objects.filter(status="ON_HOLD")
        onhold_dataframe = read_frame(total_onhold)
        onhold_dataframe['YearMonth'] = pd.to_datetime(onhold_dataframe['loan_start_date']).apply(
            lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
        onhold_users_dict = \
            onhold_dataframe.groupby(onhold_dataframe['YearMonth'])['id'].count().sort_index(axis=0)
        onhold_users_dict = dict(onhold_users_dict)

        total_completed = LoanApplication.objects.filter(status="COMPLETED")
        completed_dataframe = read_frame(total_completed)
        completed_dataframe['YearMonth'] = pd.to_datetime(completed_dataframe['loan_start_date']).apply(
            lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
        completed_users_dict = \
            completed_dataframe.groupby(completed_dataframe['YearMonth'])['id'].count().sort_index(axis=0)
        completed_users_dict = dict(completed_users_dict)

        total_cancelled = LoanApplication.objects.filter(status="CANCELLED")
        cancelled_dataframe = read_frame(total_cancelled)
        cancelled_dataframe['YearMonth'] = pd.to_datetime(cancelled_dataframe['loan_start_date']).apply(
            lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
        cancelled_users_dict = \
            cancelled_dataframe.groupby(cancelled_dataframe['YearMonth'])['id'].count().sort_index(axis=0)
        cancelled_users_dict = dict(cancelled_users_dict)

        Main_Dict = {}
        All_Dicts = [monthly_users_dict, approved_users_dict, onhold_users_dict,
                     completed_users_dict, cancelled_users_dict, submitted_users_dict]

        allkey = reduce(set.union, map(set, map(dict.keys, All_Dicts)))
        for i in allkey:
            if i in monthly_users_dict:
                pass
            else:
                monthly_users_dict[i] = None
            if i in approved_users_dict:
                pass
            else:
                approved_users_dict[i] = None
            if i in onhold_users_dict:
                pass
            else:
                onhold_users_dict[i] = None
            if i in submitted_users_dict:
                pass
            else:
                submitted_users_dict[i] = None
            if i in completed_users_dict:
                pass
            else:
                completed_users_dict[i] = None
            if i in cancelled_users_dict:
                pass
            else:
                cancelled_users_dict[i] = None

        for k in monthly_users_dict.keys():
            Main_Dict[k] = list(Main_Dict[k] for Main_Dict in All_Dicts)

        ctx['MainData'] = Main_Dict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class DaywiseLoanStatusDetails(generic.ListView):
    template_name = "admin/UserAccounts/daywise_loan_status_details.html"
    title = ' Daywise Loan Status Details '

    def get(self, request, on_month, *args, **kwargs):
        ctx = {}
        Main_Dict = {}
        monthly_users_dict = {}
        approved_users_dict = {}
        onhold_users_dict = {}
        submitted_users_dict = {}
        total_completed = {}
        total_cancelled = {}
        new_date = ""
        date_list = on_month.split('-')
        y = date_list[0]
        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}'.format(y, m)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}'.format(y, m)

        total_users = LoanApplication.objects.filter(loan_start_date__startswith=new_date).filter(
            status__in=['APPROVED', 'COMPLETED', 'ON_HOLD', 'CANCELLED', 'SUBMITTED'])
        users_dataframe = read_frame(total_users)
        users_dataframe['YearMonthDay'] = pd.to_datetime(users_dataframe['loan_start_date']).apply(
            lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
        monthly_users_dict = \
            users_dataframe.groupby(users_dataframe['YearMonthDay'])['id'].count().sort_index(axis=0)
        monthly_users_dict = dict(monthly_users_dict)

        total_Approved = LoanApplication.objects.filter(loan_start_date__startswith=new_date).filter(
            status="APPROVED")
        approved_dataframe = read_frame(total_Approved)
        approved_dataframe['YearMonthDay'] = pd.to_datetime(approved_dataframe['loan_start_date']).apply(
            lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
        approved_users_dict = \
            approved_dataframe.groupby(approved_dataframe['YearMonthDay'])['id'].count().sort_index(axis=0)
        approved_users_dict = dict(approved_users_dict)

        total_submitted = LoanApplication.objects.filter(loan_start_date__startswith=new_date).filter(
            status="SUBMITTED")
        submitted_dataframe = read_frame(total_submitted)
        submitted_dataframe['YearMonthDay'] = pd.to_datetime(submitted_dataframe['loan_start_date']).apply(
            lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
        submitted_users_dict = \
            submitted_dataframe.groupby(submitted_dataframe['YearMonthDay'])['id'].count().sort_index(axis=0)
        submitted_users_dict = dict(submitted_users_dict)

        total_onhold = LoanApplication.objects.filter(loan_start_date__startswith=new_date).filter(
            status="ON_HOLD")
        onhold_dataframe = read_frame(total_onhold)
        onhold_dataframe['YearMonthDay'] = pd.to_datetime(onhold_dataframe['loan_start_date']).apply(
            lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
        onhold_users_dict = \
            onhold_dataframe.groupby(onhold_dataframe['YearMonthDay'])['id'].count().sort_index(axis=0)
        onhold_users_dict = dict(onhold_users_dict)

        total_completed = LoanApplication.objects.filter(loan_start_date__startswith=new_date).filter(
            status="COMPLETED")
        completed_dataframe = read_frame(total_completed)
        completed_dataframe['YearMonthDay'] = pd.to_datetime(completed_dataframe['loan_start_date']).apply(
            lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
        completed_users_dict = \
            completed_dataframe.groupby(completed_dataframe['YearMonthDay'])['id'].count().sort_index(axis=0)
        completed_users_dict = dict(completed_users_dict)

        total_cancelled = LoanApplication.objects.filter(loan_start_date__startswith=new_date).filter(
            status="CANCELLED")
        cancelled_dataframe = read_frame(total_cancelled)
        cancelled_dataframe['YearMonthDay'] = pd.to_datetime(cancelled_dataframe['loan_start_date']).apply(
            lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
        cancelled_users_dict = \
            cancelled_dataframe.groupby(cancelled_dataframe['YearMonthDay'])['id'].count().sort_index(axis=0)
        cancelled_users_dict = dict(cancelled_users_dict)

        Main_Dict = {}
        All_Dicts = [monthly_users_dict, approved_users_dict, onhold_users_dict,
                     completed_users_dict, cancelled_users_dict, submitted_users_dict]

        allkey = reduce(set.union, map(set, map(dict.keys, All_Dicts)))
        for i in allkey:
            if i in monthly_users_dict:
                pass
            else:
                monthly_users_dict[i] = None
            if i in approved_users_dict:
                pass
            else:
                approved_users_dict[i] = None
            if i in onhold_users_dict:
                pass
            else:
                onhold_users_dict[i] = None
            if i in completed_users_dict:
                pass
            else:
                completed_users_dict[i] = None
            if i in cancelled_users_dict:
                pass
            else:
                cancelled_users_dict[i] = None
            if i in submitted_users_dict:
                pass
            else:
                submitted_users_dict[i] = None

        for k in monthly_users_dict.keys():
            Main_Dict[k] = list(Main_Dict[k] for Main_Dict in All_Dicts)

        ctx['MainData'] = Main_Dict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class ApprovedUserDetails(generic.ListView):
    template_name = "admin/UserAccounts/approved_user_details.html"
    title = 'Approved User Details'

    def get(self, request, on_date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        date_list = on_date.split('-')
        y = date_list[0]

        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}-{}'.format(y, m, d)
        if len(date_list[2]) == 2:
            d = date_list[2]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)

        approved_users = LoanApplication.objects.filter(loan_start_date__startswith=new_date).filter \
            (status='APPROVED')
        ctx['users'] = approved_users
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class OnholdUserDetails(generic.ListView):
    template_name = "admin/UserAccounts/onhold_user_details.html"
    title = 'Onhold User Details'

    def get(self, request, on_date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        date_list = on_date.split('-')
        y = date_list[0]

        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}-{}'.format(y, m, d)
        if len(date_list[2]) == 2:
            d = date_list[2]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)

        onhold_users = LoanApplication.objects.filter(loan_start_date__startswith=new_date).filter \
            (status='ON_HOLD')
        ctx['users'] = onhold_users
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class CompletedUserDetails(generic.ListView):
    template_name = "admin/UserAccounts/completed_user_details.html"
    title = 'Completed User Details'

    def get(self, request, on_date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        date_list = on_date.split('-')
        y = date_list[0]

        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}-{}'.format(y, m, d)
        if len(date_list[2]) == 2:
            d = date_list[2]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)

        completed_users = LoanApplication.objects.filter(loan_start_date__startswith=new_date).filter \
            (status='COMPLETED')
        ctx['users'] = completed_users
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class CancelledUserDetails(generic.ListView):
    template_name = "admin/UserAccounts/completed_user_details.html"
    title = 'Completed User Details'

    def get(self, request, on_date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        date_list = on_date.split('-')
        y = date_list[0]

        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}-{}'.format(y, m, d)
        if len(date_list[2]) == 2:
            d = date_list[2]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)

        cancelled_users = LoanApplication.objects.filter(loan_start_date__startswith=new_date).filter \
            (status='CANCELLED')
        ctx['users'] = cancelled_users
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class SubmittedUserDetails(generic.ListView):
    template_name = "admin/UserAccounts/submitted_user_details.html"
    title = 'Submitted User Details'

    def get(self, request, on_date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        date_list = on_date.split('-')
        y = date_list[0]

        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}-{}'.format(y, m, d)
        if len(date_list[2]) == 2:
            d = date_list[2]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)

        cancelled_users = LoanApplication.objects.filter(loan_start_date__startswith=new_date).filter \
            (status='SUBMITTED')
        ctx['users'] = cancelled_users
        return render(request, self.template_name, ctx)


def upload_csv(request):
    """
        Reference https://www.pythoncircle.com/post/30/how-to-upload-and-process-the-csv-file-in-django/
    :param request:
    :return:
    """
    data = {}
    if "GET" == request.method:
        return render(request, "admin/UserAccounts/process_csv_file.html", data)
    # if not GET, then proceed
    try:
        csv_file = request.FILES["csv_file"]
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'File is not CSV type')
            return HttpResponseRedirect(reverse("upload_csv"))
        # if file is too large, return
        if csv_file.multiple_chunks():
            messages.error(request, "Uploaded file is too big (%.2f MB)." % (csv_file.size / (1000 * 10000),))
            return HttpResponseRedirect(reverse("upload_csv"))

        file_data = csv_file.read().decode("utf-8")
        lines = file_data.split("\n")
        payment_data_dict = {}
        # loop over the lines and save them in db. If error , store as string and then display
        for line in lines:
            fields = line.split(",")
            data_dict = {}
            if fields and fields[0] != 'Transaction_ID':
                # Loan id 	Number	Ref no 	Payment	Amount
                data_dict["trans_id"] = str(fields[0]).replace("'", "")
                data_dict["oid"] = str(fields[1]).replace("'", "")
                data_dict["date"] = str(fields[3]).replace("'", "")
                data_dict["status"] = str(fields[6]).replace("'", "")
                data_dict["amount"] = str(fields[13]).replace("'", "")
                data_dict["mode"] = str(fields[22]).replace("'", "")

                category = "Subscription"
                mode = 'mode'
                type = 'App'
                status = ''
                response = None

                order_id = data_dict["oid"]
                if not order_id:
                    raise ValueError("ref_no is Mandatory")
                transaction_id = data_dict["trans_id"]
                if not transaction_id:
                    raise ValueError("loan id is Mandatory")
                amount = data_dict["amount"]
                if not amount:
                    raise ValueError("amount is Mandatory")
                date = data_dict["date"]
                if not date:
                    raise ValueError("date is Mandatory")
                status = data_dict["status"]
                if not status:
                    raise ValueError("status is Mandatory")
                mode = data_dict["mode"]
                if not mode:
                    raise ValueError("mode is Mandatory")

                date_time_obj = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
                expiry_date = date_time_obj + timedelta(89)
                # get_pay = Payments.objects.filter(order_id=order_id,
                #                                   status__in=['initiated', 'pending', 'PENDING', 'failed']).update(
                #     status=str(status).lower(),
                #     transaction_id=transaction_id, amount=amount,
                #     category='Subscription',
                #     mode=mode,
                #     type='App',
                #
                #     date=date
                # )
                get_pay = Payments.objects.filter(order_id=order_id,
                                                  status__in=['initiated', 'pending', 'PENDING', 'failed'])
                if get_pay:
                    get_pay[0].status = str(status).lower()
                    get_pay[0].transaction_id = transaction_id
                    get_pay[0].amount = amount
                    get_pay[0].category = 'Subscription'
                    get_pay[0].mode = mode
                    get_pay[0].type = 'App'
                    get_pay[0].date = date
                    get_pay[0].save()

                    payment = Payments.objects.filter(order_id=order_id).last()
                    # sub = Subscription.objects.filter(payment_id=payment.id).update(start_date=date_time_obj,
                    #                                                                 expiry_date=expiry_date,
                    #                                                                 payment_status=str(status).lower())
                    sub = Subscription.objects.filter(payment_id=payment.id)
                    if sub:
                        sub[0].start_date = date_time_obj
                        sub[0].expiry_date = expiry_date
                        sub[0].payment_status = str(status).lower()
                        sub[0].save()



    except Exception as e:
        logging.getLogger("error_logger").error("Unable to upload file. " + repr(e))
        messages.error(request, "Unable to upload file. " + repr(e))

    return HttpResponseRedirect(reverse("upload_csv"))


def snd_exc(args, exc_type, file_name, line_no):
    try:
        sms_message = "Exception:" + args + exc_type + file_name + line_no
        number = "7875583679"
        sms_kwrgs = {
            'sms_message': sms_message,
            'number': number
        }
        sms(**sms_kwrgs)
    except Exception as e:
        print('-----------in exception- snd_exc---------')
        print(e.args)
        import os
        import sys
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print("exc data", exc_type, fname, exc_tb.tb_lineno)
        pass


def upload_csv_loan(request):
    """
        Reference https://www.pythoncircle.com/post/30/how-to-upload-and-process-the-csv-file-in-django/
    :param request:
    :return:
    """
    data = {}
    if "GET" == request.method:
        return render(request, "admin/UserAccounts/process_csv_file.html", data)
    # if not GET, then proceed
    try:
        csv_file = request.FILES["csv_file"]
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'File is not CSV type')
            return HttpResponseRedirect(reverse("upload_csv"))
        # if file is too large, return
        if csv_file.multiple_chunks():
            messages.error(request, "Uploaded file is too big (%.2f MB)." % (csv_file.size / (1000 * 10000),))
            return HttpResponseRedirect(reverse("upload_csv"))

        file_data = csv_file.read().decode("utf-8")
        lines = file_data.split("\n")
        payment_data_dict = {}
        # loop over the lines and save them in db. If error , store as string and then display
        for line in lines:
            fields = line.split(",")
            data_dict = {}
            if fields and fields[0] != 'Transaction_ID':
                # Loan id 	Number	Ref no 	Payment	Amount
                data_dict["trans_id"] = str(fields[0]).replace("'", "")
                data_dict["oid"] = str(fields[1]).replace("'", "")
                data_dict["date"] = str(fields[3]).replace("'", "")
                data_dict["status"] = str(fields[6]).replace("'", "")
                data_dict["amount"] = str(fields[13]).replace("'", "")
                data_dict["mode"] = str(fields[22]).replace("'", "")

                category = "Subscription"
                mode = 'mode'
                type = 'App'
                status = ''
                response = None

                order_id = data_dict["oid"]
                if not order_id:
                    raise ValueError("ref_no is Mandatory")
                transaction_id = data_dict["trans_id"]
                if not transaction_id:
                    raise ValueError("loan id is Mandatory")
                amount = data_dict["amount"]
                if not amount:
                    raise ValueError("amount is Mandatory")
                date = data_dict["date"]
                if not date:
                    raise ValueError("date is Mandatory")
                status = data_dict["status"]
                if not status:
                    raise ValueError("status is Mandatory")
                mode = data_dict["mode"]
                if not mode:
                    raise ValueError("mode is Mandatory")

                date_time_obj = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
                expiry_date = date_time_obj + timedelta(89)
                # get_pay = Payments.objects.filter(order_id=order_id,
                #                                   status__in=['initiated', 'pending', 'PENDING', 'failed']).update(
                #     status=str(status).lower(),
                #     transaction_id=transaction_id, amount=amount,
                #     category='Subscription',
                #     mode=mode,
                #     type='App',
                #
                #     date=date
                # )
                get_pay = Payments.objects.filter(order_id=order_id,
                                                  status__in=['initiated', 'pending', 'PENDING', 'failed'])
                if get_pay:
                    get_pay[0].status = str(status).lower()
                    get_pay[0].transaction_id = transaction_id
                    get_pay[0].amount = amount
                    get_pay[0].category = 'Subscription'
                    get_pay[0].mode = mode
                    get_pay[0].type = 'App'
                    get_pay[0].date = date
                    get_pay[0].save()

                    payment = Payments.objects.filter(order_id=order_id).last()
                    # sub = Subscription.objects.filter(payment_id=payment.id).update(start_date=date_time_obj,
                    #                                                                 expiry_date=expiry_date,
                    #                                                                 payment_status=str(status).lower())
                    sub = Subscription.objects.filter(payment_id=payment.id)
                    if sub:
                        sub[0].start_date = date_time_obj
                        sub[0].expiry_date = expiry_date
                        sub[0].payment_status = str(status).lower()
                        sub[0].save()




    except Exception as e:
        logging.getLogger("error_logger").error("Unable to upload file. " + repr(e))
        messages.error(request, "Unable to upload file. " + repr(e))

    return HttpResponseRedirect(reverse("upload_csv"))


def upload_csv_delete(request):
    """
        Reference https://www.pythoncircle.com/post/30/how-to-upload-and-process-the-csv-file-in-django/
    :param request:
    :return:
    """
    data = {}
    if "GET" == request.method:
        return render(request, "admin/UserAccounts/process_csv_file.html", data)
    # if not GET, then proceed
    try:
        csv_file = request.FILES["csv_file"]
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'File is not CSV type')
            return HttpResponseRedirect(reverse("upload_csv"))
        # if file is too large, return
        if csv_file.multiple_chunks():
            messages.error(request, "Uploaded file is too big (%.2f MB)." % (csv_file.size / (1000 * 10000),))
            return HttpResponseRedirect(reverse("upload_csv"))

        file_data = csv_file.read().decode("utf-8")
        lines = file_data.split("\n")
        payment_data_dict = {}
        # loop over the lines and save them in db. If error , store as string and then display
        for line in lines:
            fields = line.split(",")
            data_dict = {}
            if fields and fields[0] != 'Transaction_ID':
                # Loan id 	Number	Ref no 	Payment	Amount
                data_dict["trans_id"] = str(fields[0]).replace("'", "")
                data_dict["oid"] = str(fields[1]).replace("'", "")
                data_dict["date"] = str(fields[3]).replace("'", "")
                data_dict["status"] = str(fields[6]).replace("'", "")
                data_dict["amount"] = str(fields[13]).replace("'", "")
                data_dict["mode"] = str(fields[22]).replace("'", "")

                category = "Subscription"
                mode = 'mode'
                type = 'App'
                status = ''
                response = None

                order_id = data_dict["oid"]
                if not order_id:
                    raise ValueError("ref_no is Mandatory")
                transaction_id = data_dict["trans_id"]
                if not transaction_id:
                    raise ValueError("loan id is Mandatory")
                amount = data_dict["amount"]
                if not amount:
                    raise ValueError("amount is Mandatory")
                date = data_dict["date"]
                if not date:
                    raise ValueError("date is Mandatory")
                status = data_dict["status"]
                if not status:
                    raise ValueError("status is Mandatory")
                mode = data_dict["mode"]
                if not mode:
                    raise ValueError("mode is Mandatory")

                date_time_obj = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
                expiry_date = date_time_obj + timedelta(89)
                get_pay = Payments.objects.filter(order_id=order_id).count()
                if get_pay >= 2:
                    count = 0
                    pay = Payments.objects.filter(order_id=order_id)
                    for p in pay:
                        Subscription.objects.filter(payment_id_id=p.id).delete()
                        Payments.objects.filter(id=p.id).delete()
                        if count == (get_pay - 1):
                            break

    except Exception as e:
        logging.getLogger("error_logger").error("Unable to upload file. " + repr(e))
        messages.error(request, "Unable to upload file. " + repr(e))

    return HttpResponseRedirect(reverse("upload_csv"))


def upload_csv_repayment(request):
    """
        Reference https://www.pythoncircle.com/post/30/how-to-upload-and-process-the-csv-file-in-django/
    :param request:
    :return:
    """
    data = {}
    if "GET" == request.method:
        return render(request, "admin/UserAccounts/process_csv_file_re.html", data)
    # if not GET, then proceed
    try:
        csv_file = request.FILES["csv_file"]
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'File is not CSV type')
            return HttpResponseRedirect(reverse("upload_csv_repayment"))
        # if file is too large, return
        if csv_file.multiple_chunks():
            messages.error(request, "Uploaded file is too big (%.2f MB)." % (csv_file.size / (1000 * 10000),))
            return HttpResponseRedirect(reverse("upload_csv_repayment"))

        file_data = csv_file.read().decode("utf-8")
        lines = file_data.split("\n")
        payment_data_dict = {}
        # loop over the lines and save them in db. If error , store as string and then display
        for line in lines:
            fields = line.split(",")
            data_dict = {}
            if fields and fields[0] != 'Transaction_ID':
                # Loan id 	Number	Ref no 	Payment	Amount
                data_dict["trans_id"] = str(fields[0]).replace("'", "")
                data_dict["oid"] = str(fields[1]).replace("'", "")
                data_dict["date"] = str(fields[3]).replace("'", "")
                data_dict["status"] = str(fields[6]).replace("'", "")
                data_dict["amount"] = str(fields[13]).replace("'", "")
                data_dict["mode"] = str(fields[22]).replace("'", "")

                order_id = data_dict["oid"]
                if not order_id:
                    raise ValueError("ref_no is Mandatory")
                transaction_id = data_dict["trans_id"]
                if not transaction_id:
                    raise ValueError("loan id is Mandatory")
                amount = data_dict["amount"]
                if not amount and amount != None:
                    raise ValueError("amount is Mandatory")

                date = data_dict["date"]
                if not date:
                    raise ValueError("date is Mandatory")
                status = data_dict["status"]
                if not status:
                    raise ValueError("status is Mandatory")
                mode = data_dict["mode"]
                if not mode:
                    raise ValueError("mode is Mandatory")

                date_time_obj = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
                expiry_date = date_time_obj + timedelta(89)
                # Payments.objects.filter(order_id=order_id).update(status=str(status).lower(),
                #                                                   transaction_id=transaction_id,
                #                                                   amount=float(amount),
                #                                                   category='Loan',
                #                                                   mode=mode,
                #                                                   type='App',
                #                                                   date=date
                #                                                   )
                pay_obj = Payments.objects.filter(order_id=order_id)
                if pay_obj:
                    pay_obj[0].status = str(status).lower()
                    pay_obj[0].transaction_id = transaction_id
                    pay_obj[0].amount = float(amount)
                    pay_obj[0].category = 'Loan'
                    pay_obj[0].mode = mode
                    pay_obj[0].type = 'App'
                    pay_obj[0].date = date
                    pay_obj[0].save()

                ord_id = str(order_id).split("m")
                loan_application_id = ''
                if ord_id:
                    loan_application_id = ord_id[0]
                loan_txn_instance = LoanApplication.objects.filter(loan_application_id=loan_application_id,
                                                                   status="APPROVED")

                if loan_txn_instance:
                    if str(status).lower() == 'success':
                        rep_amount = loan_txn_instance[0].repayment_amount.filter(
                            status='success').aggregate(Sum('amount'))
                        # Validating
                        if rep_amount:
                            if float(rep_amount['amount__sum']) >= float(loan_txn_instance[0].loan_scheme_id):
                                # complete_loan = LoanApplication.objects.filter(
                                #     loan_application_id=loan_application_id
                                # ).update(status='COMPLETED')

                                complete_loan = LoanApplication.objects.filter(
                                    loan_application_id=loan_application_id
                                )
                                if complete_loan:
                                    complete_loan[0].status = 'COMPLETED'
                                    complete_loan[0].save()

                                recovery = Recovery.objects.filter(loan_transaction_id=loan_txn_instance[0].id)
                                if recovery:
                                    # Recovery.objects.filter(loan_transaction_id=loan_txn_instance[0].id).update(
                                    #     completed_date=date, recovery_status='Yes')
                                    recovery[0].completed_date = date
                                    recovery[0].recovery_status = 'Yes'
                                    recovery[0].save()

    except Exception as e:
        logging.getLogger("error_logger").error("Unable to upload file. " + repr(e))
        messages.error(request, "Unable to upload file. " + repr(e))

    return HttpResponseRedirect(reverse("upload_csv_repayment"))


def upload_csv_bank(request):
    """
        Reference https://www.pythoncircle.com/post/30/how-to-upload-and-process-the-csv-file-in-django/
    :param request:
    :return:
    """
    data = {}
    bank_upd_data = {}
    response1 = HttpResponse(content_type='text/csv')
    response1['Content-Disposition'] = 'attachment; filename="bank_upd_status.csv"'
    writer = csv.writer(response1)
    writer.writerow(
        ['Loan Id', 'Number', 'Ref No', 'Payment', 'Date', 'Payment Added', 'Completed',
         'Full payment', 'Recovery update', 'Exception'])

    if "GET" == request.method:
        return render(request, "admin/UserAccounts/process_csv_file.html", data)
    # if not GET, then proceed
    try:
        fields_verified = True
        message = ''
        csv_file = request.FILES["csv_file"]
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'File is not CSV type')
            return HttpResponseRedirect(reverse("upload_csv"))
        # if file is too large, return
        if csv_file.multiple_chunks():
            messages.error(request, "Uploaded file is too big (%.2f MB)." % (csv_file.size / (1000 * 10000),))
            return HttpResponseRedirect(reverse("upload_csv"))

        file_data = csv_file.read().decode("utf-8")
        lines = file_data.split("\n")
        payment_data_dict = {}
        # loop over the lines and save them in db. If error , store as string and then display
        if lines:
            for line in lines:
                fields_verified = True
                if line:
                    fields = line.split(",")
                    if fields:
                        data_dict = {}
                        if fields and fields[0] != 'Loan id ':
                            # Loan id 	Number	Ref no 	Payment	Amount
                            data_dict["lid"] = fields[0]
                            data_dict["number"] = fields[1]
                            data_dict["ref_no"] = fields[2]
                            data_dict["amount"] = fields[3]
                            data_dict["date"] = fields[4]

                            category = "Loan"
                            type = "Account"
                            mode = 'NB'
                            status = 'success'
                            response = None
                            order_id = data_dict["ref_no"]
                            if not order_id:
                                fields_verified = False
                                message = 'Ref_id is invalid or None'
                                # raise ValueError("ref_no is Mandatory")
                            loan_id = data_dict["lid"]
                            if not loan_id:
                                fields_verified = False
                                message = 'loan_id is invalid or None'
                                # raise ValueError("loan_id is Mandatory")
                            transaction_id = data_dict["lid"]
                            if not transaction_id:
                                fields_verified = False
                                message = 'transaction_id is invalid or None'
                                # raise ValueError("loan id is Mandatory")
                            amount = data_dict["amount"]
                            if not amount:
                                fields_verified = False
                                message = 'amount is invalid or None'
                                # raise ValueError("amount is Mandatory")
                            date = data_dict["date"]
                            if not date:
                                fields_verified = False
                                message = 'date is invalid or None'
                                # raise ValueError("date is Mandatory")
                            # message = ''
                            date = datetime.datetime.strptime(date, '%d/%m/%Y').strftime('%Y-%m-%d %H:%M:%S.%f')
                            pay_data_dict = {
                                "order_id": order_id,
                                "loan_id": loan_id,
                                "transaction_id": transaction_id,
                                "status": status,
                                "amount": amount,
                                "type": type,
                                "category": category,
                                "mode": mode,
                                "response": response,
                                "date": date,
                                "update_date": datetime.datetime.now(),
                            }
                            if fields_verified == False:
                                writer.writerow(
                                    [pay_data_dict["loan_id"], data_dict["number"], pay_data_dict["order_id"],
                                     pay_data_dict["amount"],
                                     pay_data_dict['date'], '', '', '', '', message])

                            if fields_verified == True:
                                try:
                                    bank_upd_data = process_payment_data(pay_data_dict)
                                except Exception as e:
                                    print('-----------in exception----------')
                                    print(e.args)
                                    import os
                                    import sys
                                    exc_type, exc_obj, exc_tb = sys.exc_info()
                                    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                                    print(exc_type, fname, exc_tb.tb_lineno)
                                    writer.writerow(
                                        [bank_upd_data['Loan_Id'], data_dict["number"], bank_upd_data['order_id'],
                                         bank_upd_data['amount'],
                                         bank_upd_data['date'], bank_upd_data['payment_added'],
                                         bank_upd_data['loan_completed'],
                                         bank_upd_data['full_payment'], bank_upd_data['recovery_update'],
                                         bank_upd_data['exception']])

                                writer.writerow(
                                    [bank_upd_data['Loan_Id'], data_dict["number"], bank_upd_data['order_id'],
                                     bank_upd_data['amount'],
                                     bank_upd_data['date'], bank_upd_data['payment_added'],
                                     bank_upd_data['loan_completed'],
                                     bank_upd_data['full_payment'], bank_upd_data['recovery_update'],
                                     bank_upd_data['exception']])
    except Exception as e:
        logging.getLogger("error_logger").error("Unable to upload file. " + repr(e))
        messages.error(request, "Unable to upload file. " + repr(e))
        messages.error(request, 'File is not CSV type')

    return response1
    # return HttpResponseRedirect(reverse("upload_csv"))


def process_payment_data(pay_data_dict):
    o_id = None
    loan_id = None
    transaction_id = None
    status = None
    amount = None
    type = 'App'
    category = 'Loan'
    mode = None
    response = None

    date = None
    # Status
    bank_upd_data = {}
    loan_completed = False
    full_payment = False
    payment_added = False
    recovery_update = False

    loan_id = pay_data_dict['loan_id']
    order_id = pay_data_dict['order_id']
    pay_data_dict = {
        "order_id": pay_data_dict['order_id'],
        "transaction_id": pay_data_dict['transaction_id'],
        "status": pay_data_dict['status'],
        "amount": pay_data_dict['amount'],
        "type": pay_data_dict['type'],
        "category": pay_data_dict['category'],
        "mode": pay_data_dict['mode'],
        "response": pay_data_dict['response'],
        "date": pay_data_dict['date'],
        "update_date": datetime.datetime.now()
    }

    loan_txn_instance = LoanApplication.objects.filter(loan_application_id=loan_id,
                                                       status__in=["APPROVED", "COMPLETED"])
    if loan_txn_instance:
        recovery_status = 'Yes'
        get_pay_obj = Payments.objects.filter(order_id=order_id)
        if get_pay_obj:
            payment_data = Payments.objects.filter(order_id=order_id).order_by('-id').first()
            update_pay_data = Payments.objects.filter(id=payment_data.id).update(**pay_data_dict)
            if update_pay_data == 1:
                payment_added = True
        else:
            update_pay_data = Payments.objects.create(**pay_data_dict)

        # Adding Payment object to loan
        add_pay_obj = loan_txn_instance[0].repayment_amount.add(update_pay_data)

        rep_amount = loan_txn_instance[0].repayment_amount.filter(
            status='success').aggregate(Sum('amount'))['amount__sum']

        # Validating
        if rep_amount is None:
            rep_amount = 0
        if rep_amount is not None:
            if float(rep_amount) >= float(loan_txn_instance[0].loan_scheme_id):
                full_payment = True
                dpd = loan_application_dpd(loan_txn_instance[0].user_id)
                # complete_loan = LoanApplication.objects.filter(
                #     loan_application_id=loan_id).update(status='COMPLETED', updated_date=datetime.datetime.now(),
                #                                         dpd=dpd)
                complete_loan = LoanApplication.objects.filter(
                    loan_application_id=loan_id)
                if complete_loan:
                    complete_loan[0].status = 'COMPLETED'
                    complete_loan[0].updated_date = datetime.datetime.now()
                    complete_loan[0].dpd = dpd
                    complete_loan[0].save()

                if complete_loan == 1:
                    loan_completed = True
                recovery = Recovery.objects.filter(loan_transaction_id=loan_txn_instance[0].id)
                if recovery:
                    # rec_upd = Recovery.objects.filter(loan_transaction_id=loan_txn_instance[0].id).update(
                    #     completed_date=pay_data_dict['date'], recovery_status=recovery_status)
                    rec_upd = Recovery.objects.filter(loan_transaction_id=loan_txn_instance[0].id)
                    if rec_upd:
                        rec_upd[0].completed_date = pay_data_dict['date']
                        rec_upd[0].recovery_status = recovery_status
                        rec_upd[0].save()

                    if rec_upd == 1:
                        recovery_update = True

            bank_upd_data = {'Loan_Id': loan_id,
                             'order_id': pay_data_dict['order_id'], 'amount': pay_data_dict['amount'],
                             'date': pay_data_dict['date'], 'payment_added': payment_added,
                             'loan_completed': loan_completed, 'full_payment': full_payment,
                             'recovery_update': recovery_update, 'exception': "No"}
            return bank_upd_data

        bank_upd_data = {'Loan_Id': loan_id,
                         'order_id': pay_data_dict['order_id'], 'amount': pay_data_dict['amount'],
                         'date': pay_data_dict['date'], 'payment_added': payment_added,
                         'loan_completed': loan_completed, 'full_payment': full_payment,
                         'recovery_update': recovery_update, 'exception': "No repayment amount"}
        return bank_upd_data

    bank_upd_data = {'Loan_Id': loan_id,
                     'order_id': pay_data_dict['order_id'], 'amount': pay_data_dict['amount'],
                     'date': pay_data_dict['date'], 'payment_added': payment_added,
                     'loan_completed': loan_completed, 'full_payment': full_payment,
                     'recovery_update': recovery_update, 'exception': "No Loan id found"}

    return bank_upd_data


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class SelectStatus(generic.ListView):
    def get(self, request, *args, **kwargs):
        template_name = 'admin/UserAccounts/select_status.html'
        return render(request, template_name)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class StatusChange(generic.ListView):
    def post(self, request, *args, **kwargs):
        # Create the HttpResponse object with the appropriate CSV header.
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="Loan_status_change.csv"'
        writer = csv.writer(response)
        writer.writerow(['Name', 'Mobile number', 'LoanApplicationId', 'Loan start Date', 'Loan Amount'])
        start_date = None
        end_date = None
        status = None
        onhold_days = 0
        Reason = None
        if request.method == 'POST':
            start_date = request.POST['start_date']
            end_date = request.POST['end_date']
            status = request.POST['status']
            s_status = request.POST['s_status']
            onhold_days = request.POST['onhold_time']
            Reason = request.POST['Reason']
            if not start_date:
                messages.error(request, 'start date is not valid')
                return HttpResponseRedirect(reverse("select_status"))
            if not end_date:
                messages.error(request, 'end date is not valid')
                return HttpResponseRedirect(reverse("select_status"))
            if not Reason:
                messages.error(request, 'Please select Reason')
                return HttpResponseRedirect(reverse("select_status"))
            if not s_status:
                messages.error(request, 'Please select status')
                return HttpResponseRedirect(reverse("select_status"))
            if not status:
                messages.error(request, 'Please select update status')
                return HttpResponseRedirect(reverse("select_status"))
            if status == "ON_HOLD" and onhold_days == "0":
                messages.error(request, 'Please select On_hold days')
                return HttpResponseRedirect(reverse("select_status"))
            if not Reason or Reason == "None":
                messages.error(request, 'Please select Reason')
                return HttpResponseRedirect(reverse("select_status"))
            if status != "ON_HOLD":
                onhold_days = 0
            s = datetime.datetime.strptime(start_date, "%Y-%m-%d").date()
            e = datetime.datetime.strptime(end_date, "%Y-%m-%d").date() + timedelta(days=1)
            loans = LoanApplication.objects.filter(loan_start_date__range=(s, e), status=s_status)

            if loans:
                # LoanApplication.objects.filter(loan_start_date__range=(s, e), status=s_status).update(status=status,
                #                                                                                       hold_days=onhold_days,
                #                                                                                       reason=Reason)
                loan_obj = LoanApplication.objects.filter(loan_start_date__range=(s, e), status=s_status)
                if loan_obj:
                    loan_obj[0].status = status
                    loan_obj[0].hold_days = onhold_days
                    loan_obj[0].reason = Reason
                    loan_obj[0].save()

                for loan in loans:
                    writer.writerow([loan.user_personal.name,
                                     loan.user.username,
                                     loan.loan_application_id,
                                     loan.loan_start_date,
                                     loan.loan_scheme_id])
                return response
            else:
                messages.error(request, 'No loan applcations found')
                return HttpResponseRedirect(reverse("select_status"))


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class sub_and_reg_datepicker(generic.ListView):
    '''
    Function to get datewise  sub and reg in csv file
​
    '''

    template_name = "admin/UserAccounts/sub_reg_csvreport.html"
    title = 'sub_reg_report'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, )


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class sub_and_reg_csvreport(generic.ListView):
    '''
    Function to get datewise  sub and reg in csv file
​
    '''
    template_name = "admin/UserAccounts/sub_reg_csvreport.html"

    def post(self, request, *args, **kwargs):
        ctx = {}
        today = datetime.datetime.now()

        if request.method == "POST":
            start_date = request.POST['start_date']
            end_date = request.POST['end_date']
            status = request.POST['id_status']
            start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d").date()
            end_date1 = datetime.datetime.strptime(end_date, "%Y-%m-%d").date()
            end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d").date() + timedelta(days=1)

            if status == 'Registered':
                response = HttpResponse(content_type='text/csv')
                Base_string = 'attachment; filename="'
                date_string = str(today)[:10]

                final_str = Base_string + date_string + "Registered between date " + str(start_date) \
                            + 'to' + str(end_date1) + '.csv"'
                response['Content-Disposition'] = final_str

                writer = csv.writer(response)
                writer.writerow(['date', 'Mobile Number', 'First Name'])

                user_reg = User.objects.filter(date_joined__range=(start_date, end_date))
                if user_reg:
                    for user in user_reg:
                        name = Personal.objects.filter(user=user.id)
                        if name:
                            writer.writerow([user.date_joined, user.username, name[0].name])
                        else:
                            writer.writerow([user.date_joined, user.username, ''])

                return response

            if status == 'Subscription':
                response = HttpResponse(content_type='text/csv')
                Base_string = 'attachment; filename="'
                date_string = str(today)[:10]
                final_str = Base_string + date_string + "Subscribed between date " + str(start_date) \
                            + 'to' + str(end_date1) + '.csv"'
                response['Content-Disposition'] = final_str

                writer = csv.writer(response)
                writer.writerow(
                    ['date', 'Mobile Number', 'Subscription status', 'order_id', 'transaction_id',
                     'pg_transaction_id', 'Payment status', 'category',
                     'mode', 'type', 'amount', 'response'])

                user_sub = Subscription.objects.filter(start_date__range=(start_date, end_date))
                for sub in user_sub:
                    writer.writerow([sub.start_date, sub.user.username, sub.payment_status, sub.payment_id.order_id,
                                     sub.payment_id.transaction_id,
                                     sub.payment_id.pg_transaction_id, sub.payment_id.status, sub.payment_id.category,
                                     sub.payment_id.mode,
                                     sub.payment_id.type, sub.payment_id.amount, sub.payment_id.response])

                return response


def getlastamount(user_id):
    # user_id = 97389, 82192, 82190
    data = SmsStringMaster.objects.filter(user_id=user_id).order_by('-date_time')
    if data:
        for sms in data:
            response = getAvailableBalance(sms)
            if isinstance(response, float):
                if response == 0.0:
                    pass
                else:
                    pass

            elif isinstance(response, str):
                if response == 0.0:
                    pass
                else:
                    data = response.split(".")
                    if len(data) > 1 and data[1] != '':
                        return response
    return 0.0


def getAvailableBalance(sms):
    sms = str(sms)
    exp = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
    exp1 = '(?i)(avl\.?\s*|avbl\.?\s*|available|Bal|Balance)'
    amount_exp = '-*[,\d]+\.?\d{0,2}'
    opt = re.compile(exp, re.VERBOSE)

    # finds all currency amount in message
    if re.findall(exp, sms):
        details = opt.findall(sms)

    # finds currency amount for available balance
    # if re.search(exp1, sms.lower()):
    if re.search(exp1, sms):
        match = re.search(exp1, sms)
        str_obj = sms.split(match.group())
        if re.search(exp, str_obj[1]):
            amount = re.search(exp, str_obj[1]).group()
            amt = re.search(amount_exp, amount)
            if amt:
                amt = amt.group()
                if amt.__contains__(','):
                    amt = amt.replace(',', '')
                return amt
    return 0.0


def differentdates(request):
    loans_list = []
    loans = LoanApplication.objects.filter(status__in=['APPROVED', 'COMPLETED'])
    if loans:
        for loan in loans:
            rbl = RBLTransaction.objects.filter(payment=loan.disbursed_amount)
            if rbl:
                transaction_date = rbl[0].txn_time
                if transaction_date and transaction_date is not None:
                    if str(transaction_date)[:10] != str(loan.loan_start_date)[:10]:
                        tenure = int(float(loan.loan_scheme_id))
                        days = scheme[str(tenure)]['ms_tenure']
                        loan_start_date = transaction_date
                        loan_end_date = transaction_date + timedelta(days=int(days))
                        update_loan = LoanApplication.objects.filter(loan_application_id=loan.loan_application_id)
                        # new = update_loan.update(loan_start_date=loan_start_date, loan_end_date=loan_end_date)
                        update_loan[0].loan_start_date = loan_start_date
                        update_loan[0].loan_end_date = loan_end_date
                        loans_list.append(loan.loan_application_id)
    return HttpResponse('OK')


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class ExtraPayment(generic.ListView):
    template_name = "admin/UserAccounts/extra_payment.html"
    title = ' Extra Payment '

    def get(self, request, *args, **kwargs):
        ctx = {}
        data_dict = {}
        loan_txn_instance = LoanApplication.objects.filter(status="APPROVED")
        if loan_txn_instance:
            for loan in loan_txn_instance:
                data_list = []
                full_payment = False
                loan_completed = False
                complete_loan = 0
                rep_amount = 0
                if loan.repayment_amount:
                    rep_amount = loan.repayment_amount.filter(
                        status__icontains='success').aggregate(Sum('amount'))['amount__sum']
                    # Validating
                    if rep_amount:
                        if float(rep_amount) > float(loan.loan_scheme_id):
                            full_payment = True
                            data_list.extend((loan.user_personal.name,
                                              loan.user.username,
                                              loan.loan_application_id,
                                              loan.loan_start_date,
                                              loan.loan_scheme_id, loan.status,
                                              rep_amount, full_payment, loan_completed))
                            data_dict.update({loan.loan_application_id: data_list})
            ctx['data'] = data_dict
            return render(request, self.template_name, ctx)


def complete_approve_loans(request):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="Loan_status_change.csv"'
    writer = csv.writer(response)
    writer.writerow(
        ['Name', 'Mobile number', 'LoanApplicationId', 'Loan start Date', 'Loan Amount', 'Status', 'Repayment amount',
         'full payment'])
    rep_amount = None
    loan_txn_instance = LoanApplication.objects.filter(status="APPROVED")
    if loan_txn_instance:
        for loan in loan_txn_instance:
            full_payment = False
            loan_completed = False
            complete_loan = 0
            rep_amount = 0
            if loan.repayment_amount:
                rep_amount = loan.repayment_amount.filter(
                    status__icontains='success').aggregate(Sum('amount'))['amount__sum']
                # Validating
                if rep_amount:
                    if float(rep_amount) > float(loan.loan_scheme_id):
                        full_payment = True

                        writer.writerow([loan.user_personal.name,
                                         loan.user.username,
                                         loan.loan_application_id,
                                         loan.loan_start_date,
                                         loan.loan_scheme_id, loan.status,
                                         rep_amount, full_payment])
    return response


def order_with_m(request):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="orders_with_m.csv"'
    writer = csv.writer(response)
    writer.writerow(
        ['Order ID', 'Name', 'Mobile number', 'LoanApplicationId', 'Loan start Date', 'Loan Amount', 'Status',
         'Repayment amount'])

    payments = Payments.objects.filter(order_id__startswith="m", status="success")
    if payments:
        for i in payments:
            loans = LoanApplication.objects.filter(repayment_amount=i)
            if loans:
                for loan in loans:
                    rep_amount = 0
                    if loan.repayment_amount:
                        rep_amount = loan.repayment_amount.filter(
                            status__icontains='success').aggregate(Sum('amount'))['amount__sum']
                        if rep_amount is None:
                            rep_amount = 0
                    writer.writerow([i.order_id, loan.user_personal.name,
                                     loan.user.username,
                                     loan.loan_application_id,
                                     loan.loan_start_date,
                                     loan.loan_scheme_id, loan.status,
                                     rep_amount])
    return response


def no_payment_linked(request):
    """
        Reference https://www.pythoncircle.com/post/30/how-to-upload-and-process-the-csv-file-in-django/
    :param request:
    :return:
    """
    data = {}
    bank_upd_data = {}
    response1 = HttpResponse(content_type='text/csv')
    response1['Content-Disposition'] = 'attachment; filename="no_payments_linked.csv"'
    writer = csv.writer(response1)
    writer.writerow(
        ['Loan Id', 'order ID', 'Full payment', 'Payment Added', 'status updated', 'status_code'])

    if "GET" == request.method:
        return render(request, "admin/UserAccounts/no_payment_linked.html", data)
    # if not GET, then proceed
    try:
        fields_verified = True
        message = ''
        csv_file = request.FILES["csv_file"]
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'File is not CSV type')
            return HttpResponseRedirect(reverse("upload_csv"))
        # if file is too large, return
        if csv_file.multiple_chunks():
            messages.error(request, "Uploaded file is too big (%.2f MB)." % (csv_file.size / (1000 * 10000),))
            return HttpResponseRedirect(reverse("upload_csv"))

        file_data = csv_file.read().decode("utf-8")
        lines = file_data.split("\n")
        pay_data_dict = {}
        # loop over the lines and save them in db. If error , store as string and then display
        if lines:
            for line in lines:
                fields_verified = True
                if line:
                    fields = line.split(",")
                    if fields:
                        data_dict = {}
                        if fields and fields[0] != 'Loan id':
                            # Loan id 	Number	Ref no 	Payment	Amount
                            data_dict["lid"] = fields[0]
                            data_dict["order_id"] = fields[1]

                            loan_id = data_dict["lid"]
                            if not loan_id:
                                fields_verified = False
                                message = 'loan_id is invalid or None'
                                # raise ValueError("loan_id is Mandatory")
                            order_id = data_dict["order_id"]
                            if not order_id:
                                fields_verified = False
                                message = 'transaction_id is invalid or None'
                                # raise ValueError("loan id is Mandatory")

                            pay_data_dict = {
                                "order_id": order_id,
                                "loan_id": loan_id,
                            }

                            payments_linked_status = payments_linked(pay_data_dict)

                            writer.writerow(
                                [payments_linked_status["Loan_Id"], payments_linked_status["order_id"],
                                 payments_linked_status['full_payment'],
                                 payments_linked_status['payment_added'],
                                 payments_linked_status['status_update'],
                                 payments_linked_status['status']])
            else:
                # End of the file
                pass
    except Exception as e:
        logging.getLogger("error_logger").error("Unable to upload file. " + repr(e))
        messages.error(request, "Unable to upload file. " + repr(e))
        # return HttpResponseRedirect(reverse("upload_csv"))

    return response1


def payments_linked(pay_data_dict):
    loan_id = pay_data_dict['loan_id']
    order_id = pay_data_dict['order_id']
    '''
    status = 1 : Payment already exists.
    status = 2 : Loan ID not found.
    '''
    full_payment = False
    payment_added = False
    status_update = False
    payment_update_data = {}
    loan_txn_instance = LoanApplication.objects.filter(loan_application_id=loan_id)
    if loan_txn_instance:
        get_pay_obj = Payments.objects.filter(order_id=order_id)
        if get_pay_obj:
            pay_obj_exist = loan_txn_instance[0].repayment_amount.filter(order_id=order_id)
            if not pay_obj_exist:
                # Adding Payment object to loan
                add_pay_obj = loan_txn_instance[0].repayment_amount.add(get_pay_obj[0])
                rep_amount = loan_txn_instance[0].repayment_amount.filter(
                    status__icontains='success').aggregate(Sum('amount'))['amount__sum']
                # check if the payment object is added
                verify_pay_obj = loan_txn_instance[0].repayment_amount.filter(order_id=order_id)
                if verify_pay_obj:
                    payment_added = True
                # Validating
                if rep_amount is not None:
                    if float(rep_amount) >= float(loan_txn_instance[0].loan_scheme_id):
                        full_payment = True
                        # loan_update = loan_txn_instance[0].update(status="COMPLETED",
                        #                                           updated_date=datetime.datetime.now())
                        loan_txn_instance[0].status = "COMPLETED"
                        loan_txn_instance[0].updated_date = datetime.datetime.now()
                        loan_txn_instance[0].save()

                        status_update = True
                        payment_update_data = {'Loan_Id': loan_id,
                                               'order_id': order_id,
                                               'full_payment': full_payment,
                                               'status_update': status_update,
                                               'payment_added': payment_added,
                                               'status': ''}
                        return payment_update_data
                    else:
                        payment_update_data = {'Loan_Id': loan_id,
                                               'order_id': order_id,
                                               'full_payment': full_payment,
                                               'status_update': status_update,
                                               'payment_added': payment_added,
                                               'status': ''}

                        return payment_update_data
            else:
                status = 1
                payment_update_data = {'Loan_Id': loan_id,
                                       'order_id': order_id,
                                       'full_payment': '', 'payment_added': payment_added,
                                       'status_update': '',
                                       'status': status}
                return payment_update_data
    else:
        status = 2
        payment_update_data = {'Loan_Id': loan_id,
                               'order_id': order_id,
                               'full_payment': '', 'payment_added': '', 'status_update': '',
                               'status': status}
        return payment_update_data


def migration(request):
    """
        Reference https://www.pythoncircle.com/post/30/how-to-upload-and-process-the-csv-file-in-django/
    :param request:
    :return:
    """
    '''
        status = 7 : Fields Not verified.
    '''
    data = {}
    migration_data = {}
    response1 = HttpResponse(content_type='text/csv')
    response1['Content-Disposition'] = 'attachment; filename="migration.csv"'
    writer = csv.writer(response1)

    writer.writerow(
        ['Loan Id', 'order ID', 'Transaction Id', 'full_payment', 'payment_added', 'payment_obj_created',
         'status_update', 'many_loans', 'status'])

    if "GET" == request.method:
        return render(request, "admin/UserAccounts/no_payment_linked.html", data)
    # if not GET, then proceed
    try:
        fields_verified = True
        message = ''
        csv_file = request.FILES["csv_file"]
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'File is not CSV type')
            return HttpResponseRedirect(reverse("upload_csv"))
        # if file is too large, return
        if csv_file.multiple_chunks():
            messages.error(request, "Uploaded file is too big (%.2f MB)." % (csv_file.size / (1000 * 10000),))
            return HttpResponseRedirect(reverse("upload_csv"))

        file_data = csv_file.read().decode("utf-8")
        lines = file_data.split("\n")
        payment_data_dict = {}
        # loop over the lines and save them in db. If error , store as string and then display
        if lines:
            for line in lines:
                fields_verified = True
                if line:
                    fields = line.split(",")
                    if fields:
                        data_dict = {
                            'mob_no': None,
                            'loan_id': None,
                            'amount': None,
                            'order_id': None,
                            'transaction_id': None,
                            'description': None,
                            'transaction_date': None,
                            'response': None,
                            'transaction_type': None,
                            'status': None,
                            'updated_date': None,
                            'tid1': None,
                            'tid2': None,
                            'tid3': None,
                        }

                        if fields and fields[0] != 'Mobile number':
                            # Loan id 	Number	Ref no 	Payment	Amount
                            data_dict["mob_no"] = fields[0]
                            data_dict["loan_id"] = fields[1]
                            data_dict["amount"] = fields[2]
                            data_dict["order_id"] = fields[7]
                            data_dict["tid1"] = fields[4]
                            data_dict["tid2"] = fields[5]
                            data_dict["tid3"] = fields[6]
                            # data_dict["transaction_id"] = fields[7]
                            data_dict["description"] = fields[8]
                            data_dict["transaction_date"] = fields[9]
                            data_dict["response"] = fields[10]
                            data_dict["transaction_type"] = fields[11]
                            data_dict["status"] = fields[12]
                            data_dict["updated_date"] = fields[13]

                            order_id = data_dict["order_id"]
                            tid1 = data_dict['tid1']
                            tid2 = data_dict['tid2']
                            tid3 = data_dict['tid3']
                            transaction_id = str(tid1) + str(tid2) + str(tid3)
                            data_dict["transaction_id"] = transaction_id
                            if order_id == "None" and transaction_id == "None":
                                status = 7
                                writer.writerow(
                                    [data_dict["loan_id"], data_dict["order_id"],
                                     data_dict["transaction_id"],
                                     '', '', '', '', '', status])

                            else:
                                return_data_dict = migrate_data(data_dict)
                                writer.writerow(
                                    [return_data_dict['loan_id'], return_data_dict["order_id"],
                                     return_data_dict['transaction_id'],
                                     return_data_dict['full_payment'],
                                     return_data_dict['payment_added'],
                                     return_data_dict['payment_obj_created'],
                                     return_data_dict['status_update'],
                                     return_data_dict['many_loans'],
                                     return_data_dict['status']])



    except Exception as e:
        logging.getLogger("error_logger").error("Unable to upload file. " + repr(e))
        messages.error(request, "Unable to upload file. " + repr(e))
        messages.error(request, 'File is not CSV type')
        # return HttpResponseRedirect(reverse("upload_csv"))
        response1

    return response1


def migrate_data(data_dict):
    mob_no = data_dict['mob_no']
    loan_id = data_dict['loan_id']
    amount = data_dict['amount']
    order_id = data_dict['order_id']
    transaction_id = data_dict['transaction_id']
    description = data_dict['description']
    transaction_date = data_dict['transaction_date']
    response = data_dict['response']
    transaction_type = data_dict['transaction_type']
    status = data_dict['status']
    updated_date = data_dict['updated_date']
    '''
    status = 1 : Payment already exists.
    status = 2 : Loan ID not found.
    status = 3 : Many loans.
    status = 4 : Full payment.
    status = 5 : Not Full payment.
    status = 6 : Payment already linked with loan.
    '''
    full_payment = False
    payment_added = False
    payment_obj_created = False
    status_update = False
    return_data_dict = {}
    many_loans_list = []
    # repayment_amount = None
    many_loans = many_loans_list
    loan_txn_instance = LoanApplication.objects.filter(loan_application_id=loan_id)
    if loan_txn_instance:
        if order_id is not None and order_id != "None":
            get_pay_obj = Payments.objects.filter(order_id=order_id)
        else:
            get_pay_obj = Payments.objects.filter(transaction_id=transaction_id)
        if get_pay_obj:
            loan = LoanPayment.objects.filter(payment_id=get_pay_obj[0].id).values_list('loanapplication_id', flat=True)
            if loan:
                loan_list = set(loan)
                for loan in loan_list:
                    if loan != loan_txn_instance[0].id:
                        status = 3
                        # This order id is linked with many loan ID's.
                        many_loans_list.append(loan)
                        return_data_dict = {'loan_id': loan_id, 'order_id': order_id, 'transaction_id': transaction_id,
                                            'full_payment': full_payment, 'payment_added': payment_added,
                                            'payment_obj_created': payment_obj_created,
                                            'status_update': status_update, 'many_loans': many_loans_list,
                                            'status': status}
                        return return_data_dict

            pay_obj_exist = loan_txn_instance[0].repayment_amount.filter(id=get_pay_obj[0].id)
            if not pay_obj_exist:
                # Adding Payment object to loan
                add_pay_obj = loan_txn_instance[0].repayment_amount.add(get_pay_obj[0])
                # check if the payment object is added
                verify_pay_obj = loan_txn_instance[0].repayment_amount.filter(id=get_pay_obj[0].id)
                if verify_pay_obj:
                    payment_added = True
                rep_amount = loan_txn_instance[0].repayment_amount.filter(
                    status__icontains='success').aggregate(Sum('amount'))['amount__sum']

                # Validating
                if rep_amount is not None:
                    if float(rep_amount) >= float(loan_txn_instance[0].loan_scheme_id):
                        status = 4
                        full_payment = True
                        # loan_update = loan_txn_instance.update(status="COMPLETED",
                        #                                        updated_date=datetime.datetime.now())
                        loan_txn_instance.status = "COMPLETED"
                        loan_txn_instance.updated_date = datetime.datetime.now()
                        loan_txn_instance.save()

                        status_update = True
                        return_data_dict = {'loan_id': loan_id, 'order_id': order_id, 'transaction_id': transaction_id,
                                            'full_payment': full_payment, 'payment_added': payment_added,
                                            'payment_obj_created': payment_obj_created,
                                            'status_update': status_update, 'many_loans': many_loans_list,
                                            'status': status}
                        return return_data_dict
                    else:
                        status = 5
                        return_data_dict = {'loan_id': loan_id, 'order_id': order_id, 'transaction_id': transaction_id,
                                            'full_payment': full_payment, 'payment_added': payment_added,
                                            'payment_obj_created': payment_obj_created,
                                            'status_update': status_update, 'many_loans': many_loans_list,
                                            'status': status}
                        return return_data_dict
            else:
                status = 6
                return_data_dict = {'loan_id': loan_id, 'order_id': order_id, 'transaction_id': transaction_id,
                                    'full_payment': full_payment, 'payment_added': payment_added,
                                    'payment_obj_created': payment_obj_created,
                                    'status_update': status_update, 'many_loans': many_loans_list,
                                    'status': status}
                return return_data_dict
        else:
            payment_obj = Payments.objects.create(amount=amount,
                                                  order_id=order_id,
                                                  transaction_id=transaction_id,
                                                  description=description,
                                                  date=transaction_date,
                                                  response=response,
                                                  type=transaction_type,
                                                  status=status,
                                                  update_date=updated_date)

            if order_id is not None:
                get_pay_obj = Payments.objects.filter(order_id=order_id)
            else:
                get_pay_obj = Payments.objects.filter(transaction_id=transaction_id)
            if get_pay_obj:
                payment_obj_created = True
                pay_obj_exist = loan_txn_instance[0].repayment_amount.filter(id=get_pay_obj[0].id)
                if not pay_obj_exist:
                    # Adding Payment object to loan
                    add_pay_obj = loan_txn_instance[0].repayment_amount.add(get_pay_obj[0])
                    # check if the payment object is added
                    verify_pay_obj = loan_txn_instance[0].repayment_amount.filter(id=get_pay_obj[0].id)
                    if verify_pay_obj:
                        payment_added = True
                    rep_amount = loan_txn_instance[0].repayment_amount.filter(
                        status__icontains='success').aggregate(Sum('amount'))['amount__sum']

                    # Validating
                    if rep_amount is not None:
                        if float(rep_amount) >= float(loan_txn_instance[0].loan_scheme_id):
                            status = 4
                            full_payment = True
                            # loan_update = loan_txn_instance.update(status="COMPLETED",
                            #                                        updated_date=datetime.datetime.now())
                            loan_txn_instance.status = "COMPLETED"
                            loan_txn_instance.updated_date = datetime.datetime.now()
                            loan_txn_instance.save()
                            status_update = True
                            return_data_dict = {'loan_id': loan_id, 'order_id': order_id,
                                                'transaction_id': transaction_id,
                                                'full_payment': full_payment, 'payment_added': payment_added,
                                                'payment_obj_created': payment_obj_created,
                                                'status_update': status_update, 'many_loans': many_loans_list,
                                                'status': status}

                            return return_data_dict
                        else:
                            status = 5
                            return_data_dict = {'loan_id': loan_id, 'order_id': order_id,
                                                'transaction_id': transaction_id,
                                                'full_payment': full_payment, 'payment_added': payment_added,
                                                'payment_obj_created': payment_obj_created,
                                                'status_update': status_update, 'many_loans': many_loans_list,
                                                'status': status}
                            return return_data_dict

    else:
        status = 2
        return_data_dict = {'loan_id': loan_id, 'order_id': order_id, 'transaction_id': transaction_id,
                            'full_payment': full_payment, 'payment_added': payment_added,
                            'payment_obj_created': payment_obj_created,
                            'status_update': status_update, 'many_loans': many_loans_list,
                            'status': status}
        return return_data_dict


def less_payment_loans(request):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="less_payment_loans.csv"'
    writer = csv.writer(response)
    writer.writerow(
        ['User id', 'Mobile number', 'Name', 'Loan Application ID', 'Loan start date',
         'Loan End Date', 'loan amount', 'total amt paid', "Loan status",
         'Payment?'])
    repayment_sum = 0.0
    payment = ''
    loans = LoanApplication.objects.filter(status__in=["COMPLETED", "APPROVED"])
    for loan in loans:
        if loan:
            rep_amount = loan.repayment_amount.filter(
                status='success').aggregate(Sum('amount'))
            # Validating
            if rep_amount:
                repayment_sum = rep_amount['amount__sum']
                if repayment_sum is None or repayment_sum == '':
                    repayment_sum = 0.0
                else:
                    repayment_sum = rep_amount['amount__sum']
                if loan:
                    if float(repayment_sum) < float(loan.loan_scheme_id):
                        payment = 'less_payment'
                        writer.writerow(
                            [loan.user_id, loan.user.username, loan.user_personal.name, loan.loan_application_id,
                             loan.loan_start_date, loan.loan_end_date,
                             loan.loan_scheme_id, repayment_sum, loan.status, payment])
                    elif float(repayment_sum) > float(loan.loan_scheme_id):
                        payment = 'extra_payment'
                        writer.writerow(
                            [loan.user_id, loan.user.username, loan.user_personal.name, loan.loan_application_id,
                             loan.loan_start_date, loan.loan_end_date,
                             loan.loan_scheme_id, repayment_sum, loan.status, payment])

    return response


def mobilesearch(request):
    return render(request, "admin/UserAccounts/loans_by_mobile_search_new.html")


@method_decorator([login_required(login_url="/recovery/login/")], name='dispatch')
class LoansByMobile(generic.ListView):
    template_name = "admin/UserAccounts/loan_details_by_mobile_new.html"
    title = ' Pancard User Loans '

    def post(self, request, *args, **kwargs):
        ctx = {}
        loans_dict = {}
        if request.method == "POST":
            mob_no = request.POST['mob_no']
            if mob_no and mob_no is not None:
                user = User.objects.filter(username=mob_no)
                if user:
                    loans = LoanApplication.objects.filter(user=user[0].id)
                    if loans:
                        for loan in loans:
                            if loan:
                                data_list = []
                                repayment_sum = 0
                                payment = ''
                                amount = 0
                                rep_amount = loan.repayment_amount.filter(
                                    status='success').aggregate(Sum('amount'))
                                # Validating
                                if rep_amount:
                                    repayment_sum = rep_amount['amount__sum']
                                    if repayment_sum is None or repayment_sum == '':
                                        repayment_sum = 0.0
                                    else:
                                        repayment_sum = rep_amount['amount__sum']
                                    if loan:
                                        if loan.status == 'APPROVED' or loan.status == 'COMPLETED':
                                            if float(repayment_sum) < float(loan.loan_scheme_id):
                                                payment = 'less_payment'
                                                amount = float(loan.loan_scheme_id) - float(repayment_sum)
                                            elif float(repayment_sum) > float(loan.loan_scheme_id):
                                                payment = 'extra_payment'
                                                amount = float(repayment_sum) - float(loan.loan_scheme_id)
                                            elif float(repayment_sum) == float(loan.loan_scheme_id):
                                                payment = 'complete_payment'
                                                amount = float(repayment_sum)
                                        else:
                                            payment = None
                                            amount = 0.0

                                data_list.extend((loan.user_personal, loan.loan_start_date, loan.loan_end_date,
                                                  loan.loan_scheme_id, repayment_sum, payment, amount, loan.status))
                                loans_dict.update({loan: data_list})

                    ctx['loans'] = loans_dict
                return render(request, self.template_name, ctx)
            else:
                return render(request, "admin/UserAccounts/loans_by_mobile_search.html")


import csv
from django.conf import settings


def random_loans():
    import csv
    from pathlib import Path
    yesterday = (datetime.datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d')
    path = settings.BASE_DIR + '/csv'
    asdf = Path(path).mkdir(parents=True, exist_ok=True)
    if asdf is None:
        import os
        try:
            ase = os.makedirs(path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
    filename = yesterday + '.csv'
    csvpath = path + '/' + filename
    with open(csvpath, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(
            ['Loan ID', 'Username', 'Loan status', 'Automation status', 'Response code'])

        loans = LoanApplication.objects.filter(created_date__startswith=yesterday).order_by(
            '?')[:20]
        if loans:
            for loan in loans:
                if loan:
                    pass
    check_mail(filename)


def check_mail(filename):
    # Python code to illustrate Sending mail with attachments
    # from your Gmail account

    # libraries to be imported
    import smtplib
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.mime.base import MIMEBase
    from email import encoders

    fromaddr = "shreyasvetal99@gmail.com"
    toaddr = "shreyash@mudrakwik.com"
    cc = 'jayvant@mudrakwik.com', 'shreyash@mudrakwik.com', 'keval.thakkar@mudrakwik.com'
    # instance of MIMEMultipart
    msg = MIMEMultipart()

    # storing the senders email address
    msg['From'] = fromaddr

    # storing the receivers email address
    msg['To'] = toaddr
    msg['Cc'] = ",".join(cc)
    # storing the subject
    msg['Subject'] = "Random loan applications"
    data = "Please find the attachment for yesterdays random loan applications."
    # string to store the body of the mail
    body = data

    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # open the file to be sent
    filename = filename
    path = '/home/mudra/PycharmProjects/prime_mudra/mudra/csv/' + filename
    if path and path is not None:
        # attachment = open("/home/test/Documents/upload_csv.csv", "rb")
        attachment = open(path, "rb")

        # instance of MIMEBase and named as p
        p = MIMEBase('application', 'octet-stream')

        # To change the payload into encoded form
        p.set_payload((attachment).read())

        # encode into base64
        encoders.encode_base64(p)

        p.add_header('Content-Disposition', "attachment; filename= %s" % filename)

        # attach the instance 'p' to instance 'msg'
        msg.attach(p)

    # creates SMTP session
    s = smtplib.SMTP('smtp.gmail.com', 587)

    # start TLS for security
    s.starttls()

    # Authentication
    s.login(fromaddr, "iojahghilvgwnlur")

    # Converts the Multipart msg into a string
    text = msg.as_string()

    # sending the mail
    toadrss = [toaddr] + list(cc)
    s.sendmail(fromaddr, toadrss, text)


# -------------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------------
@method_decorator([login_required(login_url="/login/")], name='dispatch')
class ActiveOnholdMonthwise(generic.ListView):
    '''
        Template : active_onholduser_monthwise.html
        shows active_onholduser_monthwise count
        '''
    template_name = "admin/UserAccounts/active_onholduser_monthwise.html"
    title = ' Active Onhold Monthwise '

    def get(self, request, *args, **kwargs):
        ctx = {}
        month_wise_active_count_dict = {}
        try:
            active_users = LoanApplication.objects.order_by('user_id', '-loan_start_date').filter(
                status="ON_HOLD").distinct(
                "user_id")

            today = datetime.datetime.now()
            cnt = 0
            df = pd.DataFrame(columns=['loan_start_date'])
            for user in active_users:
                x = today - user.loan_start_date

                y = user.hold_days
                if y > x.days:
                    df = df.append({"loan_start_date": str(user.loan_start_date)[:7]}, ignore_index=True)

                    cnt += 1
                else:
                    pass
            month_wise_active_count = \
                df.groupby(df['loan_start_date'])['loan_start_date'].count().sort_index(axis=0)
            month_wise_active_count_dict = dict(month_wise_active_count)
        except Exception as e:

            print('-----------in exception last loan dpd----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
        # return HttpResponse("OK")
        ctx['month_wise_active'] = month_wise_active_count_dict
        return render(request, self.template_name, ctx)


# Test Date wise count
@method_decorator([login_required(login_url="/login/")], name='dispatch')
class ActiveOnholdDatewise(generic.ListView):
    template_name = "admin/UserAccounts/active_onholduser_monthwise.html"

    def get(self, request, month, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        date_list = month.split('-')
        y = date_list[0]
        date_wise_active_count_dict = {}
        today = datetime.datetime.now()
        cnt = 0
        try:
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
                # new_date = '{}-{}-{}'.format(y, m, d)

                # new_date = '{}-{}-{}'.format(y, m, d)
            new_date = '{}-{}'.format(y, m, )
            active_users = LoanApplication.objects.order_by('user_id', '-loan_start_date').filter(
                status="ON_HOLD", loan_start_date__startswith=new_date).distinct(
                "user_id")

            df = pd.DataFrame(columns=['loan_start_date', ])
            for user in active_users:
                x = today - user.loan_start_date

                y = user.hold_days
                if y > x.days:
                    df = df.append({"loan_start_date": str(user.loan_start_date)[:10]}, ignore_index=True)

                    cnt += 1
                else:
                    pass

            date_wise_active_count = \
                df.groupby(df['loan_start_date'])['loan_start_date'].count().sort_index(axis=0)
            date_wise_active_count_dict = dict(date_wise_active_count)
        except Exception as e:

            print('-----------in exception last loan dpd----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
        ctx['date_wise_active'] = date_wise_active_count_dict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class onhold_active_user_perday(generic.ListView):
    template_name = "admin/UserAccounts/active_onholduser_monthwise.html"
    title = 'Active User'

    def get(self, request, date, *args, **kwargs):
        ctx = {}
        number_list = []
        today = datetime.datetime.now()
        new_date = ''
        m = ''
        d = ''
        date_list = date.split('-')
        y = date_list[0]
        cnt = 0
        if len(date_list[1]) > 1:
            m = date_list[1]
        else:
            m = '0' + str(date_list[1])
        if len(date_list[2]) > 1:
            d = date_list[2]
        else:
            d = '0' + str(date_list[2])
        new_date = '{}-{}-{}'.format(y, m, d)
        try:
            active_users = LoanApplication.objects.filter(status="ON_HOLD",
                                                          loan_start_date__startswith=new_date).order_by("user_id",
                                                                                                         "-loan_start_date").distinct(
                "user_id")

            if active_users and active_users is not None:
                for user in active_users:
                    x = today - user.loan_start_date

                    y = user.hold_days
                    if y > x.days:
                        number_list.append(user.user.username)
                        cnt += 1
                    else:
                        pass


        except Exception as e:

            print('-----------in exception last loan dpd----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
        ctx['number_list'] = number_list
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class pan_count_view(generic.ListView):
    template_name = "admin/UserAccounts/pan_count_numbers_new.html"
    title = ' pan card numberss '

    def get(self, request, pan, *args, **kwargs):
        ctx = {}
        number_list = []
        # pan = request.data.get("pan")
        # pan = "AUZPK8189K"
        if pan and pan is not None:
            personal_data = Personal.objects.filter(pan_number=pan)
            if personal_data:
                all_data = LoanApplication.objects.filter(user_personal__in=personal_data).distinct("user_id")
                for data in all_data:
                    number_list.append(data.user.username)
            ctx["number_list"] = set(number_list)
        return render(request, self.template_name, ctx)
        # return HttpResponse("done")


class PaymentPtmRpaycount(generic.ListView):
    '''
    Template : payments_ptm_rpay.html
    shows count of payment made by paytm and rpay
    '''
    template_name = "admin/UserAccounts/payments_ptm_rpay.html"
    title = 'Payments Count of paytm and rpay'

    def get(self, request, *args, **kwargs):
        ctx = {}

        Rpay_payments = Payments.objects.filter(pay_source="Rpay", status__icontains="Success")
        rpay_dataframe = read_frame(Rpay_payments)
        rpay_dataframe['YearMonthDay'] = pd.to_datetime(rpay_dataframe['create_date']).apply(
            lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
        monthwise_payment_dict = \
            rpay_dataframe.groupby(rpay_dataframe['YearMonthDay'])['order_id'].count().sort_index(axis=0)
        monthwise_payment_dict = dict(monthwise_payment_dict)

        paytm_payments = Payments.objects.filter(pay_source="Ptm", status__icontains="Success", category="Subscription")
        paytm_dataframe = read_frame(paytm_payments)
        paytm_dataframe['YearMonthDay'] = pd.to_datetime(paytm_dataframe['create_date']).apply(
            lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
        monthwise_paytm_dict = \
            paytm_dataframe.groupby(paytm_dataframe['YearMonthDay'])['order_id'].count().sort_index(axis=0)
        monthwise_paytm_dict = dict(monthwise_paytm_dict)

        BothDicts = [monthwise_payment_dict, monthwise_paytm_dict]
        allkey = reduce(set.union, map(set, map(dict.keys, BothDicts)))
        for i in allkey:
            if i in monthwise_payment_dict:
                pass
            else:
                monthwise_payment_dict[i] = None
            if i in monthwise_paytm_dict:
                pass
            else:
                monthwise_paytm_dict[i] = None

        MainDict = {}
        for k in monthwise_payment_dict.keys():
            MainDict[k] = list(MainDict[k] for MainDict in BothDicts)
        ctx['MainData'] = MainDict

        return render(request, self.template_name, ctx)


class PaymentCountDetailsTestRazr(generic.ListView):
    '''
    Template : payments_ptm_rpay.html
    shows count of payment made by paytm and rpay
    '''
    template_name = "admin/UserAccounts/payments_count_details_test.html"
    title = 'Payments Count of paytm and rpay'

    def get(self, request, date, *args, **kwargs):
        ctx = {}
        new_date = ''
        m = ''
        d = ''
        try:

            date_list = date.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
                # new_date = '{}-{}-{}'.format(y, m, d)
            if len(date_list[2]) > 1:
                d = date_list[2]
            else:
                d = '0' + str(date_list[2])
                # new_date = '{}-{}-{}'.format(y, m, d)
            new_date = '{}-{}-{}'.format(y, m, d)
            Rpay_payments = Payments.objects.filter(create_date__startswith=new_date, pay_source="Rpay",
                                                    status__icontains="Success")


        except:
            print("into except")

        ctx["payment_detail"] = Rpay_payments

        return render(request, self.template_name, ctx)


class PaymentCountDetailsTestPaytm(generic.ListView):
    '''
    Template : payments_ptm_rpay.html
    shows count of payment made by paytm and rpay
    '''
    template_name = "admin/UserAccounts/payments_count_details_test.html"
    title = 'Payments Count of paytm and rpay'

    def get(self, request, date, *args, **kwargs):

        try:
            ctx = {}
            new_date = ''
            m = ''
            d = ''
            date_list = date.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
            else:
                m = '0' + str(date_list[1])
                # new_date = '{}-{}-{}'.format(y, m, d)
            if len(date_list[2]) > 1:
                d = date_list[2]
            else:
                d = '0' + str(date_list[2])
                # new_date = '{}-{}-{}'.format(y, m, d)
            new_date = '{}-{}-{}'.format(y, m, d)
            # Rpay_payments = Payment.objects.filter(create_date__startswith=new_date,pay_source="rpay")

            ptm_payments = Payments.objects.filter(create_date__startswith=new_date, pay_source="Ptm",
                                                   category="Subscription")

        except:
            print("into except")

        ctx["ptm_detail"] = ptm_payments

        return render(request, self.template_name, ctx)


def defaulter_msg_data(user):
    data_list = []
    amount = []
    for i in scheme:
        amount.append(i)

    try:
        loan_amount = int(user.loan_scheme_id)
    except ValueError:
        loan_amount = int(float(user.loan_scheme_id))
    p = amount.index(str(loan_amount))

    m = p + 1
    rep_amount = user.repayment_amount.filter(
        status__icontains='success').aggregate(Sum('amount'))['amount__sum']
    if rep_amount is None and not rep_amount:
        rep_amount = 0.0
    total_amount_pending = loan_amount - float(rep_amount)
    next_loan_amount = int(amount[m])
    data_list.extend((total_amount_pending, next_loan_amount))
    return data_list


def defaulter_messages_scheduler():
    today_end_date_2 = datetime.datetime.now()
    today_end_date_1 = datetime.datetime.now() - timedelta(minutes=30)

    tomorrow_end_time_1 = (today_end_date_2 + timedelta(days=1)) - timedelta(minutes=30)
    tomorrow_end_time_2 = today_end_date_2 + timedelta(days=1)

    due_by_one_time_1 = (today_end_date_2 - timedelta(days=1)) - timedelta(minutes=30)
    due_by_one_time_2 = today_end_date_2 - timedelta(days=1)

    due_by_two_time_1 = (today_end_date_2 - timedelta(days=2)) - timedelta(minutes=30)
    due_by_two_time_2 = today_end_date_2 - timedelta(days=2)

    due_by_three_time_1 = (today_end_date_2 - timedelta(days=3)) - timedelta(minutes=30)
    due_by_three_time_2 = today_end_date_2 - timedelta(days=3)

    due_by_four_time_1 = (today_end_date_2 - timedelta(days=4)) - timedelta(minutes=30)
    due_by_four_time_2 = today_end_date_2 - timedelta(days=4)

    due_by_five_time_1 = (today_end_date_2 - timedelta(days=5)) - timedelta(minutes=30)
    due_by_five_time_2 = today_end_date_2 - timedelta(days=5)

    due_by_six_time_1 = (today_end_date_2 - timedelta(days=6)) - timedelta(minutes=30)
    due_by_six_time_2 = today_end_date_2 - timedelta(days=6)

    due_by_seven_time_1 = (today_end_date_2 - timedelta(days=7)) - timedelta(minutes=30)
    due_by_seven_time_2 = today_end_date_2 - timedelta(days=7)

    due_date_dict = {due_by_one_time_1: due_by_one_time_2,
                     due_by_two_time_1: due_by_two_time_2,
                     due_by_three_time_1: due_by_three_time_2,
                     due_by_four_time_1: due_by_four_time_2,
                     due_by_five_time_1: due_by_five_time_2,
                     due_by_six_time_1: due_by_six_time_2,
                     due_by_seven_time_1: due_by_seven_time_2}

    tomorrow_loans = LoanApplication.objects.filter(status="APPROVED",
                                                    loan_end_date__range=(tomorrow_end_time_1, tomorrow_end_time_2))
    if tomorrow_loans and tomorrow_loans is not None:
        for user in tomorrow_loans:
            if user.loan_start_date != None and user.loan_end_date != None:
                data_list = defaulter_msg_data(user)
                total_amount_pending = data_list[0]
                next_loan_amount = data_list[0]
                loan_end_date = user.loan_end_date
                loan_end_time = loan_end_date.strftime("%Y-%m-%d %H:%M")
                sms_message = ''
                sms_message = "Dear " + user.user_personal.name + ", your loan repayment of Rs." + str(
                    total_amount_pending) + " will be due tomorrow at " + str(
                    loan_end_time) + ". Pay it today and get instant loan of Rs." + str(next_loan_amount)
                sentSms(sms_message, user.user.username)

    today_loans = LoanApplication.objects.filter(status="APPROVED",
                                                 loan_end_date__range=(today_end_date_1, today_end_date_2))
    if today_loans and today_loans is not None:
        for user in today_loans:
            if user.loan_start_date != None and user.loan_end_date != None:
                data_list = defaulter_msg_data(user)
                total_amount_pending = data_list[0]
                next_loan_amount = data_list[0]

                sms_message = ''
                sms_message = "Dear " + user.user_personal.name + ", your loan repayment of Rs." + str(
                    total_amount_pending) + " is pending and your loan is due now. Pay it today and get instant loan of Rs." + str(
                    next_loan_amount)
                sentSms(sms_message, user.user.username)

    due_days = 0
    for start, end in due_date_dict.items():
        due_days += 1
        if due_days == 6:
            loans = LoanApplication.objects.filter(status="APPROVED", loan_end_date__range=(start, end))
            if loans and loans is not None:
                for user in loans:
                    sms_message = ''
                    sms_message = "Final reminder: Dear " + user.user_personal.name + "" + ", pay your overdue loan today to avoid penalty and legal action."
                    sentSms(sms_message, user.user.username)

        elif due_days == 7:
            loans = LoanApplication.objects.filter(status="APPROVED", loan_end_date__range=(start, end))
            if loans and loans is not None:
                for user in loans:
                    sms_message = ''
                    sms_message = "Dear " + user.user_personal.name + ", Despite many reminders, you have not responded. We shall report to " \
                                                                      "CIBIL and start legal action against you if not paid today. "
                    sentSms(sms_message, user.user.username)
        else:
            loans = LoanApplication.objects.filter(status="APPROVED", loan_end_date__range=(start, end))
            if loans and loans is not None:
                for user in loans:
                    if user.loan_start_date != None and user.loan_end_date != None:
                        data_list = defaulter_msg_data(user)
                        total_amount_pending = data_list[0]
                        next_loan_amount = data_list[0]

                        sms_message = ''
                        sms_message = "Dear " + user.user_personal.name + ", your loan repayment of Rs." + str(
                            total_amount_pending) + " is overdue by " + str(
                            due_days) + " days. \nPay it today to avoid penalty and get instant loan of Rs." + str(
                            next_loan_amount)
                        sentSms(sms_message, user.user.username)


class UnallocatedDefaulterList(generic.ListView):
    template_name = 'recovery/new_defaulter_list.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        allocated_list = []
        default_counter = 0
        today = datetime.datetime.now()
        yesterday = datetime.datetime.now() - timedelta(days=1)
        before_yesterday = datetime.datetime.now() - timedelta(days=2)
        users = LoanApplication.objects.filter(status="APPROVED")
        if users:
            for user in users:
                if user.status != 'COMPLETED':
                    for i in scheme:
                        if i == user.loan_scheme_id:
                            if user.loan_end_date:
                                return_date = user.loan_end_date + timedelta(int(scheme[i]['ms_tenure']))
                                if (yesterday - return_date).days == 1:
                                    default_counter += 1
                                return_date = user.loan_end_date
                                if return_date:
                                    if str(return_date)[:10] == str(today)[:10]:
                                        # Don't allocate today's defaulters
                                        pass
                                    elif str(before_yesterday)[:10] >= str(return_date)[:10]:
                                        # users in list
                                        defaulter_check = Recovery.objects.filter(loan_transaction_id=user.id)
                                        if not defaulter_check:
                                            if user.repayment_amount is None or not user.repayment_amount:
                                                repayment_amount = 0
                                            else:
                                                repayment_amount = user.repayment_amount.aggregate(Sum('amount'))[
                                                    'amount__sum']
                                            balance_amount = int(user.loan_scheme_id) - repayment_amount
                                            data_dict = {repayment_amount: [user, balance_amount]}
                                            allocated_list.append(data_dict)
                                            ctx['user'] = allocated_list
                                            break
                                        else:
                                            pass
        return render(request, self.template_name, ctx)


def CsvgetDefaulterList(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="DefaulterList.csv"'
    writer = csv.writer(response)
    writer.writerow(['Username', 'User', 'LoanApplication Id', 'Loan amount', 'Disbursed Amount',
                     'Loan Start Date', 'Loan End Date', 'Status'])
    try:
        today = datetime.datetime.now()
        loan_users = LoanApplication.objects.filter(status='APPROVED')
        for user in loan_users:
            for i in scheme:
                if i == user.loan_scheme_id:
                    amount = 0
                    if user.loan_end_date:
                        return_date = user.loan_end_date + timedelta(int(90))
                        if today > return_date:
                            rep_amount = user.repayment_amount.filter(
                                status='success').aggregate(Sum('amount'))['amount__sum']
                            if rep_amount is not None:
                                amount = rep_amount
                    writer.writerow([user.user_personal.name,
                                     user.user.username, user.loan_application_id, user.loan_scheme_id,
                                     amount, user.loan_start_date, user.loan_end_date, user.status])
        return response
    except Exception as e:
        return HttpResponse(e)


def CsvgetOverDueList(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="OverDueList.csv"'
    writer = csv.writer(response)
    writer.writerow(
        ['Username', 'User', 'LoanApplication Id', 'Loan Amount', 'Repayment Amount',
         'Loan Start Date', 'Loan End Date', 'Status'])
    try:
        today = datetime.datetime.today()
        loan_users = LoanApplication.objects.filter(status='APPROVED')
        for user in loan_users:
            for i in scheme:
                if i == user.loan_scheme_id:
                    amount = 0
                    if user.loan_end_date:
                        return_date = user.loan_end_date + timedelta(int(90))
                        if (today > user.loan_end_date) & (today <= return_date):
                            if user.repayment_amount:
                                amt = user.repayment_amount.filter(
                                    status='success').aggregate(Sum('amount'))['amount__sum']
                                if amt is not None:
                                    amount = amt
                    writer.writerow(
                        [user.user_personal.name, user.user.username, user.loan_application_id, user.loan_scheme_id,
                         amount, user.loan_start_date, user.loan_end_date, user.status])
        return response
    except Exception as e:
        return HttpResponse(e)


def loan_application_without_defaulters(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="LoanApplicationDataWithoutDefaulters.csv"'
    writer = csv.writer(response)
    writer.writerow(
        ['Mobile NO', 'User', 'LoanApplication Id', 'Loan Amount',
         'Loan Start Date', 'Loan End Date', 'Status'])
    try:
        today = datetime.datetime.now()
        loan_users = LoanApplication.objects.filter(status='APPROVED')
        for user in loan_users:
            for i in scheme:
                if i == user.loan_scheme_id:
                    if user.loan_end_date:
                        return_date = user.loan_end_date + timedelta(int(90))
                        if (today > user.loan_end_date) and (today <= return_date):
                            pass
                        else:
                            writer.writerow(
                                [user.user_personal.name, user.user.username, user.loan_application_id,
                                 user.loan_scheme_id, user.loan_start_date,
                                 user.loan_end_date, user.status])
        return response
    except:
        return response


def loan_status(request):
    ctx = {}
    status = ['SUBMITTED', 'APPROVED', 'COMPLETED', 'CANCELLED', 'REJECTED', 'PENDING', 'ON_HOLD',
              'CREDITVIDYA', 'PRE_APPROVED', 'PROCESS']
    ctx['status'] = status
    return render(request, 'business_associate/report.html', ctx)


def get_all_loans_data(request):
    if request.method == "POST":
        status = request.POST['status']
        start_date = request.POST['start_date']
        end_date = request.POST['end_date']

        # Create the HttpResponse object with the appropriate CSV header.
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="all loan data.csv"'
        writer = csv.writer(response)
        writer.writerow(
            ['Mobile number', 'name', 'Loan Application', 'Start date', 'End Date', 'Loan Status', 'Loan Scheme Id',
             'Approved Amount'])
        # get_userloans = LoanApplication.objects.all()
        get_userloans = LoanApplication.objects.filter(status=status, created_date__range=(start_date, end_date))
        if get_userloans:
            for get_userloan in get_userloans:
                name = ""
                if get_userloan.user_personal:
                    name = get_userloan.user_personal.name

                writer.writerow(
                    [get_userloan.user.username, name, get_userloan.loan_application_id,
                     get_userloan.loan_start_date, get_userloan.loan_end_date, get_userloan.status,
                     get_userloan.loan_scheme_id, get_userloan.approved_amount])

        return response


def get_all_loan_with_status(request):
    today = datetime.datetime.now()
    status = ""
    if request.method == "POST":
        if request.POST['loan_status'] is not None:
            status = request.POST['loan_status']
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    base_string = 'attachment; filename="'
    date_string = str(today)[:10]
    # status = status.capitalize()
    final_str = base_string + date_string + " " + status + " loan application data till date " + '.csv"'
    response['Content-Disposition'] = final_str
    writer = csv.writer(response)
    writer.writerow(
        ['Mobile number', 'name', 'Loan Application', 'Start date', 'End Date', 'Loan Status', 'Loan Scheme Id',
         'Approved Amount'])
    # get_userloans = LoanApplication.objects.all()
    get_userloans = LoanApplication.objects.filter(status=status)
    for get_userloan in get_userloans:
        if get_userloan is not None and get_userloan:
            # if get_userloan.user_personal is not None:
            writer.writerow(
                [get_userloan.user.username, get_userloan.user_personal.name, get_userloan.loan_application_id,
                 get_userloan.loan_start_date, get_userloan.loan_end_date, get_userloan.status,
                 get_userloan.loan_scheme_id, get_userloan.approved_amount])
    return response


def Convert(string):
    list1 = []
    list1[:0] = string
    return list1


def pan_no_check(user_id, pan_card_number):
    personal = Personal.objects.filter(type="credit", user_id=user_id)
    number_count = None
    data = {}
    if personal:
        count = 0
        try:
            personl_number_list = Convert(personal[0].pan_number)
            pan_card_number_list = Convert(pan_card_number)
            for i in range(0, 10):
                if personl_number_list[i] == pan_card_number_list[i]:
                    count = count + 1
            if count > 0:
                number_count = count * 100 / 10
            else:
                number_count = count
        except:
            number_count = count

        data["pan_number"] = personal[0].pan_number
    else:
        data["pan_number"] = None
    data["number_count"] = number_count
    return data


def month_income(user_id, month_income):
    personal = Personal.objects.filter(type="credit", user_id=user_id)
    number_count = None
    data = {}
    if personal:
        if float(personal[0].monthly_income) == float(month_income):
            number_count = 100
        else:
            number_count = 0

        data["month_income"] = personal[0].monthly_income
    else:
        data["month_income"] = None
    data["number_count"] = number_count
    return data


def pincode_number(user_id, pincode):
    address = Address.objects.filter(type="Permanent", user_id=user_id)
    number_count = None
    data = {}
    if address:
        count = 0
        try:
            personl_number_list = Convert(address[0].pin_code)
            pincode_list = Convert(pincode)
            for i in range(0, 6):
                if personl_number_list[i] == pincode_list[i]:
                    count = count + 1
            if count > 0:
                number_count = count * 100 / 6
            else:
                number_count = count
        except:
            number_count = count
        data["pincode_no"] = address[0].pin_code
    else:
        data["pincode_no"] = None
    data["number_count"] = number_count

    return data


# changed
@method_decorator([login_required(login_url="/login/")], name='dispatch')
class DefaulterAllDetails(generic.ListView):
    """
        Template : user_all_details.html
        shows user loan application
    """
    login_required = True
    template_name = "admin/UserAccounts/defaulter_all_details_new.html"
    title = 'User Details'
    pancount = 0

    def get(self, request, id):
        ctx = {}
        try:
            import json
            import re
            loan_pdf = LoanAcknowledgement.objects.filter(loan_transaction_id=id).last()
            if loan_pdf:
                ctx["file"] = loan_pdf.acknowledgement_pdf
            loan_users = LoanApplication.objects.select_related('verified_by', 'user', 'user_professional',
                                                                'user_personal', 'aadhar_id', 'bank_sts_id',
                                                                'salary_slip_id', 'company_id', 'self_video',
                                                                'current_address', 'permanent_address',
                                                                'reference_id',
                                                                'bank_id', 'cibil_id', 'device_data_id').get(id=id)

            pan_card_number = loan_users.user_personal.pan_number
            dict_param = {
                'pan_card': pan_card_number,
                'username': "creditkartpan",
                'password': "credit@123",
            }
            json_body = json.dumps(dict_param)
            prod_url = 'https://mudrakwik.anandfin.com/api/pancard-validate-creditkart/'
            headers = {
                'content-type': "application/json",
                'cache-control': "no-cache",
            }
            response = requests.post(prod_url, data=json_body, headers=headers, verify=False)
            responce_js = response.json()
            pancard_name = responce_js["name"]
            user_name = loan_users.user_personal.name
            per = per_pan_name_match(user_name, pancard_name)

            pan_card_no = pan_no_check(loan_users.user.id, pan_card_number)

            ctx["pan_card_no_per"] = pan_card_no["number_count"]
            ctx["pan_card_no"] = pan_card_no["pan_number"]

            pincodeno = pincode_number(loan_users.user.id, loan_users.current_address.pin_code)
            ctx["pincode_number_count"] = pincodeno["number_count"]
            ctx["pincode_no"] = pincodeno["pincode_no"]

            monthincome = month_income(loan_users.user.id, loan_users.user_personal.monthly_income)
            ctx["mi_number_count"] = monthincome["number_count"]
            ctx["monthly_income"] = monthincome["month_income"]

            if loan_users.repayment_amount:
                payment_obj = loan_users.repayment_amount.all()
                if payment_obj:
                    ctx['payment_details'] = payment_obj

            from .loan_funcations import loan_amount_slab
            slabs_data = loan_amount_slab(loan_users.id)
            if slabs_data:
                ctx['payment_slab'] = slabs_data

            try:
                company_id = loan_users.company_id.path
                company_id = botofile.get_url(company_id)
            except:
                company_id = None

            try:
                pan = loan_users.pancard_id.path
                pan = botofile.get_url(pan)
            except:
                pan = None

            try:
                salary_slip = loan_users.salary_slip_id.path
                salary_slip = botofile.get_url(salary_slip)
            except:
                salary_slip = None

            try:
                profile_images = loan_users.self_video.path
                profile_images = botofile.get_url(profile_images)
            except:
                profile_images = None

            equfix_score = 0
            user_value = EquiFaxMaster.objects.values_list('id').filter(user_id=loan_users.user_id).order_by('-id')
            if user_value:
                score_value = EquiFaxScoreDetails.objects.values_list('score_value').filter(
                    equifax_id=user_value[0][0]).order_by('-id')
                if score_value:
                    equfix_score = score_value[0][0]

            loan_count = 0
            repayment_txc_count = 0
            loan_ids = LoanApplication.objects.values_list('id', 'loan_application_id',
                                                           'repayment_amount').order_by(
                'loan_application_id').distinct('loan_application_id').filter(user_id=loan_users.user_id,
                                                                              status__in=['APPROVED', 'COMPLETED'])
            loan_count = len(loan_ids)

            try:
                for id in loan_ids:
                    if id[2] is not None:
                        loan_rep = LoanApplication.objects.filter(loan_application_id=id[1])
                        loan_payment_count = loan_rep[0].repayment_amount.filter(
                            status='success').count()
                        repayment_txc_count += loan_payment_count
                    else:
                        repayment_txc_count = 0
                        # TODO Raise Error
                        pass
            except:
                pass
            if loan_users.overdue:
                overdue = "This profile is under Overdue with Count :  " + str(loan_users.overdue)
            else:
                overdue = ""

            # Seraches pancard for this number
            pancount = PancardMatch(loan_users.user_personal.pan_number)

            current_address = loan_users.current_address

            # if current_address is not None:
            #     pincode = get_pincodewise_analysis(current_address.pin_code)
            #     try:
            #         if pincode:
            #             ctx['user_count'] = pincode[0]
            #             ctx['overdue'] = pincode[1]
            #         else:
            #             ctx['user_count'] = None
            #             ctx['overdue'] = None
            #     except:
            #         ctx['user_count'] = None
            #         ctx['overdue'] = None

            ctx['pan'] = pan
            ctx['company_id'] = company_id
            ctx['salary_slip'] = salary_slip

            ctx['pancount'] = pancount

            ctx['users'] = loan_users
            ctx['pancard_name'] = pancard_name
            ctx['pancard_matching_percentage'] = round(per, 2)
            ctx['users_images'] = profile_images
            ctx['overdue_count'] = overdue

            ctx['smscount'] = loan_users.total_sms
            ctx['user_dpd'] = loan_users.dpd
            ctx['equfix_score'] = equfix_score
            user_ref_list = []
            default_users = []
            # Generating user dpd list
            dpd_list = []
            loan_userss = LoanApplication.objects.values_list('loan_application_id', 'dpd', 'loan_scheme_id',
                                                              'status', 'approved_amount').filter(
                user=loan_users.user_id,
                status__in=['APPROVED', 'COMPLETED'])
            for i in loan_userss:
                dpd_list.append([i[0], i[1], i[2], i[3], i[4]])
            if dpd_list is None:
                dpd_list = None
            ctx['user_loancount_list'] = loan_count
            ctx['user_transaction_list'] = repayment_txc_count
            ctx['balance'] = 0
            ctx['dpd_list'] = dpd_list
            ctx['NOT_FOUND'] = get_reference_details(str(loan_users.loan_application_id))
            return render(request, self.template_name, ctx)

        except Exception as e:
            print("------USer all details exc", e)
            print('-----------in exception----------')
            print(e.args)
            import os
            import sys
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


def user_wise_loan_details(request):
    """
        Template : userwise_loan_details.html
        shows user loan application
    """
    ctx = {}
    template_name = "admin/UserAccounts/userwise_loan_details_new.html"
    df = read_frame(LoanApplication.objects.all(), ['user__username', 'id'])
    data_df = df.groupby(df["user__username"])['id'].count().sort_index(axis=0)
    data_dict = data_df.to_dict()
    ctx['loan_data'] = data_dict
    return render(request, template_name, ctx)


def users_loan_details(request, username):
    """
    show particular user loan details
    """
    template_name = "admin/UserAccounts/userloan_applications_list_pre_approved_new.html"
    title = ' User Loan Application'
    ctx = {}
    loan_users = LoanApplication.objects.filter(user__username=username).order_by('-id')
    if loan_users:
        ctx['users'] = loan_users
    return render(request, template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class SearchLoan(generic.ListView):
    template_name = "admin/UserAccounts/search_loan_new.html"

    def get(self, request):
        return render(request, self.template_name)

    def post(self, request):
        ctx = {}
        if request.method == "POST":
            serach_by = request.POST['search_by']
            field = request.POST['id']
            if serach_by == "loan_application_id":
                loan_obj = LoanApplication.objects.filter(loan_application_id=field)
                if loan_obj:
                    ctx['users'] = loan_obj
                else:
                    ctx['msg'] = "No Record Found"
            if serach_by == "transaction_id":
                payment_details = Payments.objects.filter(transaction_id=field)
                if payment_details:
                    ctx['payment_details'] = payment_details
                else:
                    ctx['msg'] = "No Record Found"
            if serach_by == "order_id":
                payment_details = Payments.objects.filter(order_id=field)
                if payment_details:
                    ctx['payment_details'] = payment_details
                else:
                    ctx['msg'] = "No Record Found"

            if serach_by == "mobile_no":
                loan_obj = LoanApplication.objects.filter(user__username=field)
                if loan_obj:
                    ctx['users'] = loan_obj
                else:
                    ctx['msg'] = "No Record Found"
        return render(request, self.template_name, ctx)


def user_wise_withdraw_loan(request):
    """
        Template : userwise_withdraw_loan.html
        shows user withdraw loan application
    """
    ctx = {}
    template_name = "admin/UserAccounts/userwise_withdraw_loan_new.html"
    # query = LoanApplication.objects.values_list('username', flat=True).distinct()
    # for i in query:
    #     LoanApplication.objects.filter()

    df = read_frame(LoanApplication.objects.filter(disbursed_amount__status="success"),
                    fieldnames=['user__username', 'disbursed_amount__category'])
    df = df[df['disbursed_amount__category'].astype(str).str.contains('Rbl')]
    data_df = df.groupby(df["user__username"])['disbursed_amount__category'].count().sort_index(axis=0)
    data_dict = data_df.to_dict()
    ctx['loan_data'] = data_dict
    return render(request, template_name, ctx)


def users_withdraw_loan_details(request, username):
    template_name = "admin/UserAccounts/users_withdraw_loan_details_new.html"
    ctx = {}
    df = read_frame(LoanApplication.objects.filter(user__username=username, disbursed_amount__status="success"),
                    fieldnames=['loan_application_id', 'disbursed_amount__category'])
    df = df[df['disbursed_amount__category'].astype(str).str.contains('Rbl')]
    data_df = df.groupby(df["loan_application_id"])['disbursed_amount__category'].count().sort_index(axis=0)
    data_dict = data_df.to_dict()
    ctx['loan_data'] = data_dict
    return render(request, template_name, ctx)


def users_withdraw_payment_details(request, loan_application_id):
    template_name = "admin/UserAccounts/users_withdraw_payment_details_new.html"
    ctx = {}
    loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
    if loan_obj:
        payment_details = loan_obj[0].disbursed_amount.filter(category="Rbl", status="success")
        if payment_details:
            ctx['payment_details'] = payment_details
    return render(request, template_name, ctx)


def user_data(request):
    """
    show loan user with loan application count
    """
    template_name = "admin/UserAccounts/user_data.html"
    ctx = {}
    data_dict = {}
    loan_user_list = LoanApplication.objects.values_list("user__username", flat=True).distinct()
    if loan_user_list:
        for user in loan_user_list:
            loan_count = LoanApplication.objects.filter(user__username=user).count()
            data_dict.update({user: loan_count})
        ctx['user'] = data_dict
    return render(request, template_name, ctx)


def show_user_loan_data(request, username):
    """
    show loan data for particular user
    """
    ctx = {}
    template_name = "admin/UserAccounts/show_user_loan_data.html"
    loan_obj = LoanApplication.objects.filter(user__username=username)
    if loan_obj:
        ctx['users'] = loan_obj
    return render(request, template_name, ctx)


def completed_loan_data(request):
    """
    Show Completed loan data
    """
    ctx = {}
    template_name = "admin/UserAccounts/show_user_loan_data.html"
    loan_obj = LoanApplication.objects.filter(status="COMPLETED")
    if loan_obj:
        ctx['users'] = loan_obj
    return render(request, template_name, ctx)


from .loan_funcations import loan_amount_slab


@login_required(login_url="/ecom/login/")
def show_defaulter(request):
    ctx = {}
    template_name = 'admin/dashboard/defaulter_list_new.html'
    today = datetime.datetime.now()
    today_date = str(today)[:11]
    loan_list = []
    approved_loan_obj = LoanApplication.objects.filter(status="APPROVED", user_status="Accept")
    if approved_loan_obj:
        for loan in approved_loan_obj:
            slab_list = loan_amount_slab(loan.id)
            if slab_list:
                for data in slab_list:
                    if data['pay_status'] == 'unpaid' and today_date > data['date']:
                        loan_list.append(loan)
        ctx['users'] = loan_list
    return render(request, template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class SearchLoanStatus(generic.ListView):
    template_name = "admin/UserAccounts/search_loan_status_new.html"

    def get(self, request):
        ctx = {}
        status_list = ['SUBMITTED', 'APPROVED', 'COMPLETED', 'CANCELLED', 'REJECTED', 'PENDING', 'ON_HOLD',
                       'CREDITVIDYA',
                       'PRE_APPROVED', 'PROCESS']
        if status_list:
            ctx["status_list"] = status_list
        return render(request, self.template_name, ctx)

    def post(self, request):
        from datetime import date
        ctx = {}
        loan_id_list = []
        status_list = ['SUBMITTED', 'APPROVED', 'COMPLETED', 'CANCELLED', 'REJECTED', 'PENDING', 'ON_HOLD',
                       'CREDITVIDYA',
                       'PRE_APPROVED', 'PROCESS']
        if status_list:
            ctx["status_list"] = status_list
        if request.method == "POST":
            status = request.POST['status']
            start_date = request.POST.get('start_date')
            end_date = request.POST.get('end_date')
            if status == "All":
                if start_date and end_date:
                    loan_obj = LoanApplication.objects.filter(created_date__date__range=(start_date, end_date))
                else:
                    loan_obj = LoanApplication.objects.all()
            else:
                if start_date and end_date:
                    loan_obj = LoanApplication.objects.filter(status=status,
                                                              created_date__date__range=(start_date, end_date))
                else:
                    loan_obj = LoanApplication.objects.filter(status=status)
            if loan_obj:
                for loan in loan_obj:
                    loan_id_list.append(loan.id)
                ctx["loan_id_list"] = loan_id_list
                ctx['users'] = loan_obj
            else:
                ctx['msg'] = "No Record Found"
        return render(request, self.template_name, ctx)


@login_required(login_url="/ecom/login/")
def loan_application_csv(request, loan_id_list):
    time = datetime.datetime.now()
    date_string = str(time)[:10]
    base_string = 'attachment; filename="'
    final_str = base_string + date_string + "_loanapplication_list " + '.csv"'
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = final_str
    writer = csv.writer(response)
    writer.writerow(
        ['Loan Application Id', 'Repayment Amount', 'Approved Amount', 'Status', 'Created Date'])
    loan_id = loan_id_list[1:-1]
    if len(loan_id) == 1:
        loan_obj = LoanApplication.objects.filter(id=int(loan_id))
    else:
        loan_id = loan_id.split(",")
        loan_id = [int(i) for i in loan_id]
        loan_obj = LoanApplication.objects.filter(id__in=loan_id)
    if loan_obj:
        for obj in loan_obj:
            loan_application_id = ""
            repayment_amount = ""
            approved_amount = ""
            status = ""
            created_date = ""
            if obj:
                loan_application_id = obj.loan_application_id
                repayment_amount = obj.loan_scheme_id
                approved_amount = obj.approved_amount
                status = obj.status
                created_date = str(obj.created_date)[:11]
                writer.writerow([loan_application_id, repayment_amount, approved_amount, status, created_date])
    return response


# @method_decorator([login_required(login_url="/login/")], name='dispatch')
class UserLoanAgreement(generic.ListView):
    """
        Template : user_all_details.html
        shows user loan application
    """
    login_required = True
    template_name = "admin/UserAccounts/user_loan_agreement.html"
    title = 'Loan Agreement'
    pancount = 0

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, loan_application_id):
        ctx = {}

        user_id = request.user.id

        loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
        if loan_obj:
            for i in loan_obj:
                startdate = i.loan_start_date
                date = startdate.day
                month = startdate.month
                year = startdate.year
                final_approved_amount = i.approved_amount

                ctx['approved_amount'] = final_approved_amount
                data_list = [date, month, year]
                ctx["database"] = data_list
                name = i.user_personal.name
                pan_number = i.user_personal.pan_number
                address = i.current_address.address
                city = i.current_address.city
                state = i.current_address.state
                pin_code = i.current_address.pin_code

                ctx["personal"] = [name, pan_number, address, city, state, pin_code]

        return render(request, self.template_name, ctx)


def user_loan_repayment(request):
    ctx = {}
    template_name = "admin/UserAccounts/user_loan_repayment.html"
    loan_application = LoanApplication.objects.filter(status__in=['APPROVED', 'COMPLETED'], user_status="Accept")
    dataframe = read_frame(loan_application)
    fieldnames = ['created_date', 'id']
    df = read_frame(loan_application, fieldnames)

    df["YearMonth"] = [str(i)[:10] for i in df[fieldnames[0]]]
    data_df = df.groupby(df["YearMonth"])[fieldnames[1]].count().sort_index(axis=0)
    data_dict = data_df.to_dict()

    ctx['application_details'] = data_dict

    return render(request, template_name, ctx)


def user_loan_repayment_data(request, date):
    ctx = {}
    template_name = "admin/UserAccounts/user_repayment_data.html"
    loan_user = LoanApplication.objects.filter(created_date__startswith=date, status__in=['APPROVED', 'COMPLETED'],
                                               user_status="Accept")
    if loan_user:
        for loan in loan_user:
            disbursed_amount_details = loan.disbursed_amount.all()
            repayment_loan_amount = loan.disbursed_amount.filter(category='Loan_Repayment')
            ctx['disbursed_amount_details'] = disbursed_amount_details
            ctx['repayment_loan_amount'] = repayment_loan_amount

    return render(request, template_name, ctx)


@method_decorator([login_required(login_url="/ecom/login/")], name='dispatch')
class SearchLoanApplicationData(generic.ListView):
    template_name = "admin/UserAccounts/search_loan_application.html"

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


@csrf_exempt
@login_required(login_url="/ecom/login/")
def search_loan_application(request):
    ctx = {}
    loan_application_dict = {}
    loan_application_list = []
    loan_application_obj = None
    payment_obj = None
    payment_dict = {}
    payment_list = []
    loan_id = None
    if request.method == 'POST':
        input_data = request.POST['input_data']
        option = request.POST['option']

        if option == "loan_application":
            loan_application_obj = LoanApplication.objects.filter(loan_application_id=input_data)

        if option == "mobile_no":
            loan_application_obj = LoanApplication.objects.filter(user__username=input_data)

        if option == "order_id":
            payment_obj = Payments.objects.filter(order_id=input_data)

        if loan_application_obj:
            for loan_application in loan_application_obj:
                loan_application_dict = {
                    "id": loan_application.id,
                    "loan_application_id": loan_application.loan_application_id,
                    "status": loan_application.status,
                    "loan_start_date": str(loan_application.loan_start_date),
                    "loan_end_date": str(loan_application.loan_end_date),
                    "approved_amount": loan_application.approved_amount,
                    "user_status": loan_application.user_status,

                }
                loan_application_list.append(loan_application_dict)
                ctx['loanapplication'] = loan_application_list
        if payment_obj:
            for payment in payment_obj:
                payment_order_id = payment.order_id
                payment_order_id = payment_order_id.split("l", 1)

                new_order_id = payment_order_id[0]
                loan_application = LoanApplication.objects.filter(loan_application_id=new_order_id)
                if loan_application:
                    for loan in loan_application:
                        loan_id = loan.id

                payment_dict = {
                    "id": loan_id,
                    "order_id": payment.order_id,
                    "transaction_id": payment.transaction_id,
                    "pg_transaction_id": payment.pg_transaction_id,
                    "status": payment.status,
                    "category": payment.category,
                    "mode": payment.mode,
                    "type": payment.type,
                    "amount": payment.amount,
                    "product_type": payment.product_type,
                    "platform_type": payment.platform_type,
                    "date": str(payment.create_date),

                }
                payment_list.append(payment_dict)
                ctx['payment_data'] = payment_list

        else:
            ctx['msg'] = "Data Not Exist"

    return HttpResponse(json.dumps(ctx))


def month_wise_disbursed_repayment(request):
    ctx = {}
    data_dict = {}
    data_list = []
    disbursed_amount = 0
    repayment_amount = 0

    template_name = "admin/UserAccounts/month_wise_disbursed_repayment.html"
    total = 0
    dates = LoanApplication.objects.filter(status__in=['APPROVED', 'COMPLETED'],
                                           disbursed_amount_wallet__status="loan_disbursed").values_list(
        'created_date',
        flat=True).distinct()
    if dates:
        created_date = {str(date)[:7] for date in dates}
        if dates:
            for date in created_date:
                disbursed_amount_1 = LoanApplication.objects.filter(created_date__startswith=date,
                                                                    disbursed_amount_wallet__status="loan_disbursed",
                                                                    status__in=['APPROVED', 'COMPLETED']).aggregate(
                    Sum('approved_amount'))[
                    'approved_amount__sum']
                if disbursed_amount_1 > 0:
                    disbursed_amount = disbursed_amount_1 * 90 / 100
                repayment_amount = \
                    Payments.objects.filter(category='Loan_Repayment', status__in=['Success', 'success'],
                                            date__startswith=date).aggregate(Sum('amount'))[
                        'amount__sum']

                data_dict.update({date: [disbursed_amount, repayment_amount]})
            ctx['loan_data'] = data_dict

        return render(request, template_name, ctx)


def date_wise_disbursed_repayment(request, date):
    MainDict = {}
    ctx = {}
    new_date = ""
    template_name = "admin/UserAccounts/date_wise_disbursed_repayment.html"

    month_list = Payments.objects.exclude(date=None).filter(date__startswith=date).order_by(
        "-date").values_list("date", flat=True).distinct()
    dates = [str(i)[:10] for i in month_list]
    # taking unique dates
    dates = list(dict.fromkeys(dates))
    if dates:
        for date in dates:
            m_list = []
            a = LoanApplication.objects.filter(created_date__startswith=date,
                                               disbursed_amount_wallet__status="loan_disbursed",
                                               status__in=["APPROVED", "COMPLETED"]).aggregate(
                Sum('approved_amount'))[
                'approved_amount__sum']
            if a:
                a = a * 90 / 100
            if a:
                m_list.append(a)
            else:
                m_list.append(0)

            b = Payments.objects.filter(category='Loan_Repayment', status__in=['Success', 'success'],
                                        date__startswith=date).aggregate(Sum('amount'))['amount__sum']
            if b:
                m_list.append(b)
            else:
                m_list.append(0)
            if m_list[0] == 0 and m_list[1] == 0:
                pass
            else:
                MainDict.update(
                    {
                        date: m_list
                    }
                )
    date_list = date.split('-')
    y = date_list[0]
    if len(date_list[1]) > 1:
        m = date_list[1]
        new_date = '{}-{}'.format(y, m)
    else:
        m = '0' + str(date_list[1])
        new_date = '{}-{}'.format(y, m)
    date_set = set()
    disbursed = Payments.objects.filter(date__startswith=new_date).order_by('-date')
    for user in disbursed:
        dt = str(user.date)
        date_set.add(dt[0:10])
    create_month_1 = []
    disbursed_amount = []
    for dt in date_set:
        cost_sum = \
            LoanApplication.objects.filter(loan_start_date__startswith=date, status__in=["APPROVED", "COMPLETED"],
                                           disbursed_amount_wallet__status="loan_disbursed").aggregate(
                Sum('approved_amount'))['approved_amount__sum']
        if cost_sum:
            cost_sum = cost_sum * 90 / 100
        create_month_1.append(dt)
        disbursed_amount.append(cost_sum)
    create_month_2 = []
    repayment_amount = []
    for dt in date_set:
        cost_sum = Payments.objects.filter(date__startswith=dt, category='Loan_Repayment',
                                           status__in=['Success', 'success']).aggregate(
            Sum('amount'))['amount__sum']
        create_month_2.append(dt)
        repayment_amount.append(cost_sum)
    # ctx['amount_info'] = zip((create_month_1), disbursed_amount, repayment_amount)
    ctx['loan_data'] = MainDict
    return render(request, template_name, ctx)


def disbursed_data(request, date, disbursed_id_list):
    ctx = {}
    template_name = "admin/UserAccounts/disbursed_data.html"

    disbursed_id_list = disbursed_id_list[1:-1]

    if len(disbursed_id_list) == 1:
        loan_obj = Payments.objects.filter(id=int(disbursed_id_list))
    else:
        disbursed_id_list = disbursed_id_list.split(",")
        disbursed_id_list = [int(i) for i in disbursed_id_list]
        loan_obj = Payments.objects.filter(id__in=disbursed_id_list)

    ctx['payment'] = loan_obj

    return render(request, template_name, ctx)


def repayment_data(request, date, repayment_id_list):
    ctx = {}
    template_name = "admin/UserAccounts/disbursed_data.html"

    repayment_id_list = repayment_id_list[1:-1]

    if len(repayment_id_list) == 1:
        loan_obj = Payments.objects.filter(id=int(repayment_id_list))
    else:
        repayment_id_list = repayment_id_list.split(",")
        repayment_id_list = [int(i) for i in repayment_id_list]
        loan_obj = Payments.objects.filter(id__in=repayment_id_list)

    ctx['payment'] = loan_obj

    return render(request, template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class MonthwiseLoanApplicationViewAll(generic.ListView):
    '''
    Template : monthwise_loanapplication_all.html
    shows user loan applications by date
    '''
    template_name = "admin/UserAccounts/monthwise_loanapplication_all.html"
    title = ' User Loan Application new'

    def get(self, request, UpdateMessage="", *args, **kwargs):
        day_wise_application_count_dict = {}
        try:
            ctx = {}
            loan_users = LoanApplication.objects.all()
            fieldnames = ['created_date', 'id']
            df = read_frame(loan_users, fieldnames)

            df["YearMonth"] = [str(i)[:7] for i in df[fieldnames[0]]]
            data_df = df.groupby(df["YearMonth"])[fieldnames[1]].count().sort_index(axis=0)
            data_dict = data_df.to_dict()


        except:
            pass
        ctx['application_details'] = data_dict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class MonthwiseLoanApplicationViewDisbursedAll(generic.ListView):
    '''
    Template : monthwise_loanapplication_all.html
    shows user loan applications by date
    '''
    template_name = "admin/UserAccounts/monthwise_loanapplication_all_disbursed.html"
    title = ' User Loan Application new'

    def get(self, request, UpdateMessage="", *args, **kwargs):
        day_wise_application_count_dict = {}
        try:
            ctx = {}
            loan_users = []
            loan_user = LoanApplication.objects.filter(status__in=["APPROVED", "COMPLETED"],
                                                       disbursed_amount_wallet__status="loan_disbursed")
            fieldnames = ['created_date', 'id']
            df = read_frame(loan_user, fieldnames)

            df["YearMonth"] = [str(i)[:7] for i in df[fieldnames[0]]]
            data_df = df.groupby(df["YearMonth"])[fieldnames[1]].count().sort_index(axis=0)
            data_dict = data_df.to_dict()


        except:
            pass
        ctx['application_details'] = data_dict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class LoanApplicationViewDisbursedAll(generic.ListView):
    '''
    Template : userloan_applications_list_all.html
    shows user loan applications by date
    '''
    template_name = "admin/UserAccounts/userloan_application_list_all_disbursed.html"
    title = ' User Loan Application new'

    def get(self, request, date):
        day_wise_application_count_dict = {}
        data_dict = {}
        try:
            ctx = {}
            loan_users = []
            loan_user = LoanApplication.objects.filter(created_date__startswith=date,
                                                       status__in=["APPROVED", "COMPLETED"],
                                                       disbursed_amount_wallet__status="loan_disbursed")

            fieldnames = ['created_date', 'id']
            df = read_frame(loan_user, fieldnames)

            df["YearMonth"] = [str(i)[:10] for i in df[fieldnames[0]]]
            data_df = df.groupby(df["YearMonth"])[fieldnames[1]].count().sort_index(axis=0)
            data_dict = data_df.to_dict()

        except:
            pass
        ctx['application_details'] = data_dict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class LoanApplicationViewAll(generic.ListView):
    '''
    Template : userloan_applications_list_all.html
    shows user loan applications by date
    '''
    template_name = "admin/UserAccounts/userloan_application_list_all.html"
    title = ' User Loan Application new'

    def get(self, request, date):
        day_wise_application_count_dict = {}
        try:
            ctx = {}
            loan_users = LoanApplication.objects.filter(created_date__startswith=date)
            dataframe = read_frame(loan_users)
            fieldnames = ['created_date', 'id']
            df = read_frame(loan_users, fieldnames)

            df["YearMonth"] = [str(i)[:10] for i in df[fieldnames[0]]]
            data_df = df.groupby(df["YearMonth"])[fieldnames[1]].count().sort_index(axis=0)
            data_dict = data_df.to_dict()

        except:
            pass
        ctx['application_details'] = data_dict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class UserLoanApplicationViewAll(generic.ListView):
    template_name = "admin/UserAccounts/userloan_applications_list_new.html"
    title = ' User Loan Application'

    def get(self, request, date):
        userreject = []
        loan_users = []
        ctx = {}
        UpdateMessage = ""

        try:

            loan_users = LoanApplication.objects.filter(created_date__startswith=date)


        except Exception as e:
            # print("-----------------exc", e, e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            pass
        ctx['users'] = loan_users
        ctx['mess'] = UpdateMessage
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class UserLoanApplicationViewDisbursedAll(generic.ListView):
    template_name = "admin/UserAccounts/userloan_applications_list_new_disbursed.html"
    title = ' User Loan Application'

    def get(self, request, date):
        userreject = []
        loan_users = []
        loan_user = {}
        ctx = {}
        UpdateMessage = ""

        try:

            loan_user = LoanApplication.objects.filter(created_date__startswith=date,
                                                       status__in=["APPROVED", "COMPLETED"],
                                                       disbursed_amount_wallet__status="loan_disbursed")

        except Exception as e:
            # print("-----------------exc", e, e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            pass
        ctx['users'] = loan_user
        ctx['mess'] = UpdateMessage
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/ecom/login/")], name='dispatch')
class CreateLoanBanner(generic.ListView):
    """
       This class is for Adding banner...
    """
    template_name = "admin/UserAccounts/create_loan_banner.html"

    def get(self, request, *args, **kwargs):
        ctx = {}
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        user = request.user
        try:
            if user:
                if request.method == "POST":
                    image = request.FILES['banner_image']
                    status = request.POST['status']
                    type = request.POST['type']
                    priority = request.POST['priority']
                    start_date = request.POST['start_date']
                    end_date = request.POST['end_date']
                    description = request.POST['description']
                    banner_type = request.POST['banner_type']
                    base_path = settings.BASE_DIR + "/media/loanbanner/"
                    file_path = "loanbanner/"
                    new_base_path = base_path + str(image)

                    new_file_path = file_path + str(image)

                    LoanBanner.objects.create(path=image, status=status, type=type, description=description,
                                              priority=priority, start_date=start_date, end_date=end_date,
                                              banner_type=banner_type,
                                              created_by=user)
                    upload_image(new_base_path, new_file_path)
        except:
            pass

        return redirect('/loan-app-data/loan_banner_product_list/')


@login_required(login_url="/ecom/login/")
def loan_banner_product_list(request):
    """
        This function is for product list...
    """
    ctx = {}
    data_dict = {}
    banner_obj = LoanBanner.objects.all()
    if banner_obj:
        for banner in banner_obj:
            banner_path = banner.path
            if banner_path:
                banner_image = botofile.get_url(banner_path)
            else:
                banner_image = None

            data_dict.update({banner: banner_image})

        ctx['products'] = data_dict
    return render(request, 'admin/UserAccounts/loan_banner_product_list.html', ctx)


@method_decorator([login_required(login_url="/ecom/login/")], name='dispatch')
class EditLoanBanner(generic.ListView):
    template_name = "admin/UserAccounts/edit_loan_banner.html"

    def get(self, request, id, *args, **kwargs):
        ctx = {}
        image = None
        banner_obj = LoanBanner.objects.filter(id=id)
        if banner_obj:
            image = banner_obj[0].path
            if image:
                image = botofile.get_url(image)

        ctx["path"] = image
        ctx['banner_obj'] = banner_obj[0]
        return render(request, self.template_name, ctx)

    def post(self, request, id, *args, **kwargs):
        user = request.user
        banner_obj = LoanBanner.objects.filter(id=id)
        if banner_obj:
            if request.method == "POST":
                image = request.FILES.get('banner_image')
                if image:
                    image = image
                    path = "loanbanner/" + str(image)
                else:
                    image = banner_obj[0].path
                    path = image

                status = request.POST['status']
                type = request.POST['type']
                priority = request.POST['priority']
                banner_type = request.POST['banner_type']

                start_date = request.POST['start_date']
                if start_date:
                    start_date = start_date
                else:
                    start_date = banner_obj[0].start_date

                end_date = request.POST['end_date']
                if end_date:
                    end_date = end_date
                else:
                    end_date = banner_obj[0].end_date

                description = request.POST['description']
                if description:
                    description = description
                else:
                    description = banner_obj[0].description

                # LoanBanner.objects.filter(id=id).update(status=status, type=type, path=path,
                #                                         priority=priority, start_date=start_date, end_date=end_date,
                #                                         banner_type=banner_type,
                #                                         updated_by=user, description=description,
                #                                         updated_date=datetime.datetime.now())
                loan_obj = LoanBanner.objects.filter(id=id)
                if loan_obj:
                    loan_obj[0].status = status
                    loan_obj[0].type = type
                    loan_obj[0].path = path
                    loan_obj[0].priority = priority
                    loan_obj[0].start_date = start_date
                    loan_obj[0].end_date = end_date
                    loan_obj[0].banner_type = banner_type
                    loan_obj[0].updated_by = user
                    loan_obj[0].description = description
                    loan_obj[0].updated_date = datetime.datetime.now()
                    loan_obj[0].save()
                # upload_image(new_base_path, new_file_path)
        return redirect('/loan-app-data/loan_banner_product_list/')


SmsStringMasterJsonData = apps.get_model('IcomeExpense', 'SmsStringMasterJsonData')
ContactMasterJsonData = apps.get_model('UserData', 'ContactMasterJsonData')
ContactMaster = apps.get_model('UserData', 'ContactMaster')


def contact_move_s3(request):
    l = 0
    date = ["2020-03"]
    import json
    username = ""
    # for da in date:
    users = User.objects.filter(username=username)
    users_c = User.objects.filter(username=username).count()
    if users:
        for user in users:
            contact_dict = {}
            l = l + 1
            con_list = ContactMaster.objects.filter(user_id=user.id)
            if con_list:
                for con in con_list:
                    con_l = {}
                    i = 0
                    count = ContactMaster.objects.filter(user_id=user.id, contact_no=con.contact_no).count()
                    if count > 1:
                        con_a = ContactMaster.objects.filter(user_id=user.id, contact_no=con.contact_no)
                        for c in con_a:
                            i = i + 1
                            if i >= 1:
                                contact_dict.update({c.contact_no: c.contact_name})
                                # pass
                            else:
                                print("else i $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$4$$$$$$$$$$$ ----------------",
                                      i,
                                      "conatct %%%%", c.contact_no)
                    else:
                        print("contact ----- ", count, "else part -------:wq!  -------", con.contact_no)

            json_name = "contact_" + str(user.username) + ".json"
            with open(json_name, 'w') as json_file:
                json.dump(contact_dict, json_file)

    return HttpResponse("ok")


def json_file_move(file_name):
    # file_name="contact_9960438638.json"

    new_file_path = "contact_list/" + str(file_name)
    base_path = settings.BASE_DIR + "/" + str(file_name)
    from MApi.botofile import upload_image
    upload_image(base_path, new_file_path)
    os.remove(file_name)
    return True


def loan_approved_entry(loan_application_id, status):
    # if loan_application_id and status:
    status_list = ["APPROVED", "COMPLETED"]
    if status in status_list:
        loan = ApprovedLoans.objects.create(loan_application_id=loan_application_id, status=status)

    return True


def monthwise_datewise_pandas_count(query, fieldnames, type):
    """
    parameter :  query,  fieldnames, type

    This function count month and year data from query.
    """
    data_dict = {}
    df = read_frame(query, fieldnames)
    if type == "month":
        df["YearMonth"] = [str(i)[:7] for i in df[fieldnames[0]]]

    else:
        df["YearMonth"] = [str(i)[:10] for i in df[fieldnames[0]]]
        # df['YearMonth'] = pd.to_datetime(df[fieldnames[0]]).dt.strftime("%Y-%m-%d")
    data_df = df.groupby(df["YearMonth"])[fieldnames[1]].count().sort_index(axis=0)
    data_dict = data_df.to_dict()
    return data_dict


class MonthwiseApproved(generic.ListView):
    template_name = "admin/UserAccounts/monthwise_approved_loan.html"

    def get(self, request, *args, **kwargs):
        data_dict = {}
        ctx = {}
        query = ApprovedLoans.objects.filter(status='APPROVED').order_by('created_date')
        fieldnames = ['created_date', 'id']
        data_dict = monthwise_datewise_pandas_count(query, fieldnames, "month")
        ctx['data'] = data_dict

        return render(request, self.template_name, ctx)


class DatewiseApproved(generic.ListView):
    template_name = "admin/UserAccounts/datewise_approved_loan.html"

    def get(self, request, date, *args, **kwargs):
        data_dict = {}
        ctx = {}

        query = ApprovedLoans.objects.filter(created_date__startswith=date, status='APPROVED').order_by(
            'created_date')
        fieldnames = ['created_date', 'id']
        data_dict = monthwise_datewise_pandas_count(query, fieldnames, "date")
        ctx['data'] = data_dict

        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class UserLoanApplicationApproved(generic.ListView):
    template_name = "admin/UserAccounts/userloan_application_approved.html"
    title = ' User Approved Loan Application'

    def get(self, request, date):
        userreject = []
        loan_users = []
        ctx = {}
        id_list = []
        UpdateMessage = ""

        try:

            loan_users = ApprovedLoans.objects.filter(created_date__startswith=date, status='APPROVED')
            if loan_users:
                for user_id in loan_users:
                    id_list.append(user_id.id)

        except Exception as e:
            # print("-----------------exc", e, e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            pass
        ctx["id_list"] = id_list
        ctx['users'] = loan_users
        ctx['mess'] = UpdateMessage
        return render(request, self.template_name, ctx)


def reports_page(request):
    # print('inside status')
    ctx = {}
    status = ['SUBMITTED', 'APPROVED', 'COMPLETED', 'CANCELLED', 'REJECTED', 'PENDING', 'ON_HOLD', 'CREDITVIDYA',
              'PRE_APPROVED', 'PROCESS']
    ctx['status'] = status
    return render(request, 'admin/dashboard/report.html', ctx)


def return_approved_user_csv(request, user_id):
    time = datetime.datetime.now()
    date_string = str(time)[:10]
    base_string = 'attachment; filename="'
    final_str = base_string + date_string + '.csv"'
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = final_str
    writer = csv.writer(response)
    writer.writerow(
        ["Name", "Created Date", "Loan Application Id", "Apply Amount", "Approved Amount",
         'Current Designation', 'User Name', 'Loan Start Date', 'Loan End Date'])
    user_id = user_id[1:-1]
    print("user_id", user_id)
    if len(user_id) == 1:
        user_obj = ApprovedLoans.objects.filter(id=int(user_id))
    else:
        user_id = user_id.split(",")
        user_id = [int(i) for i in user_id]
        user_obj = ApprovedLoans.objects.filter(id__in=user_id)
    if user_obj:
        for user in user_obj:
            writer.writerow(
                [user.loan_application.user_personal.name, user.created_date,
                 user.loan_application.loan_application_id,
                 user.loan_application.loan_scheme_id,
                 user.loan_application.approved_amount,
                 user.loan_application.user_professional.company_designation,
                 user.loan_application.user.username,
                 user.loan_application.loan_start_date,
                 user.loan_application.loan_end_date])
    return response


def user_data(request):
    data = {}
    bank_upd_data = {}
    response1 = HttpResponse(content_type='text/csv')
    response1['Content-Disposition'] = 'attachment; filename="user_data.csv"'
    writer = csv.writer(response1)
    writer.writerow(
        ['Mobile Number', 'Name', "Email Id", "City", "Pin code", "Last Loan Amount", "Address"])
    if "GET" == request.method:
        return render(request, "admin/UserAccounts/user_data_1.html", data)
    # if not GET, then proceed
    try:
        print('inside post function==========================')
        fields_verified = True
        message = ''
        csv_file = request.FILES["csv_file"]
        print('1111111111111111', csv_file)
        if not csv_file.name.endswith('.csv'):
            print('inside if===============')
            message = 'File is not CSV type'
            return render(request, "admin/UserAccounts/user_data_1.html", {"msg": message})
        file_data = csv_file.read().decode("utf-8")
        lines = file_data.split("\n")
        if lines:
            cnt = 0
            for line in lines:
                mobile_no = ""
                name = ""
                email_id = ""
                address = ""
                pin_code = ""
                city = ""
                last_loan_amount = 0
                if line:
                    line_1 = re.sub(',(?=[^"]*"[^"]*(?:"[^"]*"[^"]*)*$)', "", line)
                    print("line_1", line_1)
                    line_1.replace("\r", "")
                    fields = line_1.split(",")
                    if fields:
                        print('fields-----------------', len(fields), fields)
                        data_dict = {}
                        if cnt == 0:
                            if len(fields) != 1:
                                writer.writerow(
                                    ["", "", "Incorrect columns"])
                            else:
                                pass

                        cnt += 1
                        if fields and fields[0] != 'Mobile Number':
                            print('here===========================')
                            mobile_no = fields[0]

                            user_personal = Personal.objects.filter(user__username=mobile_no).order_by(
                                'id').last()
                            if user_personal:
                                name = user_personal.name
                                email_id = user_personal.email_id

                            address_obj = Address.objects.filter(user__username=mobile_no).order_by('id').last()
                            if address_obj:
                                address = address_obj.address
                                city = address_obj.city
                                pin_code = address_obj.pin_code

                            loan_obj = LoanApplication.objects.filter(user__username=mobile_no).order_by(
                                'id').last()
                            if loan_obj:
                                last_loan_amount = loan_obj.loan_scheme_id

                            writer.writerow(
                                [mobile_no, name, email_id, city, pin_code, last_loan_amount, address])

                        else:
                            message = "Something went wrong"
                            writer.writerow(
                                [mobile_no, name, email_id, city, pin_code, last_loan_amount, address])

    except Exception as e:
        logging.getLogger("error_logger").error("Unable to upload file. " + repr(e))
        print('-----------in exception lendbox_bulk_upload----------')
        print(e.args)
        import sys
        import os
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        messages.error(request, "Unable to upload file. " + repr(e))
        messages.error(request, 'File is not CSV type')
        pass
    return response1
