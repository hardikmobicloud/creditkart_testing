import datetime
import json
import time

from django.apps.registry import apps
from django.conf import settings
from django.contrib.auth.models import User
from celery import shared_task
import requests
from django.http import HttpResponse

import sys
import os

from mudra.constants import scheme
from mudra import constants

from Payments.tasks import rbl_worker

LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
AutoApproved = apps.get_model('LoanAppData', 'AutoApproved')
RBLTransaction = apps.get_model('Payments', 'RBLTransaction')


@shared_task
def loan_status(total_sms):
    loan = LoanApplication.objects.filter(loan_application_id=1).update(total_sms=total_sms, balance=total_sms)
    print("loan_status..............!")
    return "fdnkfndfkdkfdfd fdsfkdfdkfndskf sdkfdnskf sdfsdnfksdfdk"


@shared_task
def test_cahnge():
    print("test celery working .......... !")

    return True




def loan_application_dpd(user_id):
    Today_date = datetime.datetime.now()
    int_dbd_total = 0
    try:
        user = LoanApplication.objects.filter(user_id=user_id, status__in=['APPROVED', 'COMPLETED'])
        for users in user:
            payment_return = users.repayment_amount.filter(status='success').order_by('-id').first()

            if users.status == 'APPROVED':
                defaulted_by_date = Today_date - users.loan_end_date
                int_dbd = defaulted_by_date.days
                if int_dbd > 0:
                    int_dbd_total += int_dbd
            else:
                defaulted_by_date = payment_return.date - users.loan_end_date
                int_dbd = defaulted_by_date.days
                if int_dbd > 0:
                    int_dbd_total += int_dbd

    except:
        pass
    return int_dbd_total










import logging

logger = logging.getLogger(__name__)


@shared_task
def user_application_auto_approve(user_id):
    # print("------In user_application_auto_approve()---------")
    logging.info("------In user_application_auto_approve()---------")

    user_auto_id = LoanApplication.objects.filter(status="PRE_APPROVED", user_id=user_id)
    if user_auto_id:
        logger.debug("Loan Status SUBMITTED")
        dpd = loan_application_dpd(user_auto_id[0].user_id)
        logger.debug("User dpd: {}".format(dpd))
        if dpd <= 0:
            # print('dpd is <=0')
            auto_user = AutoApproved.objects.filter(status=True, user_id=user_id)
            if auto_user:
                logger.debug("User auto_user: {}".format(auto_user))
                if auto_user[0].limit_amount:
                    logger.debug("User auto app limit_amount: {}".format(auto_user[0].limit_amount))
                    if int(user_auto_id[0].loan_scheme_id) == int(auto_user[0].limit_amount):
                        AutoApproved.objects.filter(status=True, user_id=user_id).update(status=False)
                        LoanApplication.objects.filter(id=user_auto_id[0].id, status="PRE_APPROVED",
                                                       user_id=user_id).update(status="APPROVED")

                    if int(user_auto_id[0].loan_scheme_id) < int(auto_user[0].limit_amount):
                        LoanApplication.objects.filter(id=user_auto_id[0].id, status="PRE_APPROVED",
                                                       user_id=user_id).update(status="APPROVED")

                        auto_approved = AutoApproved.objects.filter(user_id=user_auto_id[0].user_id).order_by(
                            '-id').first()
                        amount = ['1000', '2000', '3000', '5000', '10000', '15000', '25000', '50000',
                                  '100000', '2500000', '5000000']
                        if auto_approved:
                            p = amount.index(str(auto_approved.option_limit))
                            m = p + 1
                            if int(user_auto_id[0].loan_scheme_id) == int(auto_approved.option_limit):
                                AutoApproved.objects.filter(user_id=user_auto_id[0].user_id,
                                                            status=True).update(
                                    status=False
                                )
                                AutoApproved.objects.create(user_id=user_auto_id[0].user_id,
                                                            limit_amount=auto_approved.limit_amount,
                                                            option_limit=amount[m])

                    loan_approved = LoanApplication.objects.filter(id=user_auto_id[0].id, status="APPROVED",
                                                                   user_id=user_id)
                    # print("++_+_ Loan app", loan_approved)
                    if loan_approved:

                        txn_rbl_instance = RBLTransaction.objects.filter(
                            transaction_id_text=user_auto_id[0].loan_application_id
                        )

                        if not txn_rbl_instance:
                            try:


                                rbl_worker.delay(loan_approved[0].loan_application_id)


                            except Exception as e:

                                import sys
                                import os
                                print('-----------in exception----------')
                                print(e.args)
                                exc_type, exc_obj, exc_tb = sys.exc_info()
                                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                                print(exc_type, fname, exc_tb.tb_lineno)

                                import requests
                                sms_message = 'Error 1 ' + str(e.args) + ' \n' + str(exc_tb.tb_lineno)
                                number = '6352820504'

                                sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
                                    settings.SMS_USERNAME) + '&password=' + str(settings.SMS_PASSWORD) + \
                                          '&type=0&dlr=1&destination=' + number + '&source=' + str(
                                    settings.SMS_SOURCE) + '&message=' + sms_message

                                # requests.get(sms_url)

                                return False

        if dpd > 0:
            # print(dpd)
            AutoApproved.objects.filter(user_id=user_id, status=True).update(
                status=False
            )
            AutoApproved.objects.create(user_id=user_id,
                                        option_limit=1000, status=False)
        # print("code successfully run")
    return True


# @shared_task
# def loan_status(total_sms):
#     loan = LoanApplication.objects.filter(loan_application_id=1).update(total_sms=total_sms, balance=total_sms)
#     print("loan_status..............!")
#     return "fdnkfndfkdkfdfd fdsfkdfdkfndskf sdkfdnskf sdfsdnfksdfdk"

from MApi.payment_auth import generate_checksum_by_str
from MApi.paytm_invoice import paytm_post_data, save_payment_link_data


@shared_task
def paytm_create_link_api(ctx):
    '''
    https://developer.paytm.com/docs/create-link-api/

    This API will be used to create new payment links
    It can be used to create all 3 types of links
    The merchant also has an option to send the link to a customer via SMS/Email
    :param request:
    :return:

    {
        "head": {
            "timestamp": "xxxxxxxxxxxxxxxx",
            "clientId": "C11",
            "channelId": "WEB",
            "version": "v1",
            "tokenType": "xxx",
            "signature": "xxxxxxxxxxxxxxxxxxxxxxxx"
        },
        "body": {
                "merchantRequestId": "xxxxxxxxxxxxxxx",
                "notificationDetails": [
                    {
                        "notifyStatus": "SENT",
                        "timestamp": "09/04/2019 16:06:23",
                        "contact": "xxxxxxxxx",
                        "customerName": "Paytm Test"
                    },
                    {
                        "notifyStatus": "SENT",
                        "timestamp": "09/04/2019 16:06:23",
                        "contact": "xxxxxxxxxxxxx",
                        "customerName": "Paytm Test"
                    }
                ],
                "merchantHtml": '<a href=https://securegw-stage.paytm.in/link/ProTest/LL_99 rel="im-checkout" data-behaviour="remote" data-style="light" data-text="Checkout With Paytm"></a>',
                "expiryDate": "Wed Apr 10 00:00:00 IST 2019",
                "linkType": "FIXED",
                "resultInfo": {
                    "resultStatus": "SUCCESS",
                    "resultCode": "200",
                    "resultMessage": "Payment link is created successfully"
                },
                "linkId": 99,
                "isActive": "True",
                "amount": 100,
                "createdDate": 1554806161950,
                "longUrl": "https://securegw-stage.paytm.in/link/ProTest/LL_99"
            }
        }
    '''

    TestMid = 'QKOugQ73327246032029'
    TestMkey = 'q&x_OIsoDqFQnXry'

    MERCHANT_MID = TestMid
    MERCHANT_KEY = TestMkey
    merchant_request_id = 1
    # customer_name = 'keval'
    cust_email = 'keval.thakkar@mudrakwik.com'
    cust_number = '7875583679'
    customer_name = 'keval'
    # cust_number = '9664810577'
    linkname = "test"  # Description of the link that merchant wants to display to customer
    amount = 1
    expiryDate = "10/04/2020"
    isActive = "true"
    sendSms = "true"
    sendEmail = "true"
    # sendSms = "false"
    # sendEmail = "false"
    link_discription = "this is only testing link"
    # Callback URL which will be used by Paytm to post the status of the transaction post payment
    paytmParams = {}
    timestamp = int(time.time())
    # "statusCallbackUrl": "",

    # dynamic data
    cust_email = 'keval.thakkar@mudrakwik.com'
    cust_number = '7875583679'
    customer_name = ctx['customer_name']
    linkname = ctx['link_name']  # Description of the link that merchant wants to display to customer
    link_discription = ctx['link_description']
    amount = ctx['amount']
    expiryDate = ctx['expiry_date']
    isActive = ctx['is_active']
    sendSms = ctx['send_sms']
    sendEmail = ctx['send_email']
    # sendSms = "false"
    # sendEmail = "false"

    paytmParams["body"] = {
        "merchantRequestId": merchant_request_id,  # Unique ID to be generated by merchant.
        "mid": MERCHANT_MID,
        "linkName": linkname,
        "linkDescription": link_discription,
        "linkType": "FIXED",
        "amount": amount,
        "expiryDate": expiryDate,
        "isActive": isActive,
        "sendSms": sendSms,
        "sendEmail": sendEmail,
        "customerContact": {
            "customerName": customer_name,
            "customerEmail": cust_email,
            "customerMobile": cust_number
        }
    }
    # print(paytmParams)
    # print("------")
    checksum = generate_checksum_by_str(json.dumps(paytmParams["body"]), MERCHANT_KEY)
    #
    # paytmParams["head"] = {
    #     "timestamp": str(timestamp),
    #     "version": "v1",
    #     "channelId": "WEB",
    #     "tokenType": "AES",
    #     "signature": checksum
    # }
    #
    # post_data = json.dumps(paytmParams)
    post_data = paytm_post_data(paytmParams["body"], checksum)
    # print("====Post Body====")
    # print(post_data)
    # print("====Post Body end====")

    # for staging
    staging_url = "https://securegw-stage.paytm.in/link/create"
    # for production
    production_url = "https://securegw.paytm.in/link/create"

    response = requests.post(url=staging_url, data=post_data, headers={"Content-type": "application/json"})
    # print("################Response@#@#")
    # print(response)
    # print("################Response@#@#")
    # print(response.request)
    # print(response.status_code)
    # print("________________++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    # print("________________++++++++++++++++saving data++++++++++++++++++++++++++++++++++++++")
    json_data = json.loads(response.content.decode())
    response_body = json_data['body']
    save_payment_link_data(response_body, ctx)
    # print("________________++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    # print("________________++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    return HttpResponse(response)











