from Payments.models import Payment
from User.models import Address, Reference, Bank, Documents, Professional, Personal
from UserData.models import DeviceData, CibilData
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from mudra import constants,ecom_constants
from ecom.models import UserWallet
# Create your models here.
class LoanApplication(models.Model):
    loan_application_id = models.CharField(max_length=256)
    status = models.CharField(db_index=True, max_length=50, choices=settings.LOAN_APPLICATION_STATUS, blank=True,
                              null=True,
                              default='SUBMITTED')
    loan_start_date = models.DateTimeField(db_index=True, null=True, blank=True)
    loan_end_date = models.DateTimeField(db_index=True, null=True, blank=True)
    device_id = models.CharField(null=True, blank=True, max_length=50)
    device_model = models.CharField(null=True, blank=True, max_length=100)
    location = models.CharField(null=True, blank=True, max_length=256)
    reason = models.CharField(null=True, blank=True, max_length=256)
    verified_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True, related_name="verified_by")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_application")
    user_professional = models.ForeignKey(Professional, on_delete=models.CASCADE, related_name="user_professional",
                                          null=True, blank=True)
    user_personal = models.ForeignKey(Personal, on_delete=models.CASCADE, related_name="user_personal", null=True,
                                      blank=True)
    verified_date = models.DateTimeField(null=True, blank=True)
    rec_status = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, related_name="user_by")
    pancard_id = models.ForeignKey(Documents, on_delete=models.CASCADE, blank=True, null=True, related_name="pancard")
    aadhar_id = models.ForeignKey(Documents, on_delete=models.CASCADE, blank=True, null=True, related_name="aadhar")
    bank_sts_id = models.ForeignKey(Documents, on_delete=models.CASCADE, blank=True, null=True,
                                    related_name="bank_statement")
    salary_slip_id = models.ForeignKey(Documents, on_delete=models.CASCADE, blank=True, null=True,
                                       related_name="salary_slip_id")
    company_id = models.ForeignKey(Documents, on_delete=models.CASCADE, blank=True, null=True,
                                   related_name="company_id")
    self_video = models.ForeignKey(Documents, on_delete=models.CASCADE, blank=True, null=True,
                                   related_name="self_video")
    cur_address_id = models.ForeignKey(Documents, on_delete=models.CASCADE, blank=True, null=True,
                                       related_name="current_address_document")
    per_address_id = models.ForeignKey(Documents, on_delete=models.CASCADE, blank=True, null=True,
                                       related_name="permanent_address_documnet")
    current_address = models.ForeignKey(Address, on_delete=models.CASCADE, blank=True, null=True,
                                        related_name="current_address")
    permanent_address = models.ForeignKey(Address, on_delete=models.CASCADE, blank=True, null=True,
                                          related_name="permanent_address")
    reference_id = models.ForeignKey(Reference, on_delete=models.CASCADE, related_name="reference_details", blank=True,
                                     null=True)
    bank_id = models.ForeignKey(Bank, on_delete=models.CASCADE, related_name="bank_details", blank=True, null=True)
    loan_scheme_id = models.CharField(db_index=True, max_length=100, choices=settings.LOAN_SCHEME)
    cibil_id = models.ForeignKey(CibilData, on_delete=models.CASCADE, blank=True, null=True,
                                 related_name="cibil_details")
    device_data_id = models.ForeignKey(DeviceData, on_delete=models.CASCADE, blank=True, null=True,
                                       related_name="device_data")
    disbursed_amount = models.ManyToManyField(Payment, blank=True, null=True,
                                              related_name="disbursed_amount")
    disbursed_amount_wallet = models.ForeignKey(UserWallet, on_delete=models.CASCADE, blank=True, null=True,
                                                related_name="disbursed_amount_wallet")
    repayment_amount = models.ManyToManyField(Payment, blank=True, null=True,
                                              related_name="repayment_amount")
    # loan application processing dat
    overdue = models.CharField(db_index=True, blank=True, null=True, max_length=100)
    dpd = models.CharField(db_index=True, blank=True, null=True, max_length=100)
    total_sms = models.CharField(blank=True, null=True, max_length=100)
    balance = models.CharField(db_index=True, blank=True, null=True, max_length=100)
    hold_days = models.IntegerField(db_index=True, default=0, blank=True, null=True)
    purpose_loan = models.CharField(choices=constants.PURPOSE_LOAN, max_length=256, blank=True, null=True)
    approved_amount = models.IntegerField(null=True, blank=True)
    user_status = models.CharField(db_index=True, max_length=50, blank=True, null=True)
    aadhar_front = models.ForeignKey(Documents, on_delete=models.CASCADE, blank=True, null=True,
                                     related_name="aadhar_front")
    aadhar_back = models.ForeignKey(Documents, on_delete=models.CASCADE, blank=True, null=True,
                                    related_name="aadhar_back")
    def __str__(self):
        return 'Loan Application id {}'.format(self.loan_application_id)


class LoanAcknowledgement(models.Model):
    """
    To store user loan acknowledgement
    """
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, related_name='user_ack')
    loan_transaction = models.ForeignKey(LoanApplication, on_delete=models.CASCADE, blank=True, null=True,
                                         related_name='user_loan_transaction')
    acknowledgement_pdf = models.FileField(upload_to='loan_applications', blank=True, null=True, max_length=256)
    sign = models.CharField(db_index=True, blank=True, null=True, max_length=100)


class OverdueReport(models.Model):
    date = models.DateField(null=False)
    overdue_count = models.IntegerField(null=True, blank=True)
    defaulter_count = models.IntegerField(null=True, blank=True)
    recovery_overdue_count = models.IntegerField(null=True, blank=True)
    recovery_defaulter_count = models.IntegerField(null=True, blank=True)
    status = models.CharField(null=True, blank=True, max_length=10)
    create_date = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    class Meta:
        ordering = ['-id']


status = (
    ('True', 'True'),
    ('False', 'False'),
)


class AutoApproved(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, related_name='user_auto_approved')
    limit_amount = models.IntegerField(db_index=True, null=True, blank=True)
    status = models.CharField(db_index=True, default=True, max_length=10, choices=status)
    created_date = models.DateTimeField(db_index=True, auto_now=True)
    option_limit = models.IntegerField(db_index=True, default=1000, null=False, blank=False)
    verified_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE,
                                    related_name='user_verified_by')

    class Meta:
        ordering = ['-id']


# Razorpay Models

class RazorpayCustomer(models.Model):
    personal = models.ForeignKey(Personal, null=True, blank=True, on_delete=models.CASCADE,
                                 related_name="razorpay_customer")
    customer_id = models.CharField(max_length=250, null=True, blank=True, db_index=True)
    fail_existing = models.CharField(max_length=250, null=True, blank=True, db_index=True)
    response = models.TextField(null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)


class RazorpayOrder(models.Model):
    customer_id = models.ForeignKey(RazorpayCustomer, null=True, blank=True, on_delete=models.CASCADE,
                                    related_name="razorpay_customer")
    order_id = models.CharField(db_index=True, max_length=200, null=True, blank=True)
    transaction_id = models.CharField(db_index=True, max_length=200, null=True, blank=True)
    pg_transaction_id = models.CharField(max_length=50, null=True, blank=True)
    amount = models.FloatField(db_index=True, max_length=50, null=True, blank=True)
    bank = models.ForeignKey(Bank, null=True, blank=True, on_delete=models.CASCADE,
                             related_name="razorpay_bank_details")
    payment = models.ForeignKey(Payment, null=True, blank=True, on_delete=models.CASCADE,
                                related_name="payment_details")
    type = models.CharField(db_index=True, max_length=50, null=True, blank=True, choices=constants.RAZORPAY_ORDER_TYPE)
    response = models.TextField(null=True, blank=True)
    auth_type = models.CharField(db_index=True, max_length=50, null=True, blank=True,
                                 choices=constants.RAZORPAY_AUTH_TYPE)
    status = models.CharField(db_index=True, max_length=50, null=True, blank=True, choices=constants.STATUS_TYPE)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)


class RazorpayToken(models.Model):
    razorpay_order = models.ForeignKey(RazorpayOrder, null=True, blank=True, on_delete=models.CASCADE,
                                       related_name="razorpay_order")
    token_id = models.CharField(max_length=250, null=True, blank=True, db_index=True)
    token = models.CharField(max_length=250, null=True, blank=True, db_index=True)
    expiry_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    re_status = models.BooleanField(default=True)
    response = models.TextField(null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    status = models.CharField(db_index=True, max_length=50, null=True, blank=True, choices=constants.ENACH_STATUS_TYPE)







class LoanBanner(models.Model):
    """
        This table is for LoanBanner..
    """
    path = models.ImageField(upload_to='loanbanner', max_length=256, null=True, blank=True, db_index=True)
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    status = models.BooleanField(default=True)
    banner_type = models.CharField(max_length=25, default="loan", null=True, blank=True, choices=ecom_constants.BANNER_TYPE_L)
    type = models.CharField(max_length=25, null=True, blank=True, choices=ecom_constants.OFFER_BANNER_TYPE)
    priority = models.IntegerField(default=None, null=True, blank=True)
    description = models.CharField(max_length=256, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="loan_banner_created_by",
                                   null=True,
                                   blank=True)
    updated_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="loan_banner_updated_by",
                                   null=True,
                                   blank=True)

class ApprovedLoans(models.Model):
    """
    This model is used to save the date of approved Loans and Completed Loans..
    """
    loan_application = models.ForeignKey(LoanApplication, on_delete=models.CASCADE, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    status = models.CharField(max_length=30, null=True, blank=True)


