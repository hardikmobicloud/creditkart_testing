# Register your models here.

from django.contrib import admin
from .models import *
from django.conf import settings


# @admin.register(LoanApplication)
class LoanApplicationAdmin(admin.ModelAdmin):
    change_list_template = 'admin/UserAccounts/loan_profile_change_list.html'
    # form = LoanApplyLoanAdminForm
    change_form_template = 'admin/UserAccounts/loan_add_form.html'
    list_filter = ('status', 'user_status', 'loan_scheme_id', 'approved_amount')
    list_display = (
        'loan_application_id',
        'user', 'created_date', 'updated_date', 'loan_start_date', 'loan_end_date', 'loan_scheme_id', 'approved_amount',
        'status', 'user_status', 'reason', 'salary_slip_id', 'company_id', 'self_video', 'current_address',
        'permanent_address',
        'pancard_id', 'user_personal', 'user_professional', 'hold_days'
    )
    ordering = ('created_date', 'status', 'loan_start_date', 'loan_end_date', 'approved_amount',)
    search_fields = ('user__username', 'loan_application_id', 'approved_amount',)

    # def rejected_loan(self, obj):
    #     try:
    #         user_loan_app = UserLoanTransaction.objects.get(id=obj)
    #         user = user_loan_app.user
    #         user_rejected = UserLoanTransaction.objects.filter(user=user, status='REJECTED')
    #         return user_rejected.count()
    #     except:
    #
    #         return 0
    #
    # def change_view(self, request, object_id, form_url='', extra_context=None):
    #     extra_context = extra_context or {}
    #     extra_context['rejected_loan'] = self.rejected_loan(object_id)
    #     return super().change_view(
    #         request, object_id, form_url, extra_context=extra_context,
    #     )
    #
    # def created_dates(self, obj):
    #     try:
    #         return obj.created_date
    #     except:
    #         return ''
    #
    # created_dates.short_description = 'Loan application date'
    #
    # def error_display(self, obj):
    #
    #     return ", ".join([
    #         tnx.error_description for tnx in obj.rbl_trn_id.all()
    #     ])
    #
    # error_display.short_description = "Reason"
    #
    # def save_model(self, request, obj, form, change):
    #
    #     if not obj.loan_application_id:
    #         obj.loan_application_id = randint(111111, 999999995)
    #
    #     if not obj.status:
    #         obj.status = 'SUBMITTED'
    #
    #     try:
    #
    #         user_loan_profile_doc = LoanDocuments.objects.filter(user=obj.user).first()
    #         user_loan_pro_details = LoanProfessionalDetails.objects.filter(user=obj.user).first()
    # loan_scheme_id
    #         if user_loan_pro_details:
    #
    #             if not obj.id_card_photo:
    #                 obj.id_card_photo = user_loan_pro_details.id_card_photo
    #
    #         if user_loan_profile_doc:
    #
    #             if not obj.pan_card:
    #                 obj.pan_card = user_loan_profile_doc.pan_card
    #
    #             if not obj.salary_slip:
    #                 obj.salary_slip = user_loan_profile_doc.salary_slip
    #
    #             if not obj.balanced_sheet:
    #                 obj.balanced_sheet = user_loan_profile_doc.balanced_sheet
    #
    #             if not obj.residence_address_proof:
    #                 obj.residence_address_proof = user_loan_profile_doc.residence_address_proof
    #
    #             if not obj.shop_establishment_certification:
    #                 obj.shop_establishment_certification = user_loan_profile_doc.shop_establishment_certification
    #
    #             if not obj.bank_statement:
    #                 obj.bank_statement = user_loan_profile_doc.bank_statement
    #
    #             if not obj.adhar_card:
    #                 obj.adhar_card = user_loan_profile_doc.adhar_card
    #
    #     except:
    #
    #         pass
    #
    #     # super(UserLoanTransactionAdmin, self).save_model(request, obj, form, change)
    #
    #     loan_obj_email = UserLoanTransaction.objects.get(loan_application_id=obj.loan_application_id)
    #     user_instance = User.objects.get(id=loan_obj_email.user.id)
    #     # print(user_instance)
    #
    #     if obj.occupation == 'STUDENT':
    #         student_instance = LoanStudentDetails.objects.get(user=loan_obj_email.user)
    #         email_id = student_instance.email_address
    #
    #     elif obj.occupation == 'HOUSE_WIFE':
    #         house_wife_instance = LoanHouseWifeDetails.objects.get(user=loan_obj_email.user)
    #         email_id = house_wife_instance.email_address
    #     else:
    #         email_id = loan_obj_email.work_email_id
    #
    #     if obj.status == loan_obj_email.status:
    #         pass
    #
    #     else:
    #
    #         if obj.status == 'APPROVED':
    #
    #             # check txn id has already in rbl
    #
    #             txn_rbl_instance = RBLTransaction.objects.filter(
    #                 transaction_id_text=obj.loan_application_id
    #             )
    #
    #             if not txn_rbl_instance:
    #
    #                 rbl_payment(obj.loan_application_id)
    #
    #                 messages.set_level(request, messages.SUCCESS)
    #                 messages.success(request, "Loan submitted successfully")
    #
    #             else:
    #
    #                 messages.set_level(request, messages.ERROR)
    #                 messages.error(request, "Loan is already transferred for "+obj.loan_application_id)
    #
    #                 return None
    #
    #
    #         elif obj.status == 'CANCELLED':
    #
    #             # Email code
    #
    #             cancelled_date = datetime.datetime.now()
    #
    #             email_kwargs = {
    #                 'subject': 'MudraKwik - Loan Cancelled',
    #                 'to_email': email_id,
    #                 'email_template': 'emails/loan/cancelled_loan.html',
    #                 'data': {
    #                     'domain': Site.objects.get_current(),
    #                     'loan_instance': loan_obj_email,
    #                     'cancelled_date': cancelled_date,
    #                 },
    #             }
    #
    #             email(**email_kwargs)
    #             sms_message = "We regret to inform you that your loan application number " + loan_obj_email.loan_application_id + "  has been cancelled.  \nFor more details, please contact us on 8956039999 or email us on hello@mudrakwik.com "
    #
    #             sms_kwrgs = {
    #                 'sms_message': sms_message,
    #                 'number': str(loan_obj_email.user)
    #             }
    #
    #             sms(**sms_kwrgs)
    #
    #             fmp_kwargs = {
    #                 'user_id': user_instance.id,
    #                 'title': 'Loan Cancelled',
    #                 'message': sms_message,
    #                 'type': 'loan_cancelled',
    #                 'to_store': True,
    #                 'notification_id': obj.id,
    #                 'notification_key': 'loan_id',
    #             }
    #
    #             fcm_parameter_notification(**fmp_kwargs)
    #
    #         elif obj.status == 'REJECTED':
    #
    #             rejection_date = datetime.datetime.now()
    #
    #             email_kwargs = {
    #                 'subject': 'MudraKwik - Loan Rejected',
    #                 'to_email': email_id,
    #                 'email_template': 'emails/loan/rejected_loan.html',
    #                 'data': {
    #                     'domain': Site.objects.get_current(),
    #                     'loan_instance': loan_obj_email,
    #                     'rejection_date': rejection_date,
    #                 },
    #             }
    #
    #             email(**email_kwargs)
    #             sms_message = "We regret to inform you that your loan application number " + loan_obj_email.loan_application_id + "  has been rejected.  \nFor more details, please contact us on 8956039999 or email us on hello@mudrakwik.com "
    #
    #             sms_kwrgs = {
    #                 'sms_message': sms_message,
    #                 'number': str(loan_obj_email.user)
    #             }
    #
    #             sms(**sms_kwrgs)
    #
    #             fmp_kwargs = {
    #                 'user_id': user_instance.id,
    #                 'title': 'Loan Rejected',
    #                 'message': sms_message,
    #                 'type': 'loan_rejected',
    #                 'to_store': True,
    #                 'notification_id': obj.id,
    #                 'notification_key': 'loan_id',
    #             }
    #
    #             fcm_parameter_notification(**fmp_kwargs)
    #
    #         elif obj.status == 'PENDING':
    #
    #             email_kwargs = {
    #                 'subject': 'MudraKwik - Loan Pending',
    #                 'to_email': email_id,
    #                 'email_template': 'emails/loan/pending_loan.html',
    #                 'data': {
    #                     'domain': Site.objects.get_current(),
    #                     'loan_instance': loan_obj_email,
    #                 },
    #             }
    #
    #             email(**email_kwargs)
    #
    #             sms_message = "We regret to inform you that your loan application number " + loan_obj_email.loan_application_id + " is pending.  \nFor more details, please contact us on 8956039999 or email us on hello@mudrakwik.com "
    #
    #             sms_kwrgs = {
    #                 'sms_message': sms_message,
    #                 'number': str(loan_obj_email.user)
    #             }
    #
    #             sms(**sms_kwrgs)
    #
    #             fmp_kwargs = {
    #                 'user_id': user_instance.id,
    #                 'title': 'Loan Pending',
    #                 'message': sms_message,
    #                 'type': 'loan_pending',
    #                 'to_store': True,
    #                 'notification_id': obj.id,
    #                 'notification_key': 'loan_id',
    #             }
    #
    #             fcm_parameter_notification(**fmp_kwargs)
    #
    #         elif obj.status == 'COMPLETED':
    #
    #             sms_message = "We are happy to inform you that your loan application number " + loan_obj_email.loan_application_id + " is completed.  \nFor more details, please contact us on 8956039999 or email us on hello@mudrakwik.com "
    #
    #             fmp_kwargs = {
    #                 'user_id': user_instance.id,
    #                 'title': 'Loan Completed',
    #                 'message': sms_message,
    #                 'type': 'loan_completed',
    #                 'to_store': True,
    #                 'notification_id': obj.id,
    #                 'notification_key': 'loan_id',
    #             }
    #
    #             fcm_parameter_notification(**fmp_kwargs)
    #
    #     super(UserLoanTransactionAdmin, self).save_model(request, obj, form, change)


class AutoApprovedAdmin(admin.ModelAdmin):
    list_display = (
        'user', 'limit_amount', 'status', 'option_limit',
        'created_date',
    )
    search_fields = ('user__username', 'limit_amount', 'status', 'option_limit',)
    list_filter = ('user', 'limit_amount', 'status', 'option_limit',)


class OverdueReportAdmin(admin.ModelAdmin):
    list_display = (
        'date', 'overdue_count', 'defaulter_count', 'recovery_overdue_count', 'recovery_overdue_count', 'status',

    )
    list_filter = ('overdue_count', 'recovery_overdue_count', 'recovery_overdue_count', 'status', 'date',)


class RazorpayCustomerAdmin(admin.ModelAdmin):
    list_display = (
        'personal', 'customer_id', 'fail_existing', 'response', 'created_date', 'updated_date',

    )
    list_filter = ('personal', 'customer_id', 'fail_existing', 'response', 'created_date', 'updated_date')

    search_fields = ('customer_id', 'personal',)


class RazorpayOrderAdmin(admin.ModelAdmin):
    list_display = (
        'customer_id', 'order_id', 'transaction_id', 'pg_transaction_id', 'amount', 'bank', 'payment', 'type',
        'response', 'auth_type', 'status', 'created_date', 'updated_date',

    )
    list_filter = (
        'customer_id', 'order_id', 'transaction_id', 'pg_transaction_id', 'amount', 'bank', 'payment', 'type',
        'response',
        'auth_type', 'status', 'created_date', 'updated_date',
    )

    search_fields = ('order_id', 'transaction_id', 'pg_transaction_id', 'type')


class RazorpayTokenAdmin(admin.ModelAdmin):
    list_display = (
        'razorpay_order', 'token_id', 'token', 'expiry_date', 're_status', 'response', 'created_date', 'updated_date',
        'status',

    )
    list_filter = (
        'razorpay_order', 'token_id', 'token', 'expiry_date', 're_status', 'response', 'created_date', 'updated_date',
        'status',

    )

    search_fields = ('token_id', 'token', 'expiry_date', 'status')


class LoanBannerAdmin(admin.ModelAdmin):
    list_display = ('path', 'start_date', 'end_date', 'type', 'priority', 'status', 'created_date')
    list_filter = ('type', 'status', 'priority')


class ApprovedLoansAdmin(admin.ModelAdmin):
    list_display = (
        'loan_application', 'created_date', 'updated_date', 'status'
    )
    list_filter = ['status']


admin.site.register(LoanBanner, LoanBannerAdmin)

admin.site.register(RazorpayCustomer, RazorpayCustomerAdmin)
admin.site.register(RazorpayOrder, RazorpayOrderAdmin)
admin.site.register(RazorpayToken, RazorpayTokenAdmin)
admin.site.register(LoanApplication, LoanApplicationAdmin)
admin.site.register(AutoApproved, AutoApprovedAdmin)
admin.site.register(OverdueReport, OverdueReportAdmin)
admin.site.register(ApprovedLoans, ApprovedLoansAdmin)
