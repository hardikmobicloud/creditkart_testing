from django.apps import AppConfig


class LoanappdataConfig(AppConfig):
    name = 'LoanAppData'
