from django.urls import path

from .views import *
from .loan_funcations import *

urlpatterns = [

    # Reports
    path("user_data/", user_data, name="user_data"),
    path(r'defaulter-list/', getDefaulterList.as_view(), name='defaulter_list'),
    path(r'over_due_list/', getOverDueList.as_view(), name='over_due_list'),
    path(r'get_over_due_csv/', get_over_due_csv, name='get_over_due_csv'),
    path(r'get_total_due_csv/', get_total_due_csv, name='get_total_due_csv'),
    path(r'over_due_actual_list/', getActualOverDueList.as_view(), name='over_due_actual_list'),

    path(r'loan_on_hold_report/', loan_on_hold_report, name='loan_on_hold_report'),
    path(r'loan_pending_report/', loan_pending_report, name='loan_pending_report'),
    path(r'total_clients/', Total_Clients.as_view(), name='total_clients'),

    # path('get_all_active_user/', AllActiveUserView.as_view(), name='get_all_active_user'),

    path('get_all_active_user/<date>/', AllActiveUserView.as_view(), name='get_all_active_user'),
    path('month_wise_active_user/', All_Active_Monthwise.as_view(), name='month_wise_active_user'),
    path('date_wise_active_user/<month>/', All_Active_Datewise.as_view(), name='date_wise_active_user'),

    path(r'pincode_report_city/', Pincode_Report_City.as_view(), name='pincode_report_city/'),
    path(r'pin/', get_pincodewise_analysis_overdue_defaulter, name='pin'),
    path(r'payment_analysys/<on_date>/', AnalysysByPaymentStrategy.as_view(), name='payment_analysys'),
    path(r'monthly_payment_analysys/', MonthlyAnalysysByPaymentStrategy.as_view(), name='monthly_payment_analysys'),
    path(r'daywise_payment_analysys/<on_month>/', DaywiseAnalysysByPaymentStrategy.as_view(),
         name='daywise_payment_analysys'),
    path('defaulter_messages/', defaulter_messages, name='defaulter_messages'),

    # Task
    path(r'rbl_save_data/', rbl_save_data, name='rbl_save_data'),
    path(r'rbl_save_data_process/', rbl_save_data_process, name='rbl_save_data'),
    path(r'rbl_save_data_pre_approved/', rbl_save_data_pre_approved, name='rbl_save_data'),
    path(r'userloan-application-list1/<UpdateMessage>/', UserLoanApplicationView.as_view(),
         name='userloan_application_list1'),

    # test
    path(r'loan_test/', loan_test, name='loan_test'),
    path(r'paytm_send_link/', paytm_send_link, name='paytm_send_link'),

    # search view
    path(r'search_lid/', search_lid, name='search_lid'),
    path(r'SearchLoanApplication/', SearchLoanApplication.as_view(), name='SearchLoanApplication'),

    # loan change status
    # path(r'loan_app_status_change/', loan_app_status_change, name='loan_app_status_change'),

    path(r'test/', test, name='test'),

    # Loan Details monthwise
    path(r'loans-monthwise/', LoanDetailsMonthwise.as_view(), name='loans_monthwise'),
    path(r'loans-datewise/<on_month>/', LoanDetailsDatewise.as_view(), name='loans_datewise'),

    # Overdue Report
    path(r'overdue-data/', OverdueView, name='overdue_view'),
    path(r'monthwise-overdue/', MonthwiseOverdueData.as_view(), name='monthwise_overdue_data'),
    path(r'datewise-overdue/<on_month>/', DatewiseOverdueData.as_view(), name='datewise_overdue_data'),
    # rbl not data

    # Loan Disbursed Repayment Report
    path(r'disbursed-monthwise/', LoanDisbursedRepaymentMonthwise.as_view(), name='disbursed_monthwise'),
    path(r'disbursed-datewise/<on_month>/', LoanDisbursedRepaymentDatewise.as_view(), name='disbursed_datewise'),
    path(r'disbursed-by-date/<on_date>/', DatewiseDisbursmentView.as_view(), name='disbursed_by_date'),
    path(r'repayment-by-date/<on_date>/', DatewiseRepaymentView.as_view(), name='repayment_by_date'),

    path(r'loans-monthwise/', LoanDetailsMonthwise.as_view(), name='loans_monthwise'),
    path(r'loans-datewise/<on_month>/', LoanDetailsDatewise.as_view(), name='loans_datewise'),

    path(r'UndisbursedLoanMontheDetails/', UndisbursedLoanMontheDetails.as_view(), name='UndisbursedLoanMontheDetails'),

    # Loan Status Details
    path(r'loan-status-details/', LoanStatusDetails.as_view(), name='loan-status-details'),
    path(r'daywise-loan-status-details/<on_month>/', DaywiseLoanStatusDetails.as_view(),
         name='daywise-loan-status-details'),
    path(r'approved-user-details/<on_date>/', ApprovedUserDetails.as_view(), name='approved-user-details'),
    path(r'onhold-user-details/<on_date>/', OnholdUserDetails.as_view(), name='approved-user-details'),
    path(r'completed-user-details/<on_date>/', CompletedUserDetails.as_view(), name='approved-user-details'),
    path(r'cancelled-user-details/<on_date>/', CancelledUserDetails.as_view(), name='approved-user-details'),
    path(r'submitted-user-details/<on_date>/', SubmittedUserDetails.as_view(), name='approved-user-details'),

    path('csv/', upload_csv, name='upload_csv'),
    path('upload_csv_re/', upload_csv_repayment, name='upload_csv_repayment'),
    path('upload_csv_delete/', upload_csv_delete, name='upload_csv_delete'),
    path('upload_csv_bank/', upload_csv_bank, name='upload_csv_bank'),

    # path('no-payment-linked/', no_payment_linked, name='no_payment_linked'),
    path('no-payment-linked/', migration, name='no_payment_linked'),

    # csv download for sub and reg
    path('sub_reg_fordate/', sub_and_reg_datepicker.as_view(), name='sub_reg_fordate'),
    path('sub_reg_csvreport/', sub_and_reg_csvreport.as_view(), name='sub_reg_csvreport'),
    # Auto status change
    path(r'select-status/', SelectStatus.as_view(), name='select_status'),
    path(r'status-change/', StatusChange.as_view(), name='report-by-date'),
    path(r'download-csv/', CsvDownload.as_view(), name='Download_CSV'),
    # path(r'hii/', getTransactionAmount),
    path(r'hii/', getlastamount),
    path(r'different/', differentdates),

    path(r'extra-payment/', ExtraPayment.as_view(), name='extra_payment'),
    path(r'extra-payment-csv/', complete_approve_loans, name='extra_payment_csv'),
    path(r'order-with-m-csv/', order_with_m, name='order-with-m-csv'),
    path(r'less-payments/', less_payment_loans),

    # To check loans repayments by mobile search.
    path(r'search-mobile/', mobilesearch),
    path('loans-by-mobile/', LoansByMobile.as_view(), name='loans_by_mobile'),
    path('random-loans/', random_loans, name='random_loans'),
    path('chk-mail/', check_mail, name='check_mail'),

    # -----------------------------
    path(r'onhold_activestatus_count/', ActiveOnholdMonthwise.as_view(), name='onhold_activestatus_count'),
    path('date_wise_active_onholduser/<month>/', ActiveOnholdDatewise.as_view(), name='date_wise_active_user'),
    path('onhold_active_user_perday/<date>/', onhold_active_user_perday.as_view(), name='onhold_active_user_perday'),

    # numbers for pan count
    path("pan_count_view/<pan>/", pan_count_view.as_view(), name="pan_count_view"),

    # test paytm and rpay count
    path(r'paytm_rpay/', PaymentPtmRpaycount.as_view(), name='paytm_rpay'),
    path(r'paytm_rpay_details/<date>/', PaymentCountDetailsTestRazr.as_view(), name='paytm_rpay_details'),
    path(r'paytm_rpay_details_p/<date>/', PaymentCountDetailsTestPaytm.as_view(), name='paytm_rpay_details'),

    path(r'defaulter-list/', getDefaulterList.as_view(), name='defaulter_list'),
    path(r'over_due_list/', getOverDueList.as_view(), name='over_due_list'),
    path(r'defaulter-list-csv/', CsvgetDefaulterList),
    path(r'over_due_list-csv/', CsvgetOverDueList),
    path("loan_status/", loan_status),
    path("get_all_loans_data/", get_all_loans_data),
    path("get_all_loan_with_status/", get_all_loan_with_status, name="get_all_loan_with_status"),

    #User wise report
    path('user_wise_loan_details/', user_wise_loan_details),
    path('users_loan_details/<username>/', users_loan_details),

    # search
    path('search_loan/', SearchLoan.as_view()),

    #Userwise withdraw report
    path('user_wise_withdraw_loan/', user_wise_withdraw_loan),
    path('users_withdraw_loan_details/<username>/', users_withdraw_loan_details),
    path('users_withdraw_payment_details/<loan_application_id>/', users_withdraw_payment_details),

    # path('defaulter_messages_new', defaulter_messages_scheduler),
    path("recurring_amount", recurring_amount),

    path('user_loan_agreement/<loan_application_id>/', UserLoanAgreement.as_view()),
    path("user_loan_repayment/", user_loan_repayment),
    path("user_loan_repayment_data/<date>/", user_loan_repayment_data),

    path('search_loanapplication_data/', SearchLoanApplicationData.as_view()),
    path('search_loan_application/', search_loan_application),

    path('month_wise_disbursed_repayment/', month_wise_disbursed_repayment),
    path('date_wise_disbursed_repayment/<date>/', date_wise_disbursed_repayment),
    path('disbursed_data/<date>/<disbursed_id_list>/', disbursed_data),
    path('repayment_data/<date>/<repayment_id_list>/', repayment_data),

    # Loan banner
    path('create_loan_banner/', CreateLoanBanner.as_view()),
    path('loan_banner_product_list/', loan_banner_product_list),
    path('edit_loan_banner/<id>/', EditLoanBanner.as_view()),
    path('delay_days/', delay_days,name="delay_days"),

    #monthwise approved loan

    path("monthwise_approved_data/", MonthwiseApproved.as_view(), name="monthwise_approved_data"),
    path("datewise_approved_data/<date>/", DatewiseApproved.as_view(), name="datewise_approved_data"),
    path("userloan_application_approved/<slug:date>/", UserLoanApplicationApproved.as_view(), name="userloan_application_approved"),
    path('return_approved_user_csv/<user_id>/', return_approved_user_csv),

    # path("user_data_information/<date>/", user_data_information, name="user_data_information"),
    path("reports/", reports_page),

]
