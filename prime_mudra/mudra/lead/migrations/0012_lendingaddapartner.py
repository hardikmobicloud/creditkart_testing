# Generated by Django 2.2.4 on 2020-01-10 09:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('lead', '0011_auto_20200107_1159'),
    ]

    operations = [
        migrations.CreateModel(
            name='LendingAddaPartner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('click_id', models.CharField(blank=True, max_length=256, null=True)),
                ('click_date', models.DateTimeField(blank=True, null=True)),
                ('status', models.BooleanField(default=False)),
                ('is_subscribed', models.BooleanField(default=False)),
                ('amount', models.FloatField(default=20)),
                ('paid_for_leads', models.BooleanField(default=False)),
                ('payment_date', models.DateField(null=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
                ('lead_id', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='lendingadda_partnerleads', to='lead.PartnerLeads')),
            ],
        ),
    ]
