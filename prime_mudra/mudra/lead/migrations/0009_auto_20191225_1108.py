# Generated by Django 2.2.4 on 2019-12-25 11:08

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('lead', '0008_auto_20191224_1654'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='YeloPather',
            new_name='YeloPartner',
        ),
        migrations.RemoveField(
            model_name='affiliates',
            name='clicks',
        ),
        migrations.AddField(
            model_name='partnerleads',
            name='click_count',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
