# Generated by Django 2.2.4 on 2019-11-28 19:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lead', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LendingAdda',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mobile', models.CharField(max_length=13)),
                ('click_id', models.CharField(blank=True, max_length=256, null=True)),
                ('click_date', models.DateField(blank=True, null=True)),
                ('status', models.BooleanField(default=False)),
                ('is_subscribed', models.BooleanField(default=False)),
                ('is_approved', models.BooleanField(default=False)),
                ('paidToLendingAdda', models.IntegerField(default=300)),
                ('payment_date', models.DateField(null=True)),
                ('is_paidForLeads', models.BooleanField(default=False)),
                ('sign', models.CharField(blank=True, max_length=256, null=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
            ],
        ),
    ]
