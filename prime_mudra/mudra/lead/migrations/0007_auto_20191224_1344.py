# Generated by Django 2.2.4 on 2019-12-24 13:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ('lead', '0006_tecdoreports'),
    ]

    operations = [
        migrations.CreateModel(
            name='Affiliates',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('base_url', models.CharField(max_length=250)),
                ('sub_url', models.CharField(max_length=250)),
                ('type', models.CharField(max_length=250)),
                ('status', models.BooleanField(default=False)),
                ('clicks', models.IntegerField(blank=True, max_length=250, null=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Partners',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('partner_name', models.CharField(max_length=100)),
                ('status', models.BooleanField(default=False)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='YeloPather',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('click_id', models.CharField(blank=True, max_length=256, null=True)),
                ('click_date', models.DateTimeField(blank=True, null=True)),
                ('status', models.BooleanField(default=False)),
                ('is_subscribed', models.BooleanField(default=False)),
                ('amount', models.FloatField(default=13)),
                ('paid_for_leads', models.BooleanField(default=False)),
                ('payment_date', models.DateField(null=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='PatnerLeads',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mobile_number', models.CharField(max_length=250)),
                ('status', models.BooleanField(default=False)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
                ('aff_id', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE,
                                             related_name='aff', to='lead.Affiliates')),
            ],
        ),
        migrations.AddField(
            model_name='affiliates',
            name='patner_id',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE,
                                    related_name='patner', to='lead.Partners'),
        ),
    ]
