# Generated by Django 2.2.4 on 2020-01-07 11:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lead', '0010_yelopartner_lead_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='affiliates',
            name='type',
            field=models.CharField(blank=True, max_length=250, null=True),
        ),
    ]
