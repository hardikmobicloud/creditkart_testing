from django.contrib import admin
from .models import *


# Register your models here.
class YeloLeadsAdmin(admin.ModelAdmin):
    list_display = (
        'mobile', 'click_id', 'click_date', 'status', 'is_subscribed', 'paidToYelo', 'payment_date', 'is_paidForLeads',
        'created_date', 'updated_date',)


class LendingAddaAdmin(admin.ModelAdmin):
    list_display = (
        'mobile', 'click_id', 'click_date', 'status', 'is_subscribed', 'paidToLendingAdda', 'payment_date',
        'is_paidForLeads',
        'created_date', 'updated_date',)


class TecdoReportsAdmin(admin.ModelAdmin):
    list_display = (
        'mobile', 'click_count', 'paidToTecdo', 'payment_date', 'is_paidForLeads',
        'created_date', 'updated_date',)


class PartnersAdmin(admin.ModelAdmin):
    list_display = ('partner_name', 'created_date', 'status')
    list_filter = ('partner_name', 'status', 'created_date')
    search_fields = ('partner_name', 'status')


class AffiliatesAdmin(admin.ModelAdmin):
    list_display = ('base_url', 'sub_url', 'type', 'partner_id', 'created_date', 'status')
    list_filter = ('partner_id__partner_name', 'partner_id', 'sub_url', 'status')
    search_fields = ('partner_id__partner_name', 'partner_id', 'sub_url', 'status')


class PartnerLeadsAdmin(admin.ModelAdmin):
    # list_display = ('mobile_number', 'mobile_number', 'mobile_number', 'aff_id', 'get_partner')
    list_display = ('mobile_number', 'aff_id', 'created_date', 'status')
    list_filter = ('mobile_number', 'aff_id', 'created_date', 'status')
    # search_fields = ('mobile_number', 'aff_id')
    # search_fields = (
    #     'mobile_number', 'aff_id', 'aff_id_partner_id', 'aff_id_partner_id__partner_name', 'status',
    #     'created_date',)
    search_fields = (
        'mobile_number', 'status',
        'created_date',)


class LendingAddaPartnerAdmin(admin.ModelAdmin):
    # list_display = ('mobile_number', 'mobile_number', 'mobile_number', 'aff_id', 'get_partner')
    list_display = (
        'click_id', 'click_date', 'status', 'is_subscribed', 'amount', 'paid_for_leads', 'payment_date', 'lead_id',
        'af_id')
    list_filter = ('lead_id', 'status', 'is_subscribed')
    # search_fields = ('mobile_number', 'aff_id')
    # search_fields = (
    #     'mobile_number', 'aff_id', 'aff_id_partner_id', 'aff_id_partner_id__partner_name', 'status',
    #     'created_date',)
    search_fields = ('lead_id', 'status', 'is_subscribed')


class DynamicDisbursementAdmin(admin.ModelAdmin):
    list_display = (
        'click_id', 'click_id', 'is_approved', 'amount', 'payment_date', 'af_id', 'lead_id', 'created_date', 'status')
    list_filter = ('click_id', 'is_approved', 'af_id', 'lead_id', 'created_date', 'status')
    search_fields = ('click_id', 'is_approved', 'af_id', 'lead_id', 'status')


class AssociatesUserAdmin(admin.ModelAdmin):
    list_display = ('mobile_no', 're_status')
    list_filter = ['re_status']
    search_fields = ['mobile_no']


admin.site.register(AssociatesUser, AssociatesUserAdmin)

admin.site.register(DynamicDisbursement, DynamicDisbursementAdmin)
admin.site.register(LendingAddaPartner, LendingAddaPartnerAdmin)
admin.site.register(TecdoReports, TecdoReportsAdmin)
admin.site.register(LendingAdda, LendingAddaAdmin)
admin.site.register(Partners, PartnersAdmin)
admin.site.register(Affiliates, AffiliatesAdmin)
admin.site.register(PartnerLeads, PartnerLeadsAdmin)
admin.site.register(YeloPartner)
