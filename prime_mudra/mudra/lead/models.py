from django.db import models


class YeloLeads(models.Model):
    """
    To store data of Leads from Yelo
    """
    mobile = models.CharField(max_length=13)
    click_id = models.CharField(max_length=256, null=True, blank=True)
    click_date = models.DateTimeField(null=True, blank=True)
    status = models.BooleanField(default=False)
    is_subscribed = models.BooleanField(default=False)
    paidToYelo = models.FloatField(default=13)
    payment_date = models.DateField(null=True)
    is_paidForLeads = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)


class LendingAdda(models.Model):
    """
    To store data of Leads from lending adda
    """
    mobile = models.CharField(max_length=13)
    click_id = models.CharField(max_length=256, null=True, blank=True)
    click_date = models.DateTimeField(null=True, blank=True)
    status = models.BooleanField(default=False)
    is_subscribed = models.BooleanField(default=False)
    is_approved = models.BooleanField(default=False)
    paidToLendingAdda = models.IntegerField(default=300)
    payment_date = models.DateTimeField(null=True)
    is_paidForLeads = models.BooleanField(default=False)
    sign = models.CharField(max_length=256, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)


class TecdoLeads(models.Model):
    """
    To store data of Leads from Tecdo
    """
    mobile = models.CharField(max_length=13)
    click_id = models.CharField(max_length=256, null=True, blank=True)
    click_date = models.DateTimeField(null=True, blank=True)
    status = models.BooleanField(default=False)

    is_subscribed = models.BooleanField(default=False)
    paidToTecdo = models.IntegerField(default=13)
    payment_date = models.DateField(null=True)
    is_paidForLeads = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)


class TecdoReports(models.Model):
    mobile = models.CharField(max_length=13)
    click_count = models.CharField(max_length=13, null=True, blank=True)
    paidToTecdo = models.IntegerField(default=13)
    payment_date = models.DateField(null=True)
    is_paidForLeads = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)


# Dynamic Link

class Partners(models.Model):
    """
    All lead partners
    """
    partner_name = models.CharField(max_length=100)
    status = models.BooleanField(default=False)

    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)

    is_disbursed = models.BooleanField(default=False)

    def __str__(self):
        return self.partner_name


class Affiliates(models.Model):
    """
        Stores Paters urls, type , status
    """
    base_url = models.CharField(max_length=250)
    sub_url = models.CharField(max_length=250)
    type = models.CharField(max_length=250, null=True, blank=True)
    status = models.BooleanField(default=False)
    partner_id = models.ForeignKey(Partners, on_delete=models.CASCADE, blank=True, null=True,
                                   related_name="partner")
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return '%s %s' % (self.base_url, self.sub_url)


class PartnerLeads(models.Model):
    """
        Stores Paters leads
    """
    mobile_number = models.CharField(max_length=250)
    status = models.BooleanField(default=False)
    click_count = models.IntegerField(blank=True, null=True)
    aff_id = models.ForeignKey(Affiliates, on_delete=models.CASCADE, blank=True, null=True,
                               related_name="aff")
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return '%s %s' % (self.mobile_number, self.aff_id.sub_url)


# Dynamic link end

class YeloPartner(models.Model):
    """
    To store data of Leads from Yelo
    """
    click_id = models.CharField(max_length=256, null=True, blank=True)
    # from aff table
    click_date = models.DateTimeField(null=True, blank=True)
    status = models.BooleanField(default=False)
    is_subscribed = models.BooleanField(default=False)
    amount = models.FloatField(default=13)
    paid_for_leads = models.BooleanField(default=False)
    payment_date = models.DateField(null=True)
    lead_id = models.ForeignKey(PartnerLeads, on_delete=models.CASCADE, blank=True, null=True,
                                related_name="partnerleads")
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)


class LendingAddaPartner(models.Model):
    """
    To store data of Leads from LendingAdda
    """
    click_id = models.CharField(max_length=256, null=True, blank=True)
    # from aff table
    click_date = models.DateTimeField(null=True, blank=True)
    status = models.BooleanField(default=False)
    is_subscribed = models.BooleanField(default=False)
    amount = models.FloatField(default=20)
    paid_for_leads = models.BooleanField(default=False)
    payment_date = models.DateField(null=True)
    lead_id = models.ForeignKey(PartnerLeads, on_delete=models.CASCADE, blank=True, null=True,
                                related_name="lendingadda_partnerleads")
    af_id = models.ForeignKey(Affiliates, on_delete=models.CASCADE, blank=True, null=True,
                              related_name="partner_affiliates")
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)


class DynamicDisbursement(models.Model):
    click_id = models.CharField(max_length=256, null=True, blank=True)
    # from aff table
    click_date = models.DateTimeField(null=True, blank=True)
    status = models.BooleanField(default=False)
    is_approved = models.BooleanField(default=False)
    amount = models.FloatField(default=20)
    paid_for_leads = models.BooleanField(default=False)
    payment_date = models.DateField(null=True)
    lead_id = models.ForeignKey(PartnerLeads, on_delete=models.CASCADE, blank=True, null=True,
                                related_name="dynamic_partnerleads")
    af_id = models.ForeignKey(Affiliates, on_delete=models.CASCADE, blank=True, null=True,
                              related_name="dynamic_partner_affiliates")
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)


class SMS_Campaign_Data(models.Model):
    """
    To store data upload from csv file of sms campaign
    """
    date = models.DateTimeField(null=True, blank=True)
    number = models.CharField(max_length=250, null=True, blank=True)
    is_subscribed = models.BooleanField(default=False)
    status = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)


class AssociatesUser(models.Model):
    """
    To store users mobile number..
    """
    mobile_no = models.CharField(max_length=20, null=True, blank=True)
    re_status = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)