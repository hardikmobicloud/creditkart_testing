# Register your models here.
from .models import *
from django.contrib import admin


class SMtpSettingsAdmin(admin.ModelAdmin):
    list_display = ('smtp_username','smtp_password','default_from_email','email_host','email_port','from_email_address')



class SMSSettingsAdmin(admin.ModelAdmin):
    list_display = ('username','password','sms_url')


class AdminMasterMessagesAdmin(admin.ModelAdmin):
    list_display = ('date_created','terms_and_conditions')


class AppVersionCheckAdmin(admin.ModelAdmin):
    list_display = ('version_name','version_code','status')



class PincodeMasterAdmin(admin.ModelAdmin):
    list_display = ('pincode','taluka','city','state')


class MobileBlockAdmin(admin.ModelAdmin):
    list_display = ('mobile_no', 'pan_no', 'ck', 'mk', 'rk', 'type')
    list_filter = ('mobile_no', 'pan_no')
    search_fields = ['mobile_no', 'pan_no']


admin.site.register(PincodeMaster,PincodeMasterAdmin)
admin.site.register(SMTPSettings,SMtpSettingsAdmin)
admin.site.register(SMSSettings,SMSSettingsAdmin)
admin.site.register(AdminMasterMessages,AdminMasterMessagesAdmin)
admin.site.register(AppVersionCheck,AppVersionCheckAdmin)
admin.site.register(MobileBlock, MobileBlockAdmin)
