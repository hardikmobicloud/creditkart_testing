from django.db import models
from mudra import ecom_constants



class PincodeMaster(models.Model):
    """
    End user profile model
    """

    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(null=True, blank=True)
    pincode = models.CharField(max_length=10, blank=True, null=True, verbose_name='pincode')
    taluka = models.CharField(db_index=True,max_length=256, blank=True, null=True, verbose_name='taluka')
    city = models.CharField(db_index=True,max_length=256, blank=True, null=True, verbose_name='city')
    state = models.CharField(db_index=True,max_length=256, blank=True, null=True, verbose_name='state')

    class Meta:
        ordering = ['pincode']


class AppVersionCheck(models.Model):
    """To check APK Version"""
    # app_id=models.CharField(null=False,max_length=250)
    version_name = models.CharField(null=False, max_length=50)
    version_code = models.CharField(null=False, max_length=50)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    status = models.CharField(default="Active", max_length=10)

    class Meta:
        ordering = ['-id']






class SMTPSettings(models.Model):

    """
    To store SMTP settings
    """

    smtp_username = models.CharField(max_length=255)
    smtp_password = models.CharField(max_length=255)
    default_from_email = models.CharField(max_length=255)
    email_host = models.CharField(max_length=255)
    email_port = models.IntegerField()
    from_email_address = models.CharField(max_length=50)


class SMSSettings(models.Model):

    """
    To store SMS settings
    """

    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    sms_url = models.CharField(max_length=255)



class AdminMasterMessages(models.Model):

    """
    To store different message for admin
    """

    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True, null=True, blank=True)

    terms_and_conditions = models.TextField(blank=True, null=True)

    def __str__(self):

        if self.terms_and_conditions:

            return self.terms_and_conditions

        else:

            return ''




class MobileBlock(models.Model):
    """
    This model is used to block user from rk, ck, mk
    """
    mobile_no = models.CharField(max_length=20, null=True, blank=True, db_index=True)
    pan_no = models.CharField(max_length=50, null=True, blank=True)
    type = models.CharField(max_length=50, null=True, blank=True, choices=ecom_constants.MOBILE_BLOCK_TYPE)
    re_status = models.BooleanField(default=False)
    rk = models.BooleanField(default=False)
    mk = models.BooleanField(default=False)
    ck = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)







