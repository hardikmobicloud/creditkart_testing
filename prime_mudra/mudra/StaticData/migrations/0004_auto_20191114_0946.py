# Generated by Django 2.2.4 on 2019-11-14 09:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('StaticData', '0003_adminmastermessages_smssettings_smtpsettings'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pincodemaster',
            name='city',
            field=models.CharField(blank=True, db_index=True, max_length=256, null=True, verbose_name='city'),
        ),
        migrations.AlterField(
            model_name='pincodemaster',
            name='state',
            field=models.CharField(blank=True, db_index=True, max_length=256, null=True, verbose_name='state'),
        ),
        migrations.AlterField(
            model_name='pincodemaster',
            name='taluka',
            field=models.CharField(blank=True, db_index=True, max_length=256, null=True, verbose_name='taluka'),
        ),
    ]
