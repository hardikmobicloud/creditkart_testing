from django.apps import apps
MobileBlock = apps.get_model('StaticData', 'MobileBlock')
def check_mobile_block(mobile_no, type):
    """
        This function is to check mobile number is in block list.
    """
    if mobile_no and type:
        is_block = False
        mobile_obj = MobileBlock.objects.filter(mobile_no=mobile_no)
        if mobile_obj:
            for mobile in mobile_obj:
                if type == "mk":
                    if mobile.mk == True:
                        is_block = True
                elif type == "rk":
                    if mobile.rk == True:
                        is_block = True
                elif type == 'ck':
                    if mobile.ck == True:
                        is_block = True
            if is_block == True:
                return True
            else:
                return False
        else:
            return False
    else:
        return False

def check_pan_block(pan_no, type):
    """
        This function is to check pan number is in block list.
    """
    if pan_no and type:
        is_block = False
        pan_obj = MobileBlock.objects.filter(pan_no=pan_no)
        if pan_obj:
            for pan in pan_obj:
                if type == "mk":
                    if pan.mk == True:
                        is_block = True
                elif type == "rk":
                    if pan.rk == True:
                        is_block = True
                elif type == 'ck':
                    if pan.ck == True:
                        is_block = True
            if is_block == True:
                return True
            else:
                return False
        else:
            return False
    else:
        return False