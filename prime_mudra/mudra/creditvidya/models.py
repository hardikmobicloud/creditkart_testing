from django.db import models
from django.contrib.auth.models import User
from django.apps import apps
# LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
from LoanAppData.models import LoanApplication


class CreditVidyaSMS(models.Model):
    sms_id = models.CharField(max_length=100, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    message = models.CharField(max_length=256, null=True, blank=True)
    status = models.CharField(max_length=100, blank=True, null=True)
    sms_create_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, blank=True, null=True)


class CreditVidyaAPP(models.Model):
    app_id = models.CharField(max_length=100, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    message = models.CharField(max_length=256, null=True, blank=True)
    status = models.CharField(max_length=100, blank=True, null=True)
    app_create_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, blank=True, null=True)


class CreditVidyaDeviceData(models.Model):
    """
        Records device data send for user
    """
    app_id = models.CharField(max_length=100, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    message = models.CharField(max_length=256, null=True, blank=True)
    status = models.CharField(max_length=100, blank=True, null=True)
    app_create_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, blank=True, null=True)


class CreditVidyaLocation(models.Model):
    """
        Records Location send for user
    """
    app_id = models.CharField(max_length=100, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    message = models.CharField(max_length=256, null=True, blank=True)
    status = models.CharField(max_length=100, blank=True, null=True)
    app_create_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, blank=True, null=True)


class CreditVidyaContacts(models.Model):
    """
        Records Contacts data send for user
    """
    app_id = models.CharField(max_length=100, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    message = models.CharField(max_length=256, null=True, blank=True)
    status = models.CharField(max_length=100, blank=True, null=True)
    app_create_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, blank=True, null=True)


class CreditVidyaApplication(models.Model):
    loan_app = models.ForeignKey(LoanApplication, on_delete=models.CASCADE, blank=True, null=True)
    loan_application_id = models.CharField(max_length=100, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    status = models.CharField(max_length=50, blank=True, null=True)
    errorcode = models.CharField(max_length=100, blank=True, null=True)
    errormessage = models.CharField(max_length=256, blank=True, null=True)
    reason = models.CharField(max_length=256, blank=True, null=True)
    message = models.CharField(max_length=256, blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, blank=True, null=True)


class RegisterUser(models.Model):
    mobile_number = models.CharField(max_length=15, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    auth_token = models.CharField(max_length=500, blank=True, null=True)
    refresh_token = models.CharField(max_length=500, blank=True, null=True)
    loan_application_id = models.CharField(max_length=50, blank=True, null=True)
    url = models.CharField(max_length=500, blank=True, null=True)
    message = models.CharField(max_length=256, null=True, blank=True)
    status = models.CharField(max_length=100, blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return '%s' % (self.user)