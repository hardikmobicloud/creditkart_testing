
from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from django.conf import settings
# set the default Django settings module for the 'celery' program.
#os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mudra.settings')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mudra.production_settings')
app = Celery('mudra'
             ,backend='redis://localhost:6379',
             broker='redis://localhost:6379')

app.config_from_object('django.conf:settings')

app.conf.update(
             #CELERY_ACCEPT_CONTENT = ['json'],
             #CELERY_TASK_SERIALIZER = 'json',
             #CELERY_RESULT_SERIALIZER = 'json',
             #celery
             C_FORCE_ROOT="yes", )

# Load task modules from all registered Django app configs.
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))