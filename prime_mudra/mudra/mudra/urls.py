from django.contrib import admin
from django.conf.urls import url
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from Dashboard.sites import SitePlus
from LoanAppData.views import LoanApplicationView, UserAllDetails, DefaulterAllDetails, reject_loan_application, UserWiseLoanDetailsView, \
    UserParticularLoanWiseDetailsView, UserReferanceView, Pincode_Report, UserLoanApplicationView, \
    LoanApplicationViewNew, UndisbursedLoanDetails, LoanApplicationPreApprovedView, LoanApplicationProcessView, \
    LoanApplicationPreApprovedViewNew, LoanApplicationProcessViewNew, UserAllDetailsProcess, UserAllDetailsPreApproved,LoanApplicationViewAll,UserLoanApplicationViewAll,MonthwiseLoanApplicationViewAll,MonthwiseLoanApplicationViewDisbursedAll,LoanApplicationViewDisbursedAll,UserLoanApplicationViewDisbursedAll
from User.views import SalaryWise_Report_All_User
from Payments.views import IncomeExpenseSubscriptionByDate, IncomeExpenseSubscriptionUserByDate
from Recovery.views import ContactDetails, forword_name, DefaultersRecoveredByDate_view, CallReminder, \
    recoverydata_on_month_view, \
    UserParticularLoanWiseCallHistoryView, DefaultersRecoveredByDate, UserParticularLoanWiseDetailsRecoveryView, \
    UserWiseLoanDetailsRecoveryView, UserAllDetails_Recovery, UserReferanceView_Recovery, recoverydata_on_month, \
    recoverydata_on_day1, expected_payment_on_date_bydefaulter, RecoveryByTeamOnDate, All_Day_wise_Record, DayRecovery, \
    RecoveryByTeamOnMonth, DefaultersRecoveredDatewise, IncentiveOnMonth, DayWiseCallDetails, CallDetails, \
    Admin_All_Day_wise_Record, Admin_All_Months_wise_Record, AdminDayRecovery, AdminRecoveryByTeamOnDate, \
    AdminRecoveryByTeamOnMonth, AdminUserAllDetails_Recovery, AdminDefaultersRecoveredDatewise, \
    AdminUserParticularLoanWiseDetailsRecoveryView, MonthwiseDefaultersAllocated, UserwiseDefaulters, \
    AmountWiseDefaulters, SelfRecovery, PerticularAmountwiseDefaulters

from MApi.logging import index
from mudra.log_settings import log_test
from Equfix.views import equfix_status_check
# import debug_toolbar

# from Equfix.views import equfix_status_check
from Payments.scheduler import *
from django.views.static import serve

admin.site = SitePlus()
admin.autodiscover()
admin.sites.site = admin.site
admin.autodiscover()

urlpatterns = [

    # Including Apps Urls
    # path('debug/', include(debug_toolbar.urls)),
    url(r'^media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', serve,{'document_root': settings.STATIC_ROOT}),

    path('dashboard/', include('Dashboard.urls')),
    path('automation/', include('automation.urls')),
    path('', admin.site.urls),
    path(r'recovery/', include('Recovery.urls')),
    path(r'equfix/', include('Equfix.urls')),
    path(r'crif/', include('crif.urls')),
    path(r'api/', include('MApi.urls')),
    path(r'loan-app-data/', include('LoanAppData.urls')),
    path(r'payments/', include('Payments.urls')),
    path(r'ecomapi/', include('EApi.urls')),
    path(r'icome-expense/', include('IcomeExpense.urls')),
    path('lendbox/', include('lendbox.urls')),
    path('insurance/', include('insurance.urls')),

    # LoanAppData
    path('monthwise_loanapplication_all/', MonthwiseLoanApplicationViewAll.as_view(), name='monthwise_loanapplication_all'),
    path('monthwise_loanapplication_all_disbursed/', MonthwiseLoanApplicationViewDisbursedAll.as_view(), name='monthwise_loanapplication_all_disbursed'),

    path(r'user_referance/<int:loan_application_id>/', UserReferanceView.as_view(),
         name='user_referanceLoanApplicationPreApprovedView'),
    path('userloan_application_all/<slug:date>/', UserLoanApplicationViewAll.as_view(), name='userloan_application_all'),
    path('userloan_application_all_disbursed/<slug:date>/', UserLoanApplicationViewDisbursedAll.as_view(), name='userloan_application_all_disbursed'),

    path('userloan-application-list/<slug:date>/', LoanApplicationView.as_view(), name='userloan_application_list'),
    path('userloan-application-list-process/<slug:date>/', LoanApplicationProcessView.as_view(),
         name='userloan_application_list_process'),
    path('userloan-application-list-pre-approved/<slug:date>/', LoanApplicationPreApprovedView.as_view(),
         name='userloan_application_list_pre_approved'),
    path('userloan_application_list_all/<slug:date>/', LoanApplicationViewAll.as_view(), name='userloan_application_list_all'),
    path('userloan_application_list_all_disbursed/<slug:date>/', LoanApplicationViewDisbursedAll.as_view(), name='userloan_application_list_all_disbursed'),

    path('userloan-application-list-new/', LoanApplicationViewNew.as_view(), name='userloan_application_list_new'),
    path('userloan-application-list-new-process/', LoanApplicationProcessViewNew.as_view(),
         name='userloan_application_list_new'),
    path('userloan-application-list-new-pre-approved/', LoanApplicationPreApprovedViewNew.as_view(),
         name='userloan_application_list_new'),
    path(r'user-all-details/<int:id>/', UserAllDetails.as_view(), name='user_all_details'),
    path(r'defaulter-all-details/<int:id>/', DefaulterAllDetails.as_view(), name='user_all_details'),
    path(r'user-all-details-process/<int:id>/', UserAllDetailsProcess.as_view(), name='user_all_details'),
    path(r'user-all-details-pre-approved/<int:id>/', UserAllDetailsPreApproved.as_view(), name='user_all_details'),

    # LoanAppData
    path(r'loan-app-data/', include('LoanAppData.urls')),
    path(r'userloan-application-list1/<slug:date>/', LoanApplicationView.as_view(),
         name='userloan_application_list1'),

    path(r'UndisbursedLoanDetails/<slug:date>/', UndisbursedLoanDetails.as_view(), name='UndisbursedLoanDetails'),

    # User
    path(r'user/', include('User.urls')),

    # User Data
    path(r'user-data/', include('UserData.urls')),

    # yelo
    path(r'lead/', include('lead.urls')),

    # Payments
    path(r'payment/', include('Payments.urls')),
    path(r'income_expense_payments/<on_month>/', IncomeExpenseSubscriptionByDate.as_view(),
         name='income_expense_payments'),
    path(r'income_expense_payments_user/<create_month>/', IncomeExpenseSubscriptionUserByDate.as_view(),
         name='income_expense_payments_user'),

    # report urls
    path(r'salary_wise__all_report/<income>/', SalaryWise_Report_All_User.as_view(),
         name='cibil_cibilwise_report'),

    # Recovery
    path(r'forword_name/<recover_id>/', forword_name.as_view(), name='forword_name'),
    path(r'contact_details/<file_name>/', ContactDetails.as_view(), name='contact_details'),
    path(r'defaulter_recovery_on_date/<on_day>/', DefaultersRecoveredByDate.as_view(),
         name='defaulter_recovery_on_date'),
    path(r'defaulter_recovery_on_date_view/<on_day>/', DefaultersRecoveredByDate_view.as_view(),
         name='defaulter_recovery_on_date'),
    path(r'SelfRecovery/', SelfRecovery.as_view(),
         name='SelfRecovery'),
    path(r'datewise_recovery_details/<on_day>/', RecoveryByTeamOnDate.as_view(),
         name='datewise_recovery_details'),
    path(r'datewise_recovery/<on_day>/', DefaultersRecoveredDatewise.as_view(),
         name='datewise_recovery'),
    path(r'call_reminder/<on_day>/', CallReminder.as_view(), name='call_reminder'),
    path(r'recoverydata_on_month_view/<year_month>/', recoverydata_on_month_view.as_view(),
         name='recoverydata_on_month_view'),
    path(r'user_particular_loanWise_call_history/<int:recovery_id>/', UserParticularLoanWiseCallHistoryView.as_view(),
         name='user_particular_loanWise_call_history'),
    path('recovery_user_particular_loanwise_details/<loan_application_id>/',
         UserParticularLoanWiseDetailsRecoveryView.as_view(),
         name='recovery_user_particular_loanwise_details'),
    path('recovery_user_wise_loan_details/<username>/', UserWiseLoanDetailsRecoveryView.as_view(),
         name='recovery_user_wise_loan_details'),
    path(r'recovery-user-all-details/<int:id>/', UserAllDetails_Recovery.as_view(), name='recovery_user_all_details'),

    path(r'recoverydata_on_month/<year_month>/', recoverydata_on_month.as_view(), name='recoverydata_on_month'),
    path(r'recoverydata_on_day/<on_day>/', recoverydata_on_day1.as_view(), name='recoverydatauser_on_day'),
    path(r'expected_payment_on_date_bydefaulter/<on_day>/', expected_payment_on_date_bydefaulter.as_view(),
         name='expected_payment_on_date_bydefaulter'),
    path(r'all_day_wise_report/<on_month>/', All_Day_wise_Record.as_view(),
         name='all_day_wise_report'),

    # staff analysis for admin
    path(r'all_day_wise_report_1/<on_month>/', Admin_All_Day_wise_Record.as_view(),
         name='admin_all_day_wise_report'),
    path(r'admin_month_wise_report/', Admin_All_Months_wise_Record.as_view(), name='admin_month_wise_report'),
    path(r'admin_day_recovery/<on_day>/', AdminDayRecovery.as_view(), name='admin_day_recovery'),
    path(r'admin_datewise_recovery_details/<on_day>/', AdminRecoveryByTeamOnDate.as_view(),
         name='admin_datewise_recovery_details'),
    path(r'admin_month_recovery_details/<on_month>/', AdminRecoveryByTeamOnMonth.as_view(),
         name='admin_month_recovery_details'),
    path(r'admin-recovery-user-all-details/<int:id>/', AdminUserAllDetails_Recovery.as_view(),
         name='admin_recovery_user_all_details'),
    path(r'admin_datewise_recovery/<on_day>/', AdminDefaultersRecoveredDatewise.as_view(),
         name='admin_datewise_recovery'),
    path('admin_recovery_user_particular_loanwise_details/<loan_application_id>/',
         AdminUserParticularLoanWiseDetailsRecoveryView.as_view(),
         name='admin_recovery_user_particular_loanwise_details'),

    path(r'month_recovery_details/<on_month>/', RecoveryByTeamOnMonth.as_view(),
         name='month_recovery_details'),
    path(r'Incentive_OnMonth/<on_month>/', IncentiveOnMonth.as_view(),
         name='Incentive_OnMonth'),
    path(r'day_call_details/<on_month>/', DayWiseCallDetails.as_view(),
         name='day_call_details'),

    # Monthwise Allotted defaulters to perticular user
    path(r'userwise-defaulters/', UserwiseDefaulters.as_view(),
         name='monthwise defaulters_allocated'),
    url(r'amountwise-defaulters/(?P<month>\w*)-(?P<day>\w*)/(?P<user>\w*)/$',
        AmountWiseDefaulters.as_view(), name='amount_wise_defaulters'),
    url(r'particular-amountwise-defaulters/(?P<amount>\w*)/(?P<month>\w*)-(?P<day>\w*)/(?P<user>\w*)/$',
        PerticularAmountwiseDefaulters.as_view(), name='particular-amountwise-defaulters'),
    path(r'monthwise_defaulters_allocated/<user>/', MonthwiseDefaultersAllocated.as_view(),
         name='monthwise defaulters_allocated'),

    # Recovery Functions
    path(r'call_history/<int:loan_application_id>/', CallDetails.as_view(),
         name='day_call_details'),
    path(r'day_recovery/<on_day>/', DayRecovery.as_view(),
         name='day_recovery'),
    path(r'recovery_user_referance/<int:loan_application_id>/', UserReferanceView_Recovery.as_view(),
         name='recovery_user_referance'),

    # loan other data
    path(r'reject_loan_application/<int:loan_application_id>/', reject_loan_application,
         name='reject_loan_application'),
    path('user_wise_loan_details/<username>/', UserWiseLoanDetailsView.as_view(), name='user_wise_loan_details'),
    path('user_particular_loanwise_details/<loan_application_id>/', UserParticularLoanWiseDetailsView.as_view(),
         name='user_particular_loanwise_details'),
    path(r'city_pin_code/<pin_code>/', Pincode_Report.as_view(), name='city_pin_code'),

    # Test
    path(r'log_test/', index, name='log_test'),
    path(r'logging_test/', log_test, name='logging_test'),

    # Equfix
    path('equfix_dy_API/', equfix_status_check, name='equfix_dy_API'),

    # Usersharedata
    # path(r'sharedata/', include('Usersharedata.urls')),

    # Customer Executive
    path(r'customer/', include('CustomerExecutive.urls')),
    path('ecom/', include('ecom.urls')),
    path('npci/', include('npci.urls')),


]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]


