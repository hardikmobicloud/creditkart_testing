from django.apps import apps
from django.contrib.auth.models import User

# from prime_mudra.mudra.User.models import Documents
AppVersionCheck = apps.get_model('StaticData', 'AppVersionCheck')
Personal = apps.get_model('User', 'Personal')
Bank = apps.get_model('User', 'Bank')
Address = apps.get_model('User', 'Address')
Reference = apps.get_model('User', 'Reference')
Document = apps.get_model('User', 'Documents')
Professional = apps.get_model('User', 'Professional')
Payment = apps.get_model('Payments', 'payment')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
LoanAcknowledgement = apps.get_model('LoanAppData', 'LoanAcknowledgement')
DeviceData = apps.get_model('UserData', 'DeviceData')

DOCUMENT_TYPE = (
    ('Aadhar', 'AA'),
    ('Pan', 'PN'),
    ('Company_Id', 'CI'),
    ('Self_Video', 'SV'),
)
Document = apps.get_model('User', 'Documents')


def populate_documents():
    print("--------Popolating Documents Data--------------")
    user = User.objects.all().first()
    print("USer", user)
    for i, j in DOCUMENT_TYPE:
        print("creating doc obj for", i)
        document = Document.objects.create(type=i, path="images/98c81b17-ae6.jpg", verify_date="2019-1-1",
                                           verify_by=user,
                                           is_verified="True", user_id="1")
        print(document)


# populate_documents()


def payment():
    print("--------Popolating Documents Data--------------")
    user_loan = LoanApplication.objects.filter(id=1)
    for i in user_loan:
            # get payments details (Many to many fields)
            payment = i.repayment_amount_set.all()
            if payment:
                print(payment)