# Ecom constants

APPROVAL_STATUS = (
    ('SUBMITTED', 'SUBMITTED'),
    ('APPROVED', 'APPROVED'),
    ('REJECTED', 'REJECTED'),
    ('DELETED', 'DELETED'),
)

ORDER_STATUS = (
    ('initiated', 'initiated'),
    ('placed', 'placed'),
    ('placed_pending', 'placed_pending'),
    ('accepted', 'accepted'),
    ('packing', 'packing'),
    ('packed', 'packed'),
    ('s_dispatched', 's_dispatched'),
    ('cancelled', 'cancelled'),
    ('pending', 'pending'),
    ('shipped', 'shipped'),
    ('in_transit', 'in_transit'),
    ('dispatched', 'dispatched'),
    ('delivered', 'delivered'),
    ('return', 'return'),
    ('exchange', 'exchange'),
    ('undelivered', 'undelivered'),
    ('lost', 'lost'),
    ('return_cancelled', 'return_cancelled'),
)

COMBINATION_TYPE = (
    ('single', 'single'),
    ('multiple', 'multiple')
)

ECOM_DOCUMENT_TYPE = (
    ('Shop_act_licence', 'SAL'),
    ('Pan', 'PN'),
    ('Shop_Image', 'SI'),
)

PRODUCT_IMAGE_TYPE = (
    ('main', 'main'),
    ('other', 'other'),
    ('seller_return_image', 'seller_return_image'),
    ('customer_return_image', 'customer_return_image'),
)

RETURN_REASON = (
    ('The Product does not match description', 'The Product does not match description'),
    ('Damage Product', 'Damage Product'),
    ('Received the wrong size', 'Received the wrong size'),
    ('Some other reason', 'Some other reason'),
)

SELLER_AMOUNT_STATUS = (
    ('dispatched', 'dispatched'),
    ('delivered', 'delivered'),
)

SELLER_TYPE = (
    ('Seller', 'Seller'),
    ('Logistics', 'Logistics')
)

CREDIT_STATUS = (
    ('Initiated', 'Initiated'),
    ('Pay', 'Pay'),
    ('Paid', 'Paid'),
    ('Failed', 'Failed')
)

LOGISTICS_ORDER_MODE = (
    ('Delivery', 'Delivery'),
    ('Reverse', 'Reverse')
)

CREDIT_DATA_STATUS = (

    ('fetched', 'fetched'),
    ('default', 'default'),
    ('repayment', 'repayment'),
    ('initiated', 'initiated'),
    ('success', 'success'),
    ('failed', 'failed'),
    ('settlement', 'settlement'),
    ('refund', 'refund'),
    ('recharge_initiated', 'recharge_initiated'),
    ('recharge_success', 'recharge_success'),
    ('recharge_failed', 'recharge_failed'),
    ('order_return', 'order_return'),
    ('order_cancel', 'order_cancel'),
    ('order_exchange', 'order_exchange'),
    ('order_exchange', 'order_exchange'),
    ('order_undelivered', 'order_undelivered'),
    ('recharge', 'recharge'),
    ('suspicious', 'suspicious'),

)

WALLET_DATA_STATUS = (
    ('account', 'account'),
    ('wallet', 'wallet'),
    ('order', 'order'),
    ('delivery_charge', 'delivery_charge'),
    ('default', 'default'),
    ('repayment', 'repayment'),
    ('initiated', 'initiated'),
    ('refund', 'refund'),
    ('refund_cancel', 'refund_cancel'),
    ('success', 'success'),
    ('settlement', 'settlement'),
    ('recharge_initiated', 'recharge_initiated'),
    ('recharge_success', 'recharge_success'),
    ('recharge_failed', 'recharge_failed'),
    ('convenience_charge', 'convenience_charge'),
    ('order_return', 'order_return'),
    ('order_cancel', 'order_cancel'),
    ('order_exchange', 'order_exchange'),
    ('cashback_amount', 'cashback_amount'),
    ('recharge_cashback', 'recharge_cashback'),
    ('cancel_recharge_cashback', 'cancel_recharge_cashback'),
    ('cancel_cashback', 'cancel_cashback'),
    ('repayment_cashback', 'repayment_cashback'),
    ('undelivered_delivery_charge', 'undelivered_delivery_charge'),
    ('order_undelivered', 'order_undelivered'),
    ('repayment_cancel_cashback', 'repayment_cancel_cashback'),
    ('order_return_discount', 'order_return_discount'),
    ('return_reject_delivery_charge', 'return_reject_delivery_charge'),
    ('return_delivery_charge_initiated', 'return_delivery_charge_initiated'),
    ('return_delivery_charge_success', 'return_delivery_charge_success'),
    ('return_cancel_delivery_charge', 'return_cancel_delivery_charge'),
    ('loan_repayment', 'loan_repayment'),
    ('loan_disbursed', 'loan_disbursed'),
    ('loan_repayment_adjust', 'loan_repayment_adjust'),
    ('disbursed','disbursed')

)

STOCK_CHANGE_BY = (
    ('order', 'order'),
    ('seller', 'seller'),
    ('admin', 'admin'),
    ('initiate', 'initiate'),
    ('undelivered', 'undelivered')
)

# For Payment

STATUS_TYPE = (
    ("initiated", "initiated"),
    ("success", "success"),
    ("failed", "failed"),
    ("cancelled", "cancelled"),
    ("cancelled_m", "cancelled manually"),
    ("pending", "pending"),
)

PAYMENT_MODE = (
    ('NB', 'Net Banking'),
    ('DC', 'Debit Card'),
    ('CC', 'Credit Card'),
    ('UPI', 'UPI'),
    ('PPI', 'Wallet'),
    ('RWT', 'Rzpay_Wallet'),
    ('C', 'Cash'),
    ('NA', 'Cancel'),
)

PAYMENT_SUB_MODE = 'IMPS'

PRODUCT_TYPE = (
    ('Mudra', 'Mudra'),
    ('Ecom', 'Ecom'),

)

CREDIT_TYPE = (

    ('Order', 'Order'),
    ('Npci', 'Npci'),

)

PaymentGateway = (
    # From which payment gateway this transaction is made
    ('Ptm', 'Ptm'),
    ('Rpay', 'Rpay'),
)
ReturnStatus = (
    # From which payment gateway this transaction is made
    ('ps2s', 'ps2s'),
    ('pstatus', 'pstatus'),
    ('rstatus', 'rstatus'),
    ('rs2s', 'rs2s'),
)

PAYMENT_Type = (
    # Payment Type
    ('Cash', 'Cash'),
    ('Account', 'Account'),
    ('App', 'App'),
    ('On_Us', 'On_Us'),
    ('Pt-Invoice', 'Pt-Invoice'),
)

CONVENIENT_FEE_STATUS = (
    ('active', 'active'),
    ('inactive', 'inactive')
)

ORDER_TYPE = (
    ('Paid', 'Paid'),
    ('Free', 'Free')
)

CONVENIENT_FEES_ORDER_TYPE = (
    ('order', 'order'),
    ('order_details', 'order_details')
)

ORDER_DETAILS_PAYMENT_MODE = (
    ('cash', 'cash'),
    ('credit', 'credit'),
    ('cod', 'cod')
)

DEFAULT_WALLET_AMOUNT_TYPE = (
    ('customer', 'customer'),
    ('employee', 'employee')
)

SELLER_ORDER_CANCEL_BY = (
    ('seller', 'seller'),
    ('auto_cancel', 'auto_cancel')
)

LOGISTIC_TYPE = (
    ('ithink', 'ithink'),
    ('shiprocket', 'shiprocket')
)

TEXT_VIEW = (
    ('Men', 'Men'),
    ('Women', 'Women'),
    ('Electronics', 'Electronics'),
    ('Home', 'Home'),
    ('Fashion_Store', 'Fashion_Store'),
    ('Flea_Store', 'Flea_Store'),
    ('Luxury_Store', 'Luxury_Store'),
    ('Must_Haves', 'Must_Haves'),
    ('Just_Arrived', 'Just_Arrived')
)

PRODUCT_PRIORITY_TYPE = (
    ('App', 'App'),
    ('Web', 'Web'),
    ('Both', 'Both'),
)

PRODUCT_PRIORITY_VIEW_TYPE = (
    ('Front', 'Front'),
    ('Back', 'Back'),
    ('Both', 'Both'),
)

BANNER_TYPE = (
    ('App', 'App'),
    ('Web', 'Web'),
    ('Both', 'Both'),
)

BANNER_TEXT_VIEW = (
    ('Home', 'Home'),
)

DISCOUNT_TYPE = (
    ('new_product', 'new_product'),
    ('offer', 'offer')
)

DEFAULTER_TYPE = (
    ('mudrakwik', 'mudrakwik'),
    ('rupeekwik', 'rupeekwik'),
    ('creditkart', 'creditkart'),
    ('instarupee', 'instarupee'),
)

OFFER_BANNER_TYPE = (
    ('App', 'App'),
    ('Web', 'Web'),
    ('Both', 'Both'),
)

BANNER_TYPE_L = (
    ('loan', 'loan'),
    ('insurance', 'insurance'),
)

USER_OFFER_TYPE = (
    ('App', 'App'),
    ('Web', 'Web'),
    ('Both', 'Both'),
)

RATING_CHOICES = (
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
)

DASHBOARD_DATA_TIME_TYPE = (
    ('day', 'day'),
    ('night', 'night')
)


MOBILE_BLOCK_TYPE = (
    ('Register_Block', 'Register_Block'),
    ('Order_Block', 'Order_Block'),
    ('Loan_Block', 'Loan_Block'),
    ('All_Block', 'All_Block')
)


LOG_ACTION = (
    ('deleted', 'deleted'),
    ('added', 'added'),
    ('updated', 'updated'),
    ('before_update', 'before_update'),
    ('after_update', 'after_update')
)

PLATFORM_TYPE = (
    ('PhonePe', 'PhonePe'),
    ('Creditkart', 'Creditkart')
)