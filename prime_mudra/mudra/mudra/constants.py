MARITAL_STATUS = (
    ('MARRIED', 'Married'),
    ('UNMARRIED', 'Not Married'),
    ('DIVORCED', 'Divorced'),
    ('WIDOW', 'Widow'),
)

OCCUPATION = (
    ('SALARIED', 'Salaried'),
    ('SELF_EMPLOYED', 'Self Employed'),
    ('STUDENT', 'Student'),
    ('HOUSE_WIFE', 'Housewife'),
)

GENDER = (
    ('Female', 'F'),
    ('Male', 'M')
)
TYPE = (
    ('Female', 'F'),
    ('Male', 'M')
)

ADDRESS_TYPE = (
    ('Permanent', 'P'),
    ('Current', 'C'),
    ('Ecom', 'E'),
    ('Billing', 'B'),
    ('Shipping', 'S'),
)

RELATION = (
    ('Mother', 'MO'),
    ('Father', 'FA'),
    ('Brother', 'BR'),
    ('Sister', 'SI'),
    ('Spouse', 'SP'),
    ('Son', 'SN'),
    ('Daughter', 'DT'),
    ('None', 'None'),
)

DOCUMENT_TYPE = (
    ('Aadhar_Front', 'AF'),
    ('Aadhar_Back', 'AB'),
    ('Pan', 'PN'),
    ('Bank_Statement', 'BS'),
    ('Salary_Slip', 'SS'),
    ('Company_Id', 'CI'),
    ('Self_Video', 'SV'),
    ('Selfie', 'SF'),
    ('Current_Address', 'CA'),
    ('Permanent_Address', 'PA'),
    ('Shop_Image', "SA"),
)

LOCATION_TYPE = (
    ('home', 'H'),
    ('office', 'O'),
)

# For Payment

STATUS_TYPE = (
    ("initiated", "initiated"),
    ("success", "success"),
    ("failed", "failed"),
    ("cancelled", "cancelled"),
    ("cancelled_m", "cancelled manually"),
    ("pending", "pending"),
)

PAYMENT_MODE = (
    ('NB', 'Net Banking'),
    ('DC', 'Debit Card'),
    ('CC', 'Credit Card'),
    ('UPI', 'UPI'),
    ('PPI', 'Wallet'),
    ('RWT', 'Rzpay_Wallet'),
    ('C', 'Cash'),
    ('NA', 'Cancel'),
)

PAYMENT_SUB_MODE = 'IMPS'

PAYMENT_CTG = (
    # Payment Category
    ('Rbl', 'Rbl'),
    ('Subscription', 'Subscription'),
    ('Loan', 'Loan'),
    ('Order', 'Order'),
    ('Repayment', 'Repayment'),
    ('Recharge', 'Recharge'),
    ('Order_Return', 'Order_Return'),
    ('Npci', 'Npci'),
    ('Loan_Repayment', 'Loan_Repayment'),

)

PRODUCT_TYPE = (
    ('Mudra', 'Mudra'),
    ('Ecom', 'Ecom'),
    ('Npci', 'Npci'),

)

PLATFORM_TYPE = (

    ('App', 'App'),
    ('Web', 'Web'),

)

PaymentGateway = (
    # From which payment gateway this transaction is made
    ('Ptm', 'Ptm'),
    ('Rpay', 'Rpay'),
)

ReturnStatus = (
    # From which payment gateway this transaction is made
    ('ps2s', 'ps2s'),
    ('pstatus', 'pstatus'),
    ('rstatus', 'rstatus'),
    ('rs2s', 'rs2s'),
)

PAYMENT_Type = (
    # Payment Type
    ('Cash', 'Cash'),
    ('Account', 'Account'),
    ('App', 'App'),
    ('On_Us', 'On_Us'),
    ('Pt-Invoice', 'Pt-Invoice'),
    ('Ecom_Wallet', 'Ecom_Wallet'),
    ('COD', 'COD'),
    ('loan', 'loan')
)

scheme = {
    '1000': {
        "ms_amount": "1000",
        "ms_pf": "50",
        "ms_interest": "50",
        "ms_net_loan": "900",
        "ms_tenure": "10",
        "ms_edi": "100",
        "ms_pf_percentage": "5",
        "ms_score": "0"
    },
    '2000': {
        "ms_amount": "2000",
        "ms_pf": "100",
        "ms_interest": "100",
        "ms_net_loan": "1800",
        "ms_tenure": "10",
        "ms_edi": "200",
        "ms_pf_percentage": "5",
        "ms_score": "20"
    },
    '3000': {
        "ms_amount": "3000",
        "ms_pf": "150",
        "ms_interest": "150",
        "ms_net_loan": "2700",
        "ms_tenure": "15",
        "ms_edi": "200",
        "ms_pf_percentage": "5",
        "ms_score": "40"
    },
    '5000': {
        "ms_amount": "5000",
        "ms_pf": "250",
        "ms_interest": "250",
        "ms_net_loan": "4500",
        "ms_tenure": "20",
        "ms_edi": "250",
        "ms_pf_percentage": "5",
        "ms_score": "70"
    },
    '10000': {
        "ms_amount": "10000",
        "ms_pf": "500",
        "ms_interest": "500",
        "ms_net_loan": "9000",
        "ms_tenure": "20",
        "ms_edi": "500",
        "ms_pf_percentage": "5",
        "ms_score": "110"
    },
    '15000': {
        "ms_amount": "15000",
        "ms_pf": "750",
        "ms_interest": "750",
        "ms_net_loan": "13500",
        "ms_tenure": "30",
        "ms_edi": "500",
        "ms_pf_percentage": "5",
        "ms_score": "150"
    },
    '25000': {
        "ms_amount": "25000",
        "ms_pf": "1000",
        "ms_interest": "1500",
        "ms_net_loan": "22500",
        "ms_tenure": "50",
        "ms_edi": "500",
        "ms_pf_percentage": "4",
        "ms_score": "210"
    },
    '50000': {
        "ms_amount": "50000",
        "ms_pf": "1500",
        "ms_interest": "3500",
        "ms_net_loan": "45000",
        "ms_tenure": "50",
        "ms_edi": "1000",
        "ms_pf_percentage": "3",
        "ms_score": "310"
    },
    '100000': {
        "ms_amount": "100000",
        "ms_pf": "3000",
        "ms_interest": "7000",
        "ms_net_loan": "90000",
        "ms_tenure": "100",
        "ms_edi": "1000",
        "ms_pf_percentage": "3",
        "ms_score": "410"
    },
    '250000': {
        "ms_amount": "250000",
        "ms_pf": "7500",
        "ms_interest": "17500",
        "ms_net_loan": "225000",
        "ms_tenure": "250",
        "ms_edi": "1000",
        "ms_pf_percentage": "3",
        "ms_score": "610"
    },
    '500000': {
        "ms_amount": "500000",
        "ms_pf": "15000",
        "ms_interest": "35000",
        "ms_net_loan": "450000",
        "ms_tenure": "500",
        "ms_edi": "1000",
        "ms_pf_percentage": "3",
        "ms_score": "985"
    },

}

COM = (
    ('Yes', 'Yes'),
    ('No', 'No'),
)

RESIDENCE_TYPE = (
    ('Owned by Self or Spouse', 'Owned by Self or Spouse'),
    ('Living with Parents', 'Living with Parents'),
    ('Rented with Family', 'Rented with Family'),
    ('Rented with Friends', 'Rented with Friends'),
    ('Rented Individually', 'Rented Individually'),
    ('Paying Guest or Hostel', 'Paying Guest or Hostel'),
    ('Others', 'Others'),

)

EDUCATIONAL = (
    ('Undergraduate', 'Undergraduate'),
    ('Graduate', 'Graduate'),
    ('Post Graduate', 'Post Graduate'),
    ('Doctorate & Above', 'Doctorate & Above'),
    ('Professional Degree', 'Professional Degree'),
    ('Diploma', 'Diploma'),
    ('Others', 'Others')
)

INDUSTRY = (
    ('Agriculture', 'Agriculture'),
    ('Automotive', 'Automotive'),
    ('Cement and cement products', 'Cement and cement products'),
    ('Chemicals', 'Chemicals'),
    ('Coal', 'Coal'),
    ('Communication', 'Communication'),
    ('Construction', 'Construction'),
    ('Consumer electronics', 'Consumer electronics'),
    (' Consumer products', ' Consumer products'),
    ('Containers and packing', 'Containers and packing'),
    ('Defense', 'Defense'),
    ('Engineering', 'Engineering'),
    ('Entertainment and leisure', 'Entertainment and leisure'),
    ('Fmcg', 'Fmcg'),
    ('Food products', 'Food products'),
    ('Gems and jewellry', 'Gems and jewellry'),
    ('Gift items and handicrafts', 'Gift items and handicrafts'),
    ('Glass and glass products', 'Glass and glass products'),
    ('Healthcare and healthcare providers', 'Healthcare and healthcare providers'),
    ('Industrial equipment', 'Industrial equipment'),
    ('Institutions and trust', 'Institutions and trust'),
    ('It and communication', 'It and communication'),
    ('Leather', 'Leather'),
    ('Media', 'Media'),
    ('Metals', 'Metals'),
    ('Oil and gas allied', 'Oil and gas allied'),
    ('Opthalmics', 'Opthalmics'),
    ('Other services', 'Other services'),
    ('Others', 'Others'),
    ('Paints', 'Paints'),
    ('Paper & pulp', 'Paper & pulp'),
    ('Petroleum products', 'Petroleum products'),
    ('Photographic equipment and allied products', 'Photographic equipment and allied products'),
    ('Plastic products', 'Plastic products'),
    ('Pollution control equipment', 'Pollution control equipment'),
    ('Professional services', 'Professional services'),
    ('Rubber and rubber products', 'Rubber and rubber products'),
    ('Stationary and other supplies', 'Stationary and other supplies'),
    ('Textiles', 'Textiles'),
    ('Transport logistics', 'Transport logistics'),
    ('Wood and wood products', 'Wood and wood products'),

)

PURPOSE_LOAN = (
    ('Education/Student loan', 'Education/Student loan'),
    ('Relocation', 'Relocation'),
    ('Medical bills', 'Medical bills'),
    ('Marriage', 'Marriage'),
    ('Rent/Bills', 'Rent/Bills'),
    ('Business expansion', 'Business expansion'),
    ('New business', 'New business'),
    ('Payoff credit cards', 'Payoff credit cards'),
    ('Payoff other loans', 'Payoff other loans'),
    ('Home improvement', 'Home improvement'),
    ('Help out a family member/friend', 'Help out a family member/friend'),
    ('Vacation/Travel', 'Vacation/Travel'),
    ('Purchase of Vehicle', 'Purchase of Vehicle'),
    ('Purchase of Mobile/TV/AC/Fridge, etc.', 'Purchase of Mobile/TV/AC/Fridge, etc.'),
    ('Others', 'Others'),
)

TYPE = (
    ('M', 'M'),
    ('C', 'C'),
    ('F', 'F'),
)

PERSONAL_ADDRESS_TYPE = (
    ('cash', 'cash'),
    ('credit', 'credit'),
    ('loan','loan')
)

# NPCI constants

NPCI_TYPE = (
    ('cash', 'cash'),
    ('credit', 'credit'),
    ('wallet', 'wallet')
)

NPCI_SERVICE_CATEGORY = (
    ('mobile_recharge', 'mobile_recharge'),
    ('dth_recharge', 'dth_recharge'),
    ('postpaid_mobile', 'postpaid_mobile'),
    ('electric_bill', 'electric_bill'),
    ('landline_bill', 'landline_bill'),
    ('gas_bill', 'gas_bill'),
    ('water_bill', 'water_bill'),
    ('broadband_bill', 'broadband_bill'),
    ('insurance_bill', 'insurance_bill'),
    ('account_balance', 'account_balance'),
    ('bill_verification', 'bill_verification'),
)

NPCI_SERVICE_TYPE = (
    ('bill', 'bill'),
    ('recharge', 'recharge'),
)

INSTALLMENT_TYPE = (
    ('1', '1'),
    ('2', '2'),
    ('3', '3')
)

RECOVERY_TYPE = (
    ('Ecom', 'Ecom'),
    ('Loan', 'Loan'),
    ('NPCI', 'NPCI'),
)

RAZORPAY_ORDER_TYPE = (
    ('AU_ORDER', 'AU_ORDER'),
    ('SU_ORDER', 'SU_ORDER')
)
RAZORPAY_AUTH_TYPE = (
    ('NetBanking', 'NetBanking'),
    ('Debit', 'Debit')
)


ACCOUNT_TYPE = (
    ('savings', 'savings'),
    ('current','current')
)
BANK_TYPE = (
    ('loan', 'loan'),
    ('auth', 'auth')
)

ENACH_STATUS_TYPE = (
    ('confirmed', 'confirmed'),
    ('rejected', 'rejected'),
    ('cancelled', 'cancelled'),
    ('paused', 'paused'),
    ('resumed', 'resumed')
)

LENDBOX_APPLICATION_STATUS = (
    ('generated', 'generated'),
    ('created', 'created'),
    ('approved', 'approved')
)

LENDBOX_API_STATUS = (
    ('initiated', 'initiated'),
    ('success', 'success'),
    ('error', 'error')
)

USER_OFFER_TYPE = (
    ('App', 'App'),
    ('Web', 'Web'),
    ('Both', 'Both'),
)