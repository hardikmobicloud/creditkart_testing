'''
    Logging settings for django projects, works with django 1.5+
    If DEBUG=True, all logs (including django logs) will be
    written to console and to debug_file.
    If DEBUG=False, logs with level INFO or higher will be
    saved to production_file.
    Logging usage:
'''

# Reference:  https://www.scalyr.com/blog/getting-started-quickly-django-logging/

from django.http import HttpResponse
from mudra import settings

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'formatters': {
        'main_formatter': {
            'format': '%(levelname)s:  %(name)s:  %(message)s '
                      '(%(asctime)s;  %(filename)s:  %(funcName)s:  %(lineno)d)',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'main_formatter',
        },
        'production_file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': settings.LOG_ROOT + '/main.log',
            'maxBytes': 1024 * 1024 * 50,  # 5 MB
            'backupCount': 7,
            'formatter': 'main_formatter',
            'filters': ['require_debug_false'],
        },
        'debug_file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': settings.LOG_ROOT + '/main_debug.log',
            'maxBytes': 1024 * 1024 * 50,  # 5 MB
            'backupCount': 7,
            'formatter': 'main_formatter',
            'filters': ['require_debug_true'],
        },
        'null': {
            "class": 'django.utils.log.NullHandler',
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins', 'console'],
            'level': 'ERROR',
            'propagate': True,
        },
        'django': {
            'handlers': ['null', ],
        },
        'py.warnings': {
            'handlers': ['null', ],
        },
        '': {
            'handlers': ['console', 'production_file', 'debug_file'],
            'level': "DEBUG",
        },
    }
}

# Test log
# This retrieves a Python logging instance (or creates it)
import logging

logger = logging.getLogger(__name__)


def log_test(request):
    # Send the Test!! log message to standard out
    logger.info("info Test!!")
    logger.debug("debug Test!!")
    logger.warning("warning Test!!")
    logger.error("error Test!!")
    logger.critical("critical Test!!")

    return HttpResponse("Hello logging world.")
