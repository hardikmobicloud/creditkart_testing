import datetime
import re
from datetime import date, timedelta
from functools import partial
from django.apps import apps
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .permissions import GuestTokenPermission

TecdoLeads = apps.get_model('lead', 'TecdoLeads')
Subscription = apps.get_model('User', 'Subscription')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')


class GetTecdoLeads(APIView):
    permission_classes = (partial(GuestTokenPermission, ['GET', 'POST', 'HEAD']),)

    def post(self, request, *args, **kwargs):
        """

        :param request:
        {
            "mobile_number":"7875583671",
            "click_id":"",
            "sign":"",
            "timestamp":"1574663853"

        }
        :param args:
        :param kwargs:
        :return:

         status 1: User already exist
         status 2: User registered successfully
         status 3: Something went wrong
         status 4: Invalid mobile number
         status 5: Please provide mobile number
         status 6: Invalid Date format


        """

        mobile_number = request.data['mobile_number']
        click_id = request.data['click_id']
        # sign = request.data['sign']     timestamp = request.data['timestamp']

        try:
            timestamp = datetime.datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')


        except Exception as e:
            print(e)
            pass

        if mobile_number:
            validate_number = re.compile(r'^[6-9]\d{9}$')

            validate = validate_number.search(mobile_number)

            if validate:

                is_user_registered = TecdoLeads.objects.filter(mobile=mobile_number)
                if is_user_registered.exists():
                    return Response(
                        {
                            'status_code': '1',
                            'message': 'User already registered',
                        },
                        status=status.HTTP_400_BAD_REQUEST
                    )
                try:
                    today = datetime.datetime.today()
                    today = today.strftime("%Y-%m-%d")
                    register_user = TecdoLeads.objects.create(mobile=mobile_number, click_date=timestamp,
                                                              click_id=click_id, )
                except ValidationError as e:
                    print(e, type(e))
                    register_user = None
                    return Response(
                        {
                            'status_code': '6',
                            'message': 'Invalid Date format'
                        },
                        status=status.HTTP_400_BAD_REQUEST
                    )

                if register_user and register_user is not None:
                    return Response(
                        {
                            'status_code': '2',
                            'message': 'User registered successfully'
                        },
                        status=status.HTTP_201_CREATED
                    )
                else:
                    return Response(
                        {
                            'status_code': '3',
                            'message': 'Something went wrong'
                        },
                        status=status.HTTP_400_BAD_REQUEST
                    )
            else:
                return Response(
                    {
                        'status_code': '4',
                        'message': 'Invalid mobile number'
                    },
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                {
                    'status_code': '5',
                    'message': 'Please provide mobile number'
                },
                status=status.HTTP_400_BAD_REQUEST
            )


class TecdoUserRegisterReport(APIView):

    def post(self, request):
        """

        :param request:
        :return:



        """
        yesterday = date.today() - timedelta(days=1)

        tecdo_subscription = TecdoLeads.objects.filter(is_subscribed=False, click_date__startswith=yesterday)

        register_user = []
        for lending in tecdo_subscription:
            user = User.objects.filter(username=lending.mobile)

            if user:
                subscription = Subscription.objects.filter(user_id=user[0].id, payment_status='success').order_by('-id')

                if subscription:
                    if lending.click_date:

                        if lending.click_date <= subscription[0].start_date and lending.click_date <= user[
                            0].date_joined:

                            register_user.append(lending.mobile)
                            TecdoLeads.objects.filter(mobile=lending.mobile).update(is_subscribed=True)
                        
        return Response(
            {
                'status': 1,
                'data': register_user
            }
        )
