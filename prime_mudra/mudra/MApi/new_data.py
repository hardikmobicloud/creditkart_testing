import datetime
import re
from datetime import date, timedelta

import requests
from django.http import HttpResponse
from functools import partial
from rest_framework.authentication import *
from rest_framework.permissions import *
from django.apps import apps
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from django_pandas.io import read_frame
import pandas as pd
import sys
import os

from MApi.permissions import GuestTokenPermission

LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
LoanAcknowledgement = apps.get_model('LoanAppData', 'LoanAcknowledgement')
Recovery = apps.get_model('Recovery', 'Recovery')
RecoveryCallHistory = apps.get_model('Recovery', 'RecoveryCallHistory')


class GetNewData(APIView):
    # permission_classes = (partial(GuestTokenPermission, ['GET', 'POST', 'HEAD']))
    def get(self, request, *args, **kwargs):
        """

            {
                "status": 1,
                "data": {
                    "recovery": {
                        "created_date": "2019-05-31 12:48:53.576145",
                        "completed_date": null,
                        "insentive_amount": "25"
                        "recovery_status": "Yes",
                        "username": "Daniel"
                    },
                    "calls_history": [
                        {
                            "created_date": "2019-05-31T15:27:15.525593",
                            "calling_status": "Yes",
                            "recovery": "10",
                            "next_call_time": "08:00:00",
                            "next_call_actual_Datetime": "2019-06-01T10:49:26.242082",
                            "call_desc": "",
                            "next_call_status": true,
                            "expected_payment_date": "2019-05-31T00:00:00"
                        },
                        {
                            "created_date": "2019-05-31T15:11:40.249385",
                            "calling_status": "Yes",
                           "recovery": "10",
                            "next_call_time": "08:00:00",
                            "next_call_actual_Datetime": "2019-06-01T10:16:48.548001",
                            "call_desc": "",
                            "next_call_status": true,
                            "expected_payment_date": "2019-05-31T00:00:00"
                        }
                    ],
                    "loan_application_id": 881353277,
                    "loan_acknowledgement": {
                        "created_date": "2019-05-10T21:50:30.908334",
                        "updated_date": "2019-05-10T21:50:30.908379",
                        "usernamel": "9899692256",
                        "acknowledgement_pdf": "loan_applications / loan_application_acknowledgement_100058420_8530692630.pdf"
                    }
                }
            }

        """

        dict_param ={
            'loan_application_id':'881353277'
        }
        url = 'http://127.0.0.1:8000/api/olddata/'
        r = requests.post(url=url, data=dict_param)
        r = r.json()

        loan_application_id = r['data']['loan_application_id']
        '''
        recovery

        '''
        calldata = r['data']['calls_history']
        recovery = r['data']['recovery']
        created_date_r = recovery['created_date']
        completed_date_r = recovery['completed_date']
        insentive_amount = recovery['insentive_amount']
        recovery_status = recovery['recovery_status']
        username = recovery['username']
        '''
        call_history
        '''

        '''
        LoanAcknowledgement
        '''
        created_date_l = r['data']['loan_acknowledgement']['created_date']
        updated_date_l = r['data']['loan_acknowledgement']['updated_date']
        acknowledgement_pdf = r['data']['loan_acknowledgement']['acknowledgement_pdf']
        usernamel = r['data']['loan_acknowledgement']['username']


        try:

            loan = LoanApplication.objects.filter(loan_application_id=loan_application_id)

            if loan:

                user = User.objects.filter(username=username)

                if user:


                    recovery_data = Recovery.objects.filter(loan_transaction_id=loan[0].id)

                    if recovery_data:

                        if not (recovery_data[0].completed_date == completed_date_r and recovery_data[
                            0].insentive_amount == insentive_amount
                                and recovery_data[0].user_id == user[0].id):

                            Recovery.objects.filter(loan_transaction_id=loan[0].id).update(
                                completed_date=completed_date_r,
                                insentive_amount=insentive_amount,
                                user_id=user[0].id)

                        if recovery_status == 'Yes':
                            Recovery.objects.filter(loan_transaction=loan[0].id).update(recovery_status=recovery_status)


                        for call in calldata:

                            call_History = RecoveryCallHistory.objects.filter(recovery_id=recovery_data[0].id,
                                                                              calling_status=call['calling_status'],
                                                                              next_call_time=call['next_call_time'],
                                                                              next_call_actual_Datetime=call[
                                                                                  'next_call_actual_Datetime'],
                                                                              call_desc=call['call_desc'],
                                                                              next_call_status=call['next_call_status'],
                                                                              expected_payment_date=call[
                                                                                  'expected_payment_date'],
                                                                              remark_id=call['remark_id'])

                            if not call_History:

                                RecoveryCallHistory.objects.create(recovery_id=recovery_data[0].id,
                                                                   calling_status=call['calling_status'],
                                                                   next_call_time=call['next_call_time'],
                                                                   next_call_actual_Datetime=call[
                                                                       'next_call_actual_Datetime'],
                                                                   call_desc=call['call_desc'],
                                                                   next_call_status=call['next_call_status'],
                                                                   expected_payment_date=call['expected_payment_date'],
                                                                   remark_id=call['remark'])

                    else:
                        Recovery.objects.create(created_date=created_date_r, completed_date=completed_date_r,
                                                insentive_amount=insentive_amount, recovery_status=recovery_status,
                                                loan_transaction_id=loan[0].id,
                                                user_id=user[0].id)

                        createdrecovery_data = Recovery.objects.filter(loan_transaction_id=loan[0].id)
                        for call in calldata:

                            call_History = RecoveryCallHistory.objects.filter(recovery_id=createdrecovery_data[0].id,
                                                                              calling_status=call['calling_status'],
                                                                              next_call_time=call['next_call_time'],
                                                                              next_call_actual_Datetime=call[
                                                                                  'next_call_actual_Datetime'],
                                                                              call_desc=call['call_desc'],
                                                                              next_call_status=call['next_call_status'],
                                                                              expected_payment_date=call[
                                                                                  'expected_payment_date'],
                                                                              remark_id=call['remark'])

                            if not call_History:

                                RecoveryCallHistory.objects.create(recovery_id=createdrecovery_data[0].id,
                                                                   calling_status=call['calling_status'],
                                                                   next_call_time=call['next_call_time'],
                                                                   next_call_actual_Datetime=call[
                                                                       'next_call_actual_Datetime'],
                                                                   call_desc=call['call_desc'],
                                                                   next_call_status=call['next_call_status'],
                                                                   expected_payment_date=call['expected_payment_date'],
                                                                   remark_id=call['remark'])


            loan_ack = LoanApplication.objects.filter(loan_application_id=loan_application_id)

            if loan_ack:
                user1 = User.objects.filter(username=usernamel)
                if user1:
                    check_id = LoanAcknowledgement.objects.filter(loan_transaction_id=loan_ack[0].id)

                    if check_id:

                        if not (check_id[0].acknowledgement_pdf == acknowledgement_pdf):

                            LoanAcknowledgement.objects.filter(loan_transaction_id=loan_ack[0].id).update(
                                acknowledgement_pdf=acknowledgement_pdf, user_id=user1[0].id)


                    else:

                        LoanAcknowledgement.objects.create(created_date=created_date_l, updated_date=updated_date_l,
                                                           acknowledgement_pdf=acknowledgement_pdf,
                                                           loan_transaction_id=loan_ack[0].id, user_id=user1[0].id)
        except Exception as e:
            print("-----------------exc", e, e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
        return HttpResponse('done')