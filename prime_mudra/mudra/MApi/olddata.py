# from django.apps import apps
# from django.contrib.auth.models import User
# from django.http import HttpResponse

from django.apps import apps

LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')

LoanAcknowledgement = apps.get_model('LoanAppData', 'LoanAcknowledgement')
Recovery = apps.get_model('Recovery', 'recovery')
RecoveryCallHistory = apps.get_model('Recovery', 'recoveryCallHistory')


from django.apps import apps
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.core.exceptions import ValidationError
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from django.apps import apps


class GetLoanAppId(APIView):
    # permission_classes = (partial(GuestTokenPermission, ['GET', 'POST', 'HEAD']),)
    def post(self, request, *args, **kwargs):
        """
        :param request:
        {
            "loan_application_id":"186414440",
        }
        """

        loan_application_id = request.data['loan_application_id']

        data = {}
        calls = {}
        recovery_data = {}
        loan_acknowledgement_data = {}
        call_list = []
        loan = LoanApplication.objects.filter(loan_application_id=loan_application_id)
        if loan:
            recovery_item = Recovery.objects.filter(loan_transaction_id=loan[0].id)
            if recovery_item:
                recovery_data['created_date'] = recovery_item[0].created_date
                recovery_data['username'] = recovery_item[0].user.username
                recovery_data['completed_date'] = recovery_item[0].completed_date
                recovery_data['insentive_amount'] = recovery_item[0].insentive_amount
                recovery_data['recovery_status'] = recovery_item[0].recovery_status
                callhistory = RecoveryCallHistory.objects.filter(recovery_id=recovery_item[0].id)
                if callhistory:
                    for call in callhistory:
                        calls = {}
                        calls['created_date'] = call.created_date
                        calls['calling_status'] = call.calling_status
                        calls['next_call_time'] = call.next_call_time
                        calls['next_call_actual_Datetime'] = call.next_call_actual_Datetime
                        calls['call_desc'] = call.call_desc
                        calls['next_call_status'] = call.next_call_status
                        calls['expected_payment_date'] = call.expected_payment_date
                        calls['remark_id'] = call.remark_id
                        call_list.append(calls)
            data['recovery'] = recovery_data
            data['calls_history'] = call_list

            data['loan_application_id'] = loan_application_id
            loan_acknowledgement_item = LoanAcknowledgement.objects.filter(loan_transaction_id=loan[0].id)
            if loan_acknowledgement_item:

                loan_acknowledgement_data['created_date'] = loan_acknowledgement_item[0].created_date
                loan_acknowledgement_data['updated_date'] = loan_acknowledgement_item[0].updated_date
                loan_acknowledgement_data['username'] = loan_acknowledgement_item[0].user.username
                loan_acknowledgement_data['acknowledgement_pdf'] = str(loan_acknowledgement_item[0].acknowledgement_pdf)
            data['loan_acknowledgement'] = loan_acknowledgement_data
        else:
            return Response(
                {
                    'status': 2,
                    'message': "No data for this loan application id"
                }
            )
        return Response(
            {
                'status': 1,
                'data': data
            }
        )
