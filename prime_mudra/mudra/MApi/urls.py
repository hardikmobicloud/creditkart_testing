from django.urls import path

from .enach_razorpay import RazorCustomerCreate,FetchTokenCustomerId,CreateRazorpayOrder,FetchTokenPaymentId,CallbackUpdateAuthorizationPayment,EnachRazorpayWebHook,CheckStatus

from .auth import LoginRegisterAPI, UserOTPConfirm, FCMEntryAPIView, SetPinAPI, reset_pin, ResetPin, \
    ReturnTokenMudrakwikUser, Version_Check_v2, \
    CheckUser, ReturnTokenRupeekwikUser, UserOTPConfirmSetPin, LoginRegisterAPI_web
from .checksum_web import ChecksumGenerate_Subcription, ChecksumGeneratePaytm_Producation,ChecksumGeneratePaytm_LoanRepayment
from .dashboard_api import DashboardDataAPI, DashboardDetailsAPIView, DashboardBankDetailsAPIView, \
    DashboardBankDetailsListAPIView, DashboardCreditCardDetailsListAPIView, DashboardDebitCardDetailsListAPIView, \
    DashboardMerchantDetailsListAPIView, DashboardDataMonthAPI
from .fcm_notification import fcm_test
from .lendingAdda import LendingAddaUserRegister, LendingAddaApproved, GetLendingAddaLeads, send_install_data, \
    send_approved_data
from .loan import CheckDefaulterInAll, LoanHistory, IsUserDetailsComplete, SubmitLoanApplicationAPIView, LoanDetails,LoanSlabDetails,LoanApplicationWithdrawApi,LoanApplicationDisbursedAmountWalletApi, pancard_used_in_active_loan_interlink
from .loan import Loan_Drop_Down_Option, UserRepayment, LoanRepayment, OnHold_Loan_Time_View, CurrentLoanDetail, \
    pan_active_loan_test, create_pdf ,user_loan_agreement,loan_graph_amount,loan_graph_amount_1,LoanBannerApi
from .new_data import GetNewData
from .olddata import GetLoanAppId

from .payment_auth import *
from .paytm_invoice import paytm_create_link_api, fetch_all_link, fetch_transaction, FetchAllLink, FetchLink, \
    CreateLink, ResendNotificationLink
from .razorpay import *
from .tecdo import GetTecdoLeads, TecdoUserRegisterReport
from .transaction_status import Sub_Transaction_Status_Check_Static, Transaction_Status_Check_Static
from .transaction_status import test_trans
from .user import Address, Professional, Personal, Reference, Bank, Document, LocationTrackingAPI, Version_Check, \
    create_new_version, AllAddress, image_upload, ValidatePincodeAPIView, ValidatePanCardVAPIView, UserProfile, \
    FundspiPanAPI,AllLoanDetails,Document_new
from .user_data import Contacts, ShareApp, ContactsNew
from .user_raw_data import UserDeviceData, SmsKeyWordListAPI, SmsImportView, UserAccountsListAPIView, InstalledAppAPI
from .usersharedata import shareddata_Count, SharedData, conversion, sub_insentive, approve_insentive, \
    Not_registered
from .yelo import GetYeloLeads, YeloUserRegisterReport
from .static_data import CheckMobileBlock



urlpatterns = [
    # Per Login/Register
    path('version-check/', Version_Check.as_view(), name='version-check'),
    path('fcm-entry/', FCMEntryAPIView.as_view(), name='fcm-entry'),
    path('fcm-entry_v2/', FCMEntryAPIView.as_view(), name='fcm-entry_v2'),

    # Login
    path('login/', LoginRegisterAPI.as_view(), name='login'),
    path('login_web/', LoginRegisterAPI_web.as_view(), name='login'),
    # path('device_api/', DeviceAPI.as_view()),



    # Auth
    path('confirm-otp/', UserOTPConfirm.as_view(), name='confirm-otp'),
    path('confirm-otp-set-pin/', UserOTPConfirmSetPin.as_view(), name='confirm-otp'),
    path('set-pin/', SetPinAPI.as_view(), name='set-pin'),
    path('reset_pin_v2/', ResetPin.as_view(), name="reset_pin"),
    path('check_version_v2/', Version_Check_v2.as_view(), name="Version_Check_v2"),

    # User Details
    path('address/', Address.as_view(), name='address'),
    path('address-all/', AllAddress.as_view(), name='all-address'),
    path('personal/', Personal.as_view(), name='personal'),
    path('professional/', Professional.as_view(), name='professional'),
    path('bank/', Bank.as_view(), name='bank'),
    path('reference/', Reference.as_view(), name='reference'),
    path('document/', Document.as_view(), name='document'),
    path('document_new/',Document_new.as_view()),
    path('user-accounts-list/', UserAccountsListAPIView.as_view(), name='user-accounts-list'),
    path('user-profile/', UserProfile.as_view(), name='user-profile'),

    # user raw data
    path('contacts/', Contacts.as_view(), name='contacts'),
    path('contacts_new/', ContactsNew.as_view(), name='contacts_new'),
    path('share-app/', ShareApp.as_view(), name='share-app'),
    path('install-app/', InstalledAppAPI.as_view(), name='install-app'),
    path('location/', LocationTrackingAPI.as_view(), name='location'),
    path('device-data/', UserDeviceData.as_view(), name='device-data'),

    # Paytm -PG
    path('checksum-subscription-web/', ChecksumGenerate_Subcription.as_view(), name='checksum-subscription-web'),
    path('checksum-repayment-web/', ChecksumGeneratePaytm_Producation.as_view(), name='checksum-repayment-web'),
    path('subscription_payment_mobile/', SubscriptionPaymentMobile.as_view(), name='subscription_payment_mobile'),
    path('loan_payment_mobile/', PaytmPaymentMobileAPIView.as_view(), name='loan_payment_mobile'),
    path('subcription_status_check/', SubcriptionStatusCheck.as_view(), name='subcription_status_check'),


    ######
    # Paytm On-Us
    # Get Payable Amount Details on Paytm
    path('get_payment_details/', BillFetchAPI_v2.as_view(), name='get_payment_details'),
    path('paytm_bill_payment_api/', PaytmPaymentAPIView.as_view(), name='paytm_bill_payment'),

    # other
    path('pin-code-validate/', ValidatePincodeAPIView.as_view(), name='pin-code-validate'),
    path('pancard-validate/', ValidatePanCardVAPIView.as_view(), name='pancard-validate'),
    path('pancard-validate-fundspi/', FundspiPanAPI.as_view(), name='pancard-validate-fundspi'),

    # SMS
    path('sms-keyword/', SmsKeyWordListAPI.as_view(), name='sms-keyword'),
    path('sms-import/', SmsImportView.as_view(), name='sms-import'),

    # dashboard
    path('dashboard-master-api/', DashboardDataAPI.as_view(), name='dashboard-master-api'),
    path('dashboard-month-api/', DashboardDataMonthAPI.as_view(), name='dashboard-month-api'),
    path('dashboard-details-api/', DashboardDetailsAPIView.as_view(), name='dashboard-details-api'),
    path('dashboard-month-wise-account-list-api/', DashboardBankDetailsAPIView.as_view(),
         name='dashboard-bank-list-api'),
    path('dashboard-bank-details-api/', DashboardBankDetailsListAPIView.as_view(),
         name='dashboard-bank-details-api'),
    path('dashboard-credit-card-details-api/', DashboardCreditCardDetailsListAPIView.as_view(),
         name='dashboard-credit-card-details-api'),
    path('dashboard-debit-card-details-api/', DashboardDebitCardDetailsListAPIView.as_view(),
         name='dashboard-debit-card-details-api'),
    path('dashboard-merchant-details-api/', DashboardMerchantDetailsListAPIView.as_view(),
         name='dashboard-merchant-details-api'),

    # loan
    path('loan-drop-down-option/', Loan_Drop_Down_Option.as_view(),
         name='loan-drop-down-option'),
    path('onhold-loan-time/', OnHold_Loan_Time_View.as_view(),
         name='onhold-loan-time'),
    path('loan-history/', LoanHistory.as_view(), name='loan-history'),
    path('loan-details/', LoanDetails.as_view(), name='loan-details'),
    path('user-repayment/', UserRepayment.as_view(), name='user-repayment'),
    path('loan-repayment/', LoanRepayment.as_view(), name='loan-repayment'),
    path('approved-loan-details/', CurrentLoanDetail.as_view(), name='approved-loan-details'),
    # path('loan-amount-list/', loan_amount_list.as_view(), name='loan_amount_list'),
    path('details-is-complete/', IsUserDetailsComplete.as_view(), name='details-is-complete'),
    path('loan-submit/', SubmitLoanApplicationAPIView.as_view(), name='loan-submit'),

    path('pdf-generator/<int:pk>/<int:application_id>/', create_pdf, name='pdf_generator'),

    # Development
    path('pan_test/', pan_active_loan_test, name='pan_test'),
    path('paytm_create_link_api/', paytm_create_link_api, name='paytm_create_link_api'),
    path('fetch_all_link/', fetch_all_link, name='fetch_all_link'),
    path('fetch_transaction/', fetch_transaction, name='fetch_transaction'),

    # UAt Testing
    path('fetch_all_uat/', FetchAllLink.as_view(), name='fetch_all_uat'),
    path('fetch_link_uat/', FetchLink.as_view(), name='fetch_link_uat'),
    path('create_link_uat/', CreateLink.as_view(), name='create_link_uat'),
    path('resend_notification_link_uat/', ResendNotificationLink.as_view(), name='resend_notification_link_uat'),

    # Test
    path('fcm_test/', fcm_test, name='fcm_test'),
    path('new-version/', create_new_version, name='new-version/'),
    path('reset-pin/', reset_pin.as_view(), name='reset-pin'),
    path('image_upload/', image_upload, name='image_upload'),

    # leandingadda
    path('lead_register/', LendingAddaUserRegister.as_view(), name='leand register'),
    path('lead_approved/', LendingAddaApproved.as_view(), name='leand approved'),

    # Yelo
    path('y_leads/', GetYeloLeads.as_view(), name='y_leads'),
    path('user_reg_report/', YeloUserRegisterReport.as_view(), name='user_reg_report'),

    # leadingadda
    path('leandingadda_leads/', GetLendingAddaLeads.as_view(), name='leandingadda leads'),
    path('send_registerReport_data/', send_install_data, name='send_registerReport_data'),
    path('send_approved_data/', send_approved_data, name='send_approved_data'),

    # lendingAdda test
    path('leandingadda_approved/', LendingAddaApproved.as_view(), name='leandingadda approved'),

    # User Shared Data

    path('shared_count/', shareddata_Count.as_view(), name='user_shared_dataleads'),
    path('shared_data/', SharedData.as_view(), name='user_shared_dataleads'),
    path('register_data/', conversion.as_view(), name='user_shared_dataleads'),
    path('sub_insentive/', sub_insentive.as_view(), name='user_shared_dataleads'),
    path('app_insentive/', approve_insentive.as_view(), name='user_shared_dataleads'),
    path('Not_registered/', Not_registered.as_view(), name='user_shared_dataleads'),

    # old data testing
    path('olddata/', GetLoanAppId.as_view(), name='old_data'),
    path('newdata/', GetNewData.as_view(), name='new_data'),

    path('leandingadda_RegisterReport/', LendingAddaUserRegister.as_view(), name='leandingadda report'),

    # Tecdo
    path('tecdo_leads/', GetTecdoLeads.as_view(), name='tecdo_leads'),
    path('tecdo_reg_report/', TecdoUserRegisterReport.as_view(), name='tecdo_reg_report'),

    # Bussion Ass
    path('shared_data/', SharedData.as_view(), name='user_shared_dataleads'),
    path('shared_count/', shareddata_Count.as_view(), name='shared_count'),
    path('register_data/', conversion.as_view(), name='user_shared_dataleads'),

    path('sub_insentive/', sub_insentive.as_view(), name='user_shared_dataleads'),
    path('app_insentive/', approve_insentive.as_view(), name='user_shared_dataleads'),
    path('Not_registered/', Not_registered.as_view(), name='user_shared_dataleads'),

    path('test_trans/', test_trans, name='test trans'),

    path('Sub_Transaction_Status_Check_Static/', Sub_Transaction_Status_Check_Static,
         name='Sub_Transaction_Status_Check_Static'),
    path('Transaction_Status_Check_Static/', Transaction_Status_Check_Static,
         name='Transaction_Status_Check_Static'),
    path('razorpay/', RazorPayOrder.as_view(), name='razorpay'),

    path('sign-verify/', RazorPaySignCheck.as_view(), name='sign_verify'),
    path('webhook-order/', RazorpayWebhookOrder.as_view(), name='webhook-order'),

    path('fail_case/', RazorPay_FailCase.as_view(), name='fail_case'),

    path('Sub_Transaction_Status_Check_Static/', Sub_Transaction_Status_Check_Static,
         name='Sub_Transaction_Status_Check_Static'),
    path('Transaction_Status_Check_Static/', Transaction_Status_Check_Static, name='Transaction_Status_Check_Static'),

    # PAytm S2S
    path('PaytmS2SRepayment/', PaytmS2SRepayment.as_view(), name='PaytmS2SRepayment'),
    path('PaytmS2SSub/', PaytmS2SSub.as_view(), name='PaytmS2SSub'),
    path('check_user/', CheckUser.as_view(), name='check-user'),

    # return token api
    path("return_token_mudrakwik_user/", ReturnTokenMudrakwikUser.as_view(), name='ReturnTokenMudrakwikUser'),
    path("return_token_rupeekwik_user/", ReturnTokenRupeekwikUser.as_view(), name='ReturnTokenRupeekwikUser'),

    # Razorpay API
    path('create_razorpay_order/', CreateRazorpayOrder.as_view()),
    path('call_back_update_authorization_payment/', CallbackUpdateAuthorizationPayment.as_view()),

    # e-nach web hook
    path("enach_web_hook/", EnachRazorpayWebHook.as_view()),
    path("loan_application_disbursed_amount_wallet/", LoanApplicationDisbursedAmountWalletApi.as_view(), name='LoanApplicationDisbursedAmountWalletApi'),
    path("loan_application_withdraw/", LoanApplicationWithdrawApi.as_view(), name='LoanApplicationWithdrawApi'),
    path('all_loan_details/', AllLoanDetails.as_view()),

    path('loan_slab_details/', LoanSlabDetails.as_view()),
    path('checksum_generate_paytm_loan_repayment/', ChecksumGeneratePaytm_LoanRepayment.as_view()),
    path('user_loan_agreement/', user_loan_agreement),
    path('loan_graph_amount/', loan_graph_amount),
    path('loan_graph_amount_1/', loan_graph_amount_1),

    path('razor_customer_create/', RazorCustomerCreate.as_view()),
    path('create_razorpay_order/', CreateRazorpayOrder.as_view()),
    path('fetch_token_customer_id/', FetchTokenCustomerId.as_view()),
    path('fetch_token_payment_id/', FetchTokenPaymentId.as_view()),
    path('call_back_update_authorization_payment/', CallbackUpdateAuthorizationPayment.as_view()),
    path('check_status/', CheckStatus.as_view()),


    #check mobile block
    path("check_mobile_block/", CheckMobileBlock.as_view()),

    # Loan banner
    path("loan_banner/",LoanBannerApi.as_view()),

    path("defaulter_check_in_all_apps/", CheckDefaulterInAll.as_view()),

    path("interlink_loan/", pancard_used_in_active_loan_interlink.as_view()),

]
