import datetime
from functools import partial
from random import randint

import requests
from django.apps.registry import apps
from django.conf import settings
from django.contrib.auth.models import User
from django.http import HttpResponse
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.views import APIView
from fcm_django.models import FCMDevice
from MApi.email import sms

from . import auth_serializer
from .permissions import GuestTokenPermission, GuestTokenPermissionSetPin, GuestTokenPermissionFCM, \
    GuestTokenPermissionRupeekwik, \
    GuestTokenPermissionRawData, GuestTokenPermissionInvite, GuestTokenPermissionMudrakwik

import datetime
from datetime import timedelta

Otp = apps.get_model('User', 'Otp')
AuthenticationPin = apps.get_model('User', 'AuthenticationPin')
Subscription = apps.get_model('User', 'Subscription')
Payment = apps.get_model('Payments', 'Payment')
AppVersionCheck = apps.get_model('StaticData', 'AppVersionCheck')

PersonalDetails = apps.get_model('User', 'Personal')
BankDetails = apps.get_model('User', 'Bank')
AddressDetails = apps.get_model('User', 'Address')
ReferenceDetails = apps.get_model('User', 'Reference')
DocumentDetails = apps.get_model('User', 'Documents')
ProfessionalDetails = apps.get_model('User', 'Professional')
UserOffer = apps.get_model('ecom', 'UserOffer')


class LoginRegisterAPI(APIView):
    """
        API For User Login/Register
        :parameter:
            Medhod: POST
            Parameter

            {
                "mobile_number": "IntegerField",     # 9763630853
                "invitation_code": "CharField",      # MU1122
                "device_id" : "CharField",          # efq-eo2dc-23kl12op
            }

            Response

            {
                "status": "CharField",                  # 0
                "message": "ChatField",                 # OTP has been sent to 9763630853
                "mobile_number": "IntegerField",        # 9763630853
                "invitation_code": "CharField",         # MU1122
                "device_id": "CharField",               # efq-eo2dc-23kl12op
                "new_user" : "BooleanField",            # true
            }

            Example:
            {
			    "status": "1",
			    "message": "OTP has been sent to 7875583679",
			    "mobile_number": "7875583679",
			    "device_id": "efq-eo2dc-23kl12op",
			    "invitation_code": "",
			    "new_user": false,
			    "otp": 212586
			}

            Note

            Guest token required

            guest-token : 'fae710e0-18bf-11e9-ab14-d663bd873d93'

            status 1 : User is already registered
            status 1 : OTP send and invitation_code blank
            status 1 : OTP send and invitation_code valid
            status 2 : OTP send and invitation_code in-valid
            status 3 : User already registered and add invitation code
            status 10 : Entered data is in-valid (Validation Error)
            status 11 : Internal server error (Django Error)

    """

    # permission_classes = (partial(GuestTokenPermission, ['GET', 'POST', 'HEAD']),)
    serializer_class = auth_serializer.LoginRegisterSerializer

    def post(self, request, *args, **kwargs):

        otp = None
        status = None
        message = 'Something went wrong'
        mobile_number = None
        device_id = None
        invitation_code = None
        new_user = None
        sms_otp = None

        user_login_serializer = self.serializer_class(data=request.data)

        if user_login_serializer.is_valid():
            mobile_number = request.data.get('mobile_number')

            device_id = request.data.get('device_id')
            if mobile_number:
                status = '11'
                message = 'Enter valid data'
                from StaticData.views import check_mobile_block
                check_block_mobile = check_mobile_block(mobile_number, 'ck')
                if check_block_mobile == True:
                    return Response({
                        "status": status,
                        "message": message
                    })
            # Check weather user is already register or not

            user_instance = User.objects.filter(username=mobile_number)

            if not user_instance:

                # Generate otp
                base_otp = randint(111111, 999995)

                api_otp = base_otp
                sms_otp = base_otp + 4

                User.objects.create(username=mobile_number, is_active=True, is_staff=False)
                user = User.objects.filter(username=mobile_number)

                Otp.objects.create(
                    user_id=user[0].id,
                    otp=sms_otp, is_expired=False
                )

                # Send otp sms message
                sms_message = 'Dear customer,\n Your OTP for CreditKart is ' + str(sms_otp)
                # sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
                #     settings.TSMS_USERNAME) + '&password=' + str(
                #     settings.TSMS_PASSWORD) + '&type=0&dlr=1&destination=' + str(mobile_number) + '&source=' + str(
                #     settings.SMS_SOURCE2) + '&message=' + str(sms_message) + '&tempid=' + str("1007273884268580801")

                sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
                    settings.TSMS_USERNAME) + '&password=' + str(
                    settings.TSMS_PASSWORD) + '&type=0&dlr=1&destination=' + str(mobile_number) + '&source=' + str(
                    settings.TSMS_SOURCE) + '&message=' + str(sms_message)

                requests.get(sms_url)

                status = '1'
                message = 'OTP has been sent to ' + str(mobile_number)
                otp = api_otp
                mobile_number = mobile_number
                device_id = str(device_id)
                invitation_code = ''
                new_user = True

            else:

                print('old user')

                if not invitation_code:
                    print('without invitation code')

                    # Generate otp
                    base_otp = randint(111111, 999995)

                    api_otp = base_otp
                    sms_otp = base_otp + 4
                    user = User.objects.filter(username=mobile_number)
                    Otp.objects.create(
                        user_id=user[0].id,
                        otp=sms_otp, is_expired=False
                    )

                    # Send otp sms message
                    sms_message = 'Dear customer,\n Your OTP for CreditKart is ' + str(sms_otp)
                    # sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
                    #     settings.SMS_USERNAME) + '&password=' + str(
                    #     settings.SMS_PASSWORD) + '&type=0&dlr=1&destination=' + str(
                    #     mobile_number) + '&source=' + str(settings.SMS_SOURCE2) + '&message=' + str(
                    #     sms_message) + '&tempid=' + str("1007273884268580801")

                    sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
                        settings.TSMS_USERNAME) + '&password=' + str(
                        settings.TSMS_PASSWORD) + '&type=0&dlr=1&destination=' + str(
                        mobile_number) + '&source=' + str(settings.TSMS_SOURCE) + '&message=' + str(
                        sms_message)

                    requests.get(sms_url)

                    status = '1'
                    message = 'OTP has been sent to ' + str(mobile_number)
                    otp = api_otp
                    mobile_number = mobile_number
                    device_id = str(device_id)
                    invitation_code = ''
                    new_user = False

                else:

                    status = '3'
                    message = 'Already registered user cannot add invitation code'

        else:

            status = '10'
            message = 'Enter valid data'
        # TODO remove otp from response
        return Response(
            {
                'status': status,
                'message': message,
                'mobile_number': mobile_number,
                'device_id': device_id,
                'invitation_code': invitation_code,
                'new_user': new_user,
                'otp': sms_otp
            }
        )


class LoginRegisterAPI_web(APIView):
    """
        API For User Login/Register
        :parameter:
            Medhod: POST
            Parameter
            {
                "mobile_number": "IntegerField",     # 9763630853
                "invitation_code": "CharField",      # MU1122
                "device_id" : "CharField",          # efq-eo2dc-23kl12op
            }
            Response
            {
                "status": "CharField",                  # 0
                "message": "ChatField",                 # OTP has been sent to 9763630853
                "mobile_number": "IntegerField",        # 9763630853
                "invitation_code": "CharField",         # MU1122
                "device_id": "CharField",               # efq-eo2dc-23kl12op
                "new_user" : "BooleanField",            # true
            }
            Example:
            {
             "status": "1",
             "message": "OTP has been sent to 7875583679",
             "mobile_number": "7875583679",
             "device_id": "efq-eo2dc-23kl12op",
             "invitation_code": "",
             "new_user": false,
             "otp": 212586
         }
            Note
            Guest token required
            guest-token : 'fae710e0-18bf-11e9-ab14-d663bd873d93'
            status 1 : User is already registered
            status 1 : OTP send and invitation_code blank
            status 1 : OTP send and invitation_code valid
            status 2 : OTP send and invitation_code in-valid
            status 3 : User already registered and add invitation code
            status 10 : Entered data is in-valid (Validation Error)
            status 11 : Internal server error (Django Error)
    """
    serializer_class = auth_serializer.LoginRegisterSerializer

    def post(self, request, *args, **kwargs):
        otp = None
        status = None
        message = 'Something went wrong'
        mobile_number = None
        device_id = None
        invitation_code = None
        new_user = None
        sms_otp = None
        user_login_serializer = self.serializer_class(data=request.data)
        if user_login_serializer.is_valid():
            # try:
            mobile_number = request.data.get('mobile_number')
            # invitation_code = request.data.get('invitation_code')
            device_id = request.data.get('device_id')
            if mobile_number:
                status = '11'
                message = 'Enter valid data'
                from StaticData.views import check_mobile_block
                check_block_mobile = check_mobile_block(mobile_number, 'ck')
                if check_block_mobile == True:
                    return Response({
                        "status": status,
                        "message": message
                    })
            # Check weather user is already register or not
            user_instance = User.objects.filter(username=mobile_number)
            if not user_instance:
                # Generate otp
                base_otp = randint(111111, 999995)
                api_otp = base_otp
                sms_otp = base_otp + 4
                User.objects.create(username=mobile_number, is_active=True, is_staff=False)
                user = User.objects.filter(username=mobile_number)

                Otp.objects.create(
                    user_id=user[0].id,
                    otp=sms_otp, is_expired=False
                )

                # Send otp sms message
                sms_message = 'Dear customer,\n Your OTP for CreditKart is ' + str(sms_otp)
                # sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
                #     settings.TSMS_USERNAME) + '&password=' + str(
                #     settings.TSMS_PASSWORD) + '&type=0&dlr=1&destination=' + str(mobile_number) + '&source=' + str(
                #     settings.SMS_SOURCE2) + '&message=' + str(sms_message) + '&tempid=' + str("1007273884268580801")

                sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
                    settings.TSMS_USERNAME) + '&password=' + str(
                    settings.TSMS_PASSWORD) + '&type=0&dlr=1&destination=' + str(mobile_number) + '&source=' + str(
                    settings.TSMS_SOURCE) + '&message=' + str(sms_message)
                requests.get(sms_url)
                status = '1'
                message = 'OTP has been sent to ' + str(mobile_number)
                otp = api_otp
                mobile_number = mobile_number
                device_id = str(device_id)
                invitation_code = ''
                new_user = True

            else:
                if not invitation_code:
                    # Generate otp
                    base_otp = randint(111111, 999995)
                    api_otp = base_otp
                    sms_otp = base_otp + 4
                    user = User.objects.filter(username=mobile_number)
                    Otp.objects.create(
                        user_id=user[0].id,
                        otp=sms_otp, is_expired=False
                    )
                    # Send otp sms message
                    sms_message = 'Dear customer,\n Your OTP for CreditKart is ' + str(sms_otp)
                    # sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
                    #     settings.SMS_USERNAME) + '&password=' + str(
                    #     settings.SMS_PASSWORD) + '&type=0&dlr=1&destination=' + str(
                    #     mobile_number) + '&source=' + str(settings.SMS_SOURCE2) + '&message=' + str(
                    #     sms_message) + '&tempid=' + str("1007273884268580801")

                    sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
                        settings.TSMS_USERNAME) + '&password=' + str(
                        settings.TSMS_PASSWORD) + '&type=0&dlr=1&destination=' + str(
                        mobile_number) + '&source=' + str(settings.TSMS_SOURCE) + '&message=' + str(
                        sms_message)
                    requests.get(sms_url)
                    status = '1'
                    message = 'OTP has been sent to ' + str(mobile_number)
                    otp = api_otp
                    mobile_number = mobile_number
                    device_id = str(device_id)
                    invitation_code = ''
                    new_user = False

                else:
                    status = '3'
                    message = 'Already registered user cannot add invitation code'


        else:
            status = '10'
            message = 'Enter valid data'
        # TODO remove otp from response
        return Response(
            {
                'status': status,
                'message': message,
                'mobile_number': mobile_number,
                'device_id': device_id,
                'invitation_code': invitation_code,
                'new_user': new_user,
                'otp': sms_otp
            }
        )


class DevLoginRegisterAPI(APIView):
    permission_classes = (partial(GuestTokenPermission, ['GET', 'POST', 'HEAD']),)
    serializer_class = auth_serializer.LoginRegisterSerializer

    def post(self, request, *args, **kwargs):

        otp = None
        status = None
        message = 'Something went wrong'
        mobile_number = None
        device_id = None
        invitation_code = None
        new_user = None

        user_login_serializer = self.serializer_class(data=request.data)

        if user_login_serializer.is_valid():

            # try:

            mobile_number = request.data.get('mobile_number')
            # invitation_code = request.data.get('invitation_code')
            device_id = request.data.get('device_id')

            # Check weather user is already register or not

            user_instance = User.objects.filter(username=mobile_number)

            if not user_instance:

                # Generate otp
                sms_otp = '123456'

                User.objects.create(username=mobile_number, is_active=True, is_staff=False)
                user = User.objects.filter(username=mobile_number)

                pin_str = str(mobile_number)
                pin = pin_str[-4:]
                user_instance = User.objects.get(username=mobile_number)
                if user_instance.is_active:
                    user_pin_instance_obj = AuthenticationPin.objects.filter(user=user_instance,
                                                                             type=type,
                                                                             pin=pin)
                    if not user_pin_instance_obj:
                        AuthenticationPin.objects.create(
                            pin=pin,
                            user=user_instance,
                            type=type,
                        )


                Otp.objects.create(
                    user_id=user[0].id,
                    otp=sms_otp, is_expired=False
                )

                # Send otp sms message
                sms_message = 'Dear customer,\n Your OTP for CreditKart is ' + str(sms_otp)
                # sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
                #     settings.SMS_USERNAME) + '&password=' + str(
                #     settings.SMS_PASSWORD) + '&type=0&dlr=1&destination=' + str(mobile_number) + '&source=' + str(
                #     settings.SMS_SOURCE2) + '&message=' + str(sms_message) + '&tempid=' + str("1007273884268580801")

                sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
                    settings.TSMS_USERNAME) + '&password=' + str(
                    settings.TSMS_PASSWORD) + '&type=0&dlr=1&destination=' + str(mobile_number) + '&source=' + str(
                    settings.TSMS_SOURCE) + '&message=' + str(sms_message)
                # TODO Uncomment otp
                requests.get(sms_url)

                status = '1'
                message = 'OTP has been sent to ' + str(mobile_number)
                otp = sms_otp
                mobile_number = mobile_number
                device_id = str(device_id)
                invitation_code = ''
                new_user = True

            else:

                if not invitation_code:

                    # Generate otp

                    sms_otp = '123456'
                    user = User.objects.filter(username=mobile_number)
                    Otp.objects.create(
                        user_id=user[0].id,
                        otp=sms_otp, is_expired=False
                    )

                    # Send otp sms message
                    sms_message = 'Dear customer,\n Your OTP for CreditKart is ' + str(sms_otp)
                    # sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
                    #     settings.SMS_USERNAME) + '&password=' + str(
                    #     settings.SMS_PASSWORD) + '&type=0&dlr=1&destination=' + str(
                    #     mobile_number) + '&source=' + str(settings.SMS_SOURCE2) + '&message=' + str(
                    #     sms_message) + '&tempid=' + str("1007273884268580801")

                    sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
                        settings.TSMS_USERNAME) + '&password=' + str(
                        settings.TSMS_PASSWORD) + '&type=0&dlr=1&destination=' + str(
                        mobile_number) + '&source=' + str(settings.TSMS_SOURCE) + '&message=' + str(
                        sms_message)
                    # TODO Uncomment otp
                    requests.get(sms_url)

                    status = '1'
                    message = 'OTP has been sent to ' + str(mobile_number)
                    otp = sms_otp
                    mobile_number = mobile_number
                    device_id = str(device_id)
                    invitation_code = ''
                    new_user = False

                else:

                    status = '3'
                    message = 'Already registered user cannot add invitation code'


        else:

            status = '10'
            message = 'Enter valid data'
        # TODO remove otp from response
        return Response(
            {
                'status': status,
                'message': message,
                'mobile_number': mobile_number,
                'device_id': device_id,
                'invitation_code': invitation_code,
                'new_user': new_user,
                'otp': sms_otp
            }
        )


class UserOTPConfirm(APIView):
    """
    To verify user otp

    :parameter:

        Parameter

        {
            "mobile_number": "CharField",
            "otp": "CharField"
        }

        Response

        {
            "status": "CharField",
            "message": "CharField"
        }

        Example:
        {
            "status": "1",
            "message": "OTP verified successfully"
        }

        Note

            Guest token required

            guest-token-otp-verify : 'f183sJ7646r1tBOxcMWW097VfpvwFEss'

            status 1 : OTP verified successfully
            status 2 : Invalid otp
            status 10 : Entered data is in-valid (Validation Error)
            status 11 : Internal server error (Django Error)

    """

    # permission_classes = (partial(permissions.GuestTokenPermissionOTP, ['GET', 'POST', 'HEAD']),)
    serializer_class = auth_serializer.OTPVerifySerializer

    def post(self, request, *args, **kwargs):

        status = None
        message = None

        serializer_instance = self.serializer_class(data=request.data)
        try:
            if serializer_instance.is_valid():
                mobile_no = request.data.get('mobile_number')
                user = User.objects.filter(username=request.data.get('mobile_number'))

                otp_instance = Otp.objects.filter(
                    user_id=user[0].id, otp=request.data.get("otp"), is_expired=False
                ).order_by('-id')[0]

                if otp_instance:
                    now = datetime.datetime.now()
                    date2 = otp_instance.created_date
                    diff = now - date2

                    if diff.seconds < 30:

                        if otp_instance.otp == request.data.get('otp'):

                            status = '1'
                            message = 'OTP verified successfully'


                            pin_str = str(mobile_no)
                            pin = pin_str[-4:]
                            user_instance = User.objects.get(username=mobile_no)
                            if user_instance.is_active:
                                user_pin_instance_obj = AuthenticationPin.objects.filter(user=user_instance,
                                                                                         type=type,
                                                                                         pin=pin)
                                if not user_pin_instance_obj:
                                    AuthenticationPin.objects.create(
                                        pin=pin,
                                        user=user_instance,
                                        type=type,
                                    )

                        else:

                            status = '2'
                            message = 'Invalid OTP'
                    else:
                        try:
                            Otp.objects.filter(
                                user_id=user[0].id, is_expired=False).delete()
                            status = '3'
                            message = 'Your OTP is Expired'
                        except:
                            status = '3'
                            message = 'Your OTP is Expired'

                else:

                    status = '2'
                    message = 'Invalid OTP'

            else:

                status = '11'
                message = 'Please enter valid details'
        except:

            status = '10'
            message = 'Something went wrong'

        # Hard Coding status for otp
        # TODO delete this
        status = '1'
        message = 'OTP verified successfully'
        return Response(
            {
                'status': status,
                'message': message
            }
        )


class UserOTPConfirmSetPin(APIView):
    """
    To verify user otp

    :parameter:

        Parameter

        {
            "mobile_number": "CharField",
            "otp": "CharField"
        }

        Response

        {
            "status": "CharField",
            "message": "CharField"
        }

        Example:
        {
            "status": "1",
            "message": "OTP verified successfully"
        }

        Note

            Guest token required

            guest-token-otp-verify : 'f183sJ7646r1tBOxcMWW097VfpvwFEss'

            status 1 : OTP verified successfully
            status 2 : Invalid otp
            status 10 : Entered data is in-valid (Validation Error)
            status 11 : Internal server error (Django Error)

    """

    serializer_class = auth_serializer.OTPVerifySerializer
    serializer_class_1 = auth_serializer.SetPinSerializer

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        serializer_instance = self.serializer_class(data=request.data)
        try:
            if serializer_instance.is_valid():
                mobile_no = request.data.get('mobile_number')
                user = User.objects.filter(username=mobile_no).first()

                otp_instance = Otp.objects.filter(
                    user_id=user.id, otp=request.data.get("otp"), is_expired=False
                ).order_by('id').last()

                if otp_instance:
                    now = datetime.datetime.now()
                    date2 = otp_instance.created_date
                    diff = now - date2

                    if diff.seconds < 30:
                        if otp_instance.otp == request.data.get('otp'):
                            status = None
                            message = None
                            type = None
                            set_pin_serializer_class = self.serializer_class_1(data=request.data)
                            if set_pin_serializer_class.is_valid():
                                mobile_number = request.data.get('mobile_number')
                                pin = request.data.get('pin')
                                type = "F"
                                user_instance = User.objects.get(username=mobile_number)
                                if user_instance.is_active:
                                    user_pin_instance_obj = AuthenticationPin.objects.filter(user=user_instance,
                                                                                             type=type,
                                                                                             pin=pin)
                                    if user_pin_instance_obj:
                                        status = '1'
                                        message = 'Pin verified successfully.'

                                    else:
                                        AuthenticationPin.objects.create(
                                            pin=pin,
                                            user=user_instance,
                                            type=type,
                                        )

                                        status = '2'
                                        message = 'Pin Set Successfully...!'

                            else:
                                status = '3'
                                message = 'Enter valid data'
                            return Response(
                                {
                                    'status': status,
                                    'message': message,
                                }
                            )

                        else:
                            status = '4'
                            message = 'Invalid OTP'

                    else:
                        try:
                            Otp.objects.filter(
                                user_id=user[0].id, is_expired=False).delete()
                            status = '5'
                            message = 'Your OTP is Expired'
                        except:
                            status = '6'
                            message = 'Your OTP is Expired'

                else:

                    status = '4'
                    message = 'Invalid OTP'

            else:

                status = '7'
                message = 'Please enter valid details'

            return Response(
                {
                    'status': status,
                    'message': message
                }
            )

        except:

            status = '8'
            message = 'Something went wrong'

            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


def all_is_completed(user_id):
    status = "0"
    personal = PersonalDetails.objects.filter(user_id=user_id)
    address = AddressDetails.objects.filter(user_id=user_id).exclude(type="Shipping")

    if not personal:
        status = "1"
    elif not address:
        status = "2"
    elif personal and address:
        status = "0"
    return status


class SetPinAPI_OLD(APIView):
    """
    To create/check user pin

    :parameter:

        Parameters

        {
            "mobile_number": "CharField",               # 9988776655
            "pin": "IntegerField",                      # 1345
            "device_id": "CharField",                   # 34229394483283838322923
            "invitation_code": "CharField",             # FEO2E1 ( This is optional. Required in case of reg. using invite)
        }

        Response

        {
            "status": "CharField",                      # 1
            "message": "CharField",                     # Pin verified successfully.
            "token": "CharField",                       # 0744beea9104dc3b37b50eb2ac12b7f6803b96bf
            "invitation_code" : "CharField",            # FEO2E1
        }

        Example:
        {
            "status": "1",
            "message": "Pin verified successfully.",
            "token": "495cb833ef3608930ad8d36dabf7b28675afcd62",
            "pin": "1234",
            "invitation_code": null
        }


        Note

        Guest token required

        "guest-token-set-pin" : "cae110v0-22of-53d9-vx13-r666bd873ff4"

        status 1 : New user created and set new pin and add invitation code
        status 2 : Already user username and pin verified successfully
        status 3 : Already user but username or pin in-valid
        status 4 : Already user but in-active
        status 10 : Entered data is in-valid (Validation Error)
        status 11 : Something went wrong (Django Error)
        status 12 : User created but code is not valid so not added to my invites

        status  22:Incomplete Personal
        status  24:Incomplete Professional
        status  27:Incomplete Bank
        status  25:Incomplete salary slip Document,pan card Document,Company id
        status  21:Incomplete selfile Document,
        status  26:Incomplete reference Details
        status  23:Incomplete permanent address,current address

        status  1:complate profile


    """

    permission_classes = (partial(GuestTokenPermissionSetPin, ['GET', 'POST', 'HEAD']),)
    serializer_class = auth_serializer.SetPinSerializer

    def post(self, request, *args, **kwargs):


        status = None
        sub_status = None
        sub_remaining_days = None
        sub_start_date = None
        sub_end_date = None
        message = None
        token = None
        invitation_code = None
        user_pin = None
        today_date = datetime.datetime.now()
        versionCode = None
        profile_is_complete = None
        set_pin_serializer_class = self.serializer_class(data=request.data)
        if set_pin_serializer_class.is_valid():
            v_code = AppVersionCheck.objects.filter(status='Active')
            if v_code:
                versionCode = v_code[0].version_name
                mobile_number = request.data.get('mobile_number')
                pin = request.data.get('pin')
                device_id = request.data.get('device_id')
                user_instance = User.objects.get(username=mobile_number)
                if user_instance.is_active:

                    subcription_data = Subscription.objects.filter(user_id=user_instance.id,
                                                                   payment_status="success").order_by('-id').first()

                    if subcription_data:

                        if today_date <= subcription_data.expiry_date and 50 <= subcription_data.payment_id.amount:
                            sub_status = 1
                            sub_start_date = subcription_data.start_date
                            sub_end_date = subcription_data.expiry_date
                            days = sub_end_date - today_date
                            sub_remaining_days = days.days

                    user_pin_instance = AuthenticationPin.objects.filter(user=user_instance).last()
                    if user_pin_instance:
                        user_pin = user_pin_instance.pin
                        if user_pin == pin:
                            token, _ = Token.objects.get_or_create(user=user_instance)
                            token = token.key
                            status = '1'
                            message = 'Pin verified successfully.'
                        else:
                            status = '0'
                            message = 'Pin not Valid '

                    else:
                        AuthenticationPin.objects.create(
                            pin=pin,
                            user=user_instance,
                        )
                        user_pin = pin
                        token, _ = Token.objects.get_or_create(user=user_instance)
                        token = token.key
                        status = '2'
                        message = 'Pin Set Successfully...!'

                    user_data = User.objects.filter(username=mobile_number)
                    if user_data:
                        user_id_prof = user_data[0].id
                        profile_is_complete = all_is_completed(user_id_prof)


            else:
                status = '11'
                message = 'versionCode is null'
        else:
            status = '10'
            message = 'Enter valid data'
        return Response(
            {
                'status': status,
                'message': message,
                'token': token,
                'pin': user_pin,
                'invitation_code': invitation_code,
                'sub_status': sub_status,
                'sub_start_date': sub_start_date,
                'sub_end_date': sub_end_date,
                'sub_remaining_days': sub_remaining_days,
                'profile_is_complete': profile_is_complete,
                "versionCode": versionCode

            }
        )


class SetPinAPI(APIView):
    """
    To create/check user pin
    :parameter:
        Parameters
        {
            "mobile_number": "CharField",               # 9988776655
            "pin": "IntegerField",                      # 1345
            "device_id": "CharField",                   # 34229394483283838322923
            "invitation_code": "CharField",             # FEO2E1 ( This is optional. Required in case of reg. using invite)
            "type": "C",                                # C/F/M (type for creditkart, fincom or mudrakwik)
        }
        Response
        {
            "status": "CharField",                      # 1
            "message": "CharField",                     # Pin verified successfully.
            "token": "CharField",                       # 0744beea9104dc3b37b50eb2ac12b7f6803b96bf
            "invitation_code" : "CharField",            # FEO2E1
        }
        Example:
        {
            "status": "1",
            "message": "Pin verified successfully.",
            "token": "495cb833ef3608930ad8d36dabf7b28675afcd62",
            "pin": "1234",
            "invitation_code": null
        }
        Note
        Guest token required
        "guest-token-set-pin" : "cae110v0-22of-53d9-vx13-r666bd873ff4"
        status 1 : New user created and set new pin and add invitation code
        status 2 : Already user username and pin verified successfully
        status 3 : Already user but username or pin in-valid
        status 4 : Already user but in-active
        status 10 : Entered data is in-valid (Validation Error)
        status 11 : Something went wrong (Django Error)
        status 12 : User created but code is not valid so not added to my invites
        status  22:Incomplete Personal
        status  24:Incomplete Professional
        status  27:Incomplete Bank
        status  25:Incomplete salary slip Document,pan card Document,Company id
        status  21:Incomplete selfile Document,
        status  26:Incomplete reference Details
        status  23:Incomplete permanent address,current address
        status  1:complate profile
    """
    permission_classes = (partial(GuestTokenPermissionSetPin, ['GET', 'POST', 'HEAD']),)
    serializer_class = auth_serializer.SetPinSerializer

    def post(self, request, *args, **kwargs):
        # parameter's
        status = None
        sub_status = None
        sub_remaining_days = None
        sub_start_date = None
        sub_end_date = None
        message = None
        token = None
        offer_exist = False
        invitation_code = None
        user_pin = None
        today_date = datetime.datetime.now()
        versionCode = None
        profile_is_complete = None
        type = None
        set_pin_serializer_class = self.serializer_class(data=request.data)
        if set_pin_serializer_class.is_valid():
            v_code = AppVersionCheck.objects.filter(status='Active')
            if v_code:
                versionCode = v_code[0].version_name
                mobile_number = request.data.get('mobile_number')
                pin = request.data.get('pin')
                device_id = request.data.get('device_id')
                # type = request.data.get('type')
                type = "F"
                user_instance = User.objects.get(username=mobile_number)
                if user_instance.is_active:
                    # Adding amount in wallet
                    try:

                        from EApi.wallet import add_default_wallet_amount
                        add_default_wallet_amount(mobile_number)
                    except Exception as e:
                        print("Inside Exception of add_default_wallet_amount in UserLogin API")

                    subcription_data = Subscription.objects.filter(user_id=user_instance.id,
                                                                   payment_status="success").order_by('-id').first()
                    if subcription_data:

                        if today_date <= subcription_data.expiry_date and 50 <= subcription_data.payment_id.amount:
                            sub_status = 1
                            sub_start_date = subcription_data.start_date
                            sub_end_date = subcription_data.expiry_date
                            days = sub_end_date - today_date
                            sub_remaining_days = days.days
                    user_pin_instance = AuthenticationPin.objects.filter(user=user_instance, type=type).last()
                    if user_pin_instance:
                        user_pin = user_pin_instance.pin
                        # TODO remove static pin
                        pin = user_pin_instance.pin
                        if user_pin == pin:
                            token, _ = Token.objects.get_or_create(user=user_instance)
                            token = token.key
                            status = '1'
                            message = 'Pin verified successfully.'
                            try:
                                offer_existance = UserOffer.objects.filter(user=user_instance, is_read=False)
                                if offer_existance:
                                    offer_exist = True
                            except Exception as e:
                                print("Inside Exception of offer_existance in SetPin API", e)
                        else:
                            status = '0'
                            message = 'Pin not Valid '
                    else:
                        AuthenticationPin.objects.create(
                            pin=pin,
                            user=user_instance,
                            type=type,
                        )
                        user_pin = pin
                        token, _ = Token.objects.get_or_create(user=user_instance)
                        token = token.key
                        status = '2'
                        message = 'Pin Set Successfully...!'
                    user_data = User.objects.filter(username=mobile_number)
                    if user_data:
                        user_id_prof = user_data[0].id
                        profile_is_complete = all_is_completed(user_id_prof)


            else:
                status = '11'
                message = 'versionCode is null'
        else:
            status = '10'
            message = 'Enter valid data'
        return Response(
            {
                'status': status,
                'message': message,
                'token': token,
                'pin': user_pin,
                'offer_exist': offer_exist,
                'invitation_code': invitation_code,
                'sub_status': sub_status,
                'sub_start_date': sub_start_date,
                'sub_end_date': sub_end_date,
                'sub_remaining_days': sub_remaining_days,
                'profile_is_complete': profile_is_complete,
                "versionCode": versionCode,
                "type": type
            }
        )


def create_get_user_pin(mobile_number, pin):
    # new user
    User.objects.get_or_create(username=mobile_number, is_active=True)
    new_user_instance = User.objects.get(username=mobile_number)

    # send sms and email code if required

    # create new user pin
    AuthenticationPin.objects.create(
        pin=pin,
        user=new_user_instance,
    )


class reset_pin(APIView):

    def post(self, request, *args, **kwargs):
        mobile_number = request.data.get('mobile_number')
        pin = request.data.get('pin')

        user_instance = User.objects.filter(username=mobile_number)


        user_pin_instance = AuthenticationPin.objects.filter(user_id=user_instance[0].id).update(pin=pin)
        if user_pin_instance:

            sms_message = "Dear valued customer, your pin is set successfully"
            # TODO Extra msg from Kiran..
            sms_kwrgs = {
                'sms_message': sms_message,
                'number': str(mobile_number)
            }
            sms(**sms_kwrgs)
        else:
            AuthenticationPin.objects.create(user_id=user_instance[0].id, pin=pin)
        return HttpResponse("done")


class ResetPin(APIView):
    permission_classes = (partial(GuestTokenPermission, ['GET', 'POST', 'HEAD']),)

    def post(self, request, *args, **kwargs):
        if 'mobile_number' in request.data and 'pin' in request.data:
            mobile_number = request.data.get('mobile_number', )
            pin = request.data.get('pin')

            if mobile_number is not None and mobile_number and pin is not None and pin:
                user_instance = User.objects.filter(username=mobile_number)

                if user_instance is not None and user_instance:
                    user_pin_instance = AuthenticationPin.objects.filter(user_id=user_instance[0].id).order_by(
                        '-id').first()
                    if user_pin_instance:
                        user_pin_instance = AuthenticationPin.objects.filter(id=user_pin_instance.id).update(pin=pin)
                    else:
                        create_pin = AuthenticationPin.objects.create(user_id=user_instance[0].id, pin=pin)
                        sms_message = "Dear valued customer, your pin is reset successfully"
                        sms_kwrgs = {
                            'sms_message': sms_message,
                            'number': str(mobile_number)
                        }
                        sms(**sms_kwrgs)
                    return Response(
                        {
                            'status': 1,
                            'message': "User Pin reset successful",
                        }
                    )
                else:
                    return Response(
                        {'status': 2,
                         'message': "User not found",
                         }
                    )
            else:
                return Response(
                    {
                        'status': 3,
                        'message': "Mobile number and Pin are mandatory",
                    }
                )
        else:
            return Response(
                {
                    'status': 4,
                    'message': "Mobile number and Pin are mandatory",
                }
            )


class FCMEntryAPIView(APIView):
    """
     For FCM Notifications
    :parameter:
        Parameters
        {
            "fcm_token": "CharField",               # d9g2xygdTyI:APA91bEPeL8Dp1WKFEwW8PAkvOXXk...
            "device_id": "CharField",               # d9g2xygdTyI:APA91bEPeL8Dp1WKFEwW8PAkvOXXk...
            "app_version": "CharField",             # 1.1.2
        }
        Response
        {
            "status": "CharField",
            "message": "CharField"
        }
        Note
        guest-token base authentication
        "guest-token-fcm" : "bg511rm1-031c-533d-rl11-r666bd000ffl"
        status 1 :  Fcm added successfully
        status 10 : Entered data is in-valid (Validation Error)
        status 11 : Something went wrong (Django Error)
    """
    serializer_class = auth_serializer.FCMSerializer
    permission_classes = (partial(GuestTokenPermissionFCM, ['GET', 'POST', 'HEAD']),)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        fcm_serializer = self.serializer_class(data=request.data)
        fcm_token = request.data.get('fcm_token')
        device_id = request.data.get('device_id')
        app_version = request.data.get('app_version')
        mobile_number = request.data.get('mobile_number')
        if fcm_serializer.is_valid():
            try:
                is_fcm_exist = FCMDevice.objects.filter(
                    device_id=device_id,
                )
                user = None
                if mobile_number is not None:
                    user_instance = User.objects.filter(username=mobile_number)
                    if user_instance:
                        user = user_instance[0]
                if is_fcm_exist:
                    FCMDevice.objects.filter(device_id=device_id).update(registration_id=fcm_token, user=user)
                    if device_id and app_version:

                        pass
                else:
                    FCMDevice.objects.create(
                        device_id=device_id,
                        registration_id=fcm_token,
                        user=user
                    )
                    if device_id and app_version:

                        pass
                status = '1'
                message = 'Fcm added successfully'
            except:
                status = '11'
                message = 'Something went wrong!'
        else:
            status = '10'
            message = 'Enter valid data'
        return Response(
            {
                'status': status,
                'message': message
            }
        )


from django.views.decorators.csrf import csrf_exempt


# @csrf_exempt
class Version_Check_v2(APIView):

    permission_classes = (partial(GuestTokenPermission, ['GET', 'POST', 'HEAD']),)

    def post(self, request, *args, **kwargs):
        """
        :param request: version_name + Token
            {
                "version_name" : "3.0.14"
            }
        guest-token : fae710e0-18bf-11e9-ab14-d663bd873d93
        :return:
            Case 1: for success
                {
                    "status": "1",
                    "is_valid": true,
                    "message": "Valid Version"
                }
            Case 2: failed cases
                {
                    "status": '2' # (2 - 5) for failed cases
                    "is_valid": false,
                    "message": "Invalid Version"
                }
        """

        if 'version_name' in request.data:
            version_name = request.data['version_name']
            if version_name is not None and version_name:
                app_version = AppVersionCheck.objects.filter(status='Active').values_list('version_name', flat=True)
                if app_version:
                    if version_name in app_version:
                        return Response({
                            'status': '1',
                            'is_valid': True,
                            'message': 'Valid Version'
                        })
                    else:
                        return Response({
                            'status': '2',
                            'is_valid': False,
                            'message': 'Invalid Version'
                        })
                else:
                    return Response({
                        'status': '3',
                        'is_valid': False,
                        'message': 'No Version found'
                    })
            else:
                return Response({
                    'status': '4',
                    'is_valid': False,
                    'message': 'Version name should not be empty'
                })
        else:
            return Response({
                'status': '5',
                'is_valid': False,
                'message': 'Please enter version_name'
            })


class CheckUser(APIView):
    permission_classes = (partial(GuestTokenPermissionSetPin, ['GET', 'POST', 'HEAD']),)
    serializer_class = auth_serializer.SetPinSerializer

    def post(self, request, *args, **kwargs):
        """
        {
            "status": "1",
            "message": "Pin verified successfully.",
            "token": "345de5374e1f667c008f6fbc0af81aacef837f81"
        }
        """
        status = None
        sub_status = None
        sub_remaining_days = None
        sub_start_date = None
        sub_end_date = None
        message = None
        token = None
        invitation_code = None
        user_pin = None
        today_date = datetime.datetime.now()

        profile_is_complete = None
        if "mobile_number" in request.data and "pin" in request.data and "type" in request.data:
            mobile_number = request.data.get('mobile_number')
            pin = request.data.get('pin')
            type = request.data.get('type')

            set_pin_serializer_class = self.serializer_class(data=request.data)
            if set_pin_serializer_class:
                user_instance = User.objects.filter(username=mobile_number)
                if user_instance:
                    user_pin_instance = AuthenticationPin.objects.filter(user=user_instance[0], type=type).last()
                    if user_pin_instance:
                        user_pin = user_pin_instance.pin
                        if user_pin == pin:
                            token = Token.objects.get_or_create(user=user_instance[0])
                            token = token[0].key
                            status = '1'
                            message = 'Pin verified successfully.'
                        else:
                            status = '0'
                            message = 'Pin not Valid '
                    else:
                        AuthenticationPin.objects.create(
                            pin=pin,
                            user=user_instance[0],
                            type=type
                        )
                        user_pin = pin
                        token, _ = Token.objects.get_or_create(user=user_instance[0])
                        token = token.key
                        status = '2'
                        message = 'Pin Set Successfully...!'
                else:
                    user_instance = User.objects.create(username=mobile_number, date_joined=today_date)
                    AuthenticationPin.objects.create(
                        pin=pin,
                        user=user_instance,
                        type=type,
                    )
                    token = Token.objects.get_or_create(user=user_instance)
                    token = token[0].key
                    status = '1'
                    message = 'Pin created successfully.'
            else:
                status = '10'
                message = 'Enter valid data'
        else:
            status = '11'
            message = 'Enter mandatory data'
        return Response(
            {
                'status': status,
                'message': message,
                'token': token,
            }
        )


class ReturnTokenMudrakwikUser(APIView):
    permission_classes = (partial(GuestTokenPermissionMudrakwik, ['GET', 'POST', 'HEAD']),)
    serializer_class = auth_serializer.LoginRegisterSerializer

    def post(self, request):
        """
        This api is to return user token if exist otherwise create a user and return token


    :parameter:

        Parameters

        {
            "guest-token-mudrakwik": "CharField",               # cae110v0-22of-53d9-vx13-r666bd873ff4
            "mobile_number": "IntegerField",               # 9090909090

        }

        Response
        success:
        {
            "status": "CharField",
            "message": "CharField"
            "mobile_number": IntField
            "token": "CharField"
        }
        fail:
        {
        status": "CharField",
        "message": "CharField"
        }


        status 1 :  New user created
        status 2 : User exist
        status 3 : Mobile number is not valid


        """
        user_login_serializer = self.serializer_class(data=request.data)
        if user_login_serializer.is_valid():
            mobile_number = request.data['mobile_number']
            offer_exist = False
            user_instance, created = User.objects.get_or_create(username=mobile_number)
            if created:
                status = '1'
                msg = 'New user created'
            else:
                status = "2"
                msg = "User exist"
            if user_instance:
                if user_instance.is_active:
                    token, _ = Token.objects.get_or_create(user=user_instance)
                    token = token.key

            try:
                offer_existance = UserOffer.objects.filter(user=user_instance, is_read=False)
                if offer_existance:
                    offer_exist = True
            except Exception as e:
                print("Inside Exception of offer_existance in SetPin API", e)

            profile_is_complete = None
            user_data = User.objects.filter(username=mobile_number)
            if user_data:
                user_id_prof = user_data[0].id
                profile_is_complete = all_is_completed(user_id_prof)

            try:

                from EApi.wallet import add_default_wallet_amount
                add_default_wallet_amount(mobile_number)
            except Exception as e:
                print("Inside Exception of add_default_wallet_amount in UserLogin API")

            return Response(
                {
                    "status": status,
                    "message": msg,
                    "mobile_number": mobile_number,
                    "token": token,
                    "offer_exist": offer_exist,
                    "profile_is_complete": profile_is_complete,
                }
            )

        else:
            return Response(
                {
                    "status": "3",
                    "message": "Mobile number is not valid",
                }
            )


class ReturnTokenRupeekwikUser(APIView):
    permission_classes = (partial(GuestTokenPermissionRupeekwik, ['GET', 'POST', 'HEAD']),)
    serializer_class = auth_serializer.LoginRegisterSerializer

    def post(self, request):
        """
        This api is to return user token if exist otherwise create a user and return token


    :parameter:

        Parameters

        {
            "guest-token-rupeekwik": "CharField",               # cae110v0-22of-53d9-vx13-r666bd873ff4
            "mobile_number": "IntegerField",               # 9090909090

        }

        Response
        success:
        {
            "status": "CharField",
            "message": "CharField"
            "mobile_number": IntField
            "token": "CharField"
        }
        fail:
        {
        status": "CharField",
        "message": "CharField"
        }


        status 1 :  New user created
        status 2 : User exist
        status 3 : Mobile number is not valid


        """
        user_login_serializer = self.serializer_class(data=request.data)
        if user_login_serializer.is_valid():
            mobile_number = request.data['mobile_number']
            offer_exist = False
            user_instance, created = User.objects.get_or_create(username=mobile_number)
            if created:
                status = '1'
                msg = 'New user created'
            else:
                status = "2"
                msg = "User exist"
            if user_instance:
                if user_instance.is_active:
                    token, _ = Token.objects.get_or_create(user=user_instance)
                    token = token.key

            try:
                offer_existance = UserOffer.objects.filter(user=user_instance, is_read=False)
                if offer_existance:
                    offer_exist = True
            except Exception as e:
                print("Inside Exception of offer_existance in SetPin API", e)

            profile_is_complete = None
            user_data = User.objects.filter(username=mobile_number)
            if user_data:
                user_id_prof = user_data[0].id
                profile_is_complete = all_is_completed(user_id_prof)

            try:

                from EApi.wallet import add_default_wallet_amount
                add_default_wallet_amount(mobile_number)
            except Exception as e:
                print("Inside Exception of add_default_wallet_amount in UserLogin API")

            return Response(
                {
                    "status": status,
                    "message": msg,
                    "mobile_number": mobile_number,
                    "token": token,
                    "offer_exist": offer_exist,
                    "profile_is_complete": profile_is_complete,
                }
            )

        else:
            return Response(
                {
                    "status": "3",
                    "message": "Mobile number is not valid",
                }
            )


