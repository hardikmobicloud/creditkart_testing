
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.apps.registry import apps
import datetime


Subscription = apps.get_model('User', 'Subscription')
ShareAppMaster = apps.get_model('UserData', 'ShareAppMaster')
UserSharedData = apps.get_model('UserData', 'UserSharedData')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')



def share_subdata(mobile):
    today = datetime.datetime.now()

    contact = ShareAppMaster.objects.filter(number=mobile).order_by('-id')
    if contact:
        user = User.objects.filter(username=contact[0].number)
        if user:
            subscription = Subscription.objects.filter(user_id=user[0].id, payment_status='success').order_by('-id')
            if subscription:
                if contact[0].created_date <= subscription[0].start_date and contact[0].created_date <= user[
                    0].date_joined:
                    ext_check=UserSharedData.objects.filter(is_subscribed=True, subscription_amount=12.5,
                                                  shareapp_id=contact[0].id)
                    if not ext_check:
                        UserSharedData.objects.create(is_subscribed=True, subscription_amount=12.5,
                                                      shareapp_id=contact[0].id)

    return HttpResponse('done')


def share_approveddata(mobile):

    today = datetime.datetime.now()

    contact = ShareAppMaster.objects.filter(number=mobile).order_by('-id')
    if contact:
        user = User.objects.filter(username=contact[0].number)
        if user:
            subscription = UserSharedData.objects.filter(shareapp_id=contact[0].id, is_subscribed=True,is_approved=False).order_by(
                '-id')
            if subscription:
                loan = LoanApplication.objects.filter(user_id=user[0].id,
                                                      status__in=['APPROVED', 'COMPLETED']).order_by('-id')
                if loan:
                    if contact[0].created_date <= loan[0].loan_start_date:
                        start_var = loan[0].loan_start_date.strftime('%Y-%m-%d')
                        create_var = loan[0].created_date.strftime('%Y-%m-%d')
                        if start_var == create_var:

                            UserSharedData.objects.filter(id=subscription[0].id).update(is_approved=True, Approved_amount=12.5,
                                                          updated_date=today)

    return HttpResponse('done')











