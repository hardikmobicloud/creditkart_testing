from django.conf import settings


def pull_pan_details(**pan_kwrgs):
    """
    NSDL pan verification API integration.
    """

    try:
        print('inside try---------------')
        import os
        import datetime
        import requests

        os.environ[
            'CLASSPATH'] = '.:' + settings.JAR_PATH + 'bcmail-jdk16-1.44.jar:' + settings.JAR_PATH + 'bcprov-jdk16-1.44.jar:$CLASSPATH'
        print('before pan_no-------------------')
        pan_no = pan_kwrgs['pan_number']
        pan_user = settings.PAN_USER
        file_name = settings.NSDL_PATH + '/pan_request/out_' + str(pan_no) + '_' + str(
            datetime.datetime.now().date()) + str(datetime.datetime.now().minute) + '_' + str(
            datetime.datetime.now().microsecond) + '.sig'
        pan_password = settings.PAN_PASSWORD

        os.system(
            'cd nsdl/deliverable; java pkcs7gen live_oupt.jks ' + pan_password + ' ' + pan_user + '^' + pan_no + ' ' + file_name)

        url = "https://59.163.46.2/TIN/PanInquiryBackEnd"
        f = open(file_name, 'r')

        querystring = {
            "data": pan_user + '^' + pan_no,
            "signature": f.read(),
            "version": "2"
        }

        response = requests.request(
            "POST",
            url,
            verify=False,
            params=querystring
        )

        if response.status_code == 200:
            print('inside upper if------------------')

            split_response = response.text.split('^')
            return_code = split_response[0]
            pan = split_response[1]
            pan_status = split_response[2]
            last_name = split_response[3]
            first_name = split_response[4]
            middle_name = split_response[5]

            if return_code == '1' and pan_status == 'E':
                print('inside if--------------')

                return {
                    'pan_code': return_code,
                    'pan': pan,
                    'pan_status': pan_status,
                    'first_name': first_name,
                    'middle_name': middle_name,
                    'last_name': last_name,
                    'full_name': first_name + ' ' + middle_name + ' ' + last_name,
                    'nsdl_status': '1_true'
                }

            else:
                print('inside else-----------------')

                return {
                    'pan_code': return_code,
                    'pan': pan,
                    'pan_status': pan_status,
                    'first_name': first_name,
                    'middle_name': middle_name,
                    'last_name': last_name,
                    'full_name': first_name + ' ' + middle_name + ' ' + last_name,
                    'nsdl_status': '2_false'
                }

        else:
            print('inside lower else---------------')

            return {
                'pan_code': '',
                'pan': '',
                'pan_status': '',
                'first_name': '',
                'middle_name': '',
                'last_name': '',
                'full_name': '',
                'nsdl_status': '3_false'
            }


    except Exception as e:
        import os
        import sys
        print("-----------------exc", e, e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        print('inside except-----------------')

        return {
            'pan_code': '',
            'pan': '',
            'pan_status': '',
            'first_name': '',
            'middle_name': '',
            'last_name': '',
            'full_name': '',
            'nsdl_status': '4_false'
        }
