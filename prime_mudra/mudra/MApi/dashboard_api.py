import datetime
from rest_framework import generics
from itertools import chain
from datetime import datetime
from . import dashboard_serializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import *
from rest_framework.authentication import *
from django.contrib.auth.models import User
from django.apps.registry import apps
from calendar import monthrange
from . import botofile
UserMerchantDetails = apps.get_model('UserData', 'UserMerchantDetails')
UserCreditCardDetails = apps.get_model('UserData', 'UserCreditCardDetails')
UserDebitCardDetails = apps.get_model('UserData', 'UserDebitCardDetails')
UserBankDetails = apps.get_model('UserData', 'UserBankDetails')

Personal = apps.get_model('User', 'Personal')
Professional = apps.get_model('User', 'Professional')
Documents = apps.get_model('User', 'Documents')
Reference = apps.get_model('User', 'Reference')
Bank = apps.get_model('User', 'Bank')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')


class DashboardDataAPI(generics.ListAPIView):


    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    # serializer_class = dashboard_serializer.DashboardUserBankDetailsSerializers

    def get(self, request, *args, **kwargs):

        if request.method == 'GET':
            user_instance = User.objects.get(id=request.user.id)
            user_id = request.user.id
            user_loan_status = LoanApplication.objects.filter(user=user_instance).exclude(
                status__in=['COMPLETED', 'REJECTED', 'CANCELLED', 'ON_HOLD'])
            date_today = datetime.now().date()
            total_expense_list=[]
            total_income_list=[]
            is_loan_pending=True
            loan_pending_message=''
            total_income=0
            total_expense=0
            if user_loan_status:

                user_loan_ins_obj = LoanApplication.objects.filter(user=user_instance).order_by('-id')[0]


                if user_loan_ins_obj.status == 'SUBMITTED':
                    loan_pending_message = 'Sorry! It It appears that your loan application is already under processing. We regret to inform you that, you will not able to apply for another loan until your applied loan has proceeded.'

                elif user_loan_ins_obj.status == 'APPROVED':
                    loan_pending_message = 'Sorry ! It appears that your previously approved loan is not completed yet. We regret to inform you that without completing the current ongoing loan, your requirement will not proceed further.'
                elif user_loan_ins_obj.status == 'PENDING':
                    loan_pending_message = 'Sorry ! It appears that your previously approved loan is not completed yet. We regret to inform you that without completing the current ongoing loan, your requirement will not proceed further.'
                else:
                    loan_pending_message = ''


            # graph data serializer (top 10 credit, top 10 debit)
            date=1

            while date <= date_today.day:
                if date_today:
                    new_date = str(date_today.year) + "-" + str(date_today.month) + "-" + str(date)
                    date_time_obj = datetime.strptime(new_date, '%Y-%m-%d')
                    bd_expense_instance = UserBankDetails.objects.filter(
                        user_id=user_id, transaction_type='Debit',
                        transaction_date__year=date_time_obj.year,
                        transaction_date__month=date_time_obj.month,
                        transaction_date__day=date_time_obj.day
                    ).order_by('-transaction_date').values_list('transaction_amount', flat=True)
                    dc_expense_instance = UserDebitCardDetails.objects.filter(
                        user_id=user_id,
                        transaction_type='Debit',
                         transaction_date__year=date_time_obj.year,
                        transaction_date__month=date_time_obj.month,
                        transaction_date__day=date_time_obj.day
                    ).order_by('-transaction_date').values_list('transaction_amount', flat=True)
                    cc_expense_instance = UserCreditCardDetails.objects.filter(
                        user_id=user_id,
                        transaction_type='Debit',
                         transaction_date__year=date_time_obj.year,
                        transaction_date__month=date_time_obj.month,
                        transaction_date__day=date_time_obj.day
                    ).order_by('-transaction_date').values_list('transaction_amount', flat=True)
                    bd_income_instance = UserBankDetails.objects.filter(
                        user_id=user_id, transaction_type='Credit',
                         transaction_date__year=date_time_obj.year,
                        transaction_date__month=date_time_obj.month,
                        transaction_date__day=date_time_obj.day
                    ).order_by('-transaction_date').values_list('transaction_amount', flat=True)
                    dc_income_instance = UserDebitCardDetails.objects.filter(
                        user_id=user_id,
                        transaction_type='Credit',
                         transaction_date__year=date_time_obj.year,
                        transaction_date__month=date_time_obj.month,
                        transaction_date__day=date_time_obj.day
                    ).order_by('-transaction_date').values_list('transaction_amount', flat=True)
                    cc_income_instance = UserCreditCardDetails.objects.filter(
                        user_id=user_id,
                        transaction_type='Credit',
                         transaction_date__year=date_time_obj.year,
                        transaction_date__month=date_time_obj.month,
                        transaction_date__day=date_time_obj.day
                    ).order_by('-transaction_date').values_list('transaction_amount', flat=True)
                    bd_expense_sum = sum(bd_expense_instance)
                    dc_expense_sum = sum(dc_expense_instance)
                    cc_expense_sum = sum(cc_expense_instance)

                    bd_income_sum = sum(bd_income_instance)
                    dc_income_sum = sum(dc_income_instance)
                    cc_income_sum = sum(cc_income_instance)


                    total_expense_sum = bd_expense_sum + dc_expense_sum + cc_expense_sum
                    total_income_sum = bd_income_sum + dc_income_sum + cc_income_sum
                    #if total_expense_sum < 0:
                     #   total_expense_sum = 0
                    #if total_income_sum < 0:
                     #   total_income_sum = 0

                    total_income += total_income_sum
                    total_expense += total_expense_sum



                    con_date=[date_today.year,(date_today.month-1),date]

                    total_expense_list.append({"date":con_date,"amount":total_expense_sum})
                    total_income_list.append({"date":con_date,"amount":total_income_sum})
                    date+=1
            balance=total_income-total_expense
            if balance<0:
                balance=0
            path = "canvas/canvasjs.min.js"
            url = botofile.get_url(path)

            return Response(
                {
                    'total_expense_list': total_expense_list,
                    'total_income_list': total_income_list,
                    'is_loan_pending': is_loan_pending,
                    'loan_pending_message': loan_pending_message,
                    'total_expense': total_expense,
                    'total_income': total_income,
                    'balance': balance,
                    'url': url
                }
            )


class DashboardDataMonthAPI(generics.ListAPIView):


    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    # serializer_class = dashboard_serializer.DashboardUserBankDetailsSerializers

    def post(self, request, *args, **kwargs):

        if request.method == 'POST':
            user_instance = User.objects.get(id=request.user.id)
            user_id = request.user.id
            user_loan_status = LoanApplication.objects.filter(user=user_instance).exclude(
                status__in=['COMPLETED', 'REJECTED', 'CANCELLED', 'ON_HOLD'])
            date_today = datetime.now().date()
            month=request.data.get('year_month')
            total_expense_list=[]
            total_income_list=[]
            is_loan_pending=True
            loan_pending_message=''
            total_income=0
            total_expense=0
            if user_loan_status:

                user_loan_ins_obj = LoanApplication.objects.filter(user=user_instance).order_by('-id')[0]


                if user_loan_ins_obj.status == 'SUBMITTED':
                    loan_pending_message = 'Sorry! It It appears that your loan application is already under processing. We regret to inform you that, you will not able to apply for another loan until your applied loan has proceeded.'

                elif user_loan_ins_obj.status == 'APPROVED':
                    loan_pending_message = 'Sorry ! It appears that your previously approved loan is not completed yet. We regret to inform you that without completing the current ongoing loan, your requirement will not proceed further.'
                elif user_loan_ins_obj.status == 'PENDING':
                    loan_pending_message = 'Sorry ! It appears that your previously approved loan is not completed yet. We regret to inform you that without completing the current ongoing loan, your requirement will not proceed further.'
                else:
                    loan_pending_message = ''


            # graph data serializer (top 10 credit, top 10 debit)
            date=1
            month_r = monthrange(date_today.year, int(month))

            while date <= month_r[1]:
                if date_today:
                    new_date = str(date_today.year) + "-" + str(month) + "-" + str(date)
                    date_time_obj = datetime.strptime(new_date, '%Y-%m-%d')
                    bd_expense_instance = UserBankDetails.objects.filter(
                        user_id=user_id, transaction_type='Debit',
                        transaction_date__year=date_time_obj.year,
                        transaction_date__month=date_time_obj.month,
                        transaction_date__day=date_time_obj.day
                    ).order_by('-transaction_date').values_list('transaction_amount', flat=True)
                    dc_expense_instance = UserDebitCardDetails.objects.filter(
                        user_id=user_id,
                        transaction_type='Debit',
                         transaction_date__year=date_time_obj.year,
                        transaction_date__month=date_time_obj.month,
                        transaction_date__day=date_time_obj.day
                    ).order_by('-transaction_date').values_list('transaction_amount', flat=True)
                    cc_expense_instance = UserCreditCardDetails.objects.filter(
                        user_id=user_id,
                        transaction_type='Debit',
                         transaction_date__year=date_time_obj.year,
                        transaction_date__month=date_time_obj.month,
                        transaction_date__day=date_time_obj.day
                    ).order_by('-transaction_date').values_list('transaction_amount', flat=True)
                    bd_income_instance = UserBankDetails.objects.filter(
                        user_id=user_id, transaction_type='Credit',
                         transaction_date__year=date_time_obj.year,
                        transaction_date__month=date_time_obj.month,
                        transaction_date__day=date_time_obj.day
                    ).order_by('-transaction_date').values_list('transaction_amount', flat=True)
                    dc_income_instance = UserDebitCardDetails.objects.filter(
                        user_id=user_id,
                        transaction_type='Credit',
                         transaction_date__year=date_time_obj.year,
                        transaction_date__month=date_time_obj.month,
                        transaction_date__day=date_time_obj.day
                    ).order_by('-transaction_date').values_list('transaction_amount', flat=True)
                    cc_income_instance = UserCreditCardDetails.objects.filter(
                        user_id=user_id,
                        transaction_type='Credit',
                         transaction_date__year=date_time_obj.year,
                        transaction_date__month=date_time_obj.month,
                        transaction_date__day=date_time_obj.day
                    ).order_by('-transaction_date').values_list('transaction_amount', flat=True)
                    bd_expense_sum = sum(bd_expense_instance)
                    dc_expense_sum = sum(dc_expense_instance)
                    cc_expense_sum = sum(cc_expense_instance)

                    bd_income_sum = sum(bd_income_instance)
                    dc_income_sum = sum(dc_income_instance)
                    cc_income_sum = sum(cc_income_instance)


                    total_expense_sum = bd_expense_sum + dc_expense_sum + cc_expense_sum
                    total_income_sum = bd_income_sum + dc_income_sum + cc_income_sum
                    #if total_expense_sum < 0:
                     #   total_expense_sum = 0
                    #if total_income_sum < 0:
                     #   total_income_sum = 0

                    total_income += total_income_sum
                    total_expense += total_expense_sum



                    con_date=[date_today.year,(int(month)-1),date]

                    total_expense_list.append({"date":con_date,"amount":total_expense_sum})
                    total_income_list.append({"date":con_date,"amount":total_income_sum})
                    date+=1
            balance=total_income-total_expense
            if balance<0:
                balance=0
            return Response(
                {
                    'total_expense_list': total_expense_list,
                    'total_income_list':total_income_list,
                    'is_loan_pending': is_loan_pending,
                    'loan_pending_message': loan_pending_message,
                    'total_expense':total_expense,
                    'total_income':total_income,
                    'balance':balance
                }
            )





class DashboardBankDetailsAPIView(APIView):
    """
    API to view list of user bank account numbers, credit card numbers and debit card number

    :parameter:

        API to view users all bank account numbers, credit card numbers and debit card number

        Parameter

        {
            "year_month": "DateField"           # format (yyyy-mm)
        }

        Response

        {
            "user_bank_list" : [{}],
            "user_credit_card_list" : [{}],
            "user_debit_card_list" : [{}],
            "user_merchant_details_list" : [{}],

        }

        Sample

        {
            "user_bank_list" : [
                {
                    "bank_id": "BZ-ATMSBI",
                    "logo": "https://anandfin.com/media/bank_logos/sbi_logo_256x256.png",
                    "account_number": "XXXXXXX1927"
                }
            ]
            "user_credit_card_list" : [
                {
                    "bank_id": "BZ-ATMSBI",
                    "logo": "https://anandfin.com/media/bank_logos/sbi_logo_256x256.png",
                    "credit_card_number": "XXXXXXX0011"
                }
            ],
            "user_debit_card_list" : [
                {
                    "bank_id": "BZ-ATMSBI",
                    "logo": "https://anandfin.com/media/bank_logos/sbi_logo_256x256.png",
                    "debit_card_number": "XXXXXXX2233"
                }
            ],
            "user_merchant_details_list" : [
                {
                    "merchant_id": "FLIPKART",
                    "logo": "https://anandfin.com/media/bank_logos/flpkart_256x256.png"
                }
            ]

        }

        Note :

        Authentication token required

        [
            {
                "key":"Authorization",
                "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
            }
        ]

    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        try:
            user_instance = User.objects.get(id=request.user.id)
            year_month = request.data.get('year_month')

            date_obj = datetime.datetime.strptime(year_month, '%Y-%m')

            user_bank_details_distinct_list = UserBankDetails.objects.filter(user=user_instance,
                                                                             transaction_date__year=date_obj.year,
                                                                             transaction_date__month=date_obj.month).order_by(
                'account_number').distinct('account_number')
            user_credit_card_details_distinct_list = UserCreditCardDetails.objects.filter(user=user_instance,
                                                                                          transaction_date__year=date_obj.year,
                                                                                          transaction_date__month=date_obj.month).order_by(
                'credit_card_number').distinct('credit_card_number')
            user_debit_card_details_distinct_list = UserDebitCardDetails.objects.filter(user=user_instance,
                                                                                        transaction_date__year=date_obj.year,
                                                                                        transaction_date__month=date_obj.month).order_by(
                'debit_card_number').distinct('debit_card_number')
            user_merchant_details_distinct_list = UserMerchantDetails.objects.filter(user=user_instance,
                                                                                     transaction_date__year=date_obj.year,
                                                                                     transaction_date__month=date_obj.month).order_by(
                'merchant_id').distinct('merchant_id')

            user_bank_details_distinct_serializer = dashboard_serializer.DashboardBankDetailsUniqueSerializer(
                user_bank_details_distinct_list, many=True)
            user_credit_card_details_distinct_serializer = dashboard_serializer.DashboardCreditCardDetailsUniqueSerializer(
                user_credit_card_details_distinct_list, many=True)
            user_debit_card_details_distinct_serializer = dashboard_serializer.DashboardDebitCardDetailsUniqueSerializer(
                user_debit_card_details_distinct_list, many=True)
            user_merchant_details_distinct_serializer = dashboard_serializer.DashboardMerchantDetailsUniqueSerializer(
                user_merchant_details_distinct_list, many=True)

            user_bank_details_distinct_serializer = user_bank_details_distinct_serializer.data
            user_credit_card_details_distinct_serializer = user_credit_card_details_distinct_serializer.data
            user_debit_card_details_distinct_serializer = user_debit_card_details_distinct_serializer.data
            user_merchant_details_distinct_serializer = user_merchant_details_distinct_serializer.data

        except Exception as e:

            print(e.args)

            user_bank_details_distinct_serializer = []
            user_credit_card_details_distinct_serializer = []
            user_debit_card_details_distinct_serializer = []
            user_merchant_details_distinct_serializer = []

        return Response(
            {
                'user_bank_list': user_bank_details_distinct_serializer,
                'user_credit_card_list': user_credit_card_details_distinct_serializer,
                'user_debit_card_list': user_debit_card_details_distinct_serializer,
                'user_merchant_details_list': user_merchant_details_distinct_serializer,
            }
        )


class DashboardDetailsAPIView(APIView):
    """
    Dashboard details api

    :parameter:

        API To get dashboard details

        Parameter's

        {
            "year_month": "DateField",                      # format (yyyy-mm)  2019-01
        }

        Response

        {
            'year_month': [],
            'user_bank_details': [{}],
            'user_credit_card_details': [{}],
            'user_debit_card_details': [{}],
            'user_merchant_details': [{}],
        }


        Sample

        {
            'year_month': "2019-02",
            'user_bank_details': [
                {
                    "bank_id": "MD-UNIONB",
                    "transaction_amount": 500,
                    "account_number": "XXXXXXX7996",
                    "transaction_type": "Debit",
                    "transaction_date": "2019-02-05T08:54:10",
                    "towards": "Other",
                    "category": "Other",
                    "merchant": "Union Bank of India",
                    "logo": "https://anandfin.com/media/bank_logos/union_bank_of_india_logo_3899_256x256.png"
                },
            ]
            'user_credit_card_details': [
                {
                    "bank_id": "MD-UNIONB",
                    "transaction_amount": 1500,
                    "credit_card_number": "XXXXXXX0011",
                    "transaction_type": "Debit",
                    "transaction_date": "2019-02-05T08:54:10",
                    "towards": "Other",
                    "category": "Other",
                    "merchant": "Union Bank of India",
                    "logo": "https://anandfin.com/media/bank_logos/union_bank_of_india_logo_3899_256x256.png"
                },
            ]
            'user_debit_card_details': [
                {
                    "bank_id": "MD-UNIONB",
                    "transaction_amount": 2500,
                    "dbit_card_number": "XXXXXXX2111",
                    "transaction_type": "Debit",
                    "transaction_date": "2019-02-05T08:54:10",
                    "towards": "Other",
                    "category": "Other",
                    "merchant": "Union Bank of India",
                    "logo": "https://anandfin.com/media/bank_logos/union_bank_of_india_logo_3899_256x256.png"
                },
            ]
            'user_merchant_details': [
                {
                    "merchant_id": "FLIPKART",
                    "transaction_amount": 1300,
                    "transaction_type": "Credit",
                    "transaction_date": "2019-02-21T15:40:58",
                    "category": "Other",
                    "logo": "https://anandfin.com/media/bank_logos/flpkart_256x256.png"
                },
            ],

        }

        Note :

        Authentication token required

        [
            {
                "key":"Authorization",
                "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
            }
        ]

        year_month : Data of requested month
        user_bank_details : List of user bank details
        user_credit_card_details : List of user credit card details
        user_debit_card_details : List of user debit card details
        user_merchant_details : List of user merchant details

    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    # serializer_class = dashboard_serializer.DashboardUserBankDetailsSerializers

    def post(self, request, *args, **kwargs):

        if request.method == 'POST':

            year_month = request.data.get('year_month')

            try:

                user_instance = User.objects.get(id=request.user.id)

                date_obj = datetime.datetime.strptime(year_month, '%Y-%m')

                user_bank_details = UserBankDetails.objects.filter(
                    user=user_instance,
                    transaction_date__year=date_obj.year,
                    transaction_date__month=date_obj.month
                )

                user_credit_card_details = UserCreditCardDetails.objects.filter(
                    user=user_instance,
                    transaction_date__year=date_obj.year,
                    transaction_date__month=date_obj.month
                )

                user_debit_card_details = UserDebitCardDetails.objects.filter(
                    user=user_instance,
                    transaction_date__year=date_obj.year,
                    transaction_date__month=date_obj.month
                )

                user_merchant_details = UserMerchantDetails.objects.filter(
                    user=user_instance,
                    transaction_date__year=date_obj.year,
                    transaction_date__month=date_obj.month
                )

                user_bank_serializer = dashboard_serializer.DashboardUserBankDetailsSerializersDetails(
                    user_bank_details, many=True)
                user_credit_card_serializer = dashboard_serializer.DashboardUserCreditCardDetailsSerializersDetails(
                    user_credit_card_details, many=True)
                user_debit_card_serializer = dashboard_serializer.DashboardUserDebitCardDetailsSerializersDetails(
                    user_debit_card_details, many=True)
                user_merchant_serializer = dashboard_serializer.DashboardUserMerchantDetailsSerializersDetails(
                    user_merchant_details, many=True)

                return Response(
                    {
                        'year_month': year_month,
                        'user_bank_details': user_bank_serializer.data,
                        'user_credit_card_details': user_credit_card_serializer.data,
                        'user_debit_card_details': user_debit_card_serializer.data,
                        'user_merchant_details': user_merchant_serializer.data,
                        'user_bank_details_count': user_bank_details.count(),
                        'user_credit_card_details_count': user_credit_card_details.count(),
                        'user_debit_card_details_count': user_debit_card_details.count(),
                        'user_merchant_details_count': user_merchant_details.count(),
                    }
                )

            except Exception as e:

                return Response(
                    {
                        'year_month': [],
                        'user_bank_details': [],
                        'user_credit_card_details': [],
                        'user_debit_card_details': [],
                        'user_merchant_details': [],
                    }
                )


class DashboardBankDetailsListAPIView(APIView):
    """
    API to view list of bank details according to account number

    :parameter:

        API to view list of users all bank details according to account number

        Parameter

        {
            "account_number": "CharField",
            "year_month": "DateField",              # format (yyyy-mm)
        }

        Response

        {
            "user_bank_list": [{}]
        }

        Sample

        {
            "user_bank_list": [
                {
                    "bank_id": "BW-UNIONB",
                    "transaction_amount": 300,
                    "account_number": "XXXXXXX7996",
                    "transaction_type": "Debit",
                    "transaction_date": "2019-02-18T10:04:20",
                    "towards": null,
                    "category": "ATM Withdrawal",
                    "merchant": "Union Bank of India",
                    "logo": "https://anandfin.com/media/bank_logos/union_bank_of_india_logo_3899_256x256.png"
                }
            ]
        }

        Note :

        Authentication token required

        [
            {
                "key":"Authorization",
                "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
            }
        ]
    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        account_number = request.data.get('account_number')
        year_month = request.data.get('year_month')

        try:

            user_instance = User.objects.get(id=request.user.id)
            date_obj = datetime.datetime.strptime(year_month, '%Y-%m')

            user_bank_list = UserBankDetails.objects.filter(
                user=user_instance, account_number=account_number,
                transaction_date__year=date_obj.year,
                transaction_date__month=date_obj.month,
            ).order_by('-transaction_date')

            user_bank_serializer = dashboard_serializer.DashboardUserBankDetailsSerializersDetails(user_bank_list,
                                                                                                   many=True)
            user_bank_serializer = user_bank_serializer.data

        except Exception as e:

            user_bank_serializer = []
            print(e.args)

        return Response(
            {
                'user_bank_list': user_bank_serializer
            }
        )


class DashboardCreditCardDetailsListAPIView(APIView):
    """
    API to view list of credit-card details according to credit-card number

    :parameter:

        API to view list of users all credit-card details according to credit-card number

        Parameter

        {
            "credit_card_number": "CharField",
            "year_month": "DateField",              # format (yyyy-mm)
        }

        Response

        {
            "user_bank_list": [{}]
        }

        Sample

        {
            "user_bank_list": [
                {
                    "bank_id": "BW-UNIONB",
                    "transaction_amount": 300,
                    "credit_card_number": "XXXXXXX7996",
                    "transaction_type": "Debit",
                    "transaction_date": "2019-02-18T10:04:20",
                    "towards": "Other",
                    "category": "ATM Withdrawal",
                    "merchant": "Union Bank of India",
                    "logo": "https://anandfin.com/media/bank_logos/union_bank_of_india_logo_3899_256x256.png"
                }
            ]
        }

        Note :

        Authentication token required

        [
            {
                "key":"Authorization",
                "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
            }
        ]
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        credit_card_number = request.data.get('credit_card_number')
        year_month = request.data.get('year_month')

        try:

            user_instance = User.objects.get(id=request.user.id)
            date_obj = datetime.datetime.strptime(year_month, '%Y-%m')

            user_credit_card_list = UserCreditCardDetails.objects.filter(
                user=user_instance, credit_card_number=credit_card_number,
                transaction_date__year=date_obj.year,
                transaction_date__month=date_obj.month,
            ).order_by('-transaction_date')

            user_credit_card_serializer = dashboard_serializer.DashboardUserCreditCardDetailsSerializersDetails(
                user_credit_card_list, many=True)
            user_credit_card_serializer = user_credit_card_serializer.data

        except Exception as e:

            user_credit_card_serializer = []
            print(e.args)

        return Response(
            {
                'user_bank_list': user_credit_card_serializer
            }
        )


class DashboardDebitCardDetailsListAPIView(APIView):
    """
    API to view list of debit-card details according to debit-card number

    :parameter:

        API to view list of users all debit-card details according to debit-card number

        Parameter

        {
            "debit_card_number": "CharField",
            "year_month": "DateField",              # format (yyyy-mm)
        }

        Response

        {
            "user_bank_list": [{}]
        }


        Sample

        {
            "user_bank_list": [
                {
                    "bank_id": "BW-UNIONB",
                    "transaction_amount": 300,
                    "debit_card_number": "XXXXXXX7996",
                    "transaction_type": "Debit",
                    "transaction_date": "2019-02-18T10:04:20",
                    "towards": "Other",
                    "category": "ATM Withdrawal",
                    "merchant": "Union Bank of India",
                    "logo": "https://anandfin.com/media/bank_logos/union_bank_of_india_logo_3899_256x256.png"
                }
            ]
        }

        Note :

        Authentication token required

        [
            {
                "key":"Authorization",
                "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
            }
        ]
    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        debit_card_number = request.data.get('debit_card_number')
        year_month = request.data.get('year_month')

        try:
            user_instance = User.objects.get(id=request.user.id)
            date_obj = datetime.datetime.strptime(year_month, '%Y-%m')
            user_debit_card_list = UserDebitCardDetails.objects.filter(
                user=user_instance, debit_card_number=debit_card_number,
                transaction_date__year=date_obj.year,
                transaction_date__month=date_obj.month,
            ).order_by('-transaction_date')

            user_debit_card_serializer = dashboard_serializer.DashboardUserDebitCardDetailsSerializersDetails(
                user_debit_card_list, many=True)
            user_debit_card_serializer = user_debit_card_serializer.data

        except Exception as e:

            user_debit_card_serializer = []
            print(e.args)

        return Response(
            {
                'user_bank_list': user_debit_card_serializer
            }
        )


class DashboardMerchantDetailsListAPIView(APIView):
    """
    API to view list of merchant details according to merchant id

    :parameter:

        API to view list of users all merchant details according to merchant id

        Parameter

        {
            "merchant_id": "CharField",
            "year_month": "DateField",              # format (yyyy-mm)
        }

        Response

        {
            "user_bank_list": [{}]
        }

        Sample

        {
            "user_bank_list": [
                {
                    "merchant_id": "BW-UNIONB",
                    "transaction_amount": 300,
                    "transaction_type": "Debit",
                    "transaction_date": "2019-02-18T10:04:20",
                    "category": "ATM Withdrawal",
                    "logo": "https://anandfin.com/media/bank_logos/union_bank_of_india_logo_3899_256x256.png"
                }
            ]
        }

        Note :

        Authentication token required

        [
            {
                "key":"Authorization",
                "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
            }
        ]
    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        merchant_id = request.data.get('merchant_id')
        year_month = request.data.get('year_month')

        try:

            user_instance = User.objects.get(id=request.user.id)
            date_obj = datetime.datetime.strptime(year_month, '%Y-%m')

            user_merchant_list = UserMerchantDetails.objects.filter(
                user=user_instance, merchant_id=merchant_id,
                transaction_date__year=date_obj.year,
                transaction_date__month=date_obj.month,
            ).order_by('-transaction_date')

            user_merchant_serializer = dashboard_serializer.DashboardUserMerchantDetailsSerializersDetails(
                user_merchant_list, many=True)
            user_merchant_serializer = user_merchant_serializer.data

        except Exception as e:

            user_merchant_serializer = []
            print(e.args)

        return Response(
            {
                'user_bank_list': user_merchant_serializer
            }
        )