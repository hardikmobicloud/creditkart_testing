from rest_framework import serializers
from django.apps.registry import apps

SmsKeyWordMaster = apps.get_model('IcomeExpense', 'SmsKeyWordMaster')
UserMerchantDetails = apps.get_model('UserData', 'UserMerchantDetails')
UserCreditCardDetails = apps.get_model('UserData', 'UserCreditCardDetails')
UserDebitCardDetails = apps.get_model('UserData', 'UserDebitCardDetails')
UserBankDetails = apps.get_model('UserData', 'UserBankDetails')


class SmsKeyWordSerializer(serializers.ModelSerializer):
    """
    Serializer to get sms key word's
    """

    class Meta:
        model = SmsKeyWordMaster
        fields = ('keyword', 'updated_date',)


class UserMerchantListDistSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserMerchantDetails
        fields = ('merchant_id',)


class UserBankDetailsListDistSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserBankDetails
        fields = ('account_number',)


class UserCreditCardDetailsListDistSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserCreditCardDetails
        fields = ('credit_card_number',)


class UserDebitCardDetailsDetailsListDistSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserDebitCardDetails
        fields = ('debit_card_number',)
