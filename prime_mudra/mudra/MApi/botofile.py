# from mudra import settings
from django.conf import settings

# aws_access_key_id = "AKIA3AOPBD3KAUAMRE5H"
# aws_secret_access_key = "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"

aws_access_key_id = "AKIA3AOPBD3KFQ25ETIR"
aws_secret_access_key = "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"

bucket = "mudrakwik-storage"

import logging
import boto3
from botocore.exceptions import ClientError

def get_url(path):

    """
        generated full signed url for accessing aws s3 bucket
    :param
        profile_images/7a0c6aa1-9ff.png
    path: path of document from database.json
    :return:
        https://mudrakwik-storage.s3.amazonaws.com/profile_images/7a0c6aa1-9ff.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA3AOPBD3KPALEWLPG%2F20190730%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20190730T102005Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=578529847e3a6886b96b456c5fccfe2e96d2baae5f8ebdef93a90d61fc23fe71
    """
    s3_client = boto3.client('s3', aws_access_key_id=aws_access_key_id,
                             aws_secret_access_key=aws_secret_access_key, region_name='ap-south-1')
    try:
        content_type = "image/jpg"
        image_path = str(path).split()
        if len(image_path) > 1:
            if image_path[-1] == ".png":
                content_type = "image/png"
        response = s3_client.generate_presigned_url('get_object',
                                                    Params={
                                                        'Bucket': "mudrakwik-storage",
                                                        'Key': str(path),
                                                        'ResponseContentType': content_type
                                                    },
                                                    ExpiresIn=3600)
    except ClientError as e:
        logging.error(e)
        return None
    return response


def upload_image(path, save_to_path):
    s3 = boto3.resource('s3', aws_access_key_id=aws_access_key_id,
                        aws_secret_access_key=aws_secret_access_key, region_name='ap-south-1')
    BUCKET = bucket
    # save_to_path = "ecom/" + path
    s3.Bucket(BUCKET).upload_file(path, save_to_path)


def delete_image(path_to_file):
    """
    function to delete image from aws
    """
    status = "fail"
    s3 = boto3.resource('s3', aws_access_key_id=aws_access_key_id,
                        aws_secret_access_key=aws_secret_access_key, region_name='ap-south-1')
    BUCKET = bucket

    obj = s3.Object(BUCKET, path_to_file)  # get image object
    if obj:
        # print(obj)
        obj.delete()  # delete image
        status = "success"
    return status


def file_exists(filename):
    try:
        s3 = boto3.resource('s3', aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY, region_name='ap-south-1')
        bucket = s3.Bucket("mudrakwik-storage")
        objs = list(bucket.objects.filter(Prefix=filename))
        # if objs:
        #     for i in objs:
        #         print("=======",i.key)

        if any([w.key == filename for w in objs]):
            return True
        else:
            return False
    except:
        return None

