from django.conf import settings
from rest_framework import permissions


class GuestTokenPermission(permissions.BasePermission):
    """
    Custom permission class to authenticate guest token
    """

    def __init__(self, allowed_methods):
        self.allowed_methods = allowed_methods

    def has_permission(self, request, view):

        token = request.META.get('HTTP_GUEST_TOKEN', None)

        if token == settings.GUEST_TOKEN:

            return request.method in self.allowed_methods

        else:

            if request.user.is_superuser:
                return request.method in self.allowed_methods


class GuestTokenPermissionSetPin(permissions.BasePermission):
    """
    Custom permission class to authenticate guest token
    """

    def __init__(self, allowed_methods):
        self.allowed_methods = allowed_methods

    def has_permission(self, request, view):

        token = request.META.get('HTTP_GUEST_TOKEN_SET_PIN', None)

        if token == settings.GUEST_TOKEN_SET_PIN:

            return request.method in self.allowed_methods

        else:

            if request.user.is_superuser:
                return request.method in self.allowed_methods


class GuestTokenPermissionFCM(permissions.BasePermission):
    """
    Custom permission class to authenticate guest token
    """

    def __init__(self, allowed_methods):
        self.allowed_methods = allowed_methods

    def has_permission(self, request, view):

        token = request.META.get('HTTP_GUEST_TOKEN_FCM', None)

        if token == settings.GUEST_TOKEN_FCM:

            return request.method in self.allowed_methods

        else:

            if request.user.is_superuser:
                return request.method in self.allowed_methods


class GuestTokenPermissionRawData(permissions.BasePermission):
    """
    Custom permission class to authenticate guest token
    """

    def __init__(self, allowed_methods):
        self.allowed_methods = allowed_methods

    def has_permission(self, request, view):

        token = request.META.get('HTTP_GUEST_TOKEN_RAW_DATA', None)

        if token == settings.GUEST_TOKEN_RAW_DATA:

            return request.method in self.allowed_methods

        else:

            if request.user.is_superuser:
                return request.method in self.allowed_methods


class GuestTokenPermissionInvite(permissions.BasePermission):
    """
    Custom permission class to authenticate guest token
    """

    def __init__(self, allowed_methods):
        self.allowed_methods = allowed_methods

    def has_permission(self, request, view):

        token = request.META.get('HTTP_GUEST_TOKEN_INVITE', None)

        if token == settings.GUEST_TOKEN_INVITE:

            return request.method in self.allowed_methods

        else:

            if request.user.is_superuser:
                return request.method in self.allowed_methods


class GuestTokenPermissionPincodeCheck(permissions.BasePermission):
    """
    Custom permission class to authenticate guest token
    """

    def __init__(self, allowed_methods):
        self.allowed_methods = allowed_methods

    def has_permission(self, request, view):

        token = request.META.get('HTTP_GUEST_TOKEN_PINCODE', None)

        if token == settings.GUEST_TOKEN_PINCODE:

            return request.method in self.allowed_methods

        else:

            if request.user.is_superuser:
                return request.method in self.allowed_methods


class GuestTokenPermissionOTP(permissions.BasePermission):
    """
    Custom permission class to authenticate guest token
    """

    def __init__(self, allowed_methods):
        self.allowed_methods = allowed_methods

    def has_permission(self, request, view):

        token = request.META.get('HTTP_GUEST_TOKEN_OTP_VERIFY', None)

        if token == settings.GUEST_TOKEN_OTP:

            return request.method in self.allowed_methods

        else:

            if request.user.is_superuser:
                return request.method in self.allowed_methods


class GuestTokenLoanDetails(permissions.BasePermission):
    """
    Custom permission class to authenticate guest token
    """

    def __init__(self, allowed_methods):
        self.allowed_methods = allowed_methods

    def has_permission(self, request, view):

        token = request.META.get('HTTP_GUEST_TOKEN_LOAN_DETAILS', None)

        if token == settings.GUEST_TOKEN_LOAN_DETAILS:

            return request.method in self.allowed_methods

        else:

            if request.user.is_superuser:
                return request.method in self.allowed_methods


class GuestTokenLendingAdda(permissions.BasePermission):
    """
    Custom permission class to authenticate Lending Adda
    """

    def __init__(self, allowed_methods):
        self.allowed_methods = allowed_methods

    def has_permission(self, request, view):

        token = request.META.get('HTTP_LENDING_ADDA_TOKEN', None)

        if token == settings.LENDING_ADDA_TOKEN:

            return request.method in self.allowed_methods

        else:

            if request.user.is_superuser:
                return request.method in self.allowed_methods


class GuestYoloToken(permissions.BasePermission):
    """
    Custom permission class to authenticate Yolo
    """

    def __init__(self, allowed_methods):
        self.allowed_methods = allowed_methods

    def has_permission(self, request, view):

        token = request.META.get('HTTP_YELO_TOKEN', None)

        if token == settings.YELO_TOKEN:

            return request.method in self.allowed_methods

        else:

            if request.user.is_superuser:
                return request.method in self.allowed_methods


class GuestTokenPermissionMudrakwik(permissions.BasePermission):
    """
    Custom permission class to authenticate guest token
    """

    def __init__(self, allowed_methods):
        self.allowed_methods = allowed_methods

    def has_permission(self, request, view):

        token = request.META.get('HTTP_GUEST_TOKEN_MUDRAKWIK', None)

        if token == settings.GUEST_TOKEN_MUDRAKWIK:

            return request.method in self.allowed_methods

        else:

            if request.user.is_superuser:
                return request.method in self.allowed_methods


class GuestTokenPermissionRupeekwik(permissions.BasePermission):
    """
    Custom permission class to authenticate guest token
    """

    def __init__(self, allowed_methods):
        self.allowed_methods = allowed_methods

    def has_permission(self, request, view):

        token = request.META.get('HTTP_GUEST_TOKEN_RUPEEKWIK', None)

        if token == settings.GUEST_TOKEN_RUPEEKWIK:

            return request.method in self.allowed_methods

        else:

            if request.user.is_superuser:
                return request.method in self.allowed_methods


class GuestTokenPermissionNpciReact(permissions.BasePermission):
    """
    Custom permission class to authenticate guest token from react
    """

    def __init__(self, allowed_methods):
        self.allowed_methods = allowed_methods

    def has_permission(self, request, view):

        token = request.META.get('HTTP_GUEST_TOKEN_NPCI_REACT', None)

        if token == settings.GUEST_TOKEN_NPCI_REACT:
            return request.method in self.allowed_methods
        else:
            if request.user.is_superuser:
                return request.method in self.allowed_methods
