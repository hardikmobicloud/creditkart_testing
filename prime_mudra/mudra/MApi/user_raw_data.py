import datetime
from functools import partial

from django.http import HttpResponse
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from django.contrib.auth.models import User
from django.apps import apps

from MApi.permissions import GuestTokenPermissionRawData
from . import user_raw_data_serializer
import json
from IcomeExpense.views import parseSms

DeviceData = apps.get_model('UserData', 'DeviceData')
SmsStringMaster = apps.get_model('IcomeExpense', 'SmsStringMaster')
SmsKeyWordMaster = apps.get_model('IcomeExpense', 'SmsKeyWordMaster')
UserMerchantDetails = apps.get_model('UserData', 'UserMerchantDetails')
UserCreditCardDetails = apps.get_model('UserData', 'UserCreditCardDetails')
UserDebitCardDetails = apps.get_model('UserData', 'UserDebitCardDetails')
UserBankDetails = apps.get_model('UserData', 'UserBankDetails')
InstalledAppsDetails = apps.get_model('UserData', 'InstalledApps')


class UserDeviceData(APIView):
    """
    API to import user raw data version 1

    :parameter:

        API to store user raw data details in db

        Parameters

        {
            "primary_mobile_number": "CharField",           # 9988776655
            "default_email": "EmailField",                  # user@testemail.com
            "device_model": "CharField",                    # Redmi Note 5
            "device_brand": "CharField",                    #
            "version_number": "CharField",                  #
            "release": "CharField",                         #
            "screen_size": "CharField",                     # 5.99 inch
            "device_id": "CharField",                       #
            "imei_number": "CharField",                     # 990000862471854
            "installer": "CharField",                       # play store
            "is_rooted": "BooleanField"                     # false/true
            "is_emulated": "BooleanField",                  # false/true
            "is_debuggable": "BooleanField"                 # false/true
        }

        Response

        {
            "status": "CharField",                          # 0
            "message": "CharField,                          # Raw data added successfully
        }

        Note

        guest-token base authentication

        "guest-token-raw-data" : "kg411dm1-3dk1-30cm-fw01-fc01ft112xwn"

        status 1 : Raw data added successfully
        status 1 : Device id is already exist in data-base and update
        status 10 : Entered data is in-valid (Validation Error)
        status 11 : Something went wrong (Django Error)


    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        # parameter's

        status = None
        message = None
        device_data = request.data["DeviceData"]
        primary_mobile_number = device_data["phoneNumber"]
        default_email = ""
        device_model = device_data["model"]
        device_brand = device_data["deviceName"]
        version_number = device_data["ApiLevel"]
        release = device_data["systemVersion"]
        screen_size = ""
        device_id = device_data["uniqueId"]
        imei_number = ""
        installer = ""
        is_rooted = ""
        is_emulated = device_data["isEmulator"]
        is_debuggable = ""
        is_rooted_b = False
        is_emulated_b = False
        is_debuggable_b = False

        try:

            # check device_id is already present in raw data

            user_raw_data_instance = DeviceData.objects.filter(device_id=device_id, user_id=request.user.id)

            if not user_raw_data_instance:

                if is_rooted == 'true':
                    is_rooted_b = True

                if is_debuggable == 'true':
                    is_debuggable_b = True

                if is_emulated == 'true':
                    is_emulated_b = True

                # add raw data entry
                DeviceData.objects.create(
                    primary_mobile_number=primary_mobile_number, default_email=default_email,
                    device_model=device_model, device_brand=device_brand,
                    version_number=version_number,
                    release=release, screen_size=screen_size,
                    device_id=device_id, imei_number=imei_number,
                    installer=installer, is_rooted=is_rooted_b,
                    is_emulated=is_emulated_b, is_debuggable=is_debuggable_b,
                    user_id=request.user.id
                )

                status = '1'
                message = 'Raw data added successfully'

            else:

                if is_rooted == 'true':
                    is_rooted_b = True

                if is_debuggable == 'true':
                    is_debuggable_b = True

                if is_emulated == 'true':
                    is_emulated_b = True

                DeviceData.objects.filter(device_id=device_id).update(
                    primary_mobile_number=primary_mobile_number, default_email=default_email,
                    device_model=device_model, device_brand=device_brand, version_number=version_number,
                    release=release, screen_size=screen_size, device_id=device_id,
                    imei_number=imei_number, installer=installer, is_rooted=is_rooted_b,
                    is_emulated=is_emulated_b, is_debuggable=is_debuggable_b
                )

                status = '1'
                message = 'Device id is already exists and updated successfully'

        except Exception as e:
            print("-----Exception----", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)

            status = '11'
            message = 'Something went wrong!'

        return Response(
            {
                'status': status,
                'message': message
            }
        )


class SmsKeyWordListAPI(ListAPIView):
    """
    Get list of all sms keyword's from master db
    """

    def get(self, request, *args, **kwargs):
        if request.method == 'GET':
            all_keywords = SmsKeyWordMaster.objects.all()
            last_keywords = SmsKeyWordMaster.objects.all().last()

            serializer = user_raw_data_serializer.SmsKeyWordSerializer(all_keywords, many=True)
            return Response(
                {
                    'keywords': serializer.data,
                    'updated_date': last_keywords.updated_date
                }
            )


class SmsImportView(APIView):
    """
    To store sms data of user
    :parameter:

        Parameter
        {
            "sms":[
                {
                    "sender_id" : "CharField",
                    "message" : "TextField",
                    "date_time" : "Datetime",  # format yyyy-mm-dd H:M:S
                    "location" : "CharField", # lat and long
                }
            ]
        }


        Sample Data
        {
            "sms":[
                {
                    "sender_id": "ICICIB",
                    "message": "Dear Customer, payment of Rs. 1677 ......",
                    "date_time": "2019-01-02 12:16:00",
                    "location": "18.4865,73.7968"
                },
                {
                    "sender_id": "HDFCBK",
                    "message": "UPDATE: Your A/c XX1141 is debited .........",
                    "date_time": "2019-01-02 15:14:00",
                    "location": "18.4865,73.7968"
                },
                {
                    "sender_id": "ATMSBI",
                    "message": "Rs 400 w/d at SBI ATM S1BG000454391 SBI ......",
                    "date_time": "2019-01-12 09:30:00",
                    "location": "18.4865,73.7968"
                },
                {
                    "sender_id": "CITYBK",
                    "message": "9.46 USD was spent on Card 4386XXXXXXXX6534 ......",
                    "date_time": "2019-01-15 18:16:00",
                    "location": "18.4865,73.7968"
                }
            ]
        }


        Response

        {
            'status': 'CharField',      # 1
            'message': 'CharField'      # Added SMS
        }

        Note

        Authentication token required

        status 0 : SMS data added successfully
        status 1 : Response is empty
        status 2 : Internal server error (Django Error)
        status 3 : User is in-active

    """

    parser_classes = (JSONParser,)
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        import re


        try:

            json_data = request.data
            user = User.objects.get(id=request.user.id)

            if user.is_active:

                if json_data['sms']:

                    json_parameters = json_data['sms']

                    json_length = len(json_parameters)

                    for i in range(0, json_length):
                        # Validating Transaction SMS
                        sender_id = json_parameters[i]['sender_id']
                        match = re.search(r'^[a-zA-Z]{2}[\-][A-Za-z]{6}$', sender_id)
                        if match:

                            time = int(json_parameters[i]['date_time'])
                            date_time = datetime.datetime.fromtimestamp(time / 1000.0)


                            if 'location' in json_parameters[i]:
                                location = json_parameters[i]['location']
                            else:
                                location = ''

                            sms_master_instance = SmsStringMaster.objects.filter(
                                sms_string=json_parameters[i]['message'],
                                sender_id=json_parameters[i]['sender_id'],
                                location=location,
                                date_time=date_time,
                                user=user
                            )
                            if sms_master_instance:
                                pass

                            else:
                                time = int(json_parameters[i]['date_time'])
                                date_time = datetime.datetime.fromtimestamp(time / 1000.0)


                                SmsStringMaster.objects.create(
                                    sms_string=json_parameters[i]['message'],
                                    sender_id=json_parameters[i]['sender_id'],
                                    location=location,
                                    date_time=date_time,
                                    user=user,
                                )
                        else:
                            pass

                    StoreSmsDetails(request)

                    return Response(
                        {
                            'status': '1',
                            'message': 'Added SMS'
                        }
                    )

                else:

                    return Response(
                        {
                            'status': '0',
                            'message': 'Empty data'
                        }
                    )

            else:

                return Response(
                    {
                        'status': '3',
                        'message': 'User is inactive'
                    }
                )

        except Exception as e:
            import sys
            import os
            print('-----------in exception----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(e.args, exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': '2',
                    'message': {
                        'error': str(e.args),

                        'type': str(exc_type),

                        'file': str(fname),
                        'line': str(exc_tb.tb_lineno)
                    }
                }
            )


def StoreSmsDetails(request):
    user = User.objects.get(id=request.user.id)

    sms_string = SmsStringMaster.objects.filter(is_parsed=False, user=user).order_by('sms_string').distinct(
        'sms_string')

    sms_json_obj = list(sms_string.values('sender_id', 'sms_string', 'date_time'))

    sms_parser_response = parseSms(json.dumps(sms_json_obj, default=str))
    sms_json_response = json.loads(sms_parser_response)



    for i in range(0, len(sms_json_response)):

        if 'account_number' in sms_json_response[i]:
            UserBankDetails.objects.get_or_create(
                bank_id=sms_json_response[i]['bank_id'],
                transaction_amount=float(sms_json_response[i]['transaction_amount']),
                transaction_type=sms_json_response[i]['transaction_type'],
                transaction_date=sms_json_response[i]['transaction_date'],
                available_balance=float(sms_json_response[i]['available_balance']),
                account_number=sms_json_response[i]['account_number'],
                towards=sms_json_response[i]['towards'],
                category=sms_json_response[i]['category'],
                merchant=sms_json_response[i]['merchant'],
                user=user
            )

        if 'debit_card_number' in sms_json_response[i]:
            UserDebitCardDetails.objects.get_or_create(
                bank_id=sms_json_response[i]['bank_id'],
                transaction_amount=float(sms_json_response[i]['transaction_amount']),
                transaction_type=sms_json_response[i]['transaction_type'],
                transaction_date=sms_json_response[i]['transaction_date'],
                available_balance=float(sms_json_response[i]['available_balance']),
                debit_card_number=sms_json_response[i]['debit_card_number'],
                towards=sms_json_response[i]['towards'],
                category=sms_json_response[i]['category'],
                merchant=sms_json_response[i]['merchant'],
                user=user
            )

        if 'credit_card_number' in sms_json_response[i]:
            UserCreditCardDetails.objects.get_or_create(
                bank_id=sms_json_response[i]['bank_id'],
                transaction_amount=float(sms_json_response[i]['transaction_amount']),
                transaction_type=sms_json_response[i]['transaction_type'],
                transaction_date=sms_json_response[i]['transaction_date'],
                available_balance=float(sms_json_response[i]['available_balance']),
                credit_card_number=sms_json_response[i]['credit_card_number'],
                towards=sms_json_response[i]['towards'],
                category=sms_json_response[i]['category'],
                merchant=sms_json_response[i]['merchant'],
                user=user
            )

        if 'merchant_id' in sms_json_response[i]:

            if 'paytm' in sms_json_response[i]['merchant_id'].lower() \
                    and float(sms_json_response[i]['transaction_amount']) == 0.0:
                pass

            else:
                UserMerchantDetails.objects.get_or_create(
                    merchant_id=sms_json_response[i]['merchant_id'],
                    transaction_amount=float(sms_json_response[i]['transaction_amount']),
                    transaction_type=sms_json_response[i]['transaction_type'],
                    transaction_date=sms_json_response[i]['transaction_date'],
                    category=sms_json_response[i]['category'],
                    user=user
                )




class UserAccountsListAPIView(ListAPIView):
    """
    API to view list of user merchant's, account no, cc number, dc number

    :parameter:

        API to view user merchant, bank, credit and debit card number details

        Response

        {
            "merchant_list": [{}],
            "net_banking_acc_number": [{}],
            "credit_card_number": [{}],
            "debit_card_number": [{}],
        }

        Note

        Authentication token required

        [
            {
                "key":"Authorization",
                "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
            }
        ]

    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):

        merchant_list = []
        net_banking_acc_number = []
        credit_card_number = []
        debit_card_number = []

        try:

            user_instance = User.objects.get(id=request.user.id)

            user_merchant_obj = UserMerchantDetails.objects.filter(user=user_instance).order_by(
                '-merchant_id').distinct('merchant_id')
            user_account_no_obj = UserBankDetails.objects.filter(user=user_instance).order_by(
                '-account_number').distinct('account_number')
            user_credit_card_no_obj = UserCreditCardDetails.objects.filter(user=user_instance).order_by(
                '-credit_card_number').distinct('credit_card_number')
            user_debit_card_no_obj = UserDebitCardDetails.objects.filter(user=user_instance).order_by(
                '-debit_card_number').distinct('debit_card_number')

            merchant_serializer = user_raw_data_serializer.UserMerchantListDistSerializer(user_merchant_obj, many=True)
            user_account_no_serializer = user_raw_data_serializer.UserBankDetailsListDistSerializer(user_account_no_obj,
                                                                                                    many=True)
            user_credit_no_serializer = user_raw_data_serializer.UserCreditCardDetailsListDistSerializer(
                user_credit_card_no_obj, many=True)
            user_debit_no_serializer = user_raw_data_serializer.UserDebitCardDetailsDetailsListDistSerializer(
                user_debit_card_no_obj, many=True)

            merchant_list = merchant_serializer.data
            net_banking_acc_number = user_account_no_serializer.data
            credit_card_number = user_credit_no_serializer.data
            debit_card_number = user_debit_no_serializer.data

        except Exception as e:

            pass

        return Response(
            {
                'merchant_list': merchant_list,
                'net_banking_acc_number': net_banking_acc_number,
                'credit_card_number': credit_card_number,
                'debit_card_number': debit_card_number,
            }
        )


class InstalledAppAPI(ListAPIView):
    """
    Get list of all installed apps
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        user_id = request.user.id
        get_installed_apps = InstalledAppsDetails.objects.filter(user_id=user_id).values_list('name', flat=True)

        jsondata = request.body
        data = json.loads(jsondata)
        dic = data['InstalledApps']
        ctx = {}

        for i in dic:
            app_name = i['appName']
            last_update_time = i['lastUpdateTime']
            first_install_time = i['firstInstallTime']
            package_name = i['packageName']
            first_install_time = datetime.datetime.fromtimestamp(first_install_time / 1000.0)
            last_update_time = datetime.datetime.fromtimestamp(last_update_time / 1000.0)

            if app_name not in list(get_installed_apps):

                try:
                    InstalledAppsDetails.objects.create(user_id=user_id, name=app_name,
                                                        package_name=package_name,
                                                        first_install_time=first_install_time,
                                                        last_update_time=last_update_time
                                                        )
                except Exception as e:
                    pass
        return Response(status=status.HTTP_201_CREATED)
