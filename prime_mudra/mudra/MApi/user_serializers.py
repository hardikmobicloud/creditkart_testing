from django.apps.registry import apps
from django.core.validators import RegexValidator
from rest_framework import serializers


PersonalDetails = apps.get_model('User', 'Personal')
BankDetails = apps.get_model('User', 'Bank')
AddressDetails = apps.get_model('User', 'Address')
ReferenceDetails = apps.get_model('User', 'Reference')
DocumentDetails = apps.get_model('User', 'Documents')
ProfessionalDetails = apps.get_model('User', 'Professional')
LocationDetails = apps.get_model('User', 'Location')


class Personal(serializers.ModelSerializer):
    class Meta:
        model = PersonalDetails

        fields = ('__all__')





class Address(serializers.ModelSerializer):
    class Meta:
        model = AddressDetails
        fields = ('__all__')


class Reference(serializers.ModelSerializer):
    class Meta:
        model = ReferenceDetails
        fields = ('__all__')


class Bank(serializers.ModelSerializer):
    class Meta:
        model = BankDetails
        fields = ('__all__')


class Professional(serializers.ModelSerializer):
    class Meta:
        model = ProfessionalDetails
        fields = ('__all__')


class Document(serializers.ModelSerializer):
    class Meta:
        model = DocumentDetails
        fields = ('__all__')


class Location(serializers.ModelSerializer):
    class Meta:
        model = LocationDetails
        fields = ('__all__')


class Base64ImageField(serializers.ImageField):
    """
    A Django REST framework field for handling image-uploads through raw post data.
    It uses base64 for encoding and decoding the contents of the file.

    Heavily based on
    https://github.com/tomchristie/django-rest-framework/pull/1268

    Updated for Django REST framework 3.
    """

    def to_internal_value(self, data):

        from django.core.files.base import ContentFile
        import base64
        import six
        import uuid
        import random


        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')

            # Generate file name:
            file_name = str(uuid.uuid4())[:12]  # 12 characters are more than enough.
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension,)

            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr
        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension


class LoanUploadDocumentSerailizer(serializers.ModelSerializer):
    path = Base64ImageField(required=False, allow_null=True, allow_empty_file=True)

    class Meta:
        model = DocumentDetails
        fields = (
            'path', 'type', 'user'
        )


class PincodeValidateSerializer(serializers.Serializer):
    pincode = serializers.CharField(required=True, max_length=6, min_length=6,
                                    validators=[RegexValidator('^([0-9]{6})+$', message='Enter a valid pin code.')])


class PanCardValidateSerializer(serializers.Serializer):
    pan_card = serializers.CharField(required=True)




class Personal1(serializers.Serializer):
    name = serializers.CharField(required=True)
    fathers_name = serializers.CharField(required=True)
    birthdate = serializers.CharField(required=True)

    gender = serializers.CharField(required=True)
    marital_status = serializers.CharField(required=True)
    occupation = serializers.CharField(required=True)
    mobile_no = serializers.CharField(required=True)
    alt_mobile_no = serializers.CharField(required=True)
    email_id = serializers.CharField(required=True)
    monthly_income = serializers.CharField(required=True)
    pan_number = serializers.CharField(required=True)
    aadhar_number = serializers.CharField(required=False)
    type = serializers.CharField(required=True)
    itr = serializers.CharField(required=True)
    mobile_linked_aadhaar = serializers.CharField(required=True)
    residence_type = serializers.CharField(required=True)
    industry_type = serializers.CharField(required=True)
    educational = serializers.CharField(required=True)
    user_id = serializers.CharField(required=True)

    def create(self, validated_data):
        # validated_data.get(user=request.user)

        return PersonalDetails.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.fathers_name = validated_data.get('fathers_name', instance.fathers_name)

        return instance

class Professional1(serializers.Serializer):
    company_name = serializers.CharField(required=True)
    company_email_id = serializers.CharField(required=True)
    company_address = serializers.CharField(required=True)
    company_landline_no = serializers.CharField(required=True)
    company_designation = serializers.CharField(required=True)
    experience = serializers.CharField(required=True)
    join_date = serializers.CharField(required=True)
    user_id = serializers.CharField(required=True)

    def create(self, validated_data):
        return ProfessionalDetails.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.fathers_name = validated_data.get('fathers_name', instance.fathers_name)

        return instance


class Reference1(serializers.Serializer):
    colleague_name = serializers.CharField(required=True)
    colleague_mobile_no = serializers.CharField(required=True)
    colleague_email_id = serializers.CharField(required=True)
    relationships = serializers.CharField(required=True)
    relative_name = serializers.CharField(required=True)
    relative_mobile_no = serializers.CharField(required=True)
    relative_email_id = serializers.CharField(required=True)
    user_id = serializers.CharField(required=True)

    def create(self, validated_data):
        return ReferenceDetails.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.colleague_name = validated_data.get('colleague_name', instance.colleague_name)
        instance.colleague_mobile_no = validated_data.get('colleague_mobile_no', instance.colleague_mobile_no)

        return instance


class Bank1(serializers.Serializer):
    bank_name = serializers.CharField(required=True)
    account_number = serializers.CharField(required=True)
    ifsc_code = serializers.CharField(required=True)
    user_id = serializers.CharField(required=True)
    type = serializers.CharField(required=True)

    def create(self, validated_data):
        return BankDetails.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.ifsc_code = validated_data.get('name', instance.ifsc_code)

        return instance


class Address1(serializers.Serializer):
    address = serializers.CharField(required=True)
    city = serializers.CharField(required=True)
    state = serializers.CharField(required=True)
    pin_code = serializers.CharField(required=True)
    type = serializers.CharField(required=True)
    user_id = serializers.CharField(required=True)

    def create(self, validated_data):

            return AddressDetails.objects.create(**validated_data)


    def update(self, instance, validated_data):
        instance.address = validated_data.get('address', instance.address)

        return instance
