import datetime
import hashlib
import json
import time
import logging
import random
import re
from functools import partial
from datetime import date, timedelta

import requests
from User.models import *
from django.apps import apps
from django.conf import settings
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render
from mudra import settings
from mudra.constants import scheme
from rest_framework import generics, status
from rest_framework.authentication import *
from rest_framework.permissions import *
from rest_framework.response import Response
from rest_framework.views import APIView
from .permissions import GuestTokenPermission, GuestTokenLendingAdda

LendingAdda = apps.get_model('lead', 'LendingAdda')
Subscription = apps.get_model('User', 'Subscription')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')


class LendingAddaUserRegister(APIView):
    """
        Send Lending adda
    """

    def post(self, request, *args, **kwargs):
        today = datetime.datetime.now()
        api_data = {}
        URL = "http://demo-openapi.lendingadda.com:8080/partnerevent/on"

        # Headers
        api_data['sign'] = 'a2960e022ccdd57ba0674332d3a4ab89'
        secret_key = 'a2960e022ccdd57ba0674332d3a4ab89'
        api_data['channelId'] = '36'
        api_data['data'] = []

        yesterday = date.today() - timedelta(days=1)
        leandingadda_subscription = LendingAdda.objects.filter(status=False, is_subscribed=False,
                                                               click_date__startswith=yesterday)
        register_user = []

        for leanding in leandingadda_subscription:
            print(leanding.mobile)
            data = {}
            lead_data = {}

            user = User.objects.filter(username=leanding.mobile)
            if user:

                subscription = Subscription.objects.filter(user_id=user[0].id, payment_status='success').order_by('-id')


                if subscription:
                    if leanding.click_date:
                        if leanding.click_date <= subscription[0].start_date and leanding.click_date <= user[
                            0].date_joined:
                            lead_data = {"mobile": "", "productCode": "SYS1574324797", "partnerOderId": "",
                                         "clickId": "", "advertiserId": "", "android_id": "", "idfa": "",
                                         "eventName": "Install", "eventTimestamp": "", "eventNum": "0",
                                         "eventvalue": "",
                                         }
                            lead_data['mobile'] = leanding.mobile
                            lead_data['eventTimestamp'] = int(time.time())
                            lead_data['clickId'] = leanding.click_id

                            register_user.append(lead_data)
                            LendingAdda.objects.filter(mobile=leanding.mobile).update(is_subscribed=True,
                                                                                      updated_date=today)
        api_data['data'] = str(register_user)

        get_sign = generate_sign(api_data, secret_key)
        api_data['sign'] = get_sign

        response = requests.post(url=URL, data=json.dumps(api_data), headers={"Content-type": "application/json"})
        return HttpResponse(response)


def send_install_data(request):
    today = datetime.datetime.now()
    api_data = {}
    URL = "https://openapi.lendingadda.com/partnerevent/on"

    # Headers
    api_data['sign'] = ''
    secret_key = 'a2960e022ccdd57ba0674332d3a4ab89'
    api_data['channelId'] = 24
    api_data['data'] = []


    yesterday = date.today() - timedelta(days=1)

    leandingadda_subscription = LendingAdda.objects.filter(status=False, is_subscribed=False,
                                                           click_date__startswith=yesterday)
    register_user = []


    for leanding in leandingadda_subscription:
        data = {}
        lead_data = {}


        user = User.objects.filter(username=leanding.mobile)

        if user:
            subscription = Subscription.objects.filter(user_id=user[0].id, payment_status='success').order_by('-id')

            if subscription:
                if leanding.click_date:

                    if leanding.click_date <= subscription[0].start_date and leanding.click_date <= user[
                        0].date_joined:
                        print('new sub check')
                        lead_data = {"mobile": "", "productCode": "SYS1569480505", "partnerOrderId": "",
                                     "clickId": "", "advertiserId": "1", "android_id": "", "idfa": "1",
                                     "eventName": "Registration", "eventTimestamp": "", "eventNum": "0",
                                     "eventvalue": ""}

                        lead_data['mobile'] = leanding.mobile
                        lead_data['eventTimestamp'] = int(time.time())
                        lead_data['clickId'] = leanding.click_id

                        register_user.append(lead_data)
                        LendingAdda.objects.filter(mobile=leanding.mobile).update(is_subscribed=True,
                                                                                  updated_date=today)
    api_data['data'] = str(json.dumps(register_user))

    get_sign = generate_sign(api_data, secret_key)

    api_data['sign'] = get_sign



    response = requests.post(url=URL, data=json.dumps(api_data), headers={"Content-type": "application/json"})
    return HttpResponse(response)


class LendingAddaApproved(APIView):

    def post(self, request, *args, **kwargs):
        api_data = {}
        # Headers
        api_data['sign'] = ''
        secret_key = 'a2960e022ccdd57ba0674332d3a4ab89'
        api_data['channelId'] = '36'
        api_data['data'] = []

        URL = "http://demo-openapi.lendingadda.com:8080/partnerevent/on"

        today = datetime.datetime.now()
        yesterday = date.today() - timedelta(days=1)
        leandingadda_subscription = LendingAdda.objects.filter(status=False, is_approved=False, is_subscribed=True,
                                                               click_date__startswith=yesterday)
        loan_approved = []
        for leanding in leandingadda_subscription:
            data = {}
            user = User.objects.filter(username=leanding.mobile)
            if user:
                subscription = Subscription.objects.filter(user_id=user[0].id, payment_status='success').order_by('-id')
                if leanding.click_date <= subscription[0].start_date and leanding.click_date <= user[0].date_joined:
                    loan = LoanApplication.objects.filter(user_id=user[0].id, status='APPROVED').order_by('-id')
                    if leanding.click_date <= loan[0].loan_start_date:
                        start_var = loan[0].loan_start_date.strftime('%Y-%m-%d')
                        create_var = loan[0].created_date.strftime('%Y-%m-%d')
                        if start_var == create_var:

                            LendingAdda.objects.filter(mobile=leanding.mobile).update(is_approved=True, status=True,
                                                                                      updated_date=today)
                            lead_data = {"mobile": "", "productCode": "SYS1574324797", "partnerOrderId": "",
                                         "clickId": "",
                                         "advertiserId": "", "android_id": "", "idfa": "", "eventName": "LoanDisbursed",
                                         "eventTimestamp": "", "eventNum": "0", "eventvalue": ""}
                            lead_data["mobile"] = leanding.mobile
                            lead_data['eventTimestamp'] = int(time.time())
                            lead_data['clickId'] = leanding.click_id

                            loan_approved.append(lead_data)
        api_data['data'] = str(json.dumps(loan_approved))
        get_sign = generate_sign(api_data, secret_key)

        api_data['sign'] = get_sign


        response = requests.post(url=URL, data=json.dumps(api_data), headers={"Content-type": "application/json"})
        return HttpResponse(response)


def send_approved_data(request):
    today = datetime.datetime.now()
    api_data = {}
    URL = "https://openapi.lendingadda.com/partnerevent/on"

    # Headers
    api_data['sign'] = ''
    secret_key = 'a2960e022ccdd57ba0674332d3a4ab89'
    api_data['channelId'] = 24
    api_data['data'] = []

    yesterday = date.today() - timedelta(days=1)
    leandingadda_subscription = LendingAdda.objects.filter(status=False, is_subscribed=True,
                                                           click_date__startswith=yesterday)

    loan_approved = []
    for leanding in leandingadda_subscription:
        data = {}
        user = User.objects.filter(username=leanding.mobile)
        if user:
            subscription = Subscription.objects.filter(user_id=user[0].id, payment_status='success').order_by('-id')
            if leanding.click_date <= subscription[0].start_date and leanding.click_date <= user[0].date_joined:
                loan = LoanApplication.objects.filter(user_id=user[0].id, status='APPROVED').order_by('-id')
                if leanding.click_date <= loan[0].loan_start_date:
                    start_var = loan[0].loan_start_date.strftime('%Y-%m-%d')
                    create_var = loan[0].created_date.strftime('%Y-%m-%d')
                    if start_var == create_var:

                        LendingAdda.objects.filter(mobile=leanding.mobile).update(is_approved=True, status=True,
                                                                                  updated_date=today)
                        lead_data = {"mobile": "", "productCode": "SYS1569480505", "partnerOrderId": "",
                                     "clickId": "", "advertiserId": "1", "android_id": "", "idfa": "1",
                                     "eventName": "LoanDisbursed", "eventTimestamp": "", "eventNum": "0",
                                     "eventvalue": ""}

                        lead_data['mobile'] = leanding.mobile
                        lead_data['eventTimestamp'] = int(time.time())
                        lead_data['clickId'] = leanding.click_id
                        loan_approved.append(lead_data)

    api_data['data'] = str(json.dumps(loan_approved))

    get_sign = generate_sign(api_data, secret_key)

    api_data['sign'] = get_sign


    response = requests.post(url=URL, data=json.dumps(api_data), headers={"Content-type": "application/json"})
    return HttpResponse(response)


class GetLendingAddaLeads(APIView):
    """
        Register's user if not there
        :parameter
            mobile_number

        :response
        success:
            {
                'status_code': '2',
                'message': 'User registered successfully'
            }
            status: 201
        failure:
            {
                    'status_code': 2 / 3,
                    'message': 'User registered successfully' / 'Something went wrong'
            }
            status: 400
    """

    # permission_classes = (partial(GuestTokenLendingAdda, ['GET', 'POST', 'HEAD']),)
    permission_classes = (partial(GuestTokenPermission, ['GET', 'POST', 'HEAD']),)

    def post(self, request, *args, **kwargs):

        mobile_number = request.data['mobile_number']
        click_id = request.data['click_id']
        sign = request.data['sign']
        timestamp = request.data['timestamp']
        try:
            timestamp = datetime.datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d')


        except Exception as e:
            print(e)
            pass

        if mobile_number:
            validate_number = re.compile(r'^[6-9]\d{9}$')

            validate = validate_number.search(mobile_number)

            if validate:

                is_user_registered = LendingAdda.objects.filter(mobile=mobile_number)
                if is_user_registered.exists():
                    return Response(
                        {
                            'status_code': '1',
                            'message': 'User already registered',
                        },
                        status=status.HTTP_400_BAD_REQUEST
                    )
                try:
                    today = datetime.datetime.today()
                    today = today.strftime("%Y-%m-%d")
                    register_user = LendingAdda.objects.create(mobile=mobile_number, click_date=timestamp,
                                                               click_id=click_id, sign=sign)
                except Exception as e:
                    print(e)
                    register_user = None
                if register_user and register_user is not None:
                    return Response(
                        {
                            'status_code': '2',
                            'message': 'User registered successfully'
                        },
                        status=status.HTTP_201_CREATED
                    )
                else:
                    return Response(
                        {
                            'status_code': '3',
                            'message': 'Something went wrong'
                        },
                        status=status.HTTP_400_BAD_REQUEST
                    )
            else:
                return Response(
                    {
                        'status_code': '4',
                        'message': 'Invalid mobile number'
                    },
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                {
                    'status_code': '5',
                    'message': 'Please provide mobile number'
                },
                status=status.HTTP_400_BAD_REQUEST
            )


def tset(request):
    sk = 'a2960e022ccdd57ba0674332d3a4ab89'
    data = [
        {"mobile": "9388888888", "productCode": "SYS1560156347", "partnerOderId": "", "clickId": "", "advertiserId": "",
         "android_id": "", "idfa": "", "eventName": "Install", "eventTimestamp": "1571999728", "eventNum": "",
         "eventvalue": ""},
        {"mobile": "9388888888", "productCode": "hytzf8888888", "partnerOderId": "", "clickId": "", "advertiserId": "",
         "android_id": "", "idfa": "", "eventName": "LoanApplicaton", "eventTimestamp": "1571999728", "eventNum": "",
         "eventvalue": ""}]
    dict = {"sign": "-asdf-", "channelId": 14, "data": str(data)}

    hassh = generate_sign(dict, sk)
    return HttpResponse(hassh)


def generate_sign(dict, secret_key):
    lex_str = ''
    cnt = 0
    for key, value in sorted(dict.items()):
        cnt += 1
        if key == "sign" or value is None or value == '':
            pass
        else:
            lex_str += str(key) + "=" + str(value)
            lex_str += "&"
    if lex_str[-1] == "&":
        lex_str = lex_str[:-1]

    lex_str += secret_key
    hash = hashlib.md5(lex_str.encode('utf-8')).hexdigest()

    return hash