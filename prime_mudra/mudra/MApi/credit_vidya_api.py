from datetime import datetime
import random

from django.http import HttpResponse
from rest_framework import generics, status
from rest_framework.response import Response
import json
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.apps import apps
from rest_framework.views import APIView
import json
import requests
import time

LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
CreditVidyaApplication = apps.get_model('creditvidya', 'CreditVidyaApplication')
CreditVidyaAPP = apps.get_model('creditvidya', 'CreditVidyaAPP')
CreditVidyaSMS = apps.get_model('creditvidya', 'CreditVidyaSMS')
CreditVidyaDevice = apps.get_model('creditvidya', 'CreditVidyaDeviceData')
CreditVidyaLocation = apps.get_model('creditvidya', 'CreditVidyaLocation')
CreditVidyaContacts = apps.get_model('creditvidya', 'CreditVidyaContacts')
CVRegisterUser = apps.get_model('creditvidya', 'RegisterUser')
InstalledApps = apps.get_model('UserData', 'InstalledApps')
SmsStringMaster = apps.get_model('IcomeExpense', 'SmsStringMaster')
Location = apps.get_model('User', 'Location')
DeviceData = apps.get_model('UserData', 'DeviceData')
ContactMaster = apps.get_model('UserData', 'ContactMaster')


def test_cv_dd(request):

    credit_vidya_contacts(9335)
    return HttpResponse("ok")


def credit_vidya_sms(user_id):
    date = datetime.now()
    random_number = random.sample(range(0, 9999), 1)
    sms_id = str(date.year)[2:] + "" + str(date.month) + "" + str(date.day) + "" + str(
        date.hour) + "" + str(date.minute) + "" + str(date.second) + "" + str(random_number[0])
    data = {}
    sms_string = SmsStringMaster.objects.filter(user_id=user_id).order_by('-date_time')

    data_list = []

    date = sms_string[0].date_time
    for sms in sms_string:


        ft = datetime.strptime(str(date)[:19], '%Y-%m-%d %H:%M:%S')
        ts = time.mktime(ft.timetuple())

        boday = {
            "body": sms.sms_string,
            "address": sms.sender_id,
            "time": round(ts)
        }

        data_list.append(boday)

    data["uniqueId"] = sms_id
    data["type"] = "sms"
    data["timeStamp"] = str(date)[:10]
    data["data"] = data_list

    CreditVidyaSMS.objects.create(sms_id=sms_id, user_id=user_id, sms_create_date=sms_string[0].created_date,
                                  status='initiated')
    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
        'APIKEY': 'VjjXt3PtQvRYBkHkWnTP8SVhj0ijg348'
    }
    url_uat = "https://api-uat.creditvidya.com/mw/api/post/data"
    json_body = json.dumps(data)
    response = requests.request('POST', url_uat, data=json_body, headers=headers)

    response_json = response.json()
    status = response_json["status"]

    message = ""
    if "message" in response_json:
        message = response_json["message"]
    CreditVidyaSMS.objects.filter(sms_id=sms_id).update(status=status, message=message)
    return True


def credit_vidya_app(user_id):
    date = datetime.now()
    first_install = None
    last_update = None
    random_number = random.sample(range(0, 9999), 1)
    app_id = str(date.year)[2:] + "" + str(date.month) + "" + str(date.day) + "" + str(
        date.hour) + "" + str(date.minute) + "" + str(date.second) + "" + str(random_number[0])
    data = {}
    app_string = InstalledApps.objects.filter(user_id=user_id)

    data_list = []
    for app in app_string:
        if app.first_install_time:
            ft = datetime.strptime(str(app.first_install_time)[:19], '%Y-%m-%d %H:%M:%S')
            first_install = time.mktime(ft.timetuple())
        if app.last_update_time:
            lt = datetime.strptime(str(app.last_update_time)[:19], '%Y-%m-%d %H:%M:%S')
            last_update = time.mktime(lt.timetuple())

        body = {
            "packageName": app.package_name,
            "appName": app.name,
            "firstInstalledTime": round(first_install),
            "lastUpdatedTime": round(last_update)
        }

        data_list.append(body)
    data["uniqueId"] = app_id
    data["type"] = "apps"

    data["data"] = data_list

    CreditVidyaAPP.objects.create(app_id=app_id, user_id=user_id, app_create_date=app_string[0].created_date,
                                  status='initiated')
    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
        'APIKEY': 'VjjXt3PtQvRYBkHkWnTP8SVhj0ijg348'
    }
    url_uat = "https://api-uat.creditvidya.com/mw/api/post/data"
    json_body = json.dumps(data)
    response = requests.request('POST', url_uat, data=json_body, headers=headers)

    response_json = response.json()
    status = response_json["status"]
    message = ""
    if "message" in response_json:
        message = response_json["message"]
    CreditVidyaAPP.objects.filter(app_id=app_id).update(status=status, message=message)
    return True


def credit_vidya_device_data(user_id):
    data_list = []
    date = datetime.now()
    random_number = random.sample(range(0, 9999), 1)
    app_id = str(date.year)[2:] + "" + str(date.month) + "" + str(date.day) + "" + str(
        date.hour) + "" + str(date.minute) + "" + str(date.second) + "" + str(random_number[0])
    data = {}
    device_data_dict = {"deviceInfo": "", "lockPatternInfo": "", "screenAndDisplaySettings": "",
                        "availableListOfLaunchers": "", "keyboardDetails": "", "memoryInfo": "", "ramInfo": "",
                        "networkInfo": ""}
    device_info_dict = {
        "timeZoneId": "Asia/Calcutta",
        "timeZoneName": "India Standard Time",
        "versionRelease": "",
        "brand": "",
        "manufacturer": "",
        "model": "",
        "product": "",
        "serial": "",
        "hardware": "",
        "IsDeviceRooted": False,
        "deviceModel": "",
        "isMockLocationEnabled": "0",
        "sms_default_application": "com.android.messaging",
        "serial_blacklist": "",
        "androidId": "",
        "drmIdInfo": {
            "wideVine": ""
        }
    }
    get_device_data = DeviceData.objects.filter(user_id=user_id)
    if get_device_data:
        for i in get_device_data:
            device_info_dict['IsDeviceRooted'] = i.is_rooted
            device_info_dict['model'] = i.device_model
            device_info_dict['brand'] = i.device_brand
            device_info_dict['IsDeviceRooted'] = i.is_rooted
    device_data_dict["deviceInfo"] = device_info_dict
    data_list.append(device_data_dict)

    data["uniqueId"] = app_id
    data["type"] = "device"
    data["data"] = data_list

    CreditVidyaDevice.objects.create(app_id=app_id, user_id=user_id, app_create_date=get_device_data[0].created_date,
                                     status='initiated')
    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
        'APIKEY': 'VjjXt3PtQvRYBkHkWnTP8SVhj0ijg348'
    }
    url_uat = "https://api-uat.creditvidya.com/mw/api/post/data"
    json_body = json.dumps(data)
    response = requests.request('POST', url_uat, data=json_body, headers=headers)

    response_json = response.json()

    status = response_json["status"]
    message = ""
    if "message" in response_json:
        message = response_json["message"]
    CreditVidyaDevice.objects.filter(app_id=app_id).update(status=status, message=message)
    return True


def credit_vidya_location(user_id):
    date = datetime.now()
    random_number = random.sample(range(0, 9999), 1)
    app_id = str(date.year)[2:] + "" + str(date.month) + "" + str(date.day) + "" + str(
        date.hour) + "" + str(date.minute) + "" + str(date.second) + "" + str(random_number[0])
    data = {}
    data_list = []
    location_data_dict = {"reason": "installation", "startTime": 1565714239318, "locations": [], "scannedWifi": [{}],
                          "allCellInfo": [{}], "neighbouringCellInfo": [],
                          "connectedWifi": {}}
    location_dict = {
        "isFromMockProvider": "",
        "captureTime": "",
        "accuracy": "",
        "altitude": "",
        "bearing": "",
        "latitude": "",
        "longitude": "",
        "provider": "",
        "speed": "",
        "time": "",
        "elapsedRealtimeNanos": "",
        "verticalAccuracy": ""
    }
    get_location = Location.objects.filter(user_id=user_id).first()
    ft = datetime.strptime(str(get_location.date_created)[:19], '%Y-%m-%d %H:%M:%S')
    get_time = time.mktime(ft.timetuple())
    ft1 = datetime.strptime(str(date)[:19], '%Y-%m-%d %H:%M:%S')
    get_current_time = time.mktime(ft1.timetuple())

    if get_location:
        location_dict['accuracy'] = get_location.accuracy
        location_dict['altitude'] = get_location.altitude
        location_dict['latitude'] = get_location.latitude
        location_dict['longitude'] = get_location.longitude
        location_dict['speed'] = get_location.speed
        location_dict['captureTime'] = get_time
        location_dict['time'] = get_current_time
    location_data_dict['locations'] = [location_dict]
    data_list.append(location_data_dict)
    data["uniqueId"] = app_id
    data["type"] = "location"
    data["data"] = data_list

    create_cv = CreditVidyaLocation.objects.create(app_id=app_id, user_id=user_id,
                                                   app_create_date=get_location.date_created,
                                                   status='initiated')
    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
        'APIKEY': 'VjjXt3PtQvRYBkHkWnTP8SVhj0ijg348'
    }
    url_uat = "https://api-uat.creditvidya.com/mw/api/post/data"
    json_body = json.dumps(data)
    response = requests.request('POST', url_uat, data=json_body, headers=headers)

    response_json = response.json()
    status = response_json["status"]
    message = ""
    if "message" in response_json:
        message = response_json["message"]
    cv = CreditVidyaLocation.objects.filter(app_id=app_id).update(status=status, message=message)
    return True


def credit_vidya_contacts(user_id):
    date = datetime.now()
    updated_time = None
    random_number = random.sample(range(0, 9999), 1)
    app_id = str(date.year)[2:] + "" + str(date.month) + "" + str(date.day) + "" + str(
        date.hour) + "" + str(date.minute) + "" + str(date.second) + "" + str(random_number[0])
    data = {}
    data_list = []


    get_contacts = ContactMaster.objects.filter(user_id=user_id)
    if get_contacts:
        for i in get_contacts:
            contact_dict = {}
            if i.updated_date and i.updated_date is not None:
                ft = datetime.strptime(str(i.updated_date)[:19], '%Y-%m-%d %H:%M:%S')
                updated_time = time.mktime(ft.timetuple())
                contact_dict["name"] = i.contact_name
                contact_dict["phoneNumber"] = i.contact_no
                contact_dict["contactLastUpdatedTimestamp"] = round(updated_time)
            data_list.append(contact_dict)
    data["uniqueId"] = app_id
    data["type"] = "contacts-basic"
    data["data"] = data_list
    create_cv = CreditVidyaContacts.objects.create(app_id=app_id, user_id=user_id,
                                                   app_create_date=get_contacts[0].created_date,
                                                   status='initiated')
    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
        'APIKEY': 'VjjXt3PtQvRYBkHkWnTP8SVhj0ijg348'
    }
    url_uat = "https://api-uat.creditvidya.com/mw/api/post/data"
    json_body = json.dumps(data)
    response = requests.request('POST', url_uat, data=json_body, headers=headers)

    response_json = response.json()

    status = response_json["status"]
    message = ""
    if "message" in response_json:
        message = response_json["message"]
    cv = CreditVidyaContacts.objects.filter(app_id=app_id).update(status=status, message=message)
    return True


class creadit_vidya_webhook(APIView):
    """

    HTTP METHOD      POST
    URL              <Client to provide>
    HEADER           apikey: To be shared
    Content-Type:    application/json
    ---------------------------------------
    Request body
    {
        "userId": "5d4291ce07467a392612b7f4",
        "userStatus":" PAN_REJECTED"
    }

    :param request:
    :return:
     # Status Code and Info
        HTTP_200_OK
        HTTP_400_BAD_REQUEST
        HTTP_401_UNAUTHORIZED
    """

    def post(self, request):

        data = request.data
        user_id = data['userId']
        user_status = data['userStatus']

        if user_id is not None and user_status is not None:


            data_saved = CreditVidyaApplication.objects.filter(user_id=user_id, status='success').update(
                status=user_status)

            # return 200 if the data is saved else return 400
            if data_saved:
                return Response(status=status.HTTP_200_OK)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)




def cv_api(user_id, mobile_number, loan_id):
    """
    :param user_id:
    :param mobile_number:
    :param loan_id:
    :return:
    """

    register_user = cv_register(user_id, mobile_number, loan_id)

    register_user_data = register_user
    status = register_user_data["status"]

    if status == "failure":
        # Error
        error = register_user_data['errors'][0]
        error_code = error['errorCode']
        error_message = error['errorMessage']
        register_user_data['api'] = 'cv_register_user'
        return register_user_data

    elif status == "success":


        submit_loan = credit_vidya_loan_application_details(loan_id)
        submit_loan_data = submit_loan
        status = submit_loan_data["status"]
        if status == "success":
            # Launch Web view
            submit_loan_data['api'] = 'cv_submit_loan'
            return submit_loan_data

        else:
            # Error
            submit_loan_data = submit_loan
            error = submit_loan_data['errors'][0]
            error_code = error['errorCode']
            error_message = error['errorMessage']
            submit_loan_data['api'] = 'cv_submit_loan'
            return submit_loan_data
    else:
        # Error

        register_user['api'] = 'cv_register_user'
        return register_user


def credit_vidya_loan_application_details(loan_id):
    """
    This API is used to send Application information to CreditVidya.

    :param loan_id:
    :return:
    """
    fullName = ''
    gender = ''
    dob = ''
    pannumber = ''
    personalEmailid = ''
    workEmailid = ''
    employmentType = ''
    income = ''
    CurrentAddressLine1 = ''
    CurrentAddressCity = ''
    CurrentAddressDistrict = ''
    CurrentAddressState = ''
    CurrentAddressPinCode = ''
    itrFiled = ''
    highestEducationalQualification = ''
    monthsInCurrentCompany = ''
    industryType = ''
    mobileNumberLinkedWithAadhaar = ''
    desiredLoanAmount = ''
    purposeOfLoan = ''
    residence_type = ''
    permanentAddressSameAsCurrAddress = ''
    monthsInCurrentCompany = ''


    loan = LoanApplication.objects.filter(loan_application_id=loan_id)
    if not loan and loan is None:
        return Response({'status': 'failure', 'errors': [{'errorCode': 'MK-01', 'errorMessage': 'No Loan found'}]})

    if loan:
        user_id = loan[0].user_id

        purposeOfLoan = loan[0].purpose_loan
        desiredLoanAmount = loan[0].loan_scheme_id
    else:
        return Response({'status': 'failure', 'errors': [{'errorCode': 'MK-01', 'errorMessage': 'No Loan found'}]})

    # get loan id form cv register table
    get_cv_loan_id = CVRegisterUser.objects.filter(user_id=user_id)
    if get_cv_loan_id.exists():

        cv_loan_id = get_cv_loan_id[0].loan_application_id
    else:
        cv_loan_id = None


    # Validating the CV loan id has some value and is not None
    if cv_loan_id and cv_loan_id is not None:
        loan_amount = loan[0].loan_scheme_id
        loan_amount = 10000
        if int(loan_amount) > 5000 and int(loan_amount) <= 150000:
            # Todo uncomment this
            credit_vidya_sms(loan[0].user_id)
            credit_vidya_app(loan[0].user_id)
            credit_vidya_location(loan[0].user_id)
            credit_vidya_contacts(loan[0].user_id)
            credit_vidya_device_data(loan[0].user_id)
            loan_application_id = loan[0].loan_application_id

            name = loan[0].user_personal.name
            if name and name is not None:
                fullName = name
            else:
                return HttpResponse("name is Null")

            gen = loan[0].user_personal.gender
            if gen == 'M':
                gender = 'MALE'
            elif gen == 'F':
                gender = 'FEMALE'
            else:
                gender = 'MALE'
            dob = loan[0].user_personal.birthdate
            # Converting dob to DD/MM/YYY
            dob = dob.strftime("%d/%m/%Y")
            pannumber = loan[0].user_personal.pan_number
            personalEmailid = loan[0].user_personal.email_id
            workEmailid = loan[0].user_professional.company_email_id
            etype = loan[0].user_personal.occupation
            highestEducationalQualification = loan[0].user_personal.educational
            industryType = loan[0].user_personal.industry_type
            itr = loan[0].user_personal.itr
            mobile = loan[0].user_personal.mobile_linked_aadhaar
            residence_type = loan[0].user_personal.residence_type
            from dateutil.relativedelta import relativedelta
            join_date = loan[0].user_professional.join_date
            today = datetime.now()
            date_data = relativedelta(today, join_date)
            monthsInCurrentCompany = date_data.months


            if itr == "Yes":
                itrFiled = True
            else:
                itrFiled = False

            if mobile == "Yes":
                mobileNumberLinkedWithAadhaar = True
            else:
                mobileNumberLinkedWithAadhaar = False

            if etype == 'Salaried':
                employmentType = 'SALARIED'
            else:
                employmentType = etype.upper()

            income = loan[0].user_personal.monthly_income


            CurrentAddressLine1 = loan[0].current_address.address
            CurrentAddressCity = loan[0].current_address.city
            CurrentAddressDistrict = ''
            CurrentAddressState = loan[0].current_address.state
            CurrentAddressPinCode = loan[0].current_address.pin_code
            if not CurrentAddressDistrict:
                CurrentAddressDistrict = CurrentAddressCity

            permanentAddressSameAsCurrAddress = False

            if loan[0].permanent_address:
                if CurrentAddressLine1 == loan[0].permanent_address.address and CurrentAddressCity == loan[0].permanent_address.city and CurrentAddressState == loan[0].permanent_address.state and CurrentAddressPinCode == loan[0].permanent_address.pin_code:
                    permanentAddressSameAsCurrAddress = True


            CreditVidyaApplication.objects.create(loan_app_id=loan[0].id, loan_application_id=cv_loan_id,
                                                  status='initiated', user_id=loan[0].user_id)
        else:
            print("loan amount is < 5k")
    else:
        return Response({'status': 'failure',
                         'errors': [{'errorCode': 'MK-02', 'errorMessage': 'Unable to find Credit Vidya Loan id'}]})

    data = {
        "loanId": cv_loan_id,
        "fullName": fullName,
        "gender": gender,
        "dateOfBirth": dob,
        "panName": fullName,
        "desiredLoanAmount": int(desiredLoanAmount),
        "purposeOfLoan": purposeOfLoan,
        "currentResidenceType": residence_type,
        "mobileNumberLinkedWithAadhaar": mobileNumberLinkedWithAadhaar,
        "highestEducationalQualification": highestEducationalQualification,
        "industryType": industryType,
        "panNumber": pannumber,
        "personalEmailId": personalEmailid,
        "itrFiled": itrFiled,
        "workEmailId": workEmailid,
        "employmentType": employmentType,
        "monthlyIncome": int(income),
        "currentAddressLine1": CurrentAddressLine1,
        "currentAddressCity": CurrentAddressCity,
        "currentAddressDistrict": CurrentAddressDistrict,
        "currentAddressState": CurrentAddressState,
        "currentAddressPincode": CurrentAddressPinCode,
        "permanentAddressSameAsCurrAddress": permanentAddressSameAsCurrAddress,
        "monthsInCurrentCompany": monthsInCurrentCompany
    }
    # dummy data
    # data = {
    #     "loanId": "5dba8b2189fb0b0262a21fa4",
    #     "firstName": "John",
    #     "lastName": "ADA",
    #     "middleName": "",
    #     "gender": "MALE",
    #     "dob": "15/10/1987",
    #     "panNumber": "AADPN0408N",
    #     "personalEmailId": "abc@gmail.com",
    #     "workEmailId": "abc@cbm.com",
    #     "employmentType": "SALARIED",
    #     "income": 120000,
    #     "currentAddressLine1": "Hitec City",
    #     "currentAddressCity": "Hyderabad",
    #     "currentAddressDistrict": "ABC",
    #     "currentAddressState": "Telangana",
    #     "currentAddressPincode": "500081"
    #
    # }

    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
        'APIKEY': 'VjjXt3PtQvRYBkHkWnTP8SVhj0ijg348'
    }
    # Call Credit Vidya api
    url_uat = "https://api-uat.creditvidya.com/marketplace/loans/mw/application-details/v2"
    json_body = json.dumps(data)
    response = requests.request('POST', url_uat, data=json_body, headers=headers)

    response_json = response.json()

    status = response_json["status"]

    if status == "success":
        CreditVidyaApplication.objects.filter(loan_app_id=loan[0].id, loan_application_id=cv_loan_id,
                                              status='initiated', user_id=loan[0].user_id).update(
            status='submitted', reason=response_json)

        # Launch webview
        call_back_url = get_cv_loan_id[0].url
        ctx = {
            'status': status,
            'data': {
                'call_back_url': call_back_url
            }
        }

        return ctx

    else:
        # status == 'failure':
        error = response_json['errors'][0]
        error_code = error['errorCode']
        error_message = error['errorMessage']
        data_updated = CreditVidyaApplication.objects.filter(loan_app_id=loan[0].id, loan_application_id=cv_loan_id,
                                                             status='initiated', user_id=loan[0].user_id).update(
            status='failed', errorcode=error_code, errormessage=error_message)
        if error_code == "CV-330" or error_code == "CV-999" or error_code == "CV-203":
            CVRegisterUser.objects.filter( user_id=loan[0].user_id,status="success").update(status="failed",update_date=datetime.now())

        return response_json


def cv_register(user_id, mobile_number, loan_id):
    """
    Documentation: https://docs.creditvidya.com/
    This API will initiate CreditVidya's loan journey for the user in the App Webview.

    :param user_id:
    :param mobile_number:
    HEADER PARAMETERS
        Content-Type
        required
        string
        Example: application/json
        Type of data you want receive. Mostly json.

        apikey
        required
        string
        Example: <apikey>
        api key thats given to each tenant is passed here. To be shared for UAT/PROD

        REQUEST BODY SCHEMA: application/json
        userId
        required
        string
        Created by Demand Player to uniquely identify a user

        mobileNo
        required
        string
        Mobile number of the user
    :return:
    """
    data = {}


    data["userId"] = loan_id
    data["mobileNo"] = mobile_number
    if user_id is None or mobile_number is None:
        message = 'UserId/mobileNo is mandatory should not be None'
        return Response({
            "status": "failure",
            "errors": [
                {
                    "errorCode": "MK-05",
                    "errorMessage": message
                }
            ]
        })

    user_obj, created = CVRegisterUser.objects.get_or_create(
        mobile_number=mobile_number, user_id=user_id)

    # User is already register
    if user_obj.status == 'success':
        return {
            "status": user_obj.status,
            "data": {
                "url": user_obj.url,
                "authToken": user_obj.auth_token,
                "refreshToken": user_obj.refresh_token
            },
        }

    uat_url = "https://phoenix-uat.creditvidya.com/marketplace/mw/process/register-start/pl"
    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
        'APIKEY': 'VjjXt3PtQvRYBkHkWnTP8SVhj0ijg348'
    }
    json_body = json.dumps(data)
    response = requests.request('POST', uat_url, data=json_body, headers=headers)

    response_json = response.json()
    status = response_json["status"]

    # get error message
    if status == 'failure':
        error = response_json['errors'][0]
        error_code = error['errorCode']
        error_message = error['errorMessage']

        data_saved = CVRegisterUser.objects.filter(mobile_number=mobile_number, user_id=user_id).update(
            message=error_message, status=status
        )


        return response_json
    else:

        data = response_json["data"]
        url = data['url']
        auth_token = data['authToken']
        refresh_token = data['refreshToken']
        loan_id = data['loanId']
        data_saved = CVRegisterUser.objects.filter(mobile_number=mobile_number, user_id=user_id).update(
            status=status, auth_token=auth_token, refresh_token=refresh_token,
            loan_application_id=loan_id, url=url,
        )

    return response_json