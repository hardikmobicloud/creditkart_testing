import datetime
import re
from datetime import date, timedelta
from functools import partial
from User.models import *
from rest_framework.authentication import *
from rest_framework.permissions import *
from django.apps import apps
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from . import botofile
from django_pandas.io import read_frame
import pandas as pd




ShareAppMaster = apps.get_model('UserData', 'ShareAppMaster')
UserSharedData = apps.get_model('UserData', 'UserSharedData')



class shareddata_Count(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    def get(self, request, *args, **kwargs):


        '''

        :param request:
        {
            "mobile_number":"7875583671",

        }

        '''
        exist = 0
        sub_count = 0
        approved_count = 0
        not_registered = 0
        recovery = 0
        incentive = 0
        income = 0
        total_subscrption=0
        total_approved=0
        total_sub_paid=0
        total_app_paid=0

        try:
            user_id = request.user.id

            all_shared = ShareAppMaster.objects.filter(user_id=user_id).count()
            all_data = ShareAppMaster.objects.filter(user_id=user_id)
            all_sub = UserSharedData.objects.filter(is_subscribed=True)
            all_approved = UserSharedData.objects.filter(is_approved=True)

            for subs in all_sub:
                if (subs.shareapp.user.id == user_id):
                    sub_count += 1
                    total_subscrption+=subs.subscription_amount
                    if subs.is_paid_forsubscription==True:
                        total_sub_paid += subs.subscription_amount


            for approved in all_approved:
                if (approved.shareapp.user.id == user_id):
                    approved_count += 1
                    total_approved+=approved.Approved_amount
                    if subs.is_paid_forapproval==True:
                        total_app_paid += subs.Approved_amount

            for number in all_data:
                usercheck = User.objects.filter(username=number.number)
                if usercheck:
                    existcheck = ShareAppMaster.objects.filter(number=usercheck[0].username)
                    if existcheck:
                        if usercheck[0].date_joined < existcheck[0].created_date:
                            exist += 1
                else:
                    not_registered += 1

            income = total_sub_paid + total_app_paid
            incentive = (total_subscrption-total_sub_paid) + (total_approved-total_app_paid)



        except:
            pass
        path = "canvas/canvasjs.min.js"
        url = botofile.get_url(path)
        return Response(
            {
                'not_registered': not_registered,
                'register': sub_count,
                'approved_count': approved_count,
                'exist':exist,
                'shared':all_shared,
                'recovery':recovery,
                'incentive':incentive,
                'income':income,
                'url':url,

            }
        )





class SharedData(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    def get(self, request, *args, **kwargs):




        li = []

        all_data = ShareAppMaster.objects.filter(user_id=request.user.id)
        for number in all_data:
            data = {}
            data['number'] = number.number
            data['created_date'] = number.created_date
            data['name'] = number.name
            li.append(data)
        return Response(
            {
                'shared_data': li
            }
        )


class conversion(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    def get(self, request, *args, **kwargs):

        li = []


        all_sub = UserSharedData.objects.filter(is_subscribed=True, shareapp__user=request.user.id)
        for sub in all_sub:
            data = {}
            data['Register_date'] = sub.created_date
            data['mobile_number'] = sub.shareapp.number
            data['created_date'] = sub.shareapp.created_date

            data['name'] = sub.shareapp.name
            li.append(data)

        return Response(
            {
                'Register_data': li
            }
        )

class sub_insentive(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    def get(self, request, *args, **kwargs):
        data=[]
        if request.user.id and request.user.id is not None:
            sub_insentive = UserSharedData.objects.filter(is_subscribed=True, is_paid_forsubscription=False,
                                                          shareapp__user=request.user.id)

            dataframe = read_frame(sub_insentive)

            dataframe['date'] = pd.to_datetime(dataframe['created_date']).apply(
                lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
            day_wise_subscription_amount = \
                dataframe.groupby(dataframe['date'])['subscription_amount'].sum()
            day_wise_subscription_amount_dict = dict(day_wise_subscription_amount)

            for key,value in day_wise_subscription_amount_dict.items():
                dat={}
                dat["date"]=key,
                dat["insentive"]=value
                dat["count"]=value/12.5
                data.append(dat)


            return Response(
                {
                    "data":data
                }
            )


class approve_insentive(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    def get(self, request, *args, **kwargs):

        approve = UserSharedData.objects.filter(is_approved=True, is_paid_forapproval=False,
                                                shareapp__user=request.user.id)
        dataframe = read_frame(approve)
        dataframe['date'] = pd.to_datetime(dataframe['created_date']).apply(
            lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
        day_wise_approve_amount = \
            dataframe.groupby(dataframe['date'])['Approved_amount'].sum()
        day_wise_approved_amount_dict = dict(day_wise_approve_amount)
        return Response(
            {
                "data": day_wise_approved_amount_dict
            }
        )


class Not_registered(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    def get(self, request, *args, **kwargs):
        li=[]
        all_shared = ShareAppMaster.objects.filter(user_id=request.user.id)
        for number in all_shared:
            data = {}
            numbercheck = User.objects.filter(username=number.number)
            if numbercheck:
                pass
            else:
                data['name'] = number.name
                data['number'] = number.number
                li.append(data)

        return Response(
            {
                'Not_register': li
            }
        )












