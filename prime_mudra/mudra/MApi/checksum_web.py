import base64
import string
import random
import hashlib
from django.apps.registry import apps
from Crypto.Cipher import AES
from django.db.models import Sum
from rest_framework.views import APIView
from rest_framework.response import Response

from rest_framework.permissions import *
from rest_framework.authentication import *
from django.conf import settings

UserWallet = apps.get_model('ecom', 'UserWallet')
Payment = apps.get_model('Payments', 'payment')

Subscription = apps.get_model('User', 'Subscription')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
IV = "@@@@&&&&####$$$$"
BLOCK_SIZE = 16


def generate_checksum(param_dict, merchant_key, salt=None):
    params_string = __get_param_string__(param_dict)
    salt = salt if salt else __id_generator__(4)
    final_string = '%s|%s' % (params_string, salt)

    hasher = hashlib.sha256(final_string.encode())
    hash_string = hasher.hexdigest()

    hash_string += salt

    return __encode__(hash_string, IV, merchant_key)


def generate_refund_checksum(param_dict, merchant_key, salt=None):
    for i in param_dict:
        if ("|" in param_dict[i]):
            param_dict = {}
            exit()
    params_string = __get_param_string__(param_dict)
    salt = salt if salt else __id_generator__(4)
    final_string = '%s|%s' % (params_string, salt)

    hasher = hashlib.sha256(final_string.encode())
    hash_string = hasher.hexdigest()

    hash_string += salt

    return __encode__(hash_string, IV, merchant_key)


def generate_checksum_by_str(param_str, merchant_key, salt=None):
    params_string = param_str
    salt = salt if salt else __id_generator__(4)
    final_string = '%s|%s' % (params_string, salt)

    hasher = hashlib.sha256(final_string.encode())
    hash_string = hasher.hexdigest()

    hash_string += salt

    return __encode__(hash_string, IV, merchant_key)


def verify_checksum(param_dict, merchant_key, checksum):
    # Remove checksum
    if 'CHECKSUMHASH' in param_dict:
        param_dict.pop('CHECKSUMHASH')

    # Get salt
    paytm_hash = __decode__(checksum, IV, merchant_key)
    salt = paytm_hash[-4:]
    calculated_checksum = generate_checksum(param_dict, merchant_key, salt=salt)
    return calculated_checksum == checksum


def verify_checksum_by_str(param_str, merchant_key, checksum):
    # Remove checksum
    # if 'CHECKSUMHASH' in param_dict:
    # param_dict.pop('CHECKSUMHASH')

    # Get salt
    paytm_hash = __decode__(checksum, IV, merchant_key)
    salt = paytm_hash[-4:]
    calculated_checksum = generate_checksum_by_str(param_str, merchant_key, salt=salt)
    return calculated_checksum == checksum


def __id_generator__(size=6, chars=string.ascii_uppercase + string.digits + string.ascii_lowercase):
    return ''.join(random.choice(chars) for _ in range(size))


def __get_param_string__(params):
    params_string = []
    for key in sorted(params.keys()):
        if ("REFUND" in params[key] or "|" in params[key]):
            respons_dict = {}
            exit()
        value = params[key]
        params_string.append('' if value == 'null' else str(value))
    return '|'.join(params_string)


__pad__ = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * chr(BLOCK_SIZE - len(s) % BLOCK_SIZE)
__unpad__ = lambda s: s[0:-ord(s[-1])]





def __encode__(to_encode, iv, key):
    to_encode = __pad__(to_encode)
    # Encrypt
    c = AES.new(key.encode('UTF-8'), AES.MODE_CBC, iv.encode('UTF-8'))
    to_encode = c.encrypt(to_encode.encode('UTF-8'))
    # Encode
    to_encode = base64.b64encode(to_encode)
    return to_encode.decode("UTF-8")


def __decode__(to_decode, iv, key):
    # Decode
    to_decode = base64.b64decode(to_decode)
    # Decrypt
    c = AES.new(key, AES.MODE_CBC, iv)
    to_decode = c.decrypt(to_decode)
    if type(to_decode) == bytes:
        # convert bytes array to str.
        to_decode = to_decode.decode()
    # remove pad
    return __unpad__(to_decode)


class ChecksumGenerate_Subcription(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None

        try:
            merchant_key = settings.PROD_SUB_MERCHANT_KEY
            MID = settings.PROD_SUB_MERCHANT_MID
            INDUSTRY_TYPE_ID = 'BFSI'
            CHANNEL_ID = 'WAP'
            WEBSITE = 'APPPROD'
            Callbalck_url = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="
            random_number = random.sample(range(99999), 1)
            mobile_number = str(request.data.get('mobile_number'))
            ORDER_ID = str(mobile_number) + 'ms' + str(random_number[0])
            CUST_ID = mobile_number
            TXN_AMOUNT = str(request.data.get('TXN_AMOUNT'))
            CALLBACK_URL = str(Callbalck_url) + '' + str(ORDER_ID)
            if CUST_ID == None or CUST_ID == '':
                status = 'CUST_ID01'
                message = 'CUST_ID None'
            elif TXN_AMOUNT == None or TXN_AMOUNT == '':
                status = 'TXN_AMOUNT01'
                message = 'TXN_AMOUNT None'
            else:

                dict_param = {
                    'MID': MID,
                    'ORDER_ID': ORDER_ID,
                    'CUST_ID': CUST_ID,
                    'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                    'CHANNEL_ID': CHANNEL_ID,
                    'TXN_AMOUNT': TXN_AMOUNT,
                    'WEBSITE': WEBSITE,
                    'CALLBACK_URL': CALLBACK_URL,
                }
                return_checksum = generate_checksum(dict_param, merchant_key, salt=None)
                if return_checksum:
                    Payment.objects.create(status='initiated',
                                           order_id=str(ORDER_ID), amount=float(TXN_AMOUNT), category='Subscription')
                    payment = Payment.objects.filter(order_id=ORDER_ID).last()
                    Subscription.objects.create(user_id=request.user.id, payment_id_id=payment.id)

                message = "Successfuly Done"
                status = 1

            return Response(
                {
                    'status': status,
                    'message': message,
                    'checksum': return_checksum,
                    'mid': MID,
                    'orderId': ORDER_ID,
                    'industryType': INDUSTRY_TYPE_ID,
                    'website': WEBSITE,
                    'channel': CHANNEL_ID,
                    'callback': CALLBACK_URL,
                    'amount': TXN_AMOUNT,
                    'cust_id': CUST_ID

                }
            )
        except:
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


class ChecksumGeneratePaytm_Producation(APIView):
    """
        Paytm Repayment Checksum API
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None

        try:

            merchant_key = settings.PROD_REP_MERCHANT_KEY
            MID = settings.PROD_REP_MERCHANT_MID
            INDUSTRY_TYPE_ID = 'BFSI'
            CHANNEL_ID = 'WAP'
            WEBSITE = 'APPPROD'
            Callbalck_url = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="
            random_number = random.sample(range(99999), 1)
            loan_application_id = str(request.data.get('loan_application_id'))
            CUST_ID = str(request.data.get('CUST_ID'))
            TXN_AMOUNT = str(request.data.get('TXN_AMOUNT'))

            # Get approved loan of customer if loan application id is blank or None
            if loan_application_id is None or not loan_application_id:
                if CUST_ID is None or CUST_ID == '':
                    CUST_ID = request.user.username

                get_approved_loan = LoanApplication.objects.filter(user__username=CUST_ID, status="APPROVED").order_by(
                    '-id').first()
                if get_approved_loan:
                    loan_application_id = get_approved_loan.loan_application_id
                else:
                    print("=========checksum web -- no loan application", request.data)
                    # logger.error("No Loan Application Id", request.data)
                    return Response(
                        {
                            'status': 0,
                            'message': "No Active Loan"
                        }
                    )

            if TXN_AMOUNT is None or TXN_AMOUNT == '':
                status = 'TXN_AMOUNT01'
                message = 'TXN_AMOUNT None'

            if TXN_AMOUNT is None or TXN_AMOUNT == '' or int(TXN_AMOUNT) == 0:

                TXN_AMOUNT = 100

            ORDER_ID = str(loan_application_id) + 'm' + str(random_number[0])
            CALLBACK_URL = str(Callbalck_url) + '' + str(ORDER_ID)

            dict_param = {
                'MID': MID,
                'ORDER_ID': ORDER_ID,
                'CUST_ID': CUST_ID,
                'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                'CHANNEL_ID': CHANNEL_ID,
                'TXN_AMOUNT': TXN_AMOUNT,
                'WEBSITE': WEBSITE,
                'CALLBACK_URL': CALLBACK_URL,

            }
            return_checksum = generate_checksum(dict_param, merchant_key, salt=None)

            if return_checksum:
                payment = Payment.objects.create(status='initiated',
                                                 order_id=str(ORDER_ID), amount=float(TXN_AMOUNT), category='Loan')
                loan_application = LoanApplication.objects.filter(loan_application_id=loan_application_id)
                loan_application[0].repayment_amount.add(payment)

                message = "Successfuly Done"
                status = 1
            else:
                message = "Something Went Wrong"
                status = 0

            return Response(
                {
                    'status': status,
                    'message': message,
                    'checksum': return_checksum,
                    'mid': MID,
                    'orderId': ORDER_ID,
                    'industryType': INDUSTRY_TYPE_ID,
                    'website': WEBSITE,
                    'channel': CHANNEL_ID,
                    'callback': CALLBACK_URL,
                    'amount': TXN_AMOUNT,
                    'cust_id': CUST_ID
                }
            )
        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


def check_loan_repayment_total(loan_application_id):
    loan_amount = LoanApplication.objects.filter(loan_application_id=loan_application_id, status="APPROVED").aggregate(
        Sum('repayment_amount__amount'))['repayment_amount__amount__sum']
    loan = LoanApplication.objects.filter(loan_application_id=loan_application_id, status="APPROVED")
    if loan:

        if loan_amount is not None:

            if float(loan[0].approved_amount) <= float(loan_amount):

                LoanApplication.objects.filter(id=loan[0].id).update(status="COMPLETED")
            else:
                pass
        else:
            pass
    else:
        pass


class ChecksumGeneratePaytm_LoanRepayment(APIView):
    """
        Paytm Loan Repayment Checksum API
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None
        user_id = request.user.id
        request_data = request.data
        payment_id = None
        if request_data:
            used_amount = 0
            amount_add = 0
            if "amount" in request_data and "loan_application_id" in request_data and "wallet" in request_data:
                amount = request_data["amount"]
                loan_application_id = request_data["loan_application_id"]
                wallet = int(request_data["wallet"])
                loan_details_obj = LoanApplication.objects.filter(user_id=user_id,
                                                                  loan_application_id=loan_application_id)
                if wallet == 1:
                    user_wallet = UserWallet.objects.filter(user_id=user_id).exclude(status="initiated").last()
                    if user_wallet:
                        balance_amount = user_wallet.balance_amount

                        if balance_amount > float(amount):
                            import datetime
                            date = datetime.datetime.now()
                            new_balance_amount = balance_amount - float(amount)
                            UserWallet.objects.create(amount_add=amount_add, used_amount=amount,
                                                      balance_amount=new_balance_amount, status="loan_repayment",
                                                      user_id=user_id)
                            import random
                            random_number = random.sample(range(99999), 1)
                            ORDER_ID = str(loan_application_id) + 'l' + str(random_number[0])
                            payment_obj = Payment.objects.create(order_id=ORDER_ID,
                                                                 status="success",
                                                                 category="Loan_Repayment",
                                                                 type="loan",
                                                                 date=date,
                                                                 amount=float(amount),
                                                                 product_type="Ecom")
                            # check_loan_repayment_total(loan_application_id)

                            payment_id = payment_obj.id

                            if loan_details_obj:
                                payment_data_obj = Payment.objects.filter(id=payment_id)
                                if payment_data_obj:
                                    loan_details_obj[0].repayment_amount.add(*payment_data_obj)

                            check_loan_repayment_total(loan_application_id)

                            status = 2
                            message = "Transcation Succuces"
                            return Response(
                                {
                                    'status': status,
                                    'message': message
                                }
                            )


                        else:
                            used_amount = float(amount) - balance_amount
                            new_balance_amount = 0
                            if balance_amount > 0:
                                import datetime
                                date = datetime.datetime.now()

                                UserWallet.objects.create(amount_add=amount_add, used_amount=balance_amount,
                                                          balance_amount=new_balance_amount, status="loan_repayment",
                                                          user_id=user_id)
                                import random
                                random_number = random.sample(range(99999), 1)
                                ORDER_ID = str(loan_application_id) + 'l' + str(random_number[0])
                                payment_obj = Payment.objects.create(order_id=ORDER_ID,
                                                                     status="success",
                                                                     category="Loan_Repayment",
                                                                     type="loan",
                                                                     date=date,
                                                                     amount=balance_amount,
                                                                     product_type="Ecom")

                                payment_id = payment_obj.id

                            if loan_details_obj:
                                payment_data_obj = Payment.objects.filter(id=payment_id)
                                if payment_data_obj:
                                    loan_details_obj[0].repayment_amount.add(*payment_data_obj)

                if used_amount > 0 or wallet == 0:
                    if wallet == 0:
                        used_amount = amount

                    try:
                        merchant_key = settings.PROD_REP_MERCHANT_KEY
                        MID = settings.PROD_REP_MERCHANT_MID
                        INDUSTRY_TYPE_ID = 'BFSI'
                        CHANNEL_ID = 'WAP'
                        WEBSITE = 'APPPROD'
                        Callbalck_url = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="
                        import random
                        random_number = random.sample(range(99999), 1)
                        loan_application_id = loan_application_id
                        CUST_ID = loan_details_obj[0].user.username
                        TXN_AMOUNT = used_amount
                        NEW_TXN_AMOUNT = str(TXN_AMOUNT)

                        # Get approved loan of customer if loan application id is blank or None
                        if loan_application_id is None or not loan_application_id:
                            if CUST_ID is None or CUST_ID == '':
                                CUST_ID = request.user.username
                            get_approved_loan = LoanApplication.objects.filter(user__username=CUST_ID,
                                                                               status="APPROVED",
                                                                               user_status='Accept').order_by(
                                '-id').first()
                            if get_approved_loan:
                                loan_application_id = get_approved_loan.loan_application_id
                            else:

                                # logger.error("No Loan Application Id", request.data)
                                return Response(
                                    {
                                        'status': 0,
                                        'message': "No Active Loan"
                                    }
                                )
                        if TXN_AMOUNT is None or TXN_AMOUNT == '':
                            status = 'TXN_AMOUNT01'
                            message = 'TXN_AMOUNT None'

                        ORDER_ID = str(loan_application_id) + 'l' + str(random_number[0])
                        CALLBACK_URL = str(Callbalck_url) + '' + str(ORDER_ID)
                        dict_param = {
                            'MID': MID,
                            'ORDER_ID': ORDER_ID,
                            'CUST_ID': CUST_ID,
                            'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                            'CHANNEL_ID': CHANNEL_ID,
                            'TXN_AMOUNT': NEW_TXN_AMOUNT,
                            'WEBSITE': WEBSITE,
                            'CALLBACK_URL': CALLBACK_URL,
                        }
                        return_checksum = generate_checksum(dict_param, merchant_key, salt=None)
                        if return_checksum:
                            payment = Payment.objects.create(status='initiated',
                                                             order_id=str(ORDER_ID), amount=float(TXN_AMOUNT),
                                                             category='Loan_Repayment')
                            loan_application = LoanApplication.objects.filter(loan_application_id=loan_application_id)
                            loan_application[0].repayment_amount.add(payment)
                            message = "Successfuly Done"
                            status = 1
                        else:
                            message = "Something Went Wrong"
                            status = 0
                        return Response(
                            {
                                'status': status,
                                'message': message,
                                'checksum': return_checksum,
                                'mid': MID,
                                'orderId': ORDER_ID,
                                'industryType': INDUSTRY_TYPE_ID,
                                'website': WEBSITE,
                                'channel': CHANNEL_ID,
                                'callback': CALLBACK_URL,
                                'amount': NEW_TXN_AMOUNT,
                                'cust_id': CUST_ID
                            }
                        )
                    except Exception as e:
                        print("-----Exception----in checksum web", e)
                        import sys
                        import os
                        print(e.args)
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                        print(exc_type, fname, exc_tb.tb_lineno)
                        return Response(
                            {
                                'status': status,
                                'message': message
                            }
                        )
                else:
                    pass


            else:
                message = "Enter required field"
                status = 3
                return Response(
                    {
                        'status': status,
                        'message': message
                    }
                )

        else:
            message = "require request data"
            status = 4
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )