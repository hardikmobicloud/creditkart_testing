import datetime
import logging
import sys
from datetime import timedelta
import re
import requests
from django.apps import apps
from django.conf import settings
from django.contrib.auth.models import User
from django.http import HttpResponse
from mudra.constants import *
from rest_framework import generics, status
from rest_framework.authentication import *
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from sqlalchemy.engine import create_engine

from django.db.models import Sum, Avg

from . import botofile
from . import user_serializers
from . import utils
from mudra import constants
from Payments.tasks import rbl_worker

logger = logging.getLogger(__name__)

PersonalDetails = apps.get_model('User', 'Personal')
BankDetails = apps.get_model('User', 'Bank')
AddressDetails = apps.get_model('User', 'Address')
ReferenceDetails = apps.get_model('User', 'Reference')
DocumentDetails = apps.get_model('User', 'Documents')
ProfessionalDetails = apps.get_model('User', 'Professional')
AppVersionCheck = apps.get_model('StaticData', 'AppVersionCheck')
PincodeMaster = apps.get_model('StaticData', 'PincodeMaster')
PanCardMaster = apps.get_model('UserData', 'PanCardMaster')
Location = apps.get_model('User', 'Location')
DeviceData = apps.get_model('UserData', 'DeviceData')
Payment = apps.get_model('Payments', 'Payment')
RBLTransaction = apps.get_model('Payments', 'RBLTransaction')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
PanRequest = apps.get_model('UserData', 'PanRequest')
UserWallet = apps.get_model('ecom', 'UserWallet')
OrderDetails = apps.get_model('ecom', 'OrderDetails')
Credit = apps.get_model('ecom', 'Credit')


class Personal(generics.ListAPIView):
    """
    User Personal details API

    :parameter:
        Method: GET
        Response

        {
            'name':"CharField",
            'birthdate':"Datefield"
            'gender ': "CharField",
            'marital_status':"CharField",
            'occupation ':"CharField",
            'mobile_no ':"CharField",
            'alt_mobile_no': "CharField",
            'monthly_income':"CharField",
            'pan_number':"CharField",
        }


        :Note

        MARITAL_STATUS = (
            ('MARRIED', 'Married'),
            ('UNMARRIED', 'Not Married'),
            ('DIVORCED', 'Divorced'),
            ('WIDOW', 'Widow'),
        )

        OCCUPATION = (
            ('SALARIED', 'Salaried'),
            ('SELF_EMPLOYED', 'Self Employed'),
            ('STUDENT', 'Student'),
            ('HOUSE_WIFE', 'Housewife'),
        )

        GENDER = (
            ('Female', 'F'),
            ('Male', 'M')
        )

        Method: POST
        :param
            {
                "name": "test",
                "birthdate": "2000-12-12",
                "gender": "Male",
                "marital_status": "MARRIED",
                "occupation": "SALARIED",
                "mobile_no": "1234567890",
                "alt_mobile_no": "",
                "monthly_income": "1233",
                "pan_number": "asdf"
            }
        :return

            {
                "id": 12,
                "name": "test",
                "birthdate": "2000-12-12",
                "gender": "Male",
                "marital_status": "MARRIED",
                "occupation": "SALARIED",
                "mobile_no": "1234567890",
                "alt_mobile_no": "",
                "monthly_income": 1233,
                "pan_number": "asdf",
                "created_date": "2019-09-13T11:29:23.507956",
                "updated_date": "2019-09-13T11:29:23.507973",
                "invitations": null,
                "user": 2
            }

        status 9 : No Details Found
        status 10 : Something went wrong

        # Status Code and Info
            HTTP_200_OK
            HTTP_201_CREATED
            HTTP_400_BAD_REQUEST
            HTTP_401_UNAUTHORIZED
            HTTP_403_FORBIDDEN

        CATEGORY	            DESCRIPTION
        1xx: Informational	Communicates transfer protocol-level information.
        2xx: Success	    Indicates that the client’s request was accepted successfully.
        3xx: Redirection	Indicates that the client must take some additional action in order to complete their request.
        4xx: Client Error	This category of error status codes points the finger at clients.
        5xx: Server Error	The server takes responsibility for these error status codes.

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = user_serializers.Personal

    def get(self, request, *args, **kwargs):

        try:
            user_profile_instance = PersonalDetails.objects.filter(user=request.user.id).order_by('-id').first()

            if user_profile_instance:
                return Response(
                    {
                        'name': user_profile_instance.name,
                        'fathers_name': user_profile_instance.fathers_name,
                        'birthdate': user_profile_instance.birthdate,
                        'gender ': user_profile_instance.gender,
                        'marital_status': user_profile_instance.marital_status,
                        'occupation ': user_profile_instance.occupation,
                        'mobile_no ': user_profile_instance.mobile_no,
                        'alt_mobile_no': user_profile_instance.alt_mobile_no,
                        'email_id': user_profile_instance.email_id,
                        'monthly_income': str(user_profile_instance.monthly_income),
                        'pan_number': user_profile_instance.pan_number,
                        'aadhar_number': user_profile_instance.aadhar_number,
                        "itr": user_profile_instance.itr,
                        "mobile_linked_aadhaar": user_profile_instance.mobile_linked_aadhaar,
                        "residence_type": user_profile_instance.residence_type,
                        "industry_type": user_profile_instance.industry_type,
                        "educational": user_profile_instance.educational

                    }
                )
            else:
                return Response(
                    {
                        'status': '9',
                        'message': 'No Details Found',

                    }
                )

        except Exception as e:
            print("-------User Personal Details API-------", e.args)
            return Response(
                {
                    'status': '10',
                    'message': 'Something went wrong!'
                }
            )

    def post(self, request):
        try:
            request_data = request.data
            request_data['user'] = request.user.id
            if request_data['mobile_no'] == None:
                request_data['mobile_no'] = request.user.username
            serializer_instance = user_serializers.Personal(data=request_data)
            if serializer_instance.is_valid():
                serializer_instance.save()
                return Response(serializer_instance.data, status=status.HTTP_201_CREATED)
            return Response(serializer_instance.errors, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            print("Exception post Personal", e.args)
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)


class Bank(generics.ListAPIView):
    """
    User Bank Details API

    :parameter:
        Method: GET
        Response

        {
            'bank_name':CharField
            'account_number':CharField
            'ifsc_code':CharField
            'user':CharField
        }

        Method: POST
        :param

        {
            "bank_name": "test",
            "account_number": "test",
            "ifsc_code": "test"
        }

        Response
        :return
        {
            "id": 1,
            "bank_name": "test",
            "account_number": "test",
            "ifsc_code": "test",
            "created_date": "2019-09-05T17:45:08.647049",
            "updated_date": "2019-09-05T17:45:08.647078",
            "user": 1
        }

        :Note
        Token required
        Authorization : Token 495cb833ef3608930ad8d36dabf7b28675afcd62

        status 9 : No Details Found
        status 10 : Something went wrong

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = user_serializers.Bank

    def get(self, request, *args, **kwargs):

        try:
            user_profile_instance = BankDetails.objects.filter(user=request.user.id).order_by('-id').first()

            if user_profile_instance:
                return Response(
                    {
                        'bank_name': user_profile_instance.bank_name,
                        'account_number': user_profile_instance.account_number,
                        'ifsc_code': user_profile_instance.ifsc_code,
                    }
                )
            else:
                return Response(
                    {
                        'status': '9',
                        'message': 'No Bank Details Found',

                    }
                )

        except Exception as e:
            print("-------User Bank Details API-------", e.args)
            return Response(
                {
                    'status': '10',
                    'message': 'Something went wrong!'
                }
            )

    def post(self, request):
        request_data = request.data
        request_data['user_id'] = request.user.id
        bank_serializer_instance = user_serializers.Bank1(data=request_data)

        if bank_serializer_instance.is_valid():
            bank_serializer_instance.save()
            return Response(bank_serializer_instance.data, status=status.HTTP_201_CREATED)
        return Response(bank_serializer_instance.errors, status=status.HTTP_400_BAD_REQUEST)


class Address(generics.ListAPIView):
    """
    User Address Details API

    :parameter:
        Method: GET
        Response

        {
            'address':
            'city':
            'state':
            'pin_code':
            'type':
            'user':

        }

        Method: POST
        :param
        {
            "address": "test",
            "city": "test",
            "state": "test",
            "pin_code": "test",
            "type": "Permanent"
        }

        :return
        {
            "id": 9,
            "address": "test",
            "city": "test",
            "state": "test",
            "pin_code": "test",
            "type": "Permanent",
            "created_date": "2019-09-13T11:48:12.077717",
            "updated_date": "2019-09-13T11:48:12.077748",
            "user": 2
        }

        :Note

        ADDRESS_TYPE = (
            ('Permanent', 'P'),
            ('Current', 'C')
        )

        status 9 : No Details Found
        status 10 : Something went wrong


    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = user_serializers.Address

    def post(self, request):

        request_data = request.data
        request_data['user'] = request.user.id
        serializer_instance = self.serializer_class(data=request_data)

        if serializer_instance.is_valid():
            serializer_instance.save()
            return Response(serializer_instance.data, status=status.HTTP_201_CREATED)
        return Response(serializer_instance.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):

        try:

            user_profile_instance = AddressDetails.objects.filter(user=request.user.id).order_by('-id').first()

            if user_profile_instance:
                return Response(
                    {
                        'address': user_profile_instance.address,
                        'city': user_profile_instance.city,
                        'state': user_profile_instance.state,
                        'pin_code': user_profile_instance.pin_code,
                        'type': user_profile_instance.type,

                    }
                )
            else:
                return Response(
                    {
                        'status': '9',
                        'message': 'No Details Found',

                    }
                )

        except Exception as e:
            print("-------User Address Details API-------", e.args)
            return Response(
                {
                    'status': '10',
                    'message': 'Something went wrong!'
                }
            )


class AllAddress(generics.ListAPIView):
    """
    User Permanent + Current  Address Details API

    :parameter:
        Method: GET
        Response

        {
            'permanent_address':
            'permanent_city':
            'permanent_state':
            'permanent_pin_code':
            'current_address':
            'current_city':
            'current_state':
            'current_pin_code':

        }

        Method: POST

        :param
           {
                "permanent_address": "test",
                "permanent_city": "test",
                "permanent_state": "test",
                "permanent_pin_code": "test",
                "current_address": "tes23t",
                "current_city": "tes23t",
                "current_state": "test1",
                "current_pin_code": "test"
            }

        :return

        :Note


        status 9 : No Details Found
        status 10 : Something went wrong

        status 1 : POST successful
        status 2 : Incorrect Current Address
        status 3 : Incorrect Permanent Address
        status 4 : No Details Found
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = user_serializers.Address

    # TODO get method for All Adderess
    def post(self, request):
        flag_current = False
        flag_permanent = False
        status = '4'
        message = 'Something went wrong'

        request_data = request.data
        request_data['user'] = request.user.id
        data = {}
        permanent_data = {}
        current_data = {}
        permanent_data['user'] = request_data['user']
        current_data['user'] = request_data['user']

        error = None
        try:
            # Validating Permanent address
            permanent_data['address'] = request.data['permanent_address']
            permanent_data['city'] = request.data['permanent_city']
            permanent_data['state'] = request.data['permanent_state']
            permanent_data['pin_code'] = request.data['permanent_pin_code']
            if "personal" in request.data:
                permanent_data['personal'] = request.data['personal']
            permanent_data['type'] = 'Permanent'

            permanent_serializer_instance = self.serializer_class(data=permanent_data)

            if permanent_serializer_instance.is_valid():
                flag_permanent = True

            else:
                error = permanent_serializer_instance.errors

            # Validating Current address
            current_data['address'] = request.data['current_address']
            current_data['city'] = request.data['current_city']
            current_data['state'] = request.data['current_state']
            current_data['pin_code'] = request.data['current_pin_code']
            if "personal" in request.data:
                current_data['personal'] = request.data['personal']
            current_data['type'] = 'Current'

            current_serializer_instance = self.serializer_class(data=current_data)

            if current_serializer_instance.is_valid():
                flag_current = True

            else:
                error = current_serializer_instance.errors

            # Validating if both address

            if flag_permanent == False or flag_current == False:
                if flag_permanent == False:
                    status = '2',
                    message = 'Incorrect Permanent Address',

                if flag_current == False:
                    status = '3',
                    message = 'Incorrect Current Address',

                return Response({"data": data, "status": status, "message": message,
                                 "error": error})

            else:
                # save the data because both address are valid

                p_save = permanent_serializer_instance.save()
                c_save = current_serializer_instance.save()

                data['permanent_address'] = request.data['permanent_address']
                data['permanent_city'] = request.data['permanent_city']
                data['permanent_state'] = request.data['permanent_state']
                data['permanent_pin_code'] = request.data['permanent_pin_code']
                if "personal" in request.data:
                    data['personal'] = request.data['personal']

                data['current_address'] = request.data['current_address']
                data['current_city'] = request.data['current_city']
                data['current_state'] = request.data['current_state']
                data['current_pin_code'] = request.data['current_pin_code']
                if "personal" in request.data:
                    data['personal'] = request.data['personal']

                status = '1',
                message = 'Data saved successfully'

                return Response({"data": data, "status": status, "message": message})
        except Exception as e:
            print("exc curr", e)

    def get(self, request, *args, **kwargs):
        """
        ('Permanent', 'P'),
            ('Current', 'C')
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        data = {}
        try:
            user_id = request.user.id
            type = "Permanent"
            getdata = get_address_details(user_id, type)
            data['permanent_address'] = getdata['address']
            data['permanent_city'] = getdata['city']
            data['permanent_state'] = getdata['state']
            data['permanent_pin_code'] = getdata['pin_code']

            type = "Current"
            getdata = get_address_details(user_id, type)
            data['current_address'] = getdata['address']
            data['current_city'] = getdata['city']
            data['current_state'] = getdata['state']
            data['current_pin_code'] = getdata['pin_code']

            status = '1',
            message = 'Details Found!'
            return Response({"data": data, "status": status, "message": message})

        except Exception as e:
            print("-------User Address Details Get API-------", e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': '10',
                    'message': 'Something went wrong!'
                }
            )


def get_address_details(user_id, type):
    '''

    :param user_id:
    :param type: Current  /  Permanent
    :return:
    '''
    user_profile_instance = AddressDetails.objects.filter(user=user_id, type=type).order_by('-id').first()
    ctx = {}
    if user_profile_instance:
        ctx = {
            'address': user_profile_instance.address,
            'city': user_profile_instance.city,
            'state': user_profile_instance.state,
            'pin_code': user_profile_instance.pin_code,
            'type': user_profile_instance.type,

        }
    # return json.dumps(ctx)
    return ctx


class Reference(generics.ListAPIView):
    """
    User Reference Details API

    :parameter:
        Method: GET
        Response

        {
            'colleague_name':
            'colleague_mobile_no':
            'colleague_email_id ':
            'relationships':
            'relative_name':
            'relative_mobile_no':
            'user':

        }

        :Note
        RELATION = (
            ('Mother', 'MO'),
            ('Father', 'FA'),
            ('Brother', 'BR'),
            ('Sister', 'SI'),
            ('Spouse', 'SP')
            )

        Method POST
        :param
        {
            "colleague_name": "test",
            "colleague_mobile_no": "111111111",
            "colleague_email_id": "test@gmail.com",
            "relationships": "Mother",
            "relative_name": "test",
            "relative_mobile_no": "1111111111"
        }

        response:
        {
            "id": 18,
            "colleague_name": "test",
            "colleague_mobile_no": "111111111",
            "colleague_email_id": "test@gmail.com",
            "relationships": "Mother",
            "relative_name": "test",
            "relative_mobile_no": "1111111111",
            "created_date": "2019-09-13T11:34:12.915141",
            "updated_date": "2019-09-13T11:34:12.915158",
            "user": 1
        }

        status 9 : No Details Found
        status 10 : Something went wrong

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = user_serializers.Reference

    def get(self, request, *args, **kwargs):

        try:
            user_profile_instance = ReferenceDetails.objects.filter(user=request.user.id).order_by('-id').first()
            if user_profile_instance:
                return Response(
                    {
                        'colleague_name': user_profile_instance.colleague_name,
                        'colleague_mobile_no': user_profile_instance.colleague_mobile_no,
                        'colleague_email_id ': user_profile_instance.colleague_email_id,
                        'relationships': user_profile_instance.relationships,
                        'relative_name': user_profile_instance.relative_name,
                        'relative_mobile_no': user_profile_instance.relative_mobile_no,
                        "relative_email_id": user_profile_instance.relative_email_id,
                        'user': user_profile_instance.user,

                    }
                )
            else:
                return Response(
                    {
                        'status': '9',
                        'message': 'No Details Found',

                    }
                )

        except Exception as e:
            print("-------User Reference Details API-------", e.args)
            return Response(
                {
                    'status': '10',
                    'message': 'Something went wrong!'
                }
            )

    def post(self, request):
        request_data = request.data
        request_data['user'] = request.user.id
        serializer_instance = self.serializer_class(data=request_data)

        if serializer_instance.is_valid():
            serializer_instance.save()
            return Response(serializer_instance.data, status=status.HTTP_201_CREATED)
        return Response(serializer_instance.errors, status=status.HTTP_400_BAD_REQUEST)


class Document_new(generics.ListAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = user_serializers.LoanUploadDocumentSerailizer

    def post(self, request, *args, **kwargs):

        if request:

            document = DocumentDetails.objects.filter(user_id=request.user.id, type=request.data['type']).order_by(
                '-id').first()

            if document:
                document_path = document.path
                # print("path --------- -----------------   :  ",document_path)
                document_path = botofile.get_url(document_path)
                # print("document_path", document_path)
                base64_image = image_as_base64(str(document_path))
                st = str(base64_image).split("'")

                document_dict = {
                    "status": "1",
                    "type": document.type,
                    "data": "data:image/jpeg;base64," + str(st[1]),
                    "is_verified": document.is_verified,

                }
                return Response(document_dict)
            else:
                return Response(
                    {
                        'status': '9',
                        'message': 'No Details Found',
                    }
                )


class Document(generics.ListAPIView):
    """
    User Document Details API

    :parameter:
        Method: GET
        Response

        {
            'type ':
            'path ':
            'verify_date ':
            'verify_by':
            'is_verified ':
            'user':

        }

        :Note

        DOCUMENT_TYPE = (
        ('Aadhar', 'AA'),
        ('Pan', 'PN'),
        ('Bank_Statement', 'BS'),
        ('Salary_Slip', 'SS'),
        ('Company_Id', 'CI'),
        ('Self_Video', 'SV'),
        ('Current_Address', 'CA'),
        ('Permanent_Address', 'PA'),
    )

        status 9 : No Details Found
        status 10 : Something went wrong

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = user_serializers.LoanUploadDocumentSerailizer

    def get(self, request, *args, **kwargs):

        if request:

            document = DocumentDetails.objects.filter(user_id=request.user.id, type=request.data['type']).order_by(
                '-id').first()

            if document:
                document_path = document.path
                document_path = botofile.get_url(document_path)

                base64_image = image_as_base64(str(document_path))
                st = str(base64_image).split("'")

                document_dict = {
                    # this will only fetch last document that user uploaded
                    "type": document.type,
                    "data": "data:image/jpeg;base64," + str(st[1]),
                    "is_verified": document.is_verified,

                }
                return Response(document_dict)
            else:
                return Response(
                    {
                        'status': '9',
                        'message': 'No Details Found',
                    }
                )

    def post(self, request):
        logger.info("In Document Post")
        try:
            self.request.POST._mutable = True
            request_data = request.data
            request_data['user'] = request.user.id
            logger.info("data", request_data)
            import base64
            import binascii

            serializer_instance = self.serializer_class(data=request_data)
            if serializer_instance.is_valid():
                logger.info("serializer_instance valid")

                serializer_instance.save()
                return Response(serializer_instance.data, status=status.HTTP_201_CREATED)
            logger.info("serializer_instance invalid", serializer_instance.errors)
            return Response(serializer_instance.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print("Exception post docume", e)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)


class Professional(generics.ListAPIView):
    """
        Retrieve, update or delete a Professional details.

    :parameter:
        Method: GET
        :param
            Token 2a3dbec17935dd8f52aa2266774954de0b87dc6c

        :return
            Response

            {
                "company_name": "test",
                "company_email_id": "test@gamil.com",
                "company_address": "test",
                "company_landline_no": "12345678",
                "company_designation": "test",
                "join_date": "2019-09-19",
                "experience": "0"
            }

        :

    Method POST
    :param
        {
            "company_name": "test",
            "company_email_id": "test@gamil.com",
            "company_address": "test",
            "company_landline_no": "12345678",
            "company_designation": "test",
            "join_date": "2019-09-19",
            "experience": "0"  #in months
        }
        *All CHARFIELD except join_date : datefield

        Response
        {
            "id": 45,
            "company_name": "test",
            "company_email_id": "test@gamil.com",
            "company_address": "test",
            "company_landline_no": "12345678",
            "company_designation": "test",
            "experience": "0",
            "join_date": "2019-09-13",
            "created_date": "2019-09-13T12:18:15.262943",
            "updated_date": "2019-09-13T12:18:15.262954",
            "user": 2
        }

        status 201 : Success
        status 400 : Bad Request

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = user_serializers.Professional

    def get(self, request, *args, **kwargs):

        try:
            user_profile_instance = ProfessionalDetails.objects.filter(user=request.user.id).order_by('-id').first()

            if user_profile_instance:
                return Response(
                    {

                        "company_name": user_profile_instance.company_name,
                        "company_email_id": user_profile_instance.company_email_id,
                        "company_address": user_profile_instance.company_address,
                        "company_landline_no": user_profile_instance.company_landline_no,
                        "company_designation": user_profile_instance.company_designation,
                        "join_date": user_profile_instance.join_date,
                        "experience": user_profile_instance.experience
                    }
                )
            else:
                return Response(
                    {
                        'status': '9',
                        'message': 'No Details Found',

                    }
                )

        except Exception as e:
            print("-------User Professional Details API-------", e.args)
            return Response(
                {
                    'status': '10',
                    'message': 'Something went wrong!'
                }
            )

    def post(self, request):
        try:

            request_data = request.data
            serializer_instance = user_serializers.Professional(data=request_data)

            if serializer_instance.is_valid():
                serializer_instance.save()
                return Response(serializer_instance.data, status=status.HTTP_201_CREATED)

            return Response(serializer_instance.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print("Exception post docume", e)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)


class LocationTrackingAPI(generics.ListAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = user_serializers.Location

    def post(self, request):
        ctx = {}
        try:
            request_data = request.data
            if request_data:
                time_date = str(request_data['location']['timestamp'])[:10]
                timestamp = datetime.datetime.fromtimestamp(int(time_date)).strftime('%Y-%m-%d')
                Location.objects.create(user_id=request.user.id, type="home", timestamp=timestamp,
                                        speed=request_data['location']['coords']['speed'],
                                        heading=request_data['location']['coords']['heading'],
                                        accuracy=request_data['location']['coords']['accuracy'],
                                        altitude=request_data['location']['coords']['altitude'],
                                        longitude=request_data['location']['coords']['longitude'],
                                        latitude=request_data['location']['coords']['latitude'])
                ctx['status'] = '201'
            else:
                ctx['status'] = '400'
            return Response(ctx)
        except Exception as e:
            return HttpResponse(ctx)


class Version_Check(generics.ListAPIView):
    authentication_classes = (TokenAuthentication,)

    # permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        """

        :param request: No Parameters Only Token

        :return:
            Case 1:
                'version_code': char,
                'version_name': char,
                'status': '1'
                Description: Gets Latest Version of the App

            Case 2:
                'status': '2'
                Description: No Version Found

            Case 3:
                'status': '3'
                Description: Something Went wrong
        """
        try:
            app_Version = AppVersionCheck.objects.filter(status='Active')
            if app_Version:
                # Get the latest (first) Version
                return Response(
                    {
                        'version_code': app_Version[0].version_code,
                        'version_name': app_Version[0].version_name,
                        'status': '1',
                        'message': 'Details Found'

                    })
            else:
                return Response({
                    'status': '02',
                    'message': 'No Details Found'
                })


        except:
            return Response({
                'status': '03',
                'message': 'Something Went Wrong'
            })


def create_new_version(request):
    """ Creates new version """
    try:
        app_Version = AppVersionCheck.objects.create(version_name="1", version_code="1", status="Active")
        if app_Version:
            # Get the latest (first) Version
            return HttpResponse("Object Created", app_Version)
        else:
            return HttpResponse("Object not Created", app_Version)

    except:
        return HttpResponse("exc")


class UploadLoanDocumentImageDetailsAPIView(APIView):
    """
    API to upload user document in Base64 format

    :parameter:

        API to store user document in Base64 format

        Parameter

        {
            "key": "Base64",
            "address_type": "Choices"
        }

        key : (
            id_card_photo, pan_card,
            salary_slip, balanced_sheet,
            residence_address_proof,
        )

        address_type : (
            address_proof_aadhar,
            address_proof_credit_card_bill,
            address_proof_electricity_bill,
            address_proof_telephone_bill,
            address_proof_voter_id,
            address_proof_passport,
        )

        Response

        {
            "status": "CharField",
            "message": "CharField",
            "address_thumb" : "URL",
            "pan_thumb" : "URL",
        }

        Note

        Authentication token required

        [
            {
                "key":"Authorization",
                "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
            }
        ]

        status 1 : Document add/updated successfully
        status 10 : Something went wrong (Django Error)
        status 11 : Enter valid data

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = user_serializers.LoanUploadDocumentSerailizer

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        pan_thumb = None
        address_thumb = None
        thumb = None

        ax_address_type_message = ''

        # try:
        #
        #     user_instance = User.objects.get(id=request.user.id)
        #     user_docs_instance = DocumentDetails.objects.filter(user=user_instance)
        #     type = request.data.get('type')
        #
        #     if user_docs_instance:
        #         # pf_instance_obj = LoanProfessionalDetails.objects.get(user=request.user)
        #         pf_serializer_instance = useraccounts_serializer.ApplyLoanProfessinalDetailsImageSerializer(
        #             data=request.data, instance=pf_instance_obj
        #         )
        #
        #         if pf_serializer_instance.is_valid():
        #
        #             pf_serializer_instance.save(
        #
        #             )
        #
        #             import os
        #             from sorl.thumbnail import get_thumbnail
        #
        #             pf_docs_obj = LoanProfessionalDetails.objects.get(user=request.user)
        #
        #             if pf_docs_obj.id_card_photo:
        #
        #                 pan_obj_str = pf_docs_obj.id_card_photo
        #                 file_ext = os.path.splitext(pan_obj_str.name)[1]
        #                 valid_extensions = ['.png', '.jpg', '.jpeg']
        #
        #                 current_site = Site.objects.get_current()
        #                 domain_name = current_site.domain
        #
        #                 if file_ext.lower() in valid_extensions:
        #                     # im_thumb = get_thumbnail(pf_docs_obj.id_card_photo, '256x256', crop='center',
        #                     #                          quality=99)
        #                     # thumb = domain_name + 'media/' + str(im_thumb)
        #                     id_card_photo_thumb = botofile.get_url(pf_docs_obj.id_card_photo)
        #                 else:
        #
        #                     thumb = domain_name + 'media/default_images/DEFAULT_DOC_LOGO_256x256.png'
        #
        #             return Response(
        #                 {
        #                     'status': '1',
        #                     'message': 'Document uploaded successfully',
        #                     'thumb': thumb
        #                 }
        #             )
        #
        #         else:
        #
        #             return Response(
        #                 {
        #                     'status': '11',
        #                     'message': pf_serializer_instance.errors,
        #                     'thumb': 'null'
        #                 }
        #             )
        # except:
        #     pass


def image_upload(request):
    '''
    test image upload
    :param request:
    :return:
    '''
    path = os.path.join(settings.BASE_DIR) + '/templates/'
    url = "http://127.0.0.1:8000/api/document/"

    image_as_base64(os.path.join(path, "a.jpeg"))
    with open(os.path.join(path, "a.jpeg"), "rb") as a:
        encoded_string = base64.b64encode(a.read())

        response = requests.post(url, data={'path': encoded_string, "type": "Aadhar"})

    return HttpResponse("Uploaded")


class ValidatePincodeAPIView(APIView):
    """
        To validate pin-code

        :parameter:

            Parameter

            {
                "pincode": "CharField",                    # 422401
            }

            Response

            {
                "status": "CharField",                      # 1
                "message": "CharField", 1                   # Valid pincode
            }

            Note

            Guest token required

            "guest-token-pincode" : "dgjkvcllvdrfpjfogxsomifrihmwlxqv"

            status 1 : Valid pin code
            status 2 : Invalid pin code ( not present in pin code master)
            status 10 : Something went wrong (Django Error)
            status 11 : Enter valid data

        """

    # permission_classes = (partial(GuestTokenPermissionPincodeCheck, ['GET', 'POST', 'HEAD']),)
    serializer_class = user_serializers.PincodeValidateSerializer

    def post(self, request, *args, **kwargs):

        status = None
        message = None
        taluka = None
        city = None
        state = None

        pincode = request.data.get('pincode')
        pincode_serializer = self.serializer_class(data=request.data)

        if pincode_serializer.is_valid():

            try:

                pincode_instance = PincodeMaster.objects.filter(pincode=pincode).order_by('-id').first()

                if pincode_instance:
                    taluka = pincode_instance.taluka
                    city = pincode_instance.city
                    state = pincode_instance.state
                    status = '1'
                    message = 'Valid pin code'


                else:

                    status = '2'
                    message = 'Invalid pin code'

            except Exception as e:

                status = '10'
                message = 'Something went wrong'

        else:

            status = '11'
            message = 'Enter valid data'

        return Response({
            'status': status,
            'message': message,
            'taluka': taluka,
            'city': city,
            'state': state
        })


class UserProfile(generics.ListAPIView):
    """
        Retrieve user Profile.

    :parameter:
        Method: GET
        :param
            Token 2a3dbec17935dd8f52aa2266774954de0b87dc6c

        :return
            Response

            {
                "personal": {
                    "name": "TEST",
                    "birthdate": "2016-06-01",
                    "gender": "Male",
                    "marital_status": "MARRIED",
                    "occupation": null,
                    "mobile_no": "1111111111",
                    "alt_mobile_no": "1111111111",
                    "monthly_income": 1234,
                    "pan_number": "ATHPJ2231L"
                },
                "professional": {
                    "company_name": "test",
                    "company_email_id": "test@gamil.com",
                    "company_address": "test",
                    "company_landline_no": "12345678",
                    "company_designation": "test"
                },
                "reference": {
                    "address": "test",
                    "city": "test",
                    "state": "test",
                    "pin_code": "test",
                    "type": "Permanent"
                 },
                "bank": {
                    'bank_name':CharField
                    'account_number':CharField
                    'ifsc_code':CharField
                    'user':CharField
                },
                "current_address": {
                    "address": "test",
                    "city": "test",
                    "state": "test",
                    "pin_code": "test",
                    "type": "Current"
                },
                "permanent_address": {
                    "address": "test",
                    "city": "test",
                    "state": "test",
                    "pin_code": "test",
                    "type": "Permanent"
                },
                "document": {
                    'type ':
                    'path ':
                    'verify_date ':
                    'verify_by':
                    'is_verified ':
                    'user':
                }
        :
        status 201 : Success
        status 400 : Bad Request

        Note

        DOCUMENT_TYPE = (
            ('Aadhar', 'AA'),
            ('Pan', 'PN'),
            ('Bank_Statement', 'BS'),
            ('Salary_Slip', 'SS'),
            ('Company_Id', 'CI'),
            ('Self_Video', 'SV'),
            ('Current_Address', 'CA'),
            ('Permanent_Address', 'PA'),
        )

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = user_serializers.Professional

    def get(self, request, *args, **kwargs):
        details_list = []
        user_details = {}
        personal_details_dict = {}
        professional_details_dict = {}
        current_address_dict = {}
        permanent_address_dict = {}
        reference_dict = {}
        bank_details_dict = {}
        document_dict = {}
        document_list = []
        try:
            professional = ProfessionalDetails.objects.filter(user_id=request.user.id).order_by('-id').first()
            personal = PersonalDetails.objects.filter(user_id=request.user.id).order_by('-id').first()
            bank = BankDetails.objects.filter(user_id=request.user.id).order_by('-id').first()
            reference = ReferenceDetails.objects.filter(user_id=request.user.id).order_by('-id').first()
            current_address = AddressDetails.objects.filter(user_id=request.user.id, type="Current").order_by(
                '-id').first()
            permanent_address = AddressDetails.objects.filter(user_id=request.user.id, type="Permanent").order_by(
                '-id').first()

            try:
                personal_details_dict = {
                    "name": personal.name,
                    "fathers_name": personal.fathers_name,
                    "birthdate": personal.birthdate,
                    "gender": personal.gender,
                    "marital_status": personal.marital_status,
                    "occupation": personal.occupation,
                    "mobile_no": personal.mobile_no,
                    "alt_mobile_no": personal.alt_mobile_no,
                    "monthly_income": str(personal.monthly_income),
                    'email_id': personal.email_id,
                    "pan_number": personal.pan_number,
                    "aadhar_number": personal.aadhar_number,
                    "itr": personal.itr,
                    "mobile_linked_aadhaar": personal.mobile_linked_aadhaar,
                    "residence_type": personal.residence_type,
                    "industry_type": personal.industry_type,
                    "educational": personal.educational
                }
            except:
                pass
            try:
                professional_details_dict = {
                    "company_name": professional.company_name,
                    "company_email_id": professional.company_email_id,
                    "company_address": professional.company_address,
                    "company_landline_no": professional.company_landline_no,
                    "company_designation": professional.company_designation,
                    "join_date": professional.join_date,
                    "experience": professional.experience
                }
            except:
                pass
            try:
                bank_details_dict = {
                    "bank_name": bank.bank_name,
                    "account_number": bank.account_number,
                    "ifsc_code": bank.ifsc_code,
                }
            except:
                pass

            try:
                reference_dict = {
                    "colleague_name": reference.colleague_name,
                    "colleague_mobile_no": reference.colleague_mobile_no,
                    "colleague_email_id": reference.colleague_email_id,
                    "relationships": reference.relationships,
                    "relative_name": reference.relative_name,
                    "relative_mobile_no": reference.relative_mobile_no,
                    "relative_email_id": reference.relative_email_id

                }
            except:
                pass
            try:
                c_address = str(current_address.address).split(";")
                current_address_dict = {
                    "currentLocality": c_address[0],
                    "currentStreet": c_address[1],
                    "currentLandmark": c_address[2],
                    "city": current_address.city,
                    "state": current_address.state,
                    "pin_code": current_address.pin_code,
                    "type": "Current",

                }
            except:
                pass
            try:
                p_address = str(permanent_address.address).split(";")
                permanent_address_dict = {
                    "permanentLocality": p_address[0],
                    "permanentStreet": p_address[1],
                    "permanentLandmark": p_address[2],
                    "city": permanent_address.city,
                    "state": permanent_address.state,
                    "pin_code": permanent_address.pin_code,
                    "type": "Permanent",

                }
            except:
                pass
            try:
                for i, j in DOCUMENT_TYPE:
                    document = DocumentDetails.objects.filter(user_id=request.user.id, type=i).order_by('-id').first()
                    if document:
                        document_path = botofile.get_url(document.path)

                        base64_image = image_as_base64(str(document_path))

                        document_dict = {
                            # this will only fetch last document that user uploaded
                            "type": document.type,
                            "data": base64_image,
                            "is_verified": document.is_verified,

                        }

                        document_list.append(document_dict)



            except Exception as e:
                print(e)
                print("-------Doc  API-------", e.args)
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                pass

            if user_details is not None:
                user_details['personal'] = personal_details_dict
                user_details['professional'] = professional_details_dict
                user_details['reference'] = reference_dict
                user_details['bank'] = bank_details_dict
                user_details['current_address'] = current_address_dict
                user_details['permanent_address'] = permanent_address_dict
                user_details['document'] = document_list

                return Response(
                    user_details
                )
            else:
                return Response(
                    {
                        'status': '9',
                        'message': 'No Details Found',

                    }
                )

        except Exception as e:
            print("-------User Professional Details API-------", e.args)
            return Response(
                {
                    'status': '10',
                    'message': 'Something went wrong!'
                }
            )


import os
import base64


def image_as_base64(image_file):
    """
    :param `image_file` for the complete path of image.
    """
    encoded_file = None

    if not image_file:
        return None

    # path = settings.MEDIA_ROOT + '/' + image_file
    path = image_file
    try:
        # Getting image from url

        path = base64.b64encode(requests.get(path).content)
        encoded_file = path
    except Exception as e:
        print("-------Image to base64 error-------", e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)

    return encoded_file


def application_save(username):
    engine = create_engine(
        'postgresql+psycopg2://anandfin:XBBHxQk5r0nI@anandfin.cska70p9kqpe.ap-south-1.rds.amazonaws.com/kwik_db')

    connection = engine.connect()
    user = "'" + str(username) + "'"
    user_name = str(username)
    user_user = connection.execute('select * from auth_user where username=' + user)
    if user_user:
        for us in user_user:
            user_id = "'" + str(us['id']) + "'"
            status = "'APPROVED'"
            loan_application = connection.execute(
                'select * from "UserAccounts_userloantransaction" where status=' + status + ' and user_id=' + user_id
            )

            if loan_application:
                for row in loan_application:
                    loan_application_id = row['loan_application_id']
                    loan_app_id = LoanApplication.objects.filter(loan_application_id=loan_application_id)
                    if loan_app_id:
                        loan_id = LoanApplication.objects.filter(loan_application_id=loan_application_id)
                        if loan_id:
                            payments = connection.execute(
                                'SELECT * FROM payments where user_loan_application=' + loan_applicaion_loan
                            )
                            for row3 in payments:
                                transaction_id3 = ''
                                if row3['atom_transaction_number_1']:
                                    transaction_id3 = row3['atom_transaction_number_1']
                                else:
                                    if row3['atom_transaction_number_2']:
                                        transaction_id3 = row3['atom_transaction_number_2']
                                order_id3 = ''
                                if row3['merchant_id_resvalue']:
                                    order_id3 = row['merchant_id_resvalue']
                                status3 = row['status']
                                category3 = "'Loan'"
                                mode3 = ''
                                if row3['atom_transaction_type']:
                                    mode3 = row3['atom_transaction_type']
                                type3 = "'App'"
                                response3 = ''
                                if row3['atom_response']:
                                    response3 = str(row3['atom_response']).replace("'", "`")
                                amount3 = ''
                                if row3['amount']:
                                    amount3 = row3['amount']
                                description3 = row3['atom_description']
                                created_date3 = row3['created_date']
                                date3 = ''
                                if row3['atom_transaction_date']:
                                    date3 = row3['atom_transaction_date']
                                pay = Payment.objects.create(order_id=order_id3, transaction_id=transaction_id3,
                                                             status=status3, category=category3, mode=mode3, type=type3,
                                                             response=response3, amount=amount3, date=date3,
                                                             create_date=created_date3, description=description3)

                                payment_id_data3 = Payment.objects.filter(transaction_id=transaction_id3)
                                if payment_id_data3:
                                    loan_application = LoanApplication.objects.filter(
                                        loan_application_id=loan_application_id)
                                    loan_application[0].repayment_amount.add(pay.id)
                    if not loan_app_id:
                        name = row['name']
                        birthdate = row['date_of_birth']
                        marital_status = row['marital_status']
                        mobile_no = row['mobile_number']
                        occupation = row['occupation']
                        pan_number = row['pan_number']
                        created_date = row['created_date']

                        user = User.objects.filter(username=user_name)
                        email = "'email@gmail.com'"
                        PersonalDetails.objects.create(name=name, birthdate=birthdate, marital_status=marital_status,
                                                       email_id=email,
                                                       occupation=occupation, mobile_no=mobile_no,
                                                       pan_number=pan_number,
                                                       created_date=created_date, user_id=user[0].id)

                        personal = PersonalDetails.objects.filter(user_id=user[0].id).order_by('-id')
                        personal_id = ''
                        if personal:
                            personal_id = personal[0].id

                        # Proffessional
                        company_name = ''
                        if str(row['current_empolyer_name']) != 'None':
                            company_name = row['current_empolyer_name']
                        company_email_id = ''
                        if str(row['work_email_id']) != 'None':
                            company_email_id = row['work_email_id']
                        company_address = ''
                        if str(row['work_address']) != 'None':
                            company_address = row['work_address']
                        company_designation = ''
                        if str(row['current_designation']) != 'None':
                            company_designation = row['current_designation']
                        experience = ''
                        if str(row['total_work_experience']) != 'None':
                            experience = row['total_work_experience']
                        join_date = datetime.datetime.now()
                        if str(row['working_from']) != 'None':
                            join_date = row['working_from']
                        ProfessionalDetails.objects.create(company_name=company_name, company_email_id=company_email_id,
                                                           company_address=company_address,
                                                           company_designation=company_designation,
                                                           created_date=created_date, user_id=user[0].id,
                                                           experience=experience, join_date=join_date)
                        professional = ProfessionalDetails.objects.filter(user_id=user[0].id).order_by('-id')
                        professional_id = ''
                        if professional:
                            professional_id = professional[0].id
                        # bank
                        bank_name = row['bank_name']
                        account_number = row['account_number']
                        ifsc_code = row['ifsc_code']
                        BankDetails.objects.create(created_date=created_date, bank_name=bank_name,
                                                   account_number=account_number, ifsc_code=ifsc_code,
                                                   user_id=user[0].id)
                        bank = ProfessionalDetails.objects.filter(user_id=user[0].id).order_by('-id')
                        bank_id = ''
                        if bank:
                            bank_id = bank[0].id
                        # address
                        address = row['address']
                        pin_code = row['pin_code']
                        address_type = "Current"
                        AddressDetails.objects.create(address=address, pin_code=pin_code, type=address_type,
                                                      created_date=created_date, user_id=user[0].id)
                        address = AddressDetails.objects.filter(user_id=user[0].id).order_by('-id')
                        address_id = ''
                        if address:
                            address_id = address[0].id
                        # id_card_photo
                        id_card_photo = str(row['id_card_photo']).split("/")
                        id_card = ''
                        if str(row['id_card_photo']) != "None":
                            id_card = "'" + "/images/" + str(id_card_photo[1]).replace(" ", "_") + "'"
                        Company_Id = "Company_Id"
                        DocumentDetails.objects.create(type=Company_Id, path=id_card, created_date=created_date,
                                                       user_id=user[0].id)
                        id_card_id = ''
                        document = DocumentDetails.objects.filter(user_id=user[0].id, type=Company_Id).order_by('-id')
                        if document:
                            id_card_id = document[0].id
                        # pan_card
                        pan_card_d = str(row['pan_card']).split("/")
                        pan_card = ''
                        if str(row['pan_card']) != "None":
                            pan_card = "'" + "/images/" + str(pan_card_d[1]).replace(" ", "_") + "'"
                        Pan = "Pan"
                        DocumentDetails.objects.create(type=Pan, path=pan_card, created_date=created_date,
                                                       user_id=user[0].id)
                        pan_ca_id = 1
                        document_pan_ca_id = DocumentDetails.objects.filter(user_id=user[0].id, type=Pan).order_by(
                            '-id')
                        if document_pan_ca_id:
                            pan_ca_id = document_pan_ca_id[0].id
                        # salary_slip

                        salary_slip_s = str(row['salary_slip']).split("/")
                        salary_slip = ''
                        if str(row['salary_slip']) != "None":
                            salary_slip = "'" + "/images/" + str(salary_slip_s[1]).replace(" ", "_") + "'"
                        Salary_Slip = "Salary_Slip"
                        DocumentDetails.objects.create(type=Salary_Slip, path=salary_slip, created_date=created_date,
                                                       user_id=user[0].id)
                        salary_slip_id = 1
                        document_salary_slip_id = DocumentDetails.objects.filter(user_id=user[0].id,
                                                                                 type=Salary_Slip).order_by(
                            '-id')
                        if document_salary_slip_id:
                            salary_slip_id = document_salary_slip_id[0].id
                        # balanced_sheet_bs
                        balanced_sheet_bs = str(row['balanced_sheet']).split("/")
                        balanced_sheet = ''
                        if str(row['balanced_sheet']) != "None":
                            balanced_sheet = "'" + "/images/" + str(balanced_sheet_bs[1]).replace(" ", "_") + "'"
                        Bank_Statement = "Bank_Statement"
                        DocumentDetails.objects.create(type=Bank_Statement, path=balanced_sheet,
                                                       created_date=created_date,
                                                       user_id=user[0].id)
                        balanced_sheet_id = ''
                        document_balanced_sheet_id = DocumentDetails.objects.filter(user_id=user[0].id,
                                                                                    type=Bank_Statement).order_by(
                            '-id')
                        if document_balanced_sheet_id:
                            balanced_sheet_id = document_balanced_sheet_id[0].id
                        # residence_address_proof

                        residence_address_proof_rap = str(row['residence_address_proof']).split("/")
                        residence_address_proof = ''
                        if str(row['residence_address_proof']) != "None":
                            residence_address_proof = "'" + "/images/" + str(residence_address_proof_rap[1]).replace(
                                " ",
                                "_") + "'"
                        Current_Address = "Current_Address"
                        DocumentDetails.objects.create(type=Current_Address, path=residence_address_proof,
                                                       created_date=created_date,
                                                       user_id=user[0].id)
                        residence_address_proof_id = ''
                        document_residence_address_proof_id = DocumentDetails.objects.filter(user_id=user[0].id,
                                                                                             type=Current_Address).order_by(
                            '-id')
                        if document_residence_address_proof_id:
                            residence_address_proof_id = document_residence_address_proof_id[0].id
                        # self_images_d

                        user1 = connection.execute(
                            'select * from "UserProfile_userprofile" where user_id=' + user_id + 'ORDER BY id DESC')
                        for row1 in user1:
                            self_created_date = row1['created_date']
                            profile_image = str(row1['profile_image']).split("/")
                            if str(row1['profile_image']) != "None":
                                self_path = "'" + "/images/" + str(profile_image[1]).replace(" ", "_") + "'"
                            else:
                                path = ''

                            self_type = "Self_Video"
                            DocumentDetails.objects.create(type=self_type, path=self_path,
                                                           created_date=self_created_date,
                                                           user_id=user[0].id)
                            self_images_id = ''
                            self_images_d = Document.objects.filter(user_id=user[0].id,
                                                                    type=self_type).order_by(
                                '-id')
                            if self_images_d:
                                self_images_id = self_images_d[0].id
                        # refences
                        refences = ReferenceDetails.objects.filter(user_id=user[0].id).order_by('-id')
                        refences_id = ''
                        if refences:
                            refences_id = refences[0].id
                        if not refences:
                            refences_data = connection.execute(
                                'select * from "UserAccounts_loanreferences" where user_id=' + user_id + 'ORDER BY id DESC')
                            for refences_d in refences_data:
                                relative = str(refences_d['rf_name_1']).replace("'", "`")
                                relative_name = relative
                                relative_mobile_no = str(refences_d['rf_phone_number_1'])
                                relative_email_id = str(refences_d['rf_email_address_1'])
                                colleague = str(row['rf_name_2']).replace("'", "`")
                                colleague_name = colleague
                                colleague_mobile_no = str(row['rf_phone_number_2'])
                                colleague_email_id = str(row['rf_email_address_2'])
                                user_id = str(row['user_id'])
                                created_date5 = row['created_date']
                                updated_date5 = row['updated_date']
                                ref = Reference.objects.create(colleague_name=colleague_name,
                                                               colleague_mobile_no=colleague_mobile_no,
                                                               colleague_email_id=colleague_email_id,
                                                               relative_name=relative_name,
                                                               relative_mobile_no=relative_mobile_no,
                                                               created_date=created_date5, updated_date=updated_date5,
                                                               user_id=user[0].id, relative_email_id=relative_email_id)

                                refences_id = ref.id

                        end_date = str(datetime.datetime.now())
                        loan_start_date = datetime.datetime.now()
                        if str(row['loan_application_start_date']) != "None":
                            loan_start_date = row['loan_application_start_date']
                            end_date = row['loan_application_start_date'] + timedelta(int(row['tenure']))

                        loan_end_date = end_date
                        device_id = row['device_id']
                        device_model = row['device_model']
                        user_location = row['user_location']
                        created_date = row['created_date']
                        updated_date = row['updated_date']
                        loan_scheme_id = row['loan_repayment_amount']
                        status = row['status']
                        hold_days = 0
                        if str(row['edi']) != 'None':
                            hold_days = row['edi']

                        device_data_id = 1
                        device = DeviceData.objects.filter(user_id=user[0].id).order_by(
                            '-id')
                        if device:
                            device_data_id = device[0].id
                        loan_applicaion_loan = "'" + str(loan_application_id) + "'"
                        subsc = connection.execute(
                            'select * from "UserAccounts_rbltransaction" where transaction_id_text=' + loan_applicaion_loan)
                        for row2 in subsc:

                            order_id2 = row2['transaction_id_text']
                            transaction_id2 = row2['rrn']
                            status2 = 'initiated'
                            if row2['status'] == 'Success':
                                status2 = 'success'
                            if row2['status'] == 'Failed' or row2['status'] == 'FAILED' or row2['status'] == 'Failure':
                                status2 = 'failed'
                            corp_id2 = row2['corp_id']
                            maker_id2 = row2['maker_id']
                            checker_id2 = row2['checker_id']
                            approver_id2 = row2['approver_id']
                            response_code2 = row2['response_code']
                            error_description2 = row2['error_description']
                            ref_number2 = row2['ref_number']
                            channel_part_ref_number2 = row2['channel_part_ref_number']
                            ben_acc_no2 = row2['ben_acc_no']
                            ben_ifsc_code2 = row2['ben_ifsc_code']
                            category2 = "Rbl"
                            mode2 = "NB"
                            type2 = "Account"
                            response2 = ''
                            amount2 = ''
                            if str(row2['amount']) != 'None':
                                amount2 = row2['amount']
                            created_date2 = row2['created_date']
                            date2 = 'NULL'
                            if str(row2['txn_time']) != 'None':
                                date2 = row2['txn_time']
                            updated_date2 = row2['updated_date']

                            Payment.objects.create(order_id=order_id2, transaction_id=transaction_id2, status=status2,
                                                   category=category2, mode=mode2, type=type2, response=response2,
                                                   amount=amount2, date=date2, create_date=created_date2)
                            payment = Payment.objects.filter(order_id=order_id2).order_by(
                                '-id')
                            payment_id = ''
                            if payment:
                                payment_id = payment[0].id
                                RBLTransaction.objects.create(created_date=created_date2, updated_date=updated_date2,
                                                              transaction_id_text=loan_application_id, corp_id=corp_id2,
                                                              maker_id=maker_id2, checker_id=checker_id2,
                                                              approver_id=approver_id2, status=status2,
                                                              response_code=response_code2,
                                                              error_description=error_description2,
                                                              ref_number=ref_number2,
                                                              channel_part_ref_number=channel_part_ref_number2,
                                                              rrn=transaction_id2, ben_acc_no=ben_acc_no2,
                                                              amount=amount2,
                                                              ben_ifsc_code=ben_ifsc_code2, txn_time=date2,
                                                              payment_id=payment_id)
                        LoanApplication.objects.create(loan_application_id=loan_application_id,
                                                       loan_start_date=loan_start_date,
                                                       loan_end_date=loan_end_date, device_id=device_id,
                                                       device_model=device_model, location=user_location,
                                                       created_date=created_date, updated_date=updated_date,
                                                       loan_scheme_id=loan_scheme_id, bank_id_id=bank_id,
                                                       bank_sts_id_id=balanced_sheet_id, company_id_id=id_card_id,
                                                       current_address_id=address_id, device_data_id_id=device_data_id,
                                                       disbursed_amount_id=payment_id, pancard_id_id=pan_ca_id,
                                                       reference_id_id=refences_id, salary_slip_id_id=salary_slip_id,
                                                       self_video_id=self_images_id, user_id=user[0].id, status=status,
                                                       user_personal_id=personal_id,
                                                       user_professional_id=professional_id,
                                                       hold_days=hold_days,
                                                       cur_address_id_id=residence_address_proof_id)

                        loan_id = LoanApplication.objects.filter(loan_application_id=loan_application_id)
                        if loan_id:
                            payments = connection.execute(
                                'SELECT * FROM payments where user_loan_application=' + loan_applicaion_loan
                            )
                            for row3 in payments:
                                transaction_id3 = ''
                                if row3['atom_transaction_number_1']:
                                    transaction_id3 = row3['atom_transaction_number_1']
                                else:
                                    if row3['atom_transaction_number_2']:
                                        transaction_id3 = row3['atom_transaction_number_2']
                                order_id3 = ''
                                if row3['merchant_id_resvalue']:
                                    order_id3 = row['merchant_id_resvalue']
                                status3 = row['status']
                                category3 = "'Loan'"
                                mode3 = ''
                                if row3['atom_transaction_type']:
                                    mode3 = row3['atom_transaction_type']
                                type3 = "'App'"
                                response3 = ''
                                if row3['atom_response']:
                                    response3 = str(row3['atom_response']).replace("'", "`")
                                amount3 = ''
                                if row3['amount']:
                                    amount3 = row3['amount']
                                description3 = row3['atom_description']
                                created_date3 = row3['created_date']
                                date3 = ''
                                if row3['atom_transaction_date']:
                                    date3 = row3['atom_transaction_date']
                                pay = Payment.objects.create(order_id=order_id3, transaction_id=transaction_id3,
                                                             status=status3, category=category3, mode=mode3, type=type3,
                                                             response=response3, amount=amount3, date=date3,
                                                             create_date=created_date3, description=description3)

                                payment_id_data3 = Payment.objects.filter(transaction_id=transaction_id3)
                                if payment_id_data3:
                                    loan_application = LoanApplication.objects.filter(
                                        loan_application_id=loan_application_id)
                                    loan_application[0].repayment_amount.add(pay.id)
    connection.close()

    return True


class ValidatePanCardVAPIView(APIView):
    """
    To validate pan card
    :parameter:
        Parameter
        {
            "pan_card": "CharField",                    # DPDPE1234F
        }
        Response
        {
            "status": "CharField",                      # 1
            "message": "CharField", 1                   # Valid pan card
            "pan_status": "CharField",
            "name": "CharField"
        }
        Note
        Authentication token required
        [
            {
                "key":"Authorization",
                "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
            }
        ]
        status 1 : Valid pan card
        status 2 : Invalid pan card ( already used by anothoer user)
        status 10 : Something went wrong (Django Error)
        status 11 : Enter valid data
        NSDL Status
        pan_1 : Valid pan card
        pan_2 : Fake/Blocked/Invalid pan card
        pan_3 : Invalid nsdl response
        pan_4 : System error
        pan_5 : In exception
    """
    authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    serializer_class = user_serializers.PanCardValidateSerializer

    def post(self, request, *args, **kwargs):
        import json
        pan_card = request.data.get('pan_card')
        if pan_card:
            status = '11'
            message = 'Enter valid data'
            from StaticData.views import check_pan_block
            check_block_pan = check_pan_block(pan_card, 'ck')
            if check_block_pan == True:
                return Response({
                    "status": status,
                    "message": message
                })
        user = request.user
        is_pan_linked = is_pancard_linked(pan_card, user)
        if is_pan_linked:
            return Response(
                {
                    'status': 9,
                    'message': "Pancard is already registered"
                }
            )

        dict_param = {
            'pan_card': pan_card,
            'username': "creditkartpan",
            'password': "credit@123",
        }
        json_body = json.dumps(dict_param)
        prod_url = 'https://mudrakwik.anandfin.com/api/pancard-validate-creditkart/'

        # prod_url = "https://app.thecreditkart.com/api/pancard-validate-creditkart/"
        headers = {
            'content-type': "application/json",
            'cache-control': "no-cache",
        }
        response = requests.post(prod_url, data=json_body, headers=headers, verify=False)
        responce_js = response.json()
        return Response(
            responce_js
        )


def is_pancard_linked(pan_number, user):
    """
    checks if the pancard is linked with any other number
    :param user:
    :param pan_number:
    :return:
        True if pancard is linked
        False if not linked
    """
    get_users_pan = PersonalDetails.objects.filter(user=user).exclude(pan_number__isnull=True).order_by(
        'pan_number').distinct("pan_number")
    if get_users_pan:
        if len(get_users_pan) == 1:
            get_user_pan = PersonalDetails.objects.filter(pan_number=pan_number, user=user)
            if not get_user_pan:
                # This user is already linked with other pancard
                return True
        if len(get_users_pan) > 1:
            return True

    get_personal_details = PersonalDetails.objects.filter(pan_number=pan_number).order_by('user').distinct("user")
    if get_personal_details:
        if len(get_personal_details) == 1:
            get_user_pan = PersonalDetails.objects.filter(pan_number=pan_number, user=user)
            if not get_user_pan:
                # This pan card is already linked with other user
                return True
            else:
                return False
        if len(get_personal_details) > 1:
            return True
        else:
            return False
    else:
        return False


class FundspiPanAPI(APIView):
    """
    To validate pan card

    :parameter:

        Parameter

        {
            "pan_card":"AYOPK7344K",
            "username":"fundspipan",
            "password":"admin@123"

        }

        Response

        {
            "status": "CharField",                      # 1
            "message": "CharField", 1                   # Valid pan card
            "pan_status": "CharField",
            "name": "CharField"
        }

        Note



        status 1 : Valid pan card
        status 2 : Invalid pan card ( already used by another user)
        status 3 : Invalid Credentials
        status 10 : Something went wrong (Django Error)
        status 11 : Enter valid data

        NSDL Status

        pan_1 : Valid pan card
        pan_2 : Fake/Blocked/Invalid pan card
        pan_3 : Invalid nsdl response
        pan_4 : System error
        pan_5 : In exception

    """

    serializer_class = user_serializers.PanCardValidateSerializer

    def post(self, request, *args, **kwargs):

        today = datetime.datetime.today()

        request_cnt = 1
        pan_cnt = 0
        name = None

        pan_card_serializer = self.serializer_class(data=request.data)
        pan_card = request.data.get('pan_card')
        username = request.data.get('username')
        password = request.data.get('password')
        user_authenticate = authenticate(request, username=username, password=password)
        if not user_authenticate:
            return Response(
                {
                    'status': "3",
                    'message': "Invalid credentials",
                }
            )
        elif user_authenticate:
            get_pan_request = PanRequest.objects.filter(date=today)
            if get_pan_request:
                if get_pan_request[0]:
                    request_cnt = get_pan_request[0].total_request_cnt + 1
                update_pan = get_pan_request.update(total_request_cnt=request_cnt)

            else:
                create_pan_request = PanRequest.objects.create(date=today, total_request_cnt=request_cnt)

        try:

            if pan_card_serializer.is_valid():

                # check the pan card already used by another user

                user_pan_instance = PanCardMaster.objects.filter(pancard=pan_card.upper()).order_by('-id').first()
                if user_pan_instance:

                    name = user_pan_instance.name
                    status = '1'
                    message = 'Valid pan card'

                else:

                    pan_kwrgs = {
                        'pan_number': pan_card.upper()
                    }

                    pan_response = utils.pull_pan_details(**pan_kwrgs)
                    if user_authenticate:
                        pan_cnt += 1
                        update_pan_request = PanRequest.objects.filter(date=today).update(pan_request_cnt=pan_cnt)

                    if pan_response['nsdl_status'] == '1_true':

                        # valid pan card
                        name = pan_response['full_name']
                        status = '1'
                        message = 'Valid pan card'
                        PanCardMaster.objects.create(name=name, pancard=pan_card.upper(), user_id=request.user.id)


                    elif pan_response['nsdl_status'] == '2_false':

                        # fake/invalid/deleted pancard
                        name = None
                        status = '2'
                        message = 'Pancard invalid.'

                    elif pan_response['nsdl_status'] == '3_false':

                        # invalid response code
                        name = None
                        status = '2'
                        message = 'Pancard invalid.'

                    elif pan_response['nsdl_status'] == '4_false':

                        # in exception
                        name = None
                        status = '2'
                        message = 'Pancard invalid.'

                    else:

                        # none response
                        name = None
                        status = '2'
                        message = 'Pancard invalid.'


            else:

                status = '11'
                message = 'Invalid pancard'
                name = None

        except Exception as e:
            print(e)
            status = '10'
            message = 'Something went wrong!'

        return Response(
            {
                'status': status,
                'message': message,
                'name': name,
            }
        )


class AllLoanDetails(generics.ListAPIView):
    """
      Parameter
           {
            "name": "neuton",
            "fathers_name": "shashikant",
            "birthdate": "2000-12-12",
            "gender": "Male",
            "marital_status": "MARRIED",
            "occupation": "SALARIED",
            "mobile_no": "1234567890",
            "alt_mobile_no": "9090935789",
            "monthly_income": "1233",
            "pan_number": "asdf",
            "email_id": "test@gmail.com",
            "type": "xyz",
            "itr": "1",
            "mobile_linked_aadhaar": "7890389849",
            "residence_type": "rural",
            "industry_type": "primary",
            "educational": "cyhjs",
            "company_name": "mudrakwik",
            "company_email_id": "mudra@gmail.com",
            "company_address": "Kharadi",
            "company_landline_no": "9038902882",
            "company_designation": "jr software engineer",
            "experience": "10 month",
            "join_date": "2020-09-08",
            "colleague_name": "hima",
            "colleague_mobile_no": "8983893892",
            "colleague_email_id": "ud@gmail.com",
            "relationships": "bestfrd",
            "relative_name": "sonal",
            "relative_mobile_no": "8954098598",
            "relative_email_id": "sonal@gmail.com",
            "address": "vaishnavi pg kharadi",
            "city": "pune",
            "state": "maharashtra",
            "pin_code": "441112",
            "personal_id":"2"

    }
     Response
            {
            "personal_details": {
                "message": {
                    "name": "neuton",
                    "fathers_name": "shashikant",
                    "birthdate": "2000-12-12",
                    "gender": "Male",
                    "marital_status": "MARRIED",
                    "occupation": "SALARIED",
                    "alt_mobile_no": "9090935789",
                    "email_id": "test@gmail.com",
                    "monthly_income": "1233",
                    "pan_number": "asdf",
                    "type": "xyz",
                    "itr": "1",
                    "mobile_linked_aadhaar": "7890389849",
                    "residence_type": "rural",
                    "industry_type": "primary",
                    "educational": "cyhjs",
                    "user_id": "7"
            },
            "status": 201
        },
        "professional_details": {
            "message": {
                "company_name": "mudrakwik",
                "company_email_id": "mudra@gmail.com",
                "company_address": "Kharadi",
                "company_landline_no": "9038902882",
                "company_designation": "jr software engineer",
                "experience": "10 month",
                "join_date": "2021-02-06",
                "user_id": "7"
            },
            "status": 201
        },
        "reference_details": {
            "message": {
                "colleague_name": "hima",
                "colleague_mobile_no": "8983893892",
                "colleague_email_id": "ud@gmail.com",
                "relationships": "bestfrd",
                "relative_name": "sonal",
                "relative_mobile_no": "8954098598",
                "relative_email_id": "sonal@gmail.com",
                "user_id": "7"
            },
            "status": 201
        },

        "address_details": {
            "message": {
                "address": "vaishnavi pg kharadi",
                "city": "pune",
                "state": "maharashtra",
                "pin_code": "441112",
                "type": "xyz",
                "personal_id": "2",
                "user_id": "7"
            },
            "status": 201
        }
    }
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        personal_dict = {}
        professional_dict = {}
        reference_dict = {}
        address_dict = {}

        if request:
            # Personal
            user_profile_instance = PersonalDetails.objects.filter(user=request.user.id).order_by('-id').first()
            if user_profile_instance:
                personal_dict = {
                    "status": "1",
                    'name': user_profile_instance.name,
                    'fathers_name': user_profile_instance.fathers_name,
                    'birthdate': user_profile_instance.birthdate,
                    'gender': user_profile_instance.gender,
                    'marital_status': user_profile_instance.marital_status,
                    'occupation': user_profile_instance.occupation,
                    'mobile_no': user_profile_instance.mobile_no,
                    'alt_mobile_no': user_profile_instance.alt_mobile_no,
                    'email_id': user_profile_instance.email_id,
                    'monthly_income': str(user_profile_instance.monthly_income),
                    'pan_number': user_profile_instance.pan_number,
                    'aadhar_number': user_profile_instance.aadhar_number,
                    "itr": user_profile_instance.itr,
                    "mobile_linked_aadhaar": user_profile_instance.mobile_linked_aadhaar,
                    "residence_type": user_profile_instance.residence_type,
                    "industry_type": user_profile_instance.industry_type,
                    "educational": user_profile_instance.educational
                }
            else:
                personal_dict = {
                    'status': '9',
                    'message': 'No Personal Details Found'
                }
            # Professional
            user_professional_instance = ProfessionalDetails.objects.filter(user=request.user.id).order_by(
                '-id').first()
            if user_professional_instance:
                professional_dict = {
                    "status": "1",
                    "company_name": user_professional_instance.company_name,
                    "company_email_id": user_professional_instance.company_email_id,
                    "company_address": user_professional_instance.company_address,
                    "company_landline_no": user_professional_instance.company_landline_no,
                    "company_designation": user_professional_instance.company_designation,
                    "join_date": user_professional_instance.join_date,
                    "experience": user_professional_instance.experience
                }
            else:
                professional_dict = {
                    'status': '9',
                    'message': 'No Professional Details Found',
                }
            # Reference
            user_reference_instance = ReferenceDetails.objects.filter(user=request.user.id).order_by('-id').first()
            if user_reference_instance:
                reference_dict = {
                    "status": "1",
                    'colleague_name': user_reference_instance.colleague_name,
                    'colleague_mobile_no': user_reference_instance.colleague_mobile_no,
                    'colleague_email_id': user_reference_instance.colleague_email_id,
                    'relationships': user_reference_instance.relationships,
                    'relative_name': user_reference_instance.relative_name,
                    'relative_mobile_no': user_reference_instance.relative_mobile_no,
                    "relative_email_id": user_reference_instance.relative_email_id,
                    'user': user_reference_instance.user.username,
                }
            else:
                reference_dict = {
                    'status': '9',
                    'message': 'No Reference Details Found',
                }

            # Bank
            user_profile_instance = BankDetails.objects.filter(user=request.user.id).order_by('-id').first()

            if user_profile_instance:

                bank_dict = {
                    "status": "1",
                    'bank_name': user_profile_instance.bank_name,
                    'account_number': user_profile_instance.account_number,
                    'ifsc_code': user_profile_instance.ifsc_code,
                }


            else:
                bank_dict = {
                    'status': "9",
                    'message': 'No Bank Details Found',
                }

            # Address
            user_address_instance = AddressDetails.objects.filter(user=request.user.id).order_by('-id').first()
            if user_address_instance:
                address_dict = {
                    "status": "1",
                    'address': user_address_instance.address,
                    'city': user_address_instance.city,
                    'state': user_address_instance.state,
                    'pin_code': user_address_instance.pin_code,
                    'type': user_address_instance.type,
                }
            else:
                address_dict = {
                    'status': "9",
                    'message': 'No Address Details Found',
                }
            re_status = 1
            if personal_dict["status"] == "9" or professional_dict["status"] == "9" or reference_dict[
                "status"] == "9" or bank_dict["status"] == "9" or address_dict["status"] == "9":
                re_status = 0

            if personal_dict and professional_dict and reference_dict and bank_dict and address_dict:
                return Response(
                    {
                        "status": re_status,
                        "personal_details": personal_dict,
                        "professional_details": professional_dict,
                        "reference_details": reference_dict,
                        "bank_details": bank_dict,
                        "address_details": address_dict
                    }
                )
            else:
                return Response(
                    {
                        'status': '9',
                        'message': 'No Details Found',
                    }
                )

    def post(self, request):
        personal_dict = {}
        professional_dict = {}
        reference_dict = {}
        bank_dict = {}
        address_dict = {}
        request_data = request.data
        if request_data:
            request_data['user_id'] = request.user.id
            request_data["occupation"] = "SALARIED"
            request_data["monthly_income"] = float(request_data["monthly_income"])
            if request_data['mobile_no'] is None:
                request_data['mobile_no'] = request.user.username
            personal_serializer_instance = user_serializers.Personal1(data=request_data)
            if personal_serializer_instance.is_valid():
                personal_serializer_instance.save()
                personal_dict = {
                    "data": personal_serializer_instance.data,
                    "status": status.HTTP_201_CREATED
                }
            else:
                personal_dict = {
                    "data": personal_serializer_instance.errors,
                    "status": status.HTTP_400_BAD_REQUEST
                }
            # Professional

            request_data["company_landline_no"] = str(request_data["company_landline_no"]).replace(" ", "")
            request_data["company_landline_no"] = str(request_data["company_landline_no"]).replace("-", "")

            professional_serializer_instance = user_serializers.Professional1(data=request_data)
            if professional_serializer_instance.is_valid():
                professional_serializer_instance.save()
                data = professional_serializer_instance.data
                professional_dict = {
                    "data": data,
                    "status": status.HTTP_201_CREATED
                }
            else:
                professional_dict = {
                    "data": professional_serializer_instance.errors,
                    "status": status.HTTP_400_BAD_REQUEST
                }
            # Reference

            request_data["colleague_mobile_no"] = str(request_data["colleague_mobile_no"]).replace(" ", "")
            request_data["colleague_mobile_no"] = str(request_data["colleague_mobile_no"]).replace("-", "")
            request_data["relative_mobile_no"] = str(request_data["relative_mobile_no"]).replace(" ", "")
            request_data["relative_mobile_no"] = str(request_data["relative_mobile_no"]).replace("-", "")

            reference_serializer_instance = user_serializers.Reference1(data=request_data)
            if reference_serializer_instance.is_valid():
                reference_serializer_instance.save()
                reference_dict = {
                    "data": reference_serializer_instance.data,
                    "status": status.HTTP_201_CREATED
                }
            else:
                reference_dict = {
                    "data": reference_serializer_instance.errors,
                    "status": status.HTTP_400_BAD_REQUEST
                }

            # Bank

            bank_serializer_instance = user_serializers.Bank1(data=request_data)

            if bank_serializer_instance.is_valid():
                bank_serializer_instance.save()
                bank_dict = {
                    "data": bank_serializer_instance.data,
                    "status": status.HTTP_201_CREATED

                }
            else:
                bank_dict = {
                    "data": bank_serializer_instance.errors,
                    "status": status.HTTP_400_BAD_REQUEST
                }

            # Address
            add_type = "Current"
            add_json = {
                "address": request_data["address"],
                "city": request_data["city"],
                "state": request_data["state"],
                "pin_code": request_data["pin_code"],
                "type": add_type,
                "user_id": request_data["user_id"]
            }
            address_serializer_instance = user_serializers.Address1(data=add_json)
            if address_serializer_instance.is_valid():
                address_serializer_instance.save()
                address_dict = {
                    "data": address_serializer_instance.data,
                    "status": status.HTTP_201_CREATED
                }
            else:
                address_dict = {
                    "data": address_serializer_instance.errors,
                    "status": status.HTTP_400_BAD_REQUEST
                }
            re_status = 0

            if personal_dict["status"] >= 200 and professional_dict["status"] >= 200 and reference_dict[
                "status"] >= 200 and bank_dict["status"] >= 200 and address_dict["status"] >= 200:
                re_status = 1
            if personal_dict and professional_dict and reference_dict and bank_dict and address_dict:
                return Response(
                    {
                        "status": re_status,
                        "personal_details": personal_dict,
                        "professional_details": professional_dict,
                        "reference_details": reference_dict,
                        "bank_details": bank_dict,
                        "address_details": address_dict
                    }
                )
            else:
                return Response(
                    {
                        'status': '9',
                        'message': 'Data Not Saved',
                    }
                )
        else:
            return Response(
                {
                    'status': '9',
                    'message': 'Data Not Found',
                }
            )
