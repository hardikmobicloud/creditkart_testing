
from apscheduler.schedulers.background import BackgroundScheduler
from django.apps.registry import apps
from django.conf import settings
from django.db.models import Sum
from django.shortcuts import HttpResponse
from django.views import generic
from Recovery.models import Recovery
from LoanAppData.views import *
from .email import failed_transaction_sms

Payment = apps.get_model('Payments', 'Payment')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
Subscription = apps.get_model('User', 'Subscription')
AutoApproved = apps.get_model('LoanAppData', 'AutoApproved')

import datetime
from datetime import timedelta
import csv
import base64
import string
import random
import hashlib


IV = "@@@@&&&&####$$$$"
BLOCK_SIZE = 16

__pad__ = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * chr(BLOCK_SIZE - len(s) % BLOCK_SIZE)
__unpad__ = lambda s: s[0:-ord(s[-1])]


def __encode__(to_encode, iv, key):
    # Pad
    to_encode = __pad__(to_encode)
    # Encrypt
    c = AES.new(key, AES.MODE_CBC, iv)
    to_encode = c.encrypt(to_encode)
    # Encode
    to_encode = base64.b64encode(to_encode)
    return to_encode.decode("UTF-8")


def __decode__(to_decode, iv, key):
    # Decode
    to_decode = base64.b64decode(to_decode)
    # Decrypt
    c = AES.new(key, AES.MODE_CBC, iv)
    to_decode = c.decrypt(to_decode)
    if type(to_decode) == bytes:
        # convert bytes array to str.
        to_decode = to_decode.decode()
    # remove pad
    return __unpad__(to_decode)


def __id_generator__(size=6, chars=string.ascii_uppercase + string.digits + string.ascii_lowercase):
    return ''.join(random.choice(chars) for _ in range(size))


def __get_param_string__(params, escape_refund=True):
    params_string = []
    for key in sorted(params.keys()):
        if ("|" in params[key] or (escape_refund == True and "REFUND" in params[key])):
            respons_dict = {}
            exit()
        value = params[key]
        params_string.append('' if value == 'null' else str(value))
    return '|'.join(params_string)


def generate_checksum(param_dict, merchant_key, salt=None):
    params_string = __get_param_string__(param_dict)
    return generate_checksum_by_str(params_string, merchant_key, salt)


def generate_refund_checksum(param_dict, merchant_key, salt=None):
    for i in param_dict:
        if ("|" in param_dict[i]):
            param_dict = {}
            exit()
    params_string = __get_param_string__(param_dict, False)
    return generate_checksum_by_str(params_string, merchant_key, salt)


def generate_checksum_by_str(param_str, merchant_key, salt=None):
    params_string = param_str
    salt = salt if salt else __id_generator__(4)
    final_string = '%s|%s' % (params_string, salt)
    hasher = hashlib.sha256(final_string.encode())
    hash_string = hasher.hexdigest()
    hash_string += salt
    return __encode__(hash_string, IV, merchant_key)


def verify_checksum(param_dict, merchant_key, checksum):
    # Remove checksum
    if 'CHECKSUMHASH' in param_dict:
        param_dict.pop('CHECKSUMHASH')
    params_string = __get_param_string__(param_dict, False)
    return verify_checksum_by_str(params_string, merchant_key, checksum)


def verify_checksum_by_str(param_str, merchant_key, checksum):
    paytm_hash = __decode__(checksum, IV, merchant_key)
    salt = paytm_hash[-4:]
    calculated_checksum = generate_checksum_by_str(param_str, merchant_key, salt=salt)
    return calculated_checksum == checksum





# check loan application in dpd



def Sub_Transaction_Status_Check_Static(request):
    import requests
    status = None
    message = None
    amount = 0.0
    date_strip = None
    try:
        for i in range(1, 0, -1):
            date = (datetime.datetime.today() - datetime.timedelta(days=i)).date()
            payment = Payment.objects.filter(create_date__startswith="2019-12-27",status__in=['initiated', 'pending', 'PENDING', 'failed'], category='Subscription')


            for i in payment:
                if i.order_id is not None:
                    if 'ms' in i.order_id:

                        o_id = i.order_id
                        paytm_status = ""
                        merchant_key = settings.PROD_SUB_MERCHANT_KEY
                        m_id = settings.PROD_SUB_MERCHANT_MID
                        dict_param = {
                            'MID': m_id,
                            'ORDERID': o_id,
                        }

                        checksum = str(generate_checksum(dict_param, merchant_key))

                        dict_param["CHECKSUMHASH"] = checksum

                        url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                        r = requests.get(url=url)

                        if r.status_code == 200:

                            paytm_responce = r.json()

                            if paytm_responce['STATUS'] == 'TXN_SUCCESS':
                                paytm_status = 'success'
                            elif paytm_responce['STATUS'] == 'TXN_FAILURE':
                                paytm_status = 'failed'
                            elif paytm_responce['STATUS'] == 'PENDING':
                                paytm_status = 'pending'

                            if 'TXNAMOUNT' in paytm_responce:
                                amount = paytm_responce['TXNAMOUNT']
                                if amount is None or amount == '':
                                    amount = 0.0
                            if 'TXNDATE' in paytm_responce:
                                date_strip = paytm_responce['TXNDATE']
                                if date_strip is None or date_strip == '':
                                    date_strip = None
                            mode = ''
                            if 'PAYMENTMODE' in paytm_responce:
                                if paytm_responce['PAYMENTMODE']:
                                    mode = paytm_responce['PAYMENTMODE']
                            if date_strip is None or date_strip == '':
                                expiry_date = None
                                date_time_str = None
                                date_time_obj = None
                            else:
                                expiry_date = date_strip
                                date_time_str = str(date_strip)
                                date_time_obj = datetime.datetime.strptime(date_strip, '%Y-%m-%d %H:%M:%S.%f')
                                expiry_date = date_time_obj + timedelta(89)

                            Payment.objects.filter(order_id=o_id).update(status=paytm_status,
                                                                         transaction_id=paytm_responce['TXNID'], amount=amount,
                                                                         category='Subscription',
                                                                         mode=mode,
                                                                         type='App',
                                                                         response=paytm_responce,
                                                                         date=date_time_obj
                                                                         )
                            payment = Payment.objects.filter(order_id=o_id).last()
                            Subscription.objects.filter(payment_id=payment.id).update(start_date=date_time_obj,
                                                                                      expiry_date=expiry_date,
                                                                                      payment_status=paytm_status)
    except Exception as e:

        import sys
        import os
        print('-----------in exception----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)

        import requests

        sms_message = 'Subscription Payment Update ex' + str(e.args) + '\n' + str(exc_tb.tb_lineno) + str(fname, )
        number = '6352820504'

        sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
            settings.SMS_USERNAME) + '&password=' + str(settings.SMS_PASSWORD) + \
                  '&type=0&dlr=1&destination=' + number + '&source=' + str(
            settings.SMS_SOURCE) + '&message=' + sms_message

        requests.get(sms_url)
    # return response
    return HttpResponse('ok')


def Transaction_Status_Check_Static(request):
    import requests
    status = None
    message = None
    amount = 0.0
    date_strip = None
    try:
        for i in range(1, 0, -1):
            date = (datetime.datetime.today() - datetime.timedelta(days=i)).date()

            pay_transaction = Payment.objects.filter(status__in=['initiated', 'pending', 'PENDING', 'failed'],
                                                     create_date__startswith=str(date)[:10], category='Loan')


            if pay_transaction:
                for i in pay_transaction:
                    if i.order_id is None:
                        pass
                    else:
                        if 'm' in i.order_id:
                            if 'ms' not in i.order_id:
                                o_id = i.order_id
                                loan_data = str(i.order_id).split("m")
                                loan_application_id = ''
                                if loan_data:
                                    loan_application_id = loan_data[0]
                                paytm_status = ""
                                merchant_key = settings.PROD_REP_MERCHANT_KEY
                                m_id = settings.PROD_REP_MERCHANT_MID
                                dict_param = {
                                    'MID': m_id,
                                    'ORDERID': o_id,
                                }

                                checksum = str(generate_checksum(dict_param, merchant_key))

                                dict_param["CHECKSUMHASH"] = checksum
                                url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                                r = requests.get(url=url)
                                if r.status_code == 200:

                                    paytm_responce = r.json()

                                    if paytm_responce['STATUS'] == 'TXN_SUCCESS':
                                        paytm_status = 'success'
                                    elif paytm_responce['STATUS'] == 'TXN_FAILURE':
                                        paytm_status = 'failed'
                                    elif paytm_responce['STATUS'] == 'PENDING':
                                        paytm_status = 'pending'

                                    if 'TXNAMOUNT' in paytm_responce:
                                        amount = paytm_responce['TXNAMOUNT']
                                        if amount is None or amount == '':
                                            amount = 0.0
                                    if 'TXNDATE' in paytm_responce:
                                        date_strip = paytm_responce['TXNDATE']
                                        if date_strip is None or date_strip == '':
                                            date_strip = None
                                    mode = ''
                                    if 'PAYMENTMODE' in paytm_responce:
                                        if paytm_responce['PAYMENTMODE']:
                                            mode = paytm_responce['PAYMENTMODE']
                                    Payment.objects.filter(order_id=o_id).update(status=paytm_status,
                                                                                 transaction_id=paytm_responce['TXNID'],
                                                                                 amount=amount,
                                                                                 category='Loan',
                                                                                 mode=mode,
                                                                                 type='App',
                                                                                 response=paytm_responce,
                                                                                 date=date_strip
                                                                                 )
                                    loan_txn_instance = LoanApplication.objects.filter(loan_application_id=loan_application_id,
                                                                                       status="APPROVED")

                                    if loan_txn_instance:

                                        if paytm_status == 'success':
                                            rep_amount = loan_txn_instance[0].repayment_amount.filter(
                                                status='success').aggregate(Sum('amount'))
                                            # Validating
                                            if rep_amount:
                                                if float(rep_amount['amount__sum']) >= float(loan_txn_instance[0].loan_scheme_id):
                                                    complete_loan = LoanApplication.objects.filter(
                                                        loan_application_id=loan_application_id
                                                    ).update(status='COMPLETED')
                                                    recovery = Recovery.objects.filter(loan_transaction_id=loan_txn_instance[0].id)
                                                    if recovery:
                                                        Recovery.objects.filter(loan_transaction_id=loan_txn_instance[0].id).update(
                                                            completed_date=date_strip, recovery_status='Yes')
                                                    dpd = loan_application_dpd(loan_txn_instance[0].user_id)
                                                    if dpd > 0:
                                                        AutoApproved.objects.filter(user_id=loan_txn_instance[0].user_id,
                                                                                    status=True).update(
                                                            status=False
                                                        )
                                                        AutoApproved.objects.create(user_id=loan_txn_instance[0].user_id,
                                                                                    option_limit=1000, status=False)

                                        if paytm_status == 'failed':
                                            try:
                                                mobile_number = loan_txn_instance[0].user.username
                                                failed_transaction_sms(mobile_number, amount)
                                            except Exception as e:
                                                print("Inside Exception of failed_transaction_sms", e.args)

    except Exception as e:

        import sys
        import os

        print('-----------in exception----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)

        import requests

        sms_message = 'Repayment Payment Update  ex' + str(e.args) + '\n' + str(exc_tb.tb_lineno)
        number = '7875583679'

        sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
            settings.SMS_USERNAME) + '&password=' + str(settings.SMS_PASSWORD) + \
                  '&type=0&dlr=1&destination=' + number + '&source=' + str(
            settings.SMS_SOURCE) + '&message=' + sms_message

        requests.get(sms_url)
    return HttpResponse('ok')








import json


def Sub_Transaction_Status_Check_auto():
    import requests
    status = None
    message = None
    amount = 0.0
    date_strip = None
    try:
        for i in range(3, 0, -1):
            date = (datetime.datetime.today() - datetime.timedelta(days=i)).date()
            payment = Payment.objects.filter(status__in=['initiated', 'pending', 'PENDING', 'failed'],
                                             create_date__startswith=str(date)[:10],
                                             category='Subscription')

            for i in payment:
                if i.order_id is not None:
                    if 'ms' in i.order_id:

                        o_id = i.order_id
                        paytm_status = ""
                        merchant_key = settings.PROD_SUB_MERCHANT_KEY
                        m_id = settings.PROD_SUB_MERCHANT_MID
                        dict_param = {
                            'MID': m_id,
                            'ORDERID': o_id,
                        }

                        checksum = str(generate_checksum(dict_param, merchant_key))

                        dict_param["CHECKSUMHASH"] = checksum

                        url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                        r = requests.get(url=url)
                        if r.status_code == 200:

                            paytm_responce = r.json()

                            if paytm_responce['STATUS'] == 'TXN_SUCCESS':
                                paytm_status = 'success'
                            elif paytm_responce['STATUS'] == 'TXN_FAILURE':
                                paytm_status = 'failed'
                            elif paytm_responce['STATUS'] == 'PENDING':
                                paytm_status = 'pending'

                            if 'TXNAMOUNT' in paytm_responce:
                                amount = paytm_responce['TXNAMOUNT']
                                if amount is None or amount == '':
                                    amount = 0.0
                            if 'TXNDATE' in paytm_responce:
                                date_strip = paytm_responce['TXNDATE']
                                if date_strip is None or date_strip == '':
                                    date_strip = None
                            mode = ''
                            if 'PAYMENTMODE' in paytm_responce:
                                if paytm_responce['PAYMENTMODE']:
                                    mode = paytm_responce['PAYMENTMODE']
                            if date_strip is None or date_strip == '':
                                expiry_date = None
                                date_time_str = None
                                date_time_obj = None
                            else:
                                expiry_date = date_strip
                                date_time_str = str(date_strip)
                                date_time_obj = datetime.datetime.strptime(date_strip, '%Y-%m-%d %H:%M:%S.%f')
                                expiry_date = date_time_obj + timedelta(89)

                            Payment.objects.filter(order_id=o_id).update(status=paytm_status,
                                                                         transaction_id=paytm_responce['TXNID'], amount=amount,
                                                                         category='Subscription',
                                                                         mode=mode,
                                                                         type='App',
                                                                         response=paytm_responce,
                                                                         date=date_time_obj
                                                                         )
                            payment = Payment.objects.filter(order_id=o_id).last()
                            Subscription.objects.filter(payment_id=payment.id).update(start_date=date_time_obj,
                                                                                      expiry_date=expiry_date,
                                                                                      payment_status=paytm_status)
    except Exception as e:

        import sys
        import os
        print('-----------in exception----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)

        import requests

        sms_message = 'Subscription Payment Update ex' + str(e.args) + '\n' + str(exc_tb.tb_lineno) + str(fname, )
        number = '6352820504'

        sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
            settings.SMS_USERNAME) + '&password=' + str(settings.SMS_PASSWORD) + \
                  '&type=0&dlr=1&destination=' + number + '&source=' + str(
            settings.SMS_SOURCE) + '&message=' + sms_message

        requests.get(sms_url)
    # return response


def Transaction_Status_Check_auto():
    import requests
    status = None
    message = None
    amount = 0.0
    date_strip = None
    try:
        for i in range(3, 0, -1):
            date=(datetime.datetime.today() - datetime.timedelta(days=i)).date()

            pay_transaction = Payment.objects.filter(status__in=['initiated','pending','PENDING','failed'],create_date__startswith=str(date)[:10], category='Loan')
            if pay_transaction:
                for i in pay_transaction:
                    if i.order_id is None:
                        pass
                    else:
                        if 'm' in i.order_id:
                            if 'ms' not in i.order_id:
                                o_id = i.order_id
                                loan_data = str(i.order_id).split("m")
                                loan_application_id = ''
                                if loan_data:
                                    loan_application_id = loan_data[0]
                                paytm_status = ""
                                merchant_key = settings.PROD_REP_MERCHANT_KEY
                                m_id = settings.PROD_REP_MERCHANT_MID
                                dict_param = {
                                    'MID': m_id,
                                    'ORDERID': o_id,
                                }

                                checksum = str(generate_checksum(dict_param, merchant_key))

                                dict_param["CHECKSUMHASH"] = checksum
                                url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
                                r = requests.get(url=url)
                                if r.status_code == 200:

                                    paytm_responce = r.json()

                                    if paytm_responce['STATUS'] == 'TXN_SUCCESS':
                                        paytm_status = 'success'
                                    elif paytm_responce['STATUS'] == 'TXN_FAILURE':
                                        paytm_status = 'failed'
                                    elif paytm_responce['STATUS'] == 'PENDING':
                                        paytm_status = 'pending'

                                    if 'TXNAMOUNT' in paytm_responce:
                                        amount = paytm_responce['TXNAMOUNT']
                                        if amount is None or amount == '':
                                            amount = 0.0
                                    if 'TXNDATE' in paytm_responce:
                                        date_strip = paytm_responce['TXNDATE']
                                        if date_strip is None or date_strip == '':
                                            date_strip = None
                                    mode = ''
                                    if 'PAYMENTMODE' in paytm_responce:
                                        if paytm_responce['PAYMENTMODE']:
                                            mode = paytm_responce['PAYMENTMODE']
                                    Payment.objects.filter(order_id=o_id).update(status=paytm_status,
                                                                                 transaction_id=paytm_responce['TXNID'],
                                                                                 amount=amount,
                                                                                 category='Loan',
                                                                                 mode=mode,
                                                                                 type='App',
                                                                                 response=paytm_responce,
                                                                                 date=date_strip
                                                                                 )
                                    loan_txn_instance = LoanApplication.objects.filter(loan_application_id=loan_application_id,
                                                                                       status="APPROVED")

                                    if loan_txn_instance:
                                        if paytm_status == 'success':
                                            rep_amount = loan_txn_instance[0].repayment_amount.filter(
                                                status='success').aggregate(Sum('amount'))
                                            # Validating
                                            if rep_amount:
                                                if float(rep_amount['amount__sum']) >= float(loan_txn_instance[0].loan_scheme_id):
                                                    complete_loan = LoanApplication.objects.filter(
                                                        loan_application_id=loan_application_id
                                                    ).update(status='COMPLETED')
                                                    recovery = Recovery.objects.filter(loan_transaction_id=loan_txn_instance[0].id)
                                                    if recovery:
                                                        Recovery.objects.filter(loan_transaction_id=loan_txn_instance[0].id).update(
                                                            completed_date=date_strip, recovery_status='Yes')
                                                    dpd = loan_application_dpd(loan_txn_instance[0].user_id)
                                                    if dpd > 0:
                                                        AutoApproved.objects.filter(user_id=loan_txn_instance[0].user_id,
                                                                                    status=True).update(
                                                            status=False
                                                        )
                                                        AutoApproved.objects.create(user_id=loan_txn_instance[0].user_id,
                                                                                    option_limit=1000, status=False)

                                        if paytm_status == 'failed':
                                            try:
                                                mobile_number = loan_txn_instance[0].user.username
                                                failed_transaction_sms(mobile_number, amount)
                                            except Exception as e:
                                                print("Inside Exception of failed_transaction_sms", e.args)

    except Exception as e:

        import sys
        import os

        print('-----------in exception----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)

        import requests

        sms_message = 'Repayment Payment Update  ex' + str(e.args) + '\n' + str(exc_tb.tb_lineno)
        number = '7875583679'

        sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
            settings.SMS_USERNAME) + '&password=' + str(settings.SMS_PASSWORD) + \
                  '&type=0&dlr=1&destination=' + number + '&source=' + str(
            settings.SMS_SOURCE) + '&message=' + sms_message

        requests.get(sms_url)





def test_trans(request):
    return HttpResponse("ok")



















