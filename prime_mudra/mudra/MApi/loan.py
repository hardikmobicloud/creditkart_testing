import datetime
import logging
import random
import re
from urllib import response
from django.views.decorators.csrf import csrf_exempt
from User.models import *
from django.apps import apps
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Sum
from django.http import HttpResponse
from django.shortcuts import render
from EApi.credit import check_defaulter_status_mk, check_defaulter_status_rk, check_defaulter_status_insta
from mudra.constants import scheme
from rest_framework import generics
from rest_framework.authentication import *
from rest_framework.permissions import *
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.utils import json

from mudra import settings
from .email import email, sms
from .fcm_notifications import fcm_parameter_notification
from LoanAppData.loan_funcations import loan_amount_slab, check_loan_default, loan_paid_amount
from datetime import datetime, timedelta
from Payments.tasks import rbl_worker

from ecom.views import image_thumbnail

AppVersionCheck = apps.get_model('StaticData', 'AppVersionCheck')
Personal = apps.get_model('User', 'Personal')
Bank = apps.get_model('User', 'Bank')
Address = apps.get_model('User', 'Address')
Reference = apps.get_model('User', 'Reference')
Document = apps.get_model('User', 'Documents')
Professional = apps.get_model('User', 'Professional')
PincodeMaster = apps.get_model('StaticData', 'PincodeMaster')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
LoanBanner = apps.get_model('LoanAppData', 'LoanBanner')
AutoApproved = apps.get_model('LoanAppData', 'AutoApproved')
ApprovedLoans = apps.get_model('LoanAppData', 'ApprovedLoans')
LoanAcknowledgement = apps.get_model('LoanAppData', 'LoanAcknowledgement')
DeviceData = apps.get_model('UserData', 'DeviceData')
Payment = apps.get_model('Payments', 'payment')
SmsStringMaster = apps.get_model('IcomeExpense', 'SmsStringMaster')
UserWallet = apps.get_model('ecom', 'UserWallet')
OrderDetails = apps.get_model('ecom', 'OrderDetails')
Credit = apps.get_model('ecom', 'Credit')
LoanDefaulter = apps.get_model('ecom', 'LoanDefaulter')
from django.contrib.sites.models import Site

logger = logging.getLogger(__name__)


def convert_date_into_readable_format(date):
    '''
    :param date:
    :return:
    '''
    if date is not None:
        try:
            converte_date = date.strftime("%d/%m/%Y %I:%M:%S %p")
            return converte_date
        except Exception as e:
            import sys
            import os
            print('-----Exception in Date Conversion: convert_date_into_readable_format()-----')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return date
    else:
        return None


class SubmitLoanApplicationAPIView(APIView):
    """
        Submit loan application formSubmitLoanApplicationAPIView

        :parameter:

            Parameter

            {
                "loan_amount": "CharField",
                "tenure": "CharField",
                "user_user_location": "CharField",
                "device_id": "CharField",
                "device_model": "CharField",
            }

            Response

            {
                "status": "CharField",
                "message": "CharField
            }

            Note

            Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]

            status  1 :Loan application submitted successfully
            status  2 :Invalid Loan Amount
            status  3 :You are not eligible for loan
            status  4 :Due to some security flaws in your mobile device we can not approve your loan right now. For more details, please contact us.
            status  5 :Please download latest MudraKwik app from Google Playstore and try again. For more details, please contact us.
            status  6 :Sorry! It appears that your previous loan is not completed yet, we regret to inform that without completing that loan your current requirement will not be processed further.
            status  7:Sorry! It Currently you are not eligible to apply loan. Please contact us for more details
            status  8:Pancard used in active loan
            status  9:Incomplete loan profile : Personal, Professional ,Bank
            status  10:Incomplete pan card Document
            status  11:Incomplete salary slip Document
            status  12:Incomplete  company id Document
            status  13:Incomplete  Selfie Document
            status  14:Incomplete reference Details
            status  15:Incomplete permanent address
            status  16:Incomplete current address
            status  17:Enter valid data
            status  18:Loan Application is on hold for n days
            status  19:Something Went Wrong

        """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    # serializer_class = SubmitLoanSerializer
    # logger.info("In Submit loan API------")

    def post(self, request, *args, **kwargs):

        status = None
        message = None
        user_id = request.user.id
        user_location = request.data.get('user_location')
        loan_scheme_id = request.data.get('loan_amount')
        purpose_loan = request.data.get('reasonOfLoan')

        # try:
        # check his old loan are completed
        user_instance = User.objects.get(id=request.user.id)
        user_loan_status = LoanApplication.objects.filter(user=user_instance).exclude(
            status__in=['COMPLETED', 'REJECTED', 'CANCELLED', 'ON_HOLD'])

        user_device = DeviceData.objects.filter(user_id=request.user.id)

        if not user_device:
            return Response(
                {
                    'status': '3',
                    'message': 'You are not eligible for loan'
                }
            )

        if user_loan_status:
            return Response(
                {
                    'status': '6',
                    'message': 'Sorry! It appears that your previous loan is not completed yet, we regret to inform that without completing that loan your current requirement will not be processed further.'
                }
            )

        # Check if user previous loan is on hold
        ctx = get_loan_onhold_days(request.user.id)
        try:
            if ctx is not None:
                status = ctx['status']
                message = ctx['message']
                holddays = ctx['holddays']
            else:
                status = 19
                message = "Something Went Wrong"
                holddays = 0
        except Exception as e:
            status = 19
            message = "Something Went Wrong"
            holddays = 0

        if status != 1:
            return Response(
                {
                    'status': str(status),
                    'message': message,
                    'holddays': holddays
                }
            )

        # check if user loan profile is completed

        user_pd_instance = Personal.objects.filter(user_id=request.user.id).order_by('-id').first()
        user_pro_de_instance = Professional.objects.filter(user=user_instance).order_by('-id').first()
        user_bd_instance = Bank.objects.filter(user=user_instance).order_by('-id').first()
        # TODO uncomment this
        from datetime import datetime
        if user_pd_instance:

            user_pd_obj_ins = Personal.objects.filter(user_id=request.user.id).order_by('-id').first()
            today_year = datetime.now().year
            bod_year = user_pd_obj_ins.birthdate.year
            age = today_year - bod_year

            if age < 18 or age > 45:
                return Response(
                    {
                        'status': '7',
                        'message': 'Currently you are not eligible to apply loan. Please contact us for more details'
                    }
                )
            if user_pd_obj_ins:
                if not user_pd_obj_ins.itr or not user_pd_obj_ins.educational or not user_pd_obj_ins.industry_type or not user_pd_obj_ins.residence_type or not user_pd_obj_ins.mobile_linked_aadhaar:
                    return Response(
                        {
                            'status': '50',
                            'message': 'Please update your personal details it seems that your personal details is not updated'
                        }
                    )

                    # Validating user PAN Card is not used in Active loans
            user_pan_loan = is_pancard_used_in_active_loan(user_pd_obj_ins.pan_number, user_id)
            if user_pan_loan == False:
                return Response({'status': '8', 'message': 'Pancard used in active loan'})

            if user_pd_instance.occupation == 'SALARIED':

                if not (
                        user_pd_instance and user_pro_de_instance and user_bd_instance):
                    return Response({'status': '9', 'message': 'Incomplete loan profile'})

                # new condition_
                user_professional_obj = Personal.objects.filter(user=user_instance).order_by('-id').first()

                if not user_professional_obj:
                    return Response({'status': '10', 'message': 'Incomplete loan profile'})

        # new condition_
        document_pan = Document.objects.filter(user=user_instance, type='Pan').order_by('-id').first()
        if not document_pan:
            return Response({'status': '11', 'message': 'Incomplete pan card Details'})

        document_salary_slip = Document.objects.filter(user=user_instance, type='Salary_Slip').order_by('-id').first()

        if not document_salary_slip:
            return Response({'status': '12', 'message': 'Incomplete  salary slip Details'})
        document_company_id = Document.objects.filter(user=user_instance, type='Company_Id').order_by('-id').first()
        if not document_company_id:
            return Response({'status': '13', 'message': 'Incomplete company id Details'})

        document_self_video = Document.objects.filter(user=user_instance, type='Self_Video').order_by('-id').first()

        if not document_self_video:
            return Response({'status': '14', 'message': 'Incomplete Self Video Details'})

        reference = Reference.objects.filter(user=user_instance).order_by('-id').first()

        if not reference:
            return Response({'status': '15', 'message': 'Incomplete reference Details'})

        address_current = Address.objects.filter(user=user_instance, type='Current').order_by('-id').first()
        if address_current is None:
            return Response({'status': '16', 'message': 'Incomplete current address'})

        current_site = Site.objects.get_current()
        domain_name = current_site.domain

        # if serializer_class_instance.is_valid():
        # calculate loan interest
        user_pd_obj = Personal.objects.filter(user=user_instance, type="loan").order_by('-id').first()
        if not user_pd_obj:
            return Response(
                {
                    'status': '50',
                    'message': 'Please update your personal details it seems that your personal details is not updated'
                }
            )
        user_pro_de_obj = Professional.objects.filter(user=user_instance).order_by('-id').first()
        if not user_pd_obj:
            return Response(
                {
                    'status': '50',
                    'message': 'Please update your Professional details it seems that your Professional details is not updated'
                }
            )
        user_bd_obj = Bank.objects.filter(user=user_instance, type="loan").order_by('-id').first()
        if not user_pd_obj:
            return Response(
                {
                    'status': '50',
                    'message': 'Please update your Bank details it seems that your Bank details is not updated'
                }
            )

        date = datetime.now()
        random_number = random.sample(range(0, 999), 1)
        application_id = str(date.year)[2:] + "" + str(date.month) + "" + str(date.day) + "" + str(
            date.hour) + "" + str(date.minute) + "" + str(date.second) + "" + str(random_number[0])

        overduestring = SmsStringMaster.objects.filter(user_id=request.user.id,
                                                       sms_string__contains='overdue').count()
        smscount = SmsStringMaster.objects.filter(user_id=request.user.id).count()
        last_balance = getlastamount(user_id=request.user.id)
        loan = LoanApplication.objects.create(
            user=user_instance,
            loan_application_id=application_id,
            device_id=user_device[0].device_id,
            device_model=user_device[0].device_model,
            user_professional=user_pro_de_obj,
            user_personal=user_pd_obj,
            pancard_id=document_pan,
            salary_slip_id=document_salary_slip,
            company_id=document_company_id,
            self_video=document_self_video,
            current_address=address_current,
            reference_id=reference,
            bank_id=user_bd_obj,
            loan_scheme_id=loan_scheme_id,
            loan_start_date=date,
            device_data_id_id=user_device[0].id,
            status='SUBMITTED',
            overdue=overduestring,
            total_sms=smscount,
            purpose_loan=purpose_loan,
            balance=last_balance

        )

        # Save Device Data
        try:
            from lendbox.lapi import save_user_data
            save_user_data(request, application_id)
        except Exception as e:
            print("inside device data function", e)
            pass

        # Notification code

        loan_obj_fcm = LoanApplication.objects.get(loan_application_id=application_id)

        fmp_kwargs = {
            'user_id': user_instance.id,
            'title': 'Loan applied successfully',
            'message': 'Dear customer,\nLoan of Rs.' + str(loan_scheme_id
                                                           ) + ' is applied successfully.\nLoan application id : ' + str(
                application_id),
            'to_store': True,
            'type': 'loan_submitted',
            'notification_id': loan_obj_fcm.id,
            'notification_key': 'loan_id',
        }

        fcm_parameter_notification(**fmp_kwargs)

        # Email code

        email_kwargs = {
            'subject': 'CreditKart - Loan Applied',
            'to_email': [user_pro_de_obj.company_email_id],
            'email_template': 'emails/loan/apply_loan.html',
            'data': {
                'domain': Site.objects.get_current(),
                'loan_instance': loan_obj_fcm
            },
        }

        email(**email_kwargs)

        # SMS code

        sms_message = 'Dear User,\nYour loan application ' + str(
            loan_obj_fcm.loan_application_id) + ' has been Submitted successfully.\nKindly check application.\nThanks for using CreditKart.'

        sms_kwrgs = {
            'sms_message': sms_message,
            'number': str(user_instance.username)
        }

        sms(**sms_kwrgs)

        status = '1'
        message = 'Loan application submitted successfully'
        cv_submit_loan_data = None

        return Response(
            {
                'status': status,
                'message': message,
                'credit_vidya': cv_submit_loan_data
            }
        )


def getlastamount(user_id):
    data = SmsStringMaster.objects.filter(user_id=user_id).order_by('-date_time')
    if data:
        for sms in data:
            response = getAvailableBalance(sms)
            if isinstance(response, float):
                if response == 0.0:
                    pass
                else:
                    pass

            elif isinstance(response, str):
                if response == 0.0:
                    pass
                else:
                    data = response.split(".")
                    if len(data) > 1 and data[1] != '':
                        return response
    return 0.0


def getAvailableBalance(sms):
    sms = str(sms)
    exp = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
    exp1 = '(?i)(avl\.?\s*|avbl\.?\s*|available|Bal|Balance)'
    amount_exp = '-*[,\d]+\.?\d{0,2}'
    opt = re.compile(exp, re.VERBOSE)

    # finds all currency amount in message
    if re.findall(exp, sms):
        details = opt.findall(sms)

    # finds currency amount for available balance
    # if re.search(exp1, sms.lower()):
    if re.search(exp1, sms):
        match = re.search(exp1, sms)
        str_obj = sms.split(match.group())
        if re.search(exp, str_obj[1]):
            amount = re.search(exp, str_obj[1]).group()
            amt = re.search(amount_exp, amount)
            if amt:
                amt = amt.group()
                if amt.__contains__(','):
                    amt = amt.replace(',', '')
                return amt
    return 0.0


def create_pdf(request, pk, application_id):
    """
    Generate pdf form html page

    Note:
        apt-get install wkhtmltopdf
    """
    user_instance = User.objects.get(id=pk)

    try:
        user_instance = User.objects.get(id=pk)

        user_pd_obj = Personal.objects.filter(user=user_instance).order_by('-id').first()
        user_pro_de_obj = Professional.objects.filter(user=user_instance).order_by('-id').first()
        user_document_obj = Document.objects.filter(user=user_instance).order_by('-id').first()
        user_bd_obj = Bank.objects.filter(user=user_instance).order_by('-id').first()

        user_loan_obj = LoanApplication.objects.get(loan_application_id=application_id).first()
        user_pincode = PincodeMaster.objects.filter(pincode=user_loan_obj.pin_code).first()
        user_address = Address.objects.filter(user=user_instance, type='Current').first()

        tenure = ''
        loan_amount = ''
        for i in scheme:
            if i == user_loan_obj.loan_scheme_id:
                tenure = scheme[i]['ms_tenure']
                loan_amount = scheme[i]['ms_net_loan']

        return render(
            request, 'pdf/mudrakwik_pdf_template.html',
            {
                'user_pd_obj': user_pd_obj,
                'user_pro_de_obj': user_pro_de_obj,
                'user_document_obj': user_document_obj,
                'user_bd_obj': user_bd_obj,
                'user': user_instance,
                'user_loan_obj': user_loan_obj,
                'user_pincode': user_pincode,
                'address': user_address,
                'tenure': tenure,
                'loan_amount': loan_amount

            }
        )
    except Exception as e:
        import sys
        import os
        print('-----------in exception----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        return render(
            request, 'pdf/mudrakwik_pdf_template.html',
            {
                'e': e.args
            }
        )


def pan_active_loan_test(request):
    pan_number = "asdf"
    user_id = 5
    is_used = is_pancard_used_in_active_loan(pan_number, user_id)
    return HttpResponse("Done")


def is_pancard_used_in_active_loan(pan_number, user_id):
    '''
    validated if the user's pancard is user in active loans:
        active loan status= ['APPROVED','SUBMITTED','PENDING','ON_HOLD']

    :param pan_number:
    :return: Boolean
        True: if pancard is used in active loan
        False: if pancard is not used in active loan

    '''

    total_match = 0
    total_loan = 0
    total_unique_pan = 0
    pan_list = []
    user_list = []
    personal = Personal.objects.filter(pan_number=pan_number).distinct()
    for i in personal:
        if i.user_id in user_list:
            pass
        else:
            user_list.append(i.user_id)

    if user_list is not None:
        for userid in user_list:
            # Get all active loan
            user_loan = LoanApplication.objects.filter(user_id=userid,
                                                       status__in=['APPROVED', 'SUBMITTED'])
            if user_loan:
                for loans in user_loan:
                    if loans:
                        total_loan += 1
                        # To get pancard- get user_personal id from user loan
                        user_pancard = Personal.objects.filter(id=loans.user_personal_id)
                        for i in user_pancard:
                            if pan_number == i.pan_number:
                                total_match += 1
                                pan_list.append(i.pan_number)
                                return True
                            else:
                                total_unique_pan += 1

            if total_unique_pan != 0:
                return False

class pancard_used_in_active_loan_interlink(generics.ListAPIView):
    def get(self, request, *args, **kwargs):
        total_match = 0
        total_loan = 0
        total_unique_pan = 0
        pan_list = []
        user_list = []
        status = ''
        pan_number = request.data.get('pan_number')
        print(pan_number, 'pan')
        personal = Personal.objects.filter(pan_number=pan_number).distinct()
        print(personal, 'personal')
        for i in personal:
            if i.user_id in user_list:
                pass
            else:
                user_list.append(i.user_id)
        print(user_list, 'user list 2431')
        if user_list:
            print('in if user list', user_list)
            for userid in user_list:
                # Get all active loan
                user_loan = LoanApplication.objects.filter(user_id=userid, status__in=['APPROVED', 'PENDING'])
                
                print(user_loan, 'user loans')
                if user_loan:
                    for loans in user_loan:
                        if loans:
                            total_loan += 1
                            # To get pancard- get user_personal id from user loan
                            user_pancard = Personal.objects.filter(id=loans.user_personal_id)
                            for i in user_pancard:
                                if pan_number == i.pan_number:
                                    # print("++Match=====", pan_number, i.pan_number)
                                    total_match += 1
                                    pan_list.append(i.pan_number)
                                    print('response true')
                                    status = True
                                    return Response({"status":status, "message":"User has already taken Loan."})
                                    
                                else:
                                    total_unique_pan += 1
                else:
                    status = False
                    return Response({"status":status, "message":"No Approved or Pending Loan Found."})
            if total_unique_pan != 0:
                print('response total_unique_pan')
                status = False
                return Response({"status":status, "message":"Pan did not matched"})
        else:
            print('in else',user_list)
            status = False
            return Response({"status":status, "message":"No user with this pancard"})

class Loan_Drop_Down_Option(APIView):
    """
        User loan Amount List

        :parameter:

            API to view the loan amount list for user loan application

            Method GET

            Response

            {
                'user_loans_details': [{}]
            }

            Example:
                {
                    "status": "1",
                    "drop_down_option": [
                        {
                            "ms_amount": "1000",
                            "ms_pf": "50",
                            "ms_interest": "50",
                            "ms_net_loan": "900",
                            "ms_tenure": "10",
                            "ms_edi": "100",
                            "ms_pf_percentage": "5",
                            "ms_score": "0"
                        }
                    ]
                }
            #

            Method : POST
            :param
            {
                "ms_amount": "10000"
            }

            :returns
            {
                "status": "1",
                "message": "Details Found",
                "drop_down_option": [
                    {
                        "ms_amount": "10000",
                        "ms_pf": "500",
                        "ms_interest": "500",
                        "ms_net_loan": "9000",
                        "ms_tenure": "20",
                        "ms_edi": "500",
                        "ms_pf_percentage": "5",
                        "ms_score": "110"
                    }
                ]
            }

            status = "1"   message = "Details Found"
            status = "2"   message = "No Details Found"
            status = "3"   message = "Something went wrong"

            Note

            Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token ",
                }
            ]
        """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        ctx = {}
        amount_limit = []
        call_url = None

        loan_limit_set = AutoApproved.objects.filter(user_id=request.user.id).order_by('-id').first()
        if loan_limit_set:
            for i in scheme:
                if loan_limit_set.option_limit >= int(i):
                    amount_limit.append(scheme[str(i)])

        else:
            for i in scheme:
                if 1000 == int(i):
                    amount_limit.append(scheme[str(i)])

        return Response(
            {
                'status': '1',
                'drop_down_option': amount_limit,
                "call_back_url": call_url,

            }
        )

    def post(self, request):
        ctx = {}
        amount_limit = []
        status = "1"
        message = "Something went wrong"
        ms_amount = int(request.data.get('ms_amount'))
        if ms_amount:
            for i in scheme:
                if ms_amount == int(i):
                    amount_limit.append(scheme[str(i)])
                    status = "1"
                    message = "Details Found"
                    break
                else:
                    status = "2"
                    message = "No Details Found"
        else:
            status = "3"
            message = "Missing Amount"

        return Response(
            {
                'status': status,
                'message': message,
                'drop_down_option': amount_limit
            }
        )


class OnHold_Loan_Time_View(APIView):
    """
        User loan Amount List

        :parameter:

            API to view the loan amount list for user loan application

            Method GET

            Response
            {
                "status": 0,
                "message": "You can apply now",
                "holddays": 0
            }

            status = "1"   message = "You can apply now"
                    - No Loan on hold or Hold Days Has been completed
            status = "2"   message = "Your Loan Application is OnHold, Please apply after  days"
                    - Loan Hold For x Days
            status = "3"   message = "Something went wrong"

            Note

            Authentication token required

            [
                {
                    "key":"Authorization",
                    "value":"Token ",
                }
            ]
        """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        status = 3
        message = "Something Went Wrong"
        holddays = 0

        ctx = get_loan_onhold_days(request.user.id)
        try:
            if ctx is not None and not ctx:
                status = ctx['status']
                message = ctx['message']
                holddays = ctx['holddays']
        except:
            pass
        return Response(
            {
                'status': status,
                'message': message,
                'holddays': holddays
            }
        )


def get_loan_onhold_days(user_id):
    status = 19
    message = "Something Went Wrong"
    holddays = 0
    ctx = {}
    try:
        user_loan = LoanApplication.objects.filter(user_id=user_id, status='ON_HOLD').order_by(
            '-id').first()
        if user_loan:
            today = datetime.datetime.today()
            start_date = user_loan.loan_start_date
            try:
                days = today - start_date
                loan_count = int(user_loan.hold_days) - days.days
            except:
                loan_count = 1
            if loan_count <= 0:
                holddays = 0
                status = 1
                message = 'You can apply now'
            else:
                holddays = loan_count
                status = 18
                message = ' Please apply after {} days'.format(holddays)
        else:
            holddays = 0
            message = 'You can apply now'
            status = 1

    except Exception as e:

        import sys
        import os
        print('-----------in exception----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)

        import requests
        sms_message = 'in loan on hold ex' + str(e.args) + '\n' + str(exc_tb.tb_lineno)
        number = '6352820504'

        sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
            settings.SMS_USERNAME) + '&password=' + str(settings.SMS_PASSWORD) + \
                  '&type=0&dlr=1&destination=' + number + '&source=' + str(
            settings.SMS_SOURCE) + '&message=' + sms_message

        requests.get(sms_url)

        # message = settings.SYSTEM_ERROR
        status = "3"
        message = "Something Went Wrong"
        holddays = 0

    ctx['status'] = status
    ctx['message'] = message
    ctx['holddays'] = holddays
    return ctx


class LoanHistory(generics.ListAPIView):
    """
        User loan history

    :parameter:

        API to view the details of user applied loan's

        Response

        {
            'user_loan_details': [{}]
        }

        Example:
            {
                "status": "1",
                "message": "Details Found",
                "Data": [
                    {
                        "loan_application_id": "19913141327606",
                        "loan_status": "SUBMITTED",
                        "loan_start_date": null,
                        "loan_end_date": null,
                        "created_date": "2019-09-13T14:13:02.553933",
                        "disbursed_amount_id": 900,
                        "repayment_amount_id": "1000",
                        "tenure": "10",
                        "dpd": null
                    },
                    {
                        "loan_application_id": "19913141327606",
                        "loan_status": "SUBMITTED",
                        "loan_start_date": null,
                        "loan_end_date": null,
                        "created_date": "2019-09-13T14:13:02.553933",
                        "disbursed_amount_id": 900,
                        "repayment_amount_id": "1000",
                        "tenure": "10",
                        "dpd": null
                    }
                ]
            }
        #
        Note

        Authentication token required

        [
            {
                "key":"Authorization",
                "value":"Token ",
            }
        ]
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        try:
            list = []
            disbursed_amount = None
            user_loans = LoanApplication.objects.filter(user_id=request.user.id).order_by("-id")

            if user_loans:
                for i in user_loans:
                    dict = {}

                    try:
                        if i.status == "APPROVED" or i.status == "COMPLETED":
                            if i.disbursed_amount:
                                disbursed_amount = i.disbursed_amount.all()
                            else:
                                disbursed_amount = None
                            dict['loan_application_id'] = i.loan_application_id
                            dict['loan_status'] = i.status
                            dict['loan_start_date'] = convert_date_into_readable_format(i.loan_start_date)
                            dict['loan_end_date'] = convert_date_into_readable_format(
                                i.loan_end_date)
                            dict['created_date'] = convert_date_into_readable_format(
                                i.created_date)

                            dict['repayment_amount'] = get_repayment_amount(i)
                            dict['tenure'] = get_tenure(i)
                            dict['dpd'] = i.dpd
                        else:
                            dict['loan_application_id'] = i.loan_application_id
                            dict['loan_status'] = i.status
                            dict['loan_start_date'] = convert_date_into_readable_format(i.loan_start_date)
                            dict['created_date'] = convert_date_into_readable_format(
                                i.created_date)
                            dict['loan_end_date'] = None
                            dict['disbursed_amount_id'] = None
                            dict['repayment_amount_id'] = get_repayment_amount(i)
                            dict['tenure'] = get_tenure(i)
                            dict['dpd'] = i.dpd

                        list.append(dict)

                    except Exception as e:
                        # TODO raise
                        import sys
                        import os
                        print('-----------in exception----------')
                        print(e.args)
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                        print(exc_type, fname, exc_tb.tb_lineno)

                        return Response(
                            {
                                'status': '3',
                                'message': 'Somenthin went wrong',
                            }
                        )

                    return Response(
                        {
                            'status': '1',
                            'message': 'Details Found',
                            'Data': list
                        }
                    )
            else:
                return Response(
                    {
                        'status': '2',
                        'message': 'No Details Found',
                    }
                )
        except exceptions as e:
            print(e)
            return Response({'status': 'error'})


def get_repayment_amount(loan_obj):
    repayment_amount = None
    if loan_obj:
        for j in scheme:
            if int(j) == int(float(loan_obj.loan_scheme_id)):
                repayment_amount = scheme[j]['ms_amount']
                break
    return repayment_amount


def get_tenure(loan_obj):
    tenure = None
    if loan_obj:
        for j in scheme:
            if int(j) == int(float(loan_obj.loan_scheme_id)):
                tenure = scheme[j]['ms_tenure']
                break
    return tenure


class LoanDetails(generics.ListAPIView):
    """
        User loan Detail

    :parameter:

        API to view the details of user applied loan's

        Response

        {
            'user_loans_details': [{}]
        }

        Example:
            {
                "status": "1",
                "message": "Details Found",
                "Data": [
                    {
                        "loan_application_id": "19971724412532",
                        "loan_status": "APPROVED",
                        "loan_start_date": null,
                        "loan_end_date": null,
                        "created_date": "2019-09-07T17:24:41.134000",
                        "disbursed_amount_id": 900,
                        "repayment_amount_id": "1000",
                        "tenure": "10",
                        "dpd": null
                    }
                ]
            }
        #
        Note

        Authentication token required

        [
            {
                "key":"Authorization",
                "value":"Token ",
            }
        ]
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    # serializer_class =

    def post(self, request, *args, **kwargs):
        # try:

        dict = {}
        list = []
        loan_status = 0
        loan_status_message = None

        user_id = request.user.id
        loan = LoanApplication.objects.filter(user_id=user_id).last()
        if loan:
            dict['loan_application_id'] = loan.loan_application_id
            dict['loan_status'] = loan.status
            if loan.status == "APPROVED":
                loan_status = 1
                loan_status_message = loan.status
            elif loan.status == "SUBMITTED" or loan.status == "PRE_APPROVED" and loan.status == "PROCESS":
                loan_status = 2
                loan_status_message = loan.status
            elif loan.status == "PENDING" or loan.status == "ON_HOLD":
                loan_status = 3
                loan_status_message = loan.status
            else:
                loan_status = 4
                loan_status_message = loan.status

            return Response(
                {
                    'status': loan_status,
                    'message': loan_status_message,
                    'Data': dict
                }
            )
        else:

            return Response(
                {
                    'status': 0,
                    'message': 'No Details Found',

                }
            )


class LoanApplicationConfirmation(APIView):
    """
    To confirm user loan application

    :parameter:

        API To confirm user loan applications and to check user loan profile is completed

        Response

        {
            "status": "CharField",
            "message": "CharField",
            "rate_of_interest": "CharField",
            "formula": "CharField",
            "name": "CharField",
            "bank_name": "CharField",
            "account_number": "CharField",
            "note": "CharField"
        }

        Note

        Authentication token required

        [
            {
                "key":"Authorization",
                "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
            }
        ]

        status 1 : Ok
        status 2 : In-complete loan profile
        status 10 : Something went wrong (Django Error)

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):

        status = None
        message = None
        loan_amount = None
        rate_of_interest = None
        repayment_amount = None
        name = None
        roi = None
        formula = None
        bank_name = None
        account_number = None
        amount = request.data.get('amount')
        tenure = request.data.get('tenure')
        mudra_score_master = []
        loan_amount_serializer = amount
        terms_and_condition = ''

        try:
            user_instance = User.objects.get(id=request.user.id)

            # TODO check user all profiles are complete or not

            roi = 15
            status = '1'
            message = 'Ok'
            rate_of_interest = roi
            name = 'Test'
            bank_name = 'Test'
            account_number = '12345'


        except Exception as e:

            status = '10'
            message = str(e.args)

        return Response(
            {
                'status': status,
                'message': message,
                'rate_of_interest': roi,
                'name': name,
                'bank_name': bank_name,
                'account_number': account_number,
                'mudra_score_master': mudra_score_master,
                'note': terms_and_condition
            }
        )


class LoanRepayment(APIView):
    """
    get user loan payments details
        loan repayment
        subscription
        loan disbursment

    :parameter:

        get user repayments details

        Response

        {
            "status": "CharField",
            "message": "CharField",
            "rate_of_interest": "CharField",
            "formula": "CharField",
            "name": "CharField",
            "bank_name": "CharField",
            "account_number": "CharField",
            "note": "CharField"
        }

        Note

        STATUS_TYPE = (
            ("initiated", "initiated"),
            ("success", "success"),
            ("failed", "failed"),
            ("cancelled", "cancelled"),
            ("cancelled_m", "cancelled manually"),
            ("pending", "pending"),
        )
        PAYMENT_MODE = (
            ('NB', 'Net Banking'),
            ('DC', 'Debit Card'),
            ('CC', 'Credit Card'),
            ('UPI', 'UPI'),
            ('PPI', 'Wallet'),
            ('C', 'Cash'),
            ('NA', 'Cancel'),
        )

        PAYMENT_CTG = (
            # Payment Category
            ('Rbl', 'Rbl'),
            ('Subscription', 'Subscription'),
            ('Loan', 'Loan'),

        )
        PAYMENT_Type = (
            # Payment Type
            ('Cash', 'Cash'),
            ('Account', 'Account'),
            ('App', 'App'),
            ('On_Us', 'On_Us'),

        )


        Authentication token required

        [
            {
                "key":"Authorization",
                "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
            }
        ]

        status 1 : Ok
        status 2 : In-complete loan profile
        status 10 : Something went wrong (Django Error)

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        payment = None
        ctx = {}

        loan_id = request.data.get('loan_application_id')
        try:
            pay_list = []
            user_loan = LoanApplication.objects.filter(loan_application_id=loan_id).first()
            if user_loan:
                # Getting data from Many to many Field
                try:
                    payment = user_loan.repayment_amount.all()
                except Exception as e:
                    print(e)
                if payment:
                    for i in payment:
                        pay_list.append(
                            {
                                'status': '1',
                                'message': 'User Details Found',
                                'order_id': i.order_id,
                                'type': i.type,
                                'category': i.category,
                                'mode': i.mode,
                                'amount': i.amount,
                                'date': i.date,
                                'transaction_status': i.status
                            }
                        )
                else:
                    pass
                if pay_list:
                    ctx = {
                        'status': '1',
                        'message': 'User Details Found'
                    }
                    ctx['data'] = pay_list
                    return Response(ctx)
                else:
                    return Response(
                        {
                            'status': '2',
                            'message': 'No Details Found',

                        }
                    )
            else:
                return Response(
                    {
                        'status': '2',
                        'message': 'No Details Found',

                    }
                    # dummy_data
                )

        except Exception as e:
            print("-------exc repayemnt", e)
            return Response(
                {
                    'status': '10',
                    'message': 'Somenting Went Wrong',

                }
            )


class UserRepayment(APIView):
    """
    get user payments details
        loan repayment
        subscription
        loan disbursment

    :parameter:

        Method: POST
        Response

        {
           "status": "1",
            "message": "User Details Found",
            "order_id": "3",
            "type": "Account",  choice field
            "category": "Loan", choice field
            "mode": "NB",       choice field
            "amount": 100,
            "date": "2019-09-07T17:24:41.134752",   Datefield
            "transaction_status": "success"         choice field
        }

        Note

        STATUS_TYPE = (
            ("initiated", "initiated"),
            ("success", "success"),
            ("failed", "failed"),
            ("cancelled", "cancelled"),
            ("cancelled_m", "cancelled manually"),
            ("pending", "pending"),
        )
        PAYMENT_MODE = (
            ('NB', 'Net Banking'),
            ('DC', 'Debit Card'),
            ('CC', 'Credit Card'),
            ('UPI', 'UPI'),
            ('PPI', 'Wallet'),
            ('C', 'Cash'),
            ('NA', 'Cancel'),
        )

        PAYMENT_CTG = (
            # Payment Category
            ('Rbl', 'Rbl'),
            ('Subscription', 'Subscription'),
            ('Loan', 'Loan'),

        )
        PAYMENT_Type = (
            # Payment Type
            ('Cash', 'Cash'),
            ('Account', 'Account'),
            ('App', 'App'),
            ('On_Us', 'On_Us'),

        )


        Authentication token required

        [
            {
                "key":"Authorization",
                "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
            }
        ]

        status 1 : Ok
        status 2 : In-complete loan profile
        status 10 : Something went wrong (Django Error)

           # dummy_data = {
        #     "status": "1",
        #     "message": "User (Dummy) Details Found",
        #     "data": [
        #         {
        #             "order_id": "1",
        #             "type": "Account",
        #             "category": "Rbl",
        #             "mode": "NB",
        #             "amount": 900,
        #             "date": "2019-09-07T17:24:41.134752",
        #             "transaction_status": "success"
        #         },
        #         {
        #             "order_id": "1",
        #             "type": "Account",
        #             "category": "Rbl",
        #             "mode": "NB",
        #             "amount": 900,
        #             "date": "2019-09-07T17:24:41.134752",
        #             "transaction_status": "success"
        #         },
        #         {
        #             "order_id": "test",
        #             "type": "Loan",
        #             "category": "Loan",
        #             "mode": "NB",
        #             "amount": 100,
        #             "date": None,
        #             "transaction_status": "success"
        #         },
        #         {
        #             "order_id": "test",
        #             "type": "Loan",
        #             "category": "Loan",
        #             "mode": "NB",
        #             "amount": 100,
        #             "date": None,
        #             "transaction_status": "initiated"
        #         }
        #     ]
        # }

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        user_id = request.user.id
        ctx = {}

        try:
            pay_list = []
            user_loan = LoanApplication.objects.filter(user_id=user_id)
            if user_loan:
                for loan in user_loan:
                    # Getting data from Many to many Field
                    payment = loan.repayment_amount.all()
                    if payment:
                        for i in payment:
                            pay_list.append(
                                {

                                    'loan_application_id': loan.loan_application_id,
                                    'order_id': i.order_id,
                                    'type': i.type,
                                    'category': i.category,
                                    'mode': i.mode,
                                    'amount': i.amount,
                                    'date': i.date,
                                    'transaction_status': i.status
                                }
                            )

                if pay_list:
                    ctx = {'status': '1', 'message': 'User Details Found', 'data': pay_list}
                    return Response(ctx)
                else:
                    return Response(
                        {
                            'status': '2',
                            'message': 'No Details Found',

                        }

                    )

            else:
                return Response(
                    {
                        'status': '2',
                        'message': 'No Details Found',

                    }
                )

        except Exception as e:
            print("-------exc repayemnt", e)
            return Response(
                {
                    'status': '10',
                    'message': 'Somenting Went Worng',

                }
            )


from django.apps import apps


class IsUserDetailsComplete(APIView):
    """
    checks if the user all details are complete or not

    :parameter
        Auth Token

    :Note

    status = '1' : All User Details are complete
        message = "All User Details are complete"
        detail = None


    status = '2' : Details are incomplete
        message: personal details is incomplete
        detail = "personal"

    status = '3' : Something went wrong
        detail = None

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        user_id = request.user.id

        status = "3"
        message = "Someting went wrong"
        detail = None

        try:
            app = apps.get_app_config('User')
            models = app.models

            for name, model in models.items():
                # model._meta.db_table  # Get the name of the model in the database.json
                user = User.objects.get(id=user_id)
                obj = model(user=user)
                data = obj.is_completed()
                if not data:
                    status = "2"
                    message = name + " details is incomplete"
                    detail = name

                    return Response({
                        'status': status,
                        'message': message,
                        'detail': detail

                    })
            status = "1"
            message = "All User Details are complete"
            detail = None
            return Response({
                'status': status,
                'message': message,
                'detail': detail
            })

        except Exception as e:
            print("+++++______Exception_____+++++++", e)

        return Response(
            {
                'status': status,
                'message': message,
                'detail': detail

            }
        )


class CurrentLoanDetail(APIView):
    """
        User Approved loan Detail

    :parameter:

        API to view the User's Approved loan Detail

        Response

        {
            'user_loan_details': [{}]
        }

        Example:
            {
                "status": "1",
                "message": "Details Found",
                "Data": [
                    {
                        "loan_application_id": "19913141327606",
                        "loan_status": "SUBMITTED",
                        "loan_start_date": null,
                        "loan_end_date": null,
                        "created_date": "2019-09-13T14:13:02.553933",
                        "disbursed_amount_id": 900,
                        "repayment_amount_id": "1000",
                        "tenure": "10",
                        "dpd": null
                    },
                    {
                        "loan_application_id": "19913141327606",
                        "loan_status": "SUBMITTED",
                        "loan_start_date": null,
                        "loan_end_date": null,
                        "created_date": "2019-09-13T14:13:02.553933",
                        "disbursed_amount_id": 900,
                        "repayment_amount_id": "1000",
                        "tenure": "10",
                        "dpd": null
                    }
                ]
            }
        #
        Note

        Authentication token required

        [
            {
                "key":"Authorization",
                "value":"Token ",
            }
        ]
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        try:
            dict = {}
            list = []
            user_id = request.user.id

            get_user_approved_loan = LoanApplication.objects.filter(
                user_id=user_id,
                status="APPROVED").first()
            if get_user_approved_loan:
                get_total_amount_paid = get_user_approved_loan.repayment_amount.filter(status="success").aggregate(
                    Sum('amount'))

                if get_user_approved_loan.disbursed_amount is not None:
                    get_disbursed_amount = get_user_approved_loan.disbursed_amount.amount
                else:
                    get_disbursed_amount = 0
                get_total_amount_paid = get_total_amount_paid['amount__sum']

                if get_total_amount_paid is None:
                    get_total_amount_paid = 0

                for j in scheme:
                    loan_amt = int(float(get_user_approved_loan.loan_scheme_id))
                    if int(j) == int(loan_amt):
                        try:
                            dict['loan_application_id'] = get_user_approved_loan.loan_application_id
                            dict['loan_status'] = get_user_approved_loan.status
                            dict['loan_start_date'] = get_user_approved_loan.loan_start_date
                            dict['loan_end_date'] = get_user_approved_loan.loan_end_date
                            dict['created_date'] = get_user_approved_loan.created_date
                            dict['disbursed_amount_id'] = get_disbursed_amount
                            dict['repayment_amount_id'] = scheme[j]['ms_amount']
                            dict['paid_amount'] = get_total_amount_paid
                            dict['remaining_amount'] = float(scheme[j]['ms_amount']) - float(get_total_amount_paid)
                            dict['tenure'] = scheme[j]['ms_tenure']
                            dict['dpd'] = get_user_approved_loan.dpd

                            list.append(dict)
                        except Exception as e:
                            # TODO raise
                            print(e)
                            pass
                return Response(
                    {
                        'status': '1',
                        'message': 'Details Found',
                        'Data': list

                    }
                )
            else:
                return Response(
                    {
                        'status': '2',
                        'message': 'No Details Found',
                    }
                )
        except exceptions as e:
            print(e)
            return Response({'status': 'error'})


class LoanSlabDetails(generics.ListAPIView):
    """
    Show Defaulter Loan Slab Details
     Parameter
            {
                "loan_application_id": "19720115804499",
            }
        Response
            {
             "data": {'date': '2021-01-10', 'time': '17:27:32', 'approved_amount': 667, 'remaining_amount': 667, 'pay_status': 'unpaid', 'is_overdue': 'Yes'}  # if loan_application_id found
            }
             {
             "message": "Data Not Found"  # if slab not found
            }
             {
             "message": "Loan Application Not Found"  # if loan_application_id  not found
            }
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        data_dict = {}
        data_list = []
        user_id = request.user
        loan_obj = LoanApplication.objects.filter(user_id=user_id).order_by("-id")
        if loan_obj:
            for loan in loan_obj:
                data_dict = {
                    "loan_id": loan.loan_application_id,
                    "date": str(loan.created_date)[:11],
                    "loan_status": loan.status
                }
                data_list.append(data_dict)
            return Response({
                "status": "1",
                "data": data_list
            })
        else:
            return Response({
                "status": "2",
                "message": "No Data Found"
            })

    def post(self, request):
        user_id = request.user
        request_data = request.data
        total_amount = 0
        repay_amount = 0
        balance_amount = 0
        wallet_balance_amount = 0
        if request_data['loan_application_id'] != None:
            loan_application_id = request_data['loan_application_id']
            loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id, status="APPROVED",
                                                      user_id=user_id).first()
            if loan_obj:
                if loan_obj.approved_amount:
                    user_wallet = UserWallet.objects.filter(user_id=user_id).exclude(status="initiated").last()
                    total_amount = loan_obj.approved_amount
                    repay_amount = loan_paid_amount(loan_obj.id)
                    balance_amount = total_amount - repay_amount
                    wallet_balance_amount = user_wallet.balance_amount
                slab_data = loan_amount_slab(loan_obj.id)
                if slab_data:
                    return Response(
                        {
                            "status": "1",
                            'data': slab_data,
                            "total_amount": total_amount,
                            "paid_amount": repay_amount,
                            "balance_amount": balance_amount,
                            "wallet_balance_amount": wallet_balance_amount
                        }
                    )
                return Response(
                    {
                        "status": "2",
                        'message': "Data Not Found"
                    }
                )
            else:
                return Response(
                    {
                        "status": "3",
                        'message': "Loan Application Not Found"
                    }
                )
        else:
            return Response(
                {
                    "status": "4",
                    'message': "Data Not Found"
                }
            )


def check_loan_user_order_details(order_id, user_id):
    """
     get order details id  of user
    """
    order_details_obj = OrderDetails.objects.filter(user_id=user_id, credit_status="initiated").exclude(
        status="initiated").distinct()

    if order_details_obj:
        return check_loan_user_wallet_amount(order_id, order_details_obj, user_id)


def check_loan_user_wallet_amount(prev_order_id, order_details_obj, user_id):
    """
       create entry in user wallet according to order detail id
    """

    if order_details_obj:

        for order_details in order_details_obj:

            actual_used_amount = order_details.credit.actual_used
            order_repayment_add = \
                order_details.credit_history.filter(user_id=user_id, status__in=['repayment', 'order_cancel',
                                                                                 'order_undelivered',
                                                                                 'order_return', 'refund']).aggregate(
                    Sum('repayment_add'))['repayment_add__sum']
            order_id = order_details.order_text
            if order_id != prev_order_id:
                order_list = []
                order_list.append(order_id)
                order_used_credit = 0

                amount_add = 0
                new_balance_amount = 0
                wallet = 0

                if order_repayment_add:
                    remaining_amount = actual_used_amount - order_repayment_add
                    wallet = UserWallet.objects.filter(user_id=user_id).exclude(status="initiated").last()
                    new_balance_amount = wallet.balance_amount - remaining_amount
                    if new_balance_amount >= 0:
                        amount_add = 0
                        user_wallet = UserWallet.objects.create(amount_add=amount_add, used_amount=remaining_amount,
                                                                balance_amount=new_balance_amount, status="repayment",
                                                                order_details_id=wallet.order_details_id,
                                                                user_id=user_id)
                        change_loan_payment_amount_status(order_id, user_id, remaining_amount)

                    else:
                        amount_add = 0

                        user_wallet = UserWallet.objects.create(amount_add=amount_add,
                                                                used_amount=wallet.balance_amount,
                                                                balance_amount=0,
                                                                status="repayment",
                                                                order_details_id=wallet.order_details_id,
                                                                user_id=user_id)
                        remaining_amount = wallet.balance_amount
                        change_loan_payment_amount_status(order_id, user_id, remaining_amount)

                else:
                    pass
            else:
                pass

        else:
            pass
    else:
        pass


def change_loan_payment_amount_status(order_id, user_id, remaining_amount):
    """
      create entry in payment after creating entry in userwallet when status changes to repayment

    """
    total_credit = 0
    import random
    random_number = random.sample(range(99999), 1)
    ORDER_ID = str(order_id) + 'e' + str(random_number[0])
    payment_obj = Payment.objects.create(order_id=ORDER_ID, status="success", category="Repayment", type="Ecom_Wallet",
                                         amount=remaining_amount, product_type="Ecom")
    order_details_obj = OrderDetails.objects.filter(order_text=order_id)
    payment_id = payment_obj.id
    if order_details_obj:
        payment_data_obj = Payment.objects.filter(id=payment_id)
        if payment_data_obj:
            order_details_obj[0].payment.add(*payment_data_obj)

    return change_credit_status(order_id, user_id, remaining_amount)


def change_credit_status(order_id, user_id, remaining_amount):
    """
      update user used credit and balance credit after entry in payment
    """

    credit = Credit.objects.filter(user_id=user_id).last()
    if credit:
        total_credit = credit.credit
        new_user_used_credit = credit.used_credit - remaining_amount
        new_user_balance_credit = credit.balance_credit + remaining_amount
        if new_user_used_credit <= 0:
            new_user_used_credit = 0
            new_user_balance_credit = 0

        credit_obj = Credit.objects.create(user_id=user_id, credit=total_credit, used_credit=new_user_used_credit,
                                           balance_credit=new_user_balance_credit,
                                           repayment_add=remaining_amount, status="repayment")

        order_details_obj = OrderDetails.objects.filter(order_text=order_id)
        credit_id = credit_obj.id
        if order_details_obj:
            credit_obj = Credit.objects.filter(id=credit_id)
            if credit_obj:
                order_details_obj[0].credit_history.add(*credit_obj)
            else:
                pass
        else:
            pass
    else:
        pass


import os


def user_loan_agreement(loan_application_id, final_approved_amount, user_id):
    ctx = {}

    loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
    data_dict = {}
    if loan_obj:
        for i in loan_obj:
            startdate = i.loan_start_date
            date = startdate.day
            month = startdate.month
            year = startdate.year

            ctx['approved_amount'] = final_approved_amount
            data_list = [date, month, year]
            ctx["database"] = data_list
            name = i.user_personal.name
            pan_number = i.user_personal.pan_number
            address = i.current_address.address
            city = i.current_address.city
            state = i.current_address.state
            pin_code = i.current_address.pin_code

            ctx["personal"] = [name, pan_number, address, city, state, pin_code]
            from django.template import loader
            html = loader.render_to_string('Ecom/loan_agreement.html', ctx)

            path = "media/loan_applications"
            isdir = os.path.isdir(path)
            if isdir == True:
                pass
            if isdir == False:
                os.makedirs(path)
            personal_name = i.user_personal.name.replace(" ", "_")

            pdf_name = i.user.username + "_" + personal_name + "_" + str(loan_application_id) + ".pdf"
            try:
                import pdfkit
                from django.template.loader import get_template
                # pdfkit.from_string(html, pdf_url)
                template = get_template("Ecom/loan_agreement.html").render(ctx)
                base = "media/loan_applications" + "/" + str(pdf_name)
                pdfkit.from_string(template, base)
                pdf = open(base)
                response = HttpResponse(pdf.read(),
                                        content_type='application/pdf')  # Generates the response as pdf response.
                response['Content-Disposition'] = 'attachment; filename=' + base
                pdf.close()

                return response

            except Exception as e:

                import sys
                import os
                print('-----------in exception----------')
                print(e.args)
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                import requests
                sms_message = 'pdf import pdfkit api producation ex' + str(e.args) + '\n' + str(exc_tb.tb_lineno)
                number = '6352820504'

                sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
                    settings.SMS_USERNAME) + '&password=' + str(settings.SMS_PASSWORD) + \
                          '&type=0&dlr=1&destination=' + number + '&source=' + str(
                    settings.SMS_SOURCE) + '&message=' + sms_message

                requests.get(sms_url)

            try:

                new_file_path = "media/loan_applications/" + str(pdf_name)
                # new_file_path="loan_applications/"+str(pdf_name)
                from .botofile import upload_image
                upload_image(base, new_file_path)
                # os.remove(base)

            except Exception as e:
                import sys
                import os
                print('-----------in exception----------')
                print(e.args)
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                import requests
                sms_message = 'pdf upload api producation ex' + str(e.args) + '\n' + str(exc_tb.tb_lineno)
                number = '6352820504'

                sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
                    settings.SMS_USERNAME) + '&password=' + str(settings.SMS_PASSWORD) + \
                          '&type=0&dlr=1&destination=' + number + '&source=' + str(
                    settings.SMS_SOURCE) + '&message=' + sms_message

                requests.get(sms_url)

            loan_acknowledgement_obj = LoanAcknowledgement.objects.filter(loan_transaction_id=i.id, user_id=i.user.id,
                                                                          acknowledgement_pdf=new_file_path, sign=name)
            if not loan_acknowledgement_obj:
                LoanAcknowledgement.objects.create(loan_transaction_id=i.id, user_id=i.user.id,
                                                   acknowledgement_pdf=new_file_path, sign=name)
    return True


class LoanApplicationDisbursedAmountWalletApi(generics.ListAPIView):
    """
       Change credit status according to user loan status



           Response

           {
            "Message": [
                {
                    "loan_application_id": "199058425",
                    "message": "Successfully user credit status changed"
                }
            ]
        }


       """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        data_dict = {}
        user_id = request.user.id
        request_data = request.data
        if request_data:
            loan_application_id = request_data["loan_application_id"]
            user_status = request_data["user_status"]

            loan_application_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id,
                                                                  status="APPROVED", user_id=user_id)

            if loan_application_obj:
                approved_amount = loan_application_obj[0].approved_amount
                if user_status == "Reject":
                    update_loan = LoanApplication.objects.filter(id=loan_application_obj[0].id,
                                                                 loan_application_id=loan_application_id).update(
                        user_status=user_status, status="REJECTED")
                    data_dict = {
                        "status": "1",
                        "loan_application_id": loan_application_obj[0].loan_application_id,
                        "user_status": user_status,
                        "message": "user status update successfully"

                    }
                elif user_status == "Accept":
                    if approved_amount:
                        final_approved_amount = int(approved_amount * 90) / 100

                        today = datetime.today()
                        new_date = today + timedelta(90)
                        LoanApplication.objects.filter(
                            loan_application_id=loan_application_obj[0].loan_application_id).update(
                            loan_start_date=today, loan_end_date=new_date, user_status=user_status)
                        try:
                            user_loan_agreement(loan_application_obj[0].loan_application_id, approved_amount, user_id)
                        except:
                            pass
                        order_id = None
                        if not loan_application_obj[0].disbursed_amount_wallet:

                            balance_amount = UserWallet.objects.filter(user_id=user_id).exclude(
                                status="initiated").last()
                            # order_id = None
                            if balance_amount:
                                final_balance_amount = final_approved_amount + balance_amount.balance_amount

                                user_wallet = UserWallet.objects.create(amount_add=final_approved_amount,
                                                                        balance_amount=final_balance_amount,
                                                                        status="loan_disbursed",
                                                                        user_id=balance_amount.user_id)
                                data_dict = {
                                    "status": "2",
                                    "loan_application_id": loan_application_obj[0].loan_application_id,
                                    "user_status": user_status,
                                    "message": "status changed successfully"

                                }
                            else:
                                user_wallet = UserWallet.objects.create(amount_add=final_approved_amount,
                                                                        balance_amount=final_approved_amount,
                                                                        status="loan_disbursed",
                                                                        user_id=user_id)
                                data_dict = {
                                    "status": "3",
                                    "loan_application_id": loan_application_obj[0].loan_application_id,
                                    "message": "wallet entry create successfully"
                                }
                            LoanApplication.objects.filter(id=loan_application_obj[0].id).update(
                                disbursed_amount_wallet=user_wallet.id)
                            check_order_id = check_loan_user_order_details(order_id, loan_application_obj[0].user_id)



                        else:
                            check_order_id = check_loan_user_order_details(order_id, loan_application_obj[0].user_id)
                            data_dict = {
                                "status": "4",
                                "loan_application_id": loan_application_obj[0].loan_application_id,
                                "message": "Wallet entry already exist"
                            }
                    else:
                        data_dict = {
                            "status": "9",
                            "loan_application_id": loan_application_obj[0].loan_application_id,
                            "message": "No Approved amount wallet available"
                        }
                        return Response(
                            {
                                "data": data_dict,

                            }
                        )

                else:
                    data_dict = {
                        "status": "5",
                        "loan_application_id": loan_application_obj[0].loan_application_id,
                        "user_status": user_status,
                        "message": "user status not matched"

                    }
                return Response(
                    {
                        "data": data_dict

                    }
                )

            else:

                return Response(
                    {
                        'status': "9",
                        'message': "No approved loan application",

                    }
                )

        else:

            return Response(
                {
                    'status': '9',
                    'message': "No data available",

                }
            )


def check_approved_amount(loan_id):
    loan_data = LoanApplication.objects.filter(id=loan_id)
    if loan_data:
        for loan in loan_data:
            approved_amount = loan.approved_amount
            if approved_amount:
                final_approved_amount = int(approved_amount * 50) / 100

                return final_approved_amount
            else:
                approved_amount = 0
                final_approved_amount = approved_amount

                return final_approved_amount


def check_balance_amount(loan):
    data_dict = {}
    disbursed_amount = loan.disbursed_amount.filter(status="success", category="Rbl")
    final_amount = 0
    new_final_approved_amount = check_approved_amount(loan.id)
    if disbursed_amount:

        for amount in disbursed_amount:
            amount_data = Payment.objects.filter(id=amount.id).aggregate(Sum('amount'))['amount__sum']

            final_amount += amount_data
        data_dict['final_amount'] = final_amount

        if final_amount:
            first_balance_amount = new_final_approved_amount - final_amount
            user_wallet = UserWallet.objects.filter(user_id=loan.user_id).exclude(status="initiated").last()
            data_dict["last_balance_amount"] = user_wallet.balance_amount

            if user_wallet:
                new_final_balance_amount = first_balance_amount - user_wallet.balance_amount
                if new_final_balance_amount >= 0:
                    balance_amount = user_wallet.balance_amount
                    data_dict['balance_amount'] = balance_amount
                else:
                    first_balance_amount = first_balance_amount
                    data_dict['balance_amount'] = first_balance_amount
            else:
                pass
        else:
            pass
    else:
        user_wallet = UserWallet.objects.filter(user_id=loan.user_id).exclude(status="initiated").last()
        data_dict["last_balance_amount"] = user_wallet.balance_amount

        if user_wallet:
            new_final_balance_amount = new_final_approved_amount - user_wallet.balance_amount
            if new_final_balance_amount >= 0:
                balance_amount = user_wallet.balance_amount
                data_dict['balance_amount'] = balance_amount
                data_dict['final_amount'] = balance_amount
            else:
                first_balance_amount = new_final_approved_amount
                data_dict['balance_amount'] = first_balance_amount
                data_dict['final_amount'] = first_balance_amount

    return data_dict


def adjust_loan_repayment_amount(loan_amount, loan):
    if loan_amount:
        for loan_amount in loan_amount:
            if loan_amount['is_overdue'] == 'Yes':
                payment_amount = loan_amount['remaining_amount']
                loan_application_id = loan.loan_application_id
                import random
                random_number = random.sample(range(99999), 1)
                ORDER_ID = str(loan_application_id) + 'l' + str(random_number[0])
                loan_balance_amount = loan_amount['approved_amount'] - loan_amount['remaining_amount']
                amount_add = 0
                used_amount = 0
                user_wallet = UserWallet.objects.filter(user=loan.user).exclude(status="initiated").last()
                if user_wallet:
                    user_balance_amount = user_wallet.balance_amount
                    if user_balance_amount >= loan_balance_amount:
                        loan_user_balance_amount = user_balance_amount - loan_balance_amount
                    else:
                        loan_user_balance_amount = 0
                        payment_amount = loan_balance_amount

                    user_obj = UserWallet.objects.create(
                        status="loan_repayment_adjust",
                        amount_add=amount_add, used_amount=payment_amount, balance_amount=loan_user_balance_amount,
                        order_details_id=user_wallet.order_details_id, user_id=user_wallet.user_id)

                    payment_obj = Payment.objects.create(order_id=ORDER_ID,
                                                         status="success",
                                                         category="Loan_Repayment",
                                                         type="loan",
                                                         amount=payment_amount,
                                                         product_type="Ecom")

                    if payment_obj:
                        loan.repayment_amount.add(payment_obj)

        return True


class LoanApplicationWithdrawApi(generics.ListAPIView):
    """
          Check amount withdraw limit

          parameters :

		{
		    "amount" : 1000
		}



              Response

               response:
             {
                "Status" : 1,
                "message": "Success"

             }


          """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        user_id = request.user.id
        status = None
        data_dict = {}
        data_list = []

        laon_application_obj = LoanApplication.objects.filter(user_status="Accept", status="APPROVED", user__id=user_id)

        if laon_application_obj:
            for loan in laon_application_obj:
                loan_id = loan.id
                new_final_approved_amount = check_approved_amount(loan_id)
                used_amount = check_balance_amount(loan)

                if 'final_amount' in used_amount:
                    final_used_amount = used_amount['final_amount']
                    balance_amount = used_amount['balance_amount']
                    status = "1"
                    data_dict = {
                        "status": "1",
                        "message": "Max up to" + " " + str(new_final_approved_amount) + "" + " is amount given",
                        "used_amount": final_used_amount,
                        "balance_amount": balance_amount
                    }
                    data_list.append(data_dict)
                else:
                    status = "10"
                    data_dict = {

                        "Status": "10",
                        "message": "No used amount",
                    }
                    data_list.append(data_dict)
        else:
            status = "9"
            data_dict = {
                "Status": "9",

                "message": "No loan application available",

            }
            data_list.append(data_dict)
        return Response(
            {
                "Status": status,
                "Data": data_list
            }
        )

    def post(self, request, *args, **kwargs):
        data_dict = {}
        data_list = []
        request_data = request.data
        user_id = request.user.id
        final_used_amount = 0
        if request_data:
            amount = request_data['amount']
            if amount >= 500:
                laon_application_obj = LoanApplication.objects.filter(user_status="Accept", status="APPROVED",
                                                                      user__id=user_id)
                if laon_application_obj:
                    for loan in laon_application_obj:
                        loan_id = loan.id
                        defaulter_list = check_loan_default()
                        if loan_id in defaulter_list:
                            loan_amount = loan_amount_slab(loan_id)
                            loan_repayment_amount = adjust_loan_repayment_amount(loan_amount, loan)
                            new_final_approved_amount = check_approved_amount(loan_id)
                            used_amount = check_balance_amount(loan)
                            if "balance_amount" in used_amount:

                                final_used_amount = used_amount['balance_amount']
                                loan_application_id = loan.loan_application_id
                                balance_amount = used_amount["last_balance_amount"]

                                withdraw_amount = final_used_amount - amount
                                new_bal_amount = balance_amount - amount
                                if withdraw_amount >= 0:
                                    UserWallet.objects.create(amount_add=0, used_amount=amount,
                                                              balance_amount=new_bal_amount, status="disbursed",
                                                              user_id=user_id)
                                    rbl_worker(loan_application_id, amount)
                                    data_dict = {
                                        "Status": "1",
                                        "message": "Transaction Success"
                                    }
                                    data_list.append(data_dict)
                                else:
                                    data_dict = {
                                        "Status": "2",
                                        "message": "Max up to" + " " + str(withdraw_amount),
                                    }
                                    data_list.append(data_dict)
                            else:
                                data_dict = {
                                    "Status": "9",
                                    "message": "No used amount",
                                }
                                data_list.append(data_dict)
                        else:
                            used_amount = check_balance_amount(loan)
                            if "balance_amount" in used_amount:

                                final_used_amount = used_amount['balance_amount']
                                loan_application_id = loan.loan_application_id
                                balance_amount = used_amount["last_balance_amount"]

                                withdraw_amount = final_used_amount - amount
                                new_bal_amount = balance_amount - amount
                                if withdraw_amount >= 0:
                                    UserWallet.objects.create(amount_add=0, used_amount=amount,
                                                              balance_amount=new_bal_amount, status="disbursed",
                                                              user_id=user_id)
                                    rbl_worker(loan_application_id, amount)
                                    data_dict = {
                                        "Status": "1",
                                        "message": "Success"
                                    }
                                    data_list.append(data_dict)

                                else:
                                    data_dict = {
                                        "Status": "2",
                                        "message": "Max up to" + " " + str(withdraw_amount),
                                    }
                                    data_list.append(data_dict)
                            else:
                                data_dict = {
                                    "Status": "9",
                                    "message": "No used amount",
                                }
                                data_list.append(data_dict)
                else:
                    data_dict = {
                        "Status": "9",
                        "message": "Loan application not available",
                    }
                    data_list.append(data_dict)
            else:
                data_dict = {
                    "Status": "4",
                    "message": "Amount is less than 500",
                }
                data_list.append(data_dict)
        else:
            data_dict = {
                "Status": "9",
                "message": "No amount found",
            }
            data_list.append(data_dict)

        return Response(
            {
                "Data": data_list
            }
        )


def loan_graph_amount(request):
    return render(request, "Ecom/loan_graph_amount.html")


@csrf_exempt
def loan_graph_amount_1(request):
    ctx = {}
    status = ["APPROVED", "COMPLETED"]
    loan_obj = LoanApplication.objects.filter(status__in=status, user_status="Accept")
    all_order = 0
    dispatced_orders = 0
    return_orders = 0
    delivered_orders = 0
    cancel_orders = 0
    undelivered_orders = 0
    loan_application_id = []
    withdraw_amount = []
    approved_amount = []
    total_disbursed_amount = []
    if loan_obj:
        for loan in loan_obj:
            loan_application_id.append(loan.loan_application_id)

            approved_amount.append(loan.approved_amount)

            disbursed_amount = loan.disbursed_amount.all()
            loan_id = loan.id
            new_final_approved_amount = check_approved_amount(loan_id)
            used_amount = check_balance_amount(loan)
            if "final_amount" in used_amount:
                final_used_amount = used_amount['final_amount']

                withdraw_amount_1 = new_final_approved_amount - final_used_amount
                withdraw_amount.append(withdraw_amount_1)

            total_disbursed_amount_1 = 0
            if disbursed_amount:
                for amount in disbursed_amount:
                    total_disbursed_amount_1 += amount.amount
                    total_disbursed_amount.append(total_disbursed_amount_1)

    ctx['loan_application_id'] = loan_application_id
    ctx['approved_amount'] = approved_amount
    ctx['total_disbursed_amount'] = total_disbursed_amount
    ctx['withdraw_amount'] = withdraw_amount
    return HttpResponse(json.dumps(ctx))


class LoanBannerApi(generics.ListAPIView):

    def get(self, request, *args, **kwargs):
        banner_list = []
        import datetime

        current_date = datetime.datetime.now()

        offer_banner_obj = LoanBanner.objects.filter(type__in=["App", "Both"], status=True,
                                                     start_date__lte=current_date,
                                                     end_date__gte=current_date).order_by("priority")
        if offer_banner_obj:
            for banner in offer_banner_obj:
                image_path = ""

                if banner.path:
                    image_path = image_thumbnail(banner.path)

                banner_list.append({"path": image_path})
            return Response(
                {
                    'status': 1,
                    'data': banner_list
                }
            )
        else:
            return Response(
                {
                    'status': 2,
                    'data': banner_list
                }
            )

    def post(self, request, *args, **kwargs):
        banner_list = []
        import datetime

        current_date = datetime.datetime.now()
        type = request.data.get('type')

        offer_banner_obj = LoanBanner.objects.filter(type__in=["App", "Both"], status=True,
                                                     start_date__lte=current_date, banner_type=type,
                                                     end_date__gte=current_date).order_by("priority")
        if offer_banner_obj:
            for banner in offer_banner_obj:
                image_path = ""

                if banner.path:
                    image_path = image_thumbnail(banner.path)

                banner_list.append({"path": image_path})
            return Response(
                {
                    'status': 1,
                    'data': banner_list
                }
            )
        else:
            return Response(
                {
                    'status': 2,
                    'data': banner_list
                }
            )


class CheckDefaulterInAll(generics.ListAPIView):
    """
     description = This function defaulter status of mk, rk and instarupee by using function check_defaulter_status_mk,
     check_defaulter_status_rk and check_defaulter_status_insta.
    """

    def get(self, request):
        # return Response({"status":True})
        mobile_number = request.data.get('mobile_number')
        print(mobile_number, 'mobile number')
        get_defaulter_obj = LoanDefaulter.objects.filter(mobile_number=mobile_number)
        print(get_defaulter_obj, 'defaulter obj')
        if get_defaulter_obj:
            defaulter_obj = LoanDefaulter.objects.filter(mobile_number=mobile_number, is_defaulter=True)
            print(defaulter_obj, 'defaulter obj in if')
            if defaulter_obj:
                print('in if')
                return Response({"is_defaulter":True})
        else:
            print('in else')
            defaulter_status_mk = check_defaulter_status_mk(mobile_number)
            print(defaulter_obj_mk, 'defaulter in mk')

            defaulter_status_rk = check_defaulter_status_rk(mobile_number)
            print(defaulter_status_rk, 'defaulter in rk')

            defaulter_status_insta = check_defaulter_status_insta(mobile_number)
            print(defaulter_status_insta, 'defaulter in insta')

            defaulter_obj_mk = LoanDefaulter.objects.create(mobile_number=mobile_number, is_defaulter=defaulter_status_mk,
                                                        type="mudrakwik")
            defaulter_obj_rk = LoanDefaulter.objects.create(mobile_number=mobile_number, is_defaulter=defaulter_status_rk,
                                                        type="rupeekwik")

            defaulter_obj_insta = LoanDefaulter.objects.create(mobile_number=mobile_number, is_defaulter=defaulter_status_rk,
                                                        type="instarupee")

            if not defaulter_status_mk or defaulter_obj_rk or defaulter_obj_insta:
                # return True
                return Response({"is_defaulter":True})
            else:
                # return False
                return Response({"is_defaulter":False})
            # return False
        return Response({"is_defaulter":False})