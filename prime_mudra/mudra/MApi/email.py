import requests
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.conf import settings
creditkart_failed_txn_temp_id = "1007815743174960356"


def email(*args, **email_kwargs):
    try:
        mail_subject = email_kwargs['subject']
        email_template = email_kwargs['email_template']

        message = render_to_string(
            email_template,
            email_kwargs['data']
        )

        to_email = email_kwargs['to_email']
        from_email = settings.FROM_EMAIL_ADDRESS
        email = EmailMessage(mail_subject, message, from_email, [to_email])
        email.content_subtype = "html"
        email.send()

        return True

    except:

        return False


def sms(*args, **sms_kwrgs):
    try:
        sms_message = sms_kwrgs['sms_message']
        number = sms_kwrgs['number']

        if "tempid" in sms_kwrgs and sms_kwrgs["tempid"] is not None:
            tempid = sms_kwrgs["tempid"]
        else:
            tempid = ""

        sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
            settings.SMS_USERNAME) + '&password=' + str(settings.SMS_PASSWORD) + \
                  '&type=0&dlr=1&destination=' + number + '&source=' + str(
            settings.SMS_SOURCE) + '&message=' + sms_message + '&tempid=' + tempid

        requests.get(sms_url)

        return True

    except:

        return False


def transaction_sms(*args, **tsms_kwrgs):
    '''

        Sr. No. Error Code Description
        1   1701
        Success, Message Submitted Successfully. In this case you will receive
        the response 1701|<CELL_NO>|<MESSAGE ID>. The message Id can
        then be used later to map the delivery reports to this message.
        2   1702
        Invalid URL Error. This means that one of the parameters was not
        provided or left blank.
        3 1703 Invalid value in username or password parameter.
        4 1704 Invalid value in type parameter.
        5 1705 Invalid message.
        6 1706 Invalid destination.
        7 1707 Invalid source (Sender).
        8 1708 Invalid value for dlr parameter.
        9 1709 User validation failed
        10 1710 Internal error.
        11 1025 Insufficient credit.
        12 1715 Response timeout.

    http://<server>:8080/bulksms/bulksms?
    username=XXXX&password=YYYYY&type=Y&dlr=Z&destination=QQQQQQQQQ&source=RRRR&message=
    SSSSSSSS<&url=KKKK>

    :param args:
    :param tsms_kwrgs:
    :return:

    '''


    SMS_USERNAME = str(settings.TSMS_USERNAME)
    SMS_PASSWORD = str(settings.TSMS_PASSWORD)
    SMS_SOURCE = str(settings.TSMS_SOURCE)
    SMS_SERVER = "sms6.rmlconnect.net"

    try:

        sms_message = tsms_kwrgs['sms_message']
        number = tsms_kwrgs['number']

        sms_url = 'http://' + SMS_SERVER + ':8080/bulksms/bulksms?username=' + SMS_USERNAME + '&password=' + SMS_PASSWORD + \
                  '&type=0&dlr=1&destination=' + number + '&source=' + SMS_SOURCE + '&message=' + sms_message

        sms_url2 = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
            settings.TSMS_USERNAME) + '&password=' + str(settings.TSMS_PASSWORD) + \
                   '&type=0&dlr=1&destination=' + number + '&source=' + str(
            settings.TSMS_SOURCE) + '&message=' + sms_message

        requests.get(sms_url)

        return True

    except Exception as e:
        print("======Exception T sms====", e)
        return False


def failed_transaction_sms(mobile_number, amount):
    if mobile_number and amount:
        sms_message = 'Dear customer,\n Unfortunately, the payment of  ' + str(
            amount) + ' has failed. We request you to retry payment from the Creditkart APP. Ensure that you are entering correct account/card details to avoid failure in future.'
        sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
            settings.TSMS_USERNAME) + '&password=' + str(settings.TSMS_PASSWORD) + \
                  '&type=0&dlr=1&destination=' + str(mobile_number) + '&source=' + str(
            settings.TSMS_SOURCE) + '&message=' + str(sms_message) + '&tempid=' + str(creditkart_failed_txn_temp_id)
        requests.get(sms_url)
        return True
    else:
        return False

