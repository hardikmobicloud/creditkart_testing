from django.apps.registry import apps
from django.core import validators

from rest_framework import serializers


class ContactsSerializer(serializers.Serializer):
    """
    Contacts serializer class
    """

    contact_no = serializers.CharField(

        required=False,
        validators=[
            validators.RegexValidator('^[0-9]\d{11}$', message=''),
        ]
    )
