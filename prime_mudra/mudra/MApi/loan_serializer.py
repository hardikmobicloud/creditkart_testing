from django.apps.registry import apps
from django.contrib.sites.models import Site
from django.core.validators import RegexValidator
from django.db.models import Sum
from rest_framework import serializers

from LoanAppData.models import LoanApplication





class SubmitLoanSerializer(serializers.ModelSerializer):
    """
        Submit loan application model serializer
    """

    loan_amount = serializers.CharField(required=True, allow_blank=True, allow_null=False)
    tenure = serializers.CharField(required=False, allow_null=True, allow_blank=True)

    class Meta:
        model = LoanApplication
        fields = ('loan_amount', 'tenure',)
