from django.core import validators
from rest_framework import serializers


class LoginRegisterSerializer(serializers.Serializer):
    """
    User login serializer
    """

    mobile_number = serializers.CharField(

        required=True,
        validators=[
            validators.RegexValidator('^[6-9]\d{9}$', message=''),
        ]
    )




class OTPVerifySerializer(serializers.Serializer):
    mobile_number = serializers.CharField(required=True)
    otp = serializers.CharField(
        required=True,
        validators=[
            validators.RegexValidator('^[0-9]{6}$', message='Please provide valid otp.'),
        ]
    )


class SetPinSerializer(serializers.Serializer):
    """
    Set pin serializer class
    """

    mobile_number = serializers.CharField(

        required=True,
        validators=[
            validators.RegexValidator('^[6-9]\d{9}$', message=''),
        ]
    )

    pin = serializers.CharField(
        max_length=4, min_length=4,
        required=True,
        validators=[
            validators.RegexValidator('^[0-9]*$', message='')
        ]
    )




class FCMSerializer(serializers.Serializer):
    """
    To add user fcm entry
    """

    device_id = serializers.CharField(
        required=True
    )
