from django.apps import apps
from django.http import HttpResponse
from fcm_django.models import FCMDevice
from django.contrib.auth.models import User

PersonalDetails = apps.get_model('User', 'Personal')


# Documentation link
# https://fcm-django.readthedocs.io/en/lates

def fcm_notification(user_id, device_id, title, message, data, type, to_store, *args, **kwargs):
    """
        send notification to  devices
    :param user_id:
    :param device_id:
    :param title:
    :param message:
    :param data:
    :param type:
    :param to_store:
    :param args:
    :param kwargs:
    :return:
    """
    try:

        # send notification user wise

        user_instance = User.objects.get(id=user_id)

        device = FCMDevice.objects.get(user=user_instance)

        device.send_data_message(
            data_message={
                'title': title,
                'body': message,
                'notification_type': type,
            }
        )

        # insert notification entry in data base

        json_data = {
            'notification_type': type,
            'title': title,
            'body': message,
        }



    except Exception as e:
        return HttpResponse(e.args)
        pass


def fcm_simple_notification(*args, **sm_kwargs):
    """
    Simple fcm notification without any extra parameter's
    """

    try:
        device = "c28d3c916af31854"
        device = "14018927810336463000"



        device = FCMDevice.objects.all().first()

        device.send_data_message(
            data_message={
                'title': sm_kwargs['title'],
                'body': sm_kwargs['body'],
                'notification_type': 'notification',
            }
        )

        # insert notification entry in data base

        json_data = {
            'notification_type': sm_kwargs['type'],
            'title': sm_kwargs['title'],
            'body': sm_kwargs['body'],
        }



        return True

    except Exception as e:
        print("Exc ", e.args)
        return False


def fcm_test(request):
    sm_kwargs = {
        'title': "test",
        'body': 'Loan applied successfully',
        'notification_type': 'test msg',
    }

    fcm_simple_notification(**sm_kwargs)
    return HttpResponse("done")

