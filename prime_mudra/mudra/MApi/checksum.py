from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import *
from rest_framework.authentication import *
from django.contrib.auth.models import User
import base64
import string
import random
import hashlib
from Crypto.Cipher import AES
from django.apps.registry import apps

Payment = apps.get_model('Payments', 'Payment')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
Subscription = apps.get_model('User', 'Subscription')

import base64
import string
import random
import hashlib
import Crypto
from Crypto.Cipher import AES


IV = "@@@@&&&&####$$$$"
BLOCK_SIZE = 16


def generate_checksum(param_dict, merchant_key, salt=None):
    params_string = __get_param_string__(param_dict)
    if salt != None:
        salt = salt.decode('ascii')

    salt = salt if salt else __id_generator__(4)
    final_string = '%s|%s' % (params_string, salt)

    hasher = hashlib.sha256(final_string.encode())
    hash_string = hasher.hexdigest()
    hash_string += salt
    recheck=__encode__(hash_string, IV, merchant_key)
    return recheck.decode('utf-8')

def generate_refund_checksum(param_dict, merchant_key, salt=None):
    for i in param_dict:
        if("|" in param_dict[i]):
            param_dict = {}
            exit()
        params_string = __get_param_string__(param_dict)
        salt = salt if salt else __id_generator__(4)
        final_string = '%s|%s' % (params_string, salt)
        hasher = hashlib.sha256(final_string.encode())
        hash_string = hasher.hexdigest()

        hash_string += salt

    return __encode__(hash_string, IV, merchant_key)


def generate_checksum_by_str(param_str, merchant_key, salt=None):
    params_string = param_str
    #salt = salt if salt else __id_generator__(4)
    final_string = '%s|%s' % (params_string, salt)

    hasher = hashlib.sha256(final_string.encode())
    hash_string = hasher.hexdigest()
    #hash_string += str(salt)

    return __encode__(hash_string, IV, merchant_key)

def verify_checksum(param_dict, merchant_key, checksum):
    # Remove checksum
    if 'CHECKSUMHASH' in param_dict:
        param_dict.pop('CHECKSUMHASH')
    # Get salt
    paytm_hash = __decode__(checksum, IV, merchant_key)
    salt = paytm_hash[-4:]
    calculated_checksum = generate_checksum(param_dict, merchant_key, salt=salt.decode('ascii'))
    return calculated_checksum == checksum

def verify_checksum_by_str(param_str, merchant_key, checksum):
    # Remove checksum
    #if 'CHECKSUMHASH' in param_dict:
        #param_dict.pop('CHECKSUMHASH')

    # Get salt
    paytm_hash = __decode__(checksum, IV, merchant_key)
    salt = paytm_hash[-4:]
    calculated_checksum = generate_checksum_by_str(param_str, merchant_key, salt=salt)
    return calculated_checksum == checksum



def __id_generator__(size=6, chars=string.ascii_uppercase + string.digits + string.ascii_lowercase):
    return ''.join(random.choice(chars) for _ in range(size))


def __get_param_string__(params):
    params_string = []
    for key in params:
        value = params[key]
        params_string.append('' if value == 'null' else str(value))
    return '|'.join(params_string)


__pad__ = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * chr(BLOCK_SIZE - len(s) % BLOCK_SIZE)
__unpad__ = lambda s: s[0:-ord(s[-1:])]



def __encode__(to_encode, iv, key):
    to_encode = __pad__(to_encode)
    # Encrypt
    c = AES.new(key, AES.MODE_CBC, iv)
    to_encode = c.encrypt(to_encode)
    # Encode
    to_encode = base64.b64encode(to_encode)
    return to_encode


def __decode__(to_decode, iv, key):
    # Decode

    to_decode = base64.b64decode(to_decode)
    # Decrypt
    c = AES.new(key, AES.MODE_CBC, iv)
    to_decode = c.decrypt(to_decode)
    # remove pad
    return __unpad__(to_decode)



merchant_key = 'iz%91OJJEotF4_KR'






class ChecksumGeneratePaytm(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None

        try:
            MID = 'Mudrak01031243312800'
            INDUSTRY_TYPE_ID = 'Retail'
            CHANNEL_ID = 'WAP'
            WEBSITE = 'APPSTAGING'
            Callbalck_url = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="

            random_number = random.sample(range(00000, 99999), 1)
            loan_application_id = request.data.get('loan_application_id')
            ORDER_ID = loan_application_id+'m'+random_number
            CUST_ID = request.data.get('CUST_ID')
            TXN_AMOUNT = request.data.get('TXN_AMOUNT')
            CALLBACK_URL = Callbalck_url+ORDER_ID
            if CUST_ID == None or CUST_ID == '':
                status = 'CUST_ID01'
                message = 'CUST_ID None'
            elif TXN_AMOUNT == None or TXN_AMOUNT == '':
                status = 'TXN_AMOUNT01'
                message = 'TXN_AMOUNT None'


            else:

                dict_param = {
                    'MID': MID,
                    'ORDER_ID': ORDER_ID,
                    'CUST_ID': CUST_ID,
                    'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                    'CHANNEL_ID': CHANNEL_ID,
                    'TXN_AMOUNT': TXN_AMOUNT,
                    'WEBSITE': WEBSITE,
                    'CALLBACK_URL': CALLBACK_URL,

                }
                return_checksum = generate_checksum(dict_param, merchant_key, salt=None)

                if return_checksum:
                    payment=Payment.objects.create(status='initiated',
                                            order_id=str(ORDER_ID),amount=TXN_AMOUNT)
                    loan_application=LoanApplication.objects.filter(loan_application_id=loan_application_id)
                    loan_application[0].repayment_amount.add(payment)



                    message = "Successfuly Done"
                    status = 1
                else:
                    message = "Something Went Wrong"
                    status = 0

                return Response(
                    {
                        'status': status,
                        'message': message,
                        'checksum': return_checksum,
                        'mid': MID,
                        'orderId':ORDER_ID,
                        'industryType': INDUSTRY_TYPE_ID,
                        'website': WEBSITE,
                        'channel': CHANNEL_ID,
                        'callback':CALLBACK_URL
                    }
                )
        except:
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


merchant_key_1 = 'f_00Gtw7B2pn67#j'


class ChecksumGeneratePaytm_Producation(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None

        try:
            MID = 'Mudrak01031243312800'
            INDUSTRY_TYPE_ID = 'Retail'
            CHANNEL_ID = 'WAP'
            WEBSITE = 'APPSTAGING'
            Callbalck_url = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="

            random_number = random.sample(range(00000, 99999), 1)
            loan_application_id = request.data.get('loan_application_id')
            ORDER_ID = loan_application_id + 'm' + random_number
            CUST_ID = request.data.get('CUST_ID')
            TXN_AMOUNT = request.data.get('TXN_AMOUNT')
            CALLBACK_URL = Callbalck_url + ORDER_ID

            if CUST_ID == None or CUST_ID == '':
                status = 'CUST_ID01'
                message = 'CUST_ID None'
            elif TXN_AMOUNT == None or TXN_AMOUNT == '':
                status = 'TXN_AMOUNT01'
                message = 'TXN_AMOUNT None'

            else:

                dict_param = {
                    'MID': MID,
                    'ORDER_ID': ORDER_ID,
                    'CUST_ID': CUST_ID,
                    'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                    'CHANNEL_ID': CHANNEL_ID,
                    'TXN_AMOUNT': TXN_AMOUNT,
                    'WEBSITE': WEBSITE,
                    'CALLBACK_URL': CALLBACK_URL,

                }
                return_checksum = generate_checksum(dict_param, merchant_key_1, salt=None)

                if return_checksum:
                    payment=Payment.objects.create(status='initiated',
                                           order_id=str(ORDER_ID),amount=TXN_AMOUNT)
                    loan_application = LoanApplication.objects.filter(loan_application_id=loan_application_id)
                    loan_application[0].repayment_amount.add(payment)

                    message = "Successfuly Done"
                    status = 1
                else:
                    message = "Something Went Wrong"
                    status = 0

                return Response(
                    {
                        'status': status,
                        'message': message,
                        'checksum': return_checksum,
                        'mid': MID,
                        'orderId':ORDER_ID,
                        'industryType': INDUSTRY_TYPE_ID,
                        'website': WEBSITE,
                        'channel': CHANNEL_ID,
                        'callback':CALLBACK_URL
                    }
                )
        except:
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


class ChecksumGeneratePaytm_Subcription(APIView):
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None

        try:

            #MID = 'Mudrak01031243312800'
            MID = 'Mudrak19396823772273'
            #INDUSTRY_TYPE_ID = 'Retail'
            INDUSTRY_TYPE_ID = 'BFSI'
            CHANNEL_ID = 'WAP'
            WEBSITE = 'APPPROD'
            #WEBSITE = 'APPSTAGING'
            EMAIL= 'test@mudrakwik.com'

            #Callbalck_url="https://securegw-stage.paytm.in/theia/processTransaction?ORDER_ID="
            #Callbalck_url= "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="
            Callbalck_url= "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="

            random_number = random.sample(range(99999),1)
            mobile_number = request.data.get('mobile_number')
            MOBILE_NO= str(mobile_number)
            ORDER_ID = str(mobile_number) + 'ms' + str(random_number[0])
            CUST_ID = request.data.get('CUST_ID')
            TXN_AMOUNT = request.data.get('TXN_AMOUNT')
            CALLBACK_URL =str(Callbalck_url)+''+str(ORDER_ID)
            if CUST_ID == None or CUST_ID == '':
                status = 'CUST_ID01'
                message = 'CUST_ID None'
            elif TXN_AMOUNT == None or TXN_AMOUNT == '':
                status = 'TXN_AMOUNT01'
                message = 'TXN_AMOUNT None'


            else:

                dict_param = {
                    'MID': MID,
                    'ORDER_ID': ORDER_ID,
                    'CUST_ID': CUST_ID,
                    'INDUSTRY_TYPE_ID': INDUSTRY_TYPE_ID,
                    'CHANNEL_ID': CHANNEL_ID,
                    'TXN_AMOUNT': TXN_AMOUNT,
                    'WEBSITE': WEBSITE,
                    'CALLBACK_URL': CALLBACK_URL,
                    'EMAIL':EMAIL,
                    'MOBILE_NO':MOBILE_NO

                }
                paytm_status = 'initiated'
                amount = 50
                return_checksum = generate_checksum(dict_param, merchant_key, salt=None)
                if return_checksum:
                    payment=Payment.objects.create(status='initiated',
                                           order_id=str(ORDER_ID),amount=TXN_AMOUNT)
                    user=User.objects.filter(username=mobile_number).order_by('-id').first()
                    Subscription.objects.create(user_id=user.id,payment_id_id=payment.id)

                    message = "Successfuly Done"
                    status = 1
                else:
                    message = "Something Went Wrong"
                    status = 0

            return Response(
                {
                    'status': status,
                    'message': message,
                    'checksum': return_checksum,
                    'mid': MID,
                    'orderId': ORDER_ID,
                    'industryType': INDUSTRY_TYPE_ID,
                    'website': WEBSITE,
                    'channel': CHANNEL_ID,
                    'callback': CALLBACK_URL,
                    'email': EMAIL,
                    'mobile_no': MOBILE_NO
                }
            )
        except:
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )
