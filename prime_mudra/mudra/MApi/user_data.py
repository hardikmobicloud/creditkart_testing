import json
import re

from django.apps import apps
from django.conf import settings
from django.contrib.auth.models import User
from django.http import HttpResponse, response
from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics, status
from rest_framework.authentication import *
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from . import user_serializers
from . import user_data_serializers

PersonalDetails = apps.get_model('User', 'Personal')
BankDetails = apps.get_model('User', 'Bank')
AddressDetails = apps.get_model('User', 'Address')
ReferenceDetails = apps.get_model('User', 'Reference')
DocumentDetails = apps.get_model('User', 'Documents')
ContactDetails = apps.get_model('UserData', 'ContactMaster')
ShareAppDetails = apps.get_model('UserData', 'ShareAppMaster')
ProfessionalDetails = apps.get_model('User', 'Professional')
AppVersionCheck = apps.get_model('StaticData', 'AppVersionCheck')
Location = apps.get_model('User', 'Location')
ContactMasterJsonData = apps.get_model('UserData', 'ContactMasterJsonData')

class Contacts(generics.ListAPIView):
    """
    User Contacts details API

    :parameter:
        Method: POST
        Response

        {
            'name':"CharField",
            'mobile_no ':"CharField",

        }

        :Note


        status 9 : No Details Found
        status 10 : Something went wrong

        HTTP_200_OK
        HTTP_201_CREATED
        HTTP_400_BAD_REQUEST
        HTTP_401_UNAUTHORIZED
        HTTP_403_FORBIDDEN

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = user_data_serializers.ContactsSerializer

    def get(self, request, *args, **kwargs):
        data_dictionary = {}

        try:
            contacts = ContactDetails.objects.filter(user=request.user.id)
            if contacts:
                for i in contacts:
                    if i:
                        data_dictionary['contact_name'] = i.contact_name
                        data_dictionary['contact_no'] = i.contact_no
                return Response(
                    data_dictionary
                )
            else:
                return Response(
                    {
                        'status': '9',
                        'message': 'No Details Found',

                    }
                )

        except Exception as e:
            print("-------User Contact  Details API-------", e.args)
            return Response(
                {
                    'status': '10',
                    'message': 'Something went wrong!'
                }
            )

    def post(self, request):

        default_name = "No Name Found"
        total_contacts_saved = 0
        get_contacts = ContactDetails.objects.filter(user_id=request.user.id).values_list('contact_no', flat=True)
        try:
            request_data = request.data
            request_data['user'] = request.user.id
            data_dictionary = request.data
            if 'contacts' in data_dictionary.keys():
                contacts = data_dictionary['contacts']
                for i in contacts:
                    # Store Name
                    if (i['familyName'] is not None and i['familyName'] is not "None") and \
                            (i['middleName'] is not None and i['middleName'] is not "None") and \
                            (i['givenName'] is not None and i['givenName'] is not "None"):
                        name = i['familyName'] + " " + i['middleName'] + " " + i['givenName']
                    elif i['familyName'] is not None and i['middleName'] is not None:
                        name = i['familyName'] + " " + i['middleName']
                    elif i['middleName'] is not None and i['givenName'] is not None:
                        name = i['middleName'] + " " + i['givenName']
                    elif i['familyName'] is not None and i['givenName'] is not None:
                        name = i['familyName'] + " " + i['givenName']
                    elif i['familyName'] is not None:
                        name = i['familyName']
                    elif i['middleName'] is not None:
                        name = i['middleName']
                    elif i['givenName'] is not None:
                        name = i['givenName']
                    else:
                        name = default_name
                    phone_numbers = i['phoneNumbers']

                    for j in phone_numbers:
                        # Getting Numbers
                        number = j['number']
                        if len(str(number)) >= 10:
                            number = re.sub('[^0-9]+', '', number)
                            if number not in list(get_contacts):
                                try:
                                    ContactDetails.objects.create(user_id=request.user.id, contact_name=name,
                                                                  contact_no=str(number))
                                    total_contacts_saved += 1
                                except:
                                    pass
            return Response(status=status.HTTP_201_CREATED)

        except Exception as e:
            print("Exception post Contacts", e.args)
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)


class ShareApp(generics.ListAPIView):
    """
    User share app Contacts details  API

    :parameter:
        Method: POST
        Response

        {
            'name':"CharField",
            'mobile_no ':"CharField",

        }

        :Note


        status 9 : No Details Found
        status 10 : Something went wrong

        HTTP_200_OK
        HTTP_201_CREATED
        HTTP_400_BAD_REQUEST
        HTTP_401_UNAUTHORIZED
        HTTP_403_FORBIDDEN

    """

    authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    serializer_class = user_data_serializers.ContactsSerializer


    def post(self, request):
        get_contacts = ShareAppDetails.objects.filter(user_id=request.user.id).values_list('number', flat=True)
        default_name = "No Name Found"
        total_contacts_shared = 0
        try:
            request_data = request.data
            request_data['user'] = request.user.id
            data_dictionary = request.data
            contacts = data_dictionary['contacts']
            len_cont = len(contacts)
            for i in range(len_cont):

                # Getting Numbers
                number = contacts[i]
                if len(str(number)) >= 10:
                    number = re.sub('[^0-9]+', '', number)
                    if number not in list(get_contacts):

                        contact_name = ContactDetails.objects.filter(contact_no=number, user_id=request.user.id)
                        if contact_name:

                            try:
                                ShareAppDetails.objects.create(number=str(number)[-10:], user_id=request.user.id,
                                                               name=contact_name[0].contact_name)
                                total_contacts_shared += 1
                            except Exception as e:
                                print(e)
                                pass
                        else:
                            try:
                                ShareAppDetails.objects.create(user_id=request.user.id,
                                                               number=str(number)[-10:])
                                total_contacts_shared += 1
                            except Exception as e:
                                print(e)
                                pass
            return Response(status=status.HTTP_201_CREATED)

        except Exception as e:
            print("Exception post share app", e.args)
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)


class ContactsNew(generics.ListAPIView):
    """
    User Contacts details API

    :parameter:
        Method: POST
        Response

        {
            'name':"CharField",
            'mobile_no ':"CharField",

        }

        :Note


        status 9 : No Details Found
        status 10 : Something went wrong

        HTTP_200_OK
        HTTP_201_CREATED
        HTTP_400_BAD_REQUEST
        HTTP_401_UNAUTHORIZED
        HTTP_403_FORBIDDEN

    """

    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    serializer_class = user_data_serializers.ContactsSerializer

    def get(self, request, *args, **kwargs):
        data_dictionary = {}
        data_list = []

        try:
            contacts = ContactMasterJsonData.objects.filter(user=request.user.id).values_list("json_data", flat=True)
            if contacts:
                for contact in contacts:
                    if contact:
                        json_data = json.loads(contact)
                        if "data" in json_data:
                            new_data = json_data['data']

                            for i in new_data:
                                data_dictionary = {"contact_name": i['contact_name'],
                                                        "contact_no": i['contact_no']}
                                data_list.append(data_dictionary)

                return Response({
                  "data": data_list
                })
            else:
                return Response(
                    {
                        'status': '9',
                        'message': 'No Details Found',

                    }
                )

        except Exception as e:
            print("-------User Contact  Details API-------", e.args)
            return Response(
                {
                    'status': '10',
                    'message': 'Something went wrong!'
                }
            )

    def post(self, request):
        new_data = []
        default_name = "No Name Found"
        total_contacts_saved = 0

        get_contacts = ContactMasterJsonData.objects.filter(user_id=request.user.id)
        try:
            request_data = request.data
            request_data['user'] = request.user.id
            data_dictionary = request.data
            if 'contacts' in data_dictionary.keys():
                contacts = data_dictionary['contacts']
                for i in contacts:
                    # Store Name
                    if (i['familyName'] is not None and i['familyName'] is not "None") and \
                            (i['middleName'] is not None and i['middleName'] is not "None") and \
                            (i['givenName'] is not None and i['givenName'] is not "None"):
                        name = i['familyName'] + " " + i['middleName'] + " " + i['givenName']
                    elif i['familyName'] is not None and i['middleName'] is not None:
                        name = i['familyName'] + " " + i['middleName']
                    elif i['middleName'] is not None and i['givenName'] is not None:
                        name = i['middleName'] + " " + i['givenName']
                    elif i['familyName'] is not None and i['givenName'] is not None:
                        name = i['familyName'] + " " + i['givenName']
                    elif i['familyName'] is not None:
                        name = i['familyName']
                    elif i['middleName'] is not None:
                        name = i['middleName']
                    elif i['givenName'] is not None:
                        name = i['givenName']
                    else:
                        name = default_name
                    phone_numbers = i['phoneNumbers']

                    for j in phone_numbers:
                        # Getting Numbers
                        number = j['number']
                        if len(str(number)) >= 10:
                            number = re.sub('[^0-9]+', '', number)
                            if number not in list(get_contacts):
                                try:

                                    contacts = ContactMasterJsonData.objects.filter(user=request.user.id).values_list(
                                        "json_data", flat=True)
                                    if contacts:
                                        for contact in contacts:
                                            if contact:
                                                json_data = json.loads(contact)
                                                if "data" in json_data:
                                                    new_data = json_data['data']
                                                    for i in new_data:
                                                        if i['contact_name'] == name and i['contact_no'] == str(number):
                                                            pass
                                                        else:
                                                            i['contact_name'] = name
                                                            i['contact_no'] = number

                                                    new_json_data = json.dumps(new_data)
                                                    ContactMasterJsonData.objects.create(user_id=request.user.id).update(json_data=new_json_data)
                                                    total_contacts_saved += 1

                                except:
                                    pass
            return Response(status=status.HTTP_201_CREATED)

        except Exception as e:
            print("Exception post Contacts", e.args)
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)