from fcm_django.models import FCMDevice
from django.contrib.auth.models import User
from django.apps.registry import apps
from rest_framework.permissions import *
from rest_framework.authentication import *
from rest_framework.views import APIView
from rest_framework import generics
from rest_framework.response import Response
# from .dashboard_serializer import NotificationListSerializer
from django.shortcuts import HttpResponse


NotificationEntry = apps.get_model('UserData', 'NotificationEntry')
UserBankDetails = apps.get_model('UserData', 'UserBankDetails')
UserCreditCardDetails = apps.get_model('UserData', 'UserCreditCardDetails')
UserDebitCardDetails = apps.get_model('UserData', 'UserDebitCardDetails')
UserMerchantDetails = apps.get_model('UserData', 'UserMerchantDetails')
LoanApplication = apps.get_model('LoanAppData','LoanApplication')

Personal = apps.get_model('User', 'Personal')
Professional = apps.get_model('User', 'Professional')
Documents = apps.get_model('User', 'Documents')
Reference = apps.get_model('User', 'Reference')
Bank = apps.get_model('User', 'Bank')




def fcm_notification(user_id, device_id, title, message, data, type, to_store, *args, **kwargs):

    try:

        # send notification user wise

        user_instance = User.objects.get(id=user_id)
        device = FCMDevice.objects.get(user=user_instance)

        device.send_data_message(
            data_message= {
                'title': title,
                'body': message,
                'notification_type': type,
            }
        )

        # insert notification entry in data base

        json_data = {
            'notification_type': type,
            'title': title,
            'body': message,
        }

        if to_store == True:

            NotificationEntry.objects.create(
                user=user_instance,
                notification_title=title, notification_body=message,
                notification_type=type, data= json_data,
                is_read = False
            )

    except Exception as e:
        return HttpResponse(e.args)
        pass


def promotional_notifications(*args, **pm_agrs):

    """
    To send all promotional's notifications
    """

    try:

        title = pm_agrs['title']
        message = pm_agrs['message']
        type = pm_agrs['type']

        device = FCMDevice.objects.all()

        device.send_data_message(
            data_message={
                'title': title,
                'body': message,
                'notification_type': type,
            }
        )

        return True

    except Exception as e:
        print(e.args)

        return False


def fcm_simple_notification(*args, **sm_kwargs):

    """
    Simple fcm notification without any extra parameter's
    """

    try:

        user_instance = User.objects.get(id=sm_kwargs['user_id'])
        device = FCMDevice.objects.get(user=user_instance)

        device.send_data_message(
            data_message={
                'title': sm_kwargs['title'],
                'body': sm_kwargs['message'],
                'notification_type': sm_kwargs['type'],
            }
        )

        # insert notification entry in data base

        json_data = {
            'notification_type': sm_kwargs['type'],
            'title': sm_kwargs['title'],
            'body': sm_kwargs['message'],
        }

        if sm_kwargs['to_store']:

            NotificationEntry.objects.create(
                user=user_instance,
                notification_title=sm_kwargs['title'], notification_body=sm_kwargs['message'],
                notification_type=sm_kwargs['type'], data=json_data,
                is_read=False
            )

        return True

    except:

        return False


def fcm_profile_status_incomplete(*args, **pf_kwargs):

    """
    Send notification to user if profile is in-completed
    """

    try:
        user_instance = User.objects.get(id=pf_kwargs['user_id'])

        user_loan_pd_obj = Personal.objects.filter(user=user_instance).last()
        user_loan_pro = Professional.objects.filter(user=user_instance)
        user_loan_bnk = Bank.objects.filter(user=user_instance)
        user_loan_docs = Documents.objects.filter(user=user_instance)
        user_loan_refe = Reference.objects.filter(user=user_instance)

        if user_loan_pd_obj:


            if user_loan_pd_obj.occupation == 'SALARIED':
                #    TODO check for user profile is complete
                pass



            else:

                return True

        return True

    except Exception as e:

        return False


def fcm_profile_status_complete(*args, **pf_c_kwargs):

    """
    Send notification to user after profile is completed
    """

    try:
        user_instance = User.objects.get(id=pf_kwargs['user_id'])

        user_loan_pd_obj = Personal.objects.filter(user=user_instance).last()
        user_loan_pro = Professional.objects.filter(user=user_instance)
        user_loan_bnk = Bank.objects.filter(user=user_instance)
        user_loan_docs = Documents.objects.filter(user=user_instance)
        user_loan_refe = Reference.objects.filter(user=user_instance)

        if user_loan_pd_obj:


            if user_loan_pd_obj.occupation == 'SALARIED':
                #    TODO check for user profile is complete
                pass




            else:

                return True

        return True

    except Exception as e:

        return False



def fcm_parameter_notification(*args, **fmp_kwargs):

    """
     Fcm notification with parameter's
    """

    try:

        user_instance = User.objects.get(id=fmp_kwargs['user_id'])
        device = FCMDevice.objects.get(user=user_instance)

        device.send_data_message(
            data_message={
                'title': fmp_kwargs['title'],
                'body': fmp_kwargs['message'],
                'notification_type': fmp_kwargs['type'],
                fmp_kwargs['notification_key']: fmp_kwargs['notification_id']
            }
        )

        # insert notification entry in data base

        json_data = {
            'notification_type': fmp_kwargs['type'],
            'title': fmp_kwargs['title'],
            'body': fmp_kwargs['message'],
        }

        if fmp_kwargs['to_store']:
            NotificationEntry.objects.create(
                user=user_instance,
                notification_title=fmp_kwargs['title'], notification_body=fmp_kwargs['message'],
                notification_type=fmp_kwargs['type'], data=json_data,
                is_read=False
            )

        return True

    except:

        return False



class NotificationDeleteAPIView(APIView):

    """
    API to delete notification

    :parameter:

        API to delete notification from table using id

        Parameters

        {
            "notification_id": "CharField"
        }

        Response

        {
            "status": "CharField",
            "message": "CharField"
        }

        Note :

        Authentication token required

        [
            {
                "key":"Authorization",
                "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
            }
        ]

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        status = None
        message = None

        try:

            user_instance = User.objects.get(id=request.user.id)
            notification_id = request.data.get('notification_id')

            if notification_id:

                NotificationEntry.objects.filter(id=notification_id, user=user_instance).delete()

                status = '1'
                message = 'Notification deleted successfully'

            else:

                status = '2'
                message = 'Something went wrong'

        except:

            status = '10'
            message = 'Something went wrong'

        return Response(
            {
                'status': status,
                'message': message
            }
        )


def fcm_test(request):
    sm_kwargs = {
        'title': "test",
        'body': 'Loan applied successfully',
        'notification_type': 'test msg',
    }

    fcm_simple_notification(**sm_kwargs)
    return HttpResponse("done")

