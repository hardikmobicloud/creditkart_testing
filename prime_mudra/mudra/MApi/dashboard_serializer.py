from rest_framework import serializers
from django.apps.registry import apps
from django.contrib.sites.models import Site

UserMerchantDetails = apps.get_model('UserData', 'UserMerchantDetails')
UserCreditCardDetails = apps.get_model('UserData', 'UserCreditCardDetails')
UserDebitCardDetails = apps.get_model('UserData', 'UserDebitCardDetails')
UserBankDetails = apps.get_model('UserData', 'UserBankDetails')
NotificationEntry = apps.get_model('UserData', 'NotificationEntry')
MerchantModel = apps.get_model('IcomeExpense', 'MerchantModel')


class DashboardUserBankDetailsSerializers(serializers.ModelSerializer):
    class Meta:
        model = UserBankDetails
        fields = ('__all__')


class DashboardUserCreditCardSerializers(serializers.ModelSerializer):
    class Meta:
        model = UserCreditCardDetails
        fields = ('__all__')


class DashboardUserDebitCardSerializers(serializers.ModelSerializer):
    class Meta:
        model = UserDebitCardDetails
        fields = ('__all__')


class DashboardUserBankDetailsSerializersGraph(serializers.ModelSerializer):
    """
    Dashboard master list serializer
    """

    logo = serializers.SerializerMethodField()
    transaction_amount = serializers.SerializerMethodField()

    class Meta:
        model = UserBankDetails
        fields = ('id', 'bank_id', 'transaction_amount', 'transaction_type', 'transaction_date', 'towards', 'category',
                  'merchant', 'logo',)

    def get_transaction_amount(self, obj):

        return int(obj.transaction_amount)

    def get_logo(self, obj):

        current_site = Site.objects.get_current()
        domain_name = current_site.domain

        try:

            merchant = obj.merchant

            if merchant:

                merchant_instance = MerchantModel.objects.filter(name=merchant)
                merchant_obj = MerchantModel.objects.filter(name=merchant).last()

                if merchant_instance:

                    return domain_name + 'media/' + str(merchant_obj.logo)

                elif obj.category == 'Cash':

                    return domain_name + 'media/bank_logos/Cash_256x256.png'

                elif obj.category == 'UPI':

                    return domain_name + 'media/bank_logos/upi_256x256.png'

                elif obj.category == 'Wallet':

                    return domain_name + 'media/bank_logos/wallet_256x256.png'

                elif obj.category == 'Shopping':

                    return domain_name + 'media/bank_logos/shopping.png'

                elif obj.category == 'Bill':

                    return domain_name + 'media/bank_logos/bill.png'

                elif obj.category == 'Hotel':

                    return domain_name + 'media/bank_logos/hotel.png'

                elif obj.category == 'Other':

                    return domain_name + 'media/bank_logos/other.png'

                elif obj.transaction_type == 'Debit':
                    return domain_name + 'media/bank_logos/default_debit.png'
                elif obj.transaction_type == 'Credit':
                    return domain_name + 'media/bank_logos/default_credit.png'
                else:
                    return domain_name + 'media/default_images/Mudrakwik_64X64.png'

            else:

                if obj.transaction_type == 'Debit':
                    return domain_name + 'media/bank_logos/default_debit.png'
                elif obj.transaction_type == 'Credit':
                    return domain_name + 'media/bank_logos/default_credit.png'
                else:
                    return domain_name + 'media/default_images/Mudrakwik_64X64.png'

        except:

            return domain_name + 'media/default_images/Mudrakwik_64X64.png'


class DashboardUserBankDetailsSerializersDetails(serializers.ModelSerializer):
    logo = serializers.SerializerMethodField()
    transaction_amount = serializers.SerializerMethodField()

    class Meta:
        model = UserBankDetails
        fields = ('bank_id', 'transaction_amount', 'account_number', 'transaction_type', 'transaction_date', 'towards',
                  'category', 'merchant', 'logo',)

    def get_transaction_amount(self, obj):

        return int(obj.transaction_amount)

    def get_logo(self, obj):

        current_site = Site.objects.get_current()
        domain_name = current_site.domain

        try:

            merchant = obj.merchant

            if merchant:

                merchant_instance = MerchantModel.objects.filter(name=merchant)
                merchant_obj = MerchantModel.objects.filter(name=merchant).last()

                if merchant_instance:
                    return domain_name + 'media/' + str(merchant_obj.logo)

                elif obj.category == 'Cash':

                    return domain_name + 'media/bank_logos/Cash_256x256.png'

                elif obj.category == 'UPI':

                    return domain_name + 'media/bank_logos/upi_256x256.png'

                elif obj.category == 'Wallet':

                    return domain_name + 'media/bank_logos/wallet_256x256.png'

                elif obj.category == 'Shopping':

                    return domain_name + 'media/bank_logos/shopping.png'

                elif obj.category == 'Bill':

                    return domain_name + 'media/bank_logos/bill.png'

                elif obj.category == 'Hotel':

                    return domain_name + 'media/bank_logos/hotel.png'

                elif obj.category == 'Other':

                    return domain_name + 'media/bank_logos/other.png'

                elif obj.transaction_type == 'Debit':
                    return domain_name + 'media/bank_logos/default_debit.png'
                elif obj.transaction_type == 'Credit':
                    return domain_name + 'media/bank_logos/default_credit.png'
                else:
                    return domain_name + 'media/default_images/Mudrakwik_64X64.png'

            else:

                if obj.transaction_type == 'Debit':
                    return domain_name + 'media/bank_logos/default_debit.png'
                elif obj.transaction_type == 'Credit':
                    return domain_name + 'media/bank_logos/default_credit.png'
                else:
                    return domain_name + 'media/default_images/Mudrakwik_64X64.png'

        except:

            return domain_name + 'media/default_images/Mudrakwik_64X64.png'


class DashboardUserCreditCardDetailsSerializersDetails(serializers.ModelSerializer):
    logo = serializers.SerializerMethodField()
    transaction_amount = serializers.SerializerMethodField()

    class Meta:
        model = UserCreditCardDetails
        fields = (
            'bank_id', 'transaction_amount', 'credit_card_number', 'transaction_type', 'transaction_date', 'towards',
            'category', 'merchant', 'logo',)

    def get_transaction_amount(self, obj):

        return int(obj.transaction_amount)

    def get_logo(self, obj):

        current_site = Site.objects.get_current()
        domain_name = current_site.domain

        try:

            merchant = obj.merchant

            if merchant:

                merchant_instance = MerchantModel.objects.filter(name=merchant)
                merchant_obj = MerchantModel.objects.filter(name=merchant).last()

                if merchant_instance:
                    return domain_name + 'media/' + str(merchant_obj.logo)

                elif obj.category == 'Cash':

                    return domain_name + 'media/bank_logos/Cash_256x256.png'

                elif obj.category == 'UPI':

                    return domain_name + 'media/bank_logos/upi_256x256.png'

                elif obj.category == 'Wallet':

                    return domain_name + 'media/bank_logos/wallet_256x256.png'

                elif obj.category == 'Shopping':

                    return domain_name + 'media/bank_logos/shopping.png'

                elif obj.category == 'Bill':

                    return domain_name + 'media/bank_logos/bill.png'

                elif obj.category == 'Hotel':

                    return domain_name + 'media/bank_logos/hotel.png'

                elif obj.category == 'Other':

                    return domain_name + 'media/bank_logos/other.png'

                elif obj.transaction_type == 'Debit':
                    return domain_name + 'media/bank_logos/default_debit.png'
                elif obj.transaction_type == 'Credit':
                    return domain_name + 'media/bank_logos/default_credit.png'
                else:
                    return domain_name + 'media/default_images/Mudrakwik_64X64.png'

            else:

                if obj.transaction_type == 'Debit':
                    return domain_name + 'media/bank_logos/default_debit.png'
                elif obj.transaction_type == 'Credit':
                    return domain_name + 'media/bank_logos/default_credit.png'
                else:
                    return domain_name + 'media/default_images/Mudrakwik_64X64.png'

        except:

            return domain_name + 'media/default_images/Mudrakwik_64X64.png'


class DashboardUserDebitCardDetailsSerializersDetails(serializers.ModelSerializer):
    logo = serializers.SerializerMethodField()
    transaction_amount = serializers.SerializerMethodField()

    class Meta:
        model = UserDebitCardDetails
        fields = (
            'bank_id', 'transaction_amount', 'debit_card_number', 'transaction_type', 'transaction_date', 'towards',
            'category', 'merchant', 'logo',)

    def get_transaction_amount(self, obj):

        return int(obj.transaction_amount)

    def get_logo(self, obj):

        current_site = Site.objects.get_current()
        domain_name = current_site.domain

        try:

            merchant = obj.merchant

            if merchant:

                merchant_instance = MerchantModel.objects.filter(name=merchant)
                merchant_obj = MerchantModel.objects.filter(name=merchant).last()

                if merchant_instance:
                    return domain_name + 'media/' + str(merchant_obj.logo)

                elif obj.category == 'Cash':

                    return domain_name + 'media/bank_logos/Cash_256x256.png'

                elif obj.category == 'UPI':

                    return domain_name + 'media/bank_logos/upi_256x256.png'

                elif obj.category == 'Wallet':

                    return domain_name + 'media/bank_logos/wallet_256x256.png'

                elif obj.category == 'Shopping':

                    return domain_name + 'media/bank_logos/shopping.png'

                elif obj.category == 'Bill':

                    return domain_name + 'media/bank_logos/bill.png'

                elif obj.category == 'Hotel':

                    return domain_name + 'media/bank_logos/hotel.png'

                elif obj.category == 'Other':

                    return domain_name + 'media/bank_logos/other.png'

                elif obj.transaction_type == 'Debit':
                    return domain_name + 'media/bank_logos/default_debit.png'
                elif obj.transaction_type == 'Credit':
                    return domain_name + 'media/bank_logos/default_credit.png'
                else:
                    return domain_name + 'media/default_images/Mudrakwik_64X64.png'

            else:

                if obj.transaction_type == 'Debit':
                    return domain_name + 'media/bank_logos/default_debit.png'
                elif obj.transaction_type == 'Credit':
                    return domain_name + 'media/bank_logos/default_credit.png'
                else:
                    return domain_name + 'media/default_images/Mudrakwik_64X64.png'

        except:

            return domain_name + 'media/default_images/Mudrakwik_64X64.png'


class DashboardUserMerchantDetailsSerializersDetails(serializers.ModelSerializer):
    logo = serializers.SerializerMethodField()
    transaction_amount = serializers.SerializerMethodField()

    class Meta:
        model = UserMerchantDetails
        fields = ('merchant_id', 'transaction_amount', 'transaction_type', 'transaction_date', 'category', 'logo',)

    def get_transaction_amount(self, obj):

        return int(obj.transaction_amount)

    def get_logo(self, obj):

        current_site = Site.objects.get_current()
        domain_name = current_site.domain

        try:

            merchant = obj.merchant_id

            if merchant:

                merchant_instance = MerchantModel.objects.filter(name=merchant)
                merchant_obj = MerchantModel.objects.filter(name=merchant).last()

                if merchant_instance:
                    return domain_name + 'media/' + str(merchant_obj.logo)

                elif obj.category == 'Cash':

                    return domain_name + 'media/bank_logos/Cash_256x256.png'

                elif obj.category == 'UPI':

                    return domain_name + 'media/bank_logos/upi_256x256.png'

                elif obj.category == 'Wallet':

                    return domain_name + 'media/bank_logos/wallet_256x256.png'

                elif obj.category == 'Shopping':

                    return domain_name + 'media/bank_logos/shopping.png'

                elif obj.category == 'Bill':

                    return domain_name + 'media/bank_logos/bill.png'

                elif obj.category == 'Hotel':

                    return domain_name + 'media/bank_logos/hotel.png'

                elif obj.category == 'Other':

                    return domain_name + 'media/bank_logos/other.png'

                elif obj.transaction_type == 'Debit':
                    return domain_name + 'media/bank_logos/default_debit.png'

                elif obj.transaction_type == 'Credit':
                    return domain_name + 'media/bank_logos/default_credit.png'

                else:
                    return domain_name + 'media/default_images/Mudrakwik_64X64.png'

            else:

                if obj.transaction_type == 'Debit':
                    return domain_name + 'media/bank_logos/default_debit.png'
                elif obj.transaction_type == 'Credit':
                    return domain_name + 'media/bank_logos/default_credit.png'
                else:
                    return domain_name + 'media/default_images/Mudrakwik_64X64.png'

        except Exception as e:
            print(e.args)

            return domain_name + 'media/default_images/Mudrakwik_64X64.png'


class NotificationListSerializer(serializers.ModelSerializer):
    class Meta:
        model = NotificationEntry
        fields = ('id', 'notification_title', 'notification_body', 'notification_type', 'is_read', 'date_created',)


class DashboardBankDetailsUniqueSerializer(serializers.ModelSerializer):
    logo = serializers.SerializerMethodField()
    bank_id = serializers.SerializerMethodField()

    class Meta:
        model = UserBankDetails
        fields = ('bank_id', 'logo', 'account_number',)

    def get_bank_id(self, obj):

        try:
            merchant = obj.merchant
            if merchant:
                merchant_instance = MerchantModel.objects.filter(name=merchant)
                merchant_obj = MerchantModel.objects.filter(name=merchant).last()

                if merchant_instance:

                    return str(merchant_obj.name)

                else:

                    return 'Other'


            else:

                return 'Other'

        except:

            return 'Other'

    def get_logo(self, obj):

        current_site = Site.objects.get_current()
        domain_name = current_site.domain

        try:

            merchant = obj.merchant

            if merchant:

                merchant_instance = MerchantModel.objects.filter(name=merchant)
                merchant_obj = MerchantModel.objects.filter(name=merchant).last()

                if merchant_instance:
                    return domain_name + 'media/' + str(merchant_obj.logo)

                elif obj.category == 'Cash':

                    return domain_name + 'media/bank_logos/Cash_256x256.png'

                elif obj.category == 'UPI':

                    return domain_name + 'media/bank_logos/upi_256x256.png'

                elif obj.category == 'Wallet':

                    return domain_name + 'media/bank_logos/wallet_256x256.png'

                elif obj.category == 'Shopping':

                    return domain_name + 'media/bank_logos/shopping.png'

                elif obj.category == 'Bill':

                    return domain_name + 'media/bank_logos/bill.png'

                elif obj.category == 'Hotel':

                    return domain_name + 'media/bank_logos/hotel.png'

                elif obj.category == 'Other':

                    return domain_name + 'media/bank_logos/other.png'

                elif obj.transaction_type == 'Debit':
                    return domain_name + 'media/bank_logos/default_debit.png'
                elif obj.transaction_type == 'Credit':
                    return domain_name + 'media/bank_logos/default_credit.png'
                else:
                    return domain_name + 'media/default_images/Mudrakwik_64X64.png'

            else:

                if obj.transaction_type == 'Debit':
                    return domain_name + 'media/bank_logos/default_debit.png'
                elif obj.transaction_type == 'Credit':
                    return domain_name + 'media/bank_logos/default_credit.png'
                else:
                    return domain_name + 'media/default_images/Mudrakwik_64X64.png'

        except:

            return domain_name + 'media/default_images/Mudrakwik_64X64.png'


class DashboardCreditCardDetailsUniqueSerializer(serializers.ModelSerializer):
    logo = serializers.SerializerMethodField()

    class Meta:
        model = UserCreditCardDetails
        fields = ('bank_id', 'logo', 'credit_card_number',)

    def get_logo(self, obj):

        current_site = Site.objects.get_current()
        domain_name = current_site.domain

        try:

            merchant = obj.bank_id

            if merchant:

                merchant_instance = MerchantModel.objects.filter(sender_id__icontains=merchant)
                merchant_obj = MerchantModel.objects.filter(sender_id__icontains=merchant).last()

                if merchant_instance:
                    return domain_name + 'media/' + str(merchant_obj.logo)

                elif obj.category == 'Cash':

                    return domain_name + 'media/bank_logos/Cash_256x256.png'

                elif obj.category == 'UPI':

                    return domain_name + 'media/bank_logos/upi_256x256.png'

                elif obj.category == 'Wallet':

                    return domain_name + 'media/bank_logos/wallet_256x256.png'

                elif obj.category == 'Shopping':

                    return domain_name + 'media/bank_logos/shopping.png'

                elif obj.category == 'Bill':

                    return domain_name + 'media/bank_logos/bill.png'

                elif obj.category == 'Hotel':

                    return domain_name + 'media/bank_logos/hotel.png'

                elif obj.category == 'Other':

                    return domain_name + 'media/bank_logos/other.png'


                elif obj.transaction_type == 'Debit':
                    return domain_name + 'media/bank_logos/default_debit.png'
                elif obj.transaction_type == 'Credit':
                    return domain_name + 'media/bank_logos/default_credit.png'
                else:
                    return domain_name + 'media/default_images/Mudrakwik_64X64.png'

            else:

                if obj.transaction_type == 'Debit':
                    return domain_name + 'media/bank_logos/default_debit.png'
                elif obj.transaction_type == 'Credit':
                    return domain_name + 'media/bank_logos/default_credit.png'
                else:
                    return domain_name + 'media/default_images/Mudrakwik_64X64.png'

        except:

            return domain_name + 'media/default_images/Mudrakwik_64X64.png'


class DashboardDebitCardDetailsUniqueSerializer(serializers.ModelSerializer):
    logo = serializers.SerializerMethodField()

    class Meta:
        model = UserDebitCardDetails
        fields = ('bank_id', 'logo', 'debit_card_number',)

    def get_logo(self, obj):

        current_site = Site.objects.get_current()
        domain_name = current_site.domain

        try:

            merchant = obj.bank_id

            if merchant:

                merchant_instance = MerchantModel.objects.filter(sender_id=merchant)
                merchant_obj = MerchantModel.objects.filter(sender_id=merchant).last()

                if merchant_instance:
                    return domain_name + 'media/' + str(merchant_obj.logo)

                elif obj.category == 'Cash':

                    return domain_name + 'media/bank_logos/Cash_256x256.png'

                elif obj.category == 'UPI':

                    return domain_name + 'media/bank_logos/upi_256x256.png'

                elif obj.category == 'Wallet':

                    return domain_name + 'media/bank_logos/wallet_256x256.png'

                elif obj.category == 'Shopping':

                    return domain_name + 'media/bank_logos/shopping.png'

                elif obj.category == 'Bill':

                    return domain_name + 'media/bank_logos/bill.png'

                elif obj.category == 'Hotel':

                    return domain_name + 'media/bank_logos/hotel.png'

                elif obj.category == 'Other':

                    return domain_name + 'media/bank_logos/other.png'

                elif obj.transaction_type == 'Debit':
                    return domain_name + 'media/bank_logos/default_debit.png'
                elif obj.transaction_type == 'Credit':
                    return domain_name + 'media/bank_logos/default_credit.png'
                else:
                    return domain_name + 'media/default_images/Mudrakwik_64X64.png'

            else:

                if obj.transaction_type == 'Debit':
                    return domain_name + 'media/bank_logos/default_debit.png'
                elif obj.transaction_type == 'Credit':
                    return domain_name + 'media/bank_logos/default_credit.png'
                else:
                    return domain_name + 'media/default_images/Mudrakwik_64X64.png'

        except Exception as e:
            print(e.args)
            return domain_name + 'media/default_images/Mudrakwik_64X64.png'


class DashboardMerchantDetailsUniqueSerializer(serializers.ModelSerializer):
    logo = serializers.SerializerMethodField()

    class Meta:
        model = UserMerchantDetails
        fields = ('merchant_id', 'logo',)

    def get_logo(self, obj):

        current_site = Site.objects.get_current()
        domain_name = current_site.domain

        try:

            merchant = obj.merchant_id

            if merchant:

                merchant_instance = MerchantModel.objects.filter(name=merchant)
                merchant_obj = MerchantModel.objects.filter(name=merchant).last()

                if merchant_instance:
                    return domain_name + 'media/' + str(merchant_obj.logo)

                elif obj.category == 'Cash':

                    return domain_name + 'media/bank_logos/Cash_256x256.png'

                elif obj.category == 'UPI':

                    return domain_name + 'media/bank_logos/upi_256x256.png'

                elif obj.category == 'Wallet':

                    return domain_name + 'media/bank_logos/wallet_256x256.png'

                elif obj.category == 'Shopping':

                    return domain_name + 'media/bank_logos/shopping.png'

                elif obj.category == 'Bill':

                    return domain_name + 'media/bank_logos/bill.png'

                elif obj.category == 'Hotel':

                    return domain_name + 'media/bank_logos/hotel.png'

                elif obj.category == 'Other':

                    return domain_name + 'media/bank_logos/other.png'


                elif obj.transaction_type == 'Debit':
                    return domain_name + 'media/bank_logos/default_debit.png'
                elif obj.transaction_type == 'Credit':
                    return domain_name + 'media/bank_logos/default_credit.png'
                else:
                    return domain_name + 'media/default_images/Mudrakwik_64X64.png'

            else:

                if obj.transaction_type == 'Debit':
                    return domain_name + 'media/bank_logos/default_debit.png'
                elif obj.transaction_type == 'Credit':
                    return domain_name + 'media/bank_logos/default_credit.png'
                else:
                    return domain_name + 'media/default_images/Mudrakwik_64X64.png'

        except:

            return domain_name + 'media/default_images/Mudrakwik_64X64.png'
