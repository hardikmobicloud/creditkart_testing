import json
import sys
from functools import partial

import requests
from django.apps import apps
from django.conf import settings
from django.contrib.auth.models import User
from django.http import HttpResponse, response
from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics, status
from rest_framework.authentication import *
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from mudra.constants import *
from MApi.permissions import GuestTokenPermissionPincodeCheck
from . import user_serializers
from . import utils


import datetime
from datetime import timedelta
from sqlalchemy.engine import create_engine
engine = create_engine('postgresql+psycopg2://anandfin:XBBHxQk5r0nI@anandfin.cska70p9kqpe.ap-south-1.rds.amazonaws.com/kwik_db')



PersonalDetails = apps.get_model('User', 'Personal')
BankDetails = apps.get_model('User', 'Bank')
AddressDetails = apps.get_model('User', 'Address')
ReferenceDetails = apps.get_model('User', 'Reference')
DocumentDetails = apps.get_model('User', 'Documents')
ProfessionalDetails = apps.get_model('User', 'Professional')
AppVersionCheck = apps.get_model('StaticData', 'AppVersionCheck')
PincodeMaster = apps.get_model('StaticData', 'PincodeMaster')
PanCardMaster = apps.get_model('UserData', 'PanCardMaster')
Location = apps.get_model('User', 'Location')
DeviceData=apps.get_model('UserData','DeviceData')
Payment=apps.get_model('Payments','Payment')
RBLTransaction=apps.get_model('Payments','RBLTransaction')
LoanApplication=apps.get_model('LoanAppData','LoanApplication')
Reference=apps.get_model('User','Reference')



def application_save():
    connection = engine.connect()


    user = connection.execute('select * from auth_user')
    if user:
        for us in user:
            user_id = "'" + str(us['id']) + "'"
            status = "APPROVED"
            loan_application = connection.execute(
                'select * from "UserAccounts_userloantransaction" where status=' + status + ' and user_id=' + user_id
            )

            if loan_application:
                for row in loan_application:
                    loan_application_id = row['loan_application_id']
                    name = row['name']
                    birthdate = row['date_of_birth']
                    marital_status = row['marital_status']
                    mobile_no = row['mobile_number']
                    occupation = row['occupation']
                    pan_number = row['pan_number']
                    created_date = row['created_date']

                    user = User.objects.filter(username=us['username'])

                    PersonalDetails.objects.create(name=name, birthdate=birthdate, marital_status=marital_status,
                                                   occupation=occupation, mobile_no=mobile_no, pan_number=pan_number,
                                                   created_date=created_date, user_id=user[0].id)

                    personal = PersonalDetails.objects.filter(user_id=user[0].id).order('-id')
                    personal_id = ''
                    if personal:
                        personal_id = personal[0].id

                    # Proffessional
                    company_name = ''
                    if str(row['current_empolyer_name']) != 'None':
                        company_name = row['current_empolyer_name']
                    company_email_id = 'NULL'
                    if str(row['work_email_id']) != 'None':
                        company_email_id = row['work_email_id']
                    company_address = 'NULL'
                    if str(row['work_address']) != 'None':
                        company_address = row['work_address']
                    company_designation = 'NULL'
                    if str(row['current_designation']) != 'None':
                        company_designation = row['current_designation']
                    experience = 'NULL'
                    if str(row['total_work_experience']) != 'None':
                        experience = row['total_work_experience']
                    join_date = datetime.datetime.now()
                    if str(row['working_from']) != 'None':
                        join_date = row['working_from']

                    ProfessionalDetails.objects.filter(company_name=company_name, company_email_id=company_email_id,
                                                       company_address=company_address,
                                                       company_designation=company_designation,
                                                       created_date=created_date, user_id=user[0].id,
                                                       experience=experience, join_date=join_date)
                    professional = ProfessionalDetails.objects.filter(user_id=user[0].id).order('-id')
                    professional_id = ''
                    if professional:
                        professional_id = professional[0].id

                    # bank
                    bank_name = row['bank_name']
                    account_number = row['account_number']
                    ifsc_code = row['ifsc_code']

                    BankDetails.objects.filter(created_date=created_date, bank_name=bank_name,
                                               account_number=account_number, ifsc_code=ifsc_code, user_id=user[0].id)
                    bank = ProfessionalDetails.objects.filter(user_id=user[0].id).order('-id')
                    bank_id = ''
                    if bank:
                        bank_id = bank[0].id

                    # address
                    address = row['address']
                    pin_code = row['pin_code']
                    address_type = "Current"
                    AddressDetails.objects.create(address=address, pin_code=pin_code, type=address_type,
                                                  created_date=created_date, user_id=user[0].id)
                    address = AddressDetails.objects.filter(user_id=user[0].id).order('-id')
                    address_id = ''
                    if address:
                        address_id = address[0].id

                    # id_card_photo
                    id_card_photo = str(row['id_card_photo']).split("/")
                    id_card = ''
                    if str(row['id_card_photo']):
                        id_card = "'" + "/images/" + str(id_card_photo[1]).replace(" ", "_") + "'"
                    Company_Id = "Company_Id"
                    DocumentDetails.objects.create(type=Company_Id, path=id_card, created_date=created_date,
                                            user_id=user[0].id)
                    id_card_id = ''
                    document = DocumentDetails.objects.filter(user_id=user[0].id, type=Company_Id).order('-id')
                    if document:
                        id_card_id = document[0].id

                    # pan_card
                    pan_card_d = str(row['pan_card']).split("/")
                    pan_card = ''
                    if str(row['pan_card']):
                        pan_card = "'" + "/images/" + str(pan_card_d[1]).replace(" ", "_") + "'"
                    Pan = "Pan"
                    DocumentDetails.objects.create(type=Pan, path=pan_card, created_date=created_date,
                                            user_id=user[0].id)
                    pan_ca_id = ''
                    document_pan_ca_id = DocumentDetails.objects.filter(user_id=user[0].id, type=Pan).order('-id')
                    if document_pan_ca_id:
                        pan_ca_id = document_pan_ca_id[0].id

                    # salary_slip

                    salary_slip_s = str(row['salary_slip']).split("/")
                    salary_slip = ''
                    if str(row['salary_slip']):
                        salary_slip = "'" + "/images/" + str(salary_slip_s[1]).replace(" ", "_") + "'"
                    Salary_Slip = "Salary_Slip"
                    DocumentDetails.objects.create(type=Salary_Slip, path=salary_slip, created_date=created_date,
                                            user_id=user[0].id)
                    salary_slip_id = ''
                    document_salary_slip_id = DocumentDetails.objects.filter(user_id=user[0].id, type=Salary_Slip).order('-id')
                    if document_salary_slip_id:
                        salary_slip_id = document_salary_slip_id[0].id

                    # balanced_sheet_bs
                    balanced_sheet_bs = str(row['balanced_sheet']).split("/")
                    balanced_sheet = ''
                    if str(row['balanced_sheet']):
                        balanced_sheet = "'" + "/images/" + str(balanced_sheet_bs[1]).replace(" ", "_") + "'"
                    Bank_Statement = "Bank_Statement"
                    DocumentDetails.objects.create(type=Bank_Statement, path=balanced_sheet, created_date=created_date,
                                            user_id=user[0].id)
                    balanced_sheet_id = ''
                    document_balanced_sheet_id = DocumentDetails.objects.filter(user_id=user[0].id, type=Bank_Statement).order(
                        '-id')
                    if document_balanced_sheet_id:
                        balanced_sheet_id = document_balanced_sheet_id[0].id

                    # residence_address_proof

                    residence_address_proof_rap = str(row['residence_address_proof']).split("/")
                    residence_address_proof = ''
                    if str(row['residence_address_proof']):
                        residence_address_proof = "'" + "/images/" + str(residence_address_proof_rap[1]).replace(" ",
                                                                                                                 "_") + "'"
                    Current_Address = "Current_Address"
                    DocumentDetails.objects.create(type=Current_Address, path=residence_address_proof,
                                            created_date=created_date,
                                            user_id=user[0].id)
                    residence_address_proof_id = 'NULL'
                    document_residence_address_proof_id = DocumentDetails.objects.filter(user_id=user[0].id,
                                                                                  type=Current_Address).order('-id')
                    if document_residence_address_proof_id:
                        residence_address_proof_id = document_residence_address_proof_id[0].id

                    # self_images_d

                    user1 = connection.execute(
                        'select * from "UserProfile_userprofile" where user_id=' + user_id + 'ORDER BY id DESC')
                    for row1 in user1:
                        self_created_date = row1['created_date']
                        profile_image = str(row1['profile_image']).split("/")
                        if str(row1['profile_image']):
                            self_path = "'" + "/images/" + str(profile_image[1]).replace(" ", "_") + "'"
                        else:
                            path = ''

                        self_type = "Self_Video"
                        DocumentDetails.objects.create(type=self_type, path=self_path,
                                                created_date=self_created_date,
                                                user_id=user[0].id)
                        self_images_id = ''
                        self_images_d = DocumentDetails.objects.filter(user_id=user[0].id,
                                                                type=self_type).order(
                            '-id')
                        if self_images_d:
                            self_images_id = self_images_d[0].id

                        # refences

                    refences = Reference.objects.filter(user_id=user[0].id)
                    refences_id = ''
                    if refences:
                        for ref in refences:
                            refences_id = "'" + str(ref['id']) + "'"

                    end_date = str(datetime.datetime.now())
                    loan_start_date = datetime.datetime.now()
                    if str(row['loan_application_start_date']) != "None":
                        loan_start_date = row['loan_application_start_date']
                        end_date = row['loan_application_start_date'] + timedelta(int(row['tenure']))

                    loan_end_date = end_date
                    device_id = row['device_id']
                    device_model = row['device_model']
                    user_location = row['user_location']
                    created_date = row['created_date']
                    updated_date = row['updated_date']
                    loan_scheme_id = row['loan_repayment_amount']
                    status = row['status']
                    hold_days = "'0'"
                    if str(row['edi']) != 'None':
                        hold_days = row['edi']

                    device_data_id = 'NULL'
                    device = DeviceData.objects.filter(user_id=user_id).order(
                        '-id')
                    if device:
                        device_data_id = device[0].id

                    subsc = connection.execute(
                        'select * from "UserAccounts_rbltransaction" where transaction_id_text=' + loan_application_id)
                    for row2 in subsc:

                        order_id2 = row2['transaction_id_text']
                        transaction_id2 = row2['rrn']
                        status2 = row2['status']
                        corp_id2 = row2['corp_id']
                        maker_id2 = row2['maker_id']
                        checker_id2 = row2['checker_id']
                        approver_id2 = row2['approver_id']
                        response_code2 = row2['response_code']
                        error_description2 = row2['error_description']
                        ref_number2 = row2['ref_number']
                        channel_part_ref_number2 = row2['channel_part_ref_number']
                        ben_acc_no2 = row2['ben_acc_no']
                        ben_ifsc_code2 = row2['ben_ifsc_code']
                        category2 = "Rbl"
                        mode2 = "NB"
                        type2 = "Account"
                        response2 = ''
                        amount2 = ''
                        if str(row2['amount']) != 'None':
                            amount2 = row2['amount']
                        created_date2 = row2['created_date']
                        date2 = ''
                        if str(row2['txn_time']) != 'None':
                            date2 = row2['txn_time']
                        updated_date2 = row2['updated_date']

                        Payment.objects.create(order_id=order_id2, transaction_id=transaction_id2, status=status2,
                                               category=category2, mode=mode2, type=type2, response=response2,
                                               amount=amount2, date=date2, create_date=created_date2)
                        payment = Payment.objects.filter(order_id=order_id2).order(
                            '-id')
                        payment_id = ''
                        if payment:
                            payment_id = payment[0].id
                            RBLTransaction.objects.create(created_date=created_date2, updated_date=updated_date2,
                                                          transaction_id_text=loan_application_id, corp_id=corp_id2,
                                                          maker_id=maker_id2, checker_id=checker_id2,
                                                          approver_id=approver_id2, status=status2,
                                                          response_code=response_code2,
                                                          error_description=error_description2, ref_number=ref_number2,
                                                          channel_part_ref_number=channel_part_ref_number2,
                                                          rrn=transaction_id2, ben_acc_no=ben_acc_no2, amount=amount2,
                                                          ben_ifsc_code=ben_ifsc_code2, txn_time=date2,
                                                          payment_id=payment_id)

                    LoanApplication.objects.create(loan_application_id=loan_application_id, loan_start=loan_start_date,
                                                   loan_end_date=loan_end_date, device_id=device_id,
                                                   device_model=device_model, location=user_location,
                                                   created_date=created_date2, updated_date=updated_date2,
                                                   loan_scheme_id=loan_scheme_id, bank_id_id=bank_id,
                                                   bank_sts_id_id=balanced_sheet_id, company_id_id=id_card_id,
                                                   current_address_id=address_id, device_data_id_id=device_data_id,
                                                   disbursed_amount_id=payment_id, pancard_id_id=pan_ca_id,
                                                   reference_id_id=refences_id, salary_slip_id_id=salary_slip_id,
                                                   self_video_id=self_images_id, user_id=user[0].id, status=status,
                                                   user_personal_id=personal_id, user_professional_id=professional_id,
                                                   hold_days=hold_days, cur_address_id_id=residence_address_proof_id,
                                                   rec_status=True)

                    loan_id = LoanApplication.objects.filter(loan_application_id=loan_application_id)

                    if loan_id:
                        payments = connection.execute(
                            'SELECT * FROM payments where user_loan_application=' + loan_application_id
                        )
                        for row3 in payments:
                            transaction_id3 = ''
                            if row3['atom_transaction_number_1']:
                                transaction_id3 = row3['atom_transaction_number_1']
                            else:
                                if row3['atom_transaction_number_2']:
                                    transaction_id3 = row3['atom_transaction_number_2']
                            order_id3 = ''
                            if row3['merchant_id_resvalue']:
                                order_id3 = row['merchant_id_resvalue']
                            status3 = row['status']
                            category3 = "'Loan'"
                            mode3 = ''
                            if row3['atom_transaction_type']:
                                mode3 = row3['atom_transaction_type']
                            type3 = "'App'"
                            response3 = ''
                            if row3['atom_response']:
                                response3 = str(row3['atom_response']).replace("'", "`")
                            amount3 = ''
                            if row3['amount']:
                                amount3 = row3['amount']
                            description3 = row3['atom_description']
                            created_date3 = row3['created_date']
                            date3 = ''
                            if row3['atom_transaction_date']:
                                date3 = row3['atom_transaction_date']
                            pay = Payment.objects.create(order_id=order_id3, transaction_id=transaction_id3,
                                                         status=status3, category=category3, mode=mode3, type=type3,
                                                         response=response3, amount=amount3, date=date3,
                                                         create_date=created_date3, description=description3)

                            payment_id_data3 = Payment.objects.filter(transaction_id=transaction_id3)
                            if payment_id_data3:
                                loan_application = LoanApplication.objects.filter(
                                    loan_application_id=loan_application_id)
                                loan_application[0].repayment_amount.add(pay.id)
    connection.close()
    return True
















