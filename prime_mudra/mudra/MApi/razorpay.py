from datetime import timedelta
from datetime import datetime
import razorpay
import hmac
import hashlib
import random
from django.shortcuts import render, HttpResponse
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
# from mudra.User.models import Personal
from django.apps.registry import apps
from django.contrib.auth.models import User
import json

Payments = apps.get_model('Payments', 'Payment')
Personal = apps.get_model('User', 'Personal')
Subscription = apps.get_model("User", "Subscription")

# Test cred
# Key_id = 'rzp_test_FIEv6FzTK4cKxr'
# Key_secret = 'AUAgWyzansR434zsZDLVzTOw'

# Prod cred
Key_id = 'rzp_live_Y5lFYWm2huQpwi'
Key_secret = 'guqrrTgcrdYvM5OapnCd1SR5'


class RazorPayOrder(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        """
        Generates razor pay's order id for a transaction
        And pass to Android / React APP
        :param
        Authentication token required
            [
                {
                    "key":"Authorization",
                    "value":"Token 70b7535c875a00fa530886dcb07fccacbea76472",
                }
            ]
        :resp
        {
            "status": "1",
            "message": "Success",
            "data": {
                "order_id": "order_E0IwU12atgSak8",
                "amount": 100,
                "status": "created",
                "created_at": 1578120322,
                "currency": "INR",
                "image_url": "",
                "key_id": "rzp_test_FIEv6FzTK4cKxr"
            }
        }
        """
        ctx = {}
        resp_data = {}
        status = '3'
        message = 'Something went wrong'
        user = request.user.id

        mobile_no = User.objects.filter(id=user).values_list('username', flat=True)
        random_number = random.sample(range(99999), 1)
        ORDER_ID = str(mobile_no[0]) + 'ms' + str(random_number[0])
        data = {'amount': 5900, 'currency': 'INR', 'receipt': ORDER_ID, 'payment_capture': '1'}
        client = razorpay.Client(auth=(Key_id, Key_secret))
        order = client.order.create(data)
        if order:
            ctx['order'] = order
            resp_data = {
                'order_id': order['id'],
                'amount': str(order['amount']),
                'status': order['status'],
                'created_at': order['created_at'],
                'currency': order['currency'],
                'image_url': '',
                'key_id': Key_id
            }

            timestamp = order['created_at']
            try:
                timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')

            except Exception as e:
                print(e)
                pass

            real_amount = order['amount'] / 100

            Payments.objects.create(order_id=ORDER_ID, pg_transaction_id=resp_data['order_id'], \
                                    status="initiated", amount=real_amount, \
                                    category="Subscription", date=timestamp, return_status="rstatus", pay_source="Rpay")

            pay = Payments.objects.filter(order_id=ORDER_ID)
            if pay:
                Subscription.objects.create(payment_status="initiated", payment_id_id=pay[0].id, user_id=user)
            status = '1'
            message = 'Success'

        return Response(
            {
                'status': status,
                'message': message,
                'data': resp_data
            }
        )




def order_status(razorpay_order_id):
    resp_data = {}
    if razorpay_order_id and razorpay_order_id is not None:
        chk_order_id = razorpay_order_id
        client = razorpay.Client(auth=(Key_id, Key_secret))
        order = client.order.fetch(chk_order_id)
        if order:
            resp_data = {
                'order_id': order['id'],
                'amount': str(order['amount']),
                'amount_paid': order['amount_paid'],
                'status': order['status'],
                'receipt': order['receipt'],
                'created_at': order['created_at'],
                'currency': order['currency'],
                'image_url': '',
                'key_id': Key_id
            }

            return resp_data

    else:
        return resp_data


def payment_status(razorpay_payment_id):
    resp_data = {}
    if razorpay_payment_id and razorpay_payment_id is not None:
        chk_payment_id = razorpay_payment_id
        client = razorpay.Client(auth=(Key_id, Key_secret))
        payment = client.payment.fetch(chk_payment_id)
        if payment:
            resp_data = {
                'payment_id': payment['id'],
                'entity': payment['entity'],
                'amount': str(payment['amount']),
                'currency': str(payment['currency']),
                'status': str(payment['status']),
                'order_id': str(payment['order_id']),
                'invoice_id': payment['invoice_id'],
                'international': payment['international'],
                'method': payment['method'],
                'amount_refunded': payment['amount_refunded'],
                'refund_status': payment['refund_status'],
                'captured': payment['captured'],
                'description': payment['description'],
                'card_id': payment['card_id'],
                'bank': payment['bank'],
                'wallet': payment['wallet'],
                'vpa': payment['vpa'],
                'email': payment['email'],
                'contact': payment['contact'],
                'notes': payment['notes'],
                'fee': payment['fee'],
                'tax': payment['tax'],
                'error_code': payment['error_code'],
                'error_description': payment['error_description'],
                'created_at': payment['created_at'],
                'key_id': Key_id
            }
            return resp_data

    else:
        return resp_data








class RazorPaySignCheck(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    '''
    :param
    {
        'razorpay_payment_id'="sdsadwd"
        'razorpay_order_id' = ""
        'razorpay_signature = ''
    }
    '''

    def post(self, request, *args, **kwargs):
        message = ''
        mode = ''
        status = 0
        razorpay_payment_id = request.data.get('razorpay_payment_id')

        razorpay_order_id = request.data.get('razorpay_order_id')
        razorpay_signature = request.data.get('razorpay_signature')

        client = razorpay.Client(auth=(Key_id, Key_secret))
        params_dict = {
            'razorpay_order_id': razorpay_order_id,
            'razorpay_payment_id': razorpay_payment_id,
            'razorpay_signature': razorpay_signature
        }
        msg = "{}|{}".format(str(params_dict.get('razorpay_order_id')), str(params_dict.get('razorpay_payment_id')))

        secret = str(Key_secret)

        key = bytes(secret, 'utf-8')
        body = bytes(msg, 'utf-8')

        dig = hmac.new(key=key,
                       msg=body,
                       digestmod=hashlib.sha256)

        generated_signature = dig.hexdigest()
        result = hmac.compare_digest(generated_signature, razorpay_signature)
        if result == True:

            order_resp_data = order_status(razorpay_order_id)
            if order_resp_data and order_resp_data is not None:
                status = 1
                message = " Sign Verification Success"

            payment_data = payment_status(razorpay_payment_id)
            if payment_data and payment_data is not None:

                if payment_data['method'] == 'card':
                    mode = 'DC'
                elif payment_data['method'] == 'upi':
                    mode = 'UPI'
                elif payment_data['method'] == 'netbanking':
                    mode = 'NB'
                else:
                    mode = 'RWT'

                timestamp = payment_data['created_at']
                try:
                    timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')

                except Exception as e:
                    print(e)
                    pass

                datetime_object = datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S')
                exp_date = datetime_object + timedelta(days=89)

                first_success = Payments.objects.filter(transaction_id=None, pg_transaction_id=razorpay_order_id,
                                                        status='initiated')

                if first_success:
                    updated_pay = Payments.objects.filter(pg_transaction_id=razorpay_order_id)
                    updated_pay.update(
                        transaction_id=payment_data['payment_id'], mode=mode, type='App',
                        response=payment_data, status='success', date=timestamp)
                    pay_back = Payments.objects.filter(transaction_id=payment_data['payment_id'],
                                                       pg_transaction_id=razorpay_order_id)
                    Subscription.objects.filter(payment_id_id=pay_back[0].id).update(payment_status='success',
                                                                                     expiry_date=exp_date,
                                                                                     start_date=timestamp)


                else:
                    user = request.user.id
                    mobile_no = User.objects.filter(id=user).values_list('username', flat=True)
                    random_number = random.sample(range(99999), 1)
                    ORDER_ID = str(mobile_no[0]) + 'ms' + str(random_number[0])
                    payment_amount = int(payment_data['amount']) / 100
                    paycheck = Payments.objects.create(order_id=ORDER_ID, transaction_id=payment_data['payment_id'],
                                                       pg_transaction_id=razorpay_order_id,
                                                       status="success", amount=payment_amount,
                                                       category="Subscription", mode=mode, type='App',
                                                       response=payment_data, date=timestamp, return_status="rstatus",
                                                       pay_source="Rpay")
                    Subscription.objects.create(payment_id_id=paycheck.id, payment_status='success', user_id=user,
                                                start_date=timestamp, expiry_date=exp_date)

            return Response(
                {
                    'status': status,
                    'message': message,
                    'data': order_resp_data['status']
                }
            )
        else:
            order_resp_data = order_status(razorpay_order_id)
            if order_resp_data and order_resp_data is not None:
                status = 2
                message = " Sign Verification Failed"

            payment_data = payment_status(razorpay_payment_id)
            if payment_data and payment_data is not None:
                if payment_data['method'] == 'card':
                    mode = 'DC'
                elif payment_data['method'] == 'upi':
                    mode = 'UPI'
                elif payment_data['method'] == 'netbanking':
                    mode = 'NB'
                else:
                    mode = 'RWT'
                timestamp = payment_data['created_at']
                try:
                    timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')


                except Exception as e:
                    print(e)
                    pass

                datetime_object = datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S')
                exp_date = datetime_object + timedelta(days=89)
                if payment_data['status'] == 'captured':
                    stat = 'success'

                elif payment_data['status'] == 'authorized':
                    stat = 'pending'

                else:
                    stat = 'failed'

                first_success = Payments.objects.filter(transaction_id=None, pg_transaction_id=razorpay_order_id,
                                                        status='initiated')
                if first_success:
                    updated_pay = Payments.objects.filter(pg_transaction_id=razorpay_order_id)
                    updated_pay.update(
                        transaction_id=payment_data['payment_id'], mode=mode, type='App',
                        response=payment_data, status=stat, date=timestamp,
                        description=payment_data['error_description'])
                    pay_back = Payments.objects.filter(transaction_id=payment_data['payment_id'],
                                                       pg_transaction_id=razorpay_order_id)
                    if stat == 'success':
                        Subscription.objects.filter(payment_id_id=pay_back[0].id).update(payment_status=stat,
                                                                                         expiry_date=exp_date,
                                                                                         start_date=timestamp)
                    else:
                        Subscription.objects.filter(payment_id_id=pay_back[0].id).update(payment_status=stat, )


                else:
                    user = request.user.id
                    mobile_no = User.objects.filter(id=user).values_list('username', flat=True)
                    random_number = random.sample(range(99999), 1)
                    ORDER_ID = str(mobile_no[0]) + 'ms' + str(random_number[0])
                    payment_amount = int(payment_data['amount']) / 100

                    paycheck = Payments.objects.create(order_id=ORDER_ID, transaction_id=payment_data['payment_id'],
                                                       pg_transaction_id=razorpay_order_id,
                                                       status="success", amount=payment_amount,
                                                       category="Subscription", mode=mode, type='App',
                                                       response=payment_data, date=timestamp,
                                                       description=payment_data['error_description'],
                                                       return_status="rstatus", pay_source="Rpay")
                    if stat == 'success':
                        Subscription.objects.create(payment_id_id=paycheck.id, payment_status=stat, user_id=user,
                                                    start_date=timestamp, expiry_date=exp_date)
                    else:
                        Subscription.objects.create(payment_id_id=paycheck.id, payment_status=stat,
                                                    user_id=user)

        return Response(
            {
                'status': status,
                'message': message,
                'data': order_resp_data['status']
            }
        )


class RazorPay_FailCase(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    '''
    :param
    {
        'razorpay_order_id' = ""
    }
    '''

    def post(self, request, *args, **kwargs):
        today = datetime.now()

        razorpay_order_id = request.data.get('razorpay_order_id')


        client = razorpay.Client(auth=(Key_id, Key_secret))
        order = client.order.fetch(razorpay_order_id)
        message = 'Transaction Failed'
        pay = client.order.payments(razorpay_order_id)
        main_status = 3

        for i in pay['items']:

            timestamp = i['created_at']
            try:
                timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')

            except Exception as e:
                print(e)
                pass

            if i['method'] == 'card':
                mode = 'DC'
            elif i['method'] == 'upi':
                mode = 'UPI'
            elif i['method'] == 'netbanking':
                mode = 'NB'
            else:
                mode = 'RWT'

            if i['status'] == 'captured':
                pass
            else:
                if i['status'] == 'failed':
                    status = 'failed'

                else:
                    status = 'pending'

                first_fail = Payments.objects.filter(transaction_id=None, pg_transaction_id=razorpay_order_id,
                                                     status='initiated')
                if first_fail:
                    paycheck = Payments.objects.filter(transaction_id=None, pg_transaction_id=razorpay_order_id,
                                                       status='initiated')
                    paycheck.update(transaction_id=i['id'], status=status, date=timestamp, mode=mode, type='App',
                                    response=i, description=i['error_description'])
                    pay_back = Payments.objects.filter(transaction_id=i['id'], pg_transaction_id=razorpay_order_id)
                    Subscription.objects.filter(payment_id_id=pay_back[0].id).update(payment_status=status, )

                second_fail = Payments.objects.filter(transaction_id=i['id'], pg_transaction_id=razorpay_order_id, )
                if not second_fail:
                    user = request.user.id
                    mobile_no = User.objects.filter(id=user).values_list('username', flat=True)
                    random_number = random.sample(range(99999), 1)
                    ORDER_ID = str(mobile_no[0]) + 'ms' + str(random_number[0])

                    paycheck = Payments.objects.create(order_id=ORDER_ID, transaction_id=i['id'],
                                                       pg_transaction_id=order['id'],
                                                       status=status, amount=i['amount'] / 100,
                                                       category="Subscription", mode=mode, type='App', response=i,
                                                       date=timestamp, description=i['error_description'],
                                                       return_status="rstatus", pay_source="Rpay")
                    Subscription.objects.create(payment_status=status, payment_id_id=paycheck.id, user_id=user)

        return Response(
            {
                'status': main_status,
                'message': message,
                'data': order['status']
            }
        )


import logging

logger = logging.getLogger(__name__)


class RazorpayWebhookOrder(APIView):
    """
        PAYTM S2S
    -- Data will be posted as Form data (i.e application/x-www-form-urlencoded)
    -- URL needs to be configured at our end in order to send any S2S response
    -- Response can be posted whenever the transaction reached to the terminal stage (success/fail).
    -- Sample response:
                        {
                          "entity":"event",
                          "account_id":"acc_BFQ7uQEaa7j2z7",
                          "event":"order.paid",
                          "contains":[
                            "payment",
                            "order"
                          ],
                          "payload":{
                            "payment":{
                              "entity":{
                                "id":"pay_DESlfW9H8K9uqM",
                                "entity":"payment",
                                "amount":100,
                                "currency":"INR",
                                "status":"captured",
                                "order_id":"order_DESlLckIVRkHWj",
                                "invoice_id":null,
                                "international":false,
                                "method":"netbanking",
                                "amount_refunded":0,
                                "refund_status":null,
                                "captured":true,
                                "description":null,
                                "card_id":null,
                                "bank":"HDFC",
                                "wallet":null,
                                "vpa":null,
                                "email":"gaurav.kumar@example.com",
                                "contact":"+919876543210",
                                "notes":[],
                                "fee":2,
                                "tax":0,
                                "error_code":null,
                                "error_description":null,
                                "created_at":1567674599
                              }
                            },
                            "order":{
                              "entity":{
                                "id":"order_DESlLckIVRkHWj",
                                "entity":"order",
                                "amount":100,
                                "amount_paid":100,
                                "amount_due":0,
                                "currency":"INR",
                                "receipt":"rcptid #1",
                                "offer_id":null,
                                "status":"paid",
                                "attempts":1,
                                "notes":[],
                                "created_at":1567674581
                                }
                              }
                            },
                           "created_at":1567674606
                        }

    Details regarding Order Expiry Time:
        PFB the Time slots available for the order expiry, which can be configured at our end after your confirmation on this. Currently, there is a default timeout is configured on your MID where you can wait until the 72 hours to get the response from the bank for updated status.
        15 Minutes,
        20 Minutes
        30 Minutes,
        1 Hour,
        1 Day,
        2 Days,
        3 Days.

        status 1 : loan payment added successfully
        status 2 : payment failed
        status 3 : payment cancelled
        status 4 : payment cancelled manually
        status 5 : invalid status response (other than pre-defined)
        status 6 : invalid loan application ID
        status 10 : Something went wrong (django error)
    """

    # this method should be post
    def post(self, request, *args, **kwargs):

        request_data = {}
        status = "3"
        message = 'Something Went Wrong'
        paytm_status = ""
        payment_dict = {}

        headers = request.META
        request_data = request.data
        # request_data = {
        #     "entity": "event",
        #     "account_id": "acc_BFQ7uQEaa7j2z7",
        #     "event": "order.paid",
        #     "contains": ["payment"],
        #     "payload": {"payment": {
        #         "entity": {
        #             "id": "pay_DESlfW9H8K9uqM",
        #             "entity": "payment",
        #             "amount": 100,
        #             "currency": "INR",
        #             "status": "failed",
        #             "order_id": "order_DESlLckIVRkHWj",
        #             "invoice_id": 'null',
        #             "international": 'false',
        #             "method": "netbanking",
        #             "amount_refunded": 0,
        #             "refund_status": 'null',
        #             "captured": 'true',
        #             "description": 'null',
        #             "card_id": 'null',
        #             "bank": "HDFC",
        #             "wallet": 'nul  l',
        #             "vpa": 'null',
        #             "email": "gaurav.kumar@example.com",
        #             "contact": "+919876543210",
        #             "notes": [],
        #             "fee": 2,
        #             "tax": 0,
        #             "error_code": 'null',
        #             "error_description": 'null',
        #             "created_at": 1567674599
        #         }
        #     },
        #         "order": {
        #             "entity": {
        #                 "id": "order_DESlLckIVRkHWj",
        #                 "entity": "order",
        #                 "amount": 100,
        #                 "amount_paid": 100,
        #                 "amount_due": 0,
        #                 "currency": "INR",
        #                 "receipt": "rcptid #1",
        #                 "offer_id": 'null',
        #                 "status": "paid",
        #                 "attempts": 1,
        #                 "notes": [],
        #                 "created_at": 1567674581
        #             }
        #         }
        #     },
        #     "created_at": 1567674606
        # }

        client = razorpay.Client(auth=(Key_id, Key_secret))
        webhook_body = str(request_data)

        webhook_signature = headers['HTTP_X_RAZORPAY_SIGNATURE']
        webhook_secret = 'Mudrakwik@123'
        payload_body = json.dumps(request.data, separators=(',', ':'))


        sign_result = client.utility.verify_webhook_signature(payload_body, webhook_signature, webhook_secret)
        # Sign_result returns error if the signature doesn't match.

        # if sign_result == True:
        order_id = None
        amount = None
        status = None
        payment_id = None
        created_at = None
        if "order" in request_data['contains'] and "payment" in request_data['contains']:
            if request_data['payload']['order']['entity']['status'] == "paid":
                status = "success"

            order_id = request_data['payload']['order']['entity']['id']
            if order_id and order_id is not None:
                order_id = order_id
            amount = request_data['payload']['order']['entity']['amount']
            if amount and amount is not None:
                amount = amount
            payment_id = request_data['payload']['payment']['entity']['id']
            if payment_id and payment_id is not None:
                payment_id = payment_id

            payment_obj = Payments.objects.filter(pg_transaction_id=order_id, transaction_id=payment_id)

            if payment_obj and payment_obj is not None:
                payment_entry = Payments.objects.filter(pg_transaction_id=order_id,
                                                        transaction_id=payment_id).update(status=status,
                                                                                          return_status="rs2s",
                                                                                          pay_source="Rpay")
                update_sub = Subscription.objects.filter(payment_id=payment_obj[0]).update(payment_status=status)

        elif len(request_data['contains']) == 1 and "payment" in request_data['contains']:
            status = ''
            if request_data['payload']['payment']['entity']['status'] == "authorized":
                status = "pending"
            elif request_data['payload']['payment']['entity']['status'] == "captured":
                status = "success"
            elif request_data['payload']['payment']['entity']['status'] == "failed":
                status = "failed"

            order_id = request_data['payload']['payment']['entity']['order_id'],
            amount = request_data['payload']['payment']['entity']['amount'],
            status = status,
            payment_id = request_data['payload']['payment']['entity']['id'],

            payment_obj = Payments.objects.filter(pg_transaction_id=order_id, transaction_id=payment_id)

            if payment_obj and payment_obj is not None:
                payment_entry = Payments.objects.filter(pg_transaction_id=order_id).update(status=status,
                                                                                           return_status="rs2s",
                                                                                           pay_source="Rpay")

                update_sub = Subscription.objects.filter(payment_id=payment_obj[0]).update(payment_status=status)

        return HttpResponse('Done')


