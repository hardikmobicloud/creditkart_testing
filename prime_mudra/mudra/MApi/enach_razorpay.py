import hashlib
import hmac
from datetime import datetime
from django.apps.registry import apps
from django.contrib.auth.models import User
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework.views import APIView
from django.utils.decorators import method_decorator
import random
import razorpay
import requests
from django.shortcuts import HttpResponse
from rest_framework.permissions import *
from rest_framework.authentication import *

Personal = apps.get_model('User', 'Personal')
RazorpayCustomer = apps.get_model('LoanAppData', 'RazorpayCustomer')
RazorpayOrder = apps.get_model('LoanAppData', 'RazorpayOrder')
RazorpayToken = apps.get_model('LoanAppData', 'RazorpayToken')
Bank = apps.get_model('User', 'Bank')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
Payment = apps.get_model('Payments', 'Payment')
Document = apps.get_model('User', 'Documents')
Professional = apps.get_model('User', 'Professional')
Bank = apps.get_model('User', 'Bank')
Address = apps.get_model('User', 'Address')
Reference = apps.get_model('User', 'Reference')

# Test cred
#Key_id = 'rzp_test_p5riAqcJoED1JK'
#Key_secret = 'PQ26fiDi5sk9E1JCwRCI0wF5'


# # Prod cred
# Key_id = 'rzp_live_Euhr9VT4Z2O1U6'
# Key_secret = 'EVNAbAEH9AKNJNVZVSRwz05n'

Key_id = 'rzp_live_Y5lFYWm2huQpwi'
Key_secret = 'guqrrTgcrdYvM5OapnCd1SR5'




def payment_status(razorpay_payment_id):
    resp_data = {}
    if razorpay_payment_id and razorpay_payment_id is not None:
        chk_payment_id = razorpay_payment_id
        client = razorpay.Client(auth=(Key_id, Key_secret))
        payment = client.payment.fetch(chk_payment_id)
        if payment:
            resp_data = {
                'payment_id': payment['id'],
                'entity': payment['entity'],
                'amount': str(payment['amount']),
                'currency': str(payment['currency']),
                'status': str(payment['status']),
                'order_id': str(payment['order_id']),
                'invoice_id': payment['invoice_id'],
                'international': payment['international'],
                'method': payment['method'],
                'amount_refunded': payment['amount_refunded'],
                'refund_status': payment['refund_status'],
                'captured': payment['captured'],
                'description': payment['description'],
                'card_id': payment['card_id'],
                'bank': payment['bank'],
                'wallet': payment['wallet'],
                'vpa': payment['vpa'],
                'email': payment['email'],
                'contact': payment['contact'],
                'notes': payment['notes'],
                'fee': payment['fee'],
                'tax': payment['tax'],
                'error_code': payment['error_code'],
                'error_description': payment['error_description'],
                'created_at': payment['created_at'],
                'key_id': Key_id
            }
            return resp_data

    else:
        return resp_data




class RazorCustomerCreate(APIView):
    """
           This Api is to Create Users razorpay orders
            Url: api/razor_customer_create/
             Authentication required
              [
                  {
                      "key_id": "rzp_test_p5riAqcJoED1JK",
                      "Key_secret":"PQ26fiDi5sk9E1JCwRCI0wF5",
                  }
              ]
              method = post
              Success Response:
                  {
                    "status": "1",
                    "data": {
                        "id": "cust_GZQFtcTga8eYba",
                        "entity": "customer",
                        "name": "mudra",
                        "email": "mudra@gmail.com",
                        "contact": "7875583679",
                        "gstin": null,
                        "notes": {
                            "notes_key_1": "Tea, Earl Grey, Hot",
                            "notes_key_2": "Tea, Earl Grey… decaf."
                        },
                        "created_at": 1612862153
                    }
                }
              if status = 2  # User Data NOT AVAILABLE

          """
    authentication_classes = (TokenAuthentication,)

    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):


        user_data = Personal.objects.filter(user_id=request.user.id, type="loan").last()

        if user_data:
            name = user_data.name
            contact = user_data.user.username
            email = user_data.email_id

            header = {
                "Content-Type": "application/json"
            }
            api_body = {
                "name": name,
                "email": email,
                "contact": contact,
                "fail_existing": "0",
                "notes": {
                    "notes_key_1": "Mudrakwik,fintect",
                    "notes_key_2": "Mudrakwik,fintect"
                }
            }

            prod_url2 = "https://api.razorpay.com/v1/customers"
            import json
            json_body = json.dumps(api_body)

            res = requests.post(prod_url2, data=json_body, headers=header, auth=(Key_id, Key_secret))
            data = res.json()
            user_create_entry_already_exist =RazorpayCustomer.objects.filter(personal=user_data)
            if not user_create_entry_already_exist:
                RazorpayCustomer.objects.create(personal=user_data, customer_id=data["id"],
                                                fail_existing=api_body["fail_existing"], response=data)
                return Response(
                    {
                        "status": "1",
                        "customer_id": data["id"]
                    }
                )
            else:
                return Response(
                    {
                        "status": "2",
                        "customer_id": data["id"],
                        "message": "customer entry already exist"
                    }
                )


        else:
            return Response(
                {
                    "status": "3",
                    "message": "user data not available"
                }
            )


@method_decorator(csrf_exempt, name='dispatch')
class CreateRazorpayOrder(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
         This Api is to Create Users razorpay orders

          Url: api/create_razorpay_order/

           Authentication required

            [
                {
                    "key_id": "rzp_test_p5riAqcJoED1JK",
                    "Key_secret":"PQ26fiDi5sk9E1JCwRCI0wF5",
                }
            ]

            method = post

            request_parameter = method

            Success Response:
                {
                'status': 1,
                'order' : {
                    "id": "order_GZPXZNep8GXJjO",
                    "entity": "order",
                    "amount": 0,
                    "amount_paid": 0,
                    "amount_due": 0,
                    "currency": "INR",
                    "receipt": "2129143552106",
                    "payments": {
                        "entity": "collection",
                        "count": 0,
                        "items": []
                    },
                    "offer_id": "null",
                    "status": "created",
                    "attempts": 0,
                    "notes": [],
                    "created_at": 1612859635,
                    "token": {
                        "method": "emandate",
                        "notes": [],
                        "recurring_status": 'null',
                        "failure_reason": "null",
                        "currency": "INR",
                        "max_amount": 100000,
                        "auth_type": "netbanking",
                        "expire_at": 1612859635,
                        "bank_account": {
                            "ifsc": "HDFC0000001",
                            "bank_name": "HDFC Bank",
                            "name": "Gaurav Kumar",
                            "account_number": "1121431121541121",
                            "account_type": "savings",
                            "beneficiary_email": "gaurav.kumar@example.com",
                            "beneficiary_mobile": "9123456780"
                        },
                        "first_payment_amount": 0
                    }
                }
            }

            if status = 2  # Error in key Id and Key secret
               status = 3 # Order Not Created
               status = 4 # Data Not Found(Bank & Personal Data)
               status = 5 # Error From Razorpay Side
               status = 6 # Wrong Payment Method


        """
        user_id = request.user.id


        customer_id = None
        beneficiary_name = None
        account_number = None
        ifsc_code = None
        bank_id=None
        mobile_no=None
        email_id=None

        payment_method = str(request.data.get('method'))
        if payment_method:

            # Check user in RazorpayCustomer
            customer_obj = RazorpayCustomer.objects.filter(personal__user_id=user_id).last()
            if customer_obj:
                customer_id = customer_obj.customer_id
                beneficiary_name = customer_obj.personal.name
                mobile_no = request.user.username
                email_id = customer_obj.personal.email_id

            # Check Bank Details of User
            loan = LoanApplication.objects.filter(user=user_id,status="APPROVED").last()
            if loan:
                account_number = loan.bank_id.account_number
                ifsc_code = loan.bank_id.ifsc_code
                bank_id = loan.bank_id.id



            # Create receipt
            date = datetime.now()
            random_number = random.sample(range(0, 9999), 1)
            receipt_id = str(date.year)[2:] + "" + str(date.month) + "" + str(date.day) + "" + str(
                date.hour) + "" + str(date.minute) + "" + str(date.second) + "" + str(random_number[0])

            # Create expiry date
            import time
            expire_date = int(time.mktime(date.timetuple()))

            if customer_obj and loan and receipt_id and expire_date:
                if payment_method == "NetBanking":
                    data = {
                        "amount": 0,
                        "currency": "INR",
                        "method": "emandate",
                        "payment_capture": "1",
                        "customer_id": customer_id,
                        "receipt": receipt_id,
                        "notes": {

                        },
                        "token": {
                            "auth_type": "netbanking",
                            "max_amount": 100000,
                            "expire_at": expire_date,
                            "notes": {

                            },
                            "bank_account": {
                                "beneficiary_name": beneficiary_name,
                                "account_number": account_number,
                                "account_type": "savings",
                                "ifsc_code": ifsc_code
                            }
                        }
                    }

                elif payment_method == "Cards":
                    data = {
                        "amount": 100,
                        "currency": "INR",
                        "payment_capture": 1,
                        "receipt": receipt_id,
                        "notes": {

                        }
                    }

                elif payment_method == "UPI":
                    data = {
                        "amount": 100,
                        "currency": "INR",
                        "customer_id": customer_id,
                        "method": "upi",
                        "payment_capture": 1,
                        "token": {
                            "max_amount": 200000,
                            "expire_at": 2709971120,
                            "frequency": "monthly"
                        },
                        "receipt": receipt_id,
                        "notes": {
                        }
                    }

                else:
                    return Response({
                        'status': 6,
                        'message': "Wrong Payment Method"
                    })

                mode = ""
                if payment_method:
                    if payment_method == "NetBanking":
                        mode = "NB"
                    elif payment_method == "Cards":
                        mode = "DC"
                    elif payment_method == "UPI":
                        mode = "UPI"
                    else:
                        mode = ""

                client = razorpay.Client(auth=(Key_id, Key_secret))
                if not client:
                    return Response({
                        "status": 2,
                        "message": "Error in key Id and Key secret"
                    })

                try:
                    orders = client.order.create(data)
                    if orders:
                        auth_type = None
                        pg_transaction_id = orders['id']
                        amount = orders['amount']
                        status = orders['status']
                        type = "AU_ORDER"
                        order_id = orders['receipt']

                        new_amount = float(float(amount) / 100)

                        create_payment_entry = Payment.objects.create(order_id=receipt_id, date=datetime.now(), type="App",
                                                              status='initiated', mode=mode,amount=1,pg_transaction_id=pg_transaction_id)

                        create_intiated_entry = RazorpayOrder.objects.create(customer_id=customer_obj,
                                                                     order_id=receipt_id,
                                                                     amount=1,
                                                                     bank_id=bank_id,
                                                                     type="AU_ORDER",
                                                                     auth_type=mode,
                                                                     pg_transaction_id=pg_transaction_id,
                                                                     response=orders,
                                                                     status="initiated", payment=create_payment_entry)

                        return Response({
                            "status": 1,
                            "key":Key_id,
                            "order": orders,
                            "mobile_no": mobile_no,
                            "email_id": email_id,
                            "customer_id":customer_id
                        })

                    else:
                        return Response({
                            "status": 3,
                            "message": "Order Not Created"
                        })

                except Exception as e:
                    return Response({
                        'status': 5,
                        'message': e.args
                    })

            else:
                return Response({
                    "status": 4,
                    "message": "Data Not Found"
                })

        else:
            return Response({
                'status': 6,
                'message': "Wrong Payment Method"
            })


@method_decorator(csrf_exempt, name='dispatch')
class CallbackUpdateAuthorizationPayment(APIView):
    authentication_classes = (TokenAuthentication,)

    permission_classes = (IsAuthenticated,)
    def post(self, request, *args, **kwargs):
        """
        This Api is to update the payment status in RazorpayOrder table

        Url: api/call_back_update_authorization_payment/

        Authentication required

            [
                {
                    "key_id": "rzp_test_p5riAqcJoED1JK",
                    "Key_secret":"PQ26fiDi5sk9E1JCwRCI0wF5",
                }
            ]

        method = post

        Success Response:
                {
                'status': 1,
                'pay_status' : captured

            }

        if status = 2  # Error From Razorpay Side(Payment Id Not Found)
           status = 3 # Razorpay Order Id Not Found...

        """
        status = None
        amount = None

        # Get Razorpay Payment ID
        razorpay_payment_id = str(request.data.get('razorpay_payment_id'))

        # Get Razorpay Order Id
        razorpay_order_id = str(request.data.get('razorpay_order_id'))

        # Get Razorpay Signature
        razorpay_signature = str(request.data.get('razorpay_signature'))

        # Check order id in RazorpayOrder table
        razorpay_order_obj = RazorpayOrder.objects.filter(pg_transaction_id=razorpay_order_id)
        if razorpay_order_obj:
            try:
                payment_data = payment_status(razorpay_payment_id)
                if payment_data:
                    status = payment_data['status']
                    if status == 'captured':
                        pay_status = 'success'
                        status_code = 1
                        message ="Tranaction success"

                    elif status == 'authorized':
                        pay_status = 'pending'
                        status_code = 2
                        message = "Tranaction pending"

                    else:
                        status_code = 3
                        pay_status = 'failed'
                        message = "Tranaction failed"


                    amount = payment_data['amount']
                    new_amount = float(float(amount) / 100)
                    RazorpayOrder.objects.filter(pg_transaction_id=razorpay_order_id).update(
                                                 transaction_id=razorpay_payment_id,
                                                status=pay_status, amount=new_amount, updated_date=datetime.now())
                    Payment.objects.filter(pg_transaction_id=razorpay_order_id, type="App",status='initiated').update(transaction_id=razorpay_payment_id,status=pay_status, amount=new_amount, update_date=datetime.now())

                    return Response({
                        "status": status_code,
                        "pay_status": pay_status,
                        "message":message
                     })

            except Exception as e:
                return Response({
                    "status": 2,
                    "message": e.args
                })
        return Response({
            "status": 3,
            "message": "Razorpay Order Id Not Found..."
        })


def order_to_charge_customer_api(loan_application_id, amount, payment_obj):
    """
    Razorpy API to charge customer

     Success Response:
             {
            "id": "order_1Aa00000000002",
            "entity": "order",
            "amount": 1000,
            "amount_paid": 0,
            "amount_due": 1000,
            "currency": "INR",
            "receipt": "Receipt No. 1",
            "offer_id": null,
            "status": "created",
            "attempts": 0,
            "notes": {
              "notes_key_1":"Tea, Earl Grey, Hot",
              "notes_key_2":"Tea, Earl Grey… decaf."
            }
            "created_at": 1579782776
        }

    """
    msg = "Something Went Wrong"
    ctx = {}
    if loan_application_id and amount > 0:
        loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id).first()
        if loan_obj:
            loan_user = loan_obj.user
            if loan_user:
                # Check user in RazorpayCustomer
                customer_obj = RazorpayCustomer.objects.filter(personal__user_id=loan_user).last()
                if customer_obj:
                    date = datetime.now()

                    auth_type = ""
                    get_auth_type = RazorpayOrder.objects.filter(customer_id=customer_obj.id, type='AU_ORDER').last()
                    if get_auth_type:
                        auth_type = get_auth_type.auth_type

                    # Create receipt
                    random_number = random.sample(range(0, 9999), 1)
                    receipt_id = str(date.year)[2:] + "" + str(date.month) + "" + str(date.day) + "" + str(
                        date.hour) + "" + str(date.minute) + "" + str(date.second) + "" + str(random_number[0])

                    create_entry = RazorpayOrder.objects.create(order_id=receipt_id, type='SU_ORDER',
                                                                customer_id=customer_obj,
                                                                auth_type=auth_type, status='initiated',
                                                                payment=payment_obj)

                    notes_key_1 = loan_application_id
                    notes_key_2 = payment_obj.order_id
                    data = {
                        "amount": amount * 100,
                        "currency": "INR",
                        "payment_capture": "1",
                        "receipt": receipt_id,
                        "notes": {
                            "notes_key_1": notes_key_1,
                            "notes_key_2": notes_key_2
                        }
                    }

                    client = razorpay.Client(auth=(Key_id, Key_secret))
                    if not client:
                        msg = "Error in key Id and Key secret"
                    try:
                        rsp = client.order.create(data)
                        transaction_check = check_recurring_payment(create_entry.id)
                        if "msg" in transaction_check:
                            print("Error in Transaction", transaction_check['msg'])
                        elif "razorpay_payment_id" in transaction_check:
                            print(transaction_check)
                        else:
                            pass
                        return rsp
                    except Exception as e:
                        msg = e
    ctx['msg'] = msg
    return ctx


def check_recurring_payment(razorpay_order_id):
    """
    This function is used to validate the payment
    """
    ctx = {}
    msg = "Something Went Wrong"
    order_obj = RazorpayOrder.objects.filter(id=razorpay_order_id).first()
    if order_obj:
        amount = None
        order_id = None
        token = None
        customer_id = None
        email = None
        contact = None

        amount = order_obj.amount
        order_id = order_obj.pg_transaction_id
        token_obj = order_obj.razorpay_order.last()
        if token_obj:
            token = token_obj.token_id
        if order_obj.customer_id:
            customer_id = order_obj.customer_id.customer_id
            if order_obj.customer_id.personal:
                email = order_obj.customer_id.personal.email_id
                contact = order_obj.customer_id.personal.user.username

        data = {
            "email": email,
            "contact": contact,
            "amount": amount,
            "currency": "INR",
            "order_id": order_id,
            "customer_id": customer_id,
            "token": token,
            "recurring": "1",
            "description": "",
            "notes": {}
        }

        client = razorpay.Client(auth=(Key_id, Key_secret))
        try:
            pay = client.payment.fetch(data)
            return pay
        except Exception as e:
            msg = e
    else:
        msg = "Razorpay Order Not Found"
    ctx['msg'] = msg
    return ctx


import json


class EnachRazorpayWebHook(APIView):

    def post(self, request, *args, **kwargs):
        '''
        THis is the web hook for e-nach razorpay
        this webhook will update the status of token
        '''



        headers = request.META
        request_data = request.data

        client = razorpay.Client(auth=(Key_id, Key_secret))
        webhook_signature = headers['HTTP_X_RAZORPAY_SIGNATURE']
        webhook_secret = 'Mudrakwik@123'
        payload_body = json.dumps(request.data, separators=(',', ':'))

        sign_result = client.utility.verify_webhook_signature(payload_body, webhook_signature, webhook_secret)
        sign_result = True

        if sign_result:

            if "payload" in request_data and "token" in request_data["payload"]:
                res_token = request_data["payload"]["token"]["entity"]["id"]
                if res_token:
                    raz_token = RazorpayToken.objects.filter(token_id=res_token).first()
                    if raz_token:
                        token_status = request_data["payload"]["token"]["entity"]["recurring_details"]["status"]
                        RazorpayToken.objects.filter(token_id=res_token).update(status=token_status)

                        return HttpResponse("updated successfully")

        return HttpResponse("fail")

class FetchTokenCustomerId(APIView):
    authentication_classes = (TokenAuthentication,)

    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        user_id = request.user.id
        token_status = None

        customer_obj = RazorpayCustomer.objects.filter(personal__user_id=user_id).last()
        if customer_obj:
           customer_id  = customer_obj.customer_id


           prod_url2 = "https://api.razorpay.com/v1/customers/" + customer_id + "/tokens"

           res = requests.request('GET',prod_url2, auth=(Key_id, Key_secret))
           data = res.json()
           items = data["items"]
           if items:
               for new_data in items:
                   token_id = new_data["id"]
                   token = new_data["token"]
                   recurring_details = new_data["recurring_details"]
                   if recurring_details:
                        token_status = recurring_details['status']

                   razorpay_token=RazorpayToken.objects.filter(token_id=token_id)
                   if razorpay_token:
                       RazorpayToken.objects.filter(token_id=token_id).update(token=token, status=token_status,
                                                    customer_id=data["id"], response=data)
                   else:
                        RazorpayToken.objects.create(token_id=token_id,token=token,status=token_status,
                                                customer_id=data["id"],response=data)



                   return Response(
                        {
                            "status": "1",
                            "data": res.json()
                        }
                    )
           else:
               return Response(
                   {
                       "status": "2",
                       "data": res.json()
                   }
               )



class FetchTokenPaymentId(APIView):
    authentication_classes = (TokenAuthentication,)

    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        user_id = request.user.id
        token_status = None
        payment_obj = RazorpayOrder.objects.filter(customer_id__personal__user_id=user_id).last()
        if payment_obj:
            payment_id = payment_obj.transaction_id
            prod_url2 = "https://api.razorpay.com/v1/payments/" + payment_id


            res = requests.request('GET', prod_url2, auth=(Key_id, Key_secret))

            data = res.json()

            razorpay_order = RazorpayOrder.objects.update(pg_transaction_id=data["id"], order_id=data["order_id"],
                                         customer_id=data["customer_id"], response=data)
            if razorpay_order:
                 RazorpayToken.objects.create(razorpay_order=razorpay_order.id,token_id=data["token_id"],response=data,status=token_status)
                 cutomer_token(user_id)
                 return Response(
                     {
                         "status": "1",
                         "data": res.json()
                     }
                 )

            else:
                cutomer_token(user_id)
                return Response(
                    {
                        "status": "2",
                        "data": res.json()
                    }
                )







def cutomer_token(user_id):
    token_status = None

    customer_obj = RazorpayCustomer.objects.filter(personal__user_id=user_id).last()
    if customer_obj:
        customer_id = customer_obj.customer_id

        prod_url2 = "https://api.razorpay.com/v1/customers/" + customer_id + "/tokens"

        res = requests.request('GET', prod_url2, auth=(Key_id, Key_secret))
        data = res.json()
        items = data["items"]
        if items:
            for new_data in items:
                token_id = new_data["id"]
                token = new_data["token"]
                recurring_details = new_data["recurring_details"]
                if recurring_details:
                    token_status = recurring_details['status']
                razorpay_con = RazorpayToken.objects.filter(token_id=token_id, status="confirmed")

                if not razorpay_con:
                    razorpay_token = RazorpayToken.objects.filter(token_id=token_id)
                    if razorpay_token:
                        RazorpayToken.objects.filter(token_id=token_id).update(token=token, status=token_status,
                                                                               customer_id=data["id"], response=data)
                    else:
                        RazorpayToken.objects.create(token_id=token_id, token=token, status=token_status,
                                                     customer_id=data["id"], response=data)

                return Response(
                    {
                        "status": "1",
                        "data": res.json()
                    }
                )
        else:
            return Response(
                {
                    "status": "2",
                    "data": res.json()
                }
            )


def fetch_delete_token(customer_id,token_id):
    prod_url2 = "https://api.razorpay.com/v1/customers/" + customer_id + "/tokens/" + token_id

    res = requests.request('GET', prod_url2, auth=(Key_id, Key_secret))

    data = res.json()

    return data





def payment_token(user_id):
    token_status = None
    payment_obj = RazorpayOrder.objects.filter(customer_id__personal__user_id=user_id).last()
    if payment_obj:
        payment_id = payment_obj.transaction_id
        if payment_id:
            prod_url2 = "https://api.razorpay.com/v1/payments/" + payment_id

            res = requests.request('GET', prod_url2, auth=(Key_id, Key_secret))

            data = res.json()
            if "id" in data:
                razorpay_order = RazorpayOrder.objects.filter(transaction_id=data["id"], customer_id__personal__user_id=user_id)
                if razorpay_order:
                    razorpay_tok = RazorpayToken.objects.filter(razorpay_order_id=razorpay_order[0].id)
                    if not razorpay_tok:
                        if "token_id" in data:
                            RazorpayToken.objects.create(razorpay_order_id=razorpay_order[0].id, token_id=data["token_id"],
                                                     response=data, status=token_status)
                        return True
                    else:
                        return True
                else:
                    return False

            else:
                return False
        else:
            return False
    else:
        return False






class CheckStatus(APIView):
    authentication_classes = (TokenAuthentication,)

    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        personal_status = 0
        document_status = 0
        loan_status = 0
        enach_status = 0
        credit_status = 0
        message = None
        loan_status_message = None
        loan_application_id = None
        loan_status_agr = 0
        user_id = request.user.id
        personal = Personal.objects.filter(user_id=user_id).last()
        professional=Professional.objects.filter(user_id=user_id).last()
        address=Address.objects.filter(user_id=user_id,type__in=["Current","Permanent"]).last()
        reference=Reference.objects.filter(user_id=user_id).last()
        bank=Bank.objects.filter(user_id=user_id).last()
        if personal and professional and address and reference and bank:
            personal_status = 1
        from ecom.views import get_order_details_remaining_amount
        repaymet_dict = get_order_details_remaining_amount(user_id)
        if repaymet_dict:
            for key, value in repaymet_dict.items():
                if key == "Amount":
                    TXN_AMOUNT = value

                    if TXN_AMOUNT > 0 :
                        credit_status = 1


        pan_document = Document.objects.filter(user_id=user_id, type='Pan').order_by('-id').first()
        salary_slip_document = Document.objects.filter(user_id=user_id, type='Salary_Slip').order_by('-id').first()
        document_company_id = Document.objects.filter(user_id=user_id, type='Company_Id').order_by('-id').first()
        document_self_video = Document.objects.filter(user_id=user_id, type='Self_Video').order_by('-id').first()
        document_aadhar_front = Document.objects.filter(user_id=user_id, type='Aadhar_Front').order_by('-id').first()
        document_aadhar_back = Document.objects.filter(user_id=user_id, type='Aadhar_Back').order_by('-id').first()

        if pan_document and salary_slip_document and document_self_video and document_aadhar_back and document_aadhar_front and document_company_id:
            document_status = 1


        loan = LoanApplication.objects.filter(user_id=user_id).last()
        if loan :
            if loan.status == "APPROVED":
                loan_status = 1
                loan_status_message = loan.status
                loan_application_id = loan.loan_application_id
                if loan.user_status == "Accept":
                    loan_status_agr = 1
                elif loan.user_status == "Reject":
                    loan_status_agr = 2
                    loan_status = 4
            elif loan.status == "SUBMITTED" or loan.status == "PRE_APPROVED" and loan.status == "PROCESS":
                loan_status = 2
                loan_status_message = loan.status
            elif loan.status == "PENDING" or loan.status == "ON_HOLD":
                loan_status = 3
                loan_status_message = loan.status
            else:
                loan_status = 4
                loan_status_message = loan.status


        razorpay=RazorpayOrder.objects.filter(customer_id__personal__user_id=user_id,type="AU_ORDER",status="success").last()
        if not razorpay:
            try:
                pay = payment_token(user_id)
            except:
                pass
        if razorpay:
            try:
                cutomer_token(user_id)
            except:
                pass

            customer_obj = RazorpayCustomer.objects.filter(personal__user_id=user_id).last()
            if customer_obj:
                customer_id = customer_obj.id
                razorpaytoken=RazorpayToken.objects.filter(razorpay_order__customer_id=customer_id,status="confirmed")
                if razorpaytoken:
                    enach_status = 1
                else:
                    enach_status = 2
        enach_status = 1
        return Response(
            {
                "personal_status": personal_status,
                "document_status": document_status,
                "loan_status":loan_status,
                "enach_status":enach_status,
                "loan_status_message":loan_status_message,
                "message":message,
                "loan_status_agr":loan_status_agr,
                "loan_application_id":loan_application_id,
                "credit_status": credit_status


            }
        )








