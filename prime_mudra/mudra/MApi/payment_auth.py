import base64
import datetime
import hashlib
import json
import os
import random
import string
import sys
from datetime import timedelta

import requests

from django.apps.registry import apps
from django.conf import settings
from Crypto.Cipher import AES
from django.contrib.auth.models import User
from django.db.models import Sum
from django.shortcuts import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import *
from rest_framework.permissions import *
from rest_framework.response import Response
from rest_framework.views import APIView

from .email import sms
from .fcm_notifications import fcm_simple_notification
from .user_sharedata import share_subdata
from rest_framework import status

Payment = apps.get_model('Payments', 'Payment')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
Subscription = apps.get_model('User', 'Subscription')
Recovery = apps.get_model('Recovery', 'Recovery')
AutoApproved = apps.get_model('LoanAppData', 'AutoApproved')


def loan_application_dpd(user_id):
    Today_date = datetime.datetime.now()
    int_dbd_total = 0
    try:
        user = LoanApplication.objects.filter(user_id=user_id, status__in=['APPROVED', 'COMPLETED'])
        for users in user:
            payment_return = users.repayment_amount.filter(status='success').order_by('-id').first()

            if users.status == 'APPROVED':
                defaulted_by_date = Today_date - users.loan_end_date
                int_dbd = defaulted_by_date.days
                if int_dbd > 0:
                    int_dbd_total += int_dbd
            else:
                defaulted_by_date = payment_return.date - users.loan_end_date
                int_dbd = defaulted_by_date.days
                if int_dbd > 0:
                    int_dbd_total += int_dbd
    except:
        pass
    return int_dbd_total


# m_id = 'Mudrak01031243312800'

# merchant_key = 'f_00Gtw7B2pn67#j'

IV = "@@@@&&&&####$$$$"
BLOCK_SIZE = 16


def generate_checksum(param_dict, merchant_key, salt=None):
    params_string = __get_param_string__(param_dict)
    salt = salt if salt else __id_generator__(4)
    final_string = '%s|%s' % (params_string, salt)

    hasher = hashlib.sha256(final_string.encode())
    hash_string = hasher.hexdigest()

    hash_string += salt

    return __encode__(hash_string, IV, merchant_key)


def generate_refund_checksum(param_dict, merchant_key, salt=None):
    for i in param_dict:
        if ("|" in param_dict[i]):
            param_dict = {}
            exit()
    params_string = __get_param_string__(param_dict)
    salt = salt if salt else __id_generator__(4)
    final_string = '%s|%s' % (params_string, salt)

    hasher = hashlib.sha256(final_string.encode())
    hash_string = hasher.hexdigest()

    hash_string += salt

    return __encode__(hash_string, IV, merchant_key)


def generate_checksum_by_str(param_str, merchant_key, salt=None):
    params_string = param_str
    salt = salt if salt else __id_generator__(4)
    final_string = '%s|%s' % (params_string, salt)

    hasher = hashlib.sha256(final_string.encode())
    hash_string = hasher.hexdigest()

    hash_string += salt

    return __encode__(hash_string, IV, merchant_key)


def verify_checksum(param_dict, merchant_key, checksum):
    # Remove checksum
    if 'CHECKSUMHASH' in param_dict:
        param_dict.pop('CHECKSUMHASH')

    # Get salt
    paytm_hash = __decode__(checksum, IV, merchant_key)
    salt = paytm_hash[-4:]
    calculated_checksum = generate_checksum(param_dict, merchant_key, salt=salt)
    return calculated_checksum == checksum


def verify_checksum_by_str(param_str, merchant_key, checksum):
    # Remove checksum
    # if 'CHECKSUMHASH' in param_dict:
    # param_dict.pop('CHECKSUMHASH')

    # Get salt
    paytm_hash = __decode__(checksum, IV, merchant_key)
    salt = paytm_hash[-4:]
    calculated_checksum = generate_checksum_by_str(param_str, merchant_key, salt=salt)
    return calculated_checksum == checksum


def __id_generator__(size=6, chars=string.ascii_uppercase + string.digits + string.ascii_lowercase):
    return ''.join(random.choice(chars) for _ in range(size))


def __get_param_string__(params):
    params_string = []
    for key in sorted(params.keys()):
        if ("REFUND" in params[key] or "|" in params[key]):
            respons_dict = {}
            exit()
        value = params[key]
        params_string.append('' if value == 'null' else str(value))
    return '|'.join(params_string)


__pad__ = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * chr(BLOCK_SIZE - len(s) % BLOCK_SIZE)
__unpad__ = lambda s: s[0:-ord(s[-1])]





def __encode__(to_encode, iv, key):
    to_encode = __pad__(to_encode)
    # Encrypt
    c = AES.new(key.encode('UTF-8'), AES.MODE_CBC, iv.encode('UTF-8'))
    to_encode = c.encrypt(to_encode.encode('UTF-8'))
    # Encode
    to_encode = base64.b64encode(to_encode)
    return to_encode.decode("UTF-8")


def __decode__(to_decode, iv, key):
    # Decode
    to_decode = base64.b64decode(to_decode)
    # Decrypt
    c = AES.new(key, AES.MODE_CBC, iv)
    to_decode = c.decrypt(to_decode)
    if type(to_decode) == bytes:
        # convert bytes array to str.
        to_decode = to_decode.decode()
    # remove pad
    return __unpad__(to_decode)


def get_auth(encoded):
    '''
    string='7875583679|Mudrakwik|Paytm|Mudrak01031243312800'
    encoded = base64.b64encode(bytes(string, 'utf-8'))

    token:  b'Nzg3NTU4MzY3OXxNdWRyYWt3aWt8UGF5dG18TXVkcmFrMDEwMzEyNDMzMTI4MDA='

    :param encoded:
    :return:
    '''
    # string='9786152782|Mudrakwik|Paytm|Mudrak01031243312800'
    # encoded = base64.b64encode(bytes(string, 'utf-8'))
    data = base64.b64decode(encoded)
    data1 = data.decode('utf-8')
    user_data = data1.split("|")
    if user_data[0] != '' and user_data[1] == 'Mudrakwik' and user_data[2] == 'Paytm' and user_data[
        3] == 'Mudrak01031243312800':
        user = User.objects.get(username=user_data[0])
        if str(user) == user_data[0]:
            return "True"
        else:
            return "False"

    else:
        return "Invaild"


class PaytmPaymentAPIView(APIView):
    """
        PAYTM ON-US
    """

    def post(self, request, *args, **kwargs):

        status = None
        message = None

        try:
            # paytm response
            auth = request.data.get('auth_token')
            auth_resp = get_auth(auth)
            loan_application_id = request.data.get('loan_application_id')
            paytm_txn_id = request.data.get('txt_nid')
            o_id = request.data.get('oid')
            amount = request.data.get('amount')
            status = request.data.get('status')
            date = request.data.get('date')
            payment_mode = request.data.get('paymentmode')
            paytm_responce = request.data

            paytm_status = ""
            # get last atom entry of particular loan application id
            if auth_resp == "True":
                loan_txn_instance = LoanApplication.objects.filter(loan_application_id=loan_application_id,
                                                                   status="APPROVED")
                if loan_txn_instance:

                    # paytm transaction success
                    if status == 'TXN_SUCCESS' and paytm_responce['status'] == 'TXN_SUCCESS':
                        paytm_status = 'success'
                    elif status == 'TXN_FAILURE':
                        paytm_status = 'failed'
                    elif status == 'PENDING':
                        paytm_status = 'pending'

                    if date:
                        date_strip = date
                    else:
                        date_strip = datetime.datetime.now()

                    pay = Payment.objects.create(order_id=o_id,
                                                 status=paytm_status, response=paytm_responce,
                                                 transaction_id=paytm_txn_id, category='Loan',
                                                 mode=payment_mode, type='On_Us',
                                                 date=date_strip, amount=amount
                                                 )

                    loan_application = LoanApplication.objects.filter(loan_application_id=loan_application_id)
                    loan_application[0].repayment_amount.add(pay.id)

                    if paytm_status == 'success':
                        kwargs = {
                            'loan_application_id': loan_application_id,
                            'atom_amount': amount,
                        }

                        # notification
                        sm_kwargs = {
                            'user_id': loan_txn_instance[0].user_id,
                            'title': 'Transaction successful',
                            'body': 'Dear user, your transaction of ' + str(
                                amount) + ' has been processes successfully.',
                            'notification_type': 'paytm_success',
                            'to_store': True
                        }

                        fcm_simple_notification(**sm_kwargs)



                        rep_amount = loan_txn_instance[0].repayment_amount.filter(
                            status='success').aggregate(Sum('amount'))['amount__sum']

                        if rep_amount != None:


                            if rep_amount >= float(loan_txn_instance[0].loan_scheme_id):

                                LoanApplication.objects.filter(
                                    loan_application_id=loan_application_id
                                ).update(status='COMPLETED')


                        status = '1'
                        message = 'Transaction successful.'

                    elif paytm_status == 'failed':

                        status = '2'
                        message = 'Transaction failed'

                    elif paytm_status == 'pending':

                        status = '3'
                        message = 'Transaction pending'


                    else:

                        status = '5'
                        message = 'Invalid loan application id'
                else:

                    status = '6'
                    message = 'Invalid loan application id'


        except Exception as e:

            import sys
            import os
            print('-----------in posting exception----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)

            sms_message = 'in paytm  mobile api producation ex' + str(e.args) + '\n' + str(exc_tb.tb_lineno)
            number = '6352820504'
            sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
                settings.SMS_USERNAME) + '&password=' + str(settings.SMS_PASSWORD) + \
                      '&type=0&dlr=1&destination=' + number + '&source=' + str(
                settings.SMS_SOURCE) + '&message=' + sms_message

            # requests.get(sms_url)

            status = '10'
            # message = settings.SYSTEM_ERROR
            message = "Something went worng"

        return Response(
            {
                'status': status,
                'message': message
            }
        )


class PaytmPaymentMobileAPIView(APIView):
    """
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        status = None
        message = None

        try:

            # paytm response

            o_id = request.data.get('oid')
            loan_data = str(o_id).split("l")
            loan_application_id = ''
            if loan_data:
                loan_application_id = loan_data[0]

            paytm_status = ""
            merchant_key = settings.PROD_REP_MERCHANT_KEY
            m_id = settings.PROD_REP_MERCHANT_MID
            dict_param = {
                'MID': m_id,
                'ORDERID': o_id,

            }

            checksum = str(generate_checksum(dict_param, merchant_key))

            dict_param["CHECKSUMHASH"] = checksum

            url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
            r = requests.get(url=url)
            paytm_responce = r.json()

            if paytm_responce['STATUS'] == 'TXN_SUCCESS':
                paytm_status = 'success'
            elif paytm_responce['STATUS'] == 'TXN_FAILURE':
                paytm_status = 'failed'
            elif paytm_responce['STATUS'] == 'PENDING':
                paytm_status = 'pending'

            amount = paytm_responce['TXNAMOUNT']
            date_strip = paytm_responce['TXNDATE']
            mode = ''

            for name in paytm_responce.keys():
                if name == "PAYMENTMODE":
                    if paytm_responce['PAYMENTMODE']:
                        mode = paytm_responce['PAYMENTMODE']
            if amount is None or not amount:
                amount = 0.0

            if date_strip is None or not date_strip:
                date_strip = None

            Payment.objects.filter(order_id=o_id).update(status=paytm_status,
                                                         transaction_id=paytm_responce['TXNID'], amount=float(amount),
                                                         mode=mode,
                                                         type='App',
                                                         pay_source="Ptm",
                                                         return_status="ps2s",
                                                         response_code=paytm_responce['RESPCODE'],
                                                         description=paytm_responce['RESPMSG'],
                                                         response=paytm_responce,
                                                         date=date_strip
                                                         )
            loan_txn_instance = LoanApplication.objects.filter(loan_application_id=loan_application_id,
                                                               status="APPROVED")

            if loan_txn_instance:
                if paytm_status == 'success':
                    kwargs = {
                        'loan_application_id': loan_application_id,
                        'atom_amount': amount,
                    }

                    # notification
                    sm_kwargs = {
                        'user_id': loan_txn_instance[0].user_id,
                        'title': 'Transaction successful',
                        'body': 'Dear user, your transaction of ' + str(
                            amount) + ' has been processes successfully.',
                        'notification_type': 'paytm_success',
                        'to_store': True
                    }

                    # fcm_simple_notification(**sm_kwargs)

                    # sms code
                    sms_kwrgs = {
                        'sms_message': 'Dear user, your transaction of ' + str(
                            amount) + ' has been processes successfully.',
                        'number': loan_txn_instance[0].user.username
                    }

                    sms(**sms_kwrgs)
                    rep_amount = loan_txn_instance[0].repayment_amount.filter(
                        status='success').aggregate(Sum('amount'))

                    if rep_amount:
                        if float(rep_amount['amount__sum']) >= float(loan_txn_instance[0].approved_amount):
                            LoanApplication.objects.filter(
                                loan_application_id=loan_application_id
                            ).update(status='COMPLETED')

                    status = '1'
                    message = 'Transaction successful.'

                elif paytm_status == 'failed':

                    status = '2'
                    message = 'Transaction failed'

                elif paytm_status == 'pending':

                    status = '3'
                    message = 'Transaction pending'


                else:

                    status = '5'
                    message = 'Invalid loan application id'
            else:

                status = '6'
                message = 'Invalid loan application id'


        except Exception as e:

            import sys
            import os
            print('-----------in exception----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)

            sms_message = 'in paytm  mobile api producation ex' + str(e.args) + '\n' + str(exc_tb.tb_lineno)
            number = '6352820504'

            sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
                settings.SMS_USERNAME) + '&password=' + str(settings.SMS_PASSWORD) + \
                      '&type=0&dlr=1&destination=' + number + '&source=' + str(
                settings.SMS_SOURCE) + '&message=' + sms_message

            requests.get(sms_url)

            status = '10'
            message = settings.SYSTEM_ERROR

        return Response(
            {
                'status': status,
                'message': message
            }
        )


@csrf_exempt
def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')

    return HttpResponse(ip)


class BillFetchAPI_v2(APIView):
    """
    :parameter
       {
         "mobile_number":"9664953080"
       }

    After a successful request
    :response
            {
                "loan_application_id": "133009759",
                "loan_start_date": "2019-03-09T01:17:01.348913",
                "loan_amount": 1000,
                "paid_amount": 0,
                "balance": 1000,
                "status": "SUCCESS"
            }
    """

    def post(self, request, *args, **kwargs):
        '''
            Returns user loan details for repyment
        :param request:
            {
                "mobile_number": "9685201365",
                "auth_token":"Nzg3NTU4MzY3OXxNdWRyYWt3aWt8UGF5dG18TXVkcmFrMDEwMzEyNDMzMTI4MDA="
            }
        :param args:
        :param kwargs:
        :return:
            {
                "loan_application_id": "19971724412532",
                "user_name": "test",
                "loan_start_date": "2019-10-02T11:45:49",
                "loan_amount": "1000",
                "paid_amount": 1900,
                "balance": -900,
                "loan_status": "APPROVED",
                "status": "1"
            }
        '''
        status = None
        message = None
        loan_amount = 0
        loan_app_id = 0
        balance = 0

        mobile_number = request.data.get('mobile_number')

        try:
            auth = request.data.get('auth_token')
            if auth is not None:
                auth_resp = get_auth(auth)

                if auth_resp == "True":

                    if mobile_number:
                        try:
                            user_instance = User.objects.filter(username=mobile_number)

                            try:
                                user_loans = LoanApplication.objects.select_related('user',
                                                                                    'user_personal'). \
                                    filter(user_id=user_instance[0].id, status__in=['APPROVED', 'COMPLETED']).last()

                                if user_loans:

                                    if user_loans.status == "APPROVED":

                                        loan_amount = user_loans.loan_scheme_id
                                        loan_start_date = user_loans.loan_start_date
                                        paid_amounts = user_loans.repayment_amount.filter(
                                            status='success').aggregate(Sum('amount'))
                                        paid_amount = paid_amounts['amount__sum']
                                        if paid_amount == None:
                                            paid_amount = 0
                                        status = '1'
                                        return Response(
                                            {
                                                'loan_application_id': user_loans.loan_application_id,
                                                'user_name': user_loans.user_personal.name,
                                                'loan_start_date': loan_start_date,
                                                'loan_amount': loan_amount,
                                                'paid_amount': paid_amount,
                                                'balance': float(loan_amount) - float(paid_amount),
                                                'loan_status': user_loans.status,
                                                'status': status
                                            }
                                        )
                                    if user_loans.status == "COMPLETED":
                                        status = '1'
                                        return Response(
                                            {
                                                'loan_application_id': user_loans.loan_application_id,
                                                'balance': '0',
                                                'user_name': user_loans.user_personal.name,
                                                'loan_status': user_loans.status,
                                                'status': status
                                            }
                                        )
                                    else:
                                        status = '2'
                                        message = "Unable to find mobile and loan"
                                else:
                                    status = '2'
                                    message = "Unable to find mobile and loan"
                                return Response(
                                    {
                                        'status': status,
                                        'message': message
                                    }
                                )

                            except Exception as ex:
                                status = '4'
                                message = "Something Went wrong"

                                print("-------Bill fetch  API-------", ex.args)
                                exc_type, exc_obj, exc_tb = sys.exc_info()
                                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                                print(exc_type, fname, exc_tb.tb_lineno)

                                return Response(
                                    {
                                        'status': status,
                                        'message': message
                                    }
                                )
                        except Exception as e:
                            status = '5'
                            message = "You are not a user "

                            print("-------Bill fetcjh  API-------", e.args)
                            exc_type, exc_obj, exc_tb = sys.exc_info()
                            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                            print(exc_type, fname, exc_tb.tb_lineno)

                            return Response(
                                {
                                    'status': status,
                                    'message': message
                                }
                            )
                    else:
                        status = '6'
                        message = 'Please enter a valid Mobile Number'
                        return Response(
                            {
                                'status': status,
                                'message': message
                            }
                        )

                elif auth_resp == 'False':
                    status = '7'
                    message = 'Please enter a valid Mobile Number'


                else:
                    status = '8'
                    message = 'Invalid Authentication credentials'

                return Response(
                    {
                        'status': status,
                        'message': message
                    }
                )
            else:
                status = '9'
                message = 'Authentication credentials were not provided.'
                return Response(
                    {
                        'status': status,
                        'message': message
                    }
                )
        except Exception as x:
            status = '10'
            message = 'Something Went Wrong'

            print("-------Bill fetcjh  API-------", x.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)

            return Response(
                {
                    'status': status,
                    'message': message
                }
            )


class SubscriptionPaymentMobile(APIView):
    '''
    Paytm Subscription API
    '''
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        status = None
        message = None

        # try:
        o_id = request.data.get('oid')
        mobile_data = str(o_id).split("ms")
        mobile = ''
        if mobile_data:
            mobile = mobile_data[0]
        paytm_status = ""
        merchant_key = settings.PROD_SUB_MERCHANT_KEY
        m_id = settings.PROD_SUB_MERCHANT_MID
        dict_param = {
            'MID': m_id,
            'ORDERID': o_id,

        }

        checksum = str(generate_checksum(dict_param, merchant_key))

        dict_param["CHECKSUMHASH"] = checksum

        url = 'https://securegw.paytm.in/order/status?JsonData=' + json.dumps(dict_param)
        r = requests.get(url=url)
        paytm_responce = r.json()

        if paytm_responce['STATUS'] == 'TXN_SUCCESS':
            paytm_status = 'success'
        elif paytm_responce['STATUS'] == 'TXN_FAILURE':
            paytm_status = 'failed'
        elif paytm_responce['STATUS'] == 'PENDING':
            paytm_status = 'pending'

        amount = paytm_responce['TXNAMOUNT']
        date_strip = paytm_responce['TXNDATE']
        expiry_date = date_strip
        date_time_str = str(date_strip)
        date_time_obj = datetime.datetime.strptime(date_time_str[:19], '%Y-%m-%d %H:%M:%S')
        expiry_date = date_time_obj + timedelta(89)
        mode = ''
        for name in paytm_responce.keys():
            if name == "PAYMENTMODE":
                if paytm_responce['PAYMENTMODE']:
                    mode = paytm_responce['PAYMENTMODE']
        Payment.objects.filter(order_id=o_id).update(status=paytm_status,
                                                     transaction_id=paytm_responce['TXNID'],
                                                     amount=amount,
                                                     category='Subscription',
                                                     mode=mode,
                                                     type='App',
                                                     response=paytm_responce,
                                                     date=date_strip
                                                     )
        payment = Payment.objects.filter(order_id=o_id).last()
        Subscription.objects.filter(payment_id=payment.id).update(start_date=date_strip,
                                                                  expiry_date=expiry_date,
                                                                  payment_status=paytm_status)
        if paytm_status == 'success':
            share_subdata(mobile)


            # notification
            sm_kwargs = {
                'user_id': '',
                'title': 'Transaction successful',
                'body': 'Dear user, your transaction of ' + str(
                    amount) + ' has been processes successfully.',
                'notification_type': 'paytm_success',
                'to_store': True
            }

            # fcm_simple_notification(**sm_kwargs)

            # sms code
            sms_kwrgs = {
                'sms_message': 'Dear user, your transaction of ' + str(
                    amount) + ' has been processes successfully.',
                'number': ''
            }

            sms(**sms_kwrgs)

            status = '1'
            message = 'Transaction successful.'

        elif paytm_status == 'failed':

            status = '2'
            message = 'Transaction failed'

        elif paytm_status == 'pending':

            status = '3'
            message = 'Transaction pending'

        else:

            status = '4'
            message = 'Something went wrong'


        return Response(
            {
                'status': status,
                'message': message
            }
        )


class SubcriptionStatusCheck(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):

        status = 'fail'
        status_code = 0
        message = 'You are not a valid user'
        start_date = ''
        expiry_date = ''

        try:
            today_date = datetime.datetime.now()
            # get requested user
            user_instance = User.objects.filter(id=request.user.id)
            if user_instance:
                user_data = LoanApplication.objects.filter(user_id=request.user.id).filter(status='APPROVED')
                if user_data:
                    status_code = 1
                    message = 'You need to subcribe for income expense in future'
                    status = 'success'

                else:
                    subcription_data = Subscription.objects.filter(user_id=request.user.id,
                                                                   payment_status="success").order_by('-id').first()

                    if subcription_data:
                        # if today_date <= subcription_data.expiry_date and subcription_data.payment_id.amount>=50:
                        if today_date <= subcription_data.expiry_date:
                            status_code = 1
                            message = ' '
                            status = 'success'
                            start_date = subcription_data.start_date
                            expiry_date = subcription_data.expiry_date

                        else:
                            status_code = 3
                            message = 'You Subcription Expiry..........!'
                            status = 'fail'


                    else:

                        status_code = 2
                        message = 'You need to subcribe for a Income Expense tracker to get a loan'
                        status = 'fail'

            else:
                status_code = 0
                status = 'fail'
                message = 'You are not a valid user'

        except:
            pass
        return Response(
            {
                'status': status,
                'status_code': status_code,
                'message': message,
                'start_date': start_date,
                'expiry_date': expiry_date
            }
        )


class UserInfromations(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        try:
            pass

            bank_account_number = ''
            full_name = ''
            email_id = ''
            amount = 50
            address = ''
            status = ''


        except:
            return Response({'status': '03'})

        return Response(
            {

                'bank_account_number': bank_account_number,
                'full_name': full_name,
                'email_id': email_id,
                'address': address,
                'amount': amount,
                'status': status
            }
        )


import logging

logger = logging.getLogger(__name__)


class PaytmS2SRepayment(APIView):
    """
        PAYTM S2S
    -- Data will be posted as Form data (i.e application/x-www-form-urlencoded)
    -- URL needs to be configured at our end in order to send any S2S response
    -- Response can be posted whenever the transaction reached to the terminal stage (success/fail).
    -- Sample response:
        {CURRENCY=[INR], GATEWAYNAME=[xxxxxxxxxxx], RESPMSG=[Txn Success], BANKNAME=[xxxxxxxxxxxx], PAYMENTMODE=[xx],
        CUSTID=[xxxxx], MID=[xxxxxxxxxxxxxxxx], MERC_UNQ_REF=[], RESPCODE=[01], TXNID=[xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx],
        TXNAMOUNT=[xx.xx], ORDERID=[xxxxxxxxx], STATUS=[TXN_SUCCESS], BANKTXNID=[xxxxxxxxx], TXNDATETIME=[xxxx-xx-xx xx:xx:xx.x],
         TXNDATE=[xxxx-xx-xx]}
    Details regarding Order Expiry Time:
        PFB the Time slots available for the order expiry, which can be configured at our end after your confirmation on this. Currently, there is a default timeout is configured on your MID where you can wait until the 72 hours to get the response from the bank for updated status.
        15 Minutes,
        20 Minutes
        30 Minutes,
        1 Hour,
        1 Day,
        2 Days,
        3 Days.

        status 1 : loan payment added successfully
        status 2 : payment failed
        status 3 : payment cancelled
        status 4 : payment cancelled manually
        status 5 : invalid status response (other than pre-defined)
        status 6 : invalid loan application ID
        status 10 : Something went wrong (django error)
    """

    def post(self, request, *args, **kwargs):


        paytm_status = ""
        payment_dict = {}
        logger.info("##################################################################")
        logger.info("######################Paytm S2S Repayment Starts#######################")
        logger.info("##################################################################")
        logger.info("-------request.data", request.data)
        logger.info("", dict(request.data.lists()))

        # paytm response

        request_data = dict(request.data.lists())

        payment_dict['mode'] = request_data['PAYMENTMODE'][0]
        payment_dict['response_code'] = request_data['RESPCODE'][0]
        payment_dict['transaction_id'] = request_data['TXNID'][0]
        payment_dict['amount'] = request_data['TXNAMOUNT'][0]
        payment_dict['order_id'] = request_data['ORDERID'][0]
        payment_dict['status'] = request_data['STATUS'][0]
        payment_dict['date'] = request_data['TXNDATETIME'][0]
        payment_dict['type'] = 'App'
        payment_dict['pay_source'] = 'ptm'
        payment_dict['return_status'] = 'ps2s'
        payment_dict['response'] = request_data
        if "RESPMSG" in request_data:
            payment_dict['description'] = request_data['RESPMSG'][0]


        if payment_dict['status'] == 'TXN_SUCCESS':
            paytm_status = 'success'
        elif payment_dict['status'] == 'TXN_FAILURE':
            paytm_status = 'failed'
        elif payment_dict['status'] == 'PENDING':
            paytm_status = 'pending'

        order_id = payment_dict['order_id']
        pay_data_dict = {
            "order_id": order_id,
            "transaction_id": payment_dict['transaction_id'],
            "status": paytm_status,
            "amount": payment_dict['amount'],
            "type": 'App',
            "pay_source": "Ptm",
            "return_status": "ps2s",
            "category": 'Loan',
            "mode": payment_dict['mode'],
            "response": payment_dict['response'],
            "date": payment_dict['date'],
            "response_code": payment_dict['response_code'],
            "description": payment_dict['description'],
            "update_date": datetime.datetime.now()
        }

        if order_id:
            loan_data = str(order_id).split("m")
            loan_id = ''
            if loan_data:
                loan_id = loan_data[0]

        loan_txn_instance = LoanApplication.objects.filter(loan_application_id=loan_id,
                                                           status="APPROVED").order_by('-id').first()
        if not loan_txn_instance:
            loan_txn_instance = LoanApplication.objects.filter(loan_application_id=loan_id,
                                                               status="COMPLETED").order_by('-id').first()
        if loan_txn_instance:

            recovery_status = 'Yes'
            get_pay_obj = Payment.objects.filter(order_id=order_id, category='Loan')

            if get_pay_obj:
                payment_data = Payment.objects.filter(order_id=order_id).order_by('-id').first()

                update_pay_data = Payment.objects.filter(id=payment_data.id, category='Loan').update(**pay_data_dict)

            else:
                # As the payment must be made before changing the status
                # update_pay_data = Payment.objects.create(**pay_data_dict)
                # TODO Return error
                return Response(status=status.HTTP_400_BAD_REQUEST)

            # Adding Payment object to loan if not added
            # add_pay_obj = loan_txn_instance[0].repayment_amount.add(update_pay_data)

            rep_amount = loan_txn_instance.repayment_amount.filter(
                status='success').aggregate(Sum('amount'))['amount__sum']
            # Validating
            if rep_amount:
                if rep_amount is None:
                    rep_amount = 0
                if float(rep_amount) >= float(loan_txn_instance.loan_scheme_id):
                    full_payment = True
                    dpd = loan_application_dpd(loan_txn_instance.user_id)
                    complete_loan = LoanApplication.objects.filter(
                        loan_application_id=loan_id).update(status='COMPLETED', updated_date=datetime.datetime.now(),
                                                            dpd=dpd)
                    if complete_loan == 1:
                        loan_completed = True

                return Response(status=status.HTTP_200_OK)
        # paytm_status = payment_dict['status']

        logger.info("##################################################################")
        logger.info("######################Paytm S2S Repayment Ends#######################")
        logger.info("##################################################################")

        return Response(status=status.HTTP_400_BAD_REQUEST)


class PaytmS2SSub(APIView):
    logger.info("In Submit loan API------")

    """
        PAYTM S2S
    -- Data will be posted as Form data (i.e application/x-www-form-urlencoded)
    -- URL needs to be configured at our end in order to send any S2S response
    -- Response can be posted whenever the transaction reached to the terminal stage (success/fail).
    -- Sample response:
        {CURRENCY=[INR], GATEWAYNAME=[xxxxxxxxxxx], RESPMSG=[Txn Success], BANKNAME=[xxxxxxxxxxxx], PAYMENTMODE=[xx],
        CUSTID=[xxxxx], MID=[xxxxxxxxxxxxxxxx], MERC_UNQ_REF=[], RESPCODE=[01], TXNID=[xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx],
        TXNAMOUNT=[xx.xx], ORDERID=[xxxxxxxxx], STATUS=[TXN_SUCCESS], BANKTXNID=[xxxxxxxxx], TXNDATETIME=[xxxx-xx-xx xx:xx:xx.x],
         TXNDATE=[xxxx-xx-xx]}
    Details regarding Order Expiry Time:
        PFB the Time slots available for the order expiry, which can be configured at our end after your confirmation on this. Currently, there is a default timeout is configured on your MID where you can wait until the 72 hours to get the response from the bank for updated status.
        15 Minutes,
        20 Minutes
        30 Minutes,
        1 Hour,
        1 Day,
        2 Days,
        3 Days.

        status 1 : loan payment added successfully
        status 2 : payment failed
        status 3 : payment cancelled
        status 4 : payment cancelled manually
        status 5 : invalid status response (other than pre-defined)
        status 6 : invalid loan application ID
        status 10 : Something went wrong (django error)
    """

    def post(self, request, *args, **kwargs):
        paytm_status = ""
        payment_dict = {}
        logger.info("##################################################################")
        logger.info("######################Paytm S2S Subsc Starts#######################")
        logger.info("##################################################################")
        logger.info("-------request.data", request.data)
        logger.info("", dict(request.data.lists()))


        request_data = dict(request.data.lists())

        payment_dict['mode'] = request_data['PAYMENTMODE'][0]
        payment_dict['response_code'] = request_data['RESPCODE'][0]
        payment_dict['transaction_id'] = request_data['TXNID'][0]
        payment_dict['amount'] = request_data['TXNAMOUNT'][0]
        payment_dict['order_id'] = request_data['ORDERID'][0]
        payment_dict['status'] = request_data['STATUS'][0]
        payment_dict['date'] = request_data['TXNDATETIME'][0]
        payment_dict['type'] = 'App'
        payment_dict['pay_source'] = 'ptm'
        payment_dict['return_status'] = 'ps2s'
        payment_dict['response'] = request_data
        if "RESPMSG" in request_data:
            payment_dict['description'] = request_data['RESPMSG'][0]

        if payment_dict['status'] == 'TXN_SUCCESS':
            paytm_status = 'success'
        elif payment_dict['status'] == 'TXN_FAILURE':
            paytm_status = 'failed'
        elif payment_dict['status'] == 'PENDING':
            paytm_status = 'pending'

        payment_dict['mode'] = request_data['PAYMENTMODE'][0]
        payment_dict['response_code'] = request_data['RESPCODE'][0]
        payment_dict['transaction_id'] = request_data['TXNID'][0]
        payment_dict['amount'] = request_data['TXNAMOUNT'][0]
        payment_dict['order_id'] = request_data['ORDERID'][0]
        payment_dict['status'] = paytm_status
        payment_dict['date'] = request_data['TXNDATETIME'][0]
        payment_dict['category'] = 'Subscription'
        payment_dict['pay_source'] = 'ptm'
        payment_dict['return_status'] = 'ps2s'
        payment_dict['response'] = request_data

        # payment_dict['loan_id'] = "12345"
        order_id = payment_dict['order_id']
        pay_data_dict = {
            "order_id": order_id,
            "transaction_id": payment_dict['transaction_id'],
            "status": payment_dict['status'],
            "amount": payment_dict['amount'],
            "type": 'App',
            "pay_source": "Ptm",
            "return_status": "ps2s",
            "category": 'Subscription',
            "mode": payment_dict['mode'],
            "response": payment_dict['response'],
            "date": payment_dict['date'],
            "response_code": payment_dict['response_code'],
            "description": payment_dict['description'],
            "update_date": datetime.datetime.now()
        }
        paytm_status = payment_dict['status']

        date = payment_dict['date']
        if date is None or date == '':
            expiry_date = None
            date_time_obj = None
        else:
            date_time_obj = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S.%f')
            expiry_date = date_time_obj + timedelta(89)

        update_pay_data = Payment.objects.filter(order_id=order_id, category='Subscription').update(**pay_data_dict)
        get_pay_obj = Payment.objects.filter(order_id=order_id, category='Subscription').order_by('-id').first()
        if update_pay_data:
            update_sub = Subscription.objects.filter(payment_id=get_pay_obj.id).update(start_date=date_time_obj,
                                                                                       expiry_date=expiry_date,
                                                                                       payment_status=paytm_status)


            logger.info("##################################################################")
            logger.info("######################Paytm S2S Sub End success#######################")
            logger.info("##################################################################")
            return Response(status=status.HTTP_200_OK)
        logger.info("##################Paytm S2S Sub End Failed#########################")
        return Response(status=status.HTTP_400_BAD_REQUEST)
