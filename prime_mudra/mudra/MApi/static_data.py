from django.apps import apps
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics

MobileBlock = apps.get_model('StaticData', 'MobileBlock')


class CheckMobileBlock(generics.ListAPIView):
    """
    This Api is to check mobile number is in block list.

    Url: api/check_mobile_block/

    method = post
    parameter = {
                    "mobile_no": "7219004474",
                    "pan_no": "GNEPD2198M",
                    "type": "ck"
                }
    Response:
            {
              "status": "1",
              "msg": "Mobile no is not valid"
            }
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        msg = {}
        data = request.data
        if data and data is not None:
            is_block = False
            block_obj = None
            mobile_no = data.get('mobile_no')
            pan_no = data.get('pan_no')
            type = data.get('type')
            if type in ['mk', 'rk', 'ck']:
                mobile_block_obj = MobileBlock.objects.filter(mobile_no=mobile_no, pan_no=pan_no)
                if mobile_block_obj:
                    block_obj = mobile_block_obj
                else:
                    mobile_block_obj = MobileBlock.objects.filter(mobile_no=mobile_no)
                    if mobile_block_obj:
                        block_obj = mobile_block_obj
                    else:
                        if pan_no is not None:
                            block_obj = MobileBlock.objects.filter(pan_no=pan_no)
                if block_obj:
                    for mobile_block in block_obj:
                        if type == "mk":
                            if mobile_block.mk == True:
                                is_block = True
                        elif type == "rk":
                            if mobile_block.rk == True:
                                is_block = True
                        elif type == 'ck':
                            if mobile_block.ck == True:
                                is_block = True
                        else:
                            is_block = False
                if is_block == True:
                    return Response({
                        "status": "1",
                        "msg": "Data is not valid",
                    })
                else:
                    return Response({
                        "status": "2",
                        "msg": "Data is valid",
                    })
            else:
                return Response({
                    "status": "3",
                    "msg": "Type is not valid"
                })
        else:
            return Response({
                "status": "4",
                "msg": "Data not found"
            })
