import datetime
import re
from datetime import date, timedelta
from functools import partial
from User.models import *
from django.apps import apps
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
import requests
from .permissions import GuestYoloToken
import json

YeloLeads = apps.get_model('lead', 'YeloLeads')
Subscription = apps.get_model('User', 'Subscription')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
from apscheduler.schedulers.background import BackgroundScheduler


class GetYeloLeads(APIView):
    permission_classes = (partial(GuestYoloToken, ['GET', 'POST', 'HEAD']),)

    def post(self, request, *args, **kwargs):
        """

        :param request:
        {
            "mobile_number":"7875583671",
            "click_id":"",
            "sign":"",
            "timestamp":"1574663853"

        }
        :param args:
        :param kwargs:
        :return:

         status 1: User already exist
         status 2: User registered successfully
         status 3: Something went wrong
         status 4: Invalid mobile number
         status 5: Please provide mobile number
         status 6: Invalid Date format


        """

        mobile_number = request.data['mobile_number']
        click_id = request.data['click_id']

        timestamp = request.data['timestamp']
        try:
            timestamp = datetime.datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')


        except Exception as e:
            print(e)
            pass
        print(mobile_number)
        if mobile_number:
            validate_number = re.compile(r'^[6-9]\d{9}$')
            print(validate_number.search(mobile_number))
            validate = validate_number.search(mobile_number)
            print(validate)
            if validate:

                is_user_registered = YeloLeads.objects.filter(mobile=mobile_number)
                if is_user_registered.exists():
                    return Response(
                        {
                            'status_code': '1',
                            'message': 'User already registered',
                        },
                        status=status.HTTP_400_BAD_REQUEST
                    )
                try:
                    today = datetime.datetime.today()
                    today = today.strftime("%Y-%m-%d")
                    register_user = YeloLeads.objects.create(mobile=mobile_number, click_date=timestamp,
                                                             click_id=click_id, )
                except ValidationError as e:
                    print(e, type(e))
                    register_user = None
                    return Response(
                        {
                            'status_code': '6',
                            'message': 'Invalid Date format'
                        },
                        status=status.HTTP_400_BAD_REQUEST
                    )

                if register_user and register_user is not None:
                    return Response(
                        {
                            'status_code': '2',
                            'message': 'User registered successfully'
                        },
                        status=status.HTTP_201_CREATED
                    )
                else:
                    return Response(
                        {
                            'status_code': '3',
                            'message': 'Something went wrong'
                        },
                        status=status.HTTP_400_BAD_REQUEST
                    )
            else:
                return Response(
                    {
                        'status_code': '4',
                        'message': 'Invalid mobile number'
                    },
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                {
                    'status_code': '5',
                    'message': 'Please provide mobile number'
                },
                status=status.HTTP_400_BAD_REQUEST
            )


class YeloUserRegisterReport(APIView):
    permission_classes = (partial(GuestYoloToken, ['GET', 'POST', 'HEAD']),)

    def post(self, request):
        """

        :param request:
        :return:



        """
        yesterday = date.today() - timedelta(days=1)


        yelo_subscription = YeloLeads.objects.filter(is_subscribed=False, click_date__startswith=str(yesterday)[:10])

        today = datetime.datetime.now()
        register_user = []
        error_list = []
        for lending in yelo_subscription:

            user = User.objects.filter(username=lending.mobile)

            if user:
                subscription = Subscription.objects.filter(user_id=user[0].id, payment_status='success').order_by('-id')

                if subscription:
                    if lending.click_date:
                        if subscription[0].start_date is not None:
                            if lending.click_date <= subscription[0].start_date and lending.click_date <= user[
                                0].date_joined:


                                register_user.append(lending.mobile)
                                YeloLeads.objects.filter(mobile=lending.mobile).update(is_subscribed=True,
                                                                                       updated_date=today)
                        else:
                            error_list.append(lending.mobile)
        dict_param = {
            'status': 1,
            'data': register_user
        }
        json_body = json.dumps(dict_param)
        prod_url = "https://api.yelonow.com/v2/users/mudrakwik/report"

        headers = {
            'content-type': "application/json",
            'cache-control': "no-cache",
        }
        response = requests.post(prod_url, data=json_body, headers=headers, verify=False)

        paytm_responce = response.json()
        return Response(
            {
                'status': 1,
                'data': register_user,
                'error': error_list
            }
        )


def yelo_report():
    yesterday = date.today() - timedelta(days=1)


    yelo_subscription = YeloLeads.objects.filter(is_subscribed=False, click_date__startswith=str(yesterday)[:10])

    today = datetime.datetime.now()
    register_user = []
    error_list = []
    for lending in yelo_subscription:

        user = User.objects.filter(username=lending.mobile)

        if user:
            subscription = Subscription.objects.filter(user_id=user[0].id, payment_status='success').order_by('-id')

            if subscription:
                if lending.click_date:
                    if subscription[0].start_date is not None:
                        if lending.click_date <= subscription[0].start_date and lending.click_date <= user[
                            0].date_joined:


                            register_user.append(lending.mobile)
                            YeloLeads.objects.filter(mobile=lending.mobile).update(is_subscribed=True,
                                                                                   updated_date=today)
                    else:
                        error_list.append(lending.mobile)
    dict_param = {
        'status': 1,
        'data': register_user
    }
    json_body = json.dumps(dict_param)
    prod_url = "https://api.yelonow.com/v2/users/mudrakwik/report"

    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
    }
    response = requests.post(prod_url, data=json_body, headers=headers, verify=False)

    paytm_responce = response.json()
    return True
