from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, ListView
from django.contrib.auth.models import User
from django.apps.registry import apps
import datetime
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from datetime import timedelta
from django.db.models import Sum
from mudra.constants import scheme

LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
RBLTransaction = apps.get_model('Payments', 'RBLTransaction')
Payment = apps.get_model('Payments', 'Payment')


def get_over_due():
    count = 0
    amt = 0
    rep_amt = 0
    data = {}
    try:
        today = datetime.datetime.now()
        loan_users = LoanApplication.objects.filter(status='APPROVED')
        for user in loan_users:
            for i in scheme:
                if i == user.loan_scheme_id:
                    if user.loan_end_date:
                        return_date = user.loan_end_date + timedelta(int(90))
                        if (today > user.loan_end_date) & (today <= return_date):
                            rep_amount = user.repayment_amount.filter(
                                status='success', category='Loan').aggregate(Sum('amount'))['amount__sum']
                            if rep_amount:
                                rep_amt += rep_amount
                            amt += int(user.loan_scheme_id)
                            count += 1

        data['overdue_count'] = count
        data['overdue_amount'] = amt
        data['rep_overdue_amount'] = rep_amt
        return data


    except Exception as e:

        import sys

        import os

        print('-----------in exception----------')

        print(e.args)

        exc_type, exc_obj, exc_tb = sys.exc_info()

        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]

        print(exc_type, fname, exc_tb.tb_lineno)
        return data


def get_approved_repay_amt():
    try:

        amt = 0
        app_loan = LoanApplication.objects.filter(status='APPROVED')

        for i in app_loan:
            rep_amount = i.repayment_amount.filter(
                status='success', category='Loan').aggregate(Sum('amount'))['amount__sum']
            if rep_amount is not None:
                amt += rep_amount

        return amt


    except Exception as e:

        import sys

        import os

        print('-----------in exception----------')

        print(e.args)

        exc_type, exc_obj, exc_tb = sys.exc_info()

        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]

        print(exc_type, fname, exc_tb.tb_lineno)
        return 0


def get_defaulters():
    count = 0
    amt = 0
    rep_amt = 0
    data = {}
    try:
        today = datetime.datetime.now()

        loan_users = LoanApplication.objects.filter(status='APPROVED')
        for user in loan_users:
            for i in scheme:
                if i == user.loan_scheme_id:
                    if user.loan_end_date:
                        return_date = user.loan_end_date + timedelta(int(90))
                        if today > return_date:
                            rep_amount = user.repayment_amount.filter(
                                status='success', category='Loan').aggregate(Sum('amount'))['amount__sum']
                            if rep_amount is not None:
                                rep_amt += rep_amount
                            amt += int(user.loan_scheme_id)
                            count += 1

        data['defaulter_count'] = count
        data['defaulter_amount'] = amt
        data['rep_defaulter_amount'] = rep_amt

        return data
    except Exception as e:
        import sys
        import os
        print('-----------in exception----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)

        return data


# Create your views here.

@method_decorator([login_required(login_url="/login/")], name='dispatch')
class DashboardIndexView(TemplateView):
    template_name = 'admin/dashboard/index.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        try:

            pass


        except Exception as e:
            import sys
            import os
            print('-----------in exception----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            pass

        return self.render_to_response(context=ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class getDefaulterList(ListView):
    template_name = 'admin/dashboard/defaulter_list.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        users = []
        total_repayment = []
        re_amount = 0
        try:
            today = datetime.datetime.now()
            loan_users = LoanApplication.objects.filter(status='APPROVED')
            for user in loan_users:
                for i in scheme:
                    if i == user.loan_scheme_id:
                        if user.loan_end_date:
                            return_date = user.loan_end_date + timedelta(int(90))
                            if today > return_date:
                                rep_amount = user.repayment_amount.filter(
                                    status='success', category='Loan').aggregate(Sum('amount'))['amount__sum']
                                if rep_amount:
                                    re_amount = rep_amount

                                total_repayment.append(re_amount)
                                users.append(user)

            ctx['users'] = zip(users, total_repayment)
            return render(request, self.template_name, ctx)
        except Exception as e:
            import sys
            import os
            print('-----------in exception----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


def get_div_by_zero(x, y):
    try:
        return (x / y) * 100
    except:
        return 0.0


def get_pincodewise_analysis(request):
    template_name = "admin/dashboard/pin_code_overdue_list.html"
    title = 'Loan Application'
    ctx = {}

    try:

        unique_pincode_3_digit = []
        today = datetime.datetime.now()
        pincode_list = []
        overdue_counter_list = []
        user_counter_list = []
        unique_pincode_3_digit = get_all_pincode()
        for pin_code in unique_pincode_3_digit:
            user_counter = 0
            overdue_counter = 0
            default_counter = 0
            loan_users = LoanApplication.objects.filter(pin_code__startswith=pin_code).filter(
                status__in=['APPROVED', 'COMPLETED'])
            for user in loan_users:
                user_counter += 1
                if user.status != 'COMPLETED':
                    return_date = user.loan_application_start_date + timedelta(int(user.tenure))
                    over_due_limit_date = user.loan_application_start_date + timedelta(int(user.tenure) * 2)
                    if (today > return_date) & (today <= over_due_limit_date):
                        overdue_counter += 1
                    elif (today > over_due_limit_date):
                        default_counter += 1
            user_counter_list.append(user_counter)
            pincode_list.append(pin_code)
            overdue_counter_list.append(
                round((get_div_by_zero(overdue_counter, user_counter) + get_div_by_zero(default_counter, user_counter)),
                      2))
        ctx['users'] = zip(pincode_list, user_counter_list, overdue_counter_list)
        return render(request, template_name, ctx)
        pass
    except Exception as e:

        import sys
        import os
        print('-----------in exception----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)

        return render(request, template_name, ctx)


def get_all_pincode():
    unique_pincode_first_3_letter_list = []
    try:
        pin_code_set = set()
        pin_codes_qset = LoanApplication.objects.values_list('pin_code', flat=True).filter(
            status__in=['APPROVED', 'COMPLETED']).distinct()
        for pin in pin_codes_qset:
            first_three_digit = pin[0:3]
            pin_code_set.add(first_three_digit)
        unique_pincode_first_3_letter_list = list(pin_code_set)
        return unique_pincode_first_3_letter_list
    except Exception as e:

        import sys
        import os
        print('-----------in exception----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        return unique_pincode_first_3_letter_list


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class getOverDueList(ListView):
    template_name = 'admin/dashboard/overdue_list.html'

    def get(self, request, *args, **kwargs):

        ctx = {}
        users = []
        total_repayment = []
        re_amount = 0

        try:
            today = datetime.datetime.now()
            loan_users = LoanApplication.objects.filter(status='APPROVED')
            for user in loan_users:
                for i in scheme:
                    if i == user.loan_scheme_id:
                        if user.loan_end_date:
                            return_date = user.loan_end_date + timedelta(int(90))
                            if (today > user.loan_end_date) & (today <= return_date):
                                rep_amount = user.repayment_amount.filter(
                                    status='success').aggregate(Sum('amount'))['amount__sum']
                                if rep_amount:
                                    re_amount = rep_amount

                                total_repayment.append(re_amount)
                                users.append(user)

            ctx['users'] = zip(users, total_repayment)
            return render(request, self.template_name, ctx)
        except Exception as e:

            import sys
            import os
            print('-----------in exception----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return render(request, self.template_name, ctx)


@csrf_exempt
def get_dashboard_data_1(request):
    ctx = {}

    reject_count = LoanApplication.objects.values_list('loan_scheme_id', 'status', 'id').filter(
        status="REJECTED").count()
    approved_count = LoanApplication.objects.values_list('loan_scheme_id', 'status', 'id').filter(
        status__in=["COMPLETED", "APPROVED"]).count()

    total_count = LoanApplication.objects.values_list('loan_scheme_id', 'status', 'id').count()

    loan_pre_approved_count = LoanApplication.objects.values_list('loan_scheme_id', 'status', 'id').filter(
        status__in=["PRE_APPROVED"]).count()

    ctx['pre_approved_count'] = loan_pre_approved_count
    ctx['rejected_loan_count'] = reject_count
    ctx['applied_loan_count'] = total_count
    ctx['approved_loan_count'] = approved_count
    return JsonResponse(ctx, status=200)

@csrf_exempt
def get_dashboard_data_2(request):
    ctx = {}

    userloantransaction_approved = LoanApplication.objects.values_list('loan_scheme_id', 'status', 'id').filter(
        status="APPROVED").order_by('-id')
    loan_approved_amount_sum = 0
    for app in userloantransaction_approved:
        loan_approved_amount_sum += float(app[0])

    amount_dis = RBLTransaction.objects.filter(status__in=['Success', "success"]).aggregate(Sum('amount'))[
        'amount__sum']
    if amount_dis is not None:
        amount_disbursed = amount_dis
    else:
        amount_disbursed = 0

    userloantransaction = LoanApplication.objects.values_list('loan_scheme_id', 'status', 'id').filter(
        status="COMPLETED").order_by('-id')
    loan_complete_amount_sum = 0
    loan_completed_count = 0
    for user in userloantransaction:
        loan_complete_amount_sum += float(user[0])
        loan_completed_count += 1
    total_repay_amount = get_approved_repay_amt()
    user_total_count = User.objects.all().exclude(is_staff=True).count()

    ctx['due_amount'] = loan_approved_amount_sum
    ctx['user_count'] = user_total_count
    ctx['amount_disbursed'] = amount_disbursed
    ctx['loan_complete_amount'] = loan_complete_amount_sum
    ctx['approved_received'] = total_repay_amount
    return JsonResponse(ctx, status=200)

@csrf_exempt
def get_dashboard_data_3(request):
    ctx = {}
    userloantransaction = LoanApplication.objects.values_list('loan_scheme_id', 'status', 'id').filter(
        status="COMPLETED").order_by('-id')
    loan_complete_amount_sum = 0
    loan_completed_count = 0
    for user in userloantransaction:
        loan_complete_amount_sum += float(user[0])
        loan_completed_count += 1

    loan_complete_profit = (float(loan_complete_amount_sum) * 10) / 100

    loan_submitted_count = LoanApplication.objects.values_list('loan_scheme_id', 'status', 'id').filter(
        status__in=["SUBMITTED", "PROCESS"]).count()

    sub_amount = Payment.objects.filter(status='success', category='Subscription').aggregate(
        Sum('amount'))['amount__sum']

    defaulter = get_defaulters()
    if not defaulter['defaulter_amount']:
        defaulter['defaulter_amount']=0

    if not defaulter['rep_defaulter_amount']:
        defaulter['rep_defaulter_amount']=0

    if not defaulter['defaulter_amount']:
        defaulter['defaulter_amount']=0

    if not sub_amount:
        sub_amount=0

    net_profit = float(loan_complete_profit) - (
            float(defaulter['defaulter_amount']) - float(defaulter['rep_defaulter_amount'])) + float(sub_amount)
    amount_dis = RBLTransaction.objects.filter(status__in=['Success', "success"]).aggregate(Sum('amount'))[
        'amount__sum']
    if amount_dis is not None:
        amount_disbursed = amount_dis
        delinquency_percent = float(
            defaulter['defaulter_amount'] - defaulter['rep_defaulter_amount']) * 100 / float(amount_disbursed)
    else:
        amount_disbursed = 0
        delinquency_percent=0


    ctx['new_application_count'] = loan_submitted_count
    ctx['loan_complete_amount_profit'] = loan_complete_profit
    ctx['loan_complete_count'] = loan_completed_count
    ctx['net_profit'] = round(net_profit, 2)
    ctx['sub_amount'] = sub_amount
    ctx['delinquency_percent'] = delinquency_percent

    return JsonResponse(ctx, status=200)

@csrf_exempt
def get_dashboard_data_4(request):
    ctx = {}

    sub_count = Payment.objects.filter(status='success', category='Subscription').count()
    over_due = get_over_due()

    defaulter = get_defaulters()

    if not defaulter['rep_defaulter_amount']:
        defaulter['rep_defaulter_amount']=0

    if not defaulter['defaulter_amount']:
        defaulter['defaulter_amount']=0

    defaulter_balance_amount = defaulter['defaulter_amount'] - defaulter['rep_defaulter_amount']


    ctx['sub_count'] = sub_count
    ctx['defaulter_balance_amount'] = defaulter_balance_amount
    ctx['defaulter_count'] = defaulter['defaulter_count']
    ctx['over_due_amount'] = over_due['overdue_amount'] - over_due['rep_overdue_amount']
    ctx['Over_Due_Count'] = over_due['overdue_count']
    return JsonResponse(ctx, status=200)
