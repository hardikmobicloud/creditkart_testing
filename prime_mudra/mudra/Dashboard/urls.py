from django.conf.urls import url
from django.urls import path
from .views import *

urlpatterns = [

    path('', DashboardIndexView.as_view(), name='Dashboard'),
    path('over_due_list/', getOverDueList.as_view(), name='over_due_list'),
    path('defaulter-list/', getDefaulterList.as_view(), name='defaulter-list'),
    path('get_dashboard_data_1/', get_dashboard_data_1, name="get_dashboard_data_1"),
    path('get_dashboard_data_2/', get_dashboard_data_2, name="get_dashboard_data_2"),
    path('get_dashboard_data_3/', get_dashboard_data_3, name="get_dashboard_data_3"),
    path('get_dashboard_data_4/', get_dashboard_data_4, name="get_dashboard_data_4"),

]