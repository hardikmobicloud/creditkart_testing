from django.apps import AppConfig


class IcomeexpenseConfig(AppConfig):
    name = 'IcomeExpense'
