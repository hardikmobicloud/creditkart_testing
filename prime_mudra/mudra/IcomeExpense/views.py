from django.shortcuts import render
import math
from django.views import generic
from django.shortcuts import HttpResponse
from django.conf import settings
from django.apps.registry import apps
import json
import dateutil.parser as dparser
import re, datetime
from django.contrib.auth.models import User
from abc import ABC, abstractmethod
from django.contrib.auth.decorators import login_required

DebitedTowardsKeywordModel = apps.get_model('IcomeExpense', 'DebitedTowardsKeywordModel')
CreditedTowardsKeywordModel = apps.get_model('IcomeExpense', 'CreditedTowardsKeywordModel')
MerchantModel = apps.get_model('IcomeExpense', 'MerchantModel')
SmsStringMasterJsonData = apps.get_model('IcomeExpense', 'SmsStringMasterJsonData')
SmsStringMaster = apps.get_model('IcomeExpense', 'SmsStringMaster')


class BankView(ABC):

    def __init__(self):
        pass
    def getTransactionDate(self):
        # self.jObj = jObj
        dateFormat = '%Y-%m-%d %H:%M:%S'

        today = datetime.datetime.now()

        date_format_one = '([ -])(\d{4}|\d{2})(?:-|/| )(0?[1-9]|1[0-2])(?:-|/| )(0?[1-9]|1[0-9]|2[0-9]|30|31){1,2}([ |.|,|:])(00|[0-9]|1[0-9]|2[0-3]){2}(:([0-9]|[0-5][0-9]){2})*(:([0-9]|[0-5][0-9]){2})*'  # yyyymmdd Or yymmdd
        date_format_two = '([ -])(0?[1-9]|[12]\d|30|31)(?:-|/| )(0?[1-9]|1[0-2])(?:-|/| )(\d{4}|\d{2})([ |.|,|:])(00|[0-9]|1[0-9]|2[0-3]){2}(:([0-9]|[0-5][0-9]){2})*(:([0-9]|[0-5][0-9]){2})*'  # mmddyyyy Or mmddyy
        date_format_three = '([ -])(0?[1-9]|1[0-2])(?:-|/| )(0?[1-9]|[12]\d|30|31)(?:-|/| )(\d{4}|\d{2})([ |.|,|:])(00|[0-9]|1[0-9]|2[0-3]){2}(:([0-9]|[0-5][0-9]){2})*(:([0-9]|[0-5][0-9]){2})*'  # ddmmyyyy Or ddmmyy
        date_format_four = '([ -])(?:\d{1,2})?(?:-|\/| )?(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)[a-z]*(?:,)?(?:\d{1,2}(?:th|nd|st)?)?(?:,)?(?:-|\/|)?(?:\d{2,4})? (00|[0-9]|1[0-9]|2[0-3]){2}(:([0-9]|[0-5][0-9]){2})*(:([0-9]|[0-5][0-9]){2})*'

        if re.search(date_format_one, self.jObj['sms_string']):
            # print(jObj['sender_id'])
            dObj = re.search(date_format_one, self.jObj['sms_string']).group()[1:-1]
            try:
                dt = dparser.parse(dObj, dayfirst=True)

                if dt <= today:
                    dt = datetime.datetime.strftime(dt, dateFormat)
                    return str(dt)
                else:
                    dt = dparser.parse(self.jObj['date_time'])
                    dt = datetime.datetime.strftime(dt, dateFormat)
                    return str(dt)
            except:
                pass

        if re.search(date_format_two, self.jObj['sms_string']):
            # print(jObj['sender_id'])
            dObj = re.search(date_format_two, self.jObj['sms_string']).group()[1:-1]
            try:
                dt = dparser.parse(dObj, dayfirst=True)

                if dt <= today:
                    dt = datetime.datetime.strftime(dt, dateFormat)
                    return str(dt)
                else:
                    dt = dparser.parse(self.jObj['date_time'])
                    dt = datetime.datetime.strftime(dt, dateFormat)
                    return str(dt)
            except:
                pass

        if re.search(date_format_three, self.jObj['sms_string']):
            # print(jObj['sender_id'])
            dObj = re.search(date_format_three, self.jObj['sms_string']).group()[1:-1]
            try:
                dt = dparser.parse(dObj, dayfirst=True)

                if dt <= today:
                    dt = datetime.datetime.strftime(dt, dateFormat)
                    return str(dt)
                else:
                    dt = dparser.parse(self.jObj['date_time'])
                    dt = datetime.datetime.strftime(dt, dateFormat)
                    return str(dt)

            except:
                pass

        if re.search(date_format_four.lower(), str(self.jObj['sms_string']).lower()):
            # print(jObj['sender_id'])
            dObj = re.search(date_format_four.lower(), str(self.jObj['sms_string']).lower()).group()
            try:
                dt = dparser.parse(dObj, dayfirst=True)
                if dt <= today:
                    dt = datetime.datetime.strftime(dt, dateFormat)
                    return str(dt)
                else:
                    dt = dparser.parse(self.jObj['date_time'])
                    dt = datetime.datetime.strftime(dt, dateFormat)
                    return str(dt)
            except:
                pass

        dt = dparser.parse(self.jObj['date_time'])
        dt = datetime.datetime.strftime(dt, dateFormat)

        return str(dt)

    def getDebitedTowards(self):

        keywords = DebitedTowardsKeywordModel.objects.values_list('keyword', flat=True).all()
        keywords_list = list(keywords)
        sms_string = self.jObj['sms_string']

        for keyword in keywords_list:

            regex = '(?i)(?:' + keyword + ')(.*?)?[.]'
            match_data = re.search(regex, sms_string)

            if match_data:
                towards = match_data.group()
                if len(towards) > 30:
                    towards = towards[:25]
                return towards

        return None

    def getCreditedTowards(self):

        keywords = CreditedTowardsKeywordModel.objects.values_list('keyword', flat=True).all()
        keywords_list = list(keywords)
        sms_string = self.jObj['sms_string']

        for keyword in keywords_list:

            regex = '(?i)(?:' + keyword + ')(.*?)?[.]'
            match_data = re.search(regex, sms_string)
            if match_data:
                towards = match_data.group()
                if len(towards) > 30:
                    towards = towards[:25]
                return towards

        return None

    def getMerchant(self):

        sms_string = self.jObj['sms_string'].lower()
        merchant_list = MerchantModel.objects.values_list('name', flat=True).all()
        merchant_list = list(merchant_list)
        regular_exp_purpose = '(?i)(?: via| at)(.*?)?[.]'

        for merchant in merchant_list:
            if sms_string.__contains__(merchant):
                return merchant[:30]
        if re.findall(regular_exp_purpose, sms_string):

            merchant_string = re.findall(regular_exp_purpose, sms_string)
            merchant = merchant_string[0]
            merchant = ' '.join(merchant.split()[:2])
            if len(merchant) > 30:
                merchant = merchant[:25]
            return merchant

        return ""

    def getCategory(self):

        sms_string = self.jObj['sms_string'].lower()
        net_banking_keywords = ['neft', 'imps', 'rtgs', 'internet banking']
        atm_withdraw_keywords = ['w/d', 'withdrawn', 'by use of', 'atm']
        merchant_list = MerchantModel.objects.values_list('name', flat=True).all()
        merchant_list = list(merchant_list)
        health_keywords = ['health', 'medical', 'hospital', 'medicine']
        food_keywords = ['hotel', 'restaurant', 'drink', 'veg', 'vegetarian', 'beverage']
        shopping_keywords = ['collection', 'shop', 'purchase']

        category = 'Other'

        for word in net_banking_keywords:
            if sms_string.__contains__(word):
                category = 'Net Banking'

        for word in atm_withdraw_keywords:
            if sms_string.__contains__(word):
                category = 'ATM Withdrawal'

        for merchant in merchant_list:
            if sms_string.__contains__(merchant):
                category = 'Shopping'

        for word in shopping_keywords:
            if sms_string.__contains__(word):
                category = 'Shopping'

        for word in health_keywords:
            if sms_string.__contains__(word):
                category = 'Health'

        for word in food_keywords:
            if sms_string.__contains__(word):
                category = 'Hotel'

        if sms_string.__contains__('upi'):
            category = 'UPI'

        return category

    @abstractmethod
    def parseTransactionType(self):
        pass

    @abstractmethod
    def getAvailableBalance(self):
        pass

    @abstractmethod
    def getAccountNumber(self):
        pass

    @abstractmethod
    def getAmountCredited(self):
        pass

    @abstractmethod
    def getTransactionMethod(self):
        pass

    @abstractmethod
    def getCardType(self):
        pass

    @abstractmethod
    def getCreditCardNumber(self):
        pass

    @abstractmethod
    def getAmountDebited(self):
        pass

    @abstractmethod
    def getDebitCardNumber(self):
        pass


class CITIBankView(BankView):

    def __init__(self, jObj):
        self.jObj = jObj

    def parseTransactionType(self):

        OTP_regex = '((is|otp|password|key|code|CODE|KEY|OTP|PASSWORD).[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD)?)|(^[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD))'
        debit_keywords = ['debited', 'spent', 'received towards', 'purchase', 'txn', 'withdrawn', 'used']
        credit_keywords = ['credited', 'received from', 'deposit']

        if re.findall(OTP_regex, self.jObj['sms_string']):
            return "OTP"

        if any(word in self.jObj['sms_string'].lower() for word in debit_keywords):
            return 'Debit'
        elif any(word in self.jObj['sms_string'].lower() for word in credit_keywords):
            return 'Credit'
        else:
            return 'Reminder'

    def getAvailableBalance(self):

        exp = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        exp1 = '(?i)(avl\.?\s*| available)'
        amount_exp = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(exp, re.VERBOSE)

        # finds all currency amount in message
        if re.findall(exp, self.jObj['sms_string']):
            details = opt.findall(self.jObj['sms_string'])
            # print(details)

        # finds currency amount for available balance
        if re.search(exp1, self.jObj['sms_string'].lower()):

            match = re.search(exp1, self.jObj['sms_string'])
            str_obj = self.jObj['sms_string'].split(match.group())
            # print(match.group())
            if re.search(exp, str_obj[1]):
                amount = re.search(exp, str_obj[1]).group()
                amt = re.search(amount_exp, amount)
                if amt:
                    amt = amt.group()
                    if amt.__contains__(','):
                        amt = amt.replace(',', '')
                    return str(amt)
        return 0.0

    def getAccountNumber(self):
        regular_exp_account_number = '[0-9]*[Xx*\*]*[0-9]*[Xx\*]+[ ]*[0-9]{3,}'
        regular_exp_account = '(?i)(a/c\.?\s* | acc(ount)?)'
        regular_exp_card_number_2 = '[0-9]{4}'
        splitter_keyword = re.search(regular_exp_account, self.jObj['sms_string'])

        if splitter_keyword:
            splitted_message = self.jObj['sms_string'].split(splitter_keyword.group())
            splitted_message_part_two = splitted_message[1]
            account_number = re.search(regular_exp_account_number, splitted_message_part_two)

            if account_number:

                account_number = "XXXXXXX" + account_number.group()[-4:]
                return account_number

            elif self.jObj['sms_string'].lower().__contains__('ending'):

                new_message_string = self.jObj['sms_string'].lower().split('ending')
                opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
                account_number = opt.search(new_message_string[1])

                if account_number:
                    account_number = "XXXXXXX" + account_number.group()[-4:]
                    return account_number
        return ""

    def getAmountCredited(self):
        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getTransactionMethod(self):

        card_keywords = ['card', 'credit c', 'cc ', 'atm']
        regular_exp_account = '(?i)(a/c\.?\s*|acc(ount)?|ac|acct)'

        if re.search(regular_exp_account, self.jObj['sms_string']):
            return 'Net Banking'
        if any(word in self.jObj['sms_string'].lower() for word in card_keywords):
            return 'Card'

        else:
            return 'Net Banking'

    def getCardType(self):

        card_keywords = ['credit c', 'credit card', 'cc ']

        if any(word in self.jObj['sms_string'].lower() for word in card_keywords):
            # print(re.search(regular_exp_card_type, self.jObj['sms_string']))
            return "Credit"
        else:
            return 'Debit'

    def getCreditCardNumber(self):
        regular_exp_card_number = '([0-9]{4})?[Xx\*]+[0-9]{4}'
        regular_exp_card_number_2 = '[0-9]{4}'
        opt = re.compile(regular_exp_card_number, re.VERBOSE)
        card_number = opt.search(self.jObj['sms_string'])
        if card_number:
            card_number = "XXXXXXXXXX" + card_number.group()[-4:]
            return card_number
        elif self.jObj['sms_string'].lower().__contains__('ending'):
            new_message_string = self.jObj['sms_string'].lower().split('ending')
            opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
            card_number = opt.search(new_message_string[1])
            if card_number:
                card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                return card_number
        return ""

    def getAmountDebited(self):
        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2}) | -*(?:[,\d]+\.?\d{0,2}[ ](?i)USD)'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])

        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])

            if amount != None:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')

                if 'USD' in amountStringList[0]:
                    amount = round(float(amount) * 71.18, 2)

            return str(amount)
        return 0.0

    def getDebitCardNumber(self):

        regular_exp_debit_card = '[0-9]{0,3}[Xx\*]*[0-9]{4}'
        regular_exp_card_number_2 = '[0-9]{4}'
        opt = re.compile(regular_exp_debit_card, re.VERBOSE)
        card_number = opt.search(self.jObj['sms_string'])

        if card_number:

            card_number = "XXXXXXXXXX" + card_number.group()[-4:]
            return card_number

        elif self.jObj['sms_string'].lower().__contains__('ending'):

            new_message_string = self.jObj['sms_string'].lower().split('ending')
            opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
            card_number = opt.search(new_message_string[1])

            if card_number:
                card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                return card_number
        return ""

    def getPurpose(self):
        regular_exp_purpose = '(?i)(?:at|via|from|by|Info|IMPS)[: ](.*?)(?:is|for|Avbl|on|has|. )[: ]'

        if self.jObj['sms_string'].__contains__('Info'):
            index = self.jObj['sms_string'].index('Info')
            purpose = self.jObj['sms_string'][index:index + 30]

        elif self.jObj['sms_string'].__contains__('IMPS'):
            index = self.jObj['sms_string'].index('IMPS')
            purpose = self.jObj['sms_string'][index:index + 30]

        elif self.jObj['sms_string'].__contains__('UPI'):
            index = self.jObj['sms_string'].index('Ref')
            purpose = self.jObj['sms_string'][index:index + 20]

        elif re.findall(regular_exp_purpose, self.jObj['sms_string']):
            purpose_string = re.findall(regular_exp_purpose, self.jObj['sms_string'])
            purpose = purpose_string[0][:20]

        else:
            purpose = ""

        return purpose


class HDFCBankView(BankView):

    def __init__(self, jObj):
        self.jObj = jObj

    def parseTransactionType(self):

        OTP_regex = '((is|otp|password|key|code|CODE|KEY|OTP|PASSWORD).[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD)?)|(^[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD))'
        debit_keywords = ['debited', 'spent', 'received towards', 'purchase', 'txn', 'withdrawn', 'used']
        credit_keywords = ['credited', 'received from', 'deposit']

        if re.findall(OTP_regex, self.jObj['sms_string']):
            return "OTP"

        if any(word in self.jObj['sms_string'].lower() for word in debit_keywords):
            return 'Debit'
        elif any(word in self.jObj['sms_string'].lower() for word in credit_keywords):
            return 'Credit'

        else:
            return 'Reminder'

    def getAvailableBalance(self):

        exp = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        exp1 = '(?i)(avl\.?\s*| available)'
        amount_exp = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(exp, re.VERBOSE)

        # finds all currency amount in message
        if re.findall(exp, self.jObj['sms_string']):
            details = opt.findall(self.jObj['sms_string'])
            # print(details)

        # finds currency amount for available balance
        if re.search(exp1, self.jObj['sms_string'].lower()):

            match = re.search(exp1, self.jObj['sms_string'])
            str_obj = self.jObj['sms_string'].split(match.group())

            if re.search(exp, str_obj[1]):
                amount = re.search(exp, str_obj[1]).group()
                amt = re.search(amount_exp, amount)
                if amt:
                    amt = amt.group()
                    if amt.__contains__(','):
                        amt = amt.replace(',', '')
                    return str(amt)
        return 0.0

    def getAccountNumber(self):
        regular_exp_account_number = '[0-9]*[Xx*\*]*[0-9]*[Xx\*]+[ ]*[0-9]{3,}'
        regular_exp_account = '(?i)(a/c\.?\s* | acc(ount)?)'
        regular_exp_card_number_2 = '[0-9]{4}'
        splitter_keyword = re.search(regular_exp_account, self.jObj['sms_string'])

        if splitter_keyword:

            splitted_message = self.jObj['sms_string'].split(splitter_keyword.group())
            splitted_message_part_two = splitted_message[1]
            account_number = re.search(regular_exp_account_number, splitted_message_part_two)

            if account_number:

                account_number = "XXXXXXX" + account_number.group()[-4:]
                return account_number

            elif self.jObj['sms_string'].lower().__contains__('ending'):

                new_message_string = self.jObj['sms_string'].lower().split('ending')
                opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
                account_number = opt.search(new_message_string[1])

                if account_number:
                    account_number = "XXXXXXX" + account_number.group()[-4:]
                    return account_number
        return ""

    def getAmountCredited(self):
        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getTransactionMethod(self):

        card_keywords = ['card', 'credit c', 'cc ', 'atm']
        regular_exp_account = '(?i)(a/c\.?\s*|acc(ount)?|ac|acct)'

        if re.search(regular_exp_account, self.jObj['sms_string']):
            return 'Net Banking'
        if any(word in self.jObj['sms_string'].lower() for word in card_keywords):

            return 'Card'

        else:
            return 'Net Banking'

    def getCardType(self):

        card_keywords = ['credit c', 'credit card', 'cc ']

        if any(word in self.jObj['sms_string'].lower() for word in card_keywords):

            return "Credit"
        else:
            # print(self.jObj['sms_string'])
            return 'Debit'

    def getCreditCardNumber(self):
        regular_exp_card_number = '[Xx\*]+[0-9]{4}'
        regular_exp_card_number_2 = '[0-9]{4}'
        regular_exp_card = '(?i)(credit c|cc|card)'
        splitter_keyword = re.search(regular_exp_card, self.jObj['sms_string'])

        if splitter_keyword:

            splitted_message = self.jObj['sms_string'].split(splitter_keyword.group())
            splitted_message_part_two = splitted_message[1]
            card_number = re.search(regular_exp_card_number, splitted_message_part_two)

            if card_number:

                card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                return card_number

            elif self.jObj['sms_string'].lower().__contains__('ending'):

                new_message_string = self.jObj['sms_string'].lower().split('ending')
                opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
                card_number = opt.search(new_message_string[1])

                if card_number:
                    card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                    return card_number
        return ""

    def getAmountDebited(self):
        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
                    # print(amount)
            return str(amount)
        return 0.0

    def getDebitCardNumber(self):

        regular_exp_debit_card = '[0-9]{0,4}[Xx\*]*[0-9]{4}'
        regular_exp_card_number_2 = '[0-9]{4}'
        opt = re.compile(regular_exp_debit_card, re.VERBOSE)
        card_number = opt.search(self.jObj['sms_string'])
        if card_number:

            card_number = "XXXXXXXXXX" + card_number.group()[-4:]
            return card_number

        elif self.jObj['sms_string'].lower().__contains__('ending'):

            new_message_string = self.jObj['sms_string'].lower().split('ending')
            opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
            card_number = opt.search(new_message_string[1])

            if card_number:
                card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                return card_number
        return ""

    def getPurpose(self):

        regular_exp_purpose = '(?i)(?:at|via|by|towards)[: ](.*?)(?:is|for|Avbl|on|has|. )[: ]'

        # keyword = ['IMPS','Info']

        if self.jObj['sms_string'].__contains__('Info'):
            index = self.jObj['sms_string'].index('Info')
            purpose = self.jObj['sms_string'][index:index + 30]

        elif self.jObj['sms_string'].__contains__('IMPS'):
            index = self.jObj['sms_string'].index('IMPS')
            purpose = self.jObj['sms_string'][index:index + 30]

        elif self.jObj['sms_string'].__contains__('UPI'):
            index = self.jObj['sms_string'].index('Ref')
            purpose = self.jObj['sms_string'][index:index + 20]

        elif re.findall(regular_exp_purpose, self.jObj['sms_string']):
            purpose_string = re.findall(regular_exp_purpose, self.jObj['sms_string'])
            purpose = purpose_string[0][:25]

        else:
            purpose = ""
        return purpose


class ICICIBankView(BankView):

    def __init__(self, jObj):
        self.jObj = jObj

    def parseTransactionType(self):

        OTP_regex = '((is|otp|password|key|code|CODE|KEY|OTP|PASSWORD).[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD)?)|(^[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD))'
        debit_keywords = ['debited', 'spent', 'received towards', 'purchase', 'txn', 'withdrawn', 'used']
        credit_keywords = ['credited', 'received from', 'deposit']

        if re.findall(OTP_regex, self.jObj['sms_string']):
            return "OTP"

        if any(word in self.jObj['sms_string'].lower() for word in debit_keywords):
            return 'Debit'
        elif any(word in self.jObj['sms_string'].lower() for word in credit_keywords):
            return 'Credit'
        else:
            return 'Reminder'

    def getAvailableBalance(self):

        exp = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        exp1 = '(?i)(avl\.?\s*| available | avbl)'
        amount_exp = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(exp, re.VERBOSE)

        # finds all currency amount in message
        if re.findall(exp, self.jObj['sms_string']):
            details = opt.findall(self.jObj['sms_string'])

        # finds currency amount for available balance
        if re.search(exp1, self.jObj['sms_string'].lower()):

            match = re.search(exp1, self.jObj['sms_string'])
            str_obj = self.jObj['sms_string'].split(match.group())

            if re.search(exp, str_obj[1]):
                amount = re.search(exp, str_obj[1]).group()
                amt = re.search(amount_exp, amount)
                if amt:
                    amt = amt.group()
                    if amt.__contains__(','):
                        amt = amt.replace(',', '')
                    return str(amt)
        return 0.0

    def getAccountNumber(self):
        regular_exp_account_number = '[0-9]*[Xx*\*]*[0-9]*[Xx\*]+[ ]*[0-9]{3,}'
        regular_exp_account = '(?i)(a/c\.?\s* | acc(ount)?|acct|ac)'
        regular_exp_card_number_2 = '[0-9]{4}'
        splitter_keyword = re.search(regular_exp_account, self.jObj['sms_string'])

        if splitter_keyword:

            splitted_message = self.jObj['sms_string'].split(splitter_keyword.group())
            splitted_message_part_two = splitted_message[1]
            account_number = re.search(regular_exp_account_number, splitted_message_part_two)

            if account_number:

                account_number = "XXXXXXX" + account_number.group()[-4:]
                return account_number

            elif self.jObj['sms_string'].lower().__contains__('ending'):

                new_message_string = self.jObj['sms_string'].lower().split('ending')
                opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
                account_number = opt.search(new_message_string[1])

                if account_number:
                    account_number = "XXXXXXX" + account_number.group()[-4:]
                    return account_number

        return ""

    def getAmountCredited(self):
        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getTransactionMethod(self):

        card_keywords = ['card', 'credit c', 'cc ', 'atm']
        regular_exp_account = '(?i)(a/c\.?\s*|acc(ount)?|ac|acct)'

        if re.search(regular_exp_account, self.jObj['sms_string']):
            return 'Net Banking'
        if any(word in self.jObj['sms_string'].lower() for word in card_keywords):
            return 'Card'

        else:
            return 'Net Banking'

    def getCardType(self):

        card_keywords = ['credit c', 'credit card', 'cc ']

        if any(word in self.jObj['sms_string'].lower() for word in card_keywords):

            return "Credit"
        else:
            return 'Debit'

    def getCreditCardNumber(self):
        regular_exp_card_number = '[Xx]*[0-9]{4}'
        regular_exp_card_number_2 = '[0-9]{4}'
        opt = re.compile(regular_exp_card_number, re.VERBOSE)
        card_number = opt.search(self.jObj['sms_string'])

        if card_number:

            card_number = "XXXXXXXXXX" + card_number.group()[-4:]
            return card_number

        elif self.jObj['sms_string'].lower().__contains__('ending'):

            new_message_string = self.jObj['sms_string'].lower().split('ending')
            opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
            card_number = opt.search(new_message_string[1])

            if card_number:
                card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                return card_number

        return ""

    def getAmountDebited(self):
        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getDebitCardNumber(self):

        regular_exp_debit_card = '[0-9]{0,3}[Xx\*]*[0-9]{4}'
        regular_exp_card_number_2 = '[0-9]{4}'
        opt = re.compile(regular_exp_debit_card, re.VERBOSE)
        card_number = opt.search(self.jObj['sms_string'])

        if card_number:

            card_number = "XXXXXXXXXX" + card_number.group()[-4:]
            return card_number

        elif self.jObj['sms_string'].lower().__contains__('ending'):

            new_message_string = self.jObj['sms_string'].lower().split('ending')
            opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
            card_number = opt.search(new_message_string[1])

            if card_number:
                card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                return card_number
        return ""

    def getPurpose(self):
        regular_exp_purpose = '(?i)(?:at|via|from|by|Info|IMPS)[: ](.*?)(?:is|for|Avbl|on|has|. )[: ]'
        # keyword = ['IMPS', 'Info']

        if self.jObj['sms_string'].__contains__('Info'):
            index = self.jObj['sms_string'].index('Info')
            purpose = self.jObj['sms_string'][index:index + 30]

        elif self.jObj['sms_string'].__contains__('IMPS'):
            index = self.jObj['sms_string'].index('IMPS')
            purpose = self.jObj['sms_string'][index:index + 30]

        elif re.findall(regular_exp_purpose, self.jObj['sms_string']):
            purpose_string = re.findall(regular_exp_purpose, self.jObj['sms_string'])
            purpose = purpose_string[0][:20]

        elif self.jObj['sms_string'].__contains__('UPI'):
            index = self.jObj['sms_string'].index('Ref')
            purpose = self.jObj['sms_string'][index:index + 20]

        else:
            purpose = ""

        return purpose


class SBIBankView(BankView):

    def __init__(self, jObj):
        self.jObj = jObj

    def parseTransactionType(self):

        OTP_regex = '((is|otp|password|key|code|CODE|KEY|OTP|PASSWORD).[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD)?)|(^[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD))'
        debit_keywords = ['debited', 'spent', 'received towards', 'purchase', 'w/d', 'withdrawn', 'transaction', 'used']
        credit_keywords = ['credited', 'received from', 'deposit']
        beneficiary_keywords = ['credited to beneficiary']

        if re.findall(OTP_regex, self.jObj['sms_string']):
            return "OTP"
        elif self.jObj['sms_string'].lower().__contains__('credited to beneficiary'):
            return 'NA'
        if any(word in self.jObj['sms_string'].lower() for word in debit_keywords):
            return 'Debit'
        elif any(word in self.jObj['sms_string'].lower() for word in credit_keywords):
            return 'Credit'
        else:
            return 'Reminder'

    def getAvailableBalance(self):

        exp = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*:*(?:[,\d]+\.?\d{0,2})'
        exp1 = '(?i)(avl\.?\s*| available | avbl)'
        amount_exp = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(exp, re.VERBOSE)

        # finds all currency amount in message
        if re.findall(exp, self.jObj['sms_string']):
            details = opt.findall(self.jObj['sms_string'])

        # finds currency amount for available balance
        if re.search(exp1, self.jObj['sms_string'].lower()):

            match = re.search(exp1, self.jObj['sms_string'])
            str_obj = self.jObj['sms_string'].split(match.group())
            # print(match.group())
            if re.search(exp, str_obj[1]):
                amount = re.search(exp, str_obj[1]).group()
                amt = re.search(amount_exp, amount)
                if amt:
                    amt = amt.group()
                    if amt.__contains__(','):
                        amt = amt.replace(',', '')
                    return str(amt)
        return 0.0

    def getAccountNumber(self):

        regular_exp_account_number = '[0-9]*[Xx\*]+[ ]*[0-9]{4}'
        regular_exp_account = '(?i)(a/c\.?\s*|acc(ount)?|ac|acct)'
        regular_exp_card_number_2 = '[0-9]{4}'
        splitter_keyword = re.search(regular_exp_account, self.jObj['sms_string'])
        if splitter_keyword:

            splitted_message = self.jObj['sms_string'].split(splitter_keyword.group())
            splitted_message_part_two = splitted_message[1]
            account_number = re.search(regular_exp_account_number, splitted_message_part_two)

            if account_number:

                account_number = "XXXXXXX" + account_number.group()[-4:]
                return account_number

            elif self.jObj['sms_string'].lower().__contains__('ending'):

                new_message_string = self.jObj['sms_string'].lower().split('ending')
                opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
                account_number = opt.search(new_message_string[1])

                if account_number:
                    account_number = "XXXXXXX" + account_number.group()[-4:]
                    return account_number
        return ""

    def getAmountCredited(self):

        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getTransactionMethod(self):

        card_keywords = ['card', 'credit c', 'cc ', 'atm']
        regular_exp_account = '(?i)(a/c\.?\s*|acc(ount)?|ac|acct)'

        if re.search(regular_exp_account, self.jObj['sms_string']):
            return 'Net Banking'

        if any(word in self.jObj['sms_string'].lower() for word in card_keywords):
            return 'Card'

        else:
            return 'Net Banking'

    def getCardType(self):

        card_keywords = ['credit c', 'credit card', 'cc ']

        if any(word in self.jObj['sms_string'].lower() for word in card_keywords):
            # print(re.findall(regular_exp_card_type, self.jObj['sms_string'].lower()))
            return "Credit"
        else:
            return 'Debit'

    def getCreditCardNumber(self):

        regular_exp_card_number = '[Xx\*][0-9]{4}'
        regular_exp_card_number_2 = '[0-9]{4}'
        opt = re.compile(regular_exp_card_number, re.VERBOSE)
        card_number = opt.search(self.jObj['sms_string'])

        if card_number:

            card_number = "XXXXXXXXXX" + card_number.group()[-4:]
            return card_number

        elif self.jObj['sms_string'].lower().__contains__('ending'):

            new_message_string = self.jObj['sms_string'].lower().split('ending')
            opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
            card_number = opt.search(new_message_string[1])

            if card_number:
                card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                return card_number
        return ""

    def getAmountDebited(self):

        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getDebitCardNumber(self):

        regular_exp_debit_card = '[0-9]{0,3}[Xx\*]*[0-9]{4}'
        regular_exp_card_number_2 = '[0-9]{4}'
        opt = re.compile(regular_exp_debit_card, re.VERBOSE)
        card_number = opt.search(self.jObj['sms_string'])

        if card_number:

            card_number = "XXXXXXXXXX" + card_number.group()[-4:]
            return card_number

        elif self.jObj['sms_string'].lower().__contains__('ending'):

            new_message_string = self.jObj['sms_string'].lower().split('ending')
            opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
            card_number = opt.search(new_message_string[1])

            if card_number:
                card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                return card_number
        return ""

    def getPurpose(self):

        regular_exp_purpose = '(?i)(?:at|via|from|by|Info|IMPS)[:\s]*((.*?)\w+){1,3}[. ]'

        if self.jObj['sms_string'].__contains__('IMPS'):
            index = self.jObj['sms_string'].index('IMPS')
            purpose = self.jObj['sms_string'][index:index + 30]

        elif self.jObj['sms_string'].__contains__('UPI'):
            index = self.jObj['sms_string'].index('Ref')
            purpose = self.jObj['sms_string'][index:index + 20]

        elif re.findall(regular_exp_purpose, self.jObj['sms_string']):
            purpose_string = re.search(regular_exp_purpose, self.jObj['sms_string'])
            purpose = purpose_string

        else:
            purpose = ""

        return purpose


class RBLBankView(BankView):
    def __init__(self, jObj):
        self.jObj = jObj

    def parseTransactionType(self):

        OTP_regex = '((is|otp|password|key|code|CODE|KEY|OTP|PASSWORD).[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD)?)|(^[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD))'
        debit_keywords = ['debited', 'spent', 'received towards', 'purchase', 'w/d', 'withdrawn', 'used']
        credit_keywords = ['credited', 'received from', 'deposit']

        if re.findall(OTP_regex, self.jObj['sms_string']):
            return "OTP"
        if any(word in self.jObj['sms_string'].lower() for word in debit_keywords):
            return 'Debit'
        elif any(word in self.jObj['sms_string'].lower() for word in credit_keywords):
            return 'Credit'
        else:
            return 'Reminder'

    def getAvailableBalance(self):

        exp = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*:*(?:[,\d]+\.?\d{0,2})'
        exp1 = '(?i)(avl\.?\s*| available | avbl)'
        amount_exp = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(exp, re.VERBOSE)

        # finds all currency amount in message
        if re.findall(exp, self.jObj['sms_string']):
            details = opt.findall(self.jObj['sms_string'])
            # print(details)

        # finds currency amount for available balance
        if re.search(exp1, self.jObj['sms_string'].lower()):

            match = re.search(exp1, self.jObj['sms_string'])
            str_obj = self.jObj['sms_string'].split(match.group())
            # print(match.group())
            if re.search(exp, str_obj[1]):
                amount = re.search(exp, str_obj[1]).group()
                amt = re.search(amount_exp, amount)
                if amt:
                    amt = amt.group()
                    if amt.__contains__(','):
                        amt = amt.replace(',', '')
                    return str(amt)
        return 0.0

    def getAccountNumber(self):

        regular_exp_account_number = '[0-9]{0,2}[Xx\*]*[0-9]{3,}'
        regular_exp_account = '(?i)(a/c\.?\s*|acc(ount)?)'
        regular_exp_card_number_2 = '[0-9]{4}'
        splitter_keyword = re.search(regular_exp_account, self.jObj['sms_string'])
        if splitter_keyword:

            splitted_message = self.jObj['sms_string'].split(splitter_keyword.group())
            splitted_message_part_two = splitted_message[1]
            account_number = re.search(regular_exp_account_number, splitted_message_part_two)

            if account_number:

                account_number = "XXXXXXX" + account_number.group()[-4:]
                return account_number

            elif self.jObj['sms_string'].lower().__contains__('ending'):

                new_message_string = self.jObj['sms_string'].lower().split('ending')
                opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
                account_number = opt.search(new_message_string[1])

                if account_number:
                    account_number = "XXXXXXX" + account_number.group()[-4:]
                    return account_number
        return ""

    def getAmountCredited(self):

        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getTransactionMethod(self):

        card_keywords = ['card', 'credit c', 'cc ']
        regular_exp_account = '(?i)(a/c\.?\s*|acc(ount)?|ac|acct)'
        if re.search(regular_exp_account, self.jObj['sms_string']):
            return 'Net Banking'
        if any(word in self.jObj['sms_string'].lower() for word in card_keywords):
            return 'Card'

        else:
            return 'Net Banking'

    def getCardType(self):

        card_keywords = ['credit c', 'credit card', 'cc ']

        if any(word in self.jObj['sms_string'].lower() for word in card_keywords):
            # print(re.findall(regular_exp_card_type, self.jObj['sms_string'].lower()))
            return "Credit"
        else:
            return 'Debit'

    def getCreditCardNumber(self):

        regular_exp_card_number = '[Xx\*][0-9]{4}'
        regular_exp_card_number_2 = '[0-9]{4}'
        opt = re.compile(regular_exp_card_number, re.VERBOSE)
        card_number = opt.search(self.jObj['sms_string'])

        if card_number:

            card_number = "XXXXXXXXXX" + card_number.group()[-4:]
            return card_number

        elif self.jObj['sms_string'].lower().__contains__('ending'):

            new_message_string = self.jObj['sms_string'].lower().split('ending')
            opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
            card_number = opt.search(new_message_string[1])

            if card_number:
                card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                return card_number
        return ""

    def getAmountDebited(self):

        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getDebitCardNumber(self):

        regular_exp_debit_card = '[0-9]{0,3}[Xx\*]{2}[0-9]{4}'
        regular_exp_card_number_2 = '[0-9]{4}'
        opt = re.compile(regular_exp_debit_card, re.VERBOSE)
        card_number = opt.search(self.jObj['sms_string'])

        if card_number:

            card_number = "XXXXXXXXXX" + card_number.group()[-4:]
            return card_number

        elif self.jObj['sms_string'].lower().__contains__('ending'):

            new_message_string = self.jObj['sms_string'].lower().split('ending')
            opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
            card_number = opt.search(new_message_string[1])

            if card_number:
                card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                return card_number
        return ""

    def getPurpose(self):
        regular_exp_purpose = '(?i)(?:at|via|from|by|Info|IMPS)[: ](.*?)(?:is|for|Avbl|on|has|. )[: ]'

        if self.jObj['sms_string'].__contains__('Info'):
            index = self.jObj['sms_string'].index('Info')
            purpose = self.jObj['sms_string'][index:index + 30]

        elif self.jObj['sms_string'].__contains__('IMPS'):
            index = self.jObj['sms_string'].index('IMPS')
            purpose = self.jObj['sms_string'][index:index + 30]

        elif self.jObj['sms_string'].__contains__('UPI'):
            index = self.jObj['sms_string'].index('Ref')
            purpose = self.jObj['sms_string'][index:index + 20]

        elif re.findall(regular_exp_purpose, self.jObj['sms_string']):
            purpose_string = re.findall(regular_exp_purpose, self.jObj['sms_string'])
            purpose = purpose_string[0][:20]

        else:
            purpose = ""

        return purpose


class FederalBankView(BankView):
    def __init__(self, jObj):
        self.jObj = jObj

    def parseTransactionType(self):

        OTP_regex = '((is|otp|password|key|code|CODE|KEY|OTP|PASSWORD).[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD)?)|(^[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD))'
        debit_keywords = ['debited', 'spent', 'received towards', 'purchase', 'w/d', 'withdrawn', 'used']
        credit_keywords = ['credited', 'received from', 'deposit']

        if re.findall(OTP_regex, self.jObj['sms_string']):
            return "OTP"
        if any(word in self.jObj['sms_string'].lower() for word in debit_keywords):
            return 'Debit'
        elif any(word in self.jObj['sms_string'].lower() for word in credit_keywords):
            return 'Credit'
        else:
            return 'Reminder'

    def getAvailableBalance(self):

        exp = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*:*(?:[,\d]+\.?\d{0,2})'
        exp1 = '(?i)(avl\.?\s*| available | avbl)'
        amount_exp = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(exp, re.VERBOSE)

        # finds all currency amount in message
        if re.findall(exp, self.jObj['sms_string']):
            details = opt.findall(self.jObj['sms_string'])
            # print(details)

        # finds currency amount for available balance
        if re.search(exp1, self.jObj['sms_string'].lower()):

            match = re.search(exp1, self.jObj['sms_string'])
            str_obj = self.jObj['sms_string'].split(match.group())
            # print(match.group())
            if re.search(exp, str_obj[1]):
                amount = re.search(exp, str_obj[1]).group()
                amt = re.search(amount_exp, amount)
                if amt:
                    amt = amt.group()
                    if amt.__contains__(','):
                        amt = amt.replace(',', '')
                    return str(amt)
        return 0.0

    def getAccountNumber(self):

        regular_exp_account_number = '[0-9]{0,2}[Xx\*]*[0-9]{3,}'
        regular_exp_account = '(?i)(a/c\.?\s*|acc(ount)?)'
        regular_exp_card_number_2 = '[0-9]{4}'
        splitter_keyword = re.search(regular_exp_account, self.jObj['sms_string'])
        if splitter_keyword:

            splitted_message = self.jObj['sms_string'].split(splitter_keyword.group())
            splitted_message_part_two = splitted_message[1]
            account_number = re.search(regular_exp_account_number, splitted_message_part_two)

            if account_number:

                account_number = "XXXXXXX" + account_number.group()[-4:]
                return account_number

            elif self.jObj['sms_string'].lower().__contains__('ending'):

                new_message_string = self.jObj['sms_string'].lower().split('ending')
                opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
                account_number = opt.search(new_message_string[1])

                if account_number:
                    account_number = "XXXXXXX" + account_number.group()[-4:]
                    return account_number
        return ""

    def getAmountCredited(self):

        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getTransactionMethod(self):

        card_keywords = ['card', 'credit c', 'cc ']
        regular_exp_account = '(?i)(a/c\.?\s*|acc(ount)?|ac|acct)'
        if re.search(regular_exp_account, self.jObj['sms_string']):
            return 'Net Banking'
        if any(word in self.jObj['sms_string'].lower() for word in card_keywords):
            return 'Card'

        else:
            return 'Net Banking'

    def getCardType(self):

        card_keywords = ['credit c', 'credit card', 'cc ']

        if any(word in self.jObj['sms_string'].lower() for word in card_keywords):
            # print(re.findall(regular_exp_card_type, self.jObj['sms_string'].lower()))
            return "Credit"
        else:
            return 'Debit'

    def getCreditCardNumber(self):

        regular_exp_card_number = '[Xx\*][0-9]{4}'
        regular_exp_card_number_2 = '[0-9]{4}'
        opt = re.compile(regular_exp_card_number, re.VERBOSE)
        card_number = opt.search(self.jObj['sms_string'])

        if card_number:

            card_number = "XXXXXXXXXX" + card_number.group()[-4:]
            return card_number

        elif self.jObj['sms_string'].lower().__contains__('ending'):

            new_message_string = self.jObj['sms_string'].lower().split('ending')
            opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
            card_number = opt.search(new_message_string[1])

            if card_number:
                card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                return card_number
        return ""

    def getAmountDebited(self):

        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getDebitCardNumber(self):

        regular_exp_debit_card = '[0-9]{0,3}[Xx\*]{2}[0-9]{4}'
        regular_exp_card_number_2 = '[0-9]{4}'
        opt = re.compile(regular_exp_debit_card, re.VERBOSE)
        card_number = opt.search(self.jObj['sms_string'])

        if card_number:

            card_number = "XXXXXXXXXX" + card_number.group()[-4:]
            return card_number

        elif self.jObj['sms_string'].lower().__contains__('ending'):

            new_message_string = self.jObj['sms_string'].lower().split('ending')
            opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
            card_number = opt.search(new_message_string[1])

            if card_number:
                card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                return card_number
        return ""

    def getPurpose(self):
        regular_exp_purpose = '(?i)(?:at|via|from|by|Info|IMPS)[: ](.*?)(?:is|for|Avbl|on|has|. )[: ]'

        if self.jObj['sms_string'].__contains__('Info'):
            index = self.jObj['sms_string'].index('Info')
            purpose = self.jObj['sms_string'][index:index + 30]

        elif self.jObj['sms_string'].__contains__('IMPS'):
            index = self.jObj['sms_string'].index('IMPS')
            purpose = self.jObj['sms_string'][index:index + 30]

        elif self.jObj['sms_string'].__contains__('UPI'):
            index = self.jObj['sms_string'].index('Ref')
            purpose = self.jObj['sms_string'][index:index + 20]

        elif re.findall(regular_exp_purpose, self.jObj['sms_string']):
            purpose_string = re.findall(regular_exp_purpose, self.jObj['sms_string'])
            purpose = purpose_string[0][:20]

        else:
            purpose = ""

        return purpose


class CentralBankView(BankView):
    def __init__(self, jObj):
        self.jObj = jObj

    def parseTransactionType(self):

        OTP_regex = '((is|otp|password|key|code|CODE|KEY|OTP|PASSWORD).[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD)?)|(^[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD))'
        debit_keywords = ['debited', 'spent', 'received towards', 'purchase', 'w/d', 'withdrawn', 'used']
        credit_keywords = ['credited', 'received from', 'deposit']

        if re.findall(OTP_regex, self.jObj['sms_string']):
            return "OTP"
        if any(word in self.jObj['sms_string'].lower() for word in debit_keywords):
            return 'Debit'
        elif any(word in self.jObj['sms_string'].lower() for word in credit_keywords):
            return 'Credit'
        else:
            return 'Reminder'

    def getAvailableBalance(self):

        exp = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*:*(?:[,\d]+\.?\d{0,2})'
        exp1 = '(?i)(avl\.?\s*| available | avbl)'
        amount_exp = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(exp, re.VERBOSE)

        # finds all currency amount in message
        if re.findall(exp, self.jObj['sms_string']):
            details = opt.findall(self.jObj['sms_string'])
            # print(details)

        # finds currency amount for available balance
        if re.search(exp1, self.jObj['sms_string'].lower()):

            match = re.search(exp1, self.jObj['sms_string'])
            str_obj = self.jObj['sms_string'].split(match.group())
            # print(match.group())
            if re.search(exp, str_obj[1]):
                amount = re.search(exp, str_obj[1]).group()
                amt = re.search(amount_exp, amount)
                if amt:
                    amt = amt.group()
                    if amt.__contains__(','):
                        amt = amt.replace(',', '')
                    return str(amt)
        return 0.0

    def getAccountNumber(self):

        regular_exp_account_number = '[0-9]{0,2}[Xx\*]*[0-9]{3,}'
        regular_exp_account = '(?i)(a/c\.?\s*|acc(ount)?)'
        regular_exp_card_number_2 = '[0-9]{4}'
        splitter_keyword = re.search(regular_exp_account, self.jObj['sms_string'])
        if splitter_keyword:

            splitted_message = self.jObj['sms_string'].split(splitter_keyword.group())
            splitted_message_part_two = splitted_message[1]
            account_number = re.search(regular_exp_account_number, splitted_message_part_two)

            if account_number:

                account_number = "XXXXXXX" + account_number.group()[-4:]
                return account_number

            elif self.jObj['sms_string'].lower().__contains__('ending'):

                new_message_string = self.jObj['sms_string'].lower().split('ending')
                opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
                account_number = opt.search(new_message_string[1])

                if account_number:
                    account_number = "XXXXXXX" + account_number.group()[-4:]
                    return account_number
        return ""

    def getAmountCredited(self):

        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getTransactionMethod(self):

        card_keywords = ['card', 'credit c', 'cc ']
        regular_exp_account = '(?i)(a/c\.?\s*|acc(ount)?|ac|acct)'
        if re.search(regular_exp_account, self.jObj['sms_string']):
            return 'Net Banking'
        if any(word in self.jObj['sms_string'].lower() for word in card_keywords):
            return 'Card'

        else:
            return 'Net Banking'

    def getCardType(self):

        card_keywords = ['credit c', 'credit card', 'cc ']

        if any(word in self.jObj['sms_string'].lower() for word in card_keywords):
            # print(re.findall(regular_exp_card_type, self.jObj['sms_string'].lower()))
            return "Credit"
        else:
            return 'Debit'

    def getCreditCardNumber(self):

        regular_exp_card_number = '[Xx\*][0-9]{4}'
        regular_exp_card_number_2 = '[0-9]{4}'
        opt = re.compile(regular_exp_card_number, re.VERBOSE)
        card_number = opt.search(self.jObj['sms_string'])

        if card_number:

            card_number = "XXXXXXXXXX" + card_number.group()[-4:]
            return card_number

        elif self.jObj['sms_string'].lower().__contains__('ending'):

            new_message_string = self.jObj['sms_string'].lower().split('ending')
            opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
            card_number = opt.search(new_message_string[1])

            if card_number:
                card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                return card_number
        return ""

    def getAmountDebited(self):

        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getDebitCardNumber(self):

        regular_exp_debit_card = '[0-9]{0,3}[Xx\*]*[0-9]{4}'
        regular_exp_card_number_2 = '[0-9]{4}'
        opt = re.compile(regular_exp_debit_card, re.VERBOSE)
        card_number = opt.search(self.jObj['sms_string'])

        if card_number:

            # card_number = card_number.group()
            card_number = "XXXXXXXXXX" + card_number.group()[-4:]
            return card_number

        elif self.jObj['sms_string'].lower().__contains__('ending'):

            new_message_string = self.jObj['sms_string'].lower().split('ending')
            opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
            card_number = opt.search(new_message_string[1])

            if card_number:
                card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                return card_number
        return ""

    def getPurpose(self):
        regular_exp_purpose = '(?i)(?:at|via|from|by|Info|IMPS)[: ](.*?)(?:is|for|Avbl|on|has|. )[: ]'

        if self.jObj['sms_string'].__contains__('Info'):
            index = self.jObj['sms_string'].index('Info')
            purpose = self.jObj['sms_string'][index:index + 30]

        elif self.jObj['sms_string'].__contains__('IMPS'):
            index = self.jObj['sms_string'].index('IMPS')
            purpose = self.jObj['sms_string'][index:index + 30]

        elif self.jObj['sms_string'].__contains__('UPI'):
            index = self.jObj['sms_string'].index('Ref')
            purpose = self.jObj['sms_string'][index:index + 20]

        elif re.findall(regular_exp_purpose, self.jObj['sms_string']):
            purpose_string = re.findall(regular_exp_purpose, self.jObj['sms_string'])
            purpose = purpose_string[0][:20]

        else:
            purpose = ""

        return purpose


class UnionBankView(BankView):

    def __init__(self, jObj):
        self.jObj = jObj

    def parseTransactionType(self):

        OTP_regex = '((is|otp|password|key|code|CODE|KEY|OTP|PASSWORD).[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD)?)|(^[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD))'
        debit_keywords = ['debited', 'spent', 'received towards', 'purchase', 'w/d', 'withdrawn', 'transaction', 'used']
        credit_keywords = ['credited', 'received from', 'deposit']

        if re.findall(OTP_regex, self.jObj['sms_string']):
            return "OTP"
        if any(word in self.jObj['sms_string'].lower() for word in debit_keywords):
            return 'Debit'
        elif any(word in self.jObj['sms_string'].lower() for word in credit_keywords):
            return 'Credit'
        else:
            return 'Reminder'

    def getAvailableBalance(self):

        exp = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*:*(?:[,\d]+\.?\d{0,2})'
        exp1 = '(?i)(avl\.?\s*| available | avbl)'
        amount_exp = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(exp, re.VERBOSE)

        # finds all currency amount in message
        if re.findall(exp, self.jObj['sms_string']):
            details = opt.findall(self.jObj['sms_string'])

        # finds currency amount for available balance
        if re.search(exp1, self.jObj['sms_string'].lower()):

            match = re.search(exp1, self.jObj['sms_string'])
            str_obj = self.jObj['sms_string'].split(match.group())
            # print(match.group())
            if re.search(exp, str_obj[1]):
                amount = re.search(exp, str_obj[1]).group()
                amt = re.search(amount_exp, amount)
                if amt:
                    amt = amt.group()
                    if amt.__contains__(','):
                        amt = amt.replace(',', '')
                    return str(amt)
        return 0.0

    def getAccountNumber(self):

        regular_exp_account_number = '[0-9]*[Xx\*]+[ ]*[0-9]{4,5}'
        regular_exp_account = '(?i)(a/c\.?\s*|acc(ount)?)'
        regular_exp_card_number_2 = '[0-9]{4}'
        splitter_keyword = re.search(regular_exp_account, self.jObj['sms_string'])
        if splitter_keyword:

            splitted_message = self.jObj['sms_string'].split(splitter_keyword.group())
            splitted_message_part_two = splitted_message[1]
            account_number = re.search(regular_exp_account_number, splitted_message_part_two)

            if account_number:

                account_number = "XXXXXXX" + account_number.group()[-4:]
                return account_number

            elif self.jObj['sms_string'].lower().__contains__('ending'):

                new_message_string = self.jObj['sms_string'].lower().split('ending')
                opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
                account_number = opt.search(new_message_string[1])

                if account_number:
                    account_number = "XXXXXXX" + account_number.group()[-4:]
                    return account_number
        return ""

    def getAmountCredited(self):

        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getTransactionMethod(self):

        card_keywords = ['card', 'credit c', 'cc ', 'atm']
        regular_exp_account = '(?i)(a/c\.?\s*|acc(ount)?|ac|acct)'

        if re.search(regular_exp_account, self.jObj['sms_string']):
            return 'Net Banking'

        if any(word in self.jObj['sms_string'].lower() for word in card_keywords):
            return 'Card'

        else:
            return 'Net Banking'

    def getCardType(self):

        card_keywords = ['credit c', 'credit card', 'cc ']

        if any(word in self.jObj['sms_string'].lower() for word in card_keywords):
            # print(re.findall(regular_exp_card_type, self.jObj['sms_string'].lower()))
            return "Credit"
        else:
            return 'Debit'

    def getCreditCardNumber(self):

        regular_exp_card_number = '[Xx\*][0-9]{4}'
        regular_exp_card_number_2 = '[0-9]{4}'
        opt = re.compile(regular_exp_card_number, re.VERBOSE)
        card_number = opt.search(self.jObj['sms_string'])

        if card_number:

            card_number = "XXXXXXXXXX" + card_number.group()[-4:]
            return card_number

        elif self.jObj['sms_string'].lower().__contains__('ending'):

            new_message_string = self.jObj['sms_string'].lower().split('ending')
            opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
            card_number = opt.search(new_message_string[1])

            if card_number:
                card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                return card_number
        return ""

    def getAmountDebited(self):

        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getDebitCardNumber(self):

        regular_exp_debit_card = '[0-9]{3}[Xx\*]{2}[0-9]{4}'
        regular_exp_card_number_2 = '[0-9]{4}'
        opt = re.compile(regular_exp_debit_card, re.VERBOSE)
        card_number = opt.search(self.jObj['sms_string'])

        if card_number:

            card_number = "XXXXXXXXXX" + card_number.group()[-4:]
            return card_number

        elif self.jObj['sms_string'].lower().__contains__('ending'):

            new_message_string = self.jObj['sms_string'].lower().split('ending')
            opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
            card_number = opt.search(new_message_string[1])

            if card_number:
                card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                return card_number
        return ""

    def getPurpose(self):

        regular_exp_purpose = '(?i)(?:at|via|from|by|Info|IMPS)[:\s]*((.*?)\w+){1,3}[. ]'

        if self.jObj['sms_string'].__contains__('IMPS'):
            index = self.jObj['sms_string'].index('IMPS')
            purpose = self.jObj['sms_string'][index:index + 30]

        elif self.jObj['sms_string'].__contains__('UPI'):
            index = self.jObj['sms_string'].index('Ref')
            purpose = self.jObj['sms_string'][index:index + 20]

        elif re.findall(regular_exp_purpose, self.jObj['sms_string']):
            purpose_string = re.search(regular_exp_purpose, self.jObj['sms_string'])

            purpose = purpose_string

        else:
            purpose = ""

        return purpose


class IndusIndBankView(BankView):

    def __init__(self, jObj):
        self.jObj = jObj

    def parseTransactionType(self):

        OTP_regex = '((is|otp|password|key|code|CODE|KEY|OTP|PASSWORD).[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD)?)|(^[0-9]{4,8}.(is|otp|password|key|code|CODE|KEY|OTP|PASSWORD))'
        debit_keywords = ['debited', 'spent', 'received towards', 'purchase', 'w/d', 'withdrawn', 'transaction', 'used']
        credit_keywords = ['credited', 'received from', 'deposit']

        if re.findall(OTP_regex, self.jObj['sms_string']):
            return "OTP"
        if any(word in self.jObj['sms_string'].lower() for word in debit_keywords):
            return 'Debit'
        elif any(word in self.jObj['sms_string'].lower() for word in credit_keywords):
            return 'Credit'
        else:
            return 'Reminder'

    def getAvailableBalance(self):

        exp = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*:*(?:[,\d]+\.?\d{0,2})'
        exp1 = '(?i)(avl\.?\s*| available | avbl)'
        amount_exp = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(exp, re.VERBOSE)

        # finds all currency amount in message
        if re.findall(exp, self.jObj['sms_string']):
            details = opt.findall(self.jObj['sms_string'])

        # finds currency amount for available balance
        if re.search(exp1, self.jObj['sms_string'].lower()):

            match = re.search(exp1, self.jObj['sms_string'])
            str_obj = self.jObj['sms_string'].split(match.group())
            # print(match.group())
            if re.search(exp, str_obj[1]):
                amount = re.search(exp, str_obj[1]).group()
                amt = re.search(amount_exp, amount)
                if amt:
                    amt = amt.group()
                    if amt.__contains__(','):
                        amt = amt.replace(',', '')
                    return str(amt)
        return 0.0

    def getAccountNumber(self):

        regular_exp_account_number = '[0-9]*[Xx\*]+[ ]*[0-9]{4,6}'
        regular_exp_account = '(?i)(a/c\.?\s*|acc(ount)?|acct)'
        regular_exp_card_number_2 = '[0-9]{4}'
        splitter_keyword = re.search(regular_exp_account, self.jObj['sms_string'])

        if splitter_keyword:

            splitted_message = self.jObj['sms_string'].split(splitter_keyword.group())

            splitted_message_part_two = splitted_message[1]

            account_number = re.search(regular_exp_account_number, splitted_message_part_two)

            if account_number:

                account_number = "XXXXXXX" + account_number.group()[-4:]

                return account_number

            elif self.jObj['sms_string'].lower().__contains__('ending'):

                new_message_string = self.jObj['sms_string'].lower().split('ending')
                opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
                account_number = opt.search(new_message_string[1])

                if account_number:
                    account_number = "XXXXXXX" + account_number.group()[-4:]
                    return account_number
        return ""

    def getAmountCredited(self):

        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getTransactionMethod(self):

        card_keywords = ['card', 'credit c', 'cc ', 'atm']
        regular_exp_account = '(?i)(a/c\.?\s*|acc(ount)?|ac|acct)'

        if re.search(regular_exp_account, self.jObj['sms_string']):
            return 'Net Banking'

        if any(word in self.jObj['sms_string'].lower() for word in card_keywords):
            return 'Card'

        else:
            return 'Net Banking'

    def getCardType(self):

        card_keywords = ['credit c', 'credit card', 'cc ']

        if any(word in self.jObj['sms_string'].lower() for word in card_keywords):
            # print(re.findall(regular_exp_card_type, self.jObj['sms_string'].lower()))
            return "Credit"
        else:
            return 'Debit'

    def getCreditCardNumber(self):

        regular_exp_card_number = '[Xx\*][0-9]{4}'
        regular_exp_card_number_2 = '[0-9]{4}'
        opt = re.compile(regular_exp_card_number, re.VERBOSE)
        card_number = opt.search(self.jObj['sms_string'])

        if card_number:

            card_number = "XXXXXXXXXX" + card_number.group()[-4:]
            return card_number

        elif self.jObj['sms_string'].lower().__contains__('ending'):

            new_message_string = self.jObj['sms_string'].lower().split('ending')
            opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
            card_number = opt.search(new_message_string[1])

            if card_number:
                card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                return card_number
        return ""

    def getAmountDebited(self):

        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getDebitCardNumber(self):

        regular_exp_debit_card = '[0-9]{3}[Xx\*]{2}[0-9]{4}'
        regular_exp_card_number_2 = '[0-9]{4}'
        opt = re.compile(regular_exp_debit_card, re.VERBOSE)
        card_number = opt.search(self.jObj['sms_string'])

        if card_number:

            card_number = "XXXXXXXXXX" + card_number.group()[-4:]
            return card_number

        elif self.jObj['sms_string'].lower().__contains__('ending'):

            new_message_string = self.jObj['sms_string'].lower().split('ending')
            opt = re.compile(regular_exp_card_number_2, re.VERBOSE)
            card_number = opt.search(new_message_string[1])

            if card_number:
                card_number = "XXXXXXXXXX" + card_number.group()[-4:]
                return card_number
        return ""

    def getPurpose(self):

        regular_exp_purpose = '(?i)(?:at|via|from|by|Info|IMPS)[:\s]*((.*?)\w+){1,3}[. ]'

        if self.jObj['sms_string'].__contains__('IMPS'):
            index = self.jObj['sms_string'].index('IMPS')
            purpose = self.jObj['sms_string'][index:index + 30]

        elif self.jObj['sms_string'].__contains__('UPI'):
            index = self.jObj['sms_string'].index('Ref')
            purpose = self.jObj['sms_string'][index:index + 20]

        elif re.findall(regular_exp_purpose, self.jObj['sms_string']):
            purpose_string = re.search(regular_exp_purpose, self.jObj['sms_string'])

            purpose = purpose_string


        else:
            purpose = ""

        return purpose


class PAYTMMerchant():

    def __init__(self, jObj):
        self.jObj = jObj

    def getTransactionAmount(self):

        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getTransactionType(self):

        credit_keywords = ['added', 'received']
        debit_keywords = ['paid', 'sent']
        sms_string = self.jObj['sms_string'].lower().split(" ")

        if any(word in sms_string for word in credit_keywords):
            return 'Credit'

        if any(word in sms_string for word in debit_keywords):
            return 'Debit'

        else:
            return ''

    def getTransactionDate(self):

        dateFormat = '%Y-%m-%d %H:%M:%S'
        dt = dparser.parse(self.jObj['date_time'])
        dt = datetime.datetime.strftime(dt, dateFormat)

        return str(dt)

    def getCategory(self):

        # category_regex = '(?i)(?:to|for)\s*([A-Z]\w+){1}'
        category = "other"

        sms_string = self.jObj['sms_string'].lower()

        bill_keywords = ['mseb', 'msedcl', 'mahavitaran']
        recharge_keywords = ['mobile', 'recharge']
        hotel_keywords = ['hotel', 'restaurant', 'veg', 'vegeterian']
        shopping_keywords = ['purchase', 'order']

        if any(word in sms_string for word in bill_keywords):
            category = 'bill'

        elif any(word in sms_string for word in recharge_keywords):
            category = 'recharge'

        elif any(word in sms_string for word in hotel_keywords):
            category = 'hotel'

        elif any(word in sms_string for word in shopping_keywords):
            category = 'shopping'

        else:
            category = 'other'

        return category


class AmazonMerchant():

    def __init__(self, jObj):
        self.jObj = jObj

    def getTransactionAmount(self):

        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getTransactionType(self):

        # credit_keywords = ['added','received']
        debit_keywords = ['paid', 'sent', 'order', 'placed', 'received']
        sms_string = self.jObj['sms_string'].lower().split(" ")

        # if any(word in sms_string for word in credit_keywords):
        #     return 'Credit'

        if any(word in sms_string for word in debit_keywords):
            return 'Debit'

        else:
            return ''

    def getTransactionDate(self):

        dateFormat = '%Y-%m-%d %H:%M:%S'
        dt = dparser.parse(self.jObj['date_time'])
        dt = datetime.datetime.strftime(dt, dateFormat)

        return str(dt)

    def getCategory(self):

        # category_regex = '(?i)(?:to|for)\s*([A-Z]\w+){1}'
        category = "other"

        sms_string = self.jObj['sms_string'].lower()

        bill_keywords = ['mseb', 'msedcl', 'mahavitaran']
        recharge_keywords = ['mobile', 'recharge']
        hotel_keywords = ['hotel', 'restaurant']

        if any(word in sms_string for word in bill_keywords):
            category = 'bill'

        elif any(word in sms_string for word in recharge_keywords):
            category = 'recharge'

        elif any(word in sms_string for word in hotel_keywords):
            category = 'hotel'

        else:
            category = 'other'

        return category


class FlipkartMerchant():

    def __init__(self, jObj):
        self.jObj = jObj

    def getTransactionAmount(self):

        regular_exp_amount = '(?:[[I][N][R]\.?]?|[[rR][sS]\.?]?)\s?-*(?:[,\d]+\.?\d{0,2})'
        regular_exp_decimal_digit = '-*[,\d]+\.?\d{0,2}'
        opt = re.compile(regular_exp_amount, re.VERBOSE)
        amountStringList = opt.findall(self.jObj['sms_string'])
        if amountStringList:
            amount = re.search(regular_exp_decimal_digit, amountStringList[0])
            if amount:
                amount = amount.group()
                if amount.__contains__(','):
                    amount = amount.replace(',', '')
            return str(amount)
        return 0.0

    def getTransactionType(self):

        # credit_keywords = ['added']
        debit_keywords = ['paid', 'sent', 'placed', 'order', 'received']
        sms_string = self.jObj['sms_string'].lower().split(" ")
        # if any(word in sms_string for word in credit_keywords):
        #     return 'Credit'

        if any(word in sms_string for word in debit_keywords):

            return 'Debit'

        else:
            return ''

    def getTransactionDate(self):

        dateFormat = '%Y-%m-%d %H:%M:%S'
        dt = dparser.parse(self.jObj['date_time'])
        dt = datetime.datetime.strftime(dt, dateFormat)

        return str(dt)

    def getCategory(self):

        # category_regex = '(?i)(?:to|for)\s*([A-Z]\w+){1}'
        category = "other"

        sms_string = self.jObj['sms_string'].lower()

        bill_keywords = ['mseb', 'msedcl', 'mahavitaran']
        recharge_keywords = ['mobile', 'recharge']
        hotel_keywords = ['hotel', 'restaurant']

        if any(word in sms_string for word in bill_keywords):
            category = 'bill'

        elif any(word in sms_string for word in recharge_keywords):
            category = 'recharge'

        elif any(word in sms_string for word in hotel_keywords):
            category = 'hotel'

        else:
            category = 'other'

        return category


def parseSms(json_file):
    json_obj = json.loads(json_file)

    json_arr = []

    for jObj in json_obj:
        json_dict = {}
        try:
            if 'PAYTM' in jObj['sender_id'].upper():
                paytm_object = PAYTMMerchant(jObj)

                transactionType = paytm_object.getTransactionType()

                if transactionType == 'Credit':
                    json_dict['category'] = paytm_object.getCategory()

                elif transactionType == 'Debit':
                    json_dict['category'] = paytm_object.getCategory()

                else:
                    continue

                json_dict['transaction_amount'] = paytm_object.getTransactionAmount()
                json_dict['transaction_date'] = paytm_object.getTransactionDate()
                json_dict['transaction_type'] = transactionType
                json_dict['merchant_id'] = 'PAYTM'

                json_arr.append(json_dict)

            if 'AMAZON' in jObj['sender_id'].upper():
                amazon_object = AmazonMerchant(jObj)

                transactionType = amazon_object.getTransactionType()

                # if transactionType == 'Credit':
                #     json_dict['category'] = ''

                if transactionType == 'Debit':
                    json_dict['category'] = 'shopping'

                else:
                    continue
                transactionAmount = amazon_object.getTransactionAmount()
                if transactionAmount == 0.0:
                    continue
                else:

                    json_dict['transaction_amount'] = transactionAmount
                    json_dict['transaction_date'] = amazon_object.getTransactionDate()
                    json_dict['transaction_type'] = transactionType
                    json_dict['merchant_id'] = 'AMAZON'

                    json_arr.append(json_dict)

            if 'FLP' in jObj['sender_id'].upper():
                flipkart_object = FlipkartMerchant(jObj)

                transactionType = flipkart_object.getTransactionType()

                # if transactionType == 'Credit':
                #     json_dict['category'] = ''

                if transactionType == 'Debit':
                    json_dict['category'] = 'shopping'

                else:
                    continue

                transactionAmount = flipkart_object.getTransactionAmount()
                if transactionAmount == 0.0:
                    continue
                else:
                    json_dict['transaction_amount'] = transactionAmount
                    json_dict['transaction_date'] = flipkart_object.getTransactionDate()
                    json_dict['transaction_type'] = transactionType
                    json_dict['merchant_id'] = 'FLIPKART'

                    json_arr.append(json_dict)

            else:

                if 'ICICI' in jObj['sender_id'].upper():
                    bank_Obj = ICICIBankView(jObj)
                    merchant_bank = 'ICICI Bank'

                elif 'HDFC' in jObj['sender_id'].upper():
                    bank_Obj = HDFCBankView(jObj)
                    merchant_bank = 'HDFC Bank'

                elif 'CITI' in jObj['sender_id'].upper():
                    bank_Obj = CITIBankView(jObj)
                    merchant_bank = 'CITI Bank'

                elif 'SBI' in jObj['sender_id'].upper():
                    bank_Obj = SBIBankView(jObj)
                    merchant_bank = 'State Bank of India'

                elif 'RBL' in jObj['sender_id'].upper():
                    bank_Obj = RBLBankView(jObj)
                    merchant_bank = 'Ratnakar Bank'

                elif 'FED' in jObj['sender_id'].upper():
                    bank_Obj = FederalBankView(jObj)
                    merchant_bank = 'Fedaral Bank'

                elif 'CENT' in jObj['sender_id'].upper():
                    bank_Obj = CentralBankView(jObj)
                    merchant_bank = 'Central Bank of India'

                elif 'UNION' in jObj['sender_id'].upper():
                    bank_Obj = UnionBankView(jObj)
                    merchant_bank = 'Union Bank of India'

                elif 'INDUS' in jObj['sender_id'].upper():
                    bank_Obj = IndusIndBankView(jObj)
                    merchant_bank = 'IndusInd Bank'

                else:
                    continue

                transactionType = bank_Obj.parseTransactionType()

                if transactionType == 'Credit':

                    json_dict['towards'] = bank_Obj.getCreditedTowards()

                    if bank_Obj.getAccountNumber():
                        json_dict['account_number'] = bank_Obj.getAccountNumber()
                    elif bank_Obj.getCreditCardNumber():
                        json_dict['credit_card_number'] = bank_Obj.getCreditCardNumber()
                    else:
                        continue

                    json_dict['transaction_date'] = bank_Obj.getTransactionDate()

                    json_dict['transaction_amount'] = bank_Obj.getAmountCredited()

                    json_arr.append(json_dict)

                elif transactionType == 'Debit':

                    json_dict['towards'] = bank_Obj.getDebitedTowards()

                    transactionMethod = bank_Obj.getTransactionMethod()

                    if transactionMethod == 'Card':

                        cardType = bank_Obj.getCardType()

                        if cardType == 'Debit':

                            if bank_Obj.getDebitCardNumber():

                                json_dict['debit_card_number'] = bank_Obj.getDebitCardNumber()
                            else:
                                continue

                            json_dict['transaction_date'] = bank_Obj.getTransactionDate()
                            json_dict['transaction_amount'] = bank_Obj.getAmountDebited()

                        elif cardType == 'Credit':

                            if bank_Obj.getCreditCardNumber():

                                json_dict['credit_card_number'] = bank_Obj.getCreditCardNumber()
                            else:
                                continue
                            json_dict['transaction_date'] = bank_Obj.getTransactionDate()
                            json_dict['transaction_amount'] = bank_Obj.getAmountDebited()
                        else:

                            pass

                    elif transactionMethod == 'Net Banking':

                        if bank_Obj.getAccountNumber():
                            json_dict['account_number'] = bank_Obj.getAccountNumber()
                        else:
                            continue
                        json_dict['transaction_date'] = bank_Obj.getTransactionDate()

                        json_dict['transaction_amount'] = bank_Obj.getAmountDebited()

                    else:

                        pass

                    json_arr.append(json_dict)
                else:
                    continue

                category = bank_Obj.getCategory()

                if category == 'Net Banking' or category == 'ATM Withdrawal' or category == 'UPI':
                    merchant = merchant_bank
                else:
                    if bank_Obj.getMerchant():
                        merchant = bank_Obj.getMerchant()
                    else:
                        merchant = merchant_bank

                json_dict['category'] = category
                json_dict['merchant'] = merchant
                json_dict['bank_id'] = jObj['sender_id']
                json_dict['transaction_type'] = transactionType
                json_dict['available_balance'] = bank_Obj.getAvailableBalance()
        except:
            continue
    json_str = json.dumps(json_arr)
    return json_str

@login_required(login_url="/login/")
def user_data(request):
    ctx = {}
    user_list = SmsStringMaster.objects.values_list('user', flat=True).distinct()
    if user_list:
        for user in user_list:
            json_data = user_json_data(user)
            if json_data:
                ctx = {
                    "data": json_data
                }
                json_object = json.dumps(ctx)
                user_instance = User.objects.filter(id=user)
                if user_instance:
                    SmsStringMasterJsonData.objects.create(user=user_instance[0], json_data=json_object)
                else:
                    pass
        return HttpResponse("ok")
    else:
        return HttpResponse("something went wrong")

def user_json_data(user):
    sms_dict = {}
    sms_list = []
    final_list = []
    sms_master_obj = SmsStringMaster.objects.filter(user=user).distinct()
    if sms_master_obj:
        for data in sms_master_obj:
            sms_dict = {
               "sms_string": data.sms_string,
               "sender_id": data.sender_id,
               "date_time": str(data.date_time),
               "location": data.location,
               "is_parsed": data.is_parsed,
            }
            sms_list.append(sms_dict)
    final_list = [dict(t) for t in {tuple(d.items()) for d in sms_list}]
    return final_list

