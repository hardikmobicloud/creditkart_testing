from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator,MinValueValidator
from django.conf import settings


class SmsHeaderMaster(models.Model):

    """
    To store sms headers
    """

    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    header = models.CharField(blank=True, max_length=256)

    class Meta:
        ordering = ['-created_date']

    def __str__(self):
        return self.header


class SmsStringMaster(models.Model):

    """
    To store sms strings
    """

    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    sms_string = models.TextField(blank=True,null=True)
    sender_id = models.CharField(db_index=True,blank=True, max_length=256, null=True)
    date_time = models.DateTimeField(blank=True, null=True)
    location = models.CharField(blank=True, max_length=256, null=True)  # lat log
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sms_string_user_master_old', blank=True, null=True)
    is_parsed = models.BooleanField(default=False)

    class Meta:
        ordering = ['-created_date']

    def __str__(self):
        return self.sms_string


class SmsKeyWordMaster(models.Model):

    """
    To store sms keyword
    """

    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    keyword = models.CharField(blank=True, max_length=256)

    class Meta:
        ordering = ['-created_date']

    def __str__(self):
        return self.keyword


class CategoryModel(models.Model):

    """
    To store Categories
    """

    name = models.CharField(max_length=20)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True, null=True,blank=True)


class MerchantModel(models.Model):

    """
    To store Merchants
    """

    name = models.CharField(max_length=256, blank=True, null=True)
    sender_id = models.CharField(max_length=256, blank=True, null=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True, null=True, blank=True)
    logo = models.FileField(upload_to='bank_logos',blank=True, null=True, default="bank_logos/default_credit.png")


class DebitedTowardsKeywordModel(models.Model):

    """
    To store debited towards keywords
    """

    keyword = models.CharField(max_length=30,blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True, null=True, blank=True)


class CreditedTowardsKeywordModel(models.Model):

    """
    To store credited towards keywords
    """

    keyword = models.CharField(max_length=30, blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True, null=True, blank=True)


class SmsStringMasterJsonData(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="sms_json_data",
                               null=True, blank=True)
    re_status = models.BooleanField(default=False)
    json_data = models.TextField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
