from django.contrib import admin
from .models import *


class SmsHeaderMasterAdmin(admin.ModelAdmin):
    list_display = ('header', 'created_date',)
    list_filter = ('header', 'created_date',)
    exclude = ('updated_date',)


class SmsStringMasterAdmin(admin.ModelAdmin):
    # actions = None
    list_display = ('sms_string', 'sender_id','date_time', 'user')
    list_filter = ('sender_id', 'created_date','date_time', 'user')
    exclude = ('updated_date',)
    search_fields = ('user__username','date_time')

    # def has_add_permission(self, request):
    #
    #     if request.user.email in settings.SUPER_ADMIN_EMAIL:
    #
    #         return True
    #     else:
    #
    #         return False

    # def has_delete_permission(self, request, obj=None):
    #
    #     if request.user.email in settings.SUPER_ADMIN_EMAIL:
    #
    #         return True
    #     else:
    #
    #         return False

    # def has_change_permission(self, request, obj=None):
    #
    #     if request.user.email in settings.SUPER_ADMIN_EMAIL:
    #
    #         return True
    #     else:
    #         self.list_display_links = None
    #         return False


class SmsKeyWordMasterAdmin(admin.ModelAdmin):
    list_display = ('keyword', 'created_date',)
    list_filter = ('keyword', 'created_date',)
    exclude = ('updated_date',)



class MerchantModelAdmin(admin.ModelAdmin):

    list_display = ('name', 'sender_id', 'logo',)





admin.site.register(SmsHeaderMaster, SmsHeaderMasterAdmin)
admin.site.register(SmsStringMaster, SmsStringMasterAdmin)
admin.site.register(SmsKeyWordMaster, SmsKeyWordMasterAdmin)
admin.site.register(CategoryModel)
admin.site.register(MerchantModel, MerchantModelAdmin)
admin.site.register(CreditedTowardsKeywordModel)
admin.site.register(DebitedTowardsKeywordModel)
