from django.apps import AppConfig


class MarketingdataConfig(AppConfig):
    name = 'MarketingData'
