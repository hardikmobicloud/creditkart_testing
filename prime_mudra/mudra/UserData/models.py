from django.contrib.auth.models import User
from django.db import models
from django.contrib.postgres.fields import JSONField


class CibilData(models.Model):
    """
    To store user cibil data
    """

    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, related_name='user_cibil')
    cibil_length = models.CharField(max_length=256, blank=True, null=True)
    cibil_scorename = models.CharField(max_length=256, blank=True, null=True)
    cibil_scorecardname = models.CharField(max_length=256, blank=True, null=True)
    cibil_scorecardversion = models.CharField(max_length=256, blank=True, null=True)
    cibil_scoredata = models.CharField(max_length=256, blank=True, null=True)
    cibil_score = models.CharField(max_length=256, blank=True, null=True)
    cibil_reasoncode1field = models.CharField(max_length=256, blank=True, null=True)
    cibil_reasoncode1 = models.CharField(max_length=256, blank=True, null=True)
    cibil_reasoncode2field = models.CharField(max_length=256, blank=True, null=True)
    cibil_reasoncode2 = models.CharField(max_length=256, blank=True, null=True)
    cibil_decision = models.CharField(max_length=256, blank=True, null=True)
    cibil_response = models.TextField(blank=True, null=True)


class CibilRawData(models.Model):
    """
    To store user cibil raw data
    """

    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, related_name='user_cibil_raw')
    raw_cibil_response = models.TextField(blank=True, null=True)


class CibilReasonDetail(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True)
    reason_code = models.CharField(max_length=256, null=True, blank=True)
    reason_info = models.TextField(null=True, blank=True)


#
# class SmsStringMaster(models.Model):
#     """
#     To store sms strings
#     """
#
#     created_date = models.DateTimeField(auto_now_add=True)
#     updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
#     sms_string = models.TextField(blank=True)
#     sender_id = models.CharField(blank=True, max_length=256, null=True)
#     date_time = models.DateTimeField(blank=True, null=True)
#     location = models.CharField(blank=True, max_length=256, null=True)  # lat log
#     user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sms_string_user_master', blank=True,
#                              null=True)
#     is_parsed = models.BooleanField(default=False)
#     # sms data processing
#     balance = models.FloatField(null=True,blank=True)
#     type=models.CharField(blank=True,null=True,max_length=50)
#     card_type=models.CharField(blank=True,null=True,max_length=50)
#     marchant=models.CharField(blank=True,null=True,max_length=200)
#     amount=models.FloatField(blank=True,null=True)
#     status=models.CharField(default=False,max_length=10)
#
#     class Meta:
#         ordering = ['-created_date']
#
#     def __str__(self):
#         return self.sms_string


class NotificationEntry(models.Model):
    """
    To store Notifications
    """

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='notification_user_master', blank=True,
                             null=True)
    data = models.TextField()
    notification_title = models.CharField(max_length=255, blank=True, null=True)
    notification_body = models.CharField(max_length=255, blank=True, null=True)
    notification_type = models.CharField(max_length=255, blank=True, null=True)
    is_read = models.BooleanField()
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return self.notification_title


class DeviceData(models.Model):
    """
    To store raw data of user device coming from API
    """

    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE, related_name='user_raw_data_user')
    primary_mobile_number = models.CharField(blank=True, max_length=256)
    default_email = models.EmailField(blank=True, null=True)
    device_model = models.CharField(blank=True, max_length=256)
    device_brand = models.CharField(blank=True, max_length=256)
    version_number = models.CharField(blank=True, max_length=256)
    release = models.CharField(blank=True, max_length=256)
    screen_size = models.CharField(blank=True, max_length=256)
    device_id = models.CharField(blank=True, max_length=256)
    imei_number = models.CharField(blank=True, max_length=256)
    application_version = models.CharField(blank=True, max_length=256)
    installer = models.CharField(max_length=256, blank=True, null=True)
    is_rooted = models.BooleanField(default=False)
    is_emulated = models.BooleanField(default=False)
    is_debuggable = models.BooleanField(default=False)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.primary_mobile_number


class UserLocations(models.Model):
    """
    To capture user home and office locations
    """

    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_locations', blank=True, null=True)
    home_location = models.CharField(max_length=256, blank=True, null=True)
    office_location = models.CharField(max_length=256, blank=True, null=True)


class UserBankDetails(models.Model):
    """
    To store user bank details
    """

    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, related_name='user_bank_details_users')
    bank_id = models.CharField(blank=True, max_length=256)
    transaction_amount = models.DecimalField(decimal_places=2, max_digits=12)
    transaction_type = models.CharField(db_index=True, blank=True, max_length=256, choices=None)
    transaction_date = models.DateTimeField(blank=True, null=True)
    available_balance = models.DecimalField(decimal_places=2, max_digits=12)
    account_number = models.CharField(db_index=True, blank=True, max_length=256)
    towards = models.CharField(max_length=30, blank=True, null=True)
    category = models.CharField(max_length=30, blank=True, null=True, default='Other')
    merchant = models.CharField(db_index=True, max_length=30, blank=True, null=True, default='NA')

    class Meta:
        ordering = ['-id']


class UserCreditCardDetails(models.Model):
    """
    To store user bank details
    """

    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, related_name='user_credit_card_details_users')
    bank_id = models.CharField(blank=True, max_length=256)
    transaction_amount = models.DecimalField(decimal_places=2, max_digits=12)
    transaction_type = models.CharField(db_index=True, blank=True, max_length=256, choices=None)
    transaction_date = models.DateTimeField(blank=True, null=True)
    available_balance = models.DecimalField(decimal_places=2, max_digits=12)
    credit_card_number = models.CharField(db_index=True, blank=True, max_length=256)
    towards = models.CharField(max_length=30, blank=True, null=True)
    category = models.CharField(max_length=30, blank=True, null=True, default='Other')
    merchant = models.CharField(db_index=True, max_length=30, blank=True, null=True, default='NA')

    class Meta:
        ordering = ['-id']

    # def __str__(self):
    #     return self.bank_id


class UserDebitCardDetails(models.Model):
    """
    To store user bank details
    """

    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, related_name='user_debit_card_details_users')
    bank_id = models.CharField(blank=True, max_length=256)
    transaction_amount = models.DecimalField(decimal_places=2, max_digits=12)
    transaction_type = models.CharField(db_index=True, blank=True, max_length=256, choices=None)
    transaction_date = models.DateTimeField(blank=True, null=True)
    available_balance = models.DecimalField(decimal_places=2, max_digits=12)
    debit_card_number = models.CharField(db_index=True, blank=True, max_length=256)
    towards = models.CharField(max_length=30, blank=True, null=True)
    category = models.CharField(max_length=30, blank=True, null=True, default='Other')
    merchant = models.CharField(db_index=True, max_length=30, blank=True, null=True, default='NA')

    class Meta:
        ordering = ['-id']


class UserMerchantDetails(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, related_name='user_merchant_details_users')
    merchant_id = models.CharField(db_index=True, blank=True, max_length=256)
    transaction_amount = models.DecimalField(decimal_places=2, max_digits=12)
    transaction_type = models.CharField(db_index=True, blank=True, max_length=256, choices=None)
    transaction_date = models.DateTimeField(blank=True, null=True)
    category = models.CharField(max_length=20, blank=True, null=True, default='Other')

    class Meta:
        ordering = ['-id']


class ContactMaster(models.Model):
    """
    To store Contact Master
    """

    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    contact_no = models.CharField(blank=True, max_length=50, null=True)
    contact_name = models.CharField(blank=True, max_length=256, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='contact_user_master', blank=True,
                             null=True)

    class Meta:
        ordering = ['-created_date']

    def __str__(self):
        return self.contact_no


class ShareAppMaster(models.Model):
    """
    To store data of share app
    """

    number = models.CharField(blank=True, max_length=50, null=True)
    name = models.CharField(blank=True, max_length=256, null=True)
    # app_installed =models.BooleanField(default=False)
    # regestered =models.BooleanField(default=False)
    # converted =models.BooleanField(default=False)

    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='share_app_master', blank=True,
                             null=True)

    class Meta:
        ordering = ['-created_date']

    def __str__(self):
        return self.number


class PanCardMaster(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    pancard = models.CharField(blank=True, max_length=50, null=True)
    name = models.CharField(blank=True, max_length=256, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='pan_card_user_master', blank=True,
                             null=True)

    class Meta:
        ordering = ['-created_date']


class InstalledApps(models.Model):
    """
    To store data of installed apps
    """

    name = models.CharField(blank=True, max_length=80, null=True)
    package_name = models.CharField(blank=True, max_length=200, null=True)
    first_install_time = models.DateTimeField(blank=True, null=True)
    last_update_time = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='installed_app', blank=True,
                             null=True)

    class Meta:
        ordering = ['-created_date']

    def __str__(self):
        return self.name


class UserSharedData(models.Model):
    '''
    to check how many share contacts subscribed
    '''
    is_subscribed = models.BooleanField(default=False)
    subscription_amount = models.FloatField(null=True)
    is_paid_forsubscription = models.BooleanField(default=False)
    is_paid_forsubscription_date = models.DateField(null=True)
    is_approved = models.BooleanField(default=False)
    Approved_amount = models.FloatField(null=True)
    is_paid_forapproval = models.BooleanField(default=False)
    is_paid_forapproval_date = models.DateField(null=True)
    shareapp = models.ForeignKey(ShareAppMaster, on_delete=models.CASCADE, blank=True, null=True,
                                 related_name='user_cibil')
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)


class PanRequest(models.Model):
    date = models.DateField(blank=True, max_length=50, null=True)
    total_request_cnt = models.IntegerField(blank=True, null=True, default=0)
    pan_request_cnt = models.IntegerField(blank=True, null=True, default=0)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)


class ContactMasterJsonData(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_json_data",
                             null=True, blank=True)
    re_status = models.BooleanField(default=False)
    json_data = JSONField()
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
