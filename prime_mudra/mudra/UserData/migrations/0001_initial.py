# Generated by Django 2.2.4 on 2019-08-09 07:17

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CibilReasonDetail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
                ('reason_code', models.CharField(blank=True, max_length=256, null=True)),
                ('reason_info', models.TextField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='UserLocations',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
                ('home_location', models.CharField(blank=True, max_length=256, null=True)),
                ('office_location', models.CharField(blank=True, max_length=256, null=True)),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE,
                                           related_name='user_locations', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='SmsStringMaster',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
                ('sms_string', models.TextField(blank=True)),
                ('sender_id', models.CharField(blank=True, max_length=256, null=True)),
                ('date_time', models.DateTimeField(blank=True, null=True)),
                ('location', models.CharField(blank=True, max_length=256, null=True)),
                ('is_parsed', models.BooleanField(default=False)),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE,
                                           related_name='sms_string_user_master', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-created_date'],
            },
        ),
        migrations.CreateModel(
            name='NotificationEntry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data', models.TextField()),
                ('notification_title', models.CharField(blank=True, max_length=255, null=True)),
                ('notification_body', models.CharField(blank=True, max_length=255, null=True)),
                ('notification_type', models.CharField(blank=True, max_length=255, null=True)),
                ('is_read', models.BooleanField()),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_updated', models.DateTimeField(auto_now=True, null=True)),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE,
                                           related_name='notification_user_master', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='DeviceData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
                ('primary_mobile_number', models.CharField(blank=True, max_length=256)),
                ('default_email', models.EmailField(blank=True, max_length=254, null=True)),
                ('device_model', models.CharField(blank=True, max_length=256)),
                ('device_brand', models.CharField(blank=True, max_length=256)),
                ('version_number', models.CharField(blank=True, max_length=256)),
                ('release', models.CharField(blank=True, max_length=256)),
                ('screen_size', models.CharField(blank=True, max_length=256)),
                ('device_id', models.CharField(blank=True, max_length=256)),
                ('imei_number', models.CharField(blank=True, max_length=256)),
                ('application_version', models.CharField(blank=True, max_length=256)),
                ('installer', models.CharField(blank=True, max_length=256, null=True)),
                ('is_rooted', models.BooleanField(default=False)),
                ('is_emulated', models.BooleanField(default=False)),
                ('is_debuggable', models.BooleanField(default=False)),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE,
                                           related_name='user_raw_data_user', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-id'],
            },
        ),
        migrations.CreateModel(
            name='CibilRawData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
                ('raw_cibil_response', models.TextField(blank=True, null=True)),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE,
                                           related_name='user_cibil_raw', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='CibilData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
                ('cibil_length', models.CharField(blank=True, max_length=256, null=True)),
                ('cibil_scorename', models.CharField(blank=True, max_length=256, null=True)),
                ('cibil_scorecardname', models.CharField(blank=True, max_length=256, null=True)),
                ('cibil_scorecardversion', models.CharField(blank=True, max_length=256, null=True)),
                ('cibil_scoredata', models.CharField(blank=True, max_length=256, null=True)),
                ('cibil_score', models.CharField(blank=True, max_length=256, null=True)),
                ('cibil_reasoncode1field', models.CharField(blank=True, max_length=256, null=True)),
                ('cibil_reasoncode1', models.CharField(blank=True, max_length=256, null=True)),
                ('cibil_reasoncode2field', models.CharField(blank=True, max_length=256, null=True)),
                ('cibil_reasoncode2', models.CharField(blank=True, max_length=256, null=True)),
                ('cibil_decision', models.CharField(blank=True, max_length=256, null=True)),
                ('cibil_response', models.TextField(blank=True, null=True)),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE,
                                           related_name='user_cibil', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
