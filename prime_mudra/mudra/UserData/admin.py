from django.contrib import admin
from .models import *
from django.conf import settings
from django.http import HttpResponse


def download_csv(self, request, queryset):
    import csv
    final_str = ''
    f = open('some.csv', 'w')
    writer = csv.writer(f)
    writer.writerow(['user__username', 'contact_name', 'contact_no'])
    for s in queryset:
        writer.writerow([s.user.username, s.contact_name, s.contact_no])
        base_string = 'attachment; filename="'
        final_str = base_string + s.user.username + '.csv"'
    f.close()
    f = open('some.csv', 'r')
    response = HttpResponse(f, content_type='text/csv')
    response['Content-Disposition'] = final_str
    return response


class ContactMasterAdmin(admin.ModelAdmin):
    # actions = None
    list_display = ('contact_no', 'contact_name', 'user',)
    exclude = ('updated_date',)
    search_fields = ['user__username']
    actions = [download_csv]

    def has_add_permission(self, request):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return True
        else:

            return False

    def has_delete_permission(self, request, obj=None):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return True
        else:

            return False

    def has_change_permission(self, request, obj=None):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return True
        else:
            self.list_display_links = None
            return False


class DeviceDataAdmin(admin.ModelAdmin):
    # actions = None
    list_display = (
        'user', 'primary_mobile_number', 'default_email', 'device_model', 'device_brand', 'is_rooted', 'is_emulated',
        'is_debuggable', 'created_date')
    exclude = ('updated_date',)
    search_fields = ('primary_mobile_number', 'default_email', 'user__username')




class NotificationEntryAdmin(admin.ModelAdmin):
    # actions = None
    list_display = ('user', 'data', 'notification_title', 'notification_body', 'notification_type', 'is_read',)
    list_filter = ('notification_type',)
    exclude = ('updated_date',)
    search_fields = ('user__username', 'notification_title', 'notification_type',)

    def has_add_permission(self, request):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return True
        else:

            return False

    def has_delete_permission(self, request, obj=None):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return True
        else:

            return False

    def has_change_permission(self, request, obj=None):

        if request.user.email in settings.SUPER_ADMIN_EMAIL:

            return True
        else:
            self.list_display_links = None
            return False




class PanCardMasterAdmin(admin.ModelAdmin):
    list_display = ('created_date', 'pancard', 'name', 'user')
    list_filter = ('pancard',)
    search_fields = ('user__username', 'pancard',)



admin.site.register(PanCardMaster, PanCardMasterAdmin)
admin.site.register(ContactMaster, ContactMasterAdmin)
admin.site.register(NotificationEntry, NotificationEntryAdmin)
admin.site.register(DeviceData, DeviceDataAdmin)