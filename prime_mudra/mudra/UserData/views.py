from django.http import HttpResponse
from django.contrib.auth.models import User
from django.apps.registry import apps
from django.contrib.auth.decorators import login_required
ContactMasterJsonData = apps.get_model('UserData', 'ContactMasterJsonData')
ContactMaster = apps.get_model('UserData', 'ContactMaster')





from rest_framework.utils import json

@login_required(login_url="/login/")
def user_data(request):
    ctx = {}
    username_list = ContactMaster.objects.values_list('user__username', flat=True).distinct()
    if username_list:
        for username in username_list:
            json_data = user_json_data(username)
            if json_data:
                ctx = {
                    "data": json_data
                }
                json_object = json.dumps(ctx)
                user_instance = User.objects.filter(username=username)
                if user_instance:
                    ContactMasterJsonData.objects.create(user=user_instance[0], json_data=json_object)
                else:
                    pass
    else:
        return HttpResponse("something went wrong")



def user_json_data(username):
    contact_dict = {}
    contact_list = []
    final_list = []
    contact_master_obj = ContactMaster.objects.filter(user__username=username).distinct()
    if contact_master_obj:
        for data in contact_master_obj:
            contact_dict = {
               "contact_no": data.contact_no,
               "contact_name": data.contact_name
            }
            contact_list.append(contact_dict)
    final_list = [dict(t) for t in {tuple(d.items()) for d in contact_list}]
    return final_list


def is_users_contacts_valid(user_id):
    """
    This function counts the users contacts in ContactMaster and returns boolen data
    """
    if user_id:
        contacts = ContactMaster.objects.filter(user=user_id).values_list('contact_no').distinct()
        if contacts:
            contact_length = len(contacts)
            if contact_length > 50:
                return True

    return False