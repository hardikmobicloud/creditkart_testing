from django.apps.registry import apps
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views import generic

Insurance = apps.get_model('insurance', 'Insurance')
UserInsurance = apps.get_model('insurance', 'UserInsurance')


# Create your views here.
@method_decorator([login_required(login_url="/ecom/login/")], name='dispatch')
class SearchInsurance(generic.ListView):
    """
      This class used to search order by id, mobile number, status and unique Id.
    """
    template_name = "insurance/search_insurance.html"

    def get(self, request, status, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, status, *args, **kwargs):
        ctx = {}
        id_list = []
        if request.method == 'POST':
            name = request.POST.get('name')
            status = request.POST.get('status')
            if status == "user_name":
                obj = Insurance.objects.filter(user_insurance__user__username=name)
                if obj:
                    for id in obj:
                        id_list.append(id.id)
                    ctx["id_list"] = id_list
                    ctx['all_data'] = obj
                else:
                    ctx['msg'] = "No such Insurance exist"
            if status == "policy_number":
                policy_obj = Insurance.objects.filter(policy_number=name)
                if policy_obj:
                    for policy in policy_obj:
                        id_list.append(policy.id)
                    ctx["all_data"] = policy_obj
                    ctx["id_list"] = id_list
                else:
                    ctx['msg'] = "No such Insurance exist"
        return render(request, self.template_name, ctx)


def insurance_data(request):
    ctx = {}
    template_name = "insurance/insurance_data.html"
    user = User.objects.filter(id=request.user.id)
    print("user", user)
    if user:
        insurance_obj = Insurance.objects.filter(user_insurance__user=user[0])
        ctx["insurance_obj"] = insurance_obj
    else:
        ctx['msg'] = "No Insurance exist"
    return render(request, template_name, ctx)
