
from django.urls import path
from .views import *

urlpatterns = [

    path('search_insurance/<status>/', SearchInsurance.as_view()),
    path('insurance_data/', insurance_data),
]