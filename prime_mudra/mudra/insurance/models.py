from django.db import models
from django.contrib.auth.models import User
from User.models import Personal
# Create your models here.


class UserInsurance(models.Model):
    user_insurance_id = models.CharField(db_index=True, max_length=50, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, related_name="user_insurance")
    user_personal = models.ManyToManyField(Personal,blank=True, null=True, related_name="user_personal_insurance")
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, related_name="created_by_user_insurance")
    created_date = models.DateTimeField(auto_now_add=True)
    updated_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, related_name="updated_by_user_insurance")
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    re_staus = models.BooleanField(default=True, blank=True, null=True)

    class Meta:
        ordering = ['-id']



class Insurance(models.Model):
    premium_due_date = models.DateTimeField(blank=True, null=True)
    premium_expiry_date = models.DateTimeField(blank=True, null=True)
    premium_taken_date = models.DateTimeField(blank=True, null=True)
    type = models.CharField(db_index=True, max_length=50, blank=True, null=True)
    sub_type = models.CharField(db_index=True, max_length=50, blank=True, null=True)
    amount = models.FloatField(blank=True,null=True)
    insurance_name = models.CharField(db_index=True, max_length=100, blank=True, null=True)
    policy_name = models.CharField(db_index=True, max_length=100, blank=True, null=True)
    policy_number = models.CharField(db_index=True, max_length=50, blank=True, null=True)
    pdf_url = models.CharField(db_index=True, max_length=256, blank=True, null=True)
    payment_status = models.CharField(max_length=50, null=True, blank=True)
    user_insurance = models.ForeignKey(UserInsurance, on_delete=models.CASCADE, blank=True, null=True, related_name="userinsurance_id")
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, related_name="created_by_insurance")
    created_date = models.DateTimeField(auto_now_add=True)
    updated_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, related_name="updated_by_insurance")
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    re_staus = models.BooleanField(default=True, blank=True, null=True)

    class Meta:
        ordering = ['-id']



