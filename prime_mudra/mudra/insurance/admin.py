from django.contrib import admin
from .models import *


# Register your models here.


class UserInsuranceAdmin(admin.ModelAdmin):
    list_display = ('user_insurance_id', 'user', 'created_by', 'created_date', 'updated_by', 'updated_date',)
    list_filter = ('user_insurance_id', 'user')
    search_fields = ['user_insurance_id', 'user']


admin.site.register(UserInsurance, UserInsuranceAdmin)


class InsuranceAdmin(admin.ModelAdmin):
    list_display = (
    'insurance_name', 'policy_name', 'policy_number', 'premium_due_date', 'payment_status', 'premium_expiry_date',
    'premium_taken_date',
    'type', 'sub_type', 'amount', 'pdf_url', 'user_insurance', 'created_by', 'created_date', 'updated_by',
    'updated_date',)
    list_filter = ('policy_name', 'payment_status', 'type', 'sub_type')
    search_fields = ['policy_name', 'policy_number', 'type', 'sub_type', 'user_insurance']


admin.site.register(Insurance, InsuranceAdmin)
