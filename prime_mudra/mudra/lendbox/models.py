from LoanAppData.models import LoanApplication
from django.contrib.auth.models import User
from django.db import models

from mudra import constants


class LendboxApplication(models.Model):
    lb_user_id = models.CharField(max_length=50, null=True, blank=True)
    loan_id = models.ForeignKey(LoanApplication, null=True, blank=True, on_delete=models.CASCADE)
    lb_loan_id = models.CharField(max_length=50, null=True, blank=True)
    date = models.DateTimeField(null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    response = models.TextField(blank=True, null=True)
    status = models.CharField(max_length=50, null=True, blank=True, choices=constants.LENDBOX_APPLICATION_STATUS)
    api_status = models.CharField(max_length=50, null=True, blank=True, choices=constants.LENDBOX_API_STATUS)

    def __str__(self):
        return '%s %s' % (self.loan_id, self.lb_user_id)


class LendboxUser(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    lb_user_id = models.CharField(max_length=50, null=True, blank=True)
    response = models.TextField(null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    status = models.CharField(max_length=20, null=True, blank=True, choices=constants.LENDBOX_API_STATUS)

    def __str__(self):
        return '%s %s' % (self.user, self.lb_user_id)


class UserDeviceData(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    loan_id = models.CharField(max_length=50, null=True, blank=True)
    ip_address = models.CharField(max_length=50, null=True, blank=True)
    user_agent = models.CharField(max_length=50, null=True, blank=True)
    response = models.TextField(null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s %s' % (self.user, self.user_agent)


class LendboxApiStatus(models.Model):
    loan_id = models.ForeignKey(LoanApplication, null=True, blank=True, on_delete=models.CASCADE)
    registration = models.BooleanField(default=False)
    create_loan = models.BooleanField(default=False)
    documents_upload = models.BooleanField(default=False)
    signature_verification = models.BooleanField(default=False)
    response = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=50, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return '%s %s' % (self.loan_id, self.status)