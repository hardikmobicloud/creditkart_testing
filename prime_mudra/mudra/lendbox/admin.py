from django.contrib import admin

from .models import *


class LendboxApplicationAdmin(admin.ModelAdmin):
    list_display = ['loan_id', 'lb_loan_id', 'lb_user_id', 'status', 'api_status', 'response', 'date', 'created_date']
    search_fields = ('loan_id__loan_application_id', 'lb_loan_id', 'lb_user_id')
    list_filter = ['status', 'api_status', 'lb_loan_id', 'lb_user_id', 'created_date']


admin.site.register(LendboxApplication, LendboxApplicationAdmin)


class LendboxUserAdmin(admin.ModelAdmin):
    list_display = ['user', 'lb_user_id', 'status', 'created_date', 'response', 'created_date']
    search_fields = ('user', 'lb_user_id')
    list_filter = ['status', 'created_date']


admin.site.register(LendboxUser, LendboxUserAdmin)


class UserDeviceDataAdmin(admin.ModelAdmin):
    list_display = ['user', 'loan_id', 'ip_address', 'user_agent', 'created_date']
    search_fields = ('user', 'loan_id', 'user_agent')


admin.site.register(UserDeviceData, UserDeviceDataAdmin)


class LendboxApiStatusAdmin(admin.ModelAdmin):
    list_display = ['loan_id', 'registration', 'create_loan', 'documents_upload', 'signature_verification', 'status',
                    'created_date', 'updated_date']
    search_fields = (
        'loan_id', 'registration', 'create_loan', 'documents_upload', 'signature_verification', 'status',
        'created_date',)


admin.site.register(LendboxApiStatus, LendboxApiStatusAdmin)
