from django.urls import path

from .lapi import *
from .views import *

urlpatterns = [
    path("test/", test_api),
    path("lendbox_bulk_upload/", lendbox_bulk_upload, name="lendbox_bulk_upload"),
    path("get_required_registration_details/", get_required_registration_details,
         name="get_required_registration_details"),

    path("lendbox_loan_csv/", lendbox_loan_csv, name="lendbox_loan_csv"),

    # csv report
    path("lendbox_application_report/", LendboxApplicationReport.as_view()),
    path("lendbox_user_report/", LendboxUserReport.as_view()),

]
