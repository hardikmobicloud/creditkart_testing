import csv

from django.apps.registry import apps
from django.http import HttpResponse
from django.views import generic
from django.shortcuts import render, HttpResponse
from .models import *
from datetime import timedelta

# LendboxUser = apps.get_model('lendbox', 'LendboxUser')
# UserDeviceData = apps.get_model('lendbox', 'UserDeviceData')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')


def lendbox_loan_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="Lendbox Loans.csv"'
    writer = csv.writer(response)
    writer.writerow(
        ['Username', "loan application id", "mudra loan id", 'lendbox loan id', 'lendbox user id', 'loan start date',
         'loan created date', 'loan status', 'APi status'])

    lendbox_loan = LendboxApplication.objects.filter(api_status="success").order_by('-id')
    if lendbox_loan:
        for i in lendbox_loan:
            # d = get_lb_registration_api_payload(i)
            loan_application_id = i.loan_id.loan_application_id
            mobile = i.loan_id.user.username
            get_loan = LoanApplication.objects.filter(loan_application_id=loan_application_id)
            if get_loan:
                mudra_loan_id = get_loan[0].loan_application_id
                writer.writerow(
                    [mobile, loan_application_id, mudra_loan_id, i.lb_user_id, i.lb_loan_id, i.date, i.created_date,
                     i.status, i.api_status])
    return response


import datetime
class LendboxApplicationReport(generic.ListView):
    def get(self, request, *args, **kwargs):
        template_name = "lendbox/lenbox_application_report.html"
        return render(request, template_name)
    def post(self, request, *args, **kwargs):
        time = datetime.datetime.now()
        date_string = str(time)[:10]
        base_string = 'attachment; filename="'
        final_str = base_string + date_string + "_"  "len_box_application " + '.csv"'
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = final_str
        writer = csv.writer(response)
        writer.writerow(["User", "Lendbox user ID", "Loan ID", "Lendbox loan ID", "created date", "response",'status'])
        if "start_date" in request.POST:
            print("inside")
            start_date = request.POST["start_date"]
            end_date = request.POST["end_date"]
            status = request.POST["status"]
            print("start_date",start_date)
            print("end_date",end_date)
            print("status",status)
            s = datetime.datetime.strptime(start_date, "%Y-%m-%d").date()
            e = datetime.datetime.strptime(end_date, "%Y-%m-%d").date() + timedelta(days=1)
            if status == 'all':
                object = LendboxApplication.objects.filter(created_date__range=(s, e))
            else:
                object = LendboxApplication.objects.filter(created_date__range=(s, e), api_status=status)

            if object:
                print("object", object)
                for obj in object:
                    writer.writerow(
                        [obj.loan_id.user.username, obj.lb_user_id, obj.loan_id.loan_application_id, obj.lb_loan_id,
                         obj.created_date, obj.response,obj.api_status])
        return response


class LendboxUserReport(generic.ListView):
    def get(self, request, *args, **kwargs):
        template_name = "lendbox/lenbox_user_report.html"
        return render(request, template_name)
    def post(self, request, *args, **kwargs):
        from datetime import timedelta
        time = datetime.datetime.now()
        date_string = str(time)[:10]
        base_string = 'attachment; filename="'
        final_str = base_string + date_string + "_"  "len_box_user " + '.csv"'
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = final_str
        writer = csv.writer(response)
        writer.writerow(["User", "Lendbox user ID", "created date", "response","status"])
        if "start_date" in request.POST:
            start_date = request.POST["start_date"]
            end_date = request.POST["end_date"]
            status = request.POST["status"]
            s = datetime.datetime.strptime(start_date, "%Y-%m-%d").date()
            e = datetime.datetime.strptime(end_date, "%Y-%m-%d").date() + timedelta(days=1)
            print("",s,e,request.POST)
            if status == "all":

                object = LendboxUser.objects.filter(created_date__range=(s,e))
            else:
                object = LendboxUser.objects.filter(created_date__range=(s,e), status=status)

            print("object",object)
            if object:
                for obj in object:
                    writer.writerow(
                        [obj.user.username, obj.lb_user_id, obj.created_date,obj.response,obj.status])
        return response
