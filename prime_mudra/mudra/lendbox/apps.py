from django.apps import AppConfig


class LendboxConfig(AppConfig):
    name = 'lendbox'
