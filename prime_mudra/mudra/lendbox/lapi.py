import csv
import logging
import re
import time
from datetime import datetime

import requests
from User.models import *
from django.apps.registry import apps
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, HttpResponse

logger = logging.getLogger(__name__)

Personal = apps.get_model('User', 'Personal')
Address = apps.get_model('User', 'Address')
Document = apps.get_model('User', 'Documents')
LendboxUser = apps.get_model('lendbox', 'LendboxUser')
UserDeviceData = apps.get_model('lendbox', 'UserDeviceData')

# Dev Urls
# BORROWER_REGISTRATION_URL = "https://api-dev.lendbox.in/v1/partners/borrower/signup"
# CREATE_LOAN_URL = "https://api-dev.lendbox.in/v1/partners/create-loan?lbUserId="
# UPLOAD_DOCS = "https://api-dev.lendbox.in/v1/partners/user/upload/docs?userId="
# UPLOAD_DOCS = "https://api-dev.lendbox.in/v1/partners/upload/docs?lbUserId="
# WALLET_ORDERS = "https://api-dev.lendbox.in/v1/partners/wallet/orders"
# TRANSACTION_STATUS = "https://api-dev.lendbox.in/v1/partners/wallet/orders/status"
# ESIGN_DECLARATION = "https://api-dev.lendbox.in/v1/partners/e-sign/verify?lbUserId="

# Prod Urls
BORROWER_REGISTRATION_URL = "https://api.lendbox.in/v1/partners/borrower/signup"
CREATE_LOAN_URL = "https://api.lendbox.in/v1/partners/create-loan?lbUserId="
UPLOAD_DOCS = "https://api.lendbox.in/v1/partners/upload/docs?lbUserId="
WALLET_ORDERS = "https://api.lendbox.in/v1/partners/wallet/orders"
TRANSACTION_STATUS = "https://api.lendbox.in/v1/partners/wallet/orders/status"
ESIGN_DECLARATION = "https://api.lendbox.in/v1/partners/e-sign/verify?lbUserId="

#  Dev Credentials
# prod_lendbox_api_key = "GHAE31vC9Q2wzcAb0G7M4q4PZhLLVXmPJWUJTapqDnRRGmA3LiH0PgagEJfEYvqkcNmRJDw1hbb3vgKduuP0wn2Zxun7b8LHKfEc"
# prod_lendbox_api_code = "FUVNTWX+NlnQLkeTnzyKuw=="

#  Prod Credentials
prod_lendbox_api_key = "OoC8nsn0g1cQlzFqXdkVmhYyUWCYSuPhdSeynqKfAbzSMRYaJQ6Dp1QkhUEwmlEGPIAq0fCIiJNDnXjV49RcpQG4F9n3B72X3BOK"
prod_lendbox_api_code = "n870o4k49yW0/QHllkxCCw=="

# prod_lendbox_api_key = "fdCuikdhlua8_izfdguf9pdun0gdLnL6bwzkpkf5Ck13zkkhulma0ydb5dtzbaxrmb5tfiLka5h4owtmqk_eqfiydbnvaxLfdmpg"
# prod_lendbox_api_code = "shCmwhKbFBFfrCA213zeBw=="

LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
LendboxApplication = apps.get_model('lendbox', 'LendboxApplication')
LendboxApiStatus = apps.get_model('lendbox', 'LendboxApiStatus')

import random
import re


def create_aadhar_no():
    """
    This function is used to create unique aadhar number
    """
    aadhar_number = None
    get_aadhar_number = random.sample(range(000000000000, 999999999999), 1)
    data = str(get_aadhar_number[0])
    length = len(data)
    check_is_digit = data.isdigit()
    check_first = data[0]
    l = ["0", "1"]
    if length == 12 and check_is_digit and check_first not in l:
        aadhar_number = get_aadhar_number[0]
    else:
        aadhar_number = create_aadhar_no()

    if aadhar_number:
        return aadhar_number


def get_url(url_name, user_id):
    url = ""
    if url_name is not None:
        url = url_name + str(user_id)
    return url


def call_lendbox_api(method, url, payload, files=None):
    headers = {
        'x-api-key': prod_lendbox_api_key,
        'x-api-code': prod_lendbox_api_code
    }
    response = requests.post(url, headers=headers, json=payload)
    # response = response.text.encode('utf8')
    print("===1", response)
    response = response.json()
    print("==response=", response)
    return response


def test_api():
    # data = lendbox_registration_api("9082867204")
    # now = datetime.now()
    # now.strftime('%Y-%m-%dT%H:%M:%S')
    # timestamp = int(time.mktime(now.timetuple()))
    # print("timestamp", timestamp)
    # print("timestamp11111111", int(timestamp))
    # # print("999999999999999999999999999", data)
    # timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
    # print("datetime_object", timestamp)
    name = 'JAYVANT'
    firstName = ""
    lastName = None
    temp = name.split(' ')
    if len(temp) >= 2:
        firstName = temp[0]
        lastName = temp[-1]
    else:
        firstName = temp[0]
    if lastName == "" or lastName is None:
        lastName = "Done"
    print('FURSTNAME', firstName, "lastname", lastName)


def get_lb_registration_api_payload(username):
    payload = {}
    if username is not None:
        email = ""
        title = "Mr."
        firstName = ""
        lastName = "None"
        mobile = ""
        aadhar = ""
        street1 = ""
        street2 = "None"
        city = ""
        pin = ""
        pan = ""
        dob = ""
        fathersName = "None"
        residenceType = None
        employmentType = "SALARIED_EMP"
        # modeOfIncome = "BANK_CREDIT"
        if username is not None:
            user = User.objects.filter(username=username).first()
            if user:
                try:
                    mobile = user.username
                    get_personal_obj = Personal.objects.filter(user_id=user.id).order_by('-id').first()
                    print("get_personal_obj", get_personal_obj)
                    if get_personal_obj:
                        if get_personal_obj.gender == "Male":
                            title = "Mr."
                        elif get_personal_obj.gender == "Female":
                            if get_personal_obj.marital_status == "MARRIED":
                                title = "Ms."
                            else:
                                title = "Ms."
                        if get_personal_obj.fathers_name:
                            fathersName = get_personal_obj.fathers_name
                        email = get_personal_obj.email_id
                        name = get_personal_obj.name

                        # Getting residence type from personal..
                        residence_type = get_personal_obj.residence_type
                        if residence_type == "Owned by Self or Spouse":
                            residenceType = 38
                        elif residence_type == "Living with Parents":
                            residenceType = 39
                        elif residence_type == "Rented with Family":
                            residenceType = 80
                        elif residence_type == "Rented with Friends":
                            residenceType = 94
                        elif residence_type == "Rented Individually":
                            residenceType = 95
                        elif residence_type == "Paying Guest or Hostel":
                            residenceType = 96
                        else:
                            residenceType = 80

                        get_aadhar = Personal.objects.filter(user_id=user.id).exclude(aadhar_number=None).order_by(
                            'id').last()
                        if get_aadhar:
                            aadhar = get_aadhar.aadhar_number

                        else:
                            aadhar = create_aadhar_no()  # Sample aadhar card

                        date_of_birth = str(get_personal_obj.birthdate)[:10]
                        print("date_of_birth", date_of_birth)
                        # date_of_birth = "1993-03-04"
                        dob = time.mktime(datetime.strptime(date_of_birth, "%Y-%m-%d").timetuple())
                        print('date_stamp', dob)

                        try:
                            temp = name.split(' ')
                            if len(temp) >= 2:
                                firstName = temp[0]
                                lastName = temp[-1]
                            else:
                                if len(temp) >= 1:
                                    firstName = temp[0]
                                    lastName = firstName
                                else:
                                    return False
                        except Exception as e:
                            print("inside lendbox_registration_api")

                        pan = get_personal_obj.pan_number

                        get_address_obj = Address.objects.filter(user_id=user.id).order_by('-id').first()
                        print("get_address_obj", get_address_obj)
                        if get_address_obj:
                            city = get_address_obj.city
                            pin = get_address_obj.pin_code
                            address = get_address_obj.address
                            temp = address.split(';')
                            print("temp", temp)
                            if len(temp) >= 2:
                                street1 = temp[0]
                                street2 = temp[-1]
                            else:
                                street1 = address

                            if street2 == "" or street2 is None:
                                street2 = "None"
                except Exception as e:
                    print("inside exception of get_lb_registration_api_payload", e)
                    print(e.args)
                    import os
                    import sys
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                    print(exc_type, fname, exc_tb.tb_lineno)

            payload = {
                "email": email,
                "title": title,
                "firstName": firstName,
                "lastName": lastName,
                "mobile": mobile,
                "aadhar": aadhar,
                "street1": street1,
                "street2": street2,
                "city": city,
                "pin": pin,
                "pan": pan,
                "dob": dob,
                "fathersName": fathersName,
                "residenceType": residenceType,
                "employmentType": employmentType
            }
    print("payload====================================", payload)
    return payload


def lendbox_registration_api(username):
    """
    This API is for registration of an user for Lendbox and it returns lendbox user id..
    """
    lendbox_user_id = None
    status = None
    if username is not None:
        method = 'POST'
        user = User.objects.filter(username=username).first()
        if user:
            status = "initiated"
            # Creating initiated entry in Lendbox User table..
            add_lb_user = LendboxUser.objects.create(user=user, lb_user_id=lendbox_user_id, status=status)

            payload = get_lb_registration_api_payload(user.username)
            if payload:
                # Calling Lendbox Borrower registration API..
                response = call_lendbox_api(method, BORROWER_REGISTRATION_URL, payload)
                print("====resp", response)
                # response = True
                if response:
                    """
                        {
                          "success": true,
                          "msg": "User Registered successfully.",
                          "data": {
                            "lbUserId": 190061
                          }
                        }
                    """

                    if "success" in response:
                        success = response['success']
                        if success == "true" or success == "True" or success:
                            status = "success"
                            if "data" in response:
                                data = response["data"]
                                if "lbUserId" in data:
                                    lendbox_user_id = data['lbUserId']
                            pass
                        else:
                            status = "error"
                            pass

                    if add_lb_user:
                        # Creating entry in LendboxUser Table..
                        add_lb_user = LendboxUser.objects.filter(id=add_lb_user.id).update(lb_user_id=lendbox_user_id,
                                                                                           status=status,
                                                                                           response=response)

    return lendbox_user_id


def get_lb_user_id(mobile_no):
    """
    This function get lendbox user id from LendboxUser table.. if not exists then it calls
    Lendbox Registration APi and returns lb_user_id..
    """
    lb_user_id = None
    if mobile_no is not None:
        user_obj = User.objects.filter(username=mobile_no)
        if user_obj:
            # Checking lendbox user id in Lendbox table..
            lb_user_obj = LendboxUser.objects.filter(user=user_obj[0], status="success").order_by('id').last()
            if lb_user_obj:
                lb_user_id = lb_user_obj.lb_user_id

            else:
                # Calling lendbox_registration_api..
                lb_user_id = lendbox_registration_api(mobile_no)

    return lb_user_id


def get_loan_data_payload(loan_id):
    payload = {}
    loan_amount = None
    lendbox_tenure = '30'
    lendbox_roi = '24'
    loanType = "CL_MUDRAKWIK"

    if loan_id is not None:
        loan_obj = LoanApplication.objects.filter(id=loan_id)
        if loan_obj:
            loan_amount = loan_obj[0].loan_scheme_id

    if loan_amount:
        payload = {
            "roi": int(lendbox_roi),
            "loanAmount": int(loan_amount),
            "loanTenure": int(lendbox_tenure),
            "durationInDays": 1,
            "loanType": loanType
        }
    print("get_loan_data_payload", payload)
    return payload


def lendbox_create_loan_api(loan_application_id, lb_user_id):
    if None not in (loan_application_id, lb_user_id):
        method = 'POST'
        lendbox_loan_id = None
        loan_id = None
        get_loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
        print("get_loan_obj", get_loan_obj)
        if get_loan_obj:
            create_loan = LendboxApplication.objects.create(loan_id=get_loan_obj[0], lb_user_id=lb_user_id,
                                                            api_status="initiated")
            print("create_loan", create_loan)
            loan_id = get_loan_obj[0].id
            payload = get_loan_data_payload(loan_id)
            if payload:
                api_status = None
                url = get_url(CREATE_LOAN_URL, lb_user_id)
                response = call_lendbox_api(method, url, payload)
                if response:
                    """
                    {
                      "success": true,
                      "msg": "New loan created successfully",
                      "data": {
                        "lbLoanId": 11861
                      }
                    }
                    """
                    if "success" in response:
                        success = response['success']
                        print("===success", success)
                        if success == "true" or success == "True" or success:
                            api_status = "success"
                            if "data" in response:
                                data = response["data"]
                                print("-====len", data['lbLoanId'])

                                if "lbLoanId" in data:
                                    lendbox_loan_id = data['lbLoanId']
                                    print("-====lendbox_loan_id====", lendbox_loan_id)

                        else:
                            api_status = "error"
                            pass
                    print("====create_loan", create_loan)
                    if create_loan:
                        update_loan = LendboxApplication.objects.filter(id=create_loan.id).update(
                            lb_loan_id=lendbox_loan_id, api_status=api_status, response=response)

                        if api_status == "success":
                            return True
                        if api_status == "error":
                            return False
    return False


def get_user_doc_path(user_id, type):
    path = None
    get_personal_obj = Document.objects.filter(user_id=user_id, type=type).order_by('id').last()
    if get_personal_obj:
        path = get_personal_obj.path

    return path


def lendbox_upload_document_api(loan_application_id, lb_user_id):
    extension = 'jpg'
    fname = 'test.jpg'
    method = 'POST'
    payload = {}
    url = get_url(UPLOAD_DOCS, lb_user_id)
    print("--url", url)
    print("--loan_application_id", loan_application_id)
    print("--lb_user_id", lb_user_id)
    headers = {
        'x-api-key': prod_lendbox_api_key,
        'x-api-code': prod_lendbox_api_code
    }
    status = 'PRE_APPROVED'
    loan_users = LoanApplication.objects.filter(loan_application_id=loan_application_id).first()
    print("===loan_users=", loan_users, loan_users.id)
    if loan_users:
        user_id = loan_users.user_id

        aadhar_back_doc = get_user_doc_path(user_id, "Aadhar_Back")
        if not aadhar_back_doc:
            return False

        if not loan_users.aadhar_id:
            aadhar_front_doc = get_user_doc_path(user_id, "Aadhar_Front")
            if not aadhar_front_doc:
                return False
        else:
            aadhar_front_doc = loan_users.aadhar_id.path

        pan_doc = loan_users.pancard_id.path

        # aadhar_front_doc = 'images/b00b116a-e8f.jpg'
        # aadhar_back_doc = 'images/b00b116a-e8f.jpg'

        from MApi import botofile
        print("loan_users.aadhar_id", loan_users.aadhar_id)
        print("loan_users.pancard_id", loan_users.pancard_id)
        if aadhar_front_doc and aadhar_back_doc and pan_doc:

            upload_aadhar = False
            upload_aadhar_back = False
            upload_pan = False
            document_aadhar_back = botofile.get_url(aadhar_back_doc)
            document_aadhar = botofile.get_url(aadhar_front_doc)
            document_pan = botofile.get_url(pan_doc)

            print("======================")
            # files = [('upload', ('thumbnail.png', open('thumbnail.png', 'rb'), 'image/png'))]
            aadhar_front_doc = str(aadhar_front_doc)
            temp = aadhar_front_doc.split('/')

            file_type = 'image/' + extension
            if temp:
                fname = temp[-1]
                if '.' in fname:
                    ext = fname.split('.')
                    extension = ext[-1]
                    print("=====eextension", extension)
                print("=====fname", fname)
            print("=====file_type", file_type)

            aadhar_file_path = '/media/images/' + fname
            aadhar_file_path = settings.BASE_DIR + aadhar_file_path
            aadhar_response = requests.get(document_aadhar)
            with open(aadhar_file_path, 'wb') as f:
                f.write(aadhar_response.content)
                f.close()

            image_file_descriptor = (fname, open(aadhar_file_path, 'rb'), file_type)
            files = {'AADHAR': image_file_descriptor}
            print("=============files", files)

            # response = call_lendbox_api(method, url, payload, files=files)

            response = requests.request(method, url, headers=headers, data=payload, files=files)
            print("response ", response)
            response = response.json()
            print("----res", response)
            if "success" in response:
                """
                    {
                      "success": true,
                      "msg": " uploaded successfully.",
                      "data": {}
                    }
                """
                success = response['success']
                if success == "true" or success:
                    upload_aadhar = True

            # Uploading aadhar back
            aadhar_back_doc = str(aadhar_back_doc)
            temp_ab = aadhar_back_doc.split('/')
            print("==temp_ab", temp_ab)
            file_type = 'image/' + extension
            if temp_ab:
                fname = temp_ab[-1]
                if '.' in fname:
                    ext = fname.split('.')
                    extension = ext[-1]
                    print("=====eextension", extension)
                print("=====fname", fname)
            print("=====file_type", file_type)

            aadhar_back_file_path = '/media/images/' + fname
            aadhar_back_file_path = settings.BASE_DIR + aadhar_back_file_path
            aadhar_back_response = requests.get(document_aadhar_back)
            with open(aadhar_back_file_path, 'wb') as f:
                f.write(aadhar_back_response.content)
                f.close()

            image_file_descriptor = (fname, open(aadhar_back_file_path, 'rb'), file_type)
            files = {'AADHAR': image_file_descriptor}
            print("=============files", files)

            response = requests.request(method, url, headers=headers, data=payload, files=files)
            response = response.json()
            print("----res", response)
            if "success" in response:
                """
                    {
                      "success": true,
                      "msg": " uploaded successfully.",
                      "data": {}
                    }
                """
                success = response['success']
                if success == "true" or success:
                    upload_aadhar_back = True

            # Uploading pan

            pan_response = requests.get(document_pan)
            path_pan = str(loan_users.pancard_id.path)
            temp = path_pan.split('/')

            file_type = 'image/' + extension
            if temp:
                fname = temp[-1]
                if '.' in fname:
                    ext = fname.split('.')
                    extension = ext[-1]
                    print("=====eextension", extension)
                print("=====fname", fname)
            print("=====file_type", file_type)

            pan_file_path = '/media/images/' + fname
            pan_file_path = settings.BASE_DIR + pan_file_path
            with open(pan_file_path, 'wb') as f:
                f.write(pan_response.content)
                f.close()
            image_file_descriptor = (fname, open(pan_file_path, 'rb'), file_type)
            files = {'PAN': image_file_descriptor}
            print("=============files", files)
            # response = call_lendbox_api(method, url, payload, files=files)
            response = requests.request(method, url, headers=headers, data=payload, files=files)
            response = response.json()
            print("----res", response)
            if "success" in response:
                success = response['success']
                if success == "true" or success:
                    upload_pan = True
            if upload_pan and upload_aadhar and upload_aadhar_back:
                return True
    return False


def get_user_device_data(lb_user_id):
    payload = {}
    lb_user_obj = LendboxUser.objects.filter(lb_user_id=lb_user_id, status="success").order_by('id').last()
    if lb_user_obj:
        user_id = lb_user_obj.user_id
        get_user_device_data = UserDeviceData.objects.filter(user_id=user_id).first()
        if get_user_device_data:
            timestamp = None
            try:
                date = get_user_device_data.created_date
                date.strftime('%Y-%m-%dT%H:%M:%S')
                timestamp = int(time.mktime(date.timetuple()))
                timestamp = timestamp * 1000
                print("timestamp", timestamp)
            except:
                pass

            payload = {
                "ip": get_user_device_data.ip_address,
                "ua": get_user_device_data.user_agent,
                "timestamp": timestamp
            }
    return payload


def lendbox_e_sign_declaration_api(lb_user_id):
    method = 'POST'
    user_id = lb_user_id
    url = get_url(ESIGN_DECLARATION, user_id)
    print("--url", url)
    payload = get_user_device_data(lb_user_id)

    # if "timestamp" in payload:
    #     ts = payload['timestamp']
    # else:
    #     ts = "1601448966000"
    # payload = {
    #     "ip": "223.229.190.79",
    #     "ua": "okhttp/3.12.1",
    #     "timestamp": ts
    # }

    print("--payload", payload)
    if payload:
        response = call_lendbox_api(method, url, payload)
        print("--priu", response)

        if response:
            if "success" in response:
                """
                    {
                      "success": true,
                      "msg": " uploaded successfully.",
                      "data": {}
                    }
                """
                success = response['success']
                if success == "true" or success:
                    return True
    return False


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def save_user_data(request, loan_id):
    ua = request.META['HTTP_USER_AGENT']
    ip = get_client_ip(request)
    meta = request.META
    get_loan_obj = LoanApplication.objects.filter(loan_application_id=loan_id).first()
    if get_loan_obj:
        save_data = UserDeviceData.objects.create(user_id=get_loan_obj.user_id, loan_id=loan_id, ip_address=ip,
                                                  user_agent=ua, response=meta)
    else:
        save_data = UserDeviceData.objects.create(user_id=None, loan_id=loan_id, ip_address=ip,
                                                  user_agent=ua, response=meta)


def generate_lendox_application_main(loan_application_id, mobile_no):
    """
    This function is the main function which calls all API's of lendbox API's one by one and returns
    status of each API..
    """
    return_dict = {}
    registration_done = "False"
    loan_created = "False"
    documents_uploading = "False"
    signature_declaration = "False"

    if None not in (loan_application_id, mobile_no):
        # Getting lendbox user id from get_lb_user_id function..
        lb_user_id = get_lb_user_id(mobile_no)
        if lb_user_id:
            registration_done = "True"
            # Calling Loan Generation API..
            generate_loan = lendbox_create_loan_api(loan_application_id, lb_user_id)
            print("generate_loan", generate_loan)
            if generate_loan:
                loan_created = "True"
                print("generate_loan", generate_loan)

                # Calling Documents uploading API..
                print("upload_documents---")
                # if document are already uploaded then dont upload the documents
                lendbox_applications = LendboxApplication.objects.filter(lb_user_id=lb_user_id, api_status="success")
                len_cnt = len(lendbox_applications)
                print("--lendbox_applications success count", len_cnt)
                if len_cnt < 2:
                    upload_documents = lendbox_upload_document_api(loan_application_id, lb_user_id)
                else:
                    upload_documents = True
                print("upload_documents---end", upload_documents)
                if upload_documents:
                    documents_uploading = "True"
                    # Calling E-signature verification..
                    signature_verification = lendbox_e_sign_declaration_api(lb_user_id)
                    if signature_verification:
                        signature_declaration = "True"

        return_dict["registration_done"] = registration_done
        return_dict["loan_created"] = loan_created
        return_dict["documents_uploading"] = documents_uploading
        return_dict["signature_declaration"] = signature_declaration

    return return_dict


def validate_bulk_application_data(loan_application_id, mobile_no):
    """
    This function takes Loan application id and mobile number and validate it..
    """

    loan_linked_to_mobile = False
    loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id, user__username=mobile_no)
    print("loan_obj", loan_obj)
    if loan_obj:
        loan_linked_to_mobile = True

    return loan_linked_to_mobile


@login_required(login_url="/login/")
def lendbox_bulk_upload(request):
    """
        Reference https://www.pythoncircle.com/post/30/how-to-upload-and-process-the-csv-file-in-django/
    :param request:
    :return:
    """
    data = {}
    bank_upd_data = {}
    response1 = HttpResponse(content_type='text/csv')
    response1['Content-Disposition'] = 'attachment; filename="bulk_lendbox_upload_response.csv"'
    writer = csv.writer(response1)
    writer.writerow(
        ['Loan Application ID', 'Mobile Number', "registration_done", "loan_created", "documents_uploading",
         "signature_declaration", "Message"])
    if "GET" == request.method:
        return render(request, "lendbox/lendbox_bulk_upload_csv.html", data)
    # if not GET, then proceed
    try:
        print('inside post function==========================')
        fields_verified = True
        message = ''
        csv_file = request.FILES["csv_file"]
        print('1111111111111111', csv_file)
        if not csv_file.name.endswith('.csv'):
            print('inside if===============')
            message = 'File is not CSV type'
            return render(request, "lendbox/lendbox_bulk_upload_csv.html", {"msg": message})
        file_data = csv_file.read().decode("utf-8")
        lines = file_data.split("\n")
        product_data_dict = {}
        user_id = request.user.id
        # loop over the lines and save them in db. If error , store as string and then display
        if lines:
            cnt = 0
            for line in lines:
                registration_done = "False"
                loan_created = "False"
                documents_uploading = "False"
                signature_declaration = "False"
                fields_verified = True
                if line:
                    line_1 = re.sub(',(?=[^"]*"[^"]*(?:"[^"]*"[^"]*)*$)', "", line)
                    print("line_1", line_1)
                    line_1.replace("\r", "")

                    fields = line_1.split(",")
                    if fields:
                        print('fields-----------------', len(fields), fields)
                        data_dict = {}
                        if cnt == 0:
                            if len(fields) != 2:
                                writer.writerow(
                                    ["", "", "Incorrect columns"])
                            else:
                                pass
                        cnt += 1
                        if fields and fields[0] != 'Loan Application Id':
                            print('here===========================')
                            loan_id = fields[0]
                            print("loan id 111111111111111111111111")
                            mobile_no = fields[1]
                            validate = False
                            if None not in (loan_id, mobile_no):
                                validate = validate_bulk_application_data(loan_id, mobile_no)
                                if not validate:
                                    loan_id = str(loan_id)[:-1]
                                    print("slicing of loan id", loan_id)
                                    loan_obj = LoanApplication.objects.filter(loan_application_id__startswith=loan_id,
                                                                              user__username=mobile_no)
                                    if loan_obj:
                                        validate = True
                                        loan_id = loan_obj[0].loan_application_id
                                        print("original loan id", loan_id)

                                if validate:
                                    print('invalidate================')
                                    lendbox_loan = generate_lendox_application_main(loan_id, mobile_no)
                                    # lendbox_loan = False
                                    if lendbox_loan:
                                        message = "success"
                                        registration_done = lendbox_loan["registration_done"]
                                        loan_created = lendbox_loan["loan_created"]
                                        documents_uploading = lendbox_loan["documents_uploading"]
                                        signature_declaration = lendbox_loan["signature_declaration"]
                                        writer.writerow(
                                            [loan_id, mobile_no, registration_done, loan_created, documents_uploading,
                                             signature_declaration, message])
                                    else:
                                        message = "Something went wrong"
                                        writer.writerow(
                                            [loan_id, mobile_no, registration_done, loan_created, documents_uploading,
                                             signature_declaration, message])

                                else:
                                    writer.writerow(
                                        [loan_id, mobile_no, "Data is not valid"])
                            else:
                                writer.writerow(
                                    [loan_id, mobile_no, "data can not be empty"])
        else:
            pass

    except Exception as e:
        logging.getLogger("error_logger").error("Unable to upload file. " + repr(e))
        print('-----------in exception lendbox_bulk_upload----------')
        print(e.args)
        import sys
        import os
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        messages.error(request, "Unable to upload file. " + repr(e))
        messages.error(request, 'File is not CSV type')
        pass
    return response1


'''
def get_required_registration_details(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="lendbox_get_required_registration_details.csv"'
    writer = csv.writer(response)
    writer.writerow(
        ['Username', 'First Name', 'Last Name', 'aadhar', 'pan', 'email', 'loan_present', "loan application id",
         'No of Loans'])
    l = [''9667858263]
    total_loans = 0
    loan_application_id = ""
    loan_present = "No"
    for i in l:
        d = get_lb_registration_api_payload(i)
        lendbox_loan = LendboxApplication.objects.filter(loan_id__user__username=i, api_status="success").order_by(
            '-id')
        if lendbox_loan:
            loan_present = "Yes"
        total_loans = len(lendbox_loan)
        if lendbox_loan:
            loan_application_id = lendbox_loan[0].loan_id.loan_application_id
        if d:
            writer.writerow(
                [i, d['firstName'], d['lastName'], d['aadhar'], d['pan'], d['email'], loan_present, loan_application_id,
                 total_loans])
    return response
'''


def get_required_registration_details(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="lendbox_get_required_registration_details.csv"'
    writer = csv.writer(response)
    writer.writerow(
        ['Username', 'First Name', 'Last Name', 'fathers name', 'aadhar', 'pan', 'email', 'loan_present',
         "loan application id",
         'No of Loans', 'loan application id', 'loan amount'])
    l = []
    l = ["8105710316", "8385885564", "7004908425", "8523044085", "7708988443", "6309942434", "9748529915", "8584895555",
         "9492013626", "8787462559", "8779544250", "8504925084", "7980070789", "9345481387", "7977473551", "9723718796",
         "8600314200", "9001068873", "9623450243", "7050210223", "9849546300", "9172108332", "7012760489", "9524042804",
         "9989666490", "6364247571", "7002200482", "9380188089", "8888559913", "9173747403", "7013587374", "9968913793",
         "9849648284", "9745377740", "7007732936", "8688588281", "9860652845", "7207810894", "7204760372", "7410590521",
         "9975439528", "9619993577", "8962345130", "8003412346", "7359421738", "9002474063", "7093600858", "9586896508",
         "9703450472", "9079897604", "8983372983", "9999268376", "8376905824", "9174056977", "7909044271", "9510858515",
         "9601708707", "9879643955", "9074770291", "9352648215", "9089500897", "8250898185", "9951166377", "8122211066",
         "9560099764", "8374951112", "8116205577", "7348995264", "7478293254", "9177118892", "8105523489", "8809781486",
         "9024373800", "9885665350", "8221065998", "9894055496", "8892196409", "9538914511", "9891015666", "8801462875",
         "9158473417", "9664796719", "8736960845", "6307900458", "9172286900", "8433661902", "8359964838", "9038809000",
         "9943780956", "8470987978", "7520279350", "7030499882", "7083907476", "6201493988", "9176104472", "9763944136",
         "9912225268", "9665775480", "9108782007", "9646330347", "8754317092", "8839115421", "9603750269", "8553880994",
         "8853298277", "8499939440", "9316731763", "9981161409", "9112461901", "7009218672", "6361511317", "8779139745",
         "8262951436", "6371941934", "9840842530", "9696220671", "9841820878", "9703378123", "8814039373", "9567359523",
         "9786007005", "9686677891", "9120515767", "7428196964", "8766342308", "9513338753", "9850143112", "8089890282",
         "8110915970", "9099545567", "8056060612", "9492089362", "8970973406", "9908600769", "8904750991", "7981597347",
         "9500642083", "7731044149", "7013750894", "7265077677", "8722284922", "8960182026", "6203256022", "8853298277",
         "8499939440", "7042422493", "9970582161", "9316731763", "9981161409", "9112461901", "7009218672", "6361511317",
         "9011695903", "8779139745", "9060826514", "8489525708", "8262951436", "6371941934", "9426820202", "7668373221",
         "7500437474", "9840842530", "9844590286", "7838926762", "9696220671", "9841820878", "7899773060", "9915275794",
         "8529807628", "9703378123", "8814039373", "9567359523", "9786007005", "9700969149", "7502762250", "9686677891",
         "9172160951", "9120515767", "7428196964", "7093124396", "8766342308", "9513338753", "9850143112", "8142779966",
         "7011190655", "9233110136", "8089890282", "9701123073", "8110915970", "9330454966", "7694838807", "8454024938",
         "9099545567", "8700979528", "8056060612", "9372429312", "9492089362", "8970973406", "9908600769", "8904750991",
         "9885712303", "7981597347", "9987201430", "9986430673", "9553138831", "9567089083", "9500642083", "7731044149",
         "7013750894", "8053778778", "7265077677", "8500425262", "8898805700", "8722284922", "8802388671", "8960182026",
         "9437167361", "8320288756", "6203256022"]
    total_loans = 0
    loan_amount = ''
    loan_application_id = ""
    mudra_loan_id = ""
    loan_present = "No"
    for i in l:
        total_loans = 0
        loan_amount = ''
        loan_application_id = ""
        mudra_loan_id = ""
        loan_present = "No"
        d = get_lb_registration_api_payload(i)
        lendbox_loan = LendboxApplication.objects.filter(loan_id__user__username=i, api_status="success").order_by(
            '-id')
        if lendbox_loan:
            loan_present = "Yes"
        total_loans = len(lendbox_loan)
        if lendbox_loan:
            loan_application_id = lendbox_loan[0].loan_id.loan_application_id
            loan_amount = lendbox_loan[0].loan_id.loan_scheme_id
        # get_loan = LoanApplication.objects.filter(user__username=i, status__in=['SUBMITTED', "PROCESS", "PRE_APPROVED","APPROVED"])
        get_loan = LoanApplication.objects.filter(user__username=i)
        if get_loan:
            mudra_loan_id = get_loan[0].loan_application_id

        if d:
            writer.writerow(
                [i, d['firstName'], d['lastName'], d['fathersName'], d['aadhar'], d['pan'], d['email'], loan_present,
                 loan_application_id,
                 total_loans, mudra_loan_id, loan_amount])
    return response

# loan_id = "1912111045133784"
# mobile = "7875583679"
# lb_user_id = 190450
# lb_user_id = 190449

# a = generate_lendox_application_main(loan_id, mobile)
# print("=====generate_lendox_application_main=", a)

# upload_documents = lendbox_upload_document_api(loan_id, lb_user_id)
# print("upload_documents---end", upload_documents)

# a = lendbox_e_sign_declaration_api(lb_user_id)
# print("a---end", a)
