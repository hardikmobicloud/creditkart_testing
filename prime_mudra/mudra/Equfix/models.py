from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from User.models import Personal


class EquiFaxMaster(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    personal = models.ForeignKey(Personal, on_delete=models.CASCADE, blank=True, null=True)

    first_name = models.CharField(max_length=255, blank=True, null=True)
    middle_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)

    score_type = models.CharField(max_length=255, blank=True, null=True)
    score_version = models.CharField(max_length=255, blank=True, null=True)

    status = models.CharField(max_length=255, blank=True, null=True)


class EquiFaxInquiryResponseHeader(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)

    customer_code = models.CharField(max_length=255, blank=True, null=True)
    customer_ref_field = models.CharField(max_length=255, blank=True, null=True)
    report_order_number = models.CharField(max_length=255, blank=True, null=True)
    trans_id = models.CharField(max_length=255, blank=True, null=True)
    product_code = models.CharField(max_length=255, blank=True, null=True)
    success_status_code = models.CharField(max_length=255, blank=True, null=True)
    date_requested = models.DateField(blank=True, null=True)
    time_requested = models.TimeField(blank=True, null=True)
    hit_code = models.CharField(max_length=255, blank=True, null=True)
    customer_name = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.customer_code


class EquiFaxInquiryResponseInfo(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)

    inquiry_purpose = models.CharField(max_length=255, blank=True, null=True)
    inquiry_amount = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)

    first_name = models.CharField(max_length=255, blank=True, null=True)
    middle_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)

    date_of_birth = models.DateField(blank=True, null=True)
    gender = models.CharField(choices=settings.GENDER_CHOICES, max_length=10, blank=True, null=True)

    def __str__(self):
        return self.inquiry_purpose


class EquiFaxInquiryAddress(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)
    equifax_response_info = models.ForeignKey(EquiFaxInquiryResponseInfo, on_delete=models.CASCADE, blank=True,
                                              null=True)

    sequence = models.CharField(max_length=100, blank=True, null=True)
    address_type = models.CharField(max_length=100, blank=True, null=True)
    address_line = models.TextField(blank=True, null=True)
    address_state = models.CharField(max_length=100, blank=True, null=True)
    address_postal_code = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.address_type


class EquiFaxInquiryPhones(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)
    equifax_response_info = models.ForeignKey(EquiFaxInquiryResponseInfo, on_delete=models.CASCADE, blank=True,
                                              null=True)

    sequence = models.CharField(max_length=100, blank=True, null=True)
    phone_type = models.CharField(max_length=100, blank=True, null=True)
    phone_number = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.phone_type


class EquiFaxIDDetails(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)
    equifax_response_info = models.ForeignKey(EquiFaxInquiryResponseInfo, on_delete=models.CASCADE, blank=True,
                                              null=True)

    sequence = models.CharField(max_length=100, blank=True, null=True)
    id_type = models.CharField(max_length=100, blank=True, null=True)
    id_value = models.CharField(max_length=100, blank=True, null=True)
    source = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.id_type


class EquiFaxScore(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)

    score_type = models.CharField(max_length=100, blank=True, null=True)
    score_version = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.score_type


class EquiFaxPersonalInfo(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)

    full_name = models.CharField(max_length=255, blank=True, null=True)
    first_name = models.CharField(max_length=255, blank=True, null=True)
    middle_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)

    date_of_birth = models.DateField(blank=True, null=True)
    gender = models.CharField(choices=settings.GENDER_CHOICES, max_length=10, blank=True, null=True)
    age = models.IntegerField(blank=True, null=True)

    place_of_birth = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.full_name


class EquiFaxIdentiyInfo(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)

    pan_sequence = models.CharField(max_length=100, blank=True, null=True)
    pan_reported_date = models.DateField(blank=True, null=True)
    pan_number = models.CharField(max_length=100, blank=True, null=True)

    national_id_card_sequence = models.CharField(max_length=100, blank=True, null=True)
    national_id_card_reported_date = models.DateField(blank=True, null=True)
    national_id_card_number = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.pan_number


class EquiFaxAddressInfo(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)

    sequence = models.CharField(max_length=100, blank=True, null=True)
    reported_date = models.DateField(blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    state = models.CharField(max_length=100, blank=True, null=True)
    postal_code = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.sequence


class EquiFaxPhoneInfo(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)

    type_code = models.CharField(max_length=100, blank=True, null=True)
    sequence = models.CharField(max_length=100, blank=True, null=True)
    reported_date = models.DateField(blank=True, null=True)
    number = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.type_code


class EquiFaxEmailAddressInfo(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)

    sequence = models.CharField(max_length=100, blank=True, null=True)
    reported_date = models.DateField(blank=True, null=True)
    email_address = models.EmailField(blank=True, null=True)

    def __str__(self):
        return self.email_address


class EquiFaxRetailAccountDetails(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)

    sequence = models.CharField(max_length=100, blank=True, null=True)
    account_number = models.CharField(max_length=255, blank=True, null=True)
    institution = models.CharField(max_length=255, blank=True, null=True)
    account_type = models.CharField(max_length=255, blank=True, null=True)
    ownership_type = models.CharField(max_length=255, blank=True, null=True)

    balance = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    past_due_amount = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)

    open = models.BooleanField(default=False)
    sanction_amount = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)

    date_reported = models.DateField(blank=True, null=True)
    date_opened = models.DateField(blank=True, null=True)

    account_status = models.CharField(max_length=255, blank=True, null=True)
    source = models.CharField(max_length=255, blank=True, null=True)

    history = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.sequence


class EquiFaxRetailAccountSummary(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)

    num_of_account = models.CharField(max_length=255, blank=True, null=True)
    num_of_active_account = models.CharField(max_length=255, blank=True, null=True)
    num_of_write_offs = models.CharField(max_length=255, blank=True, null=True)

    total_past_due = models.CharField(max_length=255, blank=True, null=True)
    most_server_status_last_24_months = models.CharField(max_length=255, blank=True, null=True)

    single_highest_credit = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    single_highest_sanction_amount = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    total_high_credit = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    avg_open_balance = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    single_highest_balance = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)

    num_of_past_due_accounts = models.IntegerField(blank=True, null=True)
    num_of_zero_balance_accounts = models.IntegerField(blank=True, null=True)

    recent_account = models.CharField(max_length=255, blank=True, null=True)
    oldest_account = models.CharField(max_length=255, blank=True, null=True)

    total_balance_amt = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    total_sanction_amt = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    total_credit_limit = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    total_monthly_payment_amt = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)

    def __str__(self):
        return self.num_of_account


class EquiFaxScoreDetails(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)

    score_type = models.CharField(max_length=255, blank=True, null=True)
    score_version = models.CharField(max_length=255, blank=True, null=True)
    score_name = models.CharField(max_length=255, blank=True, null=True)
    score_value = models.IntegerField(db_index=True, blank=True, null=True)
    scoring_element = models.TextField(blank=True, null=True)

    # def __str__(self):
    #
    #     return "{}".format(self.id)


class EquiFaxEnquiries(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)

    sequence = models.CharField(max_length=255, blank=True, null=True)
    institution = models.CharField(max_length=255, blank=True, null=True)

    date_reported = models.DateField(blank=True, null=True)
    date_time = models.TimeField(blank=True, null=True)

    request_purpose = models.CharField(max_length=255, blank=True, null=True)
    amount = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)

    def __str__(self):
        return self.sequence


class EquiFaxEnquirySummary(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)

    purpose = models.CharField(max_length=255, blank=True, null=True)

    total = models.IntegerField(blank=True, null=True)
    past_30_days = models.IntegerField(blank=True, null=True)
    past_12_months = models.IntegerField(blank=True, null=True)
    past_24_months = models.IntegerField(blank=True, null=True)

    recent = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.total


class EquiFaxOtherKeyInd(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)

    age_of_oldest_trade = models.IntegerField(blank=True, null=True)
    num_of_open_trade = models.IntegerField(blank=True, null=True)

    all_lines_ever_writter = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    all_lines_ever_writter_9_months = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    all_lines_ever_writter_16_months = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)

    def __str__(self):
        return self.num_of_open_trade


class EquiFaxRecentActivites(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)

    accounts_deliquent = models.IntegerField(blank=True, null=True)
    accounts_opened = models.IntegerField(blank=True, null=True)
    total_inquiries = models.IntegerField(blank=True, null=True)
    accounts_updated = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.accounts_deliquent


class EquiFaxConsumerDisputes(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)

    dispute_created_date = models.DateField(blank=True, null=True)
    dispute_status = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.dispute_status


class ConsumerDisputesDetails(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    dispute_details = models.ForeignKey(EquiFaxConsumerDisputes, on_delete=models.CASCADE)

    dispute_comments = models.CharField(max_length=255, blank=True, null=True)
    dispute_status = models.CharField(max_length=255, blank=True, null=True)
    dispute_type = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.dispute_comments


class EquiFaxDimensionalVariables(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    equifax = models.ForeignKey(EquiFaxMaster, on_delete=models.CASCADE)

    tda_mesmi_cc_psdamt_24 = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    tda_mesme_ins_psdamt_24 = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    tda_metsu_cc_psdamt_3 = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    tda_sum_pf_psdamt_3 = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)

    def __str__(self):
        return self.tda_mesme_ins_psdamt_24
