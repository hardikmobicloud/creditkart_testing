from django.apps.registry import apps

from .models import *

Address = apps.get_model('User', 'Address')
Personal = apps.get_model('User', 'Personal')


def create_equi_master(response_header, request_info, score, user_id):
    first_name = None
    last_name = None
    middle_name = None
    score_type = None
    score_version = None

    try:
        get_personal_details = Personal.objects.filter(user_id=user_id).order_by('-id').first()

        if response_header:
            if 'FirstName' in request_info:
                first_name = request_info['FirstName']
            if 'MiddleName' in request_info:
                middle_name = request_info['MiddleName']
            if 'LastName' in request_info:
                last_name = request_info['LastName']
        if score:
            if 'Type' in score[0]:
                score_type = score[0]['Type']
            if 'Version' in score[0]:
                score_version = score[0]['Version']
        data = {
            'first_name': first_name,
            'middle_name': middle_name,
            'last_name': last_name,
            'score_type': score_type,
            'score_version': score_version,
            'status': '1',
            'user_id': user_id,
            'personal': get_personal_details

        }
        equi_instance = EquiFaxMaster.objects.create(**data)
        return equi_instance
    except:
        return False


def create_equi_inq_response_header(instance, inq_header):
    try:

        customer_code = None
        customer_ref_field = None
        report_order_number = None
        trans_id = None
        product_code = None
        success_status_code = None
        date_requested = None
        time_requested = None
        hit_code = None
        customer_name = None

        if 'ClientID' in inq_header:
            customer_code = inq_header['ClientID']

        if 'CustRefField' in inq_header:
            customer_ref_field = inq_header['CustRefField']

        if 'ReportOrderNO' in inq_header:
            report_order_number = inq_header['ReportOrderNO']

        if 'ProductCode' in inq_header['ProductCode'][0]:
            product_code = inq_header['ProductCode'][0]

        if 'SuccessCode' in inq_header['SuccessCode']:
            success_status_code = inq_header['SuccessCode']

        if 'Date' in inq_header['Date']:
            date_requested = inq_header['Date']

        if 'Time' in inq_header['Time']:
            time_requested = inq_header['Time']

        data = {
            'equifax': instance,
            'customer_code': customer_code,
            'customer_ref_field': customer_ref_field,
            'report_order_number': report_order_number,
            'trans_id': trans_id,
            'product_code': product_code,
            'success_status_code': success_status_code,
            'date_requested': date_requested,
            'time_requested': time_requested,
            'hit_code': hit_code,
            'customer_name': customer_name,
        }

        EquiFaxInquiryResponseHeader.objects.create(
            **data
        )

    except Exception as e:
        pass

    return True


def create_inquiry_response_info(instance, request_info):
    inquiry_purpose = None
    inquiry_amount = None
    first_name = None
    middle_name = None
    last_name = None
    date_of_birth = None
    gender = None

    try:

        if 'InquiryPurpose' in request_info:
            inquiry_purpose = request_info['InquiryPurpose']

        if 'TransactionAmount' in request_info:
            inquiry_amount = request_info['TransactionAmount']

        if 'FirstName' in request_info:
            first_name = request_info['FirstName']

        if 'MiddleName' in request_info:
            middle_name = request_info['MiddleName']

        if 'LastName' in request_info:
            last_name = request_info['LastName']

        if 'DOB' in request_info:
            date_of_birth = request_info['DOB']

        if 'Gender' in request_info:
            gender = request_info['Gender']

        data = {
            'equifax': instance,
            'inquiry_purpose': inquiry_purpose,
            'inquiry_amount': inquiry_amount,
            'first_name': first_name,
            'middle_name': middle_name,
            'last_name': last_name,
            'date_of_birth': date_of_birth,
            'gender': gender,
        }

        equifax_response_info = EquiFaxInquiryResponseInfo.objects.create(
            **data
        )

        return equifax_response_info

    except Exception as e:

        return False


def create_inquiry_address(instance, inquiry_instance, address):
    sequence = None
    address_type = None
    address_line = None
    address_state = None
    address_postal_code = None

    try:

        if len(address['InquiryAddresses']) >= 1:

            for i in range(0, len(address['InquiryAddresses'])):

                if 'seq' in address['InquiryAddresses'][i]:
                    sequence = address['InquiryAddresses'][i]['seq']

                if 'AddressType' in address['InquiryAddresses'][i]:
                    address_type = address['InquiryAddresses'][i]['AddressType']

                if 'AddressType' in address['InquiryAddresses'][i]['AddressType']:
                    address_type = address['InquiryAddresses'][i]['AddressType'][0]

                if 'AddressLine1' in address['InquiryAddresses'][i]:
                    address_line = address['InquiryAddresses'][i]['AddressLine1']

                if 'State' in address['InquiryAddresses'][i]:
                    address_state = address['InquiryAddresses'][i]['State']

                if 'Postal' in address['InquiryAddresses'][i]:
                    address_postal_code = address['InquiryAddresses'][i]['Postal']

                data = {
                    'equifax': instance,
                    'equifax_response_info': inquiry_instance,
                    'sequence': sequence,
                    'address_type': address_type,
                    'address_line': address_line,
                    'address_state': address_state,
                    'address_postal_code': address_postal_code,
                }

                EquiFaxInquiryAddress.objects.create(
                    **data
                )

    except Exception as e:
        import sys
        import os
        print('-----------in exception----------')
        print(e.args)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        pass

    pass


def create_inquiry_phones(instance, inquiry_instance, phones):
    sequence = None
    phone_type = None
    phone_number = None

    try:

        if len(phones['InquiryPhones']) >= 1:

            for i in range(0, len(phones['InquiryPhones'])):

                if 'seq' in phones['InquiryPhones'][i]:
                    sequence = phones['InquiryPhones'][i]['seq']

                if 'PhoneType' in phones['InquiryPhones'][i]:
                    phone_type = phones['InquiryPhones'][i]['PhoneType'][0]

                if 'Number' in phones['InquiryPhones'][i]:
                    phone_number = phones['InquiryPhones'][i]['Number']

                data = {
                    'equifax': instance,
                    'equifax_response_info': inquiry_instance,
                    'sequence': sequence,
                    'phone_number': phone_number,
                    'phone_type': phone_type,
                }

                EquiFaxInquiryPhones.objects.create(
                    **data
                )

    except:
        pass

    return True


def create_inquiry_details(instance, inquiry_instance, details_json):
    sequence = None
    id_type = None
    id_value = None
    source = None

    try:

        if len(details_json['InquiryPhones']) >= 1:

            for i in range(0, len(details_json['IDDetails'])):

                if 'seq' in details_json['IDDetails'][i]:
                    sequence = details_json['IDDetails'][i]['seq']

                if 'IDType' in details_json['IDDetails'][i]:
                    id_type = details_json['IDDetails'][i]['IDType']

                if 'IDValue' in details_json['IDDetails'][i]:
                    id_value = details_json['IDDetails'][i]['IDValue']

                if 'Source' in details_json['IDDetails'][i]:
                    source = details_json['IDDetails'][i]['Source']

                data = {
                    'equifax': instance,
                    'equifax_response_info': inquiry_instance,
                    'source': source,
                    'id_value': id_value,
                    'id_type': id_type,
                    'sequence': sequence,
                }

                EquiFaxIDDetails.objects.create(
                    **data
                )
    except:
        pass

    return True


def create_score(instance, score):
    score_type = None
    score_version = None

    try:

        if len(score) >= 1:

            for i in range(0, len(score)):

                print(score[i]['Type'])

                if 'Type' in score[i]:
                    score_type = score[i]['Type']

                if 'Version' in score[i]:
                    score_version = score[i]['Version']

                data = {
                    'equifax': instance,
                    'score_type': score_type,
                    'score_version': score_version
                }

                EquiFaxScore.objects.create(
                    **data
                )

    except:
        pass

    return True


def create_personal_info(instance, personal_info):
    full_name = None
    first_name = None
    middle_name = None
    last_name = None
    date_of_birth = None
    gender = None
    age = None
    place_of_birth = None

    try:
        info = personal_info['IDAndContactInfo']['PersonalInfo']

        if 'FullName' in info['Name']:
            full_name = info['Name']['FullName']

        if 'FirstName' in info['Name']:
            first_name = info['Name']['FirstName']

        if 'MiddleName' in info['Name']:
            middle_name = info['Name']['MiddleName']

        if 'LastName' in info['Name']:
            last_name = info['Name']['LastName']

        if 'DateOfBirth' in info:
            date_of_birth = info['DateOfBirth']

        if 'Gender' in info:
            gender = info['Gender']

        if 'Age' in info['Age']:
            age = info['Age']['Age']

        data = {
            'equifax': instance,
            'full_name': full_name,
            'first_name': first_name,
            'middle_name': middle_name,
            'last_name': last_name,
            'date_of_birth': date_of_birth,
            'gender': gender,
            'age': age,
        }

        EquiFaxPersonalInfo.objects.create(
            **data
        )

    except Exception as e:
        print(e.args)
        pass

    return True


def create_identity_info(instance, identity_info):
    pan_sequence = None
    pan_reported_date = None
    pan_number = None
    national_id_card_sequence = None
    national_id_card_reported_date = None
    national_id_card_number = None

    try:

        info = identity_info['IDAndContactInfo']['IdentityInfo']

        print(info['PANId'][0])

        if info['PANId'][0]:

            if 'seq' in info['PANId'][0]:
                pan_sequence = info['PANId'][0]['seq']

            if 'ReportedDate' in info['PANId'][0]:
                pan_reported_date = info['PANId'][0]['ReportedDate']

            if 'IdNumber' in info['PANId'][0]:
                pan_number = info['PANId'][0]['IdNumber']

        if info['NationalIDCard'][0]:

            if 'seq' in info['NationalIDCard'][0]:
                national_id_card_sequence = info['NationalIDCard'][0]['seq']

            if 'ReportedDate' in info['NationalIDCard'][0]:
                national_id_card_reported_date = info['NationalIDCard'][0]['ReportedDate']

            if 'IdNumber' in info['NationalIDCard'][0]:
                national_id_card_number = info['NationalIDCard'][0]['IdNumber']

        data = {
            'equifax': instance,
            'pan_sequence': pan_sequence,
            'pan_reported_date': pan_reported_date,
            'pan_number': pan_number,
            'national_id_card_sequence': national_id_card_sequence,
            'national_id_card_reported_date': national_id_card_reported_date,
            'national_id_card_number': national_id_card_number,
        }

        EquiFaxIdentiyInfo.objects.create(
            **data
        )

    except Exception as e:
        print(e.args)
        pass

    return True


def create_address_info(instance, address_info):
    sequence = None
    reported_date = None
    address = None
    state = None
    postal_code = None

    try:
        info = address_info['IDAndContactInfo']['AddressInfo']

        for i in range(0, len(info)):

            if 'Seq' in info[i]:
                sequence = info[i]['Seq']

            if 'ReportedDate' in info[i]:
                reported_date = info[i]['ReportedDate']

            if 'Address' in info[i]:
                address = info[i]['Address']

            if 'State' in info[i]:
                state = info[i]['State']

            if 'Postal' in info[i]:
                postal_code = info[i]['Postal']

            data = {
                'equifax': instance,
                'sequence': sequence,
                'reported_date': reported_date,
                'address': address,
                'state': state,
                'postal_code': postal_code
            }

            EquiFaxAddressInfo.objects.create(
                **data
            )

    except Exception as e:
        pass

    return True


def create_phone_info(instance, phone_info):
    type_code = None
    sequence = None
    reported_date = None
    number = None

    try:

        info = phone_info['IDAndContactInfo']['PhoneInfo']

        for i in range(0, len(info)):

            if 'seq' in info[i]:
                sequence = info[i]['seq']

            if 'typeCode' in info[i]:
                type_code = info[i]['typeCode']

            if 'ReportedDate' in info[i]:
                reported_date = info[i]['ReportedDate']

            if 'Number' in info[i]:
                number = info[i]['Number']

            data = {
                'equifax': instance,
                'number': number,
                'sequence': sequence,
                'type_code': type_code,
                'reported_date': reported_date
            }

            EquiFaxPhoneInfo.objects.create(
                **data
            )

    except Exception as e:
        print(e.args)
        pass

    return True


def create_email_id_info(instance, email_info):
    sequence = None
    reported_date = None
    email_address = None

    try:

        info = email_info['IDAndContactInfo']['EmailAddressInfo']

        for i in range(0, len(info)):

            if 'seq' in info[i]:
                sequence = info[i]['seq']

            if 'ReportedDate' in info[i]:
                reported_date = info[i]['ReportedDate']

            if 'EmailAddress' in info[i]:
                email_address = info[i]['EmailAddress']

            data = {
                'equifax': instance,
                'sequence': sequence,
                'reported_date': reported_date,
                'email_address': email_address,
            }

            EquiFaxEmailAddressInfo.objects.create(**data)

        return True

    except Exception as e:
        print(e.args)
        pass

        return False


def create_retail_account_details(instance, res_info):
    sequence = None
    account_number = None
    institution = None
    account_type = None
    ownership_type = None
    balance = None
    past_due_amount = None
    open = None
    sanction_amount = None
    date_reported = None
    date_opened = None
    account_status = None
    source = None
    history = None

    try:

        info = res_info['RetailAccountDetails']

        for i in range(0, len(info)):

            if 'seq' in info[i]:
                sequence = info[i]['seq']

            if 'AccountNumber' in info[i]:
                account_number = info[i]['AccountNumber']

            if 'AccountNumber' in info[i]:
                account_number = info[i]['AccountNumber']

            if 'Institution' in info[i]:
                institution = info[i]['Institution']

            if 'AccountType' in info[i]:
                account_type = info[i]['AccountType']

            if 'OwnershipType' in info[i]:
                ownership_type = info[i]['OwnershipType']

            if 'Balance' in info[i]:
                balance = info[i]['Balance']

            if 'PastDueAmount' in info[i]:
                past_due_amount = info[i]['PastDueAmount']

            if 'SanctionAmount' in info[i]:
                sanction_amount = info[i]['SanctionAmount']

            if 'DateReported' in info[i]:
                date_reported = info[i]['DateReported']

            if 'DateOpened' in info[i]:
                date_opened = info[i]['DateOpened']

            if 'AccountStatus' in info[i]:
                account_status = info[i]['AccountStatus']

            data = {
                'equifax': instance,
                'sequence': sequence,
                'account_number': account_number,
                'institution': institution,
                'account_type': account_type,
                'ownership_type': ownership_type,
                'balance': balance,
                'past_due_amount': past_due_amount,
                'sanction_amount': sanction_amount,
                'date_reported': date_reported,
                'date_opened': date_opened,
                'account_status': account_status,
            }

            EquiFaxRetailAccountDetails.objects.create(
                **data
            )

    except Exception as e:
        print(e.args)

    return True


def create_score_details(instance, score_info):
    score_type = None
    score_version = None
    score_name = None
    score_value = None
    scoring_element = None

    try:

        info = score_info['ScoreDetails']

        for i in range(0, len(info)):

            if 'Type' in info[i]:
                score_type = info[i]['Type']

            if 'Version' in info[i]:
                score_version = info[i]['Version']

            if 'Name' in info[i]:
                score_name = info[i]['Name']

            if 'Value' in info[i]:
                score_value = info[i]['Value']

            if 'ScoringElements' in info[i]:
                scoring_element = info[i]['ScoringElements']

            data = {
                'equifax': instance,
                'score_type': score_type,
                'score_version': score_version,
                'score_value': score_value,
                'scoring_element': scoring_element,
            }

            EquiFaxScoreDetails.objects.create(**data)

    except Exception as e:
        print(e.args)

    return True


def create_enqires(instance, enqires_info):
    sequence = None
    institution = None
    date_reported = None
    date_time = None
    request_purpose = None
    amount = None

    try:

        info = enqires_info['Enquiries']

        for i in range(0, len(info)):

            if 'seq' in info[i]:
                sequence = info[i]['seq']

            if 'Institution' in info[i]:
                institution = info[i]['Institution']

            if 'Date' in info[i]:
                date_reported = info[i]['Date']

            if 'Time' in info[i]:
                date_time = info[i]['Time']

            if 'RequestPurpose' in info[i]:
                request_purpose = info[i]['RequestPurpose']

            if 'Amount' in info[i]:
                amount = info[i]['Amount']

            data = {
                'equifax': instance,
                'sequence': sequence,
                'institution': institution,
                'date_time': date_time,
                'date_reported': date_reported,
                'request_purpose': request_purpose,
                'amount': amount,
            }

            EquiFaxEnquiries.objects.create(**data)

    except Exception as e:
        print(e.args)

    return True


def create_enqires_summary(instance, enqires_sum_info):
    purpose = None
    total = None
    past_30_days = None
    past_12_months = None
    past_24_months = None
    recent = None

    try:

        info = enqires_sum_info['EnquirySummary']

        if 'Purpose' in info:
            purpose = info['Purpose']

        if 'Total' in info:
            total = info['Total']

        if 'Past30Days' in info:
            past_30_days = info['Past30Days']

        if 'Past12Months' in info:
            past_12_months = info['Past12Months']

        if 'Past24Months' in info:
            past_24_months = info['Past24Months']

        if 'Recent' in info:
            recent = info['Recent']

        data = {
            'equifax': instance,
            'recent': recent,
            'past_24_months': past_24_months,
            'past_12_months': past_12_months,
            'past_30_days': past_30_days,
            'total': total,
            'purpose': purpose,
        }

        EquiFaxEnquirySummary.objects.create(**data)

    except Exception as e:
        pass
    return True


def create_other_key_ind(instance, other_info):
    age_of_oldest_trade = None
    num_of_open_trade = None
    all_lines_ever_writter = None
    all_lines_ever_writter_9_months = None
    all_lines_ever_writter_16_months = None

    try:

        info = other_info['OtherKeyInd']

        if 'AgeOfOldestTrade' in info:
            age_of_oldest_trade = info['AgeOfOldestTrade']

        if 'NumberOfOpenTrades' in info:
            num_of_open_trade = info['NumberOfOpenTrades']

        if 'AllLinesEVERWritten' in info:
            all_lines_ever_writter = info['AllLinesEVERWritten']

        if 'AllLinesEVERWrittenIn9Months' in info:
            all_lines_ever_writter_9_months = info['AllLinesEVERWrittenIn9Months']

        if 'AllLinesEVERWrittenIn6Months' in info:
            all_lines_ever_writter_16_months = info['AllLinesEVERWrittenIn6Months']

        data = {
            'equifax': instance,
            'age_of_oldest_trade': age_of_oldest_trade,
            'num_of_open_trade': num_of_open_trade,
            'all_lines_ever_writter_9_months': all_lines_ever_writter_9_months,
            'all_lines_ever_writter_16_months': all_lines_ever_writter_16_months,
        }

        EquiFaxOtherKeyInd.objects.create(**data)

    except Exception as e:
        pass

    return True


def create_recent_act(instance, recent_info):
    accounts_deliquent = None
    accounts_opened = None
    total_inquiries = None
    accounts_updated = None

    try:

        info = recent_info['RecentActivities']

        if 'AccountsDeliquent' in info:
            accounts_deliquent = info['AccountsDeliquent']

        if 'AccountsOpened' in info:
            accounts_opened = info['AccountsOpened']

        if 'TotalInquiries' in info:
            total_inquiries = info['TotalInquiries']

        if 'AccountsUpdated' in info:
            accounts_updated = info['AccountsUpdated']

        data = {
            'equifax': instance,
            'accounts_updated': accounts_updated,
            'total_inquiries': total_inquiries,
            'accounts_opened': accounts_opened,
            'accounts_deliquent': accounts_deliquent,
        }

        EquiFaxRecentActivites.objects.create(**data)

    except Exception as e:
        pass

    return True
