import datetime

import requests
from django.apps.registry import apps
from django.http import JsonResponse
from django.shortcuts import render, HttpResponse
from django.views import generic
from django_pandas.io import read_frame

from .utils import *

Address = apps.get_model('User', 'Address')
Personal = apps.get_model('User', 'Personal')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')

# Uat Url
api_url = 'https://eportuat.equifax.co.in/cir360Report/cir360Report/'
# Prod Url
# -not working
# prod_url = 'https://ists.equifax.co.in/creditreportws/CreditReportWSInquiry/v1.0?wsdl'
# working
prod_url2 = 'https://ists.equifax.co.in/cir360service/cir360report'


def equfix_API_uat(request):
    '''
        UAT
    :param request:
    :return:
    '''

    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
    }

    uat_request_header = {
        "CustomerId": "21", "UserId": "UAT_MUDRAA",
        "Password": "abcd*12345", "MemberNumber": "999AA00007",
        "SecurityCode": "54J", "ProductCode": ["IDCR"],
        "CustRefField": "REF16272"
    }

    api_body = {
        "RequestHeader":
            uat_request_header,
        "RequestBody":
            {
                "InquiryPurpose": "05", "TransactionAmount": "100",
                "FirstName": "SHARAD", "MiddleName": "SIDDHESHWAR",
                "LastName": "NALAWAD",
                "InquiryAddresses": [
                    {
                        "seq": "1", "AddressType": ["H"],
                        "AddressLine1": "FLAT NO 303 RASIKA TOWER  MADHA RAOD KURDWADI LAUL-MH", "AddressLine2": "",
                        "Locality": "", "City": "", "State": "MH", "Postal": "413208"
                    }
                ],
                "InquiryPhones": [
                    {
                        "seq": "1", "Number": "9422025673", "PhoneType": ["M"]
                    }
                ],
                "EmailAddresses": [],
                "IDDetails": [
                    {
                        "seq": "1", "IDType": "T", "IDValue": "AGIPN1749B",
                        "Source": "Inquiry"
                    }
                ],
                "DOB": "1977-11-05", "Gender": "M"
            },
        "Score": [
            {
                "Type": "ERS", "Version": "3.0"
            }
        ]
    }

    import json
    json_body = json.dumps(api_body)

    response = requests.request('POST', api_url, data=json_body, headers=headers)
    # print(response)

    response_json = response.json()

    if response_json['InquiryResponseHeader']['SuccessCode'] == '1':

        # create master table
        eq_master = create_equi_master(response_json['InquiryResponseHeader'], response_json['InquiryRequestInfo'],
                                       response_json['Score'])

        if eq_master:
            create_equi_inq_response_header(eq_master, response_json['InquiryResponseHeader'])
            inq_res_info = create_inquiry_response_info(eq_master, response_json['InquiryRequestInfo'])
            if inq_res_info:
                # create_inquiry_address(eq_master, inq_res_info, response_json['InquiryRequestInfo'])
                create_inquiry_phones(eq_master, inq_res_info, response_json['InquiryRequestInfo'])
                create_inquiry_details(eq_master, inq_res_info, response_json['InquiryRequestInfo'])
                create_score(eq_master, response_json['Score'])
                create_personal_info(eq_master,
                                     response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_identity_info(eq_master,
                                     response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_address_info(eq_master, response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_phone_info(eq_master, response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_email_id_info(eq_master,
                                     response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_retail_account_details(eq_master,
                                              response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_score_details(eq_master,
                                     response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_enqires(eq_master, response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_enqires_summary(eq_master,
                                       response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_other_key_ind(eq_master,
                                     response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_recent_act(eq_master, response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
    # print(response.text)
    return HttpResponse(response.text)


def prod_test(request):
    prod_userid = 'STS_ANANDP'
    prod_cust_id = '5860'
    prod_member_number = '024FP00424'
    prod_security_code = 'AN5'
    prod_product = 'PCS'
    prod_password = 'abcd*1234'

    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
    }
    prod_request_header = {
        "CustomerId": prod_cust_id, "UserId": prod_userid,
        "Password": prod_password, "MemberNumber": prod_member_number,
        "SecurityCode": prod_security_code, "ProductCode": [prod_product],
        "CustRefField": "REF16272"
    }

    fname = 'Bipin'.upper()
    mname = 'chougunda'.upper()
    lname = 'patil'.upper()
    AddressLine1 = 'A/P takali near jain mandir patil galli'
    State = "MH"
    Postal = "416010"
    Number = '9860252239'
    pan = 'AUEPP3709R'
    dob = "1986-11-18"
    #
    # fname = 'Shital'.upper()
    # mname = ''
    # lname = ''
    # AddressLine1 = 'A/P takali near jain mandir patil galli'
    # State = "MH"
    # Postal = "411014"
    # Number = '7218686808'
    # pan = 'ESDPP9311J'
    # dob = "1992-07-15"

    api_body = {
        "RequestHeader":
            prod_request_header,
        "RequestBody":
            {
                "InquiryPurpose": "05", "TransactionAmount": "100",
                "FirstName": fname, "MiddleName": mname,
                "LastName": lname,
                "InquiryAddresses": [
                    {
                        "seq": "1", "AddressType": ["H"],
                        "AddressLine1": AddressLine1, "AddressLine2": "",
                        "Locality": "", "City": "", "State": State, "Postal": Postal
                    }
                ],
                "InquiryPhones": [
                    {
                        "seq": "1", "Number": Number, "PhoneType": ["M"]
                    }
                ],
                "EmailAddresses": [],
                "IDDetails": [
                    {
                        "seq": "1", "IDType": "T", "IDValue": pan,
                        "Source": "Inquiry"
                    }
                ],
                "DOB": dob, "Gender": "M"
            },
        "Score": [
            {
                "Type": "ERS", "Version": "3.0"
            }
        ]
    }

    import json
    json_body = json.dumps(api_body)
    # print("post requesdt:", json_body)
    response = requests.request('POST', prod_url2, data=json_body, headers=headers)
    # print(response)

    response_json = response.json()
    # print("DSdsdmskdnskdskdsa dsd", response_json)
    if response_json['InquiryResponseHeader']['SuccessCode'] == '1':

        # create master table
        eq_master = create_equi_master(response_json['InquiryResponseHeader'], response_json['InquiryRequestInfo'],
                                       response_json['Score'])

        if eq_master:
            create_equi_inq_response_header(eq_master, response_json['InquiryResponseHeader'])

    # print(response.text)
    return HttpResponse(response.text)


def equfix_status_API(request):
    api_url = ' https://ists.equifax.co.in/creditreportws/CreditReportWSInquiry/v1.0?wsdl'

    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
    }

    api_body = {
        "RequestHeader":
            {
                "CustomerId": "5860", "UserId": "STS_ANANDP",
                "Password": "abcd*1234", "MemberNumber": "024FP00424",
                "SecurityCode": "AN5", "ProductCode": ["PCS"],
                "CustRefField": "REF16272"
            },
        "RequestBody":
            {
                "InquiryPurpose": "05", "TransactionAmount": "100",
                "FirstName": "SHARAD", "MiddleName": "SIDDHESHWAR",
                "LastName": "NALAWAD",
                "InquiryAddresses": [
                    {
                        "seq": "1", "AddressType": ["H"],
                        "AddressLine1": "FLAT NO 303 RASIKA TOWER  MADHA RAOD KURDWADI LAUL-MH", "AddressLine2": "",
                        "Locality": "", "City": "", "State": "MH", "Postal": "413208"
                    }
                ],
                "InquiryPhones": [
                    {
                        "seq": "1", "Number": "9422025673", "PhoneType": ["M"]
                    }
                ],
                "EmailAddresses": [],
                "IDDetails": [
                    {
                        "seq": "1", "IDType": "T", "IDValue": "AGIPN1749B",
                        "Source": "Inquiry"
                    }
                ],
                "DOB": "1977-11-05", "Gender": "M"
            },
        "Score": [
            {
                "Type": "Retail", "Version": "4.0"
            }
        ]
    }

    import json
    json_body = json.dumps(api_body)
    # print('jjjjjj', json_body)
    response = requests.request('POST', prod_url2, data=json_body, headers=headers)

    response_json = response.json()
    # print('response in json ..... ', response_json)

    if response_json['InquiryResponseHeader']['SuccessCode'] == '1':
        user_id = 173926
        # create master table
        eq_master = create_equi_master(response_json['InquiryResponseHeader'], response_json['InquiryRequestInfo'],
                                       response_json['Score'], user_id)
        # print("master", eq_master)
        if eq_master:
            create_equi_inq_response_header(eq_master, response_json['InquiryResponseHeader'])

    return HttpResponse(response.text)


# def state_code_status(loan_application_id):
#     loan_obj = LoanApplication.objects.select_related('user', 'user_professional', 'user_personal',
#                                                       'current_address').get(
#         loan_application_id=loan_application_id)
#
#     state_code = ''
#
#     if loan_obj.current_address.state == "maharashtra":
#         state_code = 'MH'
#
#     if loan_obj.current_address.state == "gujarat":
#         state_code = 'GJ'
#
#     if loan_obj.current_address.state == "uttar pradesh":
#         state_code = 'UP'
#
#     if loan_obj.current_address.state == "telangana":
#         state_code = 'TG'
#
#     if loan_obj.current_address.state == "rajasthan":
#         state_code = 'AN'
#
#     if loan_obj.current_address.state == "rajasthan":
#         state_code = 'RJ'
#
#     if loan_obj.current_address.state == "kerala":
#         state_code = 'KL'
#
#     if loan_obj.current_address.state == "madhya pradesh":
#         state_code = 'MP'
#
#     if loan_obj.current_address.state == "uttarakhand":
#         state_code = 'UL'
#
#     if loan_obj.current_address.state == "haryana":
#         state_code = 'HR'
#
#     if loan_obj.current_address.state == "punjab":
#         state_code = 'PB'
#
#     if loan_obj.current_address.state == "andaman & nicobar islands":
#         state_code = 'AP'
#
#     if loan_obj.current_address.state == "jammu & kashmir":
#         state_code = 'JK'
#
#     if loan_obj.current_address.state == "odisha":
#         state_code = 'OR'
#
#     if loan_obj.current_address.state == "bihar":
#         state_code = 'BR'
#
#     if loan_obj.current_address.state == "tamil nadu":
#         state_code = 'TN'
#
#     if loan_obj.current_address.state == "karnataka":
#         state_code = 'KA'
#
#     if loan_obj.current_address.state == "west bengal":
#         state_code = 'WB'
#
#     if loan_obj.current_address.state == "assam":
#         state_code = 'AS'
#
#     if loan_obj.current_address.state == "chattisgarh":
#         state_code = 'CG'
#
#     if loan_obj.current_address.state == "himachal pradesh":
#         state_code = 'HP'
#
#     if loan_obj.current_address.state == "manipur":
#         state_code = 'MN'
#
#     if loan_obj.current_address.state == "jharkhand":
#         state_code = 'JH'
#
#     if loan_obj.current_address.state == "delhi":
#         state_code = 'DL'
#
#     if loan_obj.current_address.state == "mizoram":
#         state_code = 'MZ'
#
#     if loan_obj.current_address.state == "chandigarh":
#         state_code = 'CH'
#
#     if loan_obj.current_address.state == "arunachal pradesh":
#         state_code = 'AR'
#
#     if loan_obj.current_address.state == "dadra & nagar haveli":
#         state_code = 'DN'
#
#     if loan_obj.current_address.state == "daman & diu":
#         state_code = 'DD'
#
#     if loan_obj.current_address.state == "tripura":
#         state_code = 'TR'
#
#     if loan_obj.current_address.state == "meghalaya":
#         state_code = 'ML'
#
#     if loan_obj.current_address.state == "sikkim":
#         state_code = 'SK'
#
#     if loan_obj.current_address.state == "pondicherry":
#         state_code = 'PY'
#
#     if loan_obj.current_address.state == "nagaland":
#         state_code = 'NL'
#
#     if loan_obj.current_address.state == "lakshadweep":
#         state_code = 'LD'
#
#     if loan_obj.current_address.state == "goa":
#         state_code = 'GA'
#
#
#
#     return state_code


def get_state_code(state):
    state_code = ''

    if state == "maharashtra":
        state_code = 'MH'

    if state == "gujarat":
        state_code = 'GJ'

    if state == "uttar pradesh":
        state_code = 'UP'

    if state == "telangana":
        state_code = 'TG'

    if state == "rajasthan":
        state_code = 'AN'

    if state == "rajasthan":
        state_code = 'RJ'

    if state == "kerala":
        state_code = 'KL'

    if state == "madhya pradesh":
        state_code = 'MP'

    if state == "uttarakhand":
        state_code = 'UL'

    if state == "haryana":
        state_code = 'HR'

    if state == "punjab":
        state_code = 'PB'

    if state == "andaman & nicobar islands":
        state_code = 'AP'

    if state == "jammu & kashmir":
        state_code = 'JK'

    if state == "odisha":
        state_code = 'OR'

    if state == "bihar":
        state_code = 'BR'

    if state == "tamil nadu":
        state_code = 'TN'

    if state == "karnataka":
        state_code = 'KA'

    if state == "west bengal":
        state_code = 'WB'

    if state == "assam":
        state_code = 'AS'

    if state == "chattisgarh":
        state_code = 'CG'

    if state == "himachal pradesh":
        state_code = 'HP'

    if state == "manipur":
        state_code = 'MN'

    if state == "jharkhand":
        state_code = 'JH'

    if state == "delhi":
        state_code = 'DL'

    if state == "mizoram":
        state_code = 'MZ'

    if state == "chandigarh":
        state_code = 'CH'

    if state == "arunachal pradesh":
        state_code = 'AR'

    if state == "dadra & nagar haveli":
        state_code = 'DN'

    if state == "daman & diu":
        state_code = 'DD'

    if state == "tripura":
        state_code = 'TR'

    if state == "meghalaya":
        state_code = 'ML'

    if state == "sikkim":
        state_code = 'SK'

    if state == "pondicherry":
        state_code = 'PY'

    if state == "nagaland":
        state_code = 'NL'

    if state == "lakshadweep":
        state_code = 'LD'

    if state == "goa":
        state_code = 'GA'
    return state_code


def state_code_status(loan_application_id):
    loan_obj = LoanApplication.objects.select_related('user', 'user_professional', 'user_personal',
                                                      'current_address').get(
        loan_application_id=loan_application_id)

    state = get_state_code(loan_obj.current_address.state)
    return state


def equfix_status_check(request):
    loan_application_id = request.GET.get('loan_application_id', None)
    loan_obj = LoanApplication.objects.select_related('user', 'user_professional', 'user_personal',
                                                      'current_address').filter(
        loan_application_id=loan_application_id)

    equfix_score = ''
    if loan_obj:
        gender = 'M'
        if loan_obj[0].user_personal.gender == "Male":
            gender = 'M'
        if loan_obj[0].user_personal.gender == "Female":
            gender = 'F'
        state_code = state_code_status(loan_application_id)
        prod_userid = 'STS_ANANDP'
        prod_cust_id = '5860'
        prod_member_number = '024FP00424'
        prod_security_code = 'AN5'
        prod_product = 'PCS'
        prod_password = 'abcd*1234'

        if loan_obj[0].current_address.address != 'None':
            address = loan_obj[0].current_address.address
        else:
            address = "FLAT NO 303 RASIKA TOWER  MADHA RAOD KURDWADI LAUL-MH"
        if str(loan_obj[0].current_address.city) != 'None':
            city = loan_obj[0].current_address.city
        else:
            city = 'Pune'

        if state_code == '':
            state_code = 'MH'

        if loan_obj[0].current_address.pin_code != 'None':
            pincode = loan_obj[0].current_address.pin_code
        else:
            pincode = "413208"

        headers = {
            'content-type': "application/json",
            'cache-control': "no-cache",
        }
        prod_request_header = {
            "CustomerId": prod_cust_id, "UserId": prod_userid,
            "Password": prod_password, "MemberNumber": prod_member_number,
            "SecurityCode": prod_security_code, "ProductCode": [prod_product],
            "CustRefField": "REF16272"
        }

        fname = loan_obj[0].user_personal.name.upper()
        mname = ''
        lname = ''
        Number = str(loan_obj[0].user.username)
        pan = loan_obj[0].user_personal.pan_number
        date_time_str = str(str(loan_obj[0].user_personal.birthdate))
        dob = datetime.datetime.strptime(date_time_str[:10], '%Y-%m-%d')

        api_body = {
            "RequestHeader":
                prod_request_header,
            "RequestBody":
                {
                    "InquiryPurpose": "05", "TransactionAmount": "100",
                    "FirstName": fname, "MiddleName": mname,
                    "LastName": lname,
                    "InquiryAddresses": [
                        {
                            "seq": "1", "AddressType": ["H"],
                            "AddressLine1": address, "AddressLine2": "",
                            "Locality": "", "City": city, "State": state_code, "Postal": pincode
                        }
                    ],
                    "InquiryPhones": [
                        {
                            "seq": "1", "Number": Number, "PhoneType": ["M"]
                        }
                    ],
                    "EmailAddresses": [],
                    "IDDetails": [
                        {
                            "seq": "1", "IDType": "T", "IDValue": pan,
                            "Source": "Inquiry"
                        }
                    ],
                    "DOB": str(dob)[:10], "Gender": gender
                },
            "Score": [
                {
                    "Type": "ERS", "Version": "3.0"
                }
            ]
        }
        import json
        # Handeling error: Due to weak ssl cert ket python gives requests.exceptions.SSLError - dh key too small
        requests.packages.urllib3.disable_warnings()
        requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += 'HIGH:!DH:!aNULL'
        try:
            requests.packages.urllib3.contrib.pyopenssl.DEFAULT_SSL_CIPHER_LIST += 'HIGH:!DH:!aNULL'
        except AttributeError:
            # no pyopenssl support used / needed / available
            print("attribute error")
            pass

        json_body = json.dumps(api_body)
        # request =requests.post(prod_url2 ,data =json_body, headers=headers,verify='/etc/nginx/letsencrypt/letsencrypt/live/anandfin.com/cert.crt')
        response = requests.post(prod_url2, data=json_body, headers=headers, verify=False)
        # cert= (
        #     '',
        #     ''
        #     )
        # )
        # print(response)
        response_json = response.json()
        # import json
        # json_body = json.dumps(api_body)
        # response = requests.request('POST', prod_url2, data=json_body, headers=headers,verify=False)
        #
        # response_json = response.json()
        if response_json['InquiryResponseHeader']['SuccessCode'] == '1':

            # create master table
            eq_master = create_equi_master(response_json['InquiryResponseHeader'], response_json['InquiryRequestInfo'],
                                           response_json['Score'], loan_obj[0].user_id)

            if eq_master:
                create_equi_inq_response_header(eq_master, response_json['InquiryResponseHeader'])
                inq_res_info = create_inquiry_response_info(eq_master, response_json['InquiryRequestInfo'])
                if inq_res_info:
                    # create_inquiry_address(eq_master, inq_res_info, response_json['InquiryRequestInfo'])
                    create_inquiry_phones(eq_master, inq_res_info, response_json['InquiryRequestInfo'])
                    create_inquiry_details(eq_master, inq_res_info, response_json['InquiryRequestInfo'])
                    create_score(eq_master, response_json['Score'])
                    create_personal_info(eq_master,
                                         response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                    create_identity_info(eq_master,
                                         response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                    create_address_info(eq_master, response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                    create_phone_info(eq_master, response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                    create_email_id_info(eq_master,
                                         response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                    create_retail_account_details(eq_master,
                                                  response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                    create_score_details(eq_master,
                                         response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                    create_enqires(eq_master, response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                    create_enqires_summary(eq_master,
                                           response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                    create_other_key_ind(eq_master,
                                         response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                    create_recent_act(eq_master, response_json['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])

        user_value = EquiFaxMaster.objects.filter(user_id=loan_obj[0].user_id).order_by('-id')
        if user_value:
            score_value = EquiFaxScoreDetails.objects.filter(equifax_id=user_value[0].id).order_by('-id')
            if score_value:
                equfix_score = score_value[0].score_value
    data = {
        'is_taken': equfix_score
    }

    return JsonResponse(data)


class EqufixReport(generic.ListView):
    template_name = "admin/UserAccounts/equfix_dashboard.html"
    title = 'Equfix Dashboard'

    def get(self, request, *args, **kwargs):
        ctx = {}
        try:
            score = EquiFaxScoreDetails.objects.all()
            scoreframe = read_frame(score)
            score_wise_count = scoreframe.groupby(scoreframe['score_value'])['id'].count()
            score_wise_count_count_dict = dict(score_wise_count)
            ctx['score_info'] = score_wise_count_count_dict
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


class EqufixUserReport(generic.ListView):
    template_name = "admin/UserAccounts/equfix_user_report.html"
    title = 'Equfix User Report'

    def get(self, request, score, *args, **kwargs):
        ctx = {}
        try:
            Users = EquiFaxScoreDetails.objects.filter(score_value=score).values('equifax_id')
            master_users = EquiFaxMaster.objects.filter(id__in=Users)
            ctx['users_info'] = master_users
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


def get_data_for_equifax(user_id):
    # Personal
    get_personal_details = Personal.objects.filter(user_id=user_id,type="credit").order_by('-id').first()
    if get_personal_details:
        gender = ''
        if get_personal_details.gender == "Male":
            gender = 'M'
        if get_personal_details.gender == "Female":
            gender = 'F'

        f_name = get_personal_details.name.upper()
        m_name = ''
        l_name = ''
        number = str(get_personal_details.user.username)
        pan = get_personal_details.pan_number
        date_time_str = str(get_personal_details.birthdate)
        dob = datetime.datetime.strptime(date_time_str[:10], '%Y-%m-%d')

        # Address
        address = "FLAT NO 303 RASIKA TOWER  MADHA RAOD KURDWADI LAUL-MH"
        pin_code = "413208"
        city = 'Pune'
        state_code = 'MH'

        get_current_address = Address.objects.filter(user_id=user_id).order_by('-id').first()
        if get_current_address:
            if get_state_code(get_current_address.state) != '':
                state_code = get_state_code(get_current_address.state)

            if get_current_address.address is not None or get_current_address.address == '':
                address = get_current_address.address
            if get_current_address.city is not None or get_current_address.city == '':
                city = get_current_address.city

            if get_current_address.pin_code is not None or get_current_address.pin_code == '':
                pin_code = get_current_address.pin_code

        headers = {
            'content-type': "application/json",
            'cache-control': "no-cache",
        }

        prod_userid = 'STS_ANANDP'
        prod_cust_id = '5860'
        prod_member_number = '024FP00424'
        prod_security_code = 'AN5'
        prod_product = 'PCS'
        prod_password = 'abcd*1234'

        prod_request_header = {
            "CustomerId": prod_cust_id, "UserId": prod_userid,
            "Password": prod_password, "MemberNumber": prod_member_number,
            "SecurityCode": prod_security_code, "ProductCode": [prod_product],
            "CustRefField": "REF16272"
        }

        api_body = {
            "RequestHeader":
                prod_request_header,
            "RequestBody":
                {
                    "InquiryPurpose": "05", "TransactionAmount": "100",
                    "FirstName": f_name, "MiddleName": m_name,
                    "LastName": l_name,
                    "InquiryAddresses": [
                        {
                            "seq": "1", "AddressType": ["H"],
                            "AddressLine1": address, "AddressLine2": "",
                            "Locality": "", "City": city, "State": state_code, "Postal": pin_code
                        }
                    ],
                    "InquiryPhones": [
                        {
                            "seq": "1", "Number": number, "PhoneType": ["M"]
                        }
                    ],
                    "EmailAddresses": [],
                    "IDDetails": [
                        {
                            "seq": "1", "IDType": "T", "IDValue": pan,
                            "Source": "Inquiry"
                        }
                    ],
                    "DOB": str(dob)[:10], "Gender": gender
                },
            "Score": [
                {
                    "Type": "ERS", "Version": "3.0"
                }
            ]
        }

        data = {
            "status": "1",
            "message": "Details found",
            "api_body": api_body,
            "headers": headers,
            "user_id": user_id,
        }

    else:
        data = {
            "status": "2",
            "message": "Personal details not found",
            "api_body": None,
            "headers": None,
        }
    return data


def call_equifax_api(data):
    if "api_body" in data and "headers" in data:
        import json
        # Handling error: Due to weak ssl cert ket python gives requests.exceptions.SSLError - dh key too small
        requests.packages.urllib3.disable_warnings()
        requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += 'HIGH:!DH:!aNULL'
        try:
            requests.packages.urllib3.contrib.pyopenssl.DEFAULT_SSL_CIPHER_LIST += 'HIGH:!DH:!aNULL'
        except AttributeError:
            # no pyopenssl support used / needed / available
            print("attribute error")
            pass

        json_body = json.dumps(data['api_body'])
        response = requests.post(prod_url2, data=json_body, headers=data['headers'], verify=False)

        # print("Equifax api response--", response)
        response_json = response.json()
        return response_json
    return None


def process_equifax_response(response, user_id):
    """
        response should be in json
    :param response:
    :return:
    """
    equfix_score = None
    if response['InquiryResponseHeader']['SuccessCode'] == '1':

        # create master table
        eq_master = create_equi_master(response['InquiryResponseHeader'], response['InquiryRequestInfo'],
                                       response['Score'], user_id)

        if eq_master:
            create_equi_inq_response_header(eq_master, response['InquiryResponseHeader'])
            inq_res_info = create_inquiry_response_info(eq_master, response['InquiryRequestInfo'])
            if inq_res_info:
                # create_inquiry_address(eq_master, inq_res_info, response['InquiryRequestInfo'])
                create_inquiry_phones(eq_master, inq_res_info, response['InquiryRequestInfo'])
                create_inquiry_details(eq_master, inq_res_info, response['InquiryRequestInfo'])
                create_score(eq_master, response['Score'])
                create_personal_info(eq_master,
                                     response['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_identity_info(eq_master,
                                     response['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_address_info(eq_master, response['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_phone_info(eq_master, response['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_email_id_info(eq_master,
                                     response['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_retail_account_details(eq_master,
                                              response['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_score_details(eq_master,
                                     response['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_enqires(eq_master, response['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_enqires_summary(eq_master,
                                       response['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_other_key_ind(eq_master,
                                     response['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])
                create_recent_act(eq_master, response['CCRResponse']['CIRReportDataLst'][0]['CIRReportData'])

    user_value = EquiFaxMaster.objects.filter(user_id=user_id).order_by('-id')
    if user_value:
        score_value = EquiFaxScoreDetails.objects.filter(equifax_id=user_value[0].id).order_by('-id')
        if score_value:
            equfix_score = score_value[0].score_value

    return equfix_score


def get_equifax_score(user_id):
    """
        Gets user's equifax score

        Requirements: User Personal Details

    :param user_id:
    :return: equfix_score or None
    """
    equfix_score = None
    equifax_api_data = get_data_for_equifax(user_id)
    try:
        equifax_response = call_equifax_api(equifax_api_data)
        if equifax_response is not None:
            equfix_score = process_equifax_response(equifax_response, user_id)
    except Exception as e:
        print("------USer all details exc", e)
        print('-----------in exception----------')
        print(e.args)
        import os
        import sys
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
    return equfix_score
