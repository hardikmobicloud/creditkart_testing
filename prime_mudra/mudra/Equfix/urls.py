from django.urls import path
from .views import *

urlpatterns = [
    path('equifax_api/', equfix_API_uat, name='API'),
    path('api_p/', equfix_status_API, name='API'),
    path('equfix_dy_API/', equfix_status_check, name='equfix_dy_API'),
    path('equfix_status_check/', equfix_status_check, name='equfix_status_check'),
    path('prod_test/', prod_test, name='prod_test'),
    path(r'equfix-report/', EqufixReport.as_view(), name='equfix_report'),
    path(r'equfix-user-report/<score>/', EqufixUserReport.as_view(), name='equfix_user_report'),

]
