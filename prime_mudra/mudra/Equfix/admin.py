from django.contrib import admin
from .models import *


class EquiFaxMasterAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'score_type', 'score_version', 'status',)
    search_fields = ('first_name', 'last_name', 'user__username', 'status',)


class EquiFaxScoreDetailsAdmin(admin.ModelAdmin):
    list_display = ('equifax', 'score_type', 'score_version', 'score_name', 'score_value', 'scoring_element')


admin.site.register(EquiFaxMaster, EquiFaxMasterAdmin)
admin.site.register(EquiFaxScoreDetails, EquiFaxScoreDetailsAdmin)
