# Register your models here.
from django.contrib import admin
from .models import *
from django.conf import settings
from LoanAppData.models import LoanApplication


class RBLTransactionAdmin(admin.ModelAdmin):
    list_display = (
        'user_display', 'created_date', 'transaction_id_text', 'corp_id',
        'ref_number', 'channel_part_ref_number', 'amount',
        'ben_acc_no', 'ben_ifsc_code', 'txn_time',
        'status', 'error_description',
    )

    list_filter = (
        'transaction_id_text', 'status',
    )

    search_fields = ('transaction_id_text',)

    list_display_links = None

    def user_display(self, obj):
        loan_obj = LoanApplication.objects.get(loan_application_id=obj.transaction_id_text)
        return loan_obj.user

    user_display.short_description = "User"


class PaymentAdmin(admin.ModelAdmin):
    list_display = (
        'order_id', 'transaction_id', 'pg_transaction_id', 'status',
        'category', 'mode', 'type', 'date', 'create_date',
        'amount', 'created_by_new', 'updated_by_new'
    )

    list_filter = (
        'status',
        'category', 'mode', 'type', 'date',

    )

    search_fields = ('order_id', 'transaction_id', 'status',
                     'category', 'mode', 'type', 'date',
                     'amount',)


admin.site.register(RBLTransaction, RBLTransactionAdmin)
admin.site.register(Payment, PaymentAdmin)
