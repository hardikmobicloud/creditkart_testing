import json
import requests
from datetime import datetime, timedelta
from django.views import generic
from User.models import Subscription
from django.shortcuts import render, HttpResponse
from django.contrib.auth.models import User
from django.db.models import Sum
from requests import Response
from rest_framework import generics
from rest_framework.views import APIView

from .models import *
from .tasks import rbl_transaction_status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django_pandas.io import read_frame
import pandas as pd
from functools import reduce
from django.apps.registry import apps

# from LoanAppData.models import LoanApplication
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
LoanRepayment = apps.get_model('LoanAppData', 'loanapplication_repayment_amount')


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class IncomeExpenseSubscriptionByMonth(generic.ListView):
    template_name = "admin/UserAccounts/income_expense_by_month.html"
    title = 'Daily Repayments'

    def get(self, request, *args, **kwargs):
        ctx = {}
        try:
            date_set = set()
            incame_expense_data = Subscription.objects.all()
            for user in incame_expense_data:
                dt = str(user.start_date)
                date_set.add(dt[0:7])
            create_month = []
            amount = []
            count = []
            for dt in date_set:
                pay = Payment.objects.filter(date__startswith=dt, status='success', category='Subscription').count()
                cost_sum = \
                    Payment.objects.filter(date__startswith=dt, status='success', category='Subscription').aggregate(
                        Sum('amount'))['amount__sum']
                create_month.append(dt)
                amount.append(cost_sum)
                count.append(pay)
            ctx['repay_info'] = zip(create_month, amount, count)
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class IncomeExpenseSubscriptionUserByDate(generic.ListView):
    template_name = "admin/UserAccounts/sub.html"
    title = 'Daily Repayments'

    def get(self, request, create_month, *args, **kwargs):
        ctx = {}
        try:
            date_set = set()
            user_name = []
            start_date = []
            end_date = []
            tra_status = []
            amount = []
            incame_expense_data = Subscription.objects.filter(start_date__startswith=create_month,
                                                              payment_status='success')
            for user in incame_expense_data:
                payment = Payment.objects.filter(status='success', category='Subscription', id=user.payment_id_id)
                u = User.objects.get(id=user.user_id)
                user_name.append(u.username)
                start_date.append(user.start_date)
                end_date.append(user.expiry_date)
                tra_status.append(user.payment_status)
                if payment:
                    amount.append(payment[0].amount)
                else:
                    amount.append(0)

            ctx['repay_info'] = zip(user_name, start_date, end_date, tra_status, amount)
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class IncomeExpenseSubscriptionUserByDate_all(generic.ListView):
    template_name = "admin/UserAccounts/sub1.html"
    title = 'Daily Repayments'

    def get(self, request, *args, **kwargs):
        ctx = {}
        try:
            date_set = set()
            user_name = []
            start_date = []
            end_date = []
            tra_status = []
            active = []
            amount = []
            trnsaction_id = []
            paytm_id = []
            incame_expense_data = Subscription.objects.all()
            for user in incame_expense_data:
                u = User.objects.get(id=user.user_id)
                user_name.append(u.username)
                start_date.append(user.created_date)
                end_date.append(user.expiry_date)
                tra_status.append(user.transaction_status)
                active.append(user.subscription_status)
                amount.append(user.cost)
                trnsaction_id.append(user.transaction_id)
                paytm_id.append(user.atom_transaction_id)

            ctx['repay_info'] = zip(user_name, start_date, end_date, tra_status, active, amount, trnsaction_id,
                                    paytm_id)
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class IncomeExpenseSubscriptionByDate(generic.ListView):
    template_name = "admin/UserAccounts/subcription_cost.html"
    title = 'Daily Repayments'

    def get(self, request, on_month, *args, **kwargs):
        ctx = {}
        try:
            date_set = set()
            incame_expense_data = Subscription.objects.filter(start_date__startswith=on_month)
            for user in incame_expense_data:
                dt = str(user.start_date)
                date_set.add(dt[0:10])

            create_month = []
            amount = []
            count_total = []
            for dt in date_set:
                pay = Payment.objects.filter(date__startswith=dt, status='success', category='Subscription').count()
                cost_sum = \
                    Payment.objects.filter(date__startswith=dt, status='success', category='Subscription').aggregate(
                        Sum('amount'))['amount__sum']
                create_month.append(dt)
                count_total.append(pay)
                amount.append(cost_sum)
            ctx['repay_info'] = zip(create_month, amount, count_total)
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


def rbl_transaction_sts(request, loan_id):
    status = rbl_transaction_status(loan_id)
    return HttpResponse("ok")


def loan_rbl(request):
    date = ['2019-12-14', '2019-12-15']


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class PaymentsByMonths(generic.ListView):
    template_name = "admin/UserAccounts/payments_monthwise.html"
    title = 'Payments By Months'

    def get(self, request, *args, **kwargs):
        ctx = {}
        monthwise_payment_dict = {}
        monthwise_Paytm_payment_dict = {}
        try:
            Rpay_payments = Payment.objects.filter(pay_source="rpay")
            rpay_dataframe = read_frame(Rpay_payments)
            rpay_dataframe['YearMonth'] = pd.to_datetime(rpay_dataframe['create_date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            monthwise_payment_dict = \
                rpay_dataframe.groupby(rpay_dataframe['YearMonth'])['amount'].sum().sort_index(axis = 0)
            monthwise_payment_dict = dict(monthwise_payment_dict)

            paytm_payments = Payment.objects.filter(pay_source="paytm")
            paytm_dataframe = read_frame(paytm_payments)
            paytm_dataframe['YearMonth'] = pd.to_datetime(paytm_dataframe['create_date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            monthwise_Paytm_payment_dict = \
                paytm_dataframe.groupby(paytm_dataframe['YearMonth'])['amount'].sum().sort_index(axis = 0)
            monthwise_Paytm_payment_dict = dict(monthwise_Paytm_payment_dict)

            BothDicts = [monthwise_payment_dict, monthwise_Paytm_payment_dict]
            allkey = reduce(set.union, map(set, map(dict.keys, BothDicts)))
            for i in allkey:
                if i in monthwise_payment_dict:
                    pass
                else:
                    monthwise_payment_dict[i] = None
                if i in monthwise_Paytm_payment_dict:
                    pass
                else:
                    monthwise_Paytm_payment_dict[i] = None

            MainDict = {}
            for k in monthwise_payment_dict.keys():
                MainDict[k] = list(MainDict[k] for MainDict in BothDicts)
            ctx['MainData'] = MainDict
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class PaymentsByDates(generic.ListView):
    template_name = "admin/UserAccounts/payments_datewise.html"
    title = ' Payments Datewise '

    def get(self, request, on_month, *args, **kwargs):
        Main_Dict = {}
        datewise_payment_dict = {}
        datewise_Paytm_payment_dict = {}
        ctx = {}
        new_date = ""
        try:
            date_list = on_month.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
                new_date = '{}-{}'.format(y, m)
            else:
                m = '0' + str(date_list[1])
                new_date = '{}-{}'.format(y, m)
            # For Counting payments Datewise
            Rpay_payments = Payment.objects.filter(create_date__startswith=new_date, pay_source="rpay").order_by(
                '-create_date')
            rpay_dataframe = read_frame(Rpay_payments)
            rpay_dataframe['YearMonthDay'] = pd.to_datetime(rpay_dataframe['create_date']).apply(
                lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
            datewise_payment_dict = \
                rpay_dataframe.groupby(rpay_dataframe['YearMonthDay'])['amount'].sum().sort_index(axis = 0)
            datewise_payment_dict = dict(datewise_payment_dict)
            # For Counting Paytm payments datewise
            paytm_payments = Payment.objects.filter(create_date__startswith=new_date, pay_source="paytm").order_by(
                '-create_date')
            paytm_dataframe = read_frame(paytm_payments)
            paytm_dataframe['YearMonthDay'] = pd.to_datetime(paytm_dataframe['create_date']).apply(
                lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
            datewise_Paytm_payment_dict = \
                paytm_dataframe.groupby(paytm_dataframe['YearMonthDay'])['amount'].sum().sort_index(axis = 0)
            datewise_Paytm_payment_dict = dict(datewise_Paytm_payment_dict)
            # For Counting Completed loans datewise
            BothDicts = [datewise_payment_dict, datewise_Paytm_payment_dict]
            allkey = reduce(set.union, map(set, map(dict.keys, BothDicts)))
            for i in allkey:
                if i in datewise_payment_dict:
                    pass
                else:
                    datewise_payment_dict[i] = None
                if i in datewise_Paytm_payment_dict:
                    pass
                else:
                    datewise_Paytm_payment_dict[i] = None

            MainDict = {}
            for k in datewise_payment_dict.keys():
                MainDict[k] = list(MainDict[k] for MainDict in BothDicts)
            ctx['MainData'] = MainDict
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


from datetime import datetime, timedelta


def dailysubscriptioncheck_old(request):
    post_data = {}
    mobile_list = []
    yesterday = datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d')
    # This function is to return yesterday Subscriptions mobile no.
    sub_data = Subscription.objects.filter(start_date__startswith=str(yesterday)[:10], status="success")
    if sub_data:
        for data in sub_data:
            mobile_list.append(data.user.username)
        post_data = {'mobile_list': mobile_list}
        req_data = requests.post(url='http://127.0.0.1:8001/api/mudra-sub-check/', data=post_data)
        if req_data:
            req_data = req_data.json()
            req_data = req_data['data']
            if req_data and req_data is not None:
                sub_data = Subscription.objects.filter(user__username__in=req_data,
                                                       payment_status="success")
                if sub_data:
                    updated = Subscription.objects.filter(user__username__in=req_data,
                                                          payment_status="success").update(
                        is_loanmart="True")
            else:
                pass
        return HttpResponse('Ok')


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class MonthlyManuallyCancelled(generic.ListView):
    template_name = "admin/UserAccounts/monthly_manually_cancelled.html"
    title = 'Monthly Manually Cancelled'

    def get(self, request, *args, **kwargs):
        ctx = {}
        monthwise_cancelled_dict = {}
        try:
            payments = Payment.objects.filter(status="manually_cancelled")
            dataframe = read_frame(payments)
            dataframe['YearMonth'] = pd.to_datetime(dataframe['create_date']).apply(
                lambda x: '{year}-{month}'.format(year=x.year, month=x.month))
            monthwise_cancelled_dict = \
                dataframe.groupby(dataframe['YearMonth'])['id'].count().sort_index(axis = 0)
            monthwise_cancelled_dict = dict(monthwise_cancelled_dict)

            ctx['MainData'] = monthwise_cancelled_dict
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class DaywiseManuallyCancelled(generic.ListView):
    template_name = "admin/UserAccounts/daywise_manually_cancelled.html"
    title = ' Datewise Manually Cancelled '

    def get(self, request, on_month, *args, **kwargs):
        datewise_payment_dict = {}
        ctx = {}
        new_date = ""
        try:
            date_list = on_month.split('-')
            y = date_list[0]
            if len(date_list[1]) > 1:
                m = date_list[1]
                new_date = '{}-{}'.format(y, m)
            else:
                m = '0' + str(date_list[1])
                new_date = '{}-{}'.format(y, m)
            # For Counting payments Datewise
            payments = Payment.objects.filter(status="manually_cancelled")
            # print('pauyments', payments)
            dataframe = read_frame(payments)
            dataframe['YearMonthDay'] = pd.to_datetime(dataframe['create_date']).apply(
                lambda x: '{year}-{month}-{day}'.format(year=x.year, month=x.month, day=x.day))
            daywise_cancelled_dict = \
                dataframe.groupby(dataframe['YearMonthDay'])['id'].count().sort_index(axis = 0)
            daywise_cancelled_dict = dict(daywise_cancelled_dict)

            ctx['MainData'] = daywise_cancelled_dict
            return render(request, self.template_name, ctx)
        except:
            return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class ManuallyCancelledOnday(generic.ListView):
    template_name = "admin/UserAccounts/manually_cancelled_onday.html"
    title = ' Manually Cancelled Onday '

    def get(self, request, on_date, *args, **kwargs):
        paydata_dict = {}
        ctx = {}
        new_date = ""
        date_list = on_date.split('-')
        y = date_list[0]
        if len(date_list[1]) > 1:
            m = date_list[1]
            new_date = '{}-{}'.format(y, m)
        else:
            m = '0' + str(date_list[1])
            new_date = '{}-{}'.format(y, m)
        if len(date_list[2]) > 1:
            d = date_list[2]
            new_date = '{}-{}-{}'.format(y, m, d)
        else:
            d = '0' + str(date_list[2])
            new_date = '{}-{}-{}'.format(y, m, d)
        # For Counting payments Datewise
        payments = Payment.objects.filter(create_date__startswith=new_date,
                                          status="manually_cancelled")
        if payments:
            for payment in payments:
                payment_list = []
                if payment.order_id and payment.order_id is not None:
                    mobile_number = None
                    loanid = None
                    order_id = payment.order_id
                    if 'ms' in payment.order_id:
                        split_str = str(order_id).split("ms")
                        mobile_number = split_str[0]

                    elif 'm' in payment.order_id:
                        split_str = str(order_id).split("m")
                        loanid = split_str[0]
                payment_list.extend((mobile_number, loanid, payment.transaction_id,
                                     payment.pg_transaction_id, payment.category, payment.status))
                paydata_dict.update({order_id: payment_list})

        ctx['MainData'] = paydata_dict
        return render(request, self.template_name, ctx)


@method_decorator([login_required(login_url="/login/")], name='dispatch')
class RblInitiated(generic.ListView):
    template_name = "admin/UserAccounts/rbl_initiated.html"
    title = ' Rbl Initiated '

    def get(self, request, *args, **kwargs):
        ctx = {}
        transactions = RBLTransaction.objects.filter(status="initiated")
        if transactions:
            ctx['MainData'] = transactions
        return render(request, self.template_name, ctx)


import requests
from datetime import datetime, timedelta


def dailysubscriptioncheck():
    post_url = 'https://www.loanmartt.com/api/mudra-sub-check/'
    post_data = {}
    mobile_list = []
    y_day = datetime.now() - timedelta(days=1)
    yesterday = datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d')
    # This function is to return yesterday Subscriptions mobile no.
    sub_data = Subscription.objects.filter(start_date__startswith=str(yesterday)[:10],
                                           payment_status__icontains="success")
    if sub_data:
        for data in sub_data:
            if data.user.date_joined.date() >= y_day.date():
                mobile_list.append(data.user.username)
        post_data = {'mobile_list': mobile_list}
        req_data = requests.post(url=post_url, data=post_data)
        if req_data:
            req_data = req_data.json()
            req_data = req_data['data']
            if req_data and req_data is not None:
                sub_data = Subscription.objects.filter(user__username__in=req_data,
                                                       payment_status__icontains="success",
                                                       start_date__startswith=str(yesterday)[:10])
                if sub_data:
                    updated = Subscription.objects.filter(user__username__in=req_data,
                                                          payment_status__icontains="success",
                                                          start_date__startswith=str(yesterday)[:10]).update(
                        is_loanmart="True")
            else:
                pass
