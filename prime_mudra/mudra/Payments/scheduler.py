# from apscheduler.schedulers.background import BackgroundScheduler
# import os
# import sys
#
# from LoanAppData.views import OverdueView_sch
# from MApi.yelo import yelo_report
# from automation.views import automation_data_send
# from Recovery.views import auto_allocate_def
# from Payments.views import dailysubscriptioncheck
# from LoanAppData.views import defaulter_messages_scheduler
#
# # Initializing The scheduler
# # Note: Do Not Remove this
# scheduler = BackgroundScheduler()
#
# try:
#     OverdueView_sch = scheduler.add_job(OverdueView_sch, 'cron', day='1-31', hour='10', minute='07')
#     print("OverdueView_schedular :- OverdueView_sch", OverdueView_sch)
#
# except Exception as e:
#     print('---------Scheduler Exception----------')
#     print(e.args)
#     exc_type, exc_obj, exc_tb = sys.exc_info()
#     fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
#     print(exc_type, fname, exc_tb.tb_lineno)
#     pass
#
# try:
#     # https://apscheduler.readthedocs.io/en/latest/modules/triggers/cron.html
#     yelo_report = scheduler.add_job(yelo_report, 'cron', day='1-31', hour='10', minute='07')
#     print("yelo_report report:- yelo_report", yelo_report)
#
# except Exception as e:
#     print('---------Scheduler Exception----------')
#     print(e.args)
#     exc_type, exc_obj, exc_tb = sys.exc_info()
#     fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
#     print(exc_type, fname, exc_tb.tb_lineno)
#     pass
#
# try:
#     automation_data_send = scheduler.add_job(automation_data_send, 'cron', day='1-31', hour='3', minute='05')
#     print("automation report:- automation_data_send", automation_data_send)
# except Exception as e:
#     print('---------Scheduler Exception----------')
#     print(e.args)
#     exc_type, exc_obj, exc_tb = sys.exc_info()
#     fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
#     print(exc_type, fname, exc_tb.tb_lineno)
#     pass
#
# # try:
# #     update_yelo_leads = scheduler.add_job(update_yelo_leads, 'interval', minutes=1)
# #     print("update_yelo_leads scheduler:- update_yelo_leads", update_yelo_leads)
# #
# # except Exception as e:
# #     print('---------Scheduler Exception----------')
# #     print(e.args)
# #     exc_type, exc_obj, exc_tb = sys.exc_info()
# #     fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
# #     print(exc_type, fname, exc_tb.tb_lineno)
# #     pass
#
# # try:
# #     check_subscription = scheduler.add_job(Sub_Transaction_Status_Check_auto, 'interval', hours=1)
# #     check_tran = scheduler.add_job(Transaction_Status_Check_auto, 'interval', hours=1)
# #     print("check_subscription", check_subscription)
# #     print(check_tran)
# # except Exception as e:
# #     print('---------Scheduler Exception----------')
# #     print(e.args)
# #     exc_type, exc_obj, exc_tb = sys.exc_info()
# #     fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
# #     print(exc_type, fname, exc_tb.tb_lineno)
# #     pass
# #
#
# try:
#     defaulters_message = scheduler.add_job(defaulter_messages_scheduler, 'interval', minutes=30)
#     print("defaulters_message", defaulters_message)
#     print(defaulters_message)
# except Exception as e:
#     print('---------Scheduler Exception----------')
#     print(e.args)
#     exc_type, exc_obj, exc_tb = sys.exc_info()
#     fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
#     print(exc_type, fname, exc_tb.tb_lineno)
#     pass
#
# # try:
# #     allocate = scheduler.add_job(auto_allocate_def, 'cron', day='1-31', hour='9', minute='30')
# # except Exception as e:
# #     print('---------Scheduler Exception----------')
# #     print(e.args)
# #     exc_type, exc_obj, exc_tb = sys.exc_info()
# #     fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
# #     print(exc_type, fname, exc_tb.tb_lineno)
# #     pass
#
#
# try:
#     dailysubscriptioncheck = scheduler.add_job(dailysubscriptioncheck, 'cron', day='1-31', hour='03', minute='00')
#     print("daily subscription check scheduler:- ", dailysubscriptioncheck)
#
# except Exception as e:
#     print('---------Scheduler Exception----------')
#     print(e.args)
#     exc_type, exc_obj, exc_tb = sys.exc_info()
#     fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
#     print(exc_type, fname, exc_tb.tb_lineno)
#     pass
#
# # Note: Do Not Remove this
# scheduler.start()
