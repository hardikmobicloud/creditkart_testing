from __future__ import absolute_import, unicode_literals
from celery import shared_task
from django.core.mail import send_mail
from django.contrib.auth.models import User
import datetime
from datetime import timedelta
from django.contrib.sites.models import Site
from django.apps.registry import apps
import requests
import json
from django.conf import settings
from mudra import constants
from MApi.email import email, sms
from django.shortcuts import render, HttpResponse

RBLTransaction = apps.get_model('Payments', 'RBLTransaction')
Payments = apps.get_model('Payments', 'Payment')
LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')
NotificationEntry = apps.get_model('UserData', 'NotificationEntry')

import logging

logger = logging.getLogger(__name__)











# @shared_task
def rbl_worker(loan_application_id,amount):
    import requests

    """
    RBL payment process cron
    """
    logger.info("In Rbl_worker")
    logger.debug(loan_application_id)
    trnsId = True
    loan_application = LoanApplication.objects.get(loan_application_id=loan_application_id)

    # url = "https://apideveloper.rblbank.com/test/sb/rbl/v1/payments/corp/payment"
    #
    url = "https://gateway.rblbank.com/prod/sb/rbl/v1/payments/corp/payment"

    querystring = {
        "client_id": "1e4081cb-fc29-43ac-963b-f6df0dff228e",
        "client_secret": "F6uO3oH3rE3tB1vR7bW0nG0bS6hA7vE8mP7vP5mF7pS2dP0rF7"
    }
    loan_amount_1 = amount
    tenure = 90
    if loan_application:
        transaction_id = loan_application.loan_application_id
        loan_amount = str(loan_amount_1)
    else:
        transaction_id = ''
        loan_amount = '0.00'
    # print("{}", transaction_id, loan_amount)
    debit_acc_no = settings.DEBIT_ACC_NO
    debit_acc_name = settings.DEBIT_ACC_NAME
    debit_ifsc = settings.DEBIT_IFSC
    debit_mobile_number = settings.DEBIT_MOBILE_NO
    mode_of_payment = settings.MODE_OF_PAYMENT
    ben_IFSC = loan_application.bank_id.ifsc_code
    ben_acc_number = loan_application.bank_id.account_number
    ben_name = loan_application.user_personal.name
    ben_email = 'info@thecreditkart.com'

    if loan_application.user_professional.company_email_id:
        ben_email = loan_application.user_professional.company_email_id
    signature = ''
    user_ben_bank_name = loan_application.bank_id.bank_name
    if len(user_ben_bank_name) > 15:

        user_ben_bank_name = loan_application.bank_id.bank_name.replace(
            loan_application.bank_id.bank_name[15:], '')
        user_ben_bank_name_strip = user_ben_bank_name.strip()

    else:

        user_ben_bank_name_strip = user_ben_bank_name.strip()

    user_ben_bank_name_strip = user_ben_bank_name_strip.replace('&', 'and').replace('^', '').replace('_', '')
    user_ben_bank_name = user_ben_bank_name_strip
    ben_mobile_no = loan_application.user_personal.mobile_no
    remarks = 'Loan Request'
    today_date = datetime.datetime.now()
    loan_start_date_strip = datetime.datetime.strptime(str(today_date)[:-7],
                                                       "%Y-%m-%d %H:%M:%S")
    repayment_date = loan_start_date_strip + datetime.timedelta(days=int(tenure))

    user_loan_instance = LoanApplication.objects.get(loan_application_id=loan_application_id)

    disbursed_amount_count = loan_application.disbursed_amount.all().count()
    if disbursed_amount_count:
        new_loan_application_id = str(loan_application_id) + str(disbursed_amount_count - 1)
    else:
        new_loan_application_id = loan_application_id

    transaction_id_text = new_loan_application_id
    amount = amount
    disburse_obj = RBLTransaction.objects.create(
        transaction_id_text=new_loan_application_id,
        amount=amount,
        txn_time=today_date,
        status='initiated'

    )

    payment_instance = Payments.objects.create(
        order_id=new_loan_application_id,
        status='initiated',
        category='Rbl',
        type='Account',
        mode=mode_of_payment,
        amount=amount,
        date=today_date
    )
    # Adding payment foreign key in user loan table
    loan_obj = LoanApplication.objects.filter(loan_application_id=loan_application_id)
    loan_obj[0].disbursed_amount.add(payment_instance)
    json_obj = {}


    payload = "{\n  \"Single_Payment_Corp_Req\": {\n    \"Header\": {\n      \"TranID\": \"" + new_loan_application_id + "\",\n      \"Corp_ID\": \"" + settings.CORP_ID + "\",\n      \"Maker_ID\": \"" + settings.MAKER_ID + "\",\n      \"Checker_ID\": \"" + settings.CHECKER_ID + "\",\n      \"Approver_ID\": \"" + settings.APPROVER_ID + "\"\n    },\n    \"Body\": {\n      \"Amount\": \"" + loan_amount + "\",\n      \"Debit_Acct_No\": \"" + debit_acc_no + "\",\n      \"Debit_Acct_Name\": \"" + debit_acc_name + "\",\n      \"Debit_IFSC\": \"" + debit_ifsc + "\",\n      \"Debit_Mobile\": \"" + debit_mobile_number + "\",\n      \"Ben_IFSC\": \"" + ben_IFSC + "\",\n      \"Ben_Acct_No\": \"" + ben_acc_number + "\",\n      \"Ben_Name\": \"" + ben_name + "\",\n      \"Ben_BankName\": \"" + user_ben_bank_name + "\",\n      \"Ben_Email\": \"" + ben_email + "\",\n      \"Ben_Mobile\": \"" + ben_mobile_no + "\",\n      \"Mode_of_Pay\": \"" + mode_of_payment + "\",\n      \"Remarks\": \"" + remarks + "\"\n    },\n    \"Signature\": {\n      \"Signature\": \"" + signature + "\"\n    }\n  }\n}"

    headers = {
        'content-type': "application/json",
        'authorization': "Basic QU5BTkRQUk9QRTpNdWRyYWt3aWtAMTIz",
        'cache-control': "no-cache",
    }

    print('&&&&&&&&&&&&&&&& payload &&&&&&&&&&&&&\n')
    print(payload)
    print('&&&&&&&&&&&&&&&& payload &&&&&&&&&&&&&\n')

    response = requests.request(
        "POST",
        url,
        data=payload,
        headers=headers,
        params=querystring,

        cert=(
            '/etc/nginx/anandfin.com-ssl/f57e2eff7923b1c7.crt',
            '/etc/nginx/anandfin.com-ssl/anandfin.com.key'
        )

    )

    json_instance = json.dumps(response.text)


    json_obj = response.json()

    signature = json_obj['Single_Payment_Corp_Resp']['Signature']['Signature']

    if json_obj['Single_Payment_Corp_Resp']['Header']['Status'] == 'Success':

        # get user txn obj

        if json_obj['Single_Payment_Corp_Resp']['Header']['TranID']:
            transaction_id = json_obj['Single_Payment_Corp_Resp']['Header']['TranID']
        else:
            transaction_id = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Corp_ID']:
            corp_id = json_obj['Single_Payment_Corp_Resp']['Header']['Corp_ID']
        else:
            corp_id = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Maker_ID']:
            maker_id = json_obj['Single_Payment_Corp_Resp']['Header']['Maker_ID']
        else:
            maker_id = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Checker_ID']:
            checker_id = json_obj['Single_Payment_Corp_Resp']['Header']['Checker_ID']
        else:
            checker_id = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Approver_ID']:
            approver_id = json_obj['Single_Payment_Corp_Resp']['Header']['Approver_ID']
        else:
            approver_id = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Resp_cde']:
            response_code = json_obj['Single_Payment_Corp_Resp']['Header']['Resp_cde']
        else:
            response_code = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Error_Desc']:
            error_description = json_obj['Single_Payment_Corp_Resp']['Header']['Error_Desc']
        else:
            error_description = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Status']:
            status = json_obj['Single_Payment_Corp_Resp']['Header']['Status']
        else:
            status = ''

        if json_obj['Single_Payment_Corp_Resp']['Body']['RefNo']:
            payment_ref_no = json_obj['Single_Payment_Corp_Resp']['Body']['RefNo']
        else:
            payment_ref_no = ''

        if json_obj['Single_Payment_Corp_Resp']['Body']['channelpartnerrefno']:
            channel_part_ref_no = json_obj['Single_Payment_Corp_Resp']['Body']['channelpartnerrefno']
        else:
            channel_part_ref_no = ''

        if json_obj['Single_Payment_Corp_Resp']['Body']['RRN']:
            rrn = json_obj['Single_Payment_Corp_Resp']['Body']['RRN']
        else:
            rrn = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Status']:
            payment_status = json_obj['Single_Payment_Corp_Resp']['Header']['Status']
        else:
            payment_status = ''

        if json_obj['Single_Payment_Corp_Resp']['Body']['Ben_Acct_No']:
            ben_acc_no = json_obj['Single_Payment_Corp_Resp']['Body']['Ben_Acct_No']
        else:
            ben_acc_no = ''

        if json_obj['Single_Payment_Corp_Resp']['Body']['Amount']:
            amount = json_obj['Single_Payment_Corp_Resp']['Body']['Amount']
        else:
            amount = ''

        if json_obj['Single_Payment_Corp_Resp']['Body']['BenIFSC']:
            ben_ifsc = json_obj['Single_Payment_Corp_Resp']['Body']['BenIFSC']
        else:
            ben_ifsc = ''

        if json_obj['Single_Payment_Corp_Resp']['Body']['Txn_Time']:
            tnx_payment_datetime = json_obj['Single_Payment_Corp_Resp']['Body']['Txn_Time']
        else:
            tnx_payment_datetime = ''

        date_converted = datetime.datetime.strptime(tnx_payment_datetime, '%Y-%m-%d %H:%M:%S.%f').strftime(
            '%Y-%m-%d %H:%M:%S')

        RBLTransaction.objects.filter(
            transaction_id_text=transaction_id,
        ).update(
            corp_id=corp_id, maker_id=maker_id,
            checker_id=checker_id, approver_id=approver_id,
            status=status, response_code=response_code, error_description=error_description,
            ref_number=payment_ref_no, channel_part_ref_number=channel_part_ref_no,
            rrn=rrn, ben_acc_no=ben_acc_no, amount=amount, ben_ifsc_code=ben_ifsc,
            txn_time=datetime.datetime.now(),
            payment=payment_instance
        )



        if status == 'Success':
            status_payment = 'success'
        elif status == 'Failure':
            status_payment = 'failed'
        else:
            status_payment = status
        Payments.objects.filter(
            order_id=transaction_id).update(status=status_payment, transaction_id=rrn, date=tnx_payment_datetime)



        rp_date = repayment_date.date()
        loan_st_date = loan_start_date_strip.date()
        user_fcm = NotificationEntry.objects.filter(user_id=loan_application.user_id)



        # Email code

        email_kwargs = {
            'subject': 'CreditKart - Loan approved',
            'to_email': [str(loan_application.user_professional.company_email_id)],
            'email_template': 'emails/loan/loan_approved.html',
            'data': {
                'domain': Site.objects.get_current(),
                'loan_instance': loan_application
            },
        }

        email(**email_kwargs)

        # SMS code
        try:

            sms_message = 'Your withdrawal of Rs.' + str(amount) + ' from CreditKart wallet has been successfully transferred to your bank account.'

            sms_kwrgs = {
                'sms_message': sms_message,
                'number': str(loan_application.user.username)
            }

            sms(**sms_kwrgs)

        except:

            sms_message = 'Your withdrawal of Rs.'+str(amount)+' from CreditKart wallet has been successfully transferred to your bank account.'

            sms_kwrgs = {
                'sms_message': sms_message,
                'number': str(loan_application.user.username)
            }

            sms(**sms_kwrgs)

    else:

        if json_obj['Single_Payment_Corp_Resp']['Header']['TranID']:
            transaction_id = json_obj['Single_Payment_Corp_Resp']['Header']['TranID']
        else:
            transaction_id = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Corp_ID']:
            corp_id = json_obj['Single_Payment_Corp_Resp']['Header']['Corp_ID']
        else:
            corp_id = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Maker_ID']:

            maker_id = json_obj['Single_Payment_Corp_Resp']['Header']['Maker_ID']
        else:
            maker_id = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Checker_ID']:
            checker_id = json_obj['Single_Payment_Corp_Resp']['Header']['Checker_ID']
        else:
            checker_id = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Approver_ID']:
            approver_id = json_obj['Single_Payment_Corp_Resp']['Header']['Approver_ID']
        else:
            approver_id = ''

        if 'Resp_cde' in json_obj['Single_Payment_Corp_Resp']['Header'] and \
                json_obj['Single_Payment_Corp_Resp']['Header']['Resp_cde'] is not None:
            response_code = json_obj['Single_Payment_Corp_Resp']['Header']['Resp_cde']
        else:
            response_code = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Error_Desc']:
            error_description = json_obj['Single_Payment_Corp_Resp']['Header']['Error_Desc']
        else:
            error_description = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Status']:
            status = json_obj['Single_Payment_Corp_Resp']['Header']['Status']
        else:
            status = ''

        payment_ref_no = None

        try:
            channel_part_ref_no = json_obj['Single_Payment_Corp_Resp']['Body']['channelpartnerrefno']
        except:
            channel_part_ref_no = ''

        try:
            rrn = json_obj['Single_Payment_Corp_Resp']['Body']['RRN']
        except:
            rrn = ''

        ben_acc_no = None
        # amount = None
        ben_ifsc = None
        tnx_payment_datetime = None
        date_converted = None

        RBLTransaction.objects.filter(
            transaction_id_text=new_loan_application_id,
        ).update(
            corp_id=corp_id, maker_id=maker_id,
            checker_id=checker_id, approver_id=approver_id,
            status=status, response_code=response_code, error_description=error_description,
            ref_number=payment_ref_no, channel_part_ref_number=channel_part_ref_no,
            rrn=rrn, ben_acc_no=ben_acc_no, amount=amount, ben_ifsc_code=ben_ifsc,
            txn_time=datetime.datetime.now()
        )
        Payments.objects.filter(order_id=new_loan_application_id).update(status=status, category='Rbl', type='Account',
                                                                         amount=amount, date=today_date)


        import requests
        sms_message = 'Dear Admin, Loan Application number ' + str(
            transaction_id) + ' is not proceeding \n due to ' + str(error_description)

        sms_kwrgs = {
            'sms_message': sms_message,
            'number': "6352820504"
        }

        sms(**sms_kwrgs)
    return True








# @shared_task
def rbl_worker_old(loan_application_id,amount):
    import requests

    """
    RBL payment process cron
    """
    logger.info("In Rbl_worker")
    logger.debug(loan_application_id)
    trnsId = True
    loan_application = LoanApplication.objects.get(loan_application_id=loan_application_id)

    # url = "https://apideveloper.rblbank.com/test/sb/rbl/v1/payments/corp/payment"
    #
    url = "https://gateway.rblbank.com/prod/sb/rbl/v1/payments/corp/payment"

    querystring = {
        "client_id": "1e4081cb-fc29-43ac-963b-f6df0dff228e",
        "client_secret": "F6uO3oH3rE3tB1vR7bW0nG0bS6hA7vE8mP7vP5mF7pS2dP0rF7"
    }
    loan_amount_1 = "0"
    amount = amount
    tenure = 90

    if loan_application:
        transaction_id = loan_application.loan_application_id
        loan_amount = str(loan_amount_1)
    else:
        transaction_id = ''
        loan_amount = '0.00'

    debit_acc_no = settings.DEBIT_ACC_NO
    debit_acc_name = settings.DEBIT_ACC_NAME
    debit_ifsc = settings.DEBIT_IFSC
    debit_mobile_number = settings.DEBIT_MOBILE_NO
    mode_of_payment = settings.MODE_OF_PAYMENT
    ben_IFSC = loan_application.bank_id.ifsc_code
    ben_acc_number = loan_application.bank_id.account_number
    ben_name = loan_application.user_personal.name
    ben_email = 'info@thecreditkart.com'

    if loan_application.user_professional.company_email_id:
        ben_email = loan_application.user_professional.company_email_id
    signature = ''
    user_ben_bank_name = loan_application.bank_id.bank_name
    if len(user_ben_bank_name) > 15:

        user_ben_bank_name = loan_application.bank_id.bank_name.replace(
            loan_application.bank_id.bank_name[15:], '')
        user_ben_bank_name_strip = user_ben_bank_name.strip()

    else:

        user_ben_bank_name_strip = user_ben_bank_name.strip()

    user_ben_bank_name_strip = user_ben_bank_name_strip.replace('&', 'and').replace('^', '').replace('_', '')
    user_ben_bank_name = user_ben_bank_name_strip
    ben_mobile_no = loan_application.user_personal.mobile_no
    remarks = 'Loan Request'
    today_date = datetime.datetime.now()
    loan_start_date_strip = datetime.datetime.strptime(str(today_date)[:-7],
                                                       "%Y-%m-%d %H:%M:%S")
    repayment_date = loan_start_date_strip + datetime.timedelta(days=int(tenure))

    user_loan_instance = LoanApplication.objects.get(loan_application_id=transaction_id)

    disbursed_amount_count = loan_application.disbursed_amount.all().count()
    if disbursed_amount_count:
        new_loan_application_id = str(loan_application_id) + str(disbursed_amount_count - 1)
    else:
        new_loan_application_id = loan_application_id

    transaction_id_text = new_loan_application_id
    amount = amount
    disburse_obj = RBLTransaction.objects.create(
        transaction_id_text=new_loan_application_id,
        amount=amount,
        txn_time=today_date,
        status='initiated'

    )

    payment_instance = Payments.objects.create(
        order_id=new_loan_application_id,
        status='initiated',
        category='Rbl',
        type='Account',
        mode=mode_of_payment,
        amount=amount,
        date=today_date
    )
    # Adding payment foreign key in user loan table
    loan_obj = LoanApplication.objects.filter(loan_application_id=transaction_id)
    loan_obj[0].disbursed_amount.add(payment_instance)
    json_obj = {}


    payload = "{\n  \"Single_Payment_Corp_Req\": {\n    \"Header\": {\n      \"TranID\": \"" + transaction_id + "\",\n      \"Corp_ID\": \"" + settings.CORP_ID + "\",\n      \"Maker_ID\": \"" + settings.MAKER_ID + "\",\n      \"Checker_ID\": \"" + settings.CHECKER_ID + "\",\n      \"Approver_ID\": \"" + settings.APPROVER_ID + "\"\n    },\n    \"Body\": {\n      \"Amount\": \"" + loan_amount + "\",\n      \"Debit_Acct_No\": \"" + debit_acc_no + "\",\n      \"Debit_Acct_Name\": \"" + debit_acc_name + "\",\n      \"Debit_IFSC\": \"" + debit_ifsc + "\",\n      \"Debit_Mobile\": \"" + debit_mobile_number + "\",\n      \"Ben_IFSC\": \"" + ben_IFSC + "\",\n      \"Ben_Acct_No\": \"" + ben_acc_number + "\",\n      \"Ben_Name\": \"" + ben_name + "\",\n      \"Ben_BankName\": \"" + user_ben_bank_name + "\",\n      \"Ben_Email\": \"" + ben_email + "\",\n      \"Ben_Mobile\": \"" + ben_mobile_no + "\",\n      \"Mode_of_Pay\": \"" + mode_of_payment + "\",\n      \"Remarks\": \"" + remarks + "\"\n    },\n    \"Signature\": {\n      \"Signature\": \"" + signature + "\"\n    }\n  }\n}"

    headers = {
        'content-type': "application/json",
        'authorization': "Basic QU5BTkRQUk9QRTpNdWRyYWt3aWtAMTIz",
        'cache-control': "no-cache",
    }


    response = requests.request(
        "POST",
        url,
        data=payload,
        headers=headers,
        params=querystring,

        cert=(
            '/etc/nginx/anandfin.com-ssl/cdb451c8d349fed-anandfin.com.crt',
            '/etc/nginx/anandfin.com-ssl/anandfin.com.key'
        )

    )

    json_instance = json.dumps(response.text)

    json_obj = response.json()

    signature = json_obj['Single_Payment_Corp_Resp']['Signature']['Signature']

    if json_obj['Single_Payment_Corp_Resp']['Header']['Status'] == 'Success':

        # get user txn obj

        if json_obj['Single_Payment_Corp_Resp']['Header']['TranID']:
            transaction_id = json_obj['Single_Payment_Corp_Resp']['Header']['TranID']
        else:
            transaction_id = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Corp_ID']:
            corp_id = json_obj['Single_Payment_Corp_Resp']['Header']['Corp_ID']
        else:
            corp_id = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Maker_ID']:
            maker_id = json_obj['Single_Payment_Corp_Resp']['Header']['Maker_ID']
        else:
            maker_id = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Checker_ID']:
            checker_id = json_obj['Single_Payment_Corp_Resp']['Header']['Checker_ID']
        else:
            checker_id = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Approver_ID']:
            approver_id = json_obj['Single_Payment_Corp_Resp']['Header']['Approver_ID']
        else:
            approver_id = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Resp_cde']:
            response_code = json_obj['Single_Payment_Corp_Resp']['Header']['Resp_cde']
        else:
            response_code = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Error_Desc']:
            error_description = json_obj['Single_Payment_Corp_Resp']['Header']['Error_Desc']
        else:
            error_description = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Status']:
            status = json_obj['Single_Payment_Corp_Resp']['Header']['Status']
        else:
            status = ''

        if json_obj['Single_Payment_Corp_Resp']['Body']['RefNo']:
            payment_ref_no = json_obj['Single_Payment_Corp_Resp']['Body']['RefNo']
        else:
            payment_ref_no = ''

        if json_obj['Single_Payment_Corp_Resp']['Body']['channelpartnerrefno']:
            channel_part_ref_no = json_obj['Single_Payment_Corp_Resp']['Body']['channelpartnerrefno']
        else:
            channel_part_ref_no = ''

        if json_obj['Single_Payment_Corp_Resp']['Body']['RRN']:
            rrn = json_obj['Single_Payment_Corp_Resp']['Body']['RRN']
        else:
            rrn = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Status']:
            payment_status = json_obj['Single_Payment_Corp_Resp']['Header']['Status']
        else:
            payment_status = ''

        if json_obj['Single_Payment_Corp_Resp']['Body']['Ben_Acct_No']:
            ben_acc_no = json_obj['Single_Payment_Corp_Resp']['Body']['Ben_Acct_No']
        else:
            ben_acc_no = ''

        if json_obj['Single_Payment_Corp_Resp']['Body']['Amount']:
            amount = json_obj['Single_Payment_Corp_Resp']['Body']['Amount']
        else:
            amount = ''

        if json_obj['Single_Payment_Corp_Resp']['Body']['BenIFSC']:
            ben_ifsc = json_obj['Single_Payment_Corp_Resp']['Body']['BenIFSC']
        else:
            ben_ifsc = ''

        if json_obj['Single_Payment_Corp_Resp']['Body']['Txn_Time']:
            tnx_payment_datetime = json_obj['Single_Payment_Corp_Resp']['Body']['Txn_Time']
        else:
            tnx_payment_datetime = ''

        date_converted = datetime.datetime.strptime(tnx_payment_datetime, '%Y-%m-%d %H:%M:%S.%f').strftime(
            '%Y-%m-%d %H:%M:%S')

    RBLTransaction.objects.filter(
        transaction_id_text=transaction_id,
    ).update(
        corp_id=corp_id, maker_id=maker_id,
        checker_id=checker_id, approver_id=approver_id,
        status=status, response_code=response_code, error_description=error_description,
        ref_number=payment_ref_no, channel_part_ref_number=channel_part_ref_no,
        rrn=rrn, ben_acc_no=ben_acc_no, amount=amount, ben_ifsc_code=ben_ifsc,
        txn_time=datetime.datetime.now(),
        payment=payment_instance
    )

    # TODO Remove hardcoded status , tnx_payment_datetime written for testing
    # status = "Success"
    # tnx_payment_datetime = today_date

    if status == 'Success':
        status_payment = 'success'
    elif status == 'Failure':
        status_payment = 'failed'
    else:
        status_payment = status
    Payments.objects.filter(
        order_id=transaction_id).update(status=status_payment, transaction_id=rrn, date=tnx_payment_datetime)

    LoanApplication.objects.filter(loan_application_id=transaction_id).update(
        status='APPROVED',
        loan_start_date=today_date,
        loan_end_date=repayment_date,

    )

    rp_date = repayment_date.date()
    loan_st_date = loan_start_date_strip.date()
    user_fcm = NotificationEntry.objects.filter(user_id=loan_application.user_id)


    # Email code

    email_kwargs = {
        'subject': 'CreditKart - Loan approved',
        'to_email': [str(loan_application.user_professional.company_email_id)],
        'email_template': 'emails/loan/loan_approved.html',
        'data': {
            'domain': Site.objects.get_current(),
            'loan_instance': loan_application
        },
    }

    email(**email_kwargs)

    # SMS code
    try:

        sms_message = 'We are pleased to inform you that your loan application number ' + str(
            new_loan_application_id) + ' has been transaction successfully! '

        sms_kwrgs = {
            'sms_message': sms_message,
            'number': str(loan_application.user.username)
        }

        sms(**sms_kwrgs)

    except:

        sms_message = 'We are pleased to inform you that your loan application number ' + str(
            new_loan_application_id) + ' has been transaction successfully! '

        sms_kwrgs = {
            'sms_message': sms_message,
            'number': str(loan_application.user.username)
        }

        sms(**sms_kwrgs)

    else:

        if json_obj['Single_Payment_Corp_Resp']['Header']['TranID']:
            transaction_id = json_obj['Single_Payment_Corp_Resp']['Header']['TranID']
        else:
            transaction_id = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Corp_ID']:
            corp_id = json_obj['Single_Payment_Corp_Resp']['Header']['Corp_ID']
        else:
            corp_id = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Maker_ID']:

            maker_id = json_obj['Single_Payment_Corp_Resp']['Header']['Maker_ID']
        else:
            maker_id = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Checker_ID']:
            checker_id = json_obj['Single_Payment_Corp_Resp']['Header']['Checker_ID']
        else:
            checker_id = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Approver_ID']:
            approver_id = json_obj['Single_Payment_Corp_Resp']['Header']['Approver_ID']
        else:
            approver_id = ''

        if 'Resp_cde' in json_obj['Single_Payment_Corp_Resp']['Header'] and \
                json_obj['Single_Payment_Corp_Resp']['Header']['Resp_cde'] is not None:
            response_code = json_obj['Single_Payment_Corp_Resp']['Header']['Resp_cde']
        else:
            response_code = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Error_Desc']:
            error_description = json_obj['Single_Payment_Corp_Resp']['Header']['Error_Desc']
        else:
            error_description = ''

        if json_obj['Single_Payment_Corp_Resp']['Header']['Status']:
            status = json_obj['Single_Payment_Corp_Resp']['Header']['Status']
        else:
            status = ''

    payment_ref_no = None

    try:
        channel_part_ref_no = json_obj['Single_Payment_Corp_Resp']['Body']['channelpartnerrefno']
    except:
        channel_part_ref_no = ''

    try:
        rrn = json_obj['Single_Payment_Corp_Resp']['Body']['RRN']
    except:
        rrn = ''

    ben_acc_no = None
    # amount = None
    ben_ifsc = None
    tnx_payment_datetime = None
    date_converted = None
    corp_id = 1
    maker_id = 1
    checker_id = 1
    approver_id = 1
    status = "success"
    response_code = 1
    error_description = 1
    # user_loan_instance = LoanApplication.objects.get(loan_application_id=transaction_id)

    RBLTransaction.objects.filter(
        transaction_id_text=new_loan_application_id,
    ).update(
        corp_id=corp_id, maker_id=maker_id,
        checker_id=checker_id, approver_id=approver_id,
        status=status, response_code=response_code, error_description=error_description,
        ref_number=payment_ref_no, channel_part_ref_number=channel_part_ref_no,
        rrn=rrn, ben_acc_no=ben_acc_no, amount=amount, ben_ifsc_code=ben_ifsc,
        txn_time=datetime.datetime.now()
    )
    Payments.objects.filter(order_id=new_loan_application_id).update(status=status, category='Rbl', type='Account',
                                                                     amount=amount, date=today_date)

    LoanApplication.objects.filter(loan_application_id=transaction_id).update(
        status='PENDING',
        loan_start_date=today_date,
        loan_end_date=repayment_date

    )

    # sms to admin for rbl rejection error

    import requests
    sms_message = 'Dear Admin, Loan Application number ' + str(
        transaction_id) + ' is not proceeding \n due to ' + str(error_description)
    sms_url = 'http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=' + str(
        settings.SMS_USERNAME) + '&password=' + str(settings.SMS_PASSWORD) + '&type=0&dlr=1&destination=' + str(
        settings.ADMIN_CONTACT_NUMBER) + '&source=' + str(settings.SMS_SOURCE) + '&message=' + str(sms_message)
    requests.get(sms_url)
    return True


# @shared_task()
def rbl_transaction_status(loan_application_id, *args, **kwargs):
    logger.info("In rbl_transaction_status")

    """
    The API fetches the status of the required transaction.
    Method: POST

    Access Requirement
        The following are the information that needs to be passed in every invocation to access
        1. Protocol Header – Authorization: Valid User Name and password in encoded in Base64 in the HTTP
        header called “Authorization” with a value “BASIC XXXXXXX” where XXXXXXX is the base64
        encoded value for the string that contains username and password. For e.g. user0001:Pasww0rd!
        2. URL Parameter – “client_id” with the value of the client Id obtained from the Advanced Developer Portal
        3. URL Parameter – “client_secret” with the value of the client secret obtained from the Advanced Developer
        Portal
        4. Client Certificate – A valid client certificate that the invoking App owner has shared with RBL API
        Administration team.

    Request Sample
        {
            "get_Single_Payment_Status_Corp_Req": {
                "Header": {
                    "TranID": "0001",
                    "Corp_ID": "MC001",
                    "Maker_ID": "M001",
                    "Checker_ID": "C001",
                    "Approver_ID": "A001"
                },
                "Body": {
                    "RefNo": "1005214345231"
                },
                "Signature": {
                    "Signature": "12345"
                }
            }
        }

    Response Sample:
        {
            "get_Single_Payment_Status_Res": {
                "Header": {
                    "TranID": "0001",
                    "Corp_ID": "MC001",
                    "Maker_ID": "M001",
                    "Checker_ID": "C001",
                    "Approver_ID": "A001",
                    "Status": "SUCCESS",
                    "Error_Cde": "",
                    "Error_Desc": ""
                },
                "Body": {
                    "REFNO": "1005214345231",
                    "UTRNO": "",
                    "RRN": "",
                    "BEN_ACCT_NO": "1000110010002463",
                    "TXNSTATUS": "Success",
                    "TXNTIME": "2015-10-21 13:19:42.34"
                },
                "Signature": {
                    "Signature": "12345"
                }
            }
        }
    :param loan_application_id:
    :param args:
    :param kwargs:
    :return:
    """

    trnsId = False
    transaction_id = None
    reference_no = None
    resp_dict = {}
    signature = ''
    certf = 'cdb451c8d349fed-anandfin.com.crt'
    cert_key = 'anandfin.com.key'
    cert_path = settings.BASE_DIR + '/cert/' + certf
    key_path = settings.BASE_DIR + '/cert/' + cert_key
    cert = (
        cert_path,
        key_path
    )
    loan_application_id = str(loan_application_id)
    try:
        loan_application = LoanApplication.objects.get(loan_application_id=loan_application_id)
        if loan_application is not None:
            rbl_transaction = RBLTransaction.objects.get(transaction_id_text=loan_application_id)
            if rbl_transaction is not None:
                transaction_id = rbl_transaction.transaction_id_text
                reference_no = rbl_transaction.ref_number
                trnsId = True
    except Exception as e:
        print("---Exception in Rbs status api", e)

    # UAT
    # url = "https://apideveloper.rblbank.com/test/sb/rbl/v1/payments/corp/payment/query"

    # Prod
    url = "https://gateway.rblbank.com/prod/sb/rbl/v1/payments/corp/payment/query"

    querystring = {
        "client_id": "1e4081cb-fc29-43ac-963b-f6df0dff228e",
        "client_secret": "F6uO3oH3rE3tB1vR7bW0nG0bS6hA7vE8mP7vP5mF7pS2dP0rF7"
    }

    if trnsId is not None and trnsId != False:
        if loan_application_id:
            reference_no = "SPANANDPROPE" + str(loan_application_id)
            transaction_id = str(loan_application_id)
        if reference_no is not None:
            try:
                payload = "{\n  \"get_Single_Payment_Status_Corp_Req\": {\n    \"Header\": {\n      \"TranID\": \"" + transaction_id + "\",\n      \"Corp_ID\": \"" + settings.CORP_ID + "\",\n      \"Maker_ID\": \"" + settings.MAKER_ID + "\",\n      \"Checker_ID\": \"" + settings.CHECKER_ID + "\",\n      \"Approver_ID\": \"" + settings.APPROVER_ID + "\"\n    },\n    \"Body\": {\n      \"RefNo\": \"" + reference_no + "\"\n    },\n    \"Signature\": {\n      \"Signature\": \"" + signature + "\"\n    }\n  }\n}"

                headers = {
                    'content-type': "application/json",
                    'authorization': "Basic QU5BTkRQUk9QRTpNdWRyYWt3aWtAMTIz",
                    'cache-control': "no-cache",
                }


                response = requests.request(
                    "POST",
                    url,
                    data=payload,
                    headers=headers,
                    params=querystring,
                    # cert=(
                    #     cert_path,
                    #     key_path
                    # )
                    # Production
                    cert=(
                        '/etc/nginx/anandfin.com-ssl/cdb451c8d349fed-anandfin.com.crt',
                        '/etc/nginx/anandfin.com-ssl/anandfin.com.key'
                    )
                )
                json_obj = response.json()

                response_base = json_obj['get_Single_Payment_Status_Corp_Res']
                response_header = response_base['Header']
                response_body = ''
                if 'Body' in response_base:
                    response_body = response_base['Body']
                else:
                    response_body = ''

                if response_header:
                    if response_header['TranID']:
                        transaction_id = response_header['TranID']
                    else:
                        transaction_id = ''

                if response_header['Corp_ID']:
                    corp_id = response_header['Corp_ID']
                else:
                    corp_id = ''

                if response_header['Maker_ID']:
                    maker_id = response_header['Maker_ID']
                else:
                    maker_id = ''

                if response_header['Checker_ID']:
                    checker_id = response_header['Checker_ID']
                else:
                    checker_id = ''

                if response_header['Approver_ID']:
                    approver_id = response_header['Approver_ID']
                else:
                    approver_id = ''

                if response_header['Error_Cde']:
                    response_code = response_header['Error_Cde']
                else:
                    response_code = ''

                if response_header['Error_Desc']:
                    error_description = response_header['Error_Desc']
                else:
                    error_description = ''

                if response_header['Status']:
                    status = response_header['Status']
                else:
                    status = ''

                if 'REFNO' in response_body:
                    if response_body['REFNO']:
                        payment_ref_no = response_body['REFNO']
                    else:
                        payment_ref_no = ''
                else:
                    payment_ref_no = ''

                if 'RRN' in response_body:
                    if response_body['RRN']:
                        rrn = response_body['RRN']
                    else:
                        rrn = ''
                else:
                    rrn = ''
                amount = None
                if 'AMOUNT' in response_body:
                    if response_body['AMOUNT']:
                        amount = response_body['AMOUNT']
                    else:
                        amount = None
                else:
                    amount = None

                if 'PAYMENTSTATUS' in response_body:
                    if response_body['PAYMENTSTATUS']:
                        payment_status_no = response_body['PAYMENTSTATUS']
                    else:
                        payment_status_no = ''
                else:
                    payment_status_no = ''

                if 'BEN_ACCT_NO' in response_body:
                    if response_body['BEN_ACCT_NO']:
                        ben_acc_no = response_body['BEN_ACCT_NO']
                    else:
                        ben_acc_no = ''
                else:
                    ben_acc_no = ''

                if 'BENEFICIARYNAME' in response_body:
                    if response_body['BENEFICIARYNAME']:
                        ben_name = response_body['BENEFICIARYNAME']
                    else:
                        ben_name = ''
                else:
                    ben_name = ''

                if 'IFSCCODE' in response_body:
                    if response_body['IFSCCODE']:
                        ben_ifsc = response_body['IFSCCODE']
                    else:
                        ben_ifsc = ''
                else:
                    ben_ifsc = ''

                if 'TXNTIME' in response_body:
                    if response_body['TXNTIME']:
                        txn_payment_datetime = response_body['TXNTIME']
                    else:
                        txn_payment_datetime = ''
                else:
                    txn_payment_datetime = ''

                try:
                    if 'STATUSDESC' in response_body:
                        if response_body['STATUSDESC']:
                            txn_status_description = response_body['STATUSDESC']
                        else:
                            txn_status_description = ''
                    else:
                        txn_status_description = ''
                except Exception as e:
                    print('-----------in exception----------')
                    print(e.args)
                    import sys
                    import os
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                    print(exc_type, fname, exc_tb.tb_lineno)
                    txn_status_description = ''
                try:
                    if 'TXNSTATUS' in response_body:
                        if response_body['TXNSTATUS']:
                            txn_status = response_body['TXNSTATUS']
                        else:
                            txn_status = ''
                    else:
                        txn_status = ''

                except Exception as e:
                    print('-----------in exception----------')
                    print(e.args)
                    import sys
                    import os
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                    print(exc_type, fname, exc_tb.tb_lineno)
                    txn_status = ''
                date_converted = None
                if txn_payment_datetime:
                    date_converted = datetime.datetime.strptime(txn_payment_datetime, '%Y-%m-%d %H:%M:%S.%f').strftime(
                        '%Y-%m-%d %H:%M:%S')


                resp_dict['transaction_id'] = transaction_id
                resp_dict['status'] = txn_status
                resp_dict['amount'] = amount
                resp_dict['ben_acc_no'] = ben_acc_no
                resp_dict['rrn'] = rrn
                resp_dict['ben_ifsc_code'] = ben_ifsc
                resp_dict['ref_number'] = reference_no
                resp_dict['response_code'] = response_code
                resp_dict['error_description'] = error_description
                resp_dict['payment_status_code'] = payment_status_no
                resp_dict['transaction_time'] = date_converted
                loan_application = LoanApplication.objects.get(loan_application_id=loan_application_id)
                if loan_application:
                    loan_status = loan_application.status

                if payment_status_no == "7":
                    status = "success"
                    loan_status = "APPROVED"
                elif payment_status_no == "8":
                    status = "failed"
                    loan_status = 'CANCELLED'
                elif payment_status_no == "9":
                    status = "success"
                elif status == "FAILED":
                    status = "failed"
                    loan_status = 'CANCELLED'
                else:
                    pass

                channel_part_ref_no = ''
                RBLTransaction.objects.filter(
                    transaction_id_text=transaction_id,
                ).update(
                    corp_id=corp_id, maker_id=maker_id,
                    checker_id=checker_id, approver_id=approver_id,
                    status=status, response_code=response_code, error_description=error_description,
                    ref_number=payment_ref_no, channel_part_ref_number=channel_part_ref_no,
                    rrn=rrn, ben_acc_no=ben_acc_no, amount=amount, ben_ifsc_code=ben_ifsc,
                    txn_time=datetime.datetime.now()
                )

                updated_date = datetime.datetime.now()
                Payments.objects.filter(
                    order_id=transaction_id).update(status=status, transaction_id=rrn,
                                                    update_date=updated_date)
                LoanApplication.objects.filter(loan_application_id=transaction_id).update(
                    status=loan_status,
                    updated_date=updated_date, reason="change for rbl transaction status api check "

                )

                print(status)
            except Exception as e:
                print('-----------in exception----------')
                print(e.args)
                import os
                import sys
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)

    return resp_dict


def rbl_schedular():
    """
    The single payment api(rbl), mode: IMPS

    Payment Initiation – Response Status=  Success and Resp_code = ‘00’- Confirmed Success

    Payment status- Response PAYMENTSTATUS
    Ø Payment Status- 7: Confirmed success payment.
        Sender account will be debited by the mentioned amount and
     in real time the same will be credited to Beneficiary/Receiver account.

    Ø Payment Status- 8: Confirmed Failure payment.
        Sender account will be debited by the mentioned amount and
         the same will be refunded to Sender Account in real time.

    Ø Payment Status- 9:
        Deemed Success (In this state Transaction may be success or failure which will be updated later)
        Sender account will be debited by the pay amount and
        mark the status as 9 (Deemed Success) for the cases:
         Wrong IFSC code/Incorrect Beneficiary Account/ Incorrect  MMID/Wrong Mobile number provided while initiating payment.
          Later NPCI will either Approve or reject the Payment.
    """

    get_rbl_data = RBLTransaction.objects.filter(response_code="9")
    if get_rbl_data:
        for i in get_rbl_data:
            get_status_resp = rbl_transaction_status(i.loan_application_id)
            # get_status_resp = rbl_transaction_status("199271246309414")
            # TODO save the updated status
            if "payment_status_code" in get_status_resp:
                if get_status_resp['payment_status_code'] == "7" or get_status_resp['payment_status_code'] == "8":
                    update_tranasaction = RBLTransaction.objects.filter(
                        transaction_id_text=i.loan_application_id).update(status=get_status_resp['status'])
    return HttpResponse("-ok")




def call_rbl_status_api(request):
    loan_application_id = '191114256219517'
    loan_application_id = '1911410608469'
    get_rbl_trans_dict(loan_application_id)
    return HttpResponse("ok")


def get_rbl_trans_dict(loan_application_id):
    loan_application_id = str(loan_application_id)
    reference_no = None
    transaction_id = loan_application_id
    try:
        loan_application = LoanApplication.objects.get(loan_application_id=loan_application_id)
        if loan_application is not None:
            rbl_transaction = RBLTransaction.objects.get(transaction_id_text=loan_application_id)
            if rbl_transaction is not None:
                transaction_id = rbl_transaction.transaction_id_text
                reference_no = rbl_transaction.ref_number
                trnsId = True
    except Exception as e:
        print("---Exception in Rbs status api", e)
        import os
        import sys
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        pass
    get_rbl_transaction_status(reference_no, transaction_id)


def get_rbl_transaction_status(reference_no, transaction_id):
    if transaction_id is None:
        raise ValueError("Transaction id is mandatory")
    if reference_no is None:
        reference_no = "SPANANDPROPE" + str(transaction_id)
        transaction_id = str(transaction_id)

    resp_dict = {}
    signature = ''
    certf = 'cdb451c8d349fed-anandfin.com.crt'
    cert_key = 'anandfin.com.key'
    cert_path = settings.BASE_DIR + '/cert/' + certf
    key_path = settings.BASE_DIR + '/cert/' + cert_key

    # UAT
    # url = "https://apideveloper.rblbank.com/test/sb/rbl/v1/payments/corp/payment/query"

    # Prod
    url = "https://gateway.rblbank.com/prod/sb/rbl/v1/payments/corp/payment/query"

    querystring = {
        "client_id": "1e4081cb-fc29-43ac-963b-f6df0dff228e",
        "client_secret": "F6uO3oH3rE3tB1vR7bW0nG0bS6hA7vE8mP7vP5mF7pS2dP0rF7"
    }

    try:
        payload = "{\n  \"get_Single_Payment_Status_Corp_Req\": {\n    \"Header\": {\n      \"TranID\": \"" + transaction_id + "\",\n      \"Corp_ID\": \"" + settings.CORP_ID + "\",\n      \"Maker_ID\": \"" + settings.MAKER_ID + "\",\n      \"Checker_ID\": \"" + settings.CHECKER_ID + "\",\n      \"Approver_ID\": \"" + settings.APPROVER_ID + "\"\n    },\n    \"Body\": {\n      \"RefNo\": \"" + reference_no + "\"\n    },\n    \"Signature\": {\n      \"Signature\": \"" + signature + "\"\n    }\n  }\n}"

        headers = {
            'content-type': "application/json",
            'authorization': "Basic QU5BTkRQUk9QRTpNdWRyYWt3aWtAMTIz",
            'cache-control': "no-cache",
        }

        print('&&&&&&&&&&&&&&&& payload &&&&&&&&&&&&&\n')
        print(payload)
        print('&&&&&&&&&&&&&&&& payload &&&&&&&&&&&&&\n')

        response = requests.request(
            "POST",
            url,
            data=payload,
            headers=headers,
            params=querystring,
            cert=(
                cert_path,
                key_path
            )
            # Production
            # cert=(
            #     '/etc/nginx/anandfin.com-ssl/cdb451c8d349fed-anandfin.com.crt',
            #     '/etc/nginx/anandfin.com-ssl/anandfin.com.key'
            # )
        )
        print("----------response--")
        print(response)
        json_obj = response.json()
        print("----")
        print(json_obj)
        print("----")

        response_base = json_obj['get_Single_Payment_Status_Corp_Res']
        response_header = response_base['Header']
        response_body = ''
        if 'Body' in response_base:
            response_body = response_base['Body']
        else:
            response_body = ''

        if response_header:
            if response_header['TranID']:
                transaction_id = response_header['TranID']
            else:
                transaction_id = ''

        if response_header['Corp_ID']:
            corp_id = response_header['Corp_ID']
        else:
            corp_id = ''

        if response_header['Maker_ID']:
            maker_id = response_header['Maker_ID']
        else:
            maker_id = ''

        if response_header['Checker_ID']:
            checker_id = response_header['Checker_ID']
        else:
            checker_id = ''

        if response_header['Approver_ID']:
            approver_id = response_header['Approver_ID']
        else:
            approver_id = ''

        if response_header['Error_Cde']:
            response_code = response_header['Error_Cde']
        else:
            response_code = ''

        if response_header['Error_Desc']:
            error_description = response_header['Error_Desc']
        else:
            error_description = ''

        if response_header['Status']:
            status = response_header['Status']
        else:
            status = ''

        if 'REFNO' in response_body:
            if response_body['REFNO']:
                payment_ref_no = response_body['REFNO']
            else:
                payment_ref_no = ''
        else:
            payment_ref_no = ''

        if 'RRN' in response_body:
            if response_body['RRN']:
                rrn = response_body['RRN']
            else:
                rrn = ''
        else:
            rrn = ''
        amount = None
        if 'AMOUNT' in response_body:
            if response_body['AMOUNT']:
                amount = response_body['AMOUNT']
            else:
                amount = None
        else:
            amount = None

        if 'PAYMENTSTATUS' in response_body:
            if response_body['PAYMENTSTATUS']:
                payment_status_no = response_body['PAYMENTSTATUS']
            else:
                payment_status_no = ''
        else:
            payment_status_no = ''

        if 'BEN_ACCT_NO' in response_body:
            if response_body['BEN_ACCT_NO']:
                ben_acc_no = response_body['BEN_ACCT_NO']
            else:
                ben_acc_no = ''
        else:
            ben_acc_no = ''

        if 'BENEFICIARYNAME' in response_body:
            if response_body['BENEFICIARYNAME']:
                ben_name = response_body['BENEFICIARYNAME']
            else:
                ben_name = ''
        else:
            ben_name = ''

        if 'IFSCCODE' in response_body:
            if response_body['IFSCCODE']:
                ben_ifsc = response_body['IFSCCODE']
            else:
                ben_ifsc = ''
        else:
            ben_ifsc = ''

        if 'TXNTIME' in response_body:
            if response_body['TXNTIME']:
                txn_payment_datetime = response_body['TXNTIME']
            else:
                txn_payment_datetime = ''
        else:
            txn_payment_datetime = ''

        try:
            if 'STATUSDESC' in response_body:
                if response_body['STATUSDESC']:
                    txn_status_description = response_body['STATUSDESC']
                else:
                    txn_status_description = ''
            else:
                txn_status_description = ''
        except Exception as e:
            print('-----------in exception----------')
            print(e.args)
            import sys
            import os
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            txn_status_description = ''
        try:
            if 'TXNSTATUS' in response_body:
                if response_body['TXNSTATUS']:
                    txn_status = response_body['TXNSTATUS']
                else:
                    txn_status = ''
            else:
                txn_status = ''

        except Exception as e:
            print('-----------in exception----------')
            print(e.args)
            import sys
            import os
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            txn_status = ''
        date_converted = None
        if txn_payment_datetime:
            date_converted = datetime.datetime.strptime(txn_payment_datetime, '%Y-%m-%d %H:%M:%S.%f').strftime(
                '%Y-%m-%d %H:%M:%S')


        if payment_status_no == "7":
            status = "success"
            loan_status = "APPROVED"
        elif payment_status_no == "8":
            status = "failed"
            loan_status = 'CANCELLED'
        elif payment_status_no == "9":
            status = "success"
        elif status == "FAILED":
            status = "failed"
            loan_status = 'CANCELLED'
        else:
            status = txn_status

        resp_dict['transaction_id'] = transaction_id
        resp_dict['ref_number'] = reference_no
        resp_dict['status'] = status
        resp_dict['amount'] = amount
        resp_dict['rrn'] = rrn
        resp_dict['ben_acc_no'] = ben_acc_no
        resp_dict['ben_ifsc_code'] = ben_ifsc
        resp_dict['response_code'] = response_code
        resp_dict['error_description'] = error_description
        resp_dict['payment_status_code'] = payment_status_no
        resp_dict['txn_time'] = date_converted

        processs_rbl_transcation_data(resp_dict)
        channel_part_ref_no = ''

    except Exception as e:
        print('-----------in exception----------')
        print(e.args)
        import os
        import sys
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)


def processs_rbl_transcation_data(data):
    try:
        if data:
            transaction_id = data['transaction_id']
            try:
                # loan_application = LoanApplication.objects.get(loan_application_id=transaction_id)
                loan_application = LoanApplication.objects.filter(loan_application_id=transaction_id)
                if loan_application:
                    loan_application = loan_application[0]
            except Exception as e:
                print(e)
                loan_application is None
                pass

            if loan_application:
                loan_status = loan_application.status
                if loan_status == 'APPROVED':
                    if data['status'] == 'success':
                        new_loan_status = 'APPROVED'
                    elif data['status'] == 'failed':
                        new_loan_status = 'CANCELLED'
                elif loan_status == 'COMPLETED':
                    if data['status'] == 'success':
                        new_loan_status = 'COMPLETED'
                    elif data['status'] == 'failed':
                        new_loan_status = 'CANCELLED'
                else:
                    if data['status'] == 'success':
                        new_loan_status = loan_status
                    elif data['status'] == 'failed':
                        new_loan_status = 'CANCELLED'

                channel_part_ref_number = 'IMPS' + data['ref_number']
                if data['txn_time'] is not None:
                    tnx_time = str(data['txn_time'])[:10]

                update_rbl_obj = RBLTransaction.objects.filter(
                    transaction_id_text=transaction_id,
                ).update(
                    status=data['status'], response_code=data['response_code'],
                    error_description=data['error_description'],
                    ref_number=data['ref_number'], channel_part_ref_number=channel_part_ref_number,
                    rrn=data['rrn'], ben_acc_no=data['ben_acc_no'], amount=data['amount'],
                    ben_ifsc_code=data['ben_ifsc_code'],
                    txn_time=tnx_time, updated_date=datetime.datetime.now()
                )
                updated_date = datetime.datetime.now()
                update_pay_obj = Payments.objects.filter(
                    order_id=transaction_id).update(status=data['status'], transaction_id=data['rrn'],
                                                    update_date=datetime.datetime.now(), date=data['txn_time'],
                                                    description=data['error_description'],
                                                    response_code=data['response_code'],
                                                    return_status='rbl_status')

                update_loan_obj = LoanApplication.objects.filter(loan_application_id=transaction_id).update(
                    status=new_loan_status,
                    updated_date=updated_date, reason="change for rbl transaction status api check "
                )
                print("=========", update_loan_obj)
    except Exception as e:
        print('-----------in exception----------')
        print(e.args)
        import os
        import sys
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
