# Create your models here.
from django.db import models
# from django.apps import apps
# LoanApplication = apps.get_model('LoanAppData', 'LoanApplication')

from mudra import constants
from postgres_copy import CopyManager
from django_currentuser.db.models import CurrentUserField


class Payment(models.Model):
    order_id = models.CharField(db_index=True, max_length=200, null=True, blank=True)
    transaction_id = models.CharField(db_index=True, max_length=200, null=True, blank=True)
    pg_transaction_id = models.CharField(max_length=50, null=True, blank=True)
    status = models.CharField(db_index=True, max_length=50, null=True, blank=True, choices=constants.STATUS_TYPE)
    category = models.CharField(db_index=True, max_length=50, null=True, blank=True, choices=constants.PAYMENT_CTG)
    mode = models.CharField(db_index=True, max_length=50, null=True, blank=True, choices=constants.PAYMENT_MODE)
    type = models.CharField(db_index=True, max_length=50, null=True, blank=True, choices=constants.PAYMENT_Type)
    response = models.TextField(blank=True, null=True)
    amount = models.FloatField(db_index=True, max_length=50, null=True, blank=True)
    description = models.CharField(max_length=256, blank=True, null=True)
    date = models.DateTimeField(db_index=True, null=True, blank=True)
    pay_source = models.CharField(db_index=True, null=True, blank=True, max_length=50, choices=constants.PaymentGateway)
    return_status = models.CharField(db_index=True, null=True, blank=True, max_length=50,
                                     choices=constants.ReturnStatus)
    response_code = models.CharField(db_index=True, null=True, blank=True, max_length=50)
    product_type = models.CharField(default="Mudra", max_length=256, blank=True, null=True,
                                    choices=constants.PRODUCT_TYPE)
    platform_type = models.CharField(default="App", max_length=20, blank=True, null=True,
                                     choices=constants.PLATFORM_TYPE)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by_new = CurrentUserField(related_name="payment_created_by_new", null=True, blank=True,
                                      editable=False)
    updated_by_new = CurrentUserField(on_update=True, related_name="payment_updated_by_new",
                                      null=True, blank=True, editable=False)

    objects = CopyManager()


class RBLTransaction(models.Model):
    """
    To store rbl data
    """

    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    # transaction_id = models.ForeignKey(LoanApplication, on_delete=models.PROTECT, blank=True, null=True,
    #                                    related_name='rbl_trn_id')
    transaction_id_text = models.CharField(db_index=True, max_length=256, blank=True, null=True)
    corp_id = models.CharField(max_length=256, blank=True, null=True)
    maker_id = models.CharField(max_length=256, blank=True, null=True)
    checker_id = models.CharField(max_length=256, blank=True, null=True)
    approver_id = models.CharField(max_length=256, blank=True, null=True)
    status = models.CharField(db_index=True, max_length=256, blank=True, null=True)
    response_code = models.CharField(max_length=256, blank=True, null=True)
    error_description = models.TextField(blank=True, null=True)
    ref_number = models.CharField(max_length=256, blank=True, null=True)
    channel_part_ref_number = models.CharField(max_length=256, blank=True, null=True)
    rrn = models.CharField(max_length=256, blank=True, null=True)
    ben_acc_no = models.CharField(db_index=True, max_length=256, blank=True, null=True)
    amount = models.FloatField(max_length=256, blank=True, null=True)
    ben_ifsc_code = models.CharField(max_length=256, blank=True, null=True)
    txn_time = models.DateField(blank=True, null=True)
    payment = models.ForeignKey(Payment, blank=True, null=True, related_name="payment_dis", on_delete=models.PROTECT)


class PaymentLink(models.Model):
    loan_id = models.CharField(max_length=256, blank=True, null=True)
    merchant_request_id = models.CharField(max_length=256, blank=True, null=True)
    link_id = models.CharField(max_length=256, unique=True, blank=True, null=True)
    link_type = models.CharField(max_length=256, blank=True, null=True)
    long_url = models.CharField(max_length=256, blank=True, null=True)
    short_url = models.CharField(max_length=256, blank=True, null=True)
    request_amount = models.CharField(max_length=256, blank=True, null=True)
    customer_name = models.CharField(db_index=True, max_length=256, blank=True, null=True)
    customer_no = models.CharField(db_index=True, max_length=20, blank=True, null=True)
    customer_email = models.CharField(max_length=60, blank=True, null=True)
    expiry_date = models.DateTimeField(max_length=256, blank=True, null=True)
    is_active = models.BooleanField(default=False, blank=True, null=True)
    send_email = models.BooleanField(default=False, blank=True, null=True)
    send_sms = models.BooleanField(default=False, blank=True, null=True)
    link_created_date = models.DateTimeField(null=True, blank=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, null=True, blank=True)
