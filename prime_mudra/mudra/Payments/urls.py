from django.urls import path

from .tasks import rbl_transaction_status, call_rbl_status_api
from .views import *

urlpatterns = [

    path(r'income_expense_payments_monthwise/', IncomeExpenseSubscriptionByMonth.as_view(),
         name='income_expense_payments_montpaytm_create_link_apihwise'),

    path('rbl_transaction_status/<loan_id>/', rbl_transaction_sts, name='rbl_transaction_status'),
    path(r'monthly-payments/', PaymentsByMonths.as_view(), name='Monthly_payments'),
    path(r'daily-payments/<on_month>/', PaymentsByDates.as_view(), name='Daily_payments'),
    # path(r'user-contacts/<user>/', UserContactDetails.as_view(), name='user_contacts'),
    path(r'dailysubcheck/', dailysubscriptioncheck, name='dailysubcheck'),

    # Monthly and daywise manually cancelled report
    path(r'monthly-manually-cancelled/', MonthlyManuallyCancelled.as_view()),
    path(r'daywise-manually-cancelled/<on_month>/', DaywiseManuallyCancelled.as_view()),
    path(r'manually-cancelled-onday/<on_date>/', ManuallyCancelledOnday.as_view()),

    # path('rbl-initiated/', RblInitiated.as_view()),
    path(r'call_rbl_status_api/', call_rbl_status_api, name='call_rbl_status_api'),

]
